/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "core/Platform.h"

#if defined(__PLATFORM_WINDOWS__)
#elif defined(__PLATFORM_MAC__)
  #include <mach-o/dyld.h>
  #include <limits.h>
#else
  #include <unistd.h>
#endif

#include <string>
#include <algorithm>
#include <vector>
#include <deque>

#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/algorithm/string/predicate.hpp>

#include "utility/Filesystem.h"

#if defined(__PLATFORM_WINDOWS__)                                                                                                             
    #include "utility/windows/Filesystem.ipp"
#elif defined(__PLATFORM_MAC__)

#else
    #include "utility/unix/Filesystem.ipp"
#endif

namespace zillians {

// see: http://stackoverflow.com/questions/1746136/how-do-i-normalize-a-pathname-using-boostfilesystem/1750710#1750710
auto Filesystem::resolve(const boost::filesystem::path& p) -> boost::filesystem::path 
{
    //p = boost::filesystem::absolute(p);
    boost::filesystem::path result;
    for(boost::filesystem::path::iterator it=p.begin();
        it!=p.end();
        ++it)
    {
        if(*it == "..")
        {
            // /a/b/.. is not necessarily /a if b is a symbolic link
            if(boost::filesystem::is_symlink(result) )
                result /= *it;
            // /a/b/../.. is not /a/b/.. under most circumstances
            // We can end up with ..s in our result because of symbolic links
            else if(result.filename() == "..")
                result /= *it;
            // Otherwise it should be safe to resolve the parent
            else
                result = result.parent_path();
        }
        else if(*it == ".")
        {
            // Ignore
        }
        else
        {
            // Just cat other path entries
            result /= *it;
        }
    }
    return result;
}

bool Filesystem::enumerate_package(const boost::filesystem::path& root, const boost::filesystem::path& p, std::deque<std::wstring>& sequence)
{
    boost::filesystem::path t = p.parent_path();

    while (true)
    {
        if (t.empty())
            return false;

        if (t == root)
            break;
        else
            sequence.push_front(t.stem().wstring());

        t = t.parent_path();
    }

    return true;
}

auto Filesystem::normalize_path(boost::filesystem::path p) -> boost::filesystem::path 
{
    if(p.is_absolute())
        return resolve(p);
    else
        return resolve(boost::filesystem::absolute(p));
}

auto Filesystem::current_executable_path() -> boost::filesystem::path 
{
#if defined __PLATFORM_WINDOWS__
    char p[MAX_PATH];
    GetModuleFileName(NULL, p, MAX_PATH)
    return boost::filesystem::path(p);
#elif defined __PLATFORM_MAC__
    char buf [PATH_MAX];
    uint32_t bufsize = PATH_MAX;
    _NSGetExecutablePath(buf, &bufsize);
    return boost::filesystem::path(buf);
#else 
    pid_t pid = getpid();
    char proc_link[2048]; sprintf(proc_link, "/proc/%d/exe", pid);
    char sym_link[2048];
    std::size_t len = readlink(proc_link, sym_link, 1024);
    sym_link[len] = '\0';
    return boost::filesystem::path(sym_link);
#endif
}

auto Filesystem::path_difference(const boost::filesystem::path& base_path, const boost::filesystem::path& sub_path) -> boost::filesystem::path 
{
    boost::filesystem::path result;
    boost::filesystem::path t = sub_path;

    while(t != base_path && !t.empty()) {
        result = t.filename() / result;
        t = t.parent_path();
    }

    if(t.empty())
        return sub_path;
    else
        return result;
}

auto Filesystem::collect_files(const boost::filesystem::path& p, const std::string& extension, bool recursively) -> std::vector<boost::filesystem::path> 
{
    std::vector<boost::filesystem::path> result;

    if(is_directory(p))
    {
        if(recursively)
        {
            for(auto i = boost::filesystem::recursive_directory_iterator(p), e = boost::filesystem::recursive_directory_iterator(); i != e; ++i)
            {
                if(extension == "*" || i->path().extension() == extension)
                {
                    result.push_back(i->path());
                }
            }
        }
        else
        {
            for(auto i = boost::filesystem::directory_iterator(p), e = boost::filesystem::directory_iterator(); i != e; ++i)
            {
                if(extension == "*" || i->path().extension() == extension)
                {
                    result.push_back(i->path());
                }
            }
        }
    }

    return result;
}

bool Filesystem::is_directory(const boost::filesystem::path& p)
{
    return boost::filesystem::exists(p) && boost::filesystem::is_directory(p);
}

bool Filesystem::is_regular_file(const boost::filesystem::path& p)
{
    return boost::filesystem::exists(p) && boost::filesystem::is_regular_file(p);
}

bool Filesystem::have_different_timestamps(const boost::filesystem::path& a, const boost::filesystem::path& b)
{
    if(!is_regular_file(a))
        return false;

    if(!is_regular_file(b))
        return false;

    return (a == b);
}

bool Filesystem::have_different_size(const boost::filesystem::path& a, const boost::filesystem::path& b)
{
    if(!is_regular_file(a))
        return false;

    if(!is_regular_file(b))
        return false;

    return boost::filesystem::file_size(a) == boost::filesystem::file_size(b);
}

bool Filesystem::is_different_files(const boost::filesystem::path& a, const boost::filesystem::path& b)
{
    return have_different_timestamps(a, b) || have_different_size(a, b);
}

bool Filesystem::copy_file_if_differnt(const boost::filesystem::path& src, const boost::filesystem::path& dest)
{
    if(!is_regular_file(src))
        return false;

    if(is_regular_file(dest))
    {
        std::time_t a = boost::filesystem::last_write_time(src);
        std::time_t b = boost::filesystem::last_write_time(dest);
        if(a == b)
            return true;

        boost::filesystem::remove(dest);
    }

    // temporary work-around but it's not perfect since it does not handle the case when disk is full
    // TODO switch back to boost copy_file
    std::ifstream infile(src.string(), std::ios_base::binary);
    std::ofstream outfile(dest.string(), std::ios_base::binary);
    outfile << infile.rdbuf();
    return true;
}

} // namespace zillians
