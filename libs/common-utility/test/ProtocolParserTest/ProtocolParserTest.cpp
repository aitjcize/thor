/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "core/Prerequisite.h"
#include "utility/ProtocolParser.h"

#define BOOST_TEST_MODULE ProtocolParserTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

using namespace std;
using namespace zillians;

BOOST_AUTO_TEST_SUITE( ProtocolParserTestSuite )

BOOST_AUTO_TEST_CASE( ProtocolParserTestCase1 )
{
	ProtocolParser<std::string::iterator> parser;
	std::string s = "infiniband://api.zillians.com";
	BOOST_CHECK(parser.parse(s.begin(), s.end()));
	BOOST_CHECK(parser.transport == "infiniband");
	BOOST_CHECK(parser.address == "api.zillians.com");
	BOOST_CHECK(!parser.port.is_initialized());
	BOOST_CHECK(parser.options.size() == 0);
}

BOOST_AUTO_TEST_CASE( ProtocolParserTestCase2 )
{
	ProtocolParser<std::string::iterator> parser;
	std::string s = "infiniband://127.0.0.1:4567";
	BOOST_CHECK(parser.parse(s.begin(), s.end()));
	BOOST_CHECK(parser.transport == "infiniband");
	BOOST_CHECK(parser.address == "127.0.0.1");
	BOOST_CHECK(parser.port.is_initialized());
	BOOST_CHECK(parser.port.get() == 4567);
	BOOST_CHECK(parser.options.size() == 0);
}

BOOST_AUTO_TEST_CASE( ProtocolParserTestCase3 )
{
	ProtocolParser<std::string::iterator> parser;
	std::string s = "tcp://api.zillians.com:1234[no-delay=true,hello,world=123]";
	BOOST_CHECK(parser.parse(s.begin(), s.end()));
	BOOST_CHECK(parser.transport == "tcp");
	BOOST_CHECK(parser.address == "api.zillians.com");
	BOOST_CHECK(parser.port.is_initialized());
	BOOST_CHECK(parser.port.get() == 1234);
	BOOST_CHECK(parser.options.size() == 3);
	if(parser.options.size() > 0)
	{
		BOOST_CHECK(parser.options[0].first == "no-delay");
		BOOST_CHECK(parser.options[0].second.is_initialized());
		BOOST_CHECK(parser.options[0].second.get() == "true");
	}
	if(parser.options.size() > 1)
	{
		BOOST_CHECK(parser.options[1].first == "hello");
		BOOST_CHECK(!parser.options[1].second.is_initialized());
	}
	if(parser.options.size() > 2)
	{
		BOOST_CHECK(parser.options[2].first == "world");
		BOOST_CHECK(parser.options[2].second.is_initialized());
		BOOST_CHECK(parser.options[2].second.get() == "123");
	}
}

BOOST_AUTO_TEST_SUITE_END()
