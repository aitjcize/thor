/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <functional>

#include <boost/assert.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/thread/lock_factories.hpp>

#include "core/IntTypes.h"

#include "language/tree/ASTNodeHelper.h"
#include "language/stage/generator/context/SynthesizedObjectLayoutContext.h"

#include "framework/service/detail/ProxyHelper.h"
#include "framework/service/object/ObjectServiceMT.h"
#include "framework/processor/KernelMT.h"
#include "framework/ProcessorId.h"

namespace zillians { namespace framework { namespace service { namespace mt {

ZILLIANS_FRAMEWORK_SERVICE_PROXY_DEF(
    ObjectService,
    object       ,
    ((byte*  )(create )((int64  , size  ))((int64 , type_id      )))
    ((Object*)(clone  )((Object*, source))                         )
    ((bool   )(destroy)((int32  , count ))((byte**, p            )))

    ((byte*)(dynCast)((byte*, object))((int64 , target_type_id  )))

    ((int64)(getGCGenerationId       )                             )
    ((void )(getActiveObjectsInternal)((Vector<Object*>*, objects)))

    ((void)(           addToObjectRootSet)((int8*, object)))
    ((void)(      removeFromObjectRootSet)((int8*, object)))
    ((void)(     addToGlobalObjectRootSet)((int8*, object)))
    ((void)(removeFromGlobalObjectRootSet)((int8*, object)))
)

log4cxx::LoggerPtr ObjectService::mLogger(log4cxx::Logger::getLogger("ObjectServiceMT"));

//const unsigned int GARBAGE_TRIGGER_INTERVAL = 100;
const unsigned int GARBAGE_TRIGGER_INTERVAL = 2000;

//////////////////////////////////////////////////////////////////////////
ObjectService::ObjectService(ProcessorRT* processor)
    : mProcessor(processor)
    , tracker(new detail::Tracker(nullptr))
    , stale_trackers()
    , tracker_proctor()
    , gc(*mLogger)
{
    mLogger->setLevel(log4cxx::Level::getError());
//    mLogger->setLevel(log4cxx::Level::getAll());
}

ObjectService::~ObjectService()
{
    SAFE_DELETE(tracker);
}

//////////////////////////////////////////////////////////////////////////
bool ObjectService::isCompleted()
{
    // TODO: Decide the condition. Now, we always return false to make garbage collection works.
    return true;
}

bool ObjectService::isInvocationPending()
{
    return false;
}

//////////////////////////////////////////////////////////////////////////
void ObjectService::initialize()
{
}

void ObjectService::finalize()
{
}

void ObjectService::beforeInitializeOnKernel(shared_ptr<KernelBase> kernel)
{
    gObjectService = this;
    gc.initialize(kernel);
    gc.trigger(tracker);
}

void ObjectService::afterInitializeOnKernel(shared_ptr<KernelBase> kernel)
{
}

void ObjectService::beforeFinalizeOnKernel(shared_ptr<KernelBase> kernel)
{
}

void ObjectService::afterFinalizeOnKernel(shared_ptr<KernelBase> kernel)
{
    gc.finalize();
    gObjectService = nullptr;
}

byte* ObjectService::create(int64 size, int64 type_id)
{
    byte* p = nullptr;

    while (p == nullptr)
    {
        p = tracker->allocate(size, MUTATOR_COLOR(gc.getEpoch()), type_id);

        if (p == nullptr)
        {
            const auto& try_locker = boost::make_unique_lock(tracker_proctor, boost::try_to_lock);

            // only one thread can create the replacement pool, the others just yield and busy wait
            if (try_locker.owns_lock())
            {
                detail::Tracker* extended_tracker = new detail::Tracker(tracker);

                // allocate from it
                p = extended_tracker->allocate(size, MUTATOR_COLOR(gc.getEpoch()), type_id);

                // replace the internal tracker pointer so that all follow-up allocation is using the new tracker
                stale_trackers.push(tracker);
                tracker = extended_tracker;
            }
            else
            {
                boost::this_thread::yield();
            }
        }
    }

    //std::cout << "[object] create: " << (void*)p << std::endl;
    return p;
}

TypeId getTypeId(void* object)
{
    auto* header = GET_RAW_BUFFER(object)->tracker_ptr;
    return header->type_id;
}

auto ObjectService::clone(Object* source) -> Object*
{
    auto  type_id       = getTypeId(source);
    auto& object_layout = getObjectLayout(source);

    auto* cloned = create(object_layout.object_size, type_id);
    std::memcpy(cloned, source, object_layout.object_size);

    return reinterpret_cast<Object*>(cloned);
}

bool ObjectService::destroy(int32 count, byte** p)
{
    BOOST_ASSERT(count > 0 && "no object to be destroyed");

    const boost::shared_lock<boost::shared_mutex> shared_locker(tracker_proctor);

    return std::all_of(
        p, p + count,
        std::bind(
            &detail::Tracker::deallocate,
            tracker,
            std::placeholders::_1
        )
    );
}

byte* ObjectService::dynCast(byte* source, int64 target_type_id)
{
    auto* source_decl = tryGetClassDecl(source);
    if (source_decl == nullptr)
        return nullptr;

    auto* target_decl = tryGetClassDecl(target_type_id);
    if (target_decl == nullptr)
        return nullptr;

    auto& source_object_layout = getObjectLayout(source);

    const auto& class_offset = source_object_layout.class_offset;
    const auto& offset_pos   = class_offset.find(target_decl);

    if (offset_pos == class_offset.end())
        return nullptr;

    return source + offset_pos->second.second;
}



//////////////////////////////////////////////////////////////////////////

int64 ObjectService::getGCGenerationId()
{
    return gc.getEpoch();
}

processor::mt::Kernel* ObjectService::getKernel() const
{
    const auto&    kernel = mProcessor->getKernel();
          auto* mt_kernel = static_cast<processor::mt::Kernel*>(kernel.get());

    return mt_kernel;
}

//////////////////////////////////////////////////////////////////////////
void ObjectService::handleEvent(events::type event)
{
    switch(event)
    {
    case events::after_invocation_dispatch:
    case events::idletime_heartbeat:
        gc.trigger(tracker);
        break;
    default:
        break;
    }
}

bool ObjectService::isGCInitialized()
{
    return gc.isInitialized();
}

void ObjectService::getActiveObjectsInternal(Vector<Object*>* objects)
{
    for (TrackerHeader* header : boost::make_iterator_range(begin(*tracker), end(*tracker)))
    {
        if (!header->is_valid)
            continue;

        objects->pushBack(reinterpret_cast<Object*>(header->object_ptr));
    }
}

detail::GCSweeper::RecycleContainer& ObjectService::getRecycleObjects()
{
    return gc.getRecycleObjects();
}

void ObjectService::resetRecycleObjects()
{
    gc.resetRecycleObjects();
}

auto ObjectService::getObjectLayout(TypeId type_id) const -> ObjectLayout&
{
    auto& class_decl = getClassDecl(type_id);

    auto* object_layout = ObjectLayout::get(&class_decl);
    BOOST_ASSERT(object_layout != nullptr && "can not find object layout for class");

    return *object_layout;
}

auto ObjectService::getObjectLayout(void* object) const -> ObjectLayout&
{
    return getObjectLayout(getTypeId(object));
}

auto ObjectService::getClassDecl(TypeId type_id) const -> const ClassDecl&
{
    auto* class_decl = tryGetClassDecl(type_id);
    BOOST_ASSERT(class_decl != nullptr && "can not find related class for type id");

    return *class_decl;
}

auto ObjectService::tryGetClassDecl(TypeId type_id) const -> const ClassDecl*
{
    auto* kernel = getKernel();
    BOOST_ASSERT(kernel != nullptr && "can not find kernal");

    return kernel->queryClassDecl(type_id);
}

auto ObjectService::tryGetClassDecl(void* object) const -> const ClassDecl*
{
    return tryGetClassDecl(getTypeId(object));
}

using detail::ObjectTrackTrait;
using detail::GlobalObjectTrackTrait;

void ObjectService::     addToObjectRootSet(int8* object) { return gc.     addToObjectRootSet(object); }
void ObjectService::removeFromObjectRootSet(int8* object) { return gc.removeFromObjectRootSet(object); }
void ObjectService::     addToGlobalObjectRootSet(int8* object) { return gc.     addToGlobalRootSet(object); }
void ObjectService::removeFromGlobalObjectRootSet(int8* object) { return gc.removeFromGlobalRootSet(object); }

} } } }
