/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/service/object/detail/Tracker.h"

#include <cstdlib>
#include <cassert>

#include "core/IntTypes.h"
#include "core/Common.h" // SAFE_DELETE_ARRAY



namespace zillians { namespace framework { namespace service { namespace mt { namespace detail {

namespace {

constexpr uint64 sInitialObjectCount = 1024;

}

Tracker::Tracker(Tracker* parent/* = nullptr*/)
{
    if (!parent)
    {
        total_count = sInitialObjectCount;
        headers = new TrackerHeader*[total_count];
        allocator = new boost::lockfree::queue<TrackerHeader*>{ total_count };

        for (uint64 i = 0ULL; i < total_count; ++i)
        {
            TrackerHeader* header = new TrackerHeader;
            headers[i] = header;
            const bool pushed = allocator->push(header);
            assert(pushed);
        }
    }
    else
    {
        // double-size the previous tracker
        total_count = parent->total_count * 2;
        headers = new TrackerHeader*[total_count];

        // steal the allocator from the parent
        allocator = parent->allocator;

        // copy the existing ones
        memcpy(headers, parent->headers, sizeof(TrackerHeader*) * parent->total_count);

        // add new tracker headers
        for(uint64 i = parent->total_count; i < total_count; ++i)
        {
            TrackerHeader* header = new TrackerHeader;
            headers[i] = header;
            const bool pushed = allocator->push(header);
            assert(pushed);
        }
    }
}

Tracker::~Tracker()
{
    total_count = 0;
    SAFE_DELETE_ARRAY(headers);
    allocator = nullptr;
}

byte* Tracker::allocate(int64 size, uint64 epoch, int64 type_id)
{
    // try to find a free tracker slot
    TrackerHeader* tracker_header = 0;
    if(!allocator->pop(tracker_header))
        return NULL;

    BOOST_ASSERT(tracker_header != NULL);

    // try to allocate memory
    byte* p = (byte*)malloc(size + sizeof(ObjectHeader));
    if(!p) {
        // OUT-OF-MEMORY
        const bool pushed = allocator->push(tracker_header);
        assert(pushed);
        return NULL;
    }

    // save the tracker id into the object header
    ((ObjectHeader*)p)->magic_number = gMagicNumber;
    ((ObjectHeader*)p)->tracker_ptr = tracker_header;

    // offset the base pointer by object header size
    p += sizeof(ObjectHeader);

    tracker_header->epoch = epoch;
    tracker_header->type_id = type_id;
    tracker_header->object_ptr = p;
    tracker_header->is_valid = true;

    return p;
}

bool Tracker::deallocate(byte* p)
{
    TrackerHeader* tracker_header = GET_RAW_BUFFER(p)->tracker_ptr;

    // integrity check
    if(tracker_header->object_ptr != p)
    {
        BOOST_ASSERT(false && "Integrity check failed!");
        return false;
    }

    // clear to zero, so that we don't have the magic number scatter in the heaps
    GET_RAW_BUFFER(p)->magic_number = 0;

    // reset the header
    tracker_header->reset();

    // free up the allocated object buffer
    free(p - sizeof(ObjectHeader));

    const bool pushed = allocator->push(tracker_header);
    assert(pushed);

    return true;
}

TrackerHeader** Tracker::begin() noexcept { return headers; }
TrackerHeader** Tracker::end  () noexcept { return headers + total_count; }

} } } } }
