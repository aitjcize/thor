/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>
#include <iterator>
#include <map>
#include <string>
#include <vector>

#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <boost/range/iterator_range.hpp>

#include "utility/MemoryUtil.h"

#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/stage/generator/context/SynthesizedObjectLayoutContext.h"

#include "framework/service/object/detail/GarbageCollector.h"

#include "thor/lang/GarbageCollectable.h"
#include "thor/lang/Language.h"

namespace zillians { namespace framework { namespace service { namespace mt { namespace detail {

namespace {

const unsigned int GARBAGE_TRIGGER_INTERVAL = 2000;

bool isUnmanagedPtr(const RecordType& type)
{
    const ClassDecl*const decl = type.getAsClassDecl();

    BOOST_ASSERT(decl && "RecordType refers to non-ClassDecl");

    if (decl->name->getTemplateId() == nullptr)
        return false;

    if (decl->name->getSimpleId()->name != L"ptr_")
        return false;

    const auto& packages = ASTNodeHelper::getParentScopePackages(decl);

    if (packages.size() != 2)
        return false;

    if (packages.front()->id->name != L"thor"     ) return false;
    if (packages.back ()->id->name != L"unmanaged") return false;

    return true;
}

byte* castToThorLangObject(void* ptr)
{
    BOOST_ASSERT(ptr && "null pointer exception");

    const auto**const vptr   = reinterpret_cast<const int64**>(ptr);
    const auto*const  vtable = *vptr;
    const auto        offset = vtable[-2];

    return reinterpret_cast<byte*>(ptr) + offset;
}

}

log4cxx::LoggerPtr  GCMarker::gc_logger(log4cxx::Logger::getLogger("GCMarker"));
log4cxx::LoggerPtr GCSweeper::gc_logger(log4cxx::Logger::getLogger("GCSweeper"));

/*
 * GarbageCollectorBase
 */
GCBase::GCBase(GCContext& context) :
        is_working   (false),
        current_epoch(0),
        gc_context   (context),
        work_thread  (new boost::thread(boost::bind(&GCBase::asyncRun, this)))
#ifdef ENABLE_GC_STATS
        ,
        total_time (0.0),
        total_count(0)
#endif
{
}

GCBase::~GCBase()
{
}

void GCBase::run(EPOCH_TYPE epoch)
{
    BOOST_ASSERT(work_thread);

    current_epoch = epoch;
    is_working    = true;

    cond.signal(0);
}

void GCBase::asyncRun()
{
    while (true)
    {
        uint32 exit;
        cond.wait(exit);

        if (exit) break;

        runImpl();
        is_working = false;
    }
}

void GCBase::stop()
{
    cond.signal(1);
    if(work_thread && work_thread->joinable())
        work_thread->join();

    work_thread.reset();
}

#ifdef ENABLE_GC_STATS
void GCBase::dumpStats(const std::string& title)
{
    std::cerr << title << "\n"
              << "*************************\n" 
              << "   total time: " << total_time  << " ms\n"
              << "  total count: " << total_count << "\n"
              << " average time: " << total_time / total_count << " ms\n"
              << "*************************" << std::endl;
}
#endif

void GCBase::setTracker(Tracker* tracker)
{
    gc_context.tracker = tracker;
}

/*
 * GCMarker
 */
GCMarker::GCMarker(GCContext& context) : GCBase(context)
{
    gc_logger->setLevel(log4cxx::Level::getError());
//    gc_logger->setLevel(log4cxx::Level::getAll());
}

GCMarker::~GCMarker()
{
}

void GCMarker::runImpl()
{
    LOG4CXX_DEBUG(gc_logger, "[Marker] " << __FUNCTION__);
    tbb::tick_count start = tbb::tick_count::now();

    // clear the marked set first
    marked_objects.clear();

    // get starting roots
    std::vector<ObjectTrackTrait::ElemType> roots;

    // Start to traverse
    LOG4CXX_DEBUG(gc_logger, "[Marker] root size = " << roots.size());
    marking(roots);

    LOG4CXX_DEBUG(gc_logger, "[Marker] AsyncMark costs: " << (tbb::tick_count::now()-start).seconds() * 1000.0 << " ms");
}

std::vector<ObjectTrackTrait::ElemType> GCMarker::getSharedRoots()
{
    LOG4CXX_DEBUG(gc_logger, "[Marker] >>>>>>>>>>> object root set count:  " << gc_context.object_root_set->size());
    LOG4CXX_DEBUG(gc_logger, "[Marker] >>>>>>>>>>> global root set count:  " << gc_context.global_root_set->size());

    auto object_root = boost::adaptors::keys(*gc_context.object_root_set);
    std::vector<ObjectTrackTrait::ElemType> roots = {object_root.begin(), object_root.end()};

    for(auto entry : *gc_context.global_root_set)
    {
        auto object = *entry.first;

        if(object)
        {
            object = castToThorLangObject(object);

            TrackerHeader* tracker_header = GET_RAW_BUFFER(object)->tracker_ptr;
                     auto  magic_number   = GET_RAW_BUFFER(object)->magic_number;

            BOOST_ASSERT(magic_number == gMagicNumber && "Impossible!! Should match to magic number!!");

            roots.push_back(tracker_header);
        }
    }

    return std::move(roots);
}

void GCMarker::reMark()
{
    LOG4CXX_DEBUG(gc_logger, "[Marker] " << __FUNCTION__);
    tbb::tick_count start = tbb::tick_count::now();

    // prepare previous marked objects and current roots as new root set
    std::vector<ObjectTrackTrait::ElemType> roots = getSharedRoots();

    boost::copy(marked_objects, std::back_inserter(roots));

    LOG4CXX_DEBUG(gc_logger, "[Marker] *reMark* Root Size : " << roots.size());

    // reset the marked
    // ps. Actually, the time complexity of runPostMark and runImpl should be the same. I start to wonder if
    // we need an extra thread to perform marking.
    // A refinement is using many threads to perform recursive marking in marking() function.
    marked_objects.clear();

    // Go sync marking
    marking(roots);

    LOG4CXX_DEBUG(gc_logger, "[Marker] *reMark* Remarked Object Count: " << marked_objects.size());
    LOG4CXX_DEBUG(gc_logger, "[Marker] *reMark* costs: " << (tbb::tick_count::now()-start).seconds() * 1000.0 << " ms");
}

void GCMarker::marking(std::vector<ObjectTrackTrait::ElemType>& roots)
{
    tbb::tick_count start = tbb::tick_count::now();

#ifdef ENABLE_GC_STATS
    bool need_stats = roots.size() > 0;
#endif

    for(auto object : roots)
    {
        markObject(object);
    }

#ifdef ENABLE_GC_STATS
    if(need_stats)
    {
        double dt = (tbb::tick_count::now() - start).seconds() * 1000.0;
        total_time  += dt;
        total_count += marked_objects.size();
    }
#endif
}

void GCMarker::markObject(ObjectTrackTrait::ElemType& object)
{
    using namespace zillians::language::tree;
    using namespace zillians::language::stage;

    // no need to trace marked object
    if (marked_objects.count(object) > 0)
        return;

    // mark this object
    marked_objects.insert( object );

    const ClassDecl* class_decl = gc_context.kernel->queryClassDecl(object->type_id);
    if (class_decl == nullptr)
    {
        BOOST_ASSERT(false && "Cannot find the specific type id");
        return;
    }

    // raise epoch
    if (MARKER_COLOR(current_epoch) == object->epoch)
    {
        // TODO: Add perfect debug information
        LOG4CXX_DEBUG(gc_logger, L"[Marker] *Raise* object:  " << (void*)object->object_ptr << L" (" << class_decl->name->toString() << L")" << L" epoch advanced: new " << MUTATOR_COLOR(current_epoch) );
        object->epoch = MUTATOR_COLOR(current_epoch);
    }

    // trace each member
    traceObjectMembers(object, *class_decl);

    // see if its a container, if so, we need to traverse each object in the container
    const ClassDecl* garbage_collectable_interface = NULL;
    if (isContainer(*class_decl, garbage_collectable_interface))
    {
        traceContainedObjects(object, *class_decl, *garbage_collectable_interface);
    }
}

void GCMarker::traceObjectMembers(ObjectTrackTrait::ElemType& object, const ClassDecl& node)
{
    using namespace zillians::language::tree;
    using namespace zillians::language::stage;

    LOG4CXX_DEBUG(gc_logger, L"[Marker] " << __FUNCTION__ << L": " << node.name->toString());

    auto object_layout = GET_SYNTHESIZED_OBJECTLAYOUT(&node);
    BOOST_ASSERT(object_layout && "No object layout found");

    for(auto& attribute : object_layout->member_attributes)
    {
        const VariableDecl*const member_decl        = attribute.first;
        const RecordType*const   member_record_type = member_decl->getCanonicalType()->getAsRecordType();

        if (member_record_type == nullptr)
            continue;

        if (isUnmanagedPtr(*member_record_type))
            continue;

        const auto& attributes = attribute.second;

        LOG4CXX_DEBUG(gc_logger, L"[Marker] member: " << member_decl->name->toString() << L" type: " << member_record_type->toString());
        LOG4CXX_DEBUG(gc_logger, "[Marker] adjust pointer with offset: " << attributes.get<1>() << " the size of member: " << attributes.get<2>());
        BOOST_ASSERT(attributes.get<2>() && "member size must not be zero!!!");
        byte* member_object = object->object_ptr + attributes.get<1>();
        LOG4CXX_DEBUG(gc_logger, "[Marker] the address of the member: " << (void*)member_object);

        // Retrieve the value in the specific address
        byte* object_ptr = 0;
        memcpy(&object_ptr, member_object, attributes.get<2>());
        LOG4CXX_DEBUG(gc_logger, "[Marker] member object pointer: " << (void*)object_ptr);

        if(object_ptr)
        {
            object_ptr = castToThorLangObject(object_ptr);

            auto header = GET_RAW_BUFFER(object_ptr)->tracker_ptr;
            markObject(header);
        }
    }
}

void GCMarker::traceContainedObjects(ObjectTrackTrait::ElemType& object, const ClassDecl& node, const ClassDecl& garbage_collectable_interface)
{
    using namespace thor::lang;
    using namespace zillians::language::tree;
    using namespace zillians::language::stage;

    LOG4CXX_DEBUG(gc_logger, L"[Marker]" << __FUNCTION__ << L": " << node.name->toString());

    // Although object_ptr points to Object type, it is the same as derived class. We don't need to
    // adjust the address. All we need to do is to adjust the address of object_ptr from derived class to GarbageCollectable interface.
    auto object_layout = GET_SYNTHESIZED_OBJECTLAYOUT(&node);
    BOOST_ASSERT(object_layout && "No object layout found");
    BOOST_ASSERT(object_layout->class_offset.count(&garbage_collectable_interface) && "Cannot find garbage collectable interface for the container");

    auto offset_in_bytes = object_layout->class_offset[&garbage_collectable_interface].second;
    byte* object_ptr = object->object_ptr + offset_in_bytes;

    GarbageCollectable* gc_helper = (GarbageCollectable*)object_ptr;

    // get the contained types
    CollectableObject contained_objects;
    gc_helper->getContainedObjects(&contained_objects);

    LOG4CXX_DEBUG(gc_logger, "[Marker] " << __FUNCTION__ << ": found " << contained_objects.objects.size() << " objects inside");

    for(auto* object : contained_objects.objects)
    {
        byte* contained_object_ptr = static_cast<byte*>(object);
        if (contained_object_ptr == NULL) continue;

        TrackerHeader* header = GET_RAW_BUFFER(contained_object_ptr)->tracker_ptr;
        auto magic_number = GET_RAW_BUFFER(contained_object_ptr)->magic_number;

        if (magic_number != gMagicNumber) continue;

        BOOST_ASSERT(header->object_ptr == contained_object_ptr && "Integrity check failed");
        markObject(header);
    }
}

bool GCMarker::isContainer(const ClassDecl& node, const ClassDecl*& garbage_collectable_interface)
{
    for(auto* impl : node.implements)
    {
        Type* resolved = impl->getCanonicalType();

        ClassDecl* class_decl = resolved->getAsClassDecl();
        if (class_decl->name->toString() == L"GarbageCollectable")
        {
            garbage_collectable_interface = class_decl;
            return true;
        }
    }

    // continue to traverse
    if (node.base)
    {
        Type* resolved = node.base->getCanonicalType();

        if (isContainer(*resolved->getAsClassDecl(), garbage_collectable_interface))
            return true;
    }

    for(auto* impl : node.implements)
    {
        Type* resolved = impl->getCanonicalType();

        if (isContainer(*resolved->getAsClassDecl(), garbage_collectable_interface))
            return true;
    }

    return false;
}

/*
 * GCSweeper
 */
GCSweeper::GCSweeper(GCContext& context) : GCBase(context)
{
    gc_logger->setLevel(log4cxx::Level::getError());
//    gc_logger->setLevel(log4cxx::Level::getAll());
}

GCSweeper::~GCSweeper()
{
}

void GCSweeper::runImpl()
{
    tbb::tick_count start = tbb::tick_count::now();

    sweepObject();

    LOG4CXX_DEBUG(gc_logger, "[Sweeper] AsyncSweep costs: " << (tbb::tick_count::now()-start).seconds() * 1000.0 << " ms");

#ifdef ENABLE_GC_STATS
    if(collected_objs->size() > 0)
    {
        double dt = (tbb::tick_count::now() - start).seconds() * 1000.0;

        total_time  += dt;
        total_count += collected_objs->size();
    }
#endif
}

void GCSweeper::sweepObject()
{
    if (!gc_context.tracker) return;

    for (TrackerHeader* header : boost::make_iterator_range(begin(*gc_context.tracker), end(*gc_context.tracker)))
    {
        if (!header->is_valid)
            continue;
        if (header->epoch != SWEEPER_COLOR(current_epoch))
            continue;

        LOG4CXX_DEBUG(gc_logger, L"[Sweeper] Remove garbage: " << (void*)header->object_ptr << " (" <<
                [&]() {
                    const ClassDecl* class_decl = gc_context.kernel->queryClassDecl(header->type_id);

                    return class_decl->name->toString();
                }() << ")");

        // Before put the object into the to-recycled list, we set it to invalid to prevent from
        // sweep again
        header->is_valid = false;
        thor::lang::Object* obj = (thor::lang::Object*)header->object_ptr;

        collected_objs.push_back(obj);
    }

    LOG4CXX_DEBUG(gc_logger, "[Sweeper] =================> Recycle object count: " << collected_objs.size());
}

auto GCSweeper::getRecycleObjects() -> RecycleContainer&
{
    return garbage_objs;
}

void GCSweeper::resetRecycleObjects()
{
    garbage_objs.clear();
}

void GCSweeper::presentSweepResult()
{
    garbage_objs = std::move(collected_objs);
    collected_objs.clear();
}

GarbageCollector::GarbageCollector(log4cxx::Logger& logger)
    : current_epoch(2)
    , context(object_root_set, global_root_set)
    , marker (nullptr)
    , sweeper(nullptr)
    , logger (&logger)
{
}

GarbageCollector::~GarbageCollector()
{
    BOOST_ASSERT(marker  == nullptr && "not yet finalize");
    BOOST_ASSERT(sweeper == nullptr && "not yet finalize");
}

bool GarbageCollector::isInitialized() const noexcept
{
    return marker != nullptr && sweeper != nullptr;
}

void GarbageCollector::initialize(const std::shared_ptr<KernelBase>& kernel)
{
    BOOST_ASSERT(marker  == nullptr && "already initialized");
    BOOST_ASSERT(sweeper == nullptr && "already initialized");

    auto new_marker  = make_unique<GCMarker >(context);
    auto new_sweeper = make_unique<GCSweeper>(context);

    context.kernel = kernel;
    marker         = std::move(new_marker );
    sweeper        = std::move(new_sweeper);
}

void GarbageCollector::finalize()
{
    BOOST_ASSERT(marker  != nullptr && "not yet initialized");
    BOOST_ASSERT(sweeper != nullptr && "not yet initialized");

    marker ->stop();
    sweeper->stop();

    context.kernel = nullptr;
    marker         = nullptr;
    sweeper        = nullptr;
}

EPOCH_TYPE GarbageCollector::getEpoch() const noexcept
{
    return current_epoch;
}

EPOCH_TYPE GarbageCollector::trigger(Tracker* tracker)
{
    const tbb::tick_count now        = tbb::tick_count::now();
    const bool            should_run =
        marker  != nullptr &&
        sweeper != nullptr &&
        (now - last_time_gc).seconds() * 1000 > GARBAGE_TRIGGER_INTERVAL &&
        !isWorking()
    ;

    if (!should_run)
        return current_epoch;

    marker ->reMark();
    sweeper->presentSweepResult();

    ++current_epoch;

    LOG4CXX_DEBUG(logger, "mutator: " << MUTATOR_COLOR(current_epoch));
    LOG4CXX_DEBUG(logger, "marker : " << MARKER_COLOR(current_epoch));
    LOG4CXX_DEBUG(logger, "sweeper: " << SWEEPER_COLOR(current_epoch));

    marker ->setTracker(tracker);
    sweeper->setTracker(tracker);

    marker ->run(current_epoch);
    sweeper->run(current_epoch);

    return current_epoch;
}

template<>
typename ObjectTrackTrait::ContainerType* GarbageCollector::getRootSet<ObjectTrackTrait>()
{
    return &object_root_set;
}

template<>
typename GlobalObjectTrackTrait::ContainerType* GarbageCollector::getRootSet<GlobalObjectTrackTrait>()
{
    return &global_root_set;
}

bool GarbageCollector::isWorking() const
{
    BOOST_ASSERT(marker  && "not yet initialized");
    BOOST_ASSERT(sweeper && "not yet initialized");

    return marker->isWorking() || sweeper->isWorking();
}

GCSweeper::RecycleContainer& GarbageCollector::getRecycleObjects()
{
    BOOST_ASSERT(sweeper && "not yet initialized");

    return sweeper->getRecycleObjects();
}

void GarbageCollector::resetRecycleObjects()
{
    BOOST_ASSERT(sweeper && "not yet initialized");

    sweeper->resetRecycleObjects();
}

void GarbageCollector::addToObjectRootSet(int8* ptr)
{
    NOT_NULL(ptr);

    auto*const ptr_obj        = castToThorLangObject(ptr);
    auto*const ptr_obj_header = GET_RAW_BUFFER(ptr_obj);

    NOT_NULL(ptr_obj_header             );
    NOT_NULL(ptr_obj_header->tracker_ptr);

    addToRootSetImpl<ObjectTrackTrait>(ptr_obj_header->tracker_ptr);
}

void GarbageCollector::removeFromObjectRootSet(int8* ptr)
{
    NOT_NULL(ptr);

    auto*const ptr_obj        = castToThorLangObject(ptr);
    auto*const ptr_obj_header = GET_RAW_BUFFER(ptr_obj);

    NOT_NULL(ptr_obj_header             );
    NOT_NULL(ptr_obj_header->tracker_ptr);

    removeFromRootSetImpl<ObjectTrackTrait>(ptr_obj_header->tracker_ptr);
}

void GarbageCollector::addToGlobalRootSet(int8* ptr)
{
    NOT_NULL(ptr);

    addToRootSetImpl<GlobalObjectTrackTrait>(reinterpret_cast<GlobalObjectTrackTrait::ElemType>(ptr));
}

void GarbageCollector::removeFromGlobalRootSet(int8* ptr)
{
    NOT_NULL(ptr);

    removeFromRootSetImpl<GlobalObjectTrackTrait>(reinterpret_cast<GlobalObjectTrackTrait::ElemType>(ptr));
}

template<typename Trait>
void GarbageCollector::addToRootSetImpl(const typename Trait::ElemType object)
{
    auto*const root_set = getRootSet<Trait>();

    NOT_NULL(root_set);

    typename Trait::ContainerType::accessor accessor;

    if (!root_set->insert(accessor, {object, 1u}))
        ++accessor->second;
}

template<typename Trait>
void GarbageCollector::removeFromRootSetImpl(const typename Trait::ElemType object)
{
    auto*const root_set = getRootSet<Trait>();

    NOT_NULL(root_set);

    typename Trait::ContainerType::accessor accessor;

    const auto& is_found = root_set->find(accessor, object);

    BOOST_ASSERT(is_found && "removing not presented object, there may be some non-paired add-to-root-set/remove-from-root-set call");

    if (--accessor->second == 0)
        root_set->erase(accessor);
}

} } } } }
