/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#undef _GLIBCXX_ATOMIC_BUILTINS
#undef _GLIBCXX_USE_INT128

#include <cuda_runtime.h>
#include <stdio.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/adjacent_difference.h>
#include <thrust/scan.h>
#include <thrust/for_each.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/copy.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/binary_search.h>
#include "framework/service/Common.h"
#include "framework/service/object/ObjectServiceBuffer.h"

using namespace std;

#define CEILING(x,y) (((x) + (y) - 1) / (y))

#define HEAP_COMPACTION_THREADS_PER_BLOCK	256
#define HEAP_COMPACTION_WARP_SIZE			32
#define HEAP_COMPACTION_CHUNK_SIZE			128
#define HEAP_COMPACTION_MEMMOVE_UNIT_SIZE	4

using namespace zillians::framework::service::cuda;

namespace zillians { namespace framework { namespace service { namespace cuda { namespace detail {

const int64_t cMagicNumber = 0xFFEEFFAABBEE9933;

__global__ void __compactGlobalHeapImpl_Move(
		uint32_t heap_bucket_size,
		uint32_t total_chunk_count,
		uint32_t* heap_bucket,
		uint32_t* heap_bucket_offset,
		uint32_t* heap_bucket_scanned,
		uint32_t* compaction_work_segments,
		uint32_t* compaction_work_indices,
		char* heap,
		char* heap_next
		)
{
	//const uint32_t tid = blockDim.x * blockIdx.x + threadIdx.x;
	const uint32_t warp_size = 32;
	const uint32_t warp_id = threadIdx.x / warp_size;
	const uint32_t warps_per_block = blockDim.x / warp_size;
	const uint32_t thread_lane = threadIdx.x & (warp_size - 1);
	uint32_t local_offset = thread_lane * HEAP_COMPACTION_MEMMOVE_UNIT_SIZE;
	char* local_heap = heap + local_offset;
	char* local_heap_next = heap_next + local_offset;

	for(int i=0;i<blockDim.x;i+=warps_per_block)
	{
		uint32_t idx = blockDim.x * blockIdx.x + i + warp_id;
		if(idx >= total_chunk_count) continue;

		uint32_t segment = compaction_work_segments[idx];
		uint32_t work_index = compaction_work_indices[idx];
		uint32_t bucket_size = heap_bucket[segment];
		uint32_t bucket_offset_dst = heap_bucket_scanned[segment] - bucket_size + work_index;
		uint32_t bucket_offset_src = heap_bucket_offset[segment] + work_index;

		char* src_ptr = local_heap + bucket_offset_src * HEAP_COMPACTION_CHUNK_SIZE;
		char* dst_ptr = local_heap_next + bucket_offset_dst * HEAP_COMPACTION_CHUNK_SIZE;

		uint32_t t = *((uint32_t*)src_ptr);
		*((uint32_t*)dst_ptr) = t;

//		if(thread_lane == 0 || thread_lane == 1)
//			printf("#%d -> %d -> move %d (%p)-> %d (%p) [idx=%d, segment=%d, work_index=%d, bucket_size=%d, offset=%d, t=%u]\n", i, tid, bucket_offset_src, src_ptr, bucket_offset_dst, dst_ptr, idx, segment, work_index, bucket_size, local_offset, t);

	}
}

__global__ void __compactGlobalHeapImpl_UpdateHeader(
		uint32_t heap_bucket_size,
		uint32_t* heap_bucket,
		uint32_t* heap_bucket_offset,
		uint32_t* heap_bucket_scanned,
		char* heap,
		char* heap_next)
{
	const uint32_t tid = blockDim.x * blockIdx.x + threadIdx.x;
	if(tid >= heap_bucket_size) return;

	uint32_t bucket_size = heap_bucket[tid];
	if(bucket_size > 0)
	{
		uint32_t chunk_offset = heap_bucket_offset[tid]; // chunk offset
		uint32_t scan_offset = heap_bucket_scanned[tid] - bucket_size;
		char* ptr = heap + chunk_offset * HEAP_COMPACTION_CHUNK_SIZE;
		if(((ObjectHeader*)ptr)->magic_number != cMagicNumber)
		{
//			printf("%d: update corrupted header, ptr = %p, object header = {%lld, %p}\n", tid, ptr, ((ObjectHeader*)ptr)->magic_number, ((ObjectHeader*)ptr)->tracker_ptr);
		}
		else
		{
			TrackerHeader* header = ((ObjectHeader*)ptr)->tracker_ptr;
			char* new_ptr = heap_next + scan_offset * HEAP_COMPACTION_CHUNK_SIZE;
			//printf("%d: update header ptr from %p -> %p\n", tid, ptr, new_ptr);
			header->ptr = new_ptr;
		}
	}
}

struct is_valid_bucket
{
	__host__ __device__
	bool operator()(const uint32_t x)
	{
	  return x > 0;
	}
};

bool __compactGlobalHeap(
		char* heap_ptr,
		char* heap_next_ptr,
		uint32_t* heap_bucket_ptr,
		uint32_t* heap_bucket_next_ptr,
		uint32_t* heap_bucket_offset_ptr,
		uint32_t* heap_bucket_offset_next_ptr,
		uint32_t* t_heap_bucket_scanned_ptr,
		uint32_t* t_compaction_work_segments_ptr,
		uint32_t* t_compaction_work_indices_ptr,
		unsigned long long int& heap_next_offset,
		bool& is_fully_compacted)
{
	thrust::device_ptr<char> heap = thrust::device_pointer_cast(heap_ptr);
	thrust::device_ptr<char> heap_next = thrust::device_pointer_cast(heap_next_ptr);
	thrust::device_ptr<uint32_t> heap_bucket = thrust::device_pointer_cast(heap_bucket_ptr);
	thrust::device_ptr<uint32_t> heap_bucket_next = thrust::device_pointer_cast(heap_bucket_next_ptr);
	thrust::device_ptr<uint32_t> heap_bucket_offset = thrust::device_pointer_cast(heap_bucket_offset_ptr);
	thrust::device_ptr<uint32_t> heap_bucket_offset_next = thrust::device_pointer_cast(heap_bucket_offset_next_ptr);
	thrust::device_ptr<uint32_t> t_heap_bucket_scanned = thrust::device_pointer_cast(t_heap_bucket_scanned_ptr);
	thrust::device_ptr<uint32_t> t_compaction_work_segments = thrust::device_pointer_cast(t_compaction_work_segments_ptr);
	thrust::device_ptr<uint32_t> t_compaction_work_indices = thrust::device_pointer_cast(t_compaction_work_indices);

	uint32_t total_chunk_size = (heap_next_offset << 32) >> 32;
	uint32_t bucket_size = (uint32_t)(heap_next_offset >> 32);
	if(bucket_size == 0)
	{
		return true;
	}

//	for(int i=0;i<bucket_size;++i)
//	{
//		uint32_t t = heap_bucket[i];
//		printf("heap_bucket[%d] = %d\n", i, t);
//	}
//
//	for(int i=0;i<bucket_size;++i)
//	{
//		uint32_t t = heap_bucket_offset[i];
//		printf("heap_bucket_offset[%d] = %d\n", i, t);
//	}

	thrust::inclusive_scan(heap_bucket, heap_bucket + bucket_size, t_heap_bucket_scanned);

//	for(int i=0;i<bucket_size;++i)
//	{
//		uint32_t t = t_heap_bucket_scanned[i];
//		printf("heap_bucket_scanned[%d] = %d\n", i, t);
//	}

	uint32_t allocated_chunk_count = t_heap_bucket_scanned[bucket_size - 1];
//	printf("bucket_size = %u, allocated_chunk_size = %u, total_chunk_size = %u\n", bucket_size, allocated_chunk_count * 128, total_chunk_size);

	if(allocated_chunk_count * HEAP_COMPACTION_CHUNK_SIZE == total_chunk_size) // TODO the condition is wrong
	{
//		printf("fully compacted, return\n");
		is_fully_compacted = true;
		return true;
	}

	thrust::counting_iterator<int> counting_iter(0);
	thrust::upper_bound(t_heap_bucket_scanned, t_heap_bucket_scanned + bucket_size, counting_iter, counting_iter + allocated_chunk_count, t_compaction_work_segments);

	thrust::fill(t_compaction_work_indices, t_compaction_work_indices + allocated_chunk_count, 1);
	thrust::exclusive_scan_by_key(t_compaction_work_segments, t_compaction_work_segments + allocated_chunk_count, t_compaction_work_indices, t_compaction_work_indices);

//	for(int i=0;i<allocated_chunk_count;++i)
//	{
//		uint32_t t = t_compaction_work_segments[i];
//		printf("t_compaction_work_segments[%d] = %d\n", i, t);
//	}
//	for(int i=0;i<allocated_chunk_count;++i)
//	{
//		uint32_t t = t_compaction_work_indices[i];
//		printf("t_compaction_work_indices[%d] = %d\n", i, t);
//	}

	// compact heap
	if(true)
	{
		int threads = HEAP_COMPACTION_THREADS_PER_BLOCK;
		int blocks = ceiling<int>(allocated_chunk_count, threads);

		__compactGlobalHeapImpl_Move<<<blocks,threads>>>(
				bucket_size,
				allocated_chunk_count,
				thrust::raw_pointer_cast(heap_bucket),
				thrust::raw_pointer_cast(heap_bucket_offset),
				thrust::raw_pointer_cast(t_heap_bucket_scanned),
				thrust::raw_pointer_cast(t_compaction_work_segments),
				thrust::raw_pointer_cast(t_compaction_work_indices),
				thrust::raw_pointer_cast(heap),
				thrust::raw_pointer_cast(heap_next)
				);

		cudaError_t error = cudaDeviceSynchronize();
		if(error != cudaSuccess)
		{
			printf("failed to move memory: %s\n", cudaGetErrorString(error));
			return false;
		}
	}

	// compact heap_buckets (which stores the size of each bucket)
	typedef thrust::tuple<thrust::device_ptr<uint32_t>, thrust::device_ptr<uint32_t> > iterator_tuple_t;
	typedef thrust::zip_iterator<iterator_tuple_t> iterator_t;

	iterator_t last = thrust::copy_if(
			thrust::make_zip_iterator(thrust::make_tuple(heap_bucket, t_heap_bucket_scanned)), // input first
			thrust::make_zip_iterator(thrust::make_tuple(heap_bucket + bucket_size, t_heap_bucket_scanned + bucket_size)), // input last
			heap_bucket,
			thrust::make_zip_iterator(thrust::make_tuple(heap_bucket_next, heap_bucket_offset_next)), // output
			is_valid_bucket());

	uint32_t valid_bucket_size = thrust::get<0>(last.get_iterator_tuple()) - heap_bucket_next;

	// since the bucket chunk offsets are inclusive, we need to subtract by its actual size to get the starting offset
	thrust::transform(heap_bucket_offset_next, heap_bucket_offset_next + valid_bucket_size, heap_bucket_next, heap_bucket_offset_next, thrust::minus<uint32_t>());

//	for(int i=0;i<valid_bucket_size;++i)
//	{
//		uint32_t t = heap_bucket_offset_next[i];
//		printf("heap_bucket_offset_next[%d] = %d\n", i, t);
//	}

	if(true)
	{
		int threads = HEAP_COMPACTION_THREADS_PER_BLOCK;
		int blocks = ceiling<int>(bucket_size, threads);

		__compactGlobalHeapImpl_UpdateHeader<<<blocks,threads>>>(
				bucket_size,
				thrust::raw_pointer_cast(heap_bucket),
				thrust::raw_pointer_cast(heap_bucket_offset),
				thrust::raw_pointer_cast(t_heap_bucket_scanned),
				thrust::raw_pointer_cast(heap),
				thrust::raw_pointer_cast(heap_next)
				);

		cudaError_t error = cudaDeviceSynchronize();
		if(error != cudaSuccess)
		{
			printf("failed to update tracker header: %s\n", cudaGetErrorString(error));
			return false;
		}
	}

	heap_next_offset = valid_bucket_size;
	heap_next_offset <<= 32;
	heap_next_offset |= (allocated_chunk_count * HEAP_COMPACTION_CHUNK_SIZE);

	return true;
}

void __initializeBuffers(
		uint32_t* header_flags, std::size_t header_flags_size,
		uint32_t* b64_flags, std::size_t b64_flags_size,
		uint32_t* b128_flags, std::size_t b128_flags_size,
		uint32_t* heap_bucket, std::size_t heap_bucket_size)
{
	thrust::device_ptr<uint32_t> heap_flags_ptr = thrust::device_pointer_cast(header_flags);
	thrust::fill(heap_flags_ptr, heap_flags_ptr + header_flags_size, 0xFFFFFFFF);

	thrust::device_ptr<uint32_t> b64_flags_ptr = thrust::device_pointer_cast(b64_flags);
	thrust::fill(b64_flags_ptr, b64_flags_ptr + b64_flags_size, 0xFFFFFFFF);

	thrust::device_ptr<uint32_t> b128_flags_ptr = thrust::device_pointer_cast(b128_flags);
	thrust::fill(b128_flags_ptr, b128_flags_ptr + b128_flags_size, 0xFFFFFFFF);

	thrust::device_ptr<uint32_t> heap_bucket_ptr = thrust::device_pointer_cast(heap_bucket);
	thrust::fill(heap_bucket_ptr, heap_bucket_ptr + heap_bucket_size, 0);
}

} } } } }
