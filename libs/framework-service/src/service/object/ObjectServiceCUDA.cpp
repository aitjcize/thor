/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/service/object/ObjectServiceCUDA.h"
#include "framework/service/object/ObjectServiceBuffer.h"
#include "framework/ProcessorId.h"
#include "framework/processor/ProcessorCUDA.h"
#include "framework/processor/KernelCUDA.h"
#include "language/tree/ASTNodeHelper.h"
#include <dlfcn.h>
#include <thrust/device_ptr.h>
#include <thrust/fill.h>

using namespace zillians::framework::buffer;
using namespace zillians::framework::service::cuda;

namespace zillians { namespace framework { namespace service { namespace cuda {

namespace detail {

template<typename Assignable1, typename Assignable2>
inline void swap(Assignable1 &a, Assignable2 &b)
{
  Assignable1 temp = a;
  a = b;
  b = temp;
}

extern bool __compactGlobalHeap(
		char* heap_ptr,
		char* heap_next_ptr,
		uint32_t* heap_bucket_ptr,
		uint32_t* heap_bucket_next_ptr,
		uint32_t* heap_bucket_offset_ptr,
		uint32_t* heap_bucket_offset_next_ptr,
		uint32_t* t_heap_bucket_scanned_ptr,
		uint32_t* t_compaction_work_segments_ptr,
		uint32_t* t_compaction_work_indices_ptr,
		unsigned long long int& heap_next_offset,
		bool& is_fully_compacted);

extern void __initializeBuffers(
		uint32_t* header_flags, std::size_t header_flags_size,
		uint32_t* b64_flags, std::size_t b64_flags_size,
		uint32_t* b128_flags, std::size_t b128_flags_size,
		uint32_t* heap_bucket, std::size_t heap_bucket_size);

}

log4cxx::LoggerPtr ObjectService::mLogger(log4cxx::Logger::getLogger("ObjectServiceCUDA"));

//////////////////////////////////////////////////////////////////////////
ObjectService::ObjectService(ProcessorRT* processor) :
		mProcessor(processor),
		mKernelInitialized(false),
		mKernelApi(processor::cuda::GetKernelApi()),
		mConfigureGlobalHeapFunc(NULL)
{
	mDeviceId = ((processor::ProcessorCUDA*)mProcessor)->getDeviceId();
}

ObjectService::~ObjectService()
{
}

//////////////////////////////////////////////////////////////////////////
bool ObjectService::isCompleted()
{
	// TODO: Decide the condition. Now, we always return false to make garbage collection works.
	return true;
}

bool ObjectService::isInvocationPending()
{
	return false;
}

//////////////////////////////////////////////////////////////////////////
void ObjectService::initialize()
{
	using namespace zillians::framework::processor::cuda;

	buffer_location_t::type location = buffer_location_t::get_ptx_device_location(mDeviceId);

	mKernelApi.device.use(mDeviceId);

	mKernelApi.memory.device.allocateRaw((byte**)&mHeaderBlocks, sizeof(TrackerHeader) * cuda_object_svc_constants::max_object_count);
	mKernelApi.memory.device.allocateRaw((byte**)&mHeaderFlags, sizeof(uint32) * cuda_object_svc_constants::total_flags);

	mKernelApi.memory.device.allocateRaw((byte**)&mB64Blocks, sizeof(Block64) * cuda_object_svc_constants::max_object_count);
	mKernelApi.memory.device.allocateRaw((byte**)&mB64Flags, sizeof(uint32) * cuda_object_svc_constants::total_flags);

	mKernelApi.memory.device.allocateRaw((byte**)&mB128Blocks, sizeof(Block128) * cuda_object_svc_constants::max_object_count);
	mKernelApi.memory.device.allocateRaw((byte**)&mB128Flags, sizeof(uint32) * cuda_object_svc_constants::total_flags);

	mKernelApi.memory.device.allocateRaw((byte**)&mGlobalHeap, sizeof(char) * cuda_object_svc_constants::global_heap_size);
	mKernelApi.memory.device.allocateRaw((byte**)&mGlobalHeapNext, sizeof(char) * cuda_object_svc_constants::global_heap_size);
	mKernelApi.memory.device.allocateRaw((byte**)&mGlobalHeapBucket, sizeof(uint32) * cuda_object_svc_constants::max_object_count);
	mKernelApi.memory.device.allocateRaw((byte**)&mGlobalHeapBucketNext, sizeof(uint32) * cuda_object_svc_constants::max_object_count);
	mKernelApi.memory.device.allocateRaw((byte**)&mGlobalHeapOffset, sizeof(uint32) * cuda_object_svc_constants::max_object_count);
	mKernelApi.memory.device.allocateRaw((byte**)&mGlobalHeapOffsetNext, sizeof(uint32) * cuda_object_svc_constants::max_object_count);
	mKernelApi.memory.device.allocateRaw((byte**)&mGlobalHeapBucketScanned, sizeof(uint32) * cuda_object_svc_constants::max_object_count);
	mKernelApi.memory.device.allocateRaw((byte**)&mGlobalHeapComactionWorkSegments, sizeof(uint32) * cuda_object_svc_constants::global_heap_size / cuda_object_svc_constants::heap_compaction_chunk_size);
	mKernelApi.memory.device.allocateRaw((byte**)&mGlobalHeapComactionWorkIndices, sizeof(uint32) * cuda_object_svc_constants::global_heap_size / cuda_object_svc_constants::heap_compaction_chunk_size);

	detail::__initializeBuffers(
			mHeaderFlags, cuda_object_svc_constants::total_flags,
			mB64Flags, cuda_object_svc_constants::total_flags,
			mB128Flags, cuda_object_svc_constants::total_flags,
			mGlobalHeapBucket, cuda_object_svc_constants::max_object_count);

//	shared_ptr<ExecutionITC> execution_itc = mProcessor->getExecutionITC();
//
//	uint32 local_id = mProcessor->getId();
//
//	// register KernelITC handlers
//	execution_itc->registerHandler(
//			KernelITC::service_itc_gameobject_client_id_create_request,
//			boost::bind(&ObjectService::handleClientIdCreateRequest, this, _1, _2));
//
//	execution_itc->registerHandler(
//			KernelITC::service_itc_gameobject_client_id_destroy_request,
//			boost::bind(&ObjectService::handleClientIdDestroyRequest, this, _1, _2));
//
//	execution_itc->registerHandler(
//			KernelITC::service_itc_gameobject_client_id_query_request,
//			boost::bind(&ObjectService::handleClientIdQueryRequest, this, _1, _2));
//
//	execution_itc->registerHandler(
//			KernelITC::service_itc_gameobject_user_object_update_request,
//			boost::bind(&ObjectService::handleUserObjectUpdateRequest, this, _1, _2));
//
//	execution_itc->registerHandler(
//			KernelITC::service_itc_gameobject_user_object_query_request,
//			boost::bind(&ObjectService::handleUserObjectQueryRequest, this, _1, _2));
//
//	execution_itc->registerHandler(
//			KernelITC::service_itc_gameobject_object_create_request,
//			boost::bind(&ObjectService::handleGameObjectCreateRequest, this, _1, _2));
//
//	execution_itc->registerHandler(
//			KernelITC::service_itc_gameobject_object_destroy_request,
//			boost::bind(&ObjectService::handleGameObjectDestroyRequest, this, _1, _2));
//
//	execution_itc->registerHandler(
//			KernelITC::service_itc_gameobject_object_query_request,
//			boost::bind(&ObjectService::handleGameObjectQueryRequest, this, _1, _2));
//
//	execution_itc->registerHandler(
//			KernelITC::service_itc_gameobject_object_update_request,
//			boost::bind(&ObjectService::handleGameObjectUpdateRequest, this, _1, _2));
//
//	mGC.gc_buffer = new GarbageCollectorBuffer(local_id);
}

void ObjectService::finalize()
{
	mKernelApi.device.use(mDeviceId);
	mKernelApi.memory.device.freeRaw((byte*)mGlobalHeapComactionWorkIndices);
	mKernelApi.memory.device.freeRaw((byte*)mGlobalHeapComactionWorkSegments);
	mKernelApi.memory.device.freeRaw((byte*)mGlobalHeapBucketScanned);
	mKernelApi.memory.device.freeRaw((byte*)mGlobalHeapOffsetNext);
	mKernelApi.memory.device.freeRaw((byte*)mGlobalHeapOffset);
	mKernelApi.memory.device.freeRaw((byte*)mGlobalHeapBucketNext);
	mKernelApi.memory.device.freeRaw((byte*)mGlobalHeapBucket);
	mKernelApi.memory.device.freeRaw((byte*)mGlobalHeapNext);
	mKernelApi.memory.device.freeRaw((byte*)mGlobalHeap);

	mKernelApi.memory.device.freeRaw((byte*)mB128Flags);
	mKernelApi.memory.device.freeRaw((byte*)mB128Blocks);

	mKernelApi.memory.device.freeRaw((byte*)mB64Flags);
	mKernelApi.memory.device.freeRaw((byte*)mB64Blocks);

	mKernelApi.memory.device.freeRaw((byte*)mHeaderFlags);
	mKernelApi.memory.device.freeRaw((byte*)mHeaderBlocks);


//	shared_ptr<ExecutionITC> execution_itc = mProcessor->getExecutionITC();
//
//	execution_itc->unregisterHandler(KernelITC::service_itc_gameobject_client_id_create_request);
//	execution_itc->unregisterHandler(KernelITC::service_itc_gameobject_client_id_destroy_request);
//	execution_itc->unregisterHandler(KernelITC::service_itc_gameobject_client_id_query_request);
//	execution_itc->unregisterHandler(KernelITC::service_itc_gameobject_user_object_query_request);
//	execution_itc->unregisterHandler(KernelITC::service_itc_gameobject_user_object_update_request);
//	execution_itc->unregisterHandler(KernelITC::service_itc_gameobject_object_create_request);
//	execution_itc->unregisterHandler(KernelITC::service_itc_gameobject_object_destroy_request);
//	execution_itc->unregisterHandler(KernelITC::service_itc_gameobject_object_query_request);
//	execution_itc->unregisterHandler(KernelITC::service_itc_gameobject_object_update_request);
//
//	// destroy the object service buffer
//	SAFE_DELETE(mBuffer);
//
//	// release gc stuff
//	if (mGC.marker) mGC.marker->stop();
//	if (mGC.sweeper) mGC.sweeper->stop();
//
//	SAFE_DELETE(mGC.marker);
//	SAFE_DELETE(mGC.sweeper);
//	SAFE_DELETE(mGC.gc_buffer);
}

void ObjectService::beforeInitializeOnKernel(shared_ptr<KernelBase> kernel)
{
}

void ObjectService::afterInitializeOnKernel(shared_ptr<KernelBase> kernel)
{
	using namespace zillians::language::tree;
	using namespace zillians::language::stage;

//	GCContext context;
//	mProcessor->getKernel()->getAllKernels(context.language_infos);
//	context.processor = mProcessor;
//	context.kernel = static_pointer_cast<processor::KernelMT>(kernel);
//
//	context.gc_buffer = mGC.gc_buffer;
//	mGC.marker = new GCMarker(context);
//	mGC.sweeper = new GCSweeper(context);
//	startGC();
}

void ObjectService::beforeFinalizeOnKernel(shared_ptr<KernelBase> kernel)
{
}

void ObjectService::afterFinalizeOnKernel(shared_ptr<KernelBase> kernel)
{
}

void* ObjectService::getInternalBufferDataPtr()
{
	return NULL;
}

void* ObjectService::getInternalBufferPtr()
{
	return NULL;
}

void ObjectService::handleEvent(events::type event)
{
	switch(event)
	{
	case events::before_invocation_dispatch:
	{
		if(!mKernelInitialized)
		{
			void* handle = static_pointer_cast<processor::cuda::Kernel>(mProcessor->getKernel())->getDispatcherHandle();

			// save the shuffle indices buffer into kernel
			void (*configure_header_blocks)(TrackerHeader* blocks, uint32* bit_flags);
			void (*configure_blocks_b64)(Block64* blocks, uint32* bit_flags);
			void (*configure_blocks_b128)(Block128* blocks, uint32* bit_flags);
			void (*configure_global_heap)(char* heap, uint32 heap_size, uint32* heap_bucket, uint32 heap_bucket_size, uint32* heap_bucket_offset, unsigned long long int heap_next_offset);

			configure_header_blocks = (decltype(configure_header_blocks))dlsym(handle, "_ZN4thor4lang8internal23__configureHeaderBlocksEPNS1_13TrackerHeaderEPj");
			configure_blocks_b64 = (decltype(configure_blocks_b64))dlsym(handle, "_ZN4thor4lang8internal20__configureBlocksB64EPNS1_7Block64EPj");
			configure_blocks_b128 = (decltype(configure_blocks_b128))dlsym(handle, "_ZN4thor4lang8internal21__configureBlocksB128EPNS1_8Block128EPj");
			configure_global_heap = (decltype(configure_global_heap))dlsym(handle, "_ZN4thor4lang8internal21__configureGlobalHeapEPcjPjjS3_y");

			if(!configure_header_blocks || !configure_blocks_b64 || !configure_blocks_b128 || !configure_global_heap)
			{
				std::cerr << "Error: failed to find the object service kernel configuration functions in global dispatcher" << std::endl;
				throw std::runtime_error("failed to find the object service kernel configuration functions in global dispatcher");
			}

			configure_header_blocks(mHeaderBlocks, mHeaderFlags);
			configure_blocks_b64(mB64Blocks, mB64Flags);
			configure_blocks_b128(mB128Blocks, mB128Flags);
			configure_global_heap(mGlobalHeap, cuda_object_svc_constants::global_heap_size, mGlobalHeapBucket, cuda_object_svc_constants::max_object_count, mGlobalHeapOffset, 0ULL);

			mConfigureGlobalHeapFunc = configure_global_heap;

			mKernelInitialized = true;
		}
		break;
	}
	case events::after_invocation_dispatch:
	{
		// TODO the heap compaction should be triggered only when the heap is fragmented and the available size is too small
		{
			unsigned long long int heap_next_offset = 0;
			bool is_fully_compacted = true;
			detail::__compactGlobalHeap(
					mGlobalHeap,
					mGlobalHeapNext,
					mGlobalHeapBucket,
					mGlobalHeapBucketNext,
					mGlobalHeapOffset,
					mGlobalHeapOffsetNext,
					mGlobalHeapBucketScanned,
					mGlobalHeapComactionWorkSegments,
					mGlobalHeapComactionWorkIndices,
					heap_next_offset, is_fully_compacted);

			if(!is_fully_compacted)
			{
				// TODO update trakcer ptr

				// swap the current heap buffer with the next one
				detail::swap(mGlobalHeap, mGlobalHeapNext);
				detail::swap(mGlobalHeapBucket, mGlobalHeapBucketNext);
				detail::swap(mGlobalHeapOffset, mGlobalHeapOffsetNext);

				// reconfigure the global heap
				mConfigureGlobalHeapFunc(mGlobalHeap, cuda_object_svc_constants::global_heap_size, mGlobalHeapBucket, cuda_object_svc_constants::max_object_count, mGlobalHeapOffset, heap_next_offset);
			}
		}
		break;
	}
    case events::idletime_heartbeat:
    {
        break;
    }
	default:
		break;
	}

}

} } } }
