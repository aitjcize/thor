/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/service/Common.h"
#include "framework/service/runtime/RuntimeServiceCUDA.h"
#include "framework/buffer/BufferNetwork.h"
#include "framework/ProcessorId.h"
#include "framework/processor/ProcessorCUDA.h"
#include "framework/processor/KernelCUDA.h"
#include "framework/Configuration.h"
#include "thor/lang/Domain.h"
#include "thor/lang/Lambda.h"
#include <dlfcn.h>
#include <thrust/device_ptr.h>

namespace zillians { namespace framework { namespace service { namespace cuda {

namespace detail {

extern void __extractFunctionIds(char* invocation_buffer_ptr, int64* function_ids_buffer_ptr, int32 invocation_count);
extern void __updateShuffleIndice(int64* function_ids_buffer_ptr, int32* shuffle_indices_buffer_ptr, int32 invocation_count);

}

using namespace zillians::framework::buffer;

log4cxx::LoggerPtr RuntimeService::mLogger(log4cxx::Logger::getLogger("RuntimeService"));

//////////////////////////////////////////////////////////////////////////
RuntimeService::RuntimeService(ProcessorRT* processor) :
		mProcessor(processor),
		mSetDebugCondFunc(NULL),
		mSetCurrentInvocationBufferFunc(NULL),
		mSetNextInvocationBufferFunc(NULL),
		mKernelInitialized(false),
		mVerboseMode(false),
		mKernelApi(processor::cuda::GetKernelApi())

{
	BOOST_ASSERT(Configuration::instance()->enable_cuda);

	uint32 local_id = mProcessor->getId();
	mDeviceId = ((processor::ProcessorCUDA*)mProcessor)->getDeviceId();
	buffer_location_t::type location = buffer_location_t::get_ptx_device_location(mDeviceId);

	BufferNetwork::instance()->declareReadableLocation<InvocationRequestBuffer>(local_id, location, 0);
	BufferNetwork::instance()->declareWritableLocation<InvocationRequestBuffer>(local_id, location, 0);

	mKernelApi.device.use(mDeviceId);
	mKernelApi.memory.device.allocateRaw((byte**)&mDevicePtr.function_ids, sizeof(int64) * buffer_constants::max_invocations);
	mKernelApi.memory.device.allocateRaw((byte**)&mDevicePtr.shuffle_indices, sizeof(int32) * buffer_constants::max_invocations);
	mKernelApi.memory.device.allocateRaw((byte**)&mDevicePtr.next_invocation_buffer_container, sizeof(Invocation*) * cuda_runtime_svc_constants::next_invocation_bucket_size * buffer_constants::max_targets);
	mKernelApi.memory.device.allocateRaw((byte**)&mDevicePtr.next_invocation_size_buffer, sizeof(int32) * cuda_runtime_svc_constants::next_invocation_bucket_size * buffer_constants::max_targets);
	mKernelApi.memory.device.allocateRaw((byte**)&mDevicePtr.next_invocation_index_buffer, sizeof(int32) * cuda_runtime_svc_constants::next_invocation_bucket_size * buffer_constants::max_targets);
	mKernelApi.memory.device.allocateRaw((byte**)&mDevicePtr.control_flags, sizeof(ControlFlags));

	mDevicePtr.next_invocation_buffer_objects = new InvocationRequestBuffer*[cuda_runtime_svc_constants::next_invocation_bucket_size * buffer_constants::max_targets];
	for(int i=0;i<cuda_runtime_svc_constants::next_invocation_bucket_size * buffer_constants::max_targets;++i)
	{
		InvocationRequestBuffer* buffer = new InvocationRequestBuffer(local_id);
		BufferNetwork::instance()->create(local_id, *buffer, InvocationRequestBuffer::Dimension(buffer_constants::max_invocations));
		mDevicePtr.next_invocation_buffer_objects[i] = buffer;
	}

	mKernelApi.memory.host.allocateRaw((byte**)&mHostPtr.control_flags, sizeof(ControlFlags), true, false, false, false);
}

RuntimeService::~RuntimeService()
{
	mKernelApi.device.use(mDeviceId);

	mKernelApi.memory.host.freeRaw((byte*)mHostPtr.control_flags, true);

	SAFE_DELETE_ARRAY(mDevicePtr.next_invocation_buffer_objects);

	mKernelApi.memory.device.freeRaw((byte*)mDevicePtr.control_flags);
	mKernelApi.memory.device.freeRaw((byte*)mDevicePtr.next_invocation_index_buffer);
	mKernelApi.memory.device.freeRaw((byte*)mDevicePtr.next_invocation_size_buffer);
	mKernelApi.memory.device.freeRaw((byte*)mDevicePtr.next_invocation_buffer_container);
	mKernelApi.memory.device.freeRaw((byte*)mDevicePtr.shuffle_indices);
	mKernelApi.memory.device.freeRaw((byte*)mDevicePtr.function_ids);
}

//////////////////////////////////////////////////////////////////////////
bool RuntimeService::isCompleted()
{
	return mDispatchedInvocations.empty();
}

bool RuntimeService::isInvocationPending()
{
	return !mDispatchedInvocations.empty();
}

//////////////////////////////////////////////////////////////////////////
void RuntimeService::initialize()
{
	shared_ptr<ExecutionITC> execution_itc = mProcessor->getExecutionITC();

	uint32 local_id = mProcessor->getId();

	// register KernelITC handlers
	// TODO implement CUDA version domain service
//	execution_itc->registerHandler(
//			KernelITC::processor_itc_session_open_request,
//			boost::bind(&RuntimeService::handleSessionEvent, this, (int32)thor::lang::DomainEvent::Connected, _1, _2));
//
//	execution_itc->registerHandler(
//			KernelITC::processor_itc_session_close_request,
//			boost::bind(&RuntimeService::handleSessionEvent, this, (int32)thor::lang::DomainEvent::Disconnected, _1, _2));
//
//	execution_itc->registerHandler(
//			KernelITC::processor_itc_session_unreachable_request,
//			boost::bind(&RuntimeService::handleSessionEvent, this, (int32)thor::lang::DomainEvent::Unreachable, _1, _2));
//
//	execution_itc->registerHandler(
//			KernelITC::processor_itc_session_resume_request,
//			boost::bind(&RuntimeService::handleSessionEvent, this, (int32)thor::lang::DomainEvent::Resumed, _1, _2));
//
//	execution_itc->registerHandler(
//			KernelITC::processor_itc_session_crash_request,
//			boost::bind(&RuntimeService::handleSessionEvent, this, (int32)thor::lang::DomainEvent::Crashed, _1, _2));
//
    execution_itc->registerHandler(
            KernelITC::processor_itc_invocation_request,
            boost::bind(&RuntimeService::handleProcessorInvocation, this, _1, _2));

    // create thread-local next invocation buffer for each target
//    for(int i=0;i<buffer::buffer_constants::max_targets * buffer::buffer_constants::max_threads;++i)
//    {
//        InvocationRequestBuffer* next_invocation_buffer = new InvocationRequestBuffer(local_id);
//        BufferNetwork::instance()->create(local_id, *next_invocation_buffer, InvocationRequestBuffer::Dimension(buffer::buffer_constants::max_invocations));
//        mBuffer->setNextInvocation(i, next_invocation_buffer);
//    }
}

void RuntimeService::finalize()
{
	shared_ptr<ExecutionITC> execution_itc = mProcessor->getExecutionITC();
	uint32 local_id = mProcessor->getId();

	execution_itc->unregisterHandler(KernelITC::processor_itc_invocation_request);

	// destroy thread-local next invocation buffer for each target
//	for(int i=0;i<buffer::buffer_constants::max_targets * buffer::buffer_constants::max_threads;++i)
//	{
//		InvocationRequestBuffer* next_invocation_buffer = mBuffer->getNextInvocation(i);
//		SAFE_DELETE(next_invocation_buffer);
//		mBuffer->setNextInvocation(i, NULL);
//	}
}

void RuntimeService::afterInitializeOnKernel(shared_ptr<KernelBase> kernel)
{
    mVerboseMode = kernel->getVerbose();
}

void RuntimeService::setupArguments(const std::wstring& args)
{
    // TODO
//	mBuffer->setArguments(args);
}

int32 RuntimeService::getExitCode()
{
    using zillians::framework::processor::cuda::KernelApi;

    return mHostPtr.control_flags->exit_code;
}

//////////////////////////////////////////////////////////////////////////
void RuntimeService::handleEvent(events::type event)
{
    using zillians::framework::processor::cuda::KernelApi;

	uint32 local_id = mProcessor->getId();
	shared_ptr<ExecutionMonitor> monitor = mProcessor->getMonitor();

	BOOST_ASSERT(!!monitor && "ExecutionMonitor should be set prior to any execution event");

	// notify the execution monitor about the status of current execution
	// the default execution state is incomplete
    static bool once_init_mark_incomplete = false;
    if(!once_init_mark_incomplete)
    {
		monitor->markIncompleted(local_id);
		once_init_mark_incomplete = true;
    }

	switch(event)
	{
	case events::before_invocation_dispatch:
	{
		if(!mKernelInitialized)
		{
			void* handle = static_pointer_cast<processor::cuda::Kernel>(mProcessor->getKernel())->getDispatcherHandle();

			// get the shuffle indices buffer into kernel configuration function ptr
			// which is dynamically linked to thor::lang::internal::__setShuffleIndicesBuffer
			void (*configure_shuffle_indices_buffer)(int32*, int32);
			configure_shuffle_indices_buffer = (decltype(configure_shuffle_indices_buffer))dlsym(handle, "_ZN4thor4lang8internal25__setShuffleIndicesBufferEPii");

			// get the next invocation buffer container configuration function ptr
			// which is dynamically linked to thor::lang::internal::__setNextInvocationBufferContainer
			void (*configure_next_invocation_buffer_container)(Invocation**);
			configure_next_invocation_buffer_container = (decltype(configure_next_invocation_buffer_container))dlsym(handle, "_ZN4thor4lang8internal34__setNextInvocationBufferContainerEPPN8zillians9framework7service10InvocationE");

			// get the next invocation size buffer configuration function ptr
			// which is dynamically linked to thor::lang::internal::__setNextInvocationSizeBuffer
			void (*configure_next_invocation_size_buffer)(int32*);
			configure_next_invocation_size_buffer = (decltype(configure_next_invocation_size_buffer))dlsym(handle, "_ZN4thor4lang8internal29__setNextInvocationSizeBufferEPi");

			// get the next invocation index buffer configuration function ptr
			// which is dynamically linked to thor::lang::internal::__setNextInvocationSizeBuffer
			void (*configure_next_invocation_index_buffer)(int32*);
			configure_next_invocation_index_buffer = (decltype(configure_next_invocation_index_buffer))dlsym(handle, "_ZN4thor4lang8internal30__setNextInvocationIndexBufferEPi");

			void (*configure_control_flags)(ControlFlags*);
			configure_control_flags = (decltype(configure_control_flags))dlsym(handle, "_ZN4thor4lang8internal17__setControlFlagsEPN8zillians9framework7service4cuda12ControlFlagsE");

			mSetDebugCondFunc = (decltype(mSetDebugCondFunc))dlsym(handle, "_ZN4thor4lang8internal14__setDebugCondEb");

			// get the current invocation buffer configuration function ptr
			// which is dynamically linked to thor::lang::internal::__setInvocationBuffer
			mSetCurrentInvocationBufferFunc = (decltype(mSetCurrentInvocationBufferFunc))dlsym(handle, "_ZN4thor4lang8internal28__setCurrentInvocationBufferEPN8zillians9framework7service10InvocationEi");

			// get the current invocation buffer configuration function ptr
			// which is dynamically linked to thor::lang::internal::__setNextInvocationBuffer
			mSetNextInvocationBufferFunc = (decltype(mSetNextInvocationBufferFunc))dlsym(handle, "_ZN4thor4lang8internal25__setNextInvocationBufferEiPN8zillians9framework7service10InvocationEi");

			if( !configure_shuffle_indices_buffer ||
				!configure_next_invocation_buffer_container ||
				!configure_next_invocation_size_buffer ||
				!configure_next_invocation_index_buffer ||
				!configure_control_flags ||
				!mSetDebugCondFunc ||
				!mSetCurrentInvocationBufferFunc ||
				!mSetNextInvocationBufferFunc)
			{
				throw std::runtime_error("failed to find configuration function in global dispatcher");
			}

			configure_shuffle_indices_buffer(mDevicePtr.shuffle_indices, buffer_constants::max_invocations);
			configure_next_invocation_buffer_container(mDevicePtr.next_invocation_buffer_container);
			configure_next_invocation_size_buffer(mDevicePtr.next_invocation_size_buffer);
			configure_next_invocation_index_buffer(mDevicePtr.next_invocation_index_buffer);
			configure_control_flags(mDevicePtr.control_flags);

			mSetDebugCondFunc(false);

			// initialize all next invocation buffers
			for(int i=0;i<buffer_constants::max_targets;++i)
			{
				for(int j=0;j<cuda_runtime_svc_constants::next_invocation_bucket_size;++j)
				{
				    int index = i * cuda_runtime_svc_constants::next_invocation_bucket_size + j;
					mSetNextInvocationBufferFunc(index, mDevicePtr.next_invocation_buffer_objects[index]->getInvocation(0), buffer_constants::max_invocations);
				}
			}

            mKernelApi.execution.synchronize();
			mKernelInitialized = true;
		}

		if(!mDispatchedInvocations.empty())
		{
			std::pair<uint32, KernelITC>& t = mDispatchedInvocations.front();
			InvocationRequestBuffer* buffer = t.second.getInvocationRequest().buffer;

			// save the invocation buffer into the kernel
			mSetCurrentInvocationBufferFunc(buffer->getInvocation(0), buffer->getInvocationCount());

			// extract the function ids into a compact array
			detail::__extractFunctionIds((char*)buffer->getInvocationRawPtr(0), mDevicePtr.function_ids, buffer->getInvocationCount());

			// compute the shuffle indices (to avoid branching) for all invocations
			detail::__updateShuffleIndice(mDevicePtr.function_ids, mDevicePtr.shuffle_indices, buffer->getInvocationCount());

			// configure the kernel execution dimension
			shared_ptr<processor::cuda::Kernel> kernel = static_pointer_cast<processor::cuda::Kernel>(mProcessor->getKernel());
			dim3 blocks = { ceiling<uint32>(buffer->getInvocationCount(), buffer::cuda_runtime_svc_constants::compute_threads_per_block), 1, 1 };
			dim3 threads = { buffer::cuda_runtime_svc_constants::dispatcher_threads_per_block, 1, 1 };
			kernel->configureLaunchMode(false);
			kernel->configureLaunchShape(blocks, threads);
		}
		break;
	}
	case events::after_invocation_dispatch:
	{
        std::pair<uint32, KernelITC>& t = mDispatchedInvocations.front();
        if(t.second.continuation_id != 0)
        {
            // send invocation response back
            auto invocation_response_itc = KernelITC::make<KernelITC::InvocationResponse>(t.second.continuation_id, service::InvocationResponseBuffer{0, true});

            mProcessor->sendITC(t.first, invocation_response_itc);
        }

        BufferNetwork::instance()->destroy(*t.second.getInvocationRequest().buffer);
        SAFE_DELETE(t.second.getInvocationRequest().buffer);
        mDispatchedInvocations.pop_front();

        dispatchNextInvocations();
        dispatchObjectClearInvocations();

        static bool has_exited = false;
        if(!has_exited)
        {
            mKernelApi.memory.copyRaw(
                    (byte*)mHostPtr.control_flags,
                    (byte*)mDevicePtr.control_flags,
                    0, 0,
                    sizeof(ControlFlags),
                    KernelApi::location::host, KernelApi::location::device);

            if(mHostPtr.control_flags->implicit_exited && !mHostPtr.control_flags->daemonized)
            {
                mHostPtr.control_flags->exit_code = mHostPtr.control_flags->implicit_exit_code;
                mHostPtr.control_flags->exited = true;
                monitor->markCompleted(local_id);
                has_exited = true;
            }
            else if(mHostPtr.control_flags->explicit_exited)
            {
                mHostPtr.control_flags->exit_code = mHostPtr.control_flags->explicit_exit_code;
                mHostPtr.control_flags->exited = true;
                monitor->markCompleted(local_id);
                has_exited = true;
            }
        }

		break;
	}
    case events::idletime_heartbeat:
    {
//        dispatchObjectClearInvocations();
        break;
    }
	default:
		break;
	}

}

//////////////////////////////////////////////////////////////////////////
//void RuntimeService::splitActiveInactiveInvocation(InvocationRequestBuffer* current_buffer)
//{
//    if (current_buffer->getInvocationCount() == 0) return;
//
//    // the active invocation will be condensed in the current invocation request buffer
//    uint32 local_id = mProcessor->getId();
//
//    // get next buffer according to the target id
//    int32 target_id = util::getTargetId(current_buffer->getSessionId(0));
//    auto next_buffer = mBuffer->getNextInvocation(target_id);
//
//    int32 active_count = 0;
//    std::vector<Invocation*> next_invocations;
//    for (int32 i = 0; i < current_buffer->getInvocationCount(); i++)
//    {
//        if (current_buffer->isActive(i))
//        {
//            if (i != active_count)
//            {
//                // move the content from position i to active_count
//                current_buffer->moveInvocation(i, active_count);
//            }
//
//            active_count++;
//        }
//        else
//        {
//            Invocation* invocation = current_buffer->getInvocation(i);
//            next_invocations.push_back(invocation);
//            //std::cout << "[kernel split] move function from current to next invocation buffer: " << invocation->function_id << std::endl;
//        }
//    }
//
//    current_buffer->setInvocationCount(active_count);
//    next_buffer->insertIvocationsAtFront(next_invocations);
//}

void RuntimeService::dispatchObjectClearInvocations()
{
//    // ask object service if we are allowed to get to-recycle objects
//    auto object_service = (ObjectServiceMultiThread*)(*mProcessor)[ServiceId::SERVICE_OBJECT];
//    if (!object_service->isGCInitialized()) return;
//    if (object_service->getRecycleObjects().size() == 0) return;
//
//    // query domain callback function id
//    std::string object_destroy("_ZN4thor4lang23__destroyObjectInternalEPNS0_6ObjectE");
//    shared_ptr<KernelMultiThread> kernel = static_pointer_cast<KernelMultiThread>(mProcessor->getKernel());
//
//    static int64 object_destroy_fid = -1;
//    if (object_destroy_fid == -1)
//    {
//        if (!kernel->queryFunctionId(mProcessor->getHandle(), object_destroy, object_destroy_fid))
//        {
//            std::cerr << __FUNCTION__ << ": fail to found __destroyObject function" << std::endl;
//            return;
//        }
//    }
//
//    // create invocation buffer to call to recycle __destroyObject
//    auto recycle_objects = object_service->getRecycleObjects();
//
//    // create invocation buffer
//    uint32 local_id = mProcessor->getId();
//    InvocationRequestBuffer* buffer = new InvocationRequestBuffer(local_id);
//    BufferNetwork::instance()->create(local_id, *buffer, InvocationRequestBuffer::Dimension(recycle_objects.size()));
//    buffer->setInvocationCount(recycle_objects.size());
//
//    typedef std::remove_reference<decltype(recycle_objects.front())>::type object_type_t;
//    for(int i = 0; i < recycle_objects.size(); i++)
//    {
//        buffer->setFunctionId(i, object_desrc/libzillians-framework-service/framework/service/runtime/RuntimeService.cpp:#includestroy_fid);
//        buffer->setSessionId(i, -1);
//
//        // well, insert object pointer
//        object_type_t* parameter_ptr = (object_type_t*)buffer->getInvocationPtr(i);
//        *parameter_ptr = recycle_objects[i];
//    }
//
//    // clear the recycle list, so that the gc service will continue to work
//    object_service->resetRecycleObjects();
//
//	// send itc
//	KernelITC invoke_itc; invoke_itc.kernel_itc_type = KernelITC::processor_itc_invocation_request;
//	invoke_itc.processor_itc.invocation_request.buffer = buffer;
//
//	shared_ptr<ExecutionITC> execution_itc = mProcessor->getExecutionITC();
//	execution_itc->dispatch(ProcessorId::PROCESSOR_PRINCIPLE, invoke_itc);
}

void RuntimeService::dispatchNextInvocations()
{
	using zillians::framework::processor::cuda::KernelApi;

    shared_ptr<KernelBase> kernel = mProcessor->getKernel();
    if(!!kernel)
    {
		int32 next_invocation_index_buffer[cuda_runtime_svc_constants::next_invocation_bucket_size * buffer_constants::max_targets];
		mKernelApi.memory.copyRaw(
				(byte*)&next_invocation_index_buffer[0],
				(byte*)mDevicePtr.next_invocation_index_buffer,
				0, 0,
				sizeof(int32) * cuda_runtime_svc_constants::next_invocation_bucket_size * buffer_constants::max_targets,
				KernelApi::location::host, KernelApi::location::device);

	    uint32 local_id = mProcessor->getId();
        shared_ptr<ExecutionITC> execution_itc = mProcessor->getExecutionITC();

        // dispatch the generated invocations
        for(int i = 0; i < buffer_constants::max_targets; ++i)
        {
        	std::size_t total_invocations = 0;
        	for(int j = 0; j < cuda_runtime_svc_constants::next_invocation_bucket_size; ++j)
        	{
        		int32 index = i * cuda_runtime_svc_constants::next_invocation_bucket_size + j;

        		// update the number of invocation available in the buffer
        		mDevicePtr.next_invocation_buffer_objects[index]->setInvocationCount(next_invocation_index_buffer[index]);
        		total_invocations += mDevicePtr.next_invocation_buffer_objects[index]->getInvocationCount();
        	}

        	if(total_invocations > 0)
        	{
        		while(total_invocations > 0)
        		{
        			std::size_t buffer_size;
        			if(total_invocations > buffer_constants::max_invocations)
        			{
        				total_invocations -= total_invocations;
        				buffer_size = buffer_constants::max_invocations;
        			}
        			else
        			{
        				buffer_size = total_invocations;
        				total_invocations = 0;
        			}

        			InvocationRequestBuffer* buffer_to_dispatch = new InvocationRequestBuffer(local_id);
        			BufferNetwork::instance()->create(local_id, *buffer_to_dispatch, InvocationRequestBuffer::Dimension(buffer_size));

        			std::size_t buffer_last_size = 0;
        			for(int j = 0; j < cuda_runtime_svc_constants::next_invocation_bucket_size; ++j)
        			{
        				int32 index = i * cuda_runtime_svc_constants::next_invocation_bucket_size + j;
        				int32 next_buffer_size;
        				if( (next_buffer_size = mDevicePtr.next_invocation_buffer_objects[index]->getInvocationCount()) > 0)
        				{
							mKernelApi.memory.copyRaw(
									(byte*)buffer_to_dispatch->getInvocationRawPtr(buffer_last_size),
									(byte*)mDevicePtr.next_invocation_buffer_objects[index]->getInvocationRawPtr(0),
									0, 0,
									Invocation::PAYLOAD * next_buffer_size,
									KernelApi::location::device, KernelApi::location::device);

							buffer_last_size += next_buffer_size;
        				}
        			}

        			buffer_to_dispatch->setInvocationCount(buffer_size);

					auto itc = KernelITC::make<KernelITC::InvocationRequest>(0, 0, buffer_to_dispatch);

					execution_itc->dispatch(i, itc);
        		}
        	}
        }

        // reset the next invocation index buffer
        for(int i=0;i<cuda_runtime_svc_constants::next_invocation_bucket_size * buffer_constants::max_targets;++i) next_invocation_index_buffer[i] = 0;

		mKernelApi.memory.copyRaw(
				(byte*)mDevicePtr.next_invocation_index_buffer,
				(byte*)&next_invocation_index_buffer[0],
				0, 0,
				sizeof(int32) * cuda_runtime_svc_constants::next_invocation_bucket_size * buffer_constants::max_targets,
				KernelApi::location::device, KernelApi::location::host);

    }
}

void RuntimeService::handleProcessorInvocation(uint32 source, KernelITC& itc)
{
	BOOST_ASSERT(itc.isInvocationRequest());

	if(mVerboseMode)
	    std::cerr << "handleProcessorInvocation: invocation count=" << itc.getInvocationRequest().buffer->getInvocationCount() << std::endl;

	if(itc.getInvocationRequest().buffer->getInvocationCount() > 0)
	{
		std::pair<uint32,KernelITC> p(std::move(source), std::move(itc));
		mDispatchedInvocations.push_back(std::move(p));
	}
}

void RuntimeService::handleSessionEvent(int32 event, uint32 source, KernelITC& itc)
{
//    // query domain callback function id
//    std::string callback_function("_ZN4thor4lang15domain_callbackEi");
//    shared_ptr<KernelMultiThread> kernel = static_pointer_cast<KernelMultiThread>(mProcessor->getKernel());
//
//    static int64 callback_function_id = -1;
//    if (callback_function_id == -1)
//    {
//        if (!kernel->queryFunctionId(mProcessor->getHandle(), callback_function, callback_function_id))
//        {
//            std::cerr << __FUNCTION__ << ": fail to found domain callback function" << std::endl;
//            return;
//        }
//    }
//
//	// create invocation buffer
//	int64 session_id = itc.processor_itc.session_open_request.session_id;
//	uint32 local_id = mProcessor->getId();
//	InvocationRequestBuffer* buffer = new InvocationRequestBuffer(local_id);
//	BufferNetwork::instance()->create(local_id, *buffer, InvocationRequestBuffer::Dimension(1));
//
//	buffer->setInvocationCount(1);
//    buffer->setFunctionId(0, callback_function_id);
//    buffer->setSessionId(0, session_id);
//
//    // insert parameter, i.e. event
//    int32* parameter_ptr = (int32*)buffer->getInvocationPtr(0);
//    *parameter_ptr = event;
//
//	// TODO should we use ITC or just enqueue these invocation request?
//	KernelITC invoke_itc; invoke_itc.kernel_itc_type = KernelITC::processor_itc_invocation_request;
//	invoke_itc.processor_itc.invocation_request.buffer = buffer;
//
//	shared_ptr<ExecutionITC> execution_itc = mProcessor->getExecutionITC();
//	execution_itc->dispatch(ProcessorId::PROCESSOR_PRINCIPLE, invoke_itc);
//
//	// TODO should we clean up the domain when it's completed?
}

} } } }
