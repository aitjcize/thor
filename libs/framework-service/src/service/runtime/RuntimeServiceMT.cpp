/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <dlfcn.h>

#include <cstring>

#include <algorithm>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include <boost/assert.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/fill.hpp>
#include <boost/range/algorithm/transform.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"

#include "language/context/ManglingStageContext.h"
#include "language/context/TransformerContext.h"
#include "language/SystemFunction.h"

#include "framework/buffer/BufferNetwork.h"
#include "framework/Configuration.h"
#include "framework/Kernel.h"
#include "framework/service/detail/ProxyHelper.h"
#include "framework/service/runtime/RuntimeServiceMT.h"
#include "framework/service/object/ObjectServiceMT.h"
#include "framework/Processor.h"
#include "framework/ProcessorId.h"
#include "framework/processor/KernelMT.h"

#include "thor/lang/Domain.h"

namespace zillians { namespace framework { namespace service { namespace mt {

ZILLIANS_FRAMEWORK_SERVICE_PROXY_DEF(
    RuntimeService,
    runtime       ,
    ((const std::vector<std::wstring>&)(getArguments)                                          )
    ((void                            )(setArguments)((const std::vector<std::wstring>&, args)))

    ((void)(exit)((int32, exit_code)))

    ((int32)(getCurrentInvocationId))

    ((InvocationRequestBuffer*)(getCurrentInvocations          )                                                    )
    ((InvocationRequestBuffer*)(getCurrentThreadNextInvocations)((uint32                  , target_id             )))

    ((void)(postponeInvocationDependencyTo)((int32, target_id))((int32, postpone_to_id))                       )
    ((void)(setInvocationDependOnCount    )((int32, target_id))((int32,  invocation_id))((int32, count       )))
    ((void)(setInvocationDependOn         )((int32, target_id))((int32,  invocation_id))((int32, depend_on_id)))

    ((void )(delayInvocation)((Invocation&, invocation))                                                                                   )
    ((int32)(  addInvocation)((int32      , target_id ))((int64, session_id))((int64, function_id))((int8*                     , store_to)))
    ((void )(  addInvocation)((int32      , target_id ))((int64, session_id))((int64,  adaptor_id))((ReplicationData*          , data    )))

    ((                        int32)(reserveInvocation        )((int32, target_id ))((int64, session_id))((int64, function_id))                      )
    ((                        bool )(commitInvocation         )((int32, target_id ))((int32, id        ))                                            )
    ((                        bool )(abortInvocation          )((int32, target_id ))((int32, id        ))                                            )
    ((                        void )(setInvocationReturnPtr   )((int32, target_id ))((int32, id        ))((void*, ptr        ))                      )
    ((template<typename Type> int64)(appendInvocationParameter)((int32, target_id ))((int32, id        ))((int64, offset     ))((const Type&, value)))

    ((RemoteAdaptorSignature*)(getRemoteAdaptor)((int64, adaptor_id)))
    ((CreatorSignature*      )(getCreator      )((int64, type_id   )))
    ((SerializerSignature*   )(getSerializer   )((int64, type_id   )))
    ((DeserializerSignature* )(getDeserializer )((int64, type_id   )))

    ((const std::wstring*)(queryStringLiteral)((int64, symbol_id)))
)

namespace proxies { namespace runtime {

template int64 appendInvocationParameter<bool  >(int32 target_id, int32 id, int64 offest, bool   const& value);
template int64 appendInvocationParameter<int8  >(int32 target_id, int32 id, int64 offest, int8   const& value);
template int64 appendInvocationParameter<int16 >(int32 target_id, int32 id, int64 offest, int16  const& value);
template int64 appendInvocationParameter<int32 >(int32 target_id, int32 id, int64 offest, int32  const& value);
template int64 appendInvocationParameter<int64 >(int32 target_id, int32 id, int64 offest, int64  const& value);
template int64 appendInvocationParameter<float >(int32 target_id, int32 id, int64 offest, float  const& value);
template int64 appendInvocationParameter<double>(int32 target_id, int32 id, int64 offest, double const& value);
template int64 appendInvocationParameter<int8* >(int32 target_id, int32 id, int64 offest, int8*  const& value);

} }

namespace {

template<typename FunctionSignature, typename ContextType>
FunctionSignature* getSpecialFunctionImpl(processor::mt::Kernel& kernel, int64 type_id)
{
    const auto*const class_decl = kernel.queryClassDecl(type_id);
    BOOST_ASSERT(class_decl != nullptr && "invalid type identifer");

    // find method declaration
    const auto*const candidates = ContextType::get(class_decl);

    if (candidates == nullptr)
        return nullptr;

    if (candidates->size() != 1)
        return nullptr;

    const auto*const method = candidates->front();
    BOOST_ASSERT(method != nullptr && "internal error: such a abstract syntax tree node is not exist");

    // get callable object
    return kernel.queryCallable<FunctionSignature>(
        zillians::language::NameManglingContext::get(method)->mangled_name
    );
}

bool isExpectedType(const language::tree::TypeSpecifier& ts, const language::tree::PrimitiveKind kind)
{
    const language::tree::Type* type = ts.getCanonicalType();

    BOOST_ASSERT(type && "null pointer exception");

    const language::tree::PrimitiveType* type_primitive = type->getAsPrimitiveType();

    if (type_primitive == nullptr)
        return false;

    return type_primitive->getKind() == kind;
}

bool isExpectedType(const language::tree::TypeSpecifier& ts, const std::vector<std::wstring>& names)
{
    BOOST_ASSERT(!names.empty() && "empty class name");

    const language::tree::Type* type = ts.getCanonicalType();

    BOOST_ASSERT(type && "null pointer exception");

    const language::tree::RecordType* type_record = type->getAsRecordType();

    if (type_record == nullptr)
        return false;

    const language::tree::ClassDecl* type_cls = cast_or_null<language::tree::ClassDecl>(type_record->getDecl());

    if (type_cls == nullptr)
        return false;

    const auto& parent_packages = language::tree::ASTNodeHelper::getParentScopePackages(type_cls);

    if (parent_packages.size() != names.size() - 1)
        return false;

    const bool is_expected_package = std::equal(
        parent_packages.cbegin(), parent_packages.cend(),
        names.cbegin(),
        [](const language::tree::Package* pkg, const std::wstring& name)
        {
            BOOST_ASSERT(pkg && "null pointer exception");
            BOOST_ASSERT(pkg->id && "null pointer exception");

            return pkg->id->name == name;
        }
    );

    if (!is_expected_package)
        return false;

    BOOST_ASSERT(type_cls->name && "null pointer exception");

    return type_cls->name->toString() == names.back();
}

template<std::size_t remain_count>
struct ParameterExpectationHelper {
    template<typename parameters_type, typename ...matchers_type>
    static bool impl(const parameters_type& parameters, std::tuple<matchers_type...>&& matchers)
    {
        constexpr std::size_t index = remain_count - 1;

        const language::tree::VariableDecl*const param = parameters[index];

        BOOST_ASSERT(param && "null pointer exception");

        if (!isExpectedType(*param->type, std::get<index>(matchers)))
            return false;

        return ParameterExpectationHelper<remain_count - 1>::impl(parameters, std::forward<std::tuple<matchers_type...>>(matchers));
    }
};

template<>
struct ParameterExpectationHelper<0> {
    template<typename parameters_type, typename ...matchers_type>
    static bool impl(const parameters_type&, std::tuple<matchers_type...>&&)
    {
        return true;
    }
};

template<typename return_matcher_type, typename ...parameter_matchers_type>
bool isExpectedSignature(const language::tree::FunctionDecl& func_decl, return_matcher_type&& return_matcher, parameter_matchers_type&&... parameter_matchers)
{
    if (!func_decl.isGlobal() && (!func_decl.is_member || !func_decl.is_static))
        return false;

    if (func_decl.parameters.size() != sizeof...(parameter_matchers))
        return false;

    if (!isExpectedType(*func_decl.type, std::forward<return_matcher_type>(return_matcher)))
        return false;

    if (!ParameterExpectationHelper<sizeof...(parameter_matchers)>::impl(func_decl.parameters, std::make_tuple(std::forward<parameter_matchers_type>(parameter_matchers)...)))
        return false;

    return true;
}

}

log4cxx::LoggerPtr RuntimeService::mLogger(log4cxx::Logger::getLogger("RuntimeService"));

//////////////////////////////////////////////////////////////////////////
RuntimeService::RuntimeService(ProcessorRT* processor, std::vector<std::wstring>& args)
    : mProcessor(processor)
    , mVerboseMode(false)
    , args(args)
    , current(nullptr)
    , kernel(nullptr)
{
    boost::fill(next, nullptr);

    uint32 local_id = mProcessor->getId();

    BufferNetwork::instance()->declareReadableLocation<InvocationRequestBuffer>(local_id, buffer::buffer_location_t::host_unpinned, 0);
    BufferNetwork::instance()->declareWritableLocation<InvocationRequestBuffer>(local_id, buffer::buffer_location_t::host_unpinned, 0);

#ifdef BUILD_WITH_RDMA
    if(Configuration::instance()->enable_rdma)
    {
        BufferNetwork::instance()->declareReadableLocation<InvocationRequestBuffer>(local_id, buffer::buffer_location_t::host_ib_pinned, 1);
        BufferNetwork::instance()->declareWritableLocation<InvocationRequestBuffer>(local_id, buffer::buffer_location_t::host_ib_pinned, 1);
    }
#endif
#ifdef BUILD_WITH_CUDA
    if(Configuration::instance()->enable_cuda)
    {
        BufferNetwork::instance()->declareReadableLocation<InvocationRequestBuffer>(local_id, buffer::buffer_location_t::host_ptx_pinned, 2);
        BufferNetwork::instance()->declareWritableLocation<InvocationRequestBuffer>(local_id, buffer::buffer_location_t::host_ptx_pinned, 2);
    }
#endif
#ifdef BUILD_WITH_OPENCL
    if(Configuration::instance()->enable_opencl)
    {
        BufferNetwork::instance()->declareReadableLocation<InvocationRequestBuffer>(local_id, buffer::buffer_location_t::host_ocl_pinned, 3);
        BufferNetwork::instance()->declareWritableLocation<InvocationRequestBuffer>(local_id, buffer::buffer_location_t::host_ocl_pinned, 3);
    }
#endif
}

RuntimeService::~RuntimeService()
{
}

//////////////////////////////////////////////////////////////////////////
bool RuntimeService::isCompleted()
{
    return mDispatchedInvocations.empty();
}

bool RuntimeService::isInvocationPending()
{
    return !mDispatchedInvocations.empty();
}

//////////////////////////////////////////////////////////////////////////
void RuntimeService::initialize()
{
    shared_ptr<ExecutionITC> execution_itc = mProcessor->getExecutionITC();

    uint32 local_id = mProcessor->getId();

    // register KernelITC handlers
    execution_itc->registerHandler(
        KernelITC::processor_itc_invocation_request,
        boost::bind(
            &RuntimeService::handleProcessorInvocation, this, _1, _2
        )
    );

    // create thread-local next invocation buffer for each target
    for (InvocationRequestBuffer*& buffer : next)
    {
        InvocationRequestBuffer* newed_buffer = new InvocationRequestBuffer(local_id);
        BufferNetwork::instance()->create(local_id, *newed_buffer, InvocationRequestBuffer::Dimension(buffer::buffer_constants::max_invocations));

        buffer = newed_buffer;
    }
}

void RuntimeService::finalize()
{
    shared_ptr<ExecutionITC> execution_itc = mProcessor->getExecutionITC();
    uint32 local_id = mProcessor->getId();

    execution_itc->unregisterHandler(KernelITC::processor_itc_invocation_request);

    for (InvocationRequestBuffer*& buffer : next)
    {
        BufferNetwork::instance()->destroy(*buffer);
        delete buffer;
        buffer = nullptr;
    }
}

void RuntimeService::beforeInitializeOnKernel(shared_ptr<KernelBase> kernel)
{
    gRuntimeService = this;
}

void RuntimeService::afterInitializeOnKernel(shared_ptr<KernelBase> kernel)
{
    mVerboseMode = kernel->getVerbose();
}

void RuntimeService::beforeFinalizeOnKernel(shared_ptr<KernelBase> kernel)
{
    gRuntimeService = nullptr;
}

void RuntimeService::afterFinalizeOnKernel(shared_ptr<KernelBase> kernel)
{
//    kernel->set(Kernel::configuration::service_runtime_api_buffer, (void*)NULL);
}

const std::vector<std::wstring>& RuntimeService::getArguments() const
{
    return args;
}

void RuntimeService::setArguments(const std::vector<std::wstring>& new_args)
{
    args = new_args;
}

void RuntimeService::exit(int32 exit_code)
{
    mProcessor->exit(exit_code);
}

int32 RuntimeService::getCurrentInvocationId() const
{
    auto*const kernel = getKernel();

    return kernel->getCurrentThreadInvocationId();
}

Invocation* RuntimeService::getCurrentInvocation() const
{
    auto*const kernel = getKernel();

    return kernel->getCurrentInvocation();
}

InvocationRequestBuffer* RuntimeService::getCurrentInvocations() const
{
    return current;
}

InvocationRequestBuffer* RuntimeService::getCurrentThreadNextInvocations(uint32 target_id)
{
    using buffer::buffer_constants::max_targets;
    using buffer::buffer_constants::max_threads;

    auto*const kernel    = getKernel();

    const auto thread_id = kernel->getCurrentThreadId();
    const auto index     = thread_id * max_targets + target_id;

//    std::cerr << ">>>>>>>>>>>>>>> [giggle] getCurrentThreadNextInvocation(): thread_id  = " << thread_id << std::endl;

    BOOST_ASSERT(index < max_targets * max_threads);

    return next[index];
}

void RuntimeService::setCurrentInvocations(InvocationRequestBuffer* current_invocation_req)
{
    current = current_invocation_req;
}

void RuntimeService::postponeInvocationDependencyTo(int32 target_id, int32 postpone_to_id)
{

    auto*const        next_invocation_buffer = getCurrentThreadNextInvocations(target_id);
    auto*const postpone_to_invocation        = next_invocation_buffer->getInvocation(postpone_to_id);
    auto*const     current_invocation        = getCurrentInvocation();

    postpone_to_invocation->be_depended_counter = current_invocation->be_depended_counter;
        current_invocation->be_depended_counter = nullptr;

    postpone_to_invocation->store_to = current_invocation->store_to;
        current_invocation->store_to = nullptr;
}

void RuntimeService::setInvocationDependOnCount(int32 target_id, int32 invocation_id, int32 count)
{
    auto*const next_invocation_buffer = getCurrentThreadNextInvocations(target_id);
    auto*const next_invocation        = next_invocation_buffer->getInvocation(invocation_id);

    BOOST_ASSERT(next_invocation->depend_on_counter == nullptr && "already be depended!");

    next_invocation->depend_on_counter = new std::atomic<int32>(count);
}

void RuntimeService::setInvocationDependOn(int32 target_id, int32 invocation_id, int32 depend_on_id)
{
    auto*const      next_invocation_buffer = getCurrentThreadNextInvocations(target_id);
    auto*const   current_invocation        = next_invocation_buffer->getInvocation(invocation_id);
    auto*const depend_on_invocation        = next_invocation_buffer->getInvocation(depend_on_id);

    BOOST_ASSERT(  current_invocation->depend_on_counter   != nullptr && "no counter on current invocation");
    BOOST_ASSERT(depend_on_invocation->be_depended_counter == nullptr && "depend on invocation already be depended");

    depend_on_invocation->be_depended_counter = current_invocation->depend_on_counter;
}

void RuntimeService::delayInvocation(Invocation& invocation)
{
    BOOST_ASSERT(!invocation.remote && "remote invocation should not be delayed!");

    auto*const next_invocation_buffer = getCurrentThreadNextInvocations(ProcessorId::PROCESSOR_PRINCIPLE);
    auto*const next_invocation        = next_invocation_buffer->reserveInvocation(invocation.session_id, invocation.function_id);

    next_invocation->depend_on_counter   = invocation.depend_on_counter  ;
    next_invocation->be_depended_counter = invocation.be_depended_counter;
    next_invocation->store_to            = invocation.store_to           ;
    next_invocation->buffer              = invocation.buffer             ;

    invocation.valid               = 0;
    invocation.depend_on_counter   = nullptr;
    invocation.be_depended_counter = nullptr;
    invocation.store_to            = nullptr;
}

int32 RuntimeService::addInvocation(int32 target_id, int64 session_id, int64 function_id, int8* store_to)
{
          auto*const next_invocation_buffer = getCurrentThreadNextInvocations(target_id);
    const auto       next_invocation_id     = next_invocation_buffer->reserveInvocationId(session_id, function_id);
          auto*const next_invocation        = next_invocation_buffer->getReservedInvocation(next_invocation_id);

    next_invocation->valid    = 1;
    next_invocation->store_to = store_to;

    return next_invocation_id;
}

void RuntimeService::addInvocation(int32 target_id, int64 session_id, int64 adaptor_id, ReplicationData* data)
{
    BOOST_ASSERT(data && "null pointer exception");

    auto*const next_invocation_buffer = getCurrentThreadNextInvocations(target_id);
    auto*const next_invocation        = next_invocation_buffer->reserveInvocation(session_id, language::system_function::remote_invocation_id);

    next_invocation->remote                        = 1;
    next_invocation->buffer.remote_info.data       = data;
    next_invocation->buffer.remote_info.adaptor_id = adaptor_id;
}

int32 RuntimeService::reserveInvocation(int32 target_id, int64 session_id, int64 function_id)
{
          auto*const next_invocation_buffer = getCurrentThreadNextInvocations(target_id);
    const auto       next_invocation_id     = next_invocation_buffer->reserveInvocationId(session_id, function_id);

    return next_invocation_id;
}

bool RuntimeService::commitInvocation(int32 target_id, int32 id)
{
    auto*const next_invocation_buffer = getCurrentThreadNextInvocations(target_id);
    auto*const next_invocation        = next_invocation_buffer->getReservedInvocation(id);

    next_invocation->valid = 1;

    return true;
}

bool RuntimeService::abortInvocation(int32 target_id, int32 id)
{
    // 'valid' bit is set to '0' during reservation
    // just do nothing and should work

    return true;
}

void RuntimeService::setInvocationReturnPtr(int32 target_id, int32 id, void* ptr)
{
    auto*const next_invocation_buffer = getCurrentThreadNextInvocations(target_id);
    auto*const next_invocation        = next_invocation_buffer->getInvocation(id);

    next_invocation->store_to = ptr;
}

template<typename Type>
int64 RuntimeService::appendInvocationParameter(int32 target_id, int32 id, int64 offset, const Type& value)
{
    auto*const next_invocation_buffer = getCurrentThreadNextInvocations(target_id);
    auto*const next_invocation        = next_invocation_buffer->getReservedInvocation(id);

    constexpr auto   max_size = sizeof(next_invocation->buffer.parameters);
    constexpr auto value_size = sizeof(value);

    static_assert(max_size   <= std::numeric_limits<decltype(offset)>::max(), "how large each buffer accepts, it's over INT64_MAX now !?");
    static_assert(value_size <= 8                                           , "we need support such large value type ?");
    static_assert(value_size <  max_size                                    , "Oops! the buffer size is so small... Please re-design the parameter buffer");

    if (offset > max_size - value_size)
        return -1;

    std::memcpy(next_invocation->buffer.parameters + offset, &value, value_size);

    return static_cast<int64>(offset + value_size);
}

auto RuntimeService::getRemoteAdaptor(int64 adaptor_id) -> RemoteAdaptorSignature*
{
    auto*const kernel = getKernel();

    BOOST_ASSERT(kernel != nullptr && "no kernel!");

    auto* function_decl = kernel->queryFunctionDecl(adaptor_id);

    if (function_decl == nullptr)
        return nullptr;

    BOOST_ASSERT(function_decl && "null pointer exception");

    // validate function's signature
    const bool valid_signature = isExpectedSignature(
        *function_decl,
        language::tree::PrimitiveKind::VOID_TYPE,
        std::vector<std::wstring>({L"thor", L"lang", L"ReplicationDecoder"})
    );

    if (!valid_signature)
        return nullptr;

    // get callable instance of function
    const auto*const context = language::NameManglingContext::get(function_decl);

    if (context == nullptr)
       return nullptr;

    return kernel->queryCallable<RemoteAdaptorSignature>(context->mangled_name);
}

auto RuntimeService::getCreator(int64 type_id) -> CreatorSignature*
{
    auto*const kernel = getKernel();

    BOOST_ASSERT(kernel != nullptr && "no kernel!");

    return getSpecialFunctionImpl<CreatorSignature, zillians::language::ReplicatorCreatorContext>(*kernel, type_id);
}

auto RuntimeService::getSerializer(int64 type_id) -> SerializerSignature*
{
    auto*const kernel = getKernel();

    BOOST_ASSERT(kernel != nullptr && "no kernel!");

    return getSpecialFunctionImpl<SerializerSignature, zillians::language::ReplicatorSerializerContext>(*kernel, type_id);
}

auto RuntimeService::getDeserializer(int64 type_id) -> DeserializerSignature*
{
    auto*const kernel = getKernel();

    BOOST_ASSERT(kernel != nullptr && "no kernel!");

    return getSpecialFunctionImpl<DeserializerSignature, zillians::language::ReplicatorDeserializerContext>(*kernel, type_id);
}

const std::wstring* RuntimeService::queryStringLiteral(int64 symbol_id) const
{
    auto*const kernel = getKernel();

    BOOST_ASSERT(kernel != nullptr && "no kernel!");

    return kernel->queryStringLiteral(symbol_id);
}

processor::mt::Kernel* RuntimeService::getKernel() const
{
    const auto&    kernel = mProcessor->getKernel();
          auto* mt_kernel = static_cast<processor::mt::Kernel*>(kernel.get());

    return mt_kernel;
}

//////////////////////////////////////////////////////////////////////////
void RuntimeService::handleEvent(events::type event)
{
    //std::cout << "[giggle] begin handleEvent tid = " << boost::this_thread::get_id() << std::endl;

    uint32 local_id = mProcessor->getId();
    shared_ptr<ExecutionMonitor> monitor = mProcessor->getMonitor();

    BOOST_ASSERT(!!monitor && "ExecutionMonitor should be set prior to any execution event");

    // notify the execution monitor about the status of current execution
    // the default execution state is incomplete
    static bool once_init_mark_incomplete = false;
    if(!once_init_mark_incomplete)
    {
        monitor->markIncompleted(local_id);
        once_init_mark_incomplete = true;
    }

    switch(event)
    {
    case events::before_invocation_dispatch:
    {
        if(!mDispatchedInvocations.empty())
        {
            invocation_entry_t& t = mDispatchedInvocations.front();

            current = t.second.getInvocationRequest().buffer;
        }
        break;
    }
    case events::after_invocation_dispatch:
    {
        {
            invocation_entry_t& t = mDispatchedInvocations.front();

            if(t.second.continuation_id != 0)
            {
                // send invocation response back
                auto invocation_response_itc = KernelITC::make<KernelITC::InvocationResponse>(t.second.continuation_id, service::InvocationResponseBuffer{0, true});

                mProcessor->sendITC(t.first, invocation_response_itc);
            }

            BufferNetwork::instance()->destroy(*t.second.getInvocationRequest().buffer);
            mDispatchedInvocations.pop_front();
        }

        dispatchNextInvocations();
        dispatchObjectClearInvocations();

        break;
    }
    case events::idletime_heartbeat:
    {
        dispatchObjectClearInvocations();
        break;
    }
    default:
        break;
    }

//    std::cout << "[giggle] end handleEvent tid = " << boost::this_thread::get_id() << std::endl;
}

void RuntimeService::dispatchObjectClearInvocations()
{
    // ask object service if we are allowed to get to-recycle objects
    const auto& object_service = static_pointer_cast<ObjectService>(mProcessor->getService(ServiceId::SERVICE_OBJECT));
    if (!object_service->isGCInitialized()) return;
    if (object_service->getRecycleObjects().size() == 0) return;

    // create invocation buffer to call to recycle __destroyObject
    auto recycle_objects = object_service->getRecycleObjects();

    // create invocation buffer
    uint32 local_id = mProcessor->getId();
    InvocationRequestBuffer* buffer = new InvocationRequestBuffer(local_id);
    BufferNetwork::instance()->create(local_id, *buffer, InvocationRequestBuffer::Dimension(recycle_objects.size()));
    buffer->setInvocationCount(1);

    typedef std::remove_reference<decltype(recycle_objects.front())>::type object_type_t;

    // Note that the memory destruction is performed in __destroyObjectInternal in system bundle
    auto objects = new object_type_t[recycle_objects.size()];

    for(int i = 0; i < recycle_objects.size(); i++)
    {
        // well, insert object pointer
        objects[i] = recycle_objects[i];
    }
    buffer->setFunctionId(0, language::system_function::object_destruction_id);
    buffer->setSessionId(0, -1);

    auto parameter_ptr = buffer->getInvocationPtr(0);
    *((int32*)parameter_ptr) = recycle_objects.size();
    parameter_ptr += sizeof(int32);
    *((object_type_t**)parameter_ptr) = objects;

    // clear the recycle list, so that the gc service will continue to work
    object_service->resetRecycleObjects();

    // send itc
    auto invoke_itc = KernelITC::make<KernelITC::InvocationRequest>(0, 0, buffer);

    shared_ptr<ExecutionITC> execution_itc = mProcessor->getExecutionITC();
    execution_itc->dispatch(ProcessorId::PROCESSOR_PRINCIPLE, invoke_itc);
}

void RuntimeService::dispatchNextInvocations()
{
    shared_ptr<KernelBase> kernel = mProcessor->getKernel();
    if(!!kernel)
    {
        uint32 local_id = mProcessor->getId();
        shared_ptr<ExecutionITC> execution_itc = mProcessor->getExecutionITC();

        // dispatch the generated invocations
        for(uint32 i = 0; i <buffer::buffer_constants::max_threads; ++i)
        {
            for(uint32 j = 0; j < buffer::buffer_constants::max_targets; ++j)
            {
                uint32 index = i * buffer::buffer_constants::max_targets + j;
                InvocationRequestBuffer* next_invocation_buffer = next[index];
                if(next_invocation_buffer->getInvocationCount() > 0)
                {
                    bool      is_success = true;
                    KernelITC itc        = KernelITC::make<KernelITC::InvocationRequest>(0, 0, nullptr);

                    auto*const instance = BufferNetwork::instance();

                    if (instance->checkCompatibleLocation<InvocationRequestBuffer>(local_id, j, next_invocation_buffer->location))
                    {
                        InvocationRequestBuffer* new_buffer = new InvocationRequestBuffer(local_id);
                        instance->create(local_id, *new_buffer, buffer::buffer_constants::max_invocations);

                        BOOST_ASSERT(index < buffer::buffer_constants::max_targets * buffer::buffer_constants::max_threads);

                        next_invocation_buffer->owner     = j;
                        itc.getInvocationRequest().buffer = next_invocation_buffer;
                        next[index]                       = new_buffer;
                        is_success                        = true;
                    }
                    else
                    {
                        InvocationRequestBuffer* new_buffer = new InvocationRequestBuffer(j);

                        const bool is_created = instance->create<InvocationRequestBuffer>(j, *new_buffer, next_invocation_buffer->declared_dim);
                                   is_success = is_created && instance->copy(*next_invocation_buffer, *new_buffer);

                        if (is_success)
                        {
                            itc.getInvocationRequest().buffer = new_buffer;
                            next_invocation_buffer->setInvocationCount(0);
                        }
                        else
                        {
                            if (is_created)
                                instance->destroy(*new_buffer);

                            delete new_buffer;
                        }
                    }

                    if (is_success)
                        execution_itc->dispatch(j, itc);
                    else
                        std::cerr << "Error: failed to move invocation buffer" << std::endl;
                }
            }
        }
    }
}

void RuntimeService::handleProcessorInvocation(uint32 source, KernelITC& itc)
{
    BOOST_ASSERT(itc.isInvocationRequest());

    if(mVerboseMode)
        std::cerr << "handleProcessorInvocation: invocation count=" << itc.getInvocationRequest().buffer->getInvocationCount() << std::endl;

    if(itc.getInvocationRequest().buffer->getInvocationCount() > 0)
    {
        invocation_entry_t p(std::move(source), std::move(itc));
        mDispatchedInvocations.push_back(std::move(p));
    }
}

} } } }
