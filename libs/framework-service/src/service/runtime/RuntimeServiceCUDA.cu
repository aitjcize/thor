/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#undef _GLIBCXX_ATOMIC_BUILTINS
#undef _GLIBCXX_USE_INT128

#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/sort.h>
#include <thrust/sequence.h>
#include <stdio.h>

#include "core/IntTypes.h"
#include "framework/service/Common.h"
#include "framework/service/runtime/RuntimeServiceBuffer.h"

using namespace zillians::framework::buffer;

namespace zillians { namespace framework { namespace service { namespace cuda { namespace detail {

__global__ void __extractFunctionIdsKernel(char* invocation_buffer_ptr, int64* function_ids_buffer_ptr, int32 invocation_count)
{
	int32 id = blockIdx.x * blockDim.x + threadIdx.x;
	if(id < invocation_count)
	{
		function_ids_buffer_ptr[id] = *(int64*)(invocation_buffer_ptr + Invocation::PAYLOAD * id);
	}
}

void __extractFunctionIds(char* invocation_buffer_ptr, int64* function_ids_buffer_ptr, int32 invocation_count)
{
	int32 threads = 512;
	int32 blocks = ceiling<int32>(invocation_count, threads);
	__extractFunctionIdsKernel<<<blocks, threads>>>(invocation_buffer_ptr, function_ids_buffer_ptr, invocation_count);
}

void __updateShuffleIndice(int64* function_ids_buffer_ptr, int32* shuffle_indices_buffer_ptr, int32 invocation_count)
{
	thrust::device_ptr<int32_t> shuffle_indices = thrust::device_pointer_cast(shuffle_indices_buffer_ptr);
	thrust::sequence(shuffle_indices, shuffle_indices + invocation_count);

	thrust::device_ptr<int64_t> function_ids = thrust::device_pointer_cast(function_ids_buffer_ptr);
	thrust::sort_by_key(function_ids, function_ids + invocation_count, shuffle_indices);
}

} } } } }
