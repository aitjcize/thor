/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cstddef>

#include <algorithm>
#include <functional>
#include <string>
#include <tuple>
#include <vector>

#include <boost/preprocessor/list/adt.hpp>
#include <boost/preprocessor/list/at.hpp>
#include <boost/preprocessor/list/enum.hpp>
#include <boost/preprocessor/list/rest_n.hpp>
#include <boost/preprocessor/list/transform.hpp>
#include <boost/preprocessor/seq/elem.hpp>

#include <tbb/concurrent_hash_map.h>

#include "framework/buffer/BufferKernel.h"
#include "framework/buffer/BufferManager.h"
#include "framework/buffer/BufferNetwork.h"
#include "framework/buffer/support/BufferCopier.h"
#include "framework/processor/KernelMT.h"
#include "framework/service/runtime/RuntimeServiceMT.h"
#include "framework/service/runtime/RuntimeServiceBuffer.h"

using namespace zillians::framework::buffer;

namespace zillians { namespace framework { namespace service {

namespace detail {

#ifdef BUILD_WITH_CUDA
extern void __cudaResetInvocationBuffer(Invocation* buffer, int32 count);
#endif

#ifdef BUILD_WITH_OPENCL
extern void __oclResetInvocationBuffer(Invocation* buffer, int32 count);
#endif

}

void Invocation::cleanup()
{
    if (remote)
    {
        SAFE_DELETE(buffer.remote_info.data);

        remote = 0;
    }
}

const std::size_t ReplicationData::size_per_block = 128;

ReplicationData::ReplicationData(buffer_location_t location) noexcept
    : location(location)
    , first_block(nullptr)
    , info_read{nullptr, 0, 0}
    , info_write{nullptr, 0, 0}
{
}

ReplicationData::ReplicationData(ReplicationData&& ref) noexcept
    : ReplicationData(buffer_location_t::unknown)
{
    *this = std::move(ref);
}

ReplicationData::~ReplicationData()
{
    cleanup();
}

ReplicationData& ReplicationData::operator=(ReplicationData&& ref) noexcept
{
    cleanup();

    location       = ref.location;
    first_block    = ref.first_block;
    info_read      = ref.info_read;
    info_write     = ref.info_write;

    ref.location         = buffer_location_t::unknown;
    ref.first_block      = nullptr;
    ref.info_read .block = nullptr;
    ref.info_write.block = nullptr;

    return *this;
}

bool ReplicationData::reach_end_of_data() const
{
    return info_read.remain_size == 0 && info_read.block == nullptr;
}

std::size_t ReplicationData::read(byte* buffer, std::size_t count)
{
    std::size_t read_count = 0;

    while (read_count != count)
    {
        const std::size_t current_read_count = readsome(buffer + read_count, count - read_count);

        if (current_read_count == 0)
            break;

        read_count += current_read_count;
    }

    return read_count;
}

std::size_t ReplicationData::readsome(byte* buffer, std::size_t count)
{
    if (info_read.block == nullptr)
        return 0u;

    static_assert(size_per_block > offsetof(DataBlock, buffer), "offset of member is not smaller than block size");
    BOOST_ASSERT(size_per_block - offsetof(DataBlock, buffer) >= info_read.buffer_offset && "read out-of-range");

    const byte*const  offset_buffer      = info_read.block->buffer + info_read.buffer_offset;
    const std::size_t buffer_remain_size = size_per_block - (info_read.buffer_offset + offsetof(DataBlock, buffer));
    const std::size_t read_size          = std::min({buffer_remain_size, count, info_read.remain_size});

    memcpy_func_table[location][location](
        buffer,
        offset_buffer,
        read_size
    );

    info_read.buffer_offset += read_size;
    info_read.remain_size   -= read_size;

    if (info_read.remain_size == 0)
    {
        info_read.block = nullptr;
    }
    else if (info_read.buffer_offset == size_per_block - offsetof(DataBlock, buffer))
    {
        info_read.block         = info_read.block->next;
        info_read.buffer_offset = 0;
    }

    return read_size;
}

std::size_t ReplicationData::peek(byte* buffer, std::size_t count)
{
    std::size_t peek_count = 0;

    while (peek_count != count)
    {
        const std::size_t current_peek_count = peeksome(buffer + peek_count, count - peek_count);

        if (current_peek_count == 0)
            break;

        peek_count += current_peek_count;
    }

    return peek_count;
}

std::size_t ReplicationData::peeksome(byte* buffer, std::size_t count)
{
    if (info_read.block == nullptr)
        return 0u;

    static_assert(size_per_block > offsetof(DataBlock, buffer), "offset of member is not smaller than block size");
    BOOST_ASSERT(size_per_block - offsetof(DataBlock, buffer) >= info_read.buffer_offset && "read out-of-range");

    const byte*const  offset_buffer      = info_read.block->buffer + info_read.buffer_offset;
    const std::size_t buffer_remain_size = size_per_block - (info_read.buffer_offset + offsetof(DataBlock, buffer));
    const std::size_t peek_size          = std::min({buffer_remain_size, count, info_read.remain_size});

    memcpy_func_table[location][location](
        buffer,
        offset_buffer,
        peek_size
    );

    return peek_size;
}

std::size_t ReplicationData::write(const byte* data, std::size_t count)
{
    std::size_t write_count = 0;

    while (write_count != count)
    {
        const std::size_t current_write_count = writesome(data + write_count, count - write_count);

        if (current_write_count == 0)
            break;

        write_count += current_write_count;
    }

    return write_count;
}

std::size_t ReplicationData::writesome(const byte* data, std::size_t count)
{
    BOOST_ASSERT(
        (
            (first_block == nullptr && info_write.block == nullptr) ||
            (first_block != nullptr && info_write.block != nullptr)
        ) && "incorrect pointers to data block"
    );

    if (count == 0)
        return 0u;

    static_assert(size_per_block > offsetof(DataBlock, buffer), "offset of member is not smaller than block size");
    BOOST_ASSERT(size_per_block - offsetof(DataBlock, buffer) >= info_write.buffer_offset && "read out-of-range");

    if (info_write.block == nullptr || info_write.buffer_offset + offsetof(DataBlock, buffer) == size_per_block)
    {
        DataBlock* new_block = nullptr;

        if (!BufferManager::instance()->allocate(location, size_per_block, reinterpret_cast<byte**>(&new_block)))
            return 0u;

        if (info_write.block != nullptr)
            info_write.block->next = new_block;

        info_write.block         = new_block;
        info_write.block->next   = nullptr;
        info_write.buffer_offset = 0u;

        if (first_block == nullptr)
            first_block = new_block;
    }

    byte*const        offset_buffer      = info_write.block->buffer + info_write.buffer_offset;
    const std::size_t buffer_remain_size = size_per_block - (info_write.buffer_offset + offsetof(DataBlock, buffer));
    const std::size_t write_size         = std::min(buffer_remain_size, count);

    memcpy_func_table[location][location](
        offset_buffer,
        data,
        write_size
    );

    info_write.buffer_offset += write_size;
    info_write.written_size  += write_size;

    return write_size;
}

void ReplicationData::resetRead() noexcept
{
    info_read.block         = first_block;
    info_read.buffer_offset = 0;
    info_read.remain_size   = info_write.written_size;
}

std::size_t ReplicationData::getSize() const noexcept
{
    return info_write.written_size;
}

void ReplicationData::cleanup() noexcept
{
    for (DataBlock* next_block = nullptr; first_block != nullptr; first_block = next_block)
    {
        next_block = first_block->next;

        BufferManager::instance()->deallocate(location, reinterpret_cast<byte*>(first_block));
    }

    info_read .block = nullptr;
    info_write.block = nullptr;
}

#define _internal  ((Invocation*)data)

InvocationRequestBuffer::InvocationRequestBuffer(uint32 processor_id) :
        execution_mode(false),
        response_seq(-1),
        current_size(0),
        allocated_size(0),
        execution_context(NULL)
{ }

InvocationRequestBuffer::~InvocationRequestBuffer()
{
}

bool InvocationRequestBuffer::construct(buffer_location_t::type location, const Dimension& dim)
{
    // make sure the size of each invocation buffer equals to the pre-defined static size
    // (note that the fixed size invocation buffer makes it easier to handle in Infiniband protocol)
    BOOST_STATIC_ASSERT((sizeof(Invocation) == Invocation::PAYLOAD));

    this->location = location;
    execution_mode = false;
    response_seq = -1;
    current_size = 0;
    allocated_size = dim.number_of_invocations;

    if(!BufferManager::instance()->allocate(location, sizeof(Invocation) * allocated_size, (byte**)&data))
        return false;

    if(buffer_location_t::is_accessible_from_host(location))
    {
        for (int32 i = 0; i < allocated_size; i++)
            _internal[i].reset();
    }
    else
    {
#ifdef BUILD_WITH_CUDA
        if(buffer_location_t::is_accessible_from_cuda_device(location))
        {
            detail::__cudaResetInvocationBuffer(_internal, allocated_size);
        }
#endif
#ifdef BUILD_WITH_OPENCL
        if(buffer_location_t::is_accessible_from_ocl_device(location))
        {
            detail::__oclResetInvocationBuffer(_internal, allocated_size);
        }
#endif
    }

    declared_dim = dim;

    return true;
}

bool InvocationRequestBuffer::destruct()
{
    if(!data) return false;

    // TODO clean up buffer for CUDA device memory
    // currently this code only work for CPU, not GPU,
    // we comment it temporary.

    //std::for_each(
    //    _internal,
    //    _internal + current_size,
    //    std::mem_fn(&Invocation::cleanup)
    //);

    BufferManager::instance()->deallocate(location, (byte*)data);
    location = buffer_location_t::unknown;
    size = 0;
    owner = -1;
    data = NULL;

    execution_mode = false;
    response_seq = -1;
    current_size = 0;
    allocated_size = 0;

    return true;
}

bool InvocationRequestBuffer::copyTo(InvocationRequestBuffer& target_buffer)
{
    if(!memcpy_func_table[target_buffer.location][location](target_buffer.data, data, Invocation::PAYLOAD * current_size))
        return false;

    target_buffer.execution_mode = execution_mode;
    target_buffer.response_seq = response_seq;
    target_buffer.current_size = current_size;

    return true;
}

bool InvocationRequestBuffer::moveTo(InvocationRequestBuffer& target_buffer)
{
#define MOVE_VALUE(name, reset_value)         \
    target_buffer.name = name;                \
                  name = reset_value

    MOVE_VALUE(location         , buffer_location_t::unknown);
    MOVE_VALUE(size             , 0                         );
    MOVE_VALUE(owner            , -1                        );
    MOVE_VALUE(data             , NULL                      );
    MOVE_VALUE(declared_dim     , Dimension()               );

    MOVE_VALUE(execution_mode   , false                     );
    MOVE_VALUE(response_seq     , -1                        );
    MOVE_VALUE(current_size     , 0                         );
    MOVE_VALUE(allocated_size   , 0                         );
    MOVE_VALUE(execution_context, NULL                      );

#undef MOVE_VALUE

    return true;
}

void InvocationRequestBuffer::setExecutionMode(bool sync_or_async)
{
    execution_mode = sync_or_async;
}

bool InvocationRequestBuffer::getExecutionMode()
{
    return execution_mode;
}

int64 InvocationRequestBuffer::getResponseSequence() const
{
    return response_seq;
}

void InvocationRequestBuffer::setResponseSequence(int64 seq)
{
    response_seq = seq;
}

uint32 InvocationRequestBuffer::getInvocationCount() const
{
    return current_size;
}

void InvocationRequestBuffer::setInvocationCount(uint32 count)
{
    BOOST_ASSERT(count <= allocated_size);
    current_size = count;
}

bool InvocationRequestBuffer::isActivated(int32 index) const
{
    BOOST_ASSERT(index < current_size);
    BOOST_ASSERT(isValid(index));
    return (_internal[index].activated != 0);
}

void InvocationRequestBuffer::setActivated(int32 index)
{
    BOOST_ASSERT(index < current_size);
    BOOST_ASSERT(isValid(index));
    _internal[index].activated = 1;
}

bool InvocationRequestBuffer::isValid(int32 index) const
{
    if(buffer_location_t::is_accessible_from_host(location))
    {
        BOOST_ASSERT(index < current_size);
        return (_internal[index].valid == 1);
    }
    else
    {
        // TODO temporary work-around
        return true;
    }
}

void InvocationRequestBuffer::setInvalid(int32 index)
{
    BOOST_ASSERT(index < current_size);
    _internal[index].valid = 0;
}

bool InvocationRequestBuffer::isRemote(int32 index) const
{
    if(buffer_location_t::is_accessible_from_host(location))
    {
        BOOST_ASSERT(index < current_size);
        return _internal[index].remote != 0;
    }
    else
    {
        // TODO handle other cases
        return false;
    }
}

void InvocationRequestBuffer::setRemote(int32 index)
{
    BOOST_ASSERT(index < current_size);
    _internal[index].remote = 1;
}

Invocation* InvocationRequestBuffer::getInvocation(int32 index)
{
    BOOST_ASSERT(isValid(index));
    return &_internal[index];
}

Invocation* InvocationRequestBuffer::getReservedInvocation(int32 index)
{
    BOOST_ASSERT(!isValid(index));
    return &_internal[index];
}

Invocation* InvocationRequestBuffer::reserveInvocation(int64 session_id, int64 function_id)
{
    const int32      invocation_id = getInvocationCount();
    Invocation*const invocation    = &_internal[invocation_id];

    setInvocationCount(invocation_id + 1);

    invocation-> session_id =  session_id;
    invocation->function_id = function_id;

    return invocation;
}

int32 InvocationRequestBuffer::reserveInvocationId(int64 session_id, int64 function_id)
{
    const int32      invocation_id = getInvocationCount();
    Invocation*const invocation    = &_internal[invocation_id];

    setInvocationCount(invocation_id + 1);

    invocation->      valid =           0;
    invocation-> session_id =  session_id;
    invocation->function_id = function_id;

    return invocation_id;
}

void InvocationRequestBuffer::insertInvocationAtFront(const std::vector<Invocation*>& invocations)
{
    BOOST_ASSERT(invocations.size() + current_size < allocated_size);

    std::copy_backward(
        _internal,
        _internal + current_size,
        _internal + current_size + invocations.size()
    );

    std::transform(
        invocations.cbegin(),
        invocations.cend(),
        _internal,
        [](Invocation* invocation)
        {
            Invocation new_invocation = *invocation;

            invocation->valid = 0;
            invocation->remote = 0;

            return new_invocation;
        }
    );

    setInvocationCount(invocations.size() + current_size);
}

int64 InvocationRequestBuffer::getFunctionId(int32 index) const
{
    BOOST_ASSERT(isValid(index));
    BOOST_ASSERT(index < current_size);
    return _internal[index].function_id;
}

void InvocationRequestBuffer::setFunctionId(int32 index, int64 function_id)
{
    BOOST_ASSERT(index < allocated_size);
    _internal[index].function_id = function_id;
}

int64 InvocationRequestBuffer::getSessionId(int32 index) const
{
    BOOST_ASSERT(isValid(index));
    BOOST_ASSERT(index < current_size);
    return _internal[index].session_id;
}

void InvocationRequestBuffer::setSessionId(int32 index, int64 session_id)
{
    BOOST_ASSERT(index < allocated_size);
    _internal[index].session_id = session_id;
}

byte* InvocationRequestBuffer::getInvocationRawPtr(int32 index)
{
    BOOST_ASSERT(index < allocated_size);
    return (byte*)(&_internal[index]);
}

byte* InvocationRequestBuffer::getInvocationPtr(int32 index)
{
    BOOST_ASSERT(index < allocated_size);
    return (byte*)(&_internal[index].buffer);
}

void* InvocationRequestBuffer::getExecutionContext()
{
    return execution_context;
}

void InvocationRequestBuffer::setExecutionContext(void* ctx)
{
    execution_context = ctx;
}

#undef _internal

} } }
