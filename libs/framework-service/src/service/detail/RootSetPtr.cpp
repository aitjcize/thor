/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/service/detail/RootSetPtr.h"
#include "framework/service/object/ObjectServiceBuffer.h"

namespace thor { namespace lang {

class Object;

} }  // namespace thor::lang

namespace zillians { namespace framework { namespace service { namespace mt {

namespace mt_proxies = zillians::framework::service::mt::proxies;

void root_set_ptr_base::rootSetAdd(::thor::lang::Object* ptr)
{
    if (ptr == nullptr)
        return;

    mt_proxies::object::addToObjectRootSet(reinterpret_cast<int8*>(ptr));
}

void root_set_ptr_base::rootSetRemove(::thor::lang::Object* ptr)
{
    if (ptr == nullptr)
        return;

    mt_proxies::object::removeFromObjectRootSet(reinterpret_cast<int8*>(ptr));
}

} } } }
