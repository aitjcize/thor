/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <memory>
#include <string>
#include <type_traits>

#include <boost/optional/optional.hpp>

#include "core/IntTypes.h"

#include "utility/UUIDUtil.h"
#include "utility/UnicodeUtil.h"

#include "framework/ExecutionITC.h"
#include "framework/Kernel.h"
#include "framework/Processor.h"
#include "framework/ProcessorId.h"
#include "framework/service/detail/ProxyHelper.h"
#include "framework/service/detail/RootSetPtr.h"
#include "framework/service/domain/DomainServiceMT.h"
#include "framework/service/runtime/RuntimeServiceBuffer.h"

namespace zillians { namespace framework { namespace service { namespace mt {

using Domain             = DomainService::Domain;
using DomainConnCallback = DomainService::DomainConnCallback;
using DomainErrCallback  = DomainService::DomainErrCallback;

ZILLIANS_FRAMEWORK_SERVICE_PROXY_DEF(
    DomainService,
    domain       ,

    ((Domain*)(find  )((int64  , session_id))                       )
    ((Domain*)(insert)((int64  , session_id))((Domain*, d         )))

    ((UUID)(listen )((const std::wstring&, endpoint))((DomainConnCallback*, conn_cb))((DomainConnCallback*, disconn_cb))((DomainErrCallback*, err_cb)))
    ((UUID)(connect)((const std::wstring&, endpoint))((DomainConnCallback*, conn_cb))((DomainConnCallback*, disconn_cb))((DomainErrCallback*, err_cb)))
    ((bool)(cancel )((const UUID&        , id      ))                                                                                                 )
)

namespace {

constexpr const char* DOMAIN_CALLBACK_NAME_ERROR = "_ZN4thor4lang6Domain7__onErrEv";
constexpr const char* DOMAIN_CALLBACK_NAME_CONN  = "_ZN4thor4lang6Domain8__onConnEv";

template<typename TbbMapType>
boost::optional<typename TbbMapType::mapped_type> query(const TbbMapType& map, const typename TbbMapType::key_type& key)
{
    typename TbbMapType::const_accessor const_accessor;

    if (!map.find(const_accessor, key))
        return boost::none;

    return const_accessor->second;
}

template<typename TbbMapType, typename ConditionType>
boost::optional<typename TbbMapType::mapped_type> queryThenDropIf(TbbMapType& map, const typename TbbMapType::key_type& key, ConditionType&& condition)
{
    boost::optional<typename TbbMapType::mapped_type> callbacks;
    typename TbbMapType::accessor                     accessor;

    if (map.find(accessor, key))
    {
        callbacks = std::move(accessor->second);

        if (condition(*callbacks))
            map.erase(accessor);
    }

    return callbacks;
}

template<typename TbbMapType>
boost::optional<typename TbbMapType::mapped_type> popOrEmpty(TbbMapType& map, const typename TbbMapType::key_type& key)
{
    boost::optional<typename TbbMapType::mapped_type> callbacks;
    typename TbbMapType::accessor                     accessor;

    if (map.find(accessor, key))
    {
        callbacks = std::move(accessor->second);

        map.erase(accessor);
    }

    return callbacks;
}

template<typename T>
using is_object = std::integral_constant<
                      bool,
                      std::is_same   < ::thor::lang::Object, T>::value ||
                      std::is_base_of< ::thor::lang::Object, T>::value
                  >;

template<typename CurrentType>
void addRootSetImpl(CurrentType& current, typename std::enable_if< is_object<typename std::remove_pointer<CurrentType>::type>::value>::type * = nullptr)
{
    root_set_ptr_base::rootSetAdd(current);
}

template<typename CurrentType>
void addRootSetImpl(CurrentType& current, typename std::enable_if<!is_object<typename std::remove_pointer<CurrentType>::type>::value>::type * = nullptr)
{
}

template<std::size_t offset>
std::size_t appendParameters(Invocation& invocation)
{
    return offset;
}

template<std::size_t offset, typename CurrentType, typename ...RemainTypes>
std::size_t appendParameters(Invocation& invocation, CurrentType&& current, RemainTypes&&... remains)
{
    using                 RealCurrentType = typename std::remove_reference<CurrentType>::type;
    constexpr std::size_t RealCurrentSize = sizeof(RealCurrentType);

    static_assert(std::is_trivial<RealCurrentType>::value, "you cannot insert non-standard-layout value into invocation");

    addRootSetImpl(current);

    auto*const offset_ptr = invocation.buffer.parameters + offset;

    *reinterpret_cast<typename std::remove_cv<RealCurrentType>::type*>(offset_ptr) = std::forward<CurrentType>(current);

    return appendParameters<offset + RealCurrentSize>(invocation, std::forward<RemainTypes>(remains)...);
}

}

bool DomainService::DomainCallbacks::isByListen () const noexcept { return by == BY_LISTEN ; }
bool DomainService::DomainCallbacks::isByConnect() const noexcept { return by == BY_CONNECT; }

DomainService::DomainService(ProcessorRT* processor)
    : verbose(false)
    , processor(processor)
    , domains()
    , callback_mapping()
{
}

bool DomainService::isCompleted()
{
    return true;
}

bool DomainService::isInvocationPending()
{
    return false;
}

void DomainService::initialize()
{
    const auto& execution_itc = processor->getExecutionITC();

    const auto& handler_infos =
    {
        std::make_pair(KernelITC::domain_itc_signal_connected_request   , &DomainService::onDomainConnected   ),
        std::make_pair(KernelITC::domain_itc_signal_disconnected_request, &DomainService::onDomainDisconnected),
    };

    for (const auto& handler_info : handler_infos)
        execution_itc->registerHandler(
            handler_info.first,
            boost::bind(handler_info.second, this, _1, _2)
        );
}

void DomainService::finalize()
{
    const auto& execution_itc = processor->getExecutionITC();

    execution_itc->unregisterHandler(KernelITC::domain_itc_signal_connected_request   );
    execution_itc->unregisterHandler(KernelITC::domain_itc_signal_disconnected_request);
}

void DomainService::beforeInitializeOnKernel(std::shared_ptr<KernelBase> kernel)
{
    gDomainService = this;
}

void DomainService::afterInitializeOnKernel(std::shared_ptr<KernelBase> kernel)
{
    BOOST_ASSERT(domains.empty() && "already has domain!?");
    BOOST_ASSERT(callback_mapping.empty() && "already has domain!?");

    verbose = kernel->getVerbose();
}

void DomainService::beforeFinalizeOnKernel(std::shared_ptr<KernelBase> kernel)
{
    domains.clear();
    callback_mapping.clear();

    verbose = false;
}

void DomainService::afterFinalizeOnKernel(std::shared_ptr<KernelBase> kernel)
{
    gDomainService = nullptr;
}

void DomainService::handleEvent(events::type event)
{
}

auto DomainService::find(int64 session_id) -> Domain*
{
    tbb::concurrent_hash_map<int64, root_set_ptr<Domain>>::const_accessor const_accessor;

    if (domains.find(const_accessor, session_id))
        return const_accessor->second.get();

    return nullptr;
}

auto DomainService::insert(int64 session_id, Domain* d) -> Domain*
{
    BOOST_ASSERT(d != nullptr && "null pointer exception");

    tbb::concurrent_hash_map<int64, root_set_ptr<Domain>>::const_accessor const_accessor;

    domains.insert(const_accessor, {session_id, make_root_set_ptr(d)});

    BOOST_ASSERT(const_accessor->second != nullptr && "no valid domain is found");

    return const_accessor->second.get();
}

UUID DomainService::listen(const std::wstring& endpoint, DomainConnCallback* conn_cb, DomainConnCallback* disconn_cb, DomainErrCallback* err_cb)
{
    return domainInit<DomainCallbacks::BY_LISTEN>(
        endpoint, conn_cb, disconn_cb, err_cb,
        [this](uint32 source_id, KernelITC& itc_response)
        {
            return handleListenResponse(source_id, itc_response);
        }
    );
}

UUID DomainService::connect(const std::wstring& endpoint, DomainConnCallback* conn_cb, DomainConnCallback* disconn_cb, DomainErrCallback* err_cb)
{
    return domainInit<DomainCallbacks::BY_CONNECT>(
        endpoint, conn_cb, disconn_cb, err_cb,
        [this](uint32 source_id, KernelITC& itc_response)
        {
            return handleConnectResponse(source_id, itc_response);
        }
    );
}

bool DomainService::cancel(const UUID& id)
{
    if (callback_mapping.count(id) == 0)
        return false;

    auto itc_request = KernelITC::make<KernelITC::DomainCancelRequest>(0, id);

    processor->sendITC(
        ProcessorId::PROCESSOR_GATEWAY_START, itc_request,
        [this](uint32 source_id, KernelITC& itc_response)
        {
            return handleCancelResponse(source_id, itc_response);
        }
    );

    return true;
}

template<int RequestType, typename Handler>
UUID DomainService::domainInit(const std::wstring& endpoint, DomainConnCallback* conn_cb, DomainConnCallback* disconn_cb, DomainErrCallback* err_cb, Handler&& handler)
{
    static_assert(RequestType == DomainCallbacks::BY_LISTEN || RequestType == DomainCallbacks::BY_CONNECT, "unexpected request type");

    using ITCType = typename std::conditional<RequestType == DomainCallbacks::BY_LISTEN, KernelITC::DomainListenRequest, KernelITC::DomainConnectRequest>::type;

          auto id            = UUID::random();
          auto endpoint_temp = ws_to_s(endpoint);
    const bool inserted      = callback_mapping.insert({id, {root_set_ptr<DomainConnCallback>(   conn_cb),
                                                             root_set_ptr<DomainConnCallback>(disconn_cb),
                                                             root_set_ptr<DomainErrCallback >(    err_cb),
                                                             RequestType                                  }});

    BOOST_ASSERT(inserted && "UUID collision!");

    auto itc_request = KernelITC::make<ITCType>(0, id, std::move(endpoint_temp));

    processor->sendITC(ProcessorId::PROCESSOR_GATEWAY_START, itc_request, std::forward<Handler>(handler));

    return id;
}

void DomainService::handleListenResponse(uint32 source, KernelITC& response_itc)
{
    BOOST_ASSERT(response_itc.isDomainListenResponse() && "handling unexpected response");

    const auto& response = response_itc.getDomainListenResponse();

    const auto& id    = response.id;
    const auto& error = static_cast< ::thor::lang::DomainError>(response.error);

    const auto& is_success = error == ::thor::lang::DomainError::OK;
    const auto& callbacks  = is_success ? query(callback_mapping, id) : popOrEmpty(callback_mapping, id);

    if (!callbacks.is_initialized())
        return;

    doDomainCallback(DOMAIN_CALLBACK_NAME_ERROR, callbacks->err_callback.get(), id, error);
}

void DomainService::handleConnectResponse(uint32 source, KernelITC& response_itc)
{
    BOOST_ASSERT(response_itc.isDomainConnectResponse() && "handling unexpected response");

    const auto& response = response_itc.getDomainConnectResponse();

    const auto& id         = response.id;
    const auto& error      = static_cast< ::thor::lang::DomainError>(response.error);

    const auto& is_success = error == ::thor::lang::DomainError::OK;

    if (is_success)
        return;

    const auto& callbacks  = popOrEmpty(callback_mapping, id);

    if (!callbacks.is_initialized())
        return;

    doDomainCallback(DOMAIN_CALLBACK_NAME_ERROR, callbacks->err_callback.get(), id, error);
}

void DomainService::handleCancelResponse(uint32 source, KernelITC& response_itc)
{
    BOOST_ASSERT(response_itc.isDomainCancelResponse() && "handling unexpected response");

    const auto& response = response_itc.getDomainCancelResponse();

    const auto& id    = response.id;
    const auto& error = static_cast< ::thor::lang::DomainError>(response.error);

    const auto& is_canceled = error == ::thor::lang::DomainError::OPERATION_CANCELED;
    const auto& callbacks   = is_canceled ? popOrEmpty(callback_mapping, id) : query(callback_mapping, id);

    if (!callbacks.is_initialized())
        return;

    doDomainCallback(DOMAIN_CALLBACK_NAME_ERROR, callbacks->err_callback.get(), id, error);
}

void DomainService::onDomainConnected(uint32 source, KernelITC& request_itc)
{
    BOOST_ASSERT(request_itc.isDomainSignalConnectedRequest() && "handling unexpected request");

    const auto& request    = request_itc.getDomainSignalConnectedRequest();

    const auto& id         = request.id;
    const auto& session_id = request.session_id;

    const auto& callbacks  = query(callback_mapping, id);

    if (!callbacks.is_initialized())
        return;

    doDomainCallback(DOMAIN_CALLBACK_NAME_CONN, callbacks->conn_callback.get(), id, session_id);
}

void DomainService::onDomainDisconnected(uint32 source, KernelITC& request_itc)
{
    BOOST_ASSERT(request_itc.isDomainSignalDisconnectedRequest() && "handling unexpected request");

    const auto& request         = request_itc.getDomainSignalDisconnectedRequest();

    const auto& id              = request.id;
    const auto& session_id      = request.session_id;

    const auto& callbacks       = queryThenDropIf(callback_mapping, id, std::mem_fn(&DomainCallbacks::isByConnect));

    if (!callbacks.is_initialized())
        return;

    doDomainCallback(DOMAIN_CALLBACK_NAME_CONN, callbacks->disconn_callback.get(), id, session_id);
}

template<typename LambdaType, typename... ArgTypes>
void DomainService::doDomainCallback(const std::string& function_name, LambdaType* lambda, ArgTypes&&... args)
{
    if (lambda == nullptr)
        return;

    const auto& function_id = processor->getKernel()->queryFunctionId(function_name);
    if (function_id == -1)
    {
        std::cerr << __FUNCTION__ << ": callback not found (" << function_name << ")" << std::endl;

        return;
    }

    const int64  session_id = -1;
    const uint32   local_id = processor->getId();

    InvocationRequestBuffer* buffer = new InvocationRequestBuffer(local_id);
    BufferNetwork::instance()->create(local_id, *buffer, InvocationRequestBuffer::Dimension(1));

    buffer->setInvocationCount(1);

    Invocation*const invocation = buffer->getInvocation(0);

    invocation->session_id  = session_id;
    invocation->function_id = function_id;

    appendParameters<0>(*invocation, lambda, std::forward<ArgTypes>(args)...);

    auto invoke_itc = KernelITC::make<KernelITC::InvocationRequest>(0, 0, buffer);
    processor->sendITC(local_id, invoke_itc);
}

} } } }
