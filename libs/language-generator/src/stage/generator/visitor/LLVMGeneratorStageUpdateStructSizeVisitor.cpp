/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "core/Prerequisite.h"
#include "utility/StringUtil.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/generator/detail/LLVMHelper.h"
#include "language/stage/generator/visitor/LLVMGeneratorStageUpdateStructSizeVisitor.h"


namespace zillians { namespace language { namespace stage { namespace visitor {

LLVMGeneratorStageUpdateStructSizeVisitor::LLVMGeneratorStageUpdateStructSizeVisitor()
{
    REGISTER_ALL_VISITABLE_ASTNODE(updateInvoker);
}

void LLVMGeneratorStageUpdateStructSizeVisitor::update(ASTNode& node)
{
    revisit(node);
}

void LLVMGeneratorStageUpdateStructSizeVisitor::update(ClassDecl& node)
{
    if(!LLVMHelper::isCurrentCodeGenerationTarget(&node))
        return;

    revisit(node);
}

void LLVMGeneratorStageUpdateStructSizeVisitor::update(FunctionDecl& node)
{
    if(!LLVMHelper::isCurrentCodeGenerationTarget(&node))
        return;

    revisit(node);
}

void LLVMGeneratorStageUpdateStructSizeVisitor::update(BlockExpr& node)
{
    if(node.tag == "new_restructure")
    {
        BOOST_ASSERT(isa<DeclarativeStmt>(*node.block->objects.begin()) && "invalid new block expression found");

        TypeSpecifier* specified_type = NULL;
        if(isa<DeclarativeStmt>(*node.block->objects.begin()))
        {
            Declaration* declaration = cast<DeclarativeStmt>(*node.block->objects.begin())->declaration;
            if(declaration && isa<VariableDecl>(declaration))
            {
                specified_type = cast<VariableDecl>(declaration)->type;
            }
        }

        BOOST_ASSERT(specified_type != NULL && "invalid new block expression found");

        CallExpr* object_create_call = NULL;
        if(isa<ExpressionStmt>(*(++node.block->objects.begin())))
        {
            Expression* expr = cast<ExpressionStmt>( *(++node.block->objects.begin()) )->expr;
            if(expr && isa<BinaryExpr>(expr))
            {
                Expression* rhs_expr = cast<BinaryExpr>(expr)->right;
                if(rhs_expr && isa<CastExpr>(rhs_expr))
                {
                    object_create_call = cast<CallExpr>(cast<CastExpr>(rhs_expr)->node);
                }
            }
        }

        BOOST_ASSERT(object_create_call != NULL && "invalid new block expression found");
        BOOST_ASSERT(object_create_call->parameters.size() == 2 && "invalid new block expression found");

        Type* resolved_type = specified_type->getCanonicalType();

        if(resolved_type)
        {
            if(resolved_type->getAsRecordType())
            {
                ClassDecl* class_decl = resolved_type->getAsClassDecl();

                // we don't need to process non-fully specialized class template
                if(isa<TemplatedIdentifier>(class_decl->name) && !cast<TemplatedIdentifier>(class_decl->name)->isFullySpecialized())
                    return;

                SynthesizedObjectLayoutContext* ctx = SynthesizedObjectLayoutContext::get(resolved_type->getAsClassDecl());
                BOOST_ASSERT(ctx && "invalid synthesized object layout context");

                if(ctx)
                {
                    // set the size from object layout
                    NumericLiteral* size_literal = cast<NumericLiteral>(object_create_call->parameters[0]);

                    BOOST_ASSERT(size_literal != NULL && "invalid new block expression found");

                    size_literal->value.i64 = (int64)ctx->object_size;
                }
            }
            else
            {
                UNREACHABLE_CODE();
            }
        }
        else
        {
            // if we cannot find the canonical type for this type specifier
            // this is probably a non-fully-specialize class template...
            // then just ignore it
        }
    }
    revisit(node);
}

} } } }

