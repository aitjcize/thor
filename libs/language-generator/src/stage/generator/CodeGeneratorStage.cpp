/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/stage/generator/CodeGeneratorStage.h"
#include "language/context/ParserContext.h"
#include "language/context/GeneratorContext.h"
#include "language/logging/LoggerWrapper.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/stage/serialization/detail/ASTSerializationHelper.h"

#include <llvm/Config/config.h>
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 3
    #include <llvm/IR/Module.h>
    #include <llvm/IR/LLVMContext.h>
    #include <llvm/IR/DataLayout.h>
    #include <llvm/IRReader/IRReader.h>
#else
    #include <llvm/Module.h>
    #include <llvm/LLVMContext.h>
    #include <llvm/DataLayout.h>
    #include <llvm/Support/IRReader.h>
#endif

#include <llvm/PassManager.h>
#include <llvm/Linker.h>
#include <llvm/Target/TargetMachine.h>
#include <llvm/Bitcode/BitstreamWriter.h>
#include <llvm/Bitcode/ReaderWriter.h>
#include <llvm/ADT/Triple.h>
#include <llvm/Support/Path.h>
#include <llvm/Support/ToolOutputFile.h>
#include <llvm/Support/SourceMgr.h>
#include <llvm/Support/TargetRegistry.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Support/FormattedStream.h>
#include <llvm/Support/Host.h>
#include <llvm/Support/Program.h>
#include <llvm/Assembly/AssemblyAnnotationWriter.h>

#include <boost/regex.hpp>

#ifdef BUILD_WITH_NVVM
#include <nvvm.h>
#endif

namespace zillians { namespace language { namespace stage {

CodeGeneratorStage::CodeGeneratorStage() : verbose(false)
{ }

CodeGeneratorStage::~CodeGeneratorStage()
{ }

const char* CodeGeneratorStage::name()
{
    return "LLVM IR Generation Stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> CodeGeneratorStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options()
        ("verbose,v", "verbose mode");

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options();

    return std::make_pair(option_desc_public, option_desc_private);
}

bool CodeGeneratorStage::parseOptions(po::variables_map& vm)
{
    verbose = (vm.count("verbose") > 0);

    return true;
}

bool CodeGeneratorStage::execute(bool& continue_execution)
{
	GeneratorContext& generator_context = getGeneratorContext();

	if(!generator_context.active_config)
		return true;

    // Compile to assembly file
    if(!buildAssemblyCode())
    {
        std::cerr << "Error: failed to build assembly code for AST" << std::endl;
        return false;
    }

	return true;
}

bool CodeGeneratorStage::buildAssemblyCode()
{
	GeneratorContext& generator_context = getGeneratorContext();

	if(generator_context.active_config->arch.is_x86())
	{
		return buildAssemblyCodeLLVM();
	}
	else if(generator_context.active_config->arch.is_cuda())
	{
#ifdef BUILD_WITH_NVVM
		return buildAssemblyCodeNVVM();
#else
		return buildAssemblyCodeLLVM();
#endif
	}
	else
	{
		UNIMPLEMENTED_CODE();
		return false;
	}
}

bool CodeGeneratorStage::buildAssemblyCodeLLVM()
{
    llvm::SMDiagnostic err;

    // Initialize all targets
    llvm::InitializeAllTargets();
    llvm::InitializeAllTargetMCs();
    llvm::InitializeAllAsmPrinters();
    llvm::InitializeAllAsmParsers();

    GeneratorContext& generator_context = getGeneratorContext();

	llvm::Module* module = generator_context.active_config->module;
	if(!module)
	{
		std::cerr << "Error: invalid LLVM module" << std::endl;
		return false;
	}

	// Retrieve architecture setting (ref: llc --march)
	llvm::Triple arch_setting( module->getTargetTriple() );
	if( arch_setting.getTriple().empty())
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 1
		arch_setting.setTriple(llvm::sys::getDefaultTargetTriple());
#else
		arch_setting.setTriple(llvm::sys::getHostTriple());
#endif

	// Create the target machine
	std::string lookup_error;
	const llvm::Target *target = llvm::TargetRegistry::lookupTarget(arch_setting.getTriple(), lookup_error);

	if(!target)
	{
		std::cerr << "Error: fail to find the target : " << lookup_error << std::endl;
		return false;
	}

	// TODO: cpu type could be more flexible
	std::string mcpu;
	std::string feature;
	std::string asm_file_postfix;
	std::string asm_extension;
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 1
	llvm::TargetOptions options;
#endif
	llvm::Reloc::Model reloc_model = llvm::Reloc::PIC_; // TODO: other choices?

	if(generator_context.active_config->arch.is_x86())
	{
		mcpu = "generic";
		asm_file_postfix = "_x86";
		asm_extension = THOR_EXTENSION_ASM;
	}
	else if(generator_context.active_config->arch.is_arm())
	{
		UNIMPLEMENTED_CODE();
	}
    else if(generator_context.active_config->arch.is_cuda())
	{
		std::string isa = generator_context.active_config->arch.toString("");
		mcpu = isa;
		asm_file_postfix = "_cuda_" + isa;
		asm_extension = THOR_EXTENSION_PTX;
	}
    else if(generator_context.active_config->arch.is_opencl())
	{
		UNIMPLEMENTED_CODE();
		asm_file_postfix = "_opencl";
		asm_extension = THOR_EXTENSION_CL;
	}

	asm_file = (generator_context.global_config.build_path / (generator_context.global_config.manifest.name + asm_file_postfix + asm_extension)).string();
//	std::cerr << "asm file: " << asm_file << std::endl;

    // decide the optimization level
    llvm::CodeGenOpt::Level opt_level = llvm::CodeGenOpt::Default;
    switch(generator_context.opt_level)
    {
    case 0:
        opt_level = llvm::CodeGenOpt::None;
        if(verbose) std::cerr << "Optimization: None" << std::endl;
        break;
    case 1:
        opt_level = llvm::CodeGenOpt::Less;
        if(verbose) std::cerr << "Optimization: Less" << std::endl;
        break;
    case 2:
        opt_level = llvm::CodeGenOpt::Default;
        if(verbose) std::cerr << "Optimization: Default" << std::endl;
        break;
    case 3:
        opt_level = llvm::CodeGenOpt::Aggressive;
        if(verbose) std::cerr << "Optimization: Aggressive" << std::endl;
        break;
    }

#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 1
	shared_ptr<llvm::TargetMachine> target_machine(target->createTargetMachine(arch_setting.getTriple(), mcpu, feature, options, reloc_model, llvm::CodeModel::Default, opt_level));
#else
	shared_ptr<llvm::TargetMachine> target_machine(target->createTargetMachine(arch_setting.getTriple(), mcpu, feature, reloc_model));
#endif

	if(!target_machine.get())
	{
		std::cerr << "Error: fail to create target machine" << std::endl;
		return false;
	}

#ifdef __PLATFORM_MAC__
	target_machine->setMCUseCFI(false);
#endif

	// Add the target data from the target machine, if it exists, or the module.
	llvm::PassManager pm;
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 2
	if (const llvm::DataLayout* dl = target_machine->getDataLayout())
		pm.add(new llvm::DataLayout(*dl));
	else
		pm.add(new llvm::DataLayout(module));
#else
    if (const llvm::TargetData* td = target_machine->getTargetData())
        pm.add(new llvm::TargetData(*td));
    else
        pm.add(new llvm::TargetData(module));

#endif
	// Specify output file
	std::string out_error;
	shared_ptr<llvm::tool_output_file> out_file(new llvm::tool_output_file(asm_file.c_str(), out_error, llvm::sys::fs::F_Binary));
	out_file->keep();

	llvm::formatted_raw_ostream fos(out_file->os());
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 1
	target_machine->addPassesToEmitFile(pm, fos, llvm::TargetMachine::CGFT_AssemblyFile, true);
#else
	target_machine->addPassesToEmitFile(pm, fos, llvm::TargetMachine::CGFT_AssemblyFile, opt_level, true);
#endif

	// we ignore the return code from pass manager because it's returning the "changed" flag, which does not necessary indicate an error when returning false
	// and since all bc file should have been verified beforehand, we can assume that assembly code can always be generated as long as LLVM is working correctly
	pm.run(*module);

    if(generator_context.global_config.dump_llvm)
    {
        std::string error_info;
        std::stringstream ss; ss << "after_llvm_pass_dump_" << generator_context.active_config->arch.toString("_") << ".ll";
        boost::filesystem::path emit_file_path = generator_context.global_config.build_path / ss.str();

        llvm::raw_fd_ostream stream(emit_file_path.string().c_str(), error_info);
        if(stream.has_error())
        {
            LOG4CXX_ERROR(LoggerWrapper::GeneratorStage, "failed to prepare stream for writing LLVM IR: " << error_info);
            return false;
        }

        auto assembly_write = new llvm::AssemblyAnnotationWriter();
        generator_context.active_config->module->print(stream, assembly_write);
    }

	generator_context.active_config->asm_file = asm_file;

    return true;
}

#ifdef BUILD_WITH_NVVM
bool CodeGeneratorStage::buildAssemblyCodeNVVM()
{
    GeneratorContext& generator_context = getGeneratorContext();

    // determine the PTX output file name
	std::string asm_file_postfix;
	std::string asm_extension;
	if(generator_context.active_config->arch.is_cuda())
	{
	    std::string isa = generator_context.active_config->arch.toString("");
		asm_file_postfix = "_cuda_" + isa;
		asm_extension = THOR_EXTENSION_PTX;
	}
	else if(generator_context.active_config->arch.is_opencl())
	{
		UNIMPLEMENTED_CODE();
		asm_file_postfix = "_opencl";
		asm_extension = THOR_EXTENSION_CL;
	}

	std::string asm_file = (generator_context.global_config.build_path / (generator_context.global_config.manifest.name + asm_file_postfix + asm_extension)).string();

    // create a temporary file that is in the pure text form of LLVM bitcode
    std::string nvvm_input_file = (generator_context.global_config.build_path / (generator_context.global_config.manifest.name + "_nvvm.ll")).string();
    if(true)
    {
		llvm::Module* module = generator_context.active_config->module;
		std::string error_info;
		llvm::raw_fd_ostream stream(nvvm_input_file.c_str(), error_info);
		if(stream.has_error())
		{
			std::cerr << "Error: failed to prepare NVVM input file" << std::endl;
			return false;
		}

	    auto assembly_write = new llvm::AssemblyAnnotationWriter();
	    generator_context.active_config->module->print(stream, assembly_write);
    }

    char* file_buffer = NULL;
    std::size_t buffer_size;
    if(true)
    {
    	FILE* f = fopen(nvvm_input_file.c_str(), "rb");
    	if(!f)
    	{
    		std::cerr << "Error: failed to open NVVM input file" << std::endl;
    		fclose(f);
    		return false;
    	}

    	auto stat_size = boost::filesystem::file_size(nvvm_input_file);
    	file_buffer = (char*)malloc(stat_size);
    	buffer_size = fread(file_buffer, 1, stat_size, f);
    	if(ferror(f))
    	{
    		std::cerr << "Error: failed to read NVVM input file" << std::endl;
    		fclose(f);
    		return false;
    	}

    	fclose(f);
    }

	nvvmProgram cu;

	if(nvvmCreateProgram(&cu) != NVVM_SUCCESS)
	{
		std::cerr << "Error: failed to create NVVM compilation unit" << std::endl;
		return false;
	}

	if(nvvmAddModuleToProgram(cu, file_buffer, buffer_size, NULL) != NVVM_SUCCESS)
	{
		std::cerr << "Error: failed to add the module \"" << nvvm_input_file << "\" to the compilation unit" << std::endl;
		free(file_buffer);
		nvvmDestroyProgram(&cu);
		return false;
	}

	auto dump_log = [&]() {
		size_t log_size;
		char *log;
		if(nvvmGetProgramLogSize(cu, &log_size) != NVVM_SUCCESS)
		{
			std::cerr << "Error: failed to get the compilation log size" << std::endl;
		}
		else
		{
			log = (char*)malloc(log_size);
			if(nvvmGetProgramLog(cu, log) != NVVM_SUCCESS)
			{
				std::cerr << "Error: failed to get the compilation log" << std::endl;
			}
			else
			{
				std::cerr << log << std::endl;
			}
			free(log);
		}
	};


	int num_options;
	const char** options;

	if(generator_context.active_config->arch.is_sm_20())
	{
		num_options = 1;
		const char* o[] = { "-arch=compute_20", "" };
		options = (const char**)o;
	}
	else if(generator_context.active_config->arch.is_sm_30())
	{
		num_options = 1;
		const char* o[] = { "-arch=compute_30", "" };
		options = (const char**)o;
	}
	else if(generator_context.active_config->arch.is_sm_35())
	{
		num_options = 1;
		const char* o[] = { "-arch=compute_35", "" };
		options = (const char**)o;
	}
	else
	{
		std::cerr << "Error: unsupported CUDA architecture by NVVM" << std::endl;
		free(file_buffer);
		nvvmDestroyProgram(&cu);
		return false;
	}

	if(nvvmCompileProgram(cu, num_options, options) != NVVM_SUCCESS)
	{
		std::cerr << "Error: failed to generate PTX from the compilation unit" << std::endl;
		dump_log();
		free(file_buffer);
		nvvmDestroyProgram(&cu);
		return false;
	}

	size_t ptx_size;
	char* ptx;
	if(nvvmGetCompiledResultSize(cu, &ptx_size) != NVVM_SUCCESS)
	{
		std::cerr << "Error: failed to get the PTX output size" << std::endl;
	}
	else
	{
		ptx = (char*)malloc(ptx_size);
		if(nvvmGetCompiledResult(cu, ptx) != NVVM_SUCCESS)
		{
			std::cerr << "Error: failed to get the PTX output" << std::endl;
		}
		else
		{
			FILE* f = fopen(asm_file.c_str(), "w+");

			boost::regex pattern("call\\.uni");
			std::string replace("call    ");
			std::string input(ptx);

			std::string ptx_fixed = boost::regex_replace(input, pattern, replace);

			if(verbose)
            {
                std::cerr << "PTX Generated:\n" << ptx_fixed << std::endl;
            }

			fprintf(f, "%s\n", ptx_fixed.c_str());
			if(ferror(f))
			{
				std::cerr << "Error: failed to write the PTX output" << std::endl;
				free(file_buffer);
				nvvmDestroyProgram(&cu);
				fclose(f);
				free(ptx);
				return false;
			}
			fclose(f);
		}
		free(ptx);
	}

	dump_log();

	free(file_buffer);
	nvvmDestroyProgram(&cu);

	generator_context.active_config->asm_file = asm_file;

	return true;
}
#endif

bool CodeGeneratorStage::hasProgress()
{
	return getGeneratorContext().active_config != NULL;
}

} } }
