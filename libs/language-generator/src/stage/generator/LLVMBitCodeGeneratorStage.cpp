/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <llvm/Analysis/Verifier.h>
#include <llvm/Bitcode/ReaderWriter.h>
#include <llvm/Assembly/AssemblyAnnotationWriter.h>
#include <llvm/Support/raw_ostream.h>

#include "language/context/ParserContext.h"
#include "language/context/GeneratorContext.h"
#include "language/logging/LoggerWrapper.h"

#include "language/stage/generator/LLVMBitCodeGeneratorStage.h"

namespace zillians { namespace language { namespace stage {

LLVMBitCodeGeneratorStage::LLVMBitCodeGeneratorStage() : emit_llvm(false)
{ }

LLVMBitCodeGeneratorStage::~LLVMBitCodeGeneratorStage()
{ }

const char* LLVMBitCodeGeneratorStage::name()
{
    return "LLVM BitCode Generation Stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> LLVMBitCodeGeneratorStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options()
        ("emit-llvm", po::value<std::string>(), "emit llvm bitcode");

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options();

    return std::make_pair(option_desc_public, option_desc_private);
}

bool LLVMBitCodeGeneratorStage::parseOptions(po::variables_map& vm)
{
    emit_llvm = (vm.count("emit-llvm") > 0);
    if(emit_llvm)
    {
        llvm_bc_file = vm["emit-llvm"].as<std::string>();
    }

    return true;
}

bool LLVMBitCodeGeneratorStage::execute(bool& continue_execution)
{
    UNUSED_ARGUMENT(continue_execution);

    GeneratorContext& generator_context = getGeneratorContext();

    if(!generator_context.active_config)
    	return true;

    if(generator_context.global_config.dump_llvm)
    {
        std::string error_info;      
        std::stringstream ss; ss << "before_llvm_pass_dump_" << generator_context.active_config->arch.toString("_") << ".ll";
        boost::filesystem::path emit_file_path = generator_context.global_config.build_path / ss.str();
        llvm::raw_fd_ostream stream(emit_file_path.string().c_str(), error_info);
        if(stream.has_error())
        {
            LOG4CXX_ERROR(LoggerWrapper::GeneratorStage, "failed to prepare stream for writing LLVM IR: " << error_info);
            return false;
        }

        auto assembly_write = new llvm::AssemblyAnnotationWriter();
        generator_context.active_config->module->print(stream, assembly_write);
    }

    if(emit_llvm)
    {
        std::string error_info;

        if(llvm::verifyModule(*generator_context.active_config->module, llvm::PrintMessageAction, &error_info))
        {
            LOG4CXX_ERROR(LoggerWrapper::GeneratorStage, "failed to verify LLVM module: " << error_info);
        }

        boost::filesystem::path emit_file_path = llvm_bc_file;
        std::string prefix = generator_context.active_config->arch.toString("") + "_";

        emit_file_path = emit_file_path.parent_path() / (prefix + emit_file_path.filename().string());

        llvm::raw_fd_ostream stream(emit_file_path.string().c_str(), error_info, llvm::sys::fs::F_Binary);
        if(stream.has_error())
        {
            LOG4CXX_ERROR(LoggerWrapper::GeneratorStage, "failed to prepare stream for writing LLVM bitcode: " << error_info);
            return false;
        }

        llvm::WriteBitcodeToFile(generator_context.active_config->module, stream);
    }

    return true;
}

bool LLVMBitCodeGeneratorStage::hasProgress()
{
	return getGeneratorContext().active_config != NULL;
}

} } }
