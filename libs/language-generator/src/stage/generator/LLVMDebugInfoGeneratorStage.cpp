/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/stage/generator/LLVMDebugInfoGeneratorStage.h"
#include "language/stage/generator/visitor/LLVMDebugInfoGeneratorStageVisitor.h"
#include "language/stage/generator/visitor/LLVMDebugInfoGeneratorStagePreambleVisitor.h"
#include "language/context/ParserContext.h"
#include "language/context/GeneratorContext.h"

namespace zillians
{
namespace language
{
namespace stage
{

LLVMDebugInfoGeneratorStage::LLVMDebugInfoGeneratorStage()
{
}

LLVMDebugInfoGeneratorStage::~LLVMDebugInfoGeneratorStage()
{
}

const char* LLVMDebugInfoGeneratorStage::name()
{
	return "LLVM Debug Info Generation Stage";
}

std::pair<shared_ptr<po::options_description>,
		shared_ptr<po::options_description>> LLVMDebugInfoGeneratorStage::getOptions()
{
	shared_ptr<po::options_description> option_desc_public(
			new po::options_description());
	shared_ptr<po::options_description> option_desc_private(
			new po::options_description());

	for(auto& option : option_desc_public->options())
		option_desc_private->add(option);

	option_desc_private->add_options();

	return std::make_pair(option_desc_public, option_desc_private);
}

bool LLVMDebugInfoGeneratorStage::parseOptions(po::variables_map& vm)
{
	return true;
}

bool LLVMDebugInfoGeneratorStage::execute(bool& continue_execution)
{
	UNUSED_ARGUMENT(continue_execution);

	GeneratorContext& generator_context = getGeneratorContext();

	if (!generator_context.emit_debug_info)
	    return true;

	if (!generator_context.active_config)
		return true;

	// TODO implement debug info on X86 target
	if (generator_context.active_config->arch.is_not_x86())
	{
		std::cerr
				<< "Warning: debug info hasn't been implemented on non-x86 target"
				<< std::endl;
		return true;
	}

	llvm::DIBuilder factory(*generator_context.active_config->module);
	visitor::LLVMDebugInfoGeneratorStagePreambleVisitor preamble_visitor(
			*generator_context.active_config->context,
			*generator_context.active_config->module, factory);
	visitor::LLVMDebugInfoGeneratorStageVisitor visitor(
			*generator_context.active_config->context,
			*generator_context.active_config->module, factory);

	if (getParserContext().tangle)
	{
		preamble_visitor.visit(*getParserContext().tangle);
		visitor.visit(*getParserContext().tangle);
	}

	return true;
}

bool LLVMDebugInfoGeneratorStage::hasProgress()
{
	return getGeneratorContext().active_config != NULL;
}

}
}
}
