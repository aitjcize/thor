#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

add_definitions( ${LLVM_CPPFLAGS} )

add_library(thor-language-generator ${ZILLIANS_PREFERED_LIB_TYPE}
    src/context/GeneratorContext.cpp
    src/stage/generator/detail/CppSourceGenerator.cpp
    src/stage/generator/CppGeneratorStage.cpp
    src/stage/generator/GeneratorDriverStage.cpp
    src/stage/generator/LLVMGeneratorStage.cpp
    src/stage/generator/LLVMDebugInfoGeneratorStage.cpp
    src/stage/generator/LLVMBitCodeGeneratorStage.cpp
    src/stage/generator/LLVMCleanupStage.cpp
    src/stage/generator/CodeGeneratorStage.cpp
    src/stage/generator/visitor/LLVMGeneratorStageVisitor.cpp
    src/stage/generator/visitor/LLVMGeneratorStageFunctionPreambleVisitor.cpp
    src/stage/generator/visitor/LLVMGeneratorStageStructPreambleVisitor.cpp
    src/stage/generator/visitor/LLVMGeneratorStagePreambleVisitor.cpp
    src/stage/generator/visitor/LLVMGeneratorStageUpdateStructSizeVisitor.cpp
    src/stage/generator/visitor/LLVMDebugInfoGeneratorStagePreambleVisitor.cpp
    src/stage/generator/visitor/LLVMDebugInfoGeneratorStageVisitor.cpp
    )
target_link_libraries(thor-language-generator
    ${LLVM_ALL_LIBS}
    ${LLVM_LDFLAGS}
    )

if(NVVM_FOUND)
    target_link_libraries(thor-language-generator
        ${NVVM_LIBRARIES}
        )
    include_directories(
        ${NVVM_INCLUDE_DIR}
        )
endif()

target_link_libraries(thor-language-generator
    thor-common-utility
    thor-common-core
    thor-language-general
    thor-language-tree
    thor-language-stage-base
    )
