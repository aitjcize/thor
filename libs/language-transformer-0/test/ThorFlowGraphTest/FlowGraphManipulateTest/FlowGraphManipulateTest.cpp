/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#include "language/tree/basic/Literal.h"
#include "language/tree/statement/Statement.h"
#include "language/tree/statement/ExpressionStmt.h"
#include "language/tree/expression/BinaryExpr.h"
#include "language/tree/expression/Expression.h"
#include "language/stage/transformer/detail/FlowSpliter.h"


#define BOOST_TEST_MODULE ThorFlowGraphTest_FlowGraphManipulateTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE( ThorFlowGraphTest_FlowGraphManipulateTestSuite )

BOOST_AUTO_TEST_CASE( ThorFlowGraphTest_FlowGraphManipulateTestCaseBasic1 )
{
    using zillians::language::stage::visitor::detail::FlowGraph;
    using zillians::language::stage::visitor::detail::BasicBlock;
    using zillians::language::stage::visitor::detail::NormalTransition;
    using zillians::language::stage::visitor::detail::NullTransition;
    using zillians::language::tree::Statement;
    using zillians::language::tree::ExpressionStmt;
    using zillians::language::tree::BinaryExpr;
    using zillians::language::tree::NumericLiteral;

    // block creating, wiring, erasing
    {
        FlowGraph fg(L"fake_func");
        auto func_init = fg.create_block(BasicBlock::Tag::FUNC_INIT);
        auto func_end = fg.create_block(BasicBlock::Tag::FUNC_END);
        func_init->set_transition(NormalTransition(func_init, func_end));
        func_end->set_transition(NullTransition());
        fg.set_entry(func_init);

        BOOST_CHECK_EQUAL(fg.size(), 2);
        BOOST_CHECK_EQUAL(func_init->id(), 0);
        BOOST_CHECK_EQUAL(func_end->id(), 1);
        BOOST_CHECK_EQUAL(fg.get_entry(), func_init);

        fg.erase_block(func_init);
        BOOST_CHECK_EQUAL(fg.get_entry(), func_end);
        func_end->clear_predecessor();
        BOOST_CHECK_EQUAL(fg.get_entry()->get_predecessors().empty(), true);
    }
}

BOOST_AUTO_TEST_CASE( ThorFlowGraphTest_FlowGraphManipulateTestCaseSimpleOptimization )
{
    using zillians::language::stage::visitor::detail::FlowGraph;
    using zillians::language::stage::visitor::detail::BasicBlock;
    using zillians::language::stage::visitor::detail::NormalTransition;
    using zillians::language::stage::visitor::detail::NullTransition;
    using zillians::language::tree::Statement;
    using zillians::language::tree::ExpressionStmt;
    using zillians::language::tree::BinaryExpr;
    using zillians::language::tree::NumericLiteral;

    // erase empty block
    {
        FlowGraph fg(L"fake_func");
        auto func_init = fg.create_block(BasicBlock::Tag::FUNC_INIT);
        auto b1        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto b2        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto b3        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto b4        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto func_end  = fg.create_block(BasicBlock::Tag::FUNC_END);

        func_init -> set_transition(NormalTransition(func_init, b1));
        b1        -> set_transition(NormalTransition(b1, b2));
        b2        -> set_transition(NormalTransition(b2, b3));
        b3        -> set_transition(NormalTransition(b3, b4));
        b4        -> set_transition(NormalTransition(b4, func_end));
        func_end  -> set_transition(NullTransition());

        fg.set_entry(func_init);
        fg.remove_empty_blocks();

        BOOST_CHECK_EQUAL(fg.size(), 1);
        BOOST_CHECK_EQUAL(fg.get_entry()->id(), 5);
        BOOST_CHECK(fg.get_entry()->get_transition().type() == typeid(NullTransition));
    }

    // remove null transition block
    {
        FlowGraph fg(L"fake_func");
        auto func_init = fg.create_block(BasicBlock::Tag::FUNC_INIT);
        auto b1        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto b2        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto b3        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto b4        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto func_end  = fg.create_block(BasicBlock::Tag::FUNC_END);

        func_init -> set_transition(NormalTransition(func_init, b1));
        b1        -> set_transition(NormalTransition(b1, b2));
        b2        -> set_transition(NormalTransition(b2, b3));
        b3        -> set_transition(NormalTransition(b3, b4));
        b4        -> set_transition(NormalTransition(b4, func_end));
        func_end  -> set_transition(NullTransition());

        fg.set_entry(func_init);
        fg.remove_null_transition_block();

        BOOST_CHECK_EQUAL(fg.size(), 1);
        BOOST_CHECK_EQUAL(fg.get_entry()->id(), 0);
        BOOST_CHECK(fg.get_entry()->get_transition().type() == typeid(NullTransition));
    }

    // merge blocks
    {
        FlowGraph fg(L"fake_func");
        auto func_init = fg.create_block(BasicBlock::Tag::FUNC_INIT);
        auto b1        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto b2        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto b3        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto b4        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto func_end  = fg.create_block(BasicBlock::Tag::FUNC_END);

        func_init -> set_transition(NormalTransition(func_init, b1));
        b1        -> set_transition(NormalTransition(b1, b2));
        b2        -> set_transition(NormalTransition(b2, b3));
        b3        -> set_transition(NormalTransition(b3, b4));
        b4        -> set_transition(NormalTransition(b4, func_end));
        func_end  -> set_transition(NullTransition());

        Statement* si = new ExpressionStmt(new NumericLiteral(0));
        Statement* s1 = new ExpressionStmt(new NumericLiteral(1));
        Statement* s2 = new ExpressionStmt(new NumericLiteral(2));
        Statement* s3 = new ExpressionStmt(new NumericLiteral(3));
        Statement* s4 = new ExpressionStmt(new NumericLiteral(4));
        Statement* se = new ExpressionStmt(new NumericLiteral(5));

        func_init -> insert_statement(si);
        b1        -> insert_statement(s1);
        b2        -> insert_statement(s2);
        b3        -> insert_statement(s3);
        b4        -> insert_statement(s4);
        func_end  -> insert_statement(se);

        fg.set_entry(func_init);

        BOOST_CHECK_EQUAL(fg.size(), 6);
        BOOST_CHECK_EQUAL(fg.get_entry()->id(), 0);
        fg.merge_blocks();

        BOOST_CHECK_EQUAL(fg.size(), 1);
        BOOST_CHECK_EQUAL(fg.get_entry()->id(), 0);
        std::vector<Statement*> stats(fg.get_entry()->get_statements().begin(), fg.get_entry()->get_statements().end());
        BOOST_CHECK_EQUAL(static_cast<int>(fg.get_entry()->get_tag()), static_cast<int>(BasicBlock::Tag::FUNC_INIT));
        BOOST_CHECK_EQUAL(stats.size(), 6);
    }
    
}

BOOST_AUTO_TEST_CASE( ThorFlowGraphTest_FlowGraphManipulateTestCaseEraseEmptyTerminal )
{
    using zillians::language::stage::visitor::detail::FlowGraph;
    using zillians::language::stage::visitor::detail::BasicBlock;
    using zillians::language::stage::visitor::detail::NormalTransition;
    using zillians::language::stage::visitor::detail::NullTransition;
    using zillians::language::tree::Statement;
    using zillians::language::tree::ExpressionStmt;
    using zillians::language::tree::BinaryExpr;
    using zillians::language::tree::NumericLiteral;

    // remove null transition block
    {
        FlowGraph fg(L"fake_func");
        auto func_init = fg.create_block(BasicBlock::Tag::FUNC_INIT);
        auto b1        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto b2        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto b3        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto b4        = fg.create_block(BasicBlock::Tag::FUNC_END);
        auto func_end  = fg.create_block(BasicBlock::Tag::FUNC_END);

        func_init -> set_transition(NormalTransition(func_init, b1));
        b1        -> set_transition(NormalTransition(b1, b2));
        b2        -> set_transition(NormalTransition(b2, b3));
        b3        -> set_transition(NormalTransition(b3, b4));
        b4        -> set_transition(NormalTransition(b4, func_end));
        func_end  -> set_transition(NullTransition());

        fg.set_entry(func_init);
        fg.remove_null_transition_block();

        BOOST_CHECK_EQUAL(fg.size(), 1);
        BOOST_CHECK_EQUAL(fg.get_entry()->id(), 0);
        BOOST_CHECK(fg.get_entry()->get_transition().type() == typeid(NullTransition));
    }

}

BOOST_AUTO_TEST_SUITE_END()
