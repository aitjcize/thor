/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/stage/transformer/RestructureStage0.h"
#include "language/stage/transformer/visitor/RestructureStageVisitor0.h"
#include "language/tree/visitor/PrettyPrintVisitor.h"
#include "language/context/ParserContext.h"

namespace zillians { namespace language { namespace stage {

RestructureStage0::RestructureStage0() : dump_graphviz(false), keep_going(false), has_progress(false)
{ }

RestructureStage0::~RestructureStage0()
{ }

const char* RestructureStage0::name()
{
    return "Restructure Stage 0";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> RestructureStage0::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options()
            ("no-system", "no system bundle");

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options()
        ("debug-restructure-stage", "debug restructure stage")
        //("dump-graphviz", "dump AST in graphviz format")
        //("dump-graphviz-dir", po::value<std::string>(), "dump AST in graphviz format")
    ;

    return std::make_pair(option_desc_public, option_desc_private);
}

bool RestructureStage0::parseOptions(po::variables_map& vm)
{
    debug = (vm.count("debug-restructure-stage") > 0);
    dump_graphviz = vm["dump-graphviz"].as<bool>();
    no_system_bundle = (vm.count("no-system") > 0);

    if(vm.count("dump-graphviz-dir") > 0)
    {
        dump_graphviz_dir = vm["dump-graphviz-dir"].as<std::string>();
    }

    // keep going to static test when testing restruction
    if(vm.count("mode-xform") > 0)
    {
        keep_going = true;
    }

    return true;
}

bool RestructureStage0::execute(bool& continue_execution)
{
    has_progress = false;

    if(keep_going)
        continue_execution = true;

    if(!hasParserContext())
        return false;

    ParserContext& parser_context = getParserContext();

    BOOST_ASSERT(parser_context.tangle && "no tangle!");

    if(parser_context.tangle)
    {
        if(dump_graphviz)
        {
            boost::filesystem::path p(dump_graphviz_dir);
            ASTNodeHelper::visualize(getParserContext().tangle, p / "pre-restructure-0.dot");
            getParserContext().tangle->toSource(p / "pre-restructure-0.t");
        }

        // restructure the entire tree in multiple passes
        while(true)
        {
            visitor::RestructureStageVisitor0 restruct(no_system_bundle);
            restruct.visit(*parser_context.tangle);

            if (!restruct.hasTransforms())
                break;

            has_progress = true;
            restruct.applyTransforms();
        }

        if(debug)
        {
            tree::visitor::PrettyPrintVisitor printer;
            printer.visit(*parser_context.tangle);
        }

        if(dump_graphviz)
        {
            boost::filesystem::path p(dump_graphviz_dir);
            ASTNodeHelper::visualize(getParserContext().tangle, p / "post-restructure-0.dot");
            getParserContext().tangle->toSource(p / "post-restructure-0.t");
        }
        return true;
    }
    else
    {
        return false;
    }

}

bool RestructureStage0::hasProgress()
{
    return has_progress;
}

} } }
