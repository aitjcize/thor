/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cstddef>
#include <cstdint>

#include <functional>
#include <tuple>
#include <unordered_set>
#include <utility>
#include <vector>

#include <boost/assert.hpp>
#include <boost/mpl/joint_view.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/next_prior.hpp>
#include <boost/optional/optional.hpp>
#include <boost/range/adaptor/indirected.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm/for_each.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/numeric.hpp>

#include "core/Types.h"

#include "language/Architecture.h"
#include "language/context/ContextBase.h"
#include "language/context/ResolverContext.h"
#include "language/logging/LoggerWrapper.h"
#include "language/logging/StringTable.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Annotations.h"
#include "language/tree/basic/Block.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/Literal.h"
#include "language/tree/basic/PrimitiveKind.h"
#include "language/tree/basic/Type.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/expression/BinaryExpr.h"
#include "language/tree/expression/CallExpr.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/expression/IdExpr.h"
#include "language/tree/expression/TernaryExpr.h"
#include "language/tree/expression/TieExpr.h"
#include "language/tree/expression/UnaryExpr.h"
#include "language/tree/statement/BranchStmt.h"
#include "language/tree/statement/DeclarativeStmt.h"
#include "language/tree/statement/ExpressionStmt.h"
#include "language/tree/statement/SelectionStmt.h"
#include "language/tree/statement/Statement.h"
#include "language/tree/module/Tangle.h"

#include "language/stage/transformer/detail/AsyncHelper.h"
#include "language/stage/transformer/detail/RestructureHelper.h"

namespace zillians { namespace language { namespace stage { namespace detail {

namespace {

using ContextToCloneList = boost::mpl::joint_view<
    tree::detail::ContextToCloneTypeList,
    boost::mpl::vector<
        zillians::language::ResolvedType,
        zillians::language::ResolvedSymbol,
        zillians::language::SplitReferenceContext,
        zillians::language::SplitInverseReferenceContext
    >
>::type;

const wchar_t FUNCTION_ROOTSET_ADDER  [] = L"__addToRootSet";
const wchar_t FUNCTION_ROOTSET_REMOVER[] = L"__removeFromRootSet";

const wchar_t FUNCTION_INVOCATION_RESERVE[] = L"__reserveInvocation"        ;
const wchar_t FUNCTION_INVOCATION_APPEND [] = L"__appendInvocationParameter";
const wchar_t FUNCTION_INVOCATION_COMMIT [] = L"__commitInvocation"         ;
const wchar_t FUNCTION_INVOCATION_ABORT  [] = L"__abortInvocation"          ;

const wchar_t FUNCTION_INVOKE_REMOTELY[] = L"__invokeRemotely";

const wchar_t CLASS_REPLICATION_ENCODER[] = L"ReplicationEncoder";
const wchar_t CLASS_REPLICATION_DECODER[] = L"ReplicationDecoder";

struct AsyncIdAccepterContextTraits : ContextTraitsBase<tree::VariableDecl>
{
    static bool isValid(tree::VariableDecl* node);
};

using AsyncIdAccepterContext = ContextBase<AsyncIdAccepterContextTraits>;

const tree::FunctionDecl& locate_function(const tree::Block& block)
{
    const tree::FunctionDecl* owner_func = nullptr;

    {
        owner_func = block.getOwner<tree::FunctionDecl>();

        BOOST_ASSERT(owner_func && "block is not under any function");

        while (owner_func->is_lambda)
        {
            owner_func = owner_func->getOwner<tree::FunctionDecl>();

            BOOST_ASSERT(owner_func && "lambda function declaration is not under any function");
        }
    }

    BOOST_ASSERT(owner_func && !owner_func->is_lambda && "invalid block");

    return *owner_func;
}

bool may_adaptor_decl_conflict(const tree::AsyncBlock& origin_block)
{
    const auto*const owner_func = origin_block.getOwner<tree::FunctionDecl>();
    BOOST_ASSERT(owner_func != nullptr && "block is not under any function declaration!");

    return owner_func->may_conflict;
}

tree::DeclarativeStmt* create_remote_adaptor_var_stmt(tree::SimpleIdentifier* id, tree::TypeSpecifier* ts)
{
    BOOST_ASSERT(ts && "null pointer exception");

    return new tree::DeclarativeStmt(
        new tree::VariableDecl(
            id,
            ts,
            false, false, false,
            tree::VariableDecl::VisibilitySpecifier::DEFAULT
        )
    );
}

tree::VariableDecl* extract_variable_decl(const Statement* stmt)
{
    BOOST_ASSERT(                                                    stmt                && "null pointer exception");
    BOOST_ASSERT(                         isa<tree::DeclarativeStmt>(stmt)               && "unexpected statement");
    BOOST_ASSERT(                        cast<tree::DeclarativeStmt>(stmt)->declaration  && "null pointer exception");
    BOOST_ASSERT(isa<tree::VariableDecl>(cast<tree::DeclarativeStmt>(stmt)->declaration) && "unexpected statement");

    return cast<tree::VariableDecl>(cast<tree::DeclarativeStmt>(stmt)->declaration);
}

tree::DeclarativeStmt* create_captured_var_stmt(const tree::VariableDecl* node)
{
    BOOST_ASSERT(node && "null pointer exception");

    const tree::Type* var_type = node->getCanonicalType();

    BOOST_ASSERT(var_type && "we need type for variable declaration");

    return create_remote_adaptor_var_stmt(
        node->name->getSimpleId()->clone(),
        tree::ASTNodeHelper::createTypeSpecifierFrom(var_type)
    );
}

template<typename captured_range_type>
std::tuple<tree::VariableDecl*, std::vector<tree::VariableDecl*>> create_captured_vars(const tree::ClassDecl* owner_cls, tree::Block& block, const captured_range_type& captured_range)
{
    using boost::adaptors::transformed;

    tree::VariableDecl*              instance_var = nullptr;
    std::vector<tree::VariableDecl*> captured_vars;

    if (owner_cls != nullptr)
    {
        block.appendObject(create_remote_adaptor_var_stmt(
            tree::Identifier::randomId(L"instance_"),
            new tree::NamedSpecifier(owner_cls->name->clone())
        ));

        instance_var = extract_variable_decl(block.objects.back());
    }

    block.appendObjects(
          captured_range
        | transformed(&create_captured_var_stmt)
    );

    BOOST_ASSERT(block.objects.size() == captured_range.size() + (owner_cls == nullptr ? 1u : 2u) && "incorrect intermediate count of statement");

    boost::push_back(
        captured_vars,
          boost::make_iterator_range(
              boost::next(block.objects.cbegin(), owner_cls == nullptr ? 1 : 2),
                          block.objects.cend()
          )
        | transformed(&extract_variable_decl)
    );

    return std::make_tuple(instance_var, std::move(captured_vars));
}

void create_captured_decoders(const tree::VariableDecl& decoder_var, const tree::VariableDecl& is_success_var, const std::vector<tree::VariableDecl*>& captured_vars, tree::Block& block)
{
    for (const tree::VariableDecl* captured_var : captured_vars)
    {
        BOOST_ASSERT(captured_var && "null pointer exception");

        // is_success, captured = decoder.get(captured);
        tree::ExpressionStmt* decode_stmt = new tree::ExpressionStmt(
            new tree::BinaryExpr(
                tree::BinaryExpr::OpCode::ASSIGN,
                new tree::TieExpr(
                    {
                        new tree::IdExpr(is_success_var.name->clone()),
                        new tree::IdExpr(captured_var->name->clone())
                    }
                ),
                new tree::CallExpr(
                    tree::Expression::create({decoder_var.name->toString(), L"get"}),
                    {new tree::IdExpr(captured_var->name->clone())}
                )
            )
        );

        // if (!is_success) return;
        tree::IfElseStmt*  verify_stmt  = new tree::IfElseStmt(
            new tree::Selection(
                new tree::UnaryExpr(
                    tree::UnaryExpr::OpCode::LOGICAL_NOT,
                    new tree::IdExpr(is_success_var.name->clone())
                ),
                new tree::NormalBlock({new tree::BranchStmt(tree::BranchStmt::OpCode::RETURN)})
            )
        );

        block.appendObject(decode_stmt);
        block.appendObject(verify_stmt);
    }
}

tree::ExpressionStmt* create_remote_adapted_call(const tree::VariableDecl* instance_var, const tree::FunctionDecl& adapted_func, const std::vector<tree::VariableDecl*>& captured_vars)
{
    tree::CallExpr* adapted_call =
        instance_var != nullptr ? new tree::CallExpr(tree::Expression::create({instance_var->name->toString(), adapted_func.name->toString()}))
                                : new tree::CallExpr(tree::Expression::create({                                adapted_func.name->toString()}))
    ;

    for (const tree::VariableDecl* captured_var : captured_vars)
    {
        BOOST_ASSERT(captured_var && "null pointer exception");
        BOOST_ASSERT(captured_var->name && "null pointer exception");

        adapted_call->appendParameter(new tree::IdExpr(captured_var->name->clone()));
    }

    return new tree::ExpressionStmt(adapted_call);
}

tree::Expression* create_variable_encode(const tree::VariableDecl& encoder_var, const tree::VariableDecl* var)
{
    BOOST_ASSERT(encoder_var.name && "null pointer exception");
    BOOST_ASSERT(var && "null pointer exception");
    BOOST_ASSERT(var->name && "null pointer exception");

    return new tree::CallExpr(
        tree::Expression::create({encoder_var.name->toString(), L"put"}),
        {new tree::IdExpr(var->name->clone())}
    );
}

template<typename captured_range_type>
tree::FunctionDecl* create_remote_adaptor(const Architecture& arch, const tree::ASTNode& source_ref, const tree::ClassDecl* owner_cls, const tree::FunctionDecl& adapted_func, const captured_range_type& captured_range)
{
    tree::NormalBlock*  adaptor_block = new tree::NormalBlock();
    tree::FunctionDecl* adaptor_decl  = new tree::FunctionDecl(
        new tree::SimpleIdentifier(adapted_func.name->getSimpleId()->name + L"_remote_adaptor"),
        new tree::PrimitiveSpecifier(tree::PrimitiveKind::VOID_TYPE),
        false, false, false, false,
        tree::FunctionDecl::VisibilitySpecifier::DEFAULT,
        adaptor_block
    );

    adaptor_decl->arch = arch;

    if (owner_cls != nullptr)
    {
        adaptor_decl->is_member  = true;
        adaptor_decl->is_static  = true;
        adaptor_decl->visibility = tree::FunctionDecl::VisibilitySpecifier::PRIVATE;
    }

    adaptor_decl->setAnnotations(new tree::Annotations({new tree::Annotation(new tree::SimpleIdentifier(L"export"))}));

    tree::VariableDecl* decoder_var = new tree::VariableDecl(
        tree::Identifier::randomId(L"decoder_"),
        new tree::NamedSpecifier(tree::ASTNodeHelper::createThorLangId(&adapted_func, new tree::SimpleIdentifier(CLASS_REPLICATION_DECODER))),
        false, false, false,
        tree::VariableDecl::VisibilitySpecifier::DEFAULT
    );

    adaptor_decl->appendParameter(decoder_var);

    tree::VariableDecl*              instance_var = nullptr;
    std::vector<tree::VariableDecl*> captured_vars;

    if (owner_cls != nullptr || !captured_range.empty())
    {
        adaptor_block->appendObject(create_remote_adaptor_var_stmt(
            tree::Identifier::randomId(L"is_success_"),
            new tree::PrimitiveSpecifier(tree::PrimitiveKind::BOOL_TYPE)
        ));

        const tree::VariableDecl* is_success_var = extract_variable_decl(adaptor_block->objects.front());

        std::tie(instance_var, captured_vars) = create_captured_vars(owner_cls, *adaptor_block, captured_range);

        BOOST_ASSERT(is_success_var && "null pointer exception");

        create_captured_decoders(*decoder_var, *is_success_var, captured_vars, *adaptor_block);
    }

    // if (!decoder.isEnded()) return;
    adaptor_block->appendObject(
        new tree::IfElseStmt(
            new tree::Selection(
                new tree::UnaryExpr(
                    tree::UnaryExpr::OpCode::LOGICAL_NOT,
                    new tree::CallExpr(
                        tree::Expression::create(
                            {
                                decoder_var->name->toString(),
                                L"isEnded"
                            }
                        )
                    )
                ),
                new tree::NormalBlock({new tree::BranchStmt(tree::BranchStmt::OpCode::RETURN)})
            )
        )
    );

    adaptor_block->appendObject(create_remote_adapted_call(instance_var, adapted_func, captured_vars));

    tree::ASTNodeHelper::propogateSourceInfoRecursively(*adaptor_decl, source_ref);

    return adaptor_decl;
}

tree::VariableDecl* create_capatured_parameter(const tree::VariableDecl* captured_var)
{
    BOOST_ASSERT(captured_var && "null pointer exception");
    BOOST_ASSERT(captured_var->name && "null pointer exception");

    const Type* captured_type = captured_var->getCanonicalType();

    BOOST_ASSERT(captured_type && "null pointer exception");

    return new tree::VariableDecl(
        captured_var->name->clone(),
        tree::ASTNodeHelper::createTypeSpecifierFrom(captured_type),
        false, false, false,
        Declaration::VisibilitySpecifier::DEFAULT
    );
}

template<typename captured_range_type>
tree::FunctionDecl* create_remote_adapted(const Architecture& arch, const tree::ClassDecl* owner_cls, const tree::Block& block, const captured_range_type& captured_range)
{
    using boost::adaptors::transformed;

    const tree::FunctionDecl& owner_func = locate_function(block);

    tree::NormalBlock*  adapted_block = new tree::NormalBlock();
    tree::FunctionDecl* adapted_decl  = new tree::FunctionDecl(
        tree::Identifier::randomId(owner_func.name->getSimpleId()->name + L"_remote_adapted_"),
        new tree::PrimitiveSpecifier(tree::PrimitiveKind::VOID_TYPE),
        false, false, false, false,
        tree::FunctionDecl::VisibilitySpecifier::DEFAULT,
        adapted_block
    );

    adapted_decl->arch = arch;

    if (owner_cls != nullptr)
    {
        adapted_decl->is_member  = true;
        adapted_decl->visibility = tree::FunctionDecl::VisibilitySpecifier::PRIVATE;
    }

    adapted_decl->appendParameters(
          captured_range
        | transformed(&create_capatured_parameter)
    );

    tree::ASTNodeHelper::propogateSourceInfoRecursively(*adapted_decl, block);

    adapted_block->appendObjects(block.objects);

    tree::ASTNodeHelper::foreachApply<tree::ASTNode>(
        *adapted_block,
        [adapted_decl, &captured_range](tree::ASTNode& node)
        {
            tree::ASTNode*      resolved_symbol     = ResolvedSymbol::get(&node);
            tree::VariableDecl* resolved_symbol_var = cast_or_null<VariableDecl>(resolved_symbol);

            if (resolved_symbol_var == nullptr)
                return;

            const auto& pos = captured_range.find(resolved_symbol_var);

            if (pos == captured_range.end())
                return;

            const std::size_t   offset     = static_cast<std::size_t>(std::distance(captured_range.begin(), pos));
            tree::VariableDecl* new_symbol = adapted_decl->parameters[offset];

            ResolvedSymbol::set(&node, new_symbol);
        }
    );

    return adapted_decl;
}

template<typename captured_range_type>
tree::Block* create_encoding_block(const tree::ClassDecl* owner_cls, const tree::Block& block, tree::Expression& target, tree::FunctionDecl& adaptor_func, const captured_range_type& captured_range)
{
    using boost::adaptors::transformed;

    tree::NormalBlock*  encoder_block = new tree::NormalBlock(block.arch);
    tree::UnaryExpr*    encoder_init  = new tree::UnaryExpr(
                                            tree::UnaryExpr::OpCode::NEW,
                                            new tree::CallExpr(
                                                tree::ASTNodeHelper::useThorLangId(&block, new tree::SimpleIdentifier(CLASS_REPLICATION_ENCODER))
                                            )
                                        );
    tree::VariableDecl* encoder_var   = new tree::VariableDecl(
        tree::Identifier::randomId(L"encoder_"),
        new NamedSpecifier(tree::ASTNodeHelper::createThorLangId(&block, new tree::SimpleIdentifier(CLASS_REPLICATION_ENCODER))),
        false, false, false,
        tree::VariableDecl::VisibilitySpecifier::DEFAULT
    );

    encoder_block->appendObjects(visitor::restructure_helper::createVarWithInit(*encoder_var, *encoder_init));

    tree::Expression* invocation_condition = new tree::NumericLiteral(true);

    if (owner_cls != nullptr)
    {
        invocation_condition = tree::BinaryExpr::create<'&&'>(
            invocation_condition,
            new tree::CallExpr(
                tree::Expression::create({encoder_var->name->toString(), L"put"}),
                {new tree::ObjectLiteral(tree::ObjectLiteral::LiteralType::THIS_OBJECT)}
            )
        );
    }

    invocation_condition = boost::accumulate(
          captured_range
        | transformed(
              std::bind(
                  &create_variable_encode,
                  std::ref(*encoder_var),
                  std::placeholders::_1
              )
          ),
        invocation_condition,
        &tree::BinaryExpr::create<'&&'>
    );

    tree::FunctionIdLiteral* adaptor_id_literal = new tree::FunctionIdLiteral(L"remote_remote_adaptor");

    ResolvedSymbol::set(adaptor_id_literal, &adaptor_func);

    tree::Expression* adaptor_invocation = new tree::CallExpr(
        tree::ASTNodeHelper::useThorLangId(&block, new tree::SimpleIdentifier(FUNCTION_INVOKE_REMOTELY)),
        {
            &target,
            adaptor_id_literal,
            new tree::TernaryExpr(
                invocation_condition,
                new tree::IdExpr(encoder_var->name->clone()),
                new tree::ObjectLiteral(tree::ObjectLiteral::LiteralType::NULL_OBJECT)
            )
        }
    );

    encoder_block->appendObject(new tree::ExpressionStmt(adaptor_invocation));

    tree::ASTNodeHelper::foreachApply<ASTNode>(
        *encoder_block,
        std::bind(&tree::ASTNodeHelper::propogateSourceInfo, std::placeholders::_1, std::cref(block)),
        [&target](ASTNode& node)
        {
            return &node != &target;
        }
    );

    block.parent->replaceUseWith(block, *encoder_block);

    visitor::restructure_helper::restructureUnaryNewDirect(*encoder_init);

    return encoder_block;
}

tree::FunctionDecl* prepare_async_adapted_function(const Architecture& arch, const tree::Block& block)
{
    /**
     *  @code
     *  function callee_async_adapted(): void {
     *      // rootset remover
     *  }
     *  @endcode
     */
    const tree::FunctionDecl& owner_func = locate_function(block);

    BOOST_ASSERT(owner_func.name                != nullptr && "null pointer exception");
    BOOST_ASSERT(owner_func.name->getSimpleId() != nullptr && "null pointer exception");

    tree::FunctionDecl*const adapted_func  = new tree::FunctionDecl(
        tree::Identifier::randomId(owner_func.name->getSimpleId()->name + L"_async_adapted_"),
        new tree::PrimitiveSpecifier(tree::PrimitiveKind::VOID_TYPE),
        false, false, false, false,
        tree::FunctionDecl::VisibilitySpecifier::PRIVATE,
        new tree::NormalBlock
    );

    adapted_func->arch = arch;

    return adapted_func;
}

std::pair<tree::AsyncBlock*, tree::CallExpr*> prepare_async_adapted_caller(const Architecture& arch, const tree::AsyncBlock& origin_block, const tree::Identifier& adapted_func_name)
{
    auto*const async_call   = new tree::CallExpr(new tree::IdExpr(adapted_func_name.clone()));
    auto*const async_stmt   = new tree::ExpressionStmt(async_call);
    auto*const caller_block = new tree::AsyncBlock({async_stmt}, arch);

    caller_block->is_implicit = true;

    if (auto*const id_accepter_decl = AsyncIdAccepterContext::get(&origin_block))
        AsyncIdAccepterContext::set(caller_block, id_accepter_decl);

    return {caller_block, async_call};
}

void append_adapted_function_parameter(const tree::VariableDecl& captured_var, tree::FunctionDecl& adapted_func)
{
    const auto*const param_type = captured_var.getCanonicalType();

    BOOST_ASSERT(param_type != nullptr && "no canonical type");

    auto*const param_ts  = tree::ASTNodeHelper::createTypeSpecifierFrom(param_type);

    adapted_func.appendParameter(
        new tree::VariableDecl(
            captured_var.name->clone(),
            param_ts,
            false, false, captured_var.is_const,
            tree::VariableDecl::VisibilitySpecifier::DEFAULT
        )
    );
}

void append_adapted_call_expr_parameter(const tree::VariableDecl& captured_var, tree::CallExpr& call_expr)
{
    NOT_NULL(captured_var.name);

    call_expr.appendParameter(new tree::IdExpr(captured_var.name->clone()));
}

template<typename captured_range_type>
std::pair<tree::FunctionDecl*, tree::AsyncBlock*> create_adapted_async_call(const tree::AsyncBlock& block, const tree::ClassDecl*const owner_cls, const captured_range_type& captured_range)
{
    using boost::adaptors::indirected;

    const Architecture& arch = block.getActualArch();

    tree::FunctionDecl* adapted_func_decl = nullptr;
    tree::AsyncBlock*   adapted_caller    = nullptr;
    tree::CallExpr*     adapted_call_expr = nullptr;

    adapted_func_decl                           = prepare_async_adapted_function(arch, block);
    std::tie(adapted_caller, adapted_call_expr) = prepare_async_adapted_caller(arch, block, *adapted_func_decl->name);

    for (const auto& captured_var : captured_range | indirected)
    {
        append_adapted_function_parameter(captured_var, *adapted_func_decl);
        append_adapted_call_expr_parameter(captured_var, *adapted_call_expr);
    }

    tree::ASTNodeHelper::propogateSourceInfoRecursively(*adapted_func_decl, block);
    tree::ASTNodeHelper::propogateSourceInfoRecursively(*adapted_caller   , block);

    return {adapted_func_decl, adapted_caller};
}

/**
 *  Transform the following code
 *  @code
 *  function callee(i32: int32, f32: float32, bar: Bar): void {...}
 *  function caller(): void {
 *      var i8 : int8    = ...;
 *      var f32: float32 = ...;
 *      var bar: Bar     = ...;
 *      ...
 *      async -> {
 *          ...
 *          callee(i8, f32, bar);
 *          ...
 *      }
 *  }
 *  @endcode
 *  into
 *  @code
 *  function callee(i32: int32, f32: float32, bar: Bar): void {...}
 *  function caller_async_adapted(i64: int64, cf32: float32, bar: Bar): void {
 *      ...
 *      callee(i64, cf32, bar);
 *      ...
 *  }
 *  function caller(): void {
 *      var    i64: int64   = ...;
 *      const cf32: float32 = ...;
 *      var    bar: Bar     = ...;
 *      ...
 *      async -> caller_async_adapted(i64, cf32, bar); // this line should be restructed by next iteration
 *  }
 *  @endcode
 */
void build_local_adapted_async_call(tree::AsyncBlock& block)
{
    std::unordered_set<tree::VariableDecl*> captured_vars;
    tree::ClassDecl*                        captured_cls = nullptr;

    std::tie(captured_vars, captured_cls) = visitor::restructure_helper::getCaptureInfo(block);

    tree::FunctionDecl* adapted_func = nullptr;
    tree::AsyncBlock*   caller_block = nullptr;
    const auto&         may_conflict = may_adaptor_decl_conflict(block);

    std::tie(adapted_func, caller_block) = create_adapted_async_call(block, captured_cls, captured_vars);

    BOOST_ASSERT(adapted_func != nullptr && "null pointer exception");
    BOOST_ASSERT(caller_block != nullptr && "null pointer exception");

    tree::ASTNode*const owner = tree::ASTNodeHelper::insertIntoOwnerNameScope(block, *adapted_func);

    BOOST_ASSERT(owner != nullptr && "no scope is found");

    if (tree::isa<tree::ClassDecl>(owner))
    {
        adapted_func->is_member = true;

        if (captured_cls == nullptr)
            adapted_func->is_static = true;
    }

    if (may_conflict)
        adapted_func->may_conflict = true;

    tree::ASTNodeHelper::foreachApply<ASTNode>(
        block,
        [&captured_vars, captured_cls](ASTNode& node)
        {
            auto*const resolved_symbol = ResolvedSymbol::get(&node);

            if (captured_vars.count(tree::cast_or_null<tree::VariableDecl>(resolved_symbol)) != 0)
            {
                ResolvedType  ::set(&node, nullptr);
                ResolvedSymbol::set(&node, nullptr);
            }
        }
    );

    adapted_func->block->appendObjects(block.objects);
    block.parent->replaceUseWith(block, *caller_block);
}

std::uint32_t get_implicit_async_flags_impl(const tree::AsyncBlock& block, const std::tuple<bool, tree::CallExpr*, tree::BinaryExpr*>& info)
{
    using tree::cast_or_null;

    std::uint32_t flags = async_invalid_flags::OK;

    const auto& has_cast = std::get<0>(info);
    auto*const  call     = std::get<1>(info);
    auto*const  binary   = std::get<2>(info);

    if (has_cast)
        flags |= async_invalid_flags::FOUND_CAST;

    if (call == nullptr)
        flags |= async_invalid_flags::NOT_A_CALL;

    if (binary != nullptr)
    {
        if (binary->opcode != tree::BinaryExpr::OpCode::ASSIGN)
            flags |= async_invalid_flags::NOT_A_ASSIGN;

        NOT_NULL(binary->left);

        const auto*const lhs     = binary->left;
        const auto*const lhs_sym = cast_or_null<tree::VariableDecl>(tree::ASTNodeHelper::getCanonicalSymbol(lhs));

        const auto& is_valid_assign =
             lhs_sym != nullptr &&
            !lhs_sym->isEnumerator() &&
            !lhs_sym->is_const &&
            (
                // Test if such symbol is on global/heap address space, which will be valid after current invocation
                lhs_sym->isGlobal() ||
                lhs_sym->is_member
            )
        ;

        if (!is_valid_assign)
            flags |= async_invalid_flags::INVALID_ASSIGN;
    }

    return flags;
}

std::tuple<bool, tree::CallExpr*, tree::BinaryExpr*> get_implicit_async_info(const tree::AsyncBlock& block)
{
    using tree::cast;

    BOOST_ASSERT(block.is_implicit && "non-implicit async should not be here");
    BOOST_ASSERT(block.objects.size() == 1 && "implicit block should contain exactly one statement");

    if (block.objects.size() != 1)
        return std::make_tuple(false, nullptr, nullptr);

    NOT_NULL(block.objects.front());

    const auto*const expr_stmt = cast<tree::ExpressionStmt>(block.objects.front());

    if (expr_stmt == nullptr)
        return std::make_tuple(false, nullptr, nullptr);

    NOT_NULL(expr_stmt->expr);

    auto*const             expr = expr_stmt->expr;
    bool          has_cast_expr = false;
    auto*const      binary_expr = cast<tree::BinaryExpr>(expr);
    auto*      top_of_call_expr = binary_expr == nullptr ? expr : binary_expr->right;

    while (true)
    {
        NOT_NULL(top_of_call_expr);

        if (auto*const call_expr = cast<tree::CallExpr>(top_of_call_expr))
        {
            return std::make_tuple(has_cast_expr, call_expr, binary_expr);
        }
        else if (auto*const cast_expr = cast<tree::CastExpr>(top_of_call_expr))
        {
               has_cast_expr = true;
            top_of_call_expr = cast_expr->node;
        }
        else
        {
            break;
        }
    }

    return std::make_tuple(has_cast_expr, nullptr, binary_expr);
}

std::tuple<
    boost::optional<tree::Expression*>, // object for member function call ('null' means this pointer is required)
    tree::Expression*                 , // expression to be kept
    tree::FunctionDecl*                 // callee
> get_implicit_async_state(tree::AsyncBlock& block, tree::CallExpr& call_expr)
{
    NOT_NULL(call_expr.node);

    BOOST_ASSERT((tree::isa<tree::IdExpr, tree::MemberExpr>(call_expr.node)) && "CallExpr's child must be IdExpr or MemberExpr");

          auto*const func_name_expr        = call_expr.node;
    const auto*const func_name_expr_member = tree::cast<tree::MemberExpr>(func_name_expr);
          auto*const callee_decl           = tree::cast_or_null<tree::FunctionDecl>(tree::ASTNodeHelper::getCanonicalSymbol(func_name_expr));

    BOOST_ASSERT(callee_decl != nullptr && "no resolved symbol to declaration of callee");

    const auto& callee_is_non_static_member = callee_decl->is_member && !callee_decl->is_static;
    const auto& has_caller_provided_value   = func_name_expr_member != nullptr && func_name_expr_member->node->hasValue();

    boost::optional<tree::Expression*> optional_expr_obj;
    tree::Expression*                           expr_kept = nullptr;

    if (callee_is_non_static_member)
        optional_expr_obj  = has_caller_provided_value ? func_name_expr_member->node : nullptr;
    else if (has_caller_provided_value)
                 expr_kept = func_name_expr_member->node;

    return std::make_tuple(std::move(optional_expr_obj), std::move(expr_kept), callee_decl);
}

tree::Expression* create_async_id_assignment(const tree::AsyncBlock& block, tree::Expression* origin_expr)
{
    const auto*const id_accepter_decl = AsyncIdAccepterContext::get(&block);

    if (id_accepter_decl == nullptr)
        return origin_expr;

    NOT_NULL(id_accepter_decl);
    BOOST_ASSERT(tree::isa<tree::SimpleIdentifier>(id_accepter_decl->name) && "variable's name should be simple identifier");

    auto*const accepter_id = id_accepter_decl->name->getSimpleId();

    NOT_NULL(accepter_id);

    auto*const cloned_id   = tree::ASTNodeHelper::createWithSourceInfo<tree::SimpleIdentifier>(block, accepter_id->name);
    auto*const id_expr     = tree::ASTNodeHelper::createWithSourceInfo<tree::IdExpr          >(block, cloned_id);
    auto*const assign_expr = tree::ASTNodeHelper::createWithSourceInfo<tree::BinaryExpr      >(block, tree::BinaryExpr::OpCode::ASSIGN, id_expr, origin_expr);

    return assign_expr;
}

/**
 *  transforms the following code
 *  @code
 *  // package pkg_a
 *  function callee_global(...) {...}
 *
 *  class bar
 *  {
 *      public function callee_member(...) {...}
 *      public function callee_static(...) {...}
 *  }
 *
 *  // package pkg_b
 *  import .= pkg_b;
 *
 *  class foo extends bar
 *  {
 *      public function callee_member(...) {...}
 *      public function callee_static(...) {...}
 *
 *      public function caller()
 *      {
 *          const vfoo: foo = ...;
 *
 *          async ->            callee_global(...); // ref global function
 *          async -> pkg_a.     callee_global(...); // ref global function
 *          async ->            callee_member(...); // ref member function
 *          async ->        foo.callee_member(...); // ref member function
 *          async ->       vfoo.callee_member(...); // ref member function
 *          async ->        bar.callee_member(...); // ref member function
 *          async -> pkg_a. bar.callee_member(...); // ref member function
 *          async ->            callee_static(...); // ref static function
 *          async ->        foo.callee_static(...); // ref static function
 *          async ->       vfoo.callee_static(...); // ref static function
 *          async ->        bar.callee_static(...); // ref static function
 *          async -> pkg_a. bar.callee_static(...); // ref static function
 *      }
 *  }
 *  @endcode
 *  into
 *  @code
 *  // package pkg_a
 *  function callee_global(...) {...}
 *
 *  class bar
 *  {
 *      public function callee_member(...) {...}
 *      public function callee_static(...) {...}
 *  }
 *
 *  // package pkg_b
 *  import .= pkg_b;
 *
 *  class foo extends bar
 *  {
 *      public function callee_member(...) {...}
 *      public function callee_static(...) {...}
 *
 *      public function caller()
 *      {
 *          const vfoo: foo = ...;
 *
 *          {       <sys-call: async>(<func-id: pkg_a.    callee_global>(),       ...); } // ref global function
 *          {       <sys-call: async>(<func-id: pkg_a.    callee_global>(),       ...); } // ref global function
 *          {       <sys-call: async>(<func-id: pkg_b.foo.callee_member>(), this, ...); } // ref member function
 *          {       <sys-call: async>(<func-id: pkg_b.foo.callee_member>(), this, ...); } // ref member function
 *          {       <sys-call: async>(<func-id: pkg_b.foo.callee_member>(), vfoo, ...); } // ref member function
 *          {       <sys-call: async>(<func-id: pkg_a.bar.callee_member>(), this, ...); } // ref member function
 *          {       <sys-call: async>(<func-id: pkg_a.bar.callee_member>(), this, ...); } // ref member function
 *          {       <sys-call: async>(<func-id: pkg_b.foo.callee_static>(),       ...); } // ref static function
 *          {  foo; <sys-call: async>(<func-id: pkg_b.foo.callee_static>(),       ...); } // ref static function
 *          { vfoo; <sys-call: async>(<func-id: pkg_b.foo.callee_static>(),       ...); } // ref static function
 *          {       <sys-call: async>(<func-id: pkg_a.bar.callee_static>(),       ...); } // ref static function
 *          {       <sys-call: async>(<func-id: pkg_a.bar.callee_static>(),       ...); } // ref static function
 *      }
 *  }
 *  @endcode
 */
void build_local_implicit_async_call(tree::AsyncBlock& block, tree::CallExpr& call_expr, tree::Expression* assignee_expr)
{
    boost::optional<tree::Expression*> optional_expr_obj;
    tree::Expression*                           expr_kept   = nullptr;
    tree::FunctionDecl*                         callee_decl = nullptr;

    std::tie(optional_expr_obj, expr_kept, callee_decl) = get_implicit_async_state(block, call_expr);

    const auto*const id_accepter_decl = AsyncIdAccepterContext::get(&block);

    auto*const new_block  = tree::ASTNodeHelper::createWithSourceInfo<tree::NormalBlock   >(block, block.arch);
    auto*const async_call = tree::ASTNodeHelper::createWithSourceInfo<tree::SystemCallExpr>(block, tree::SystemCallExpr::CallType::ASYNC);
    auto*const async_expr = create_async_id_assignment(block, async_call);
    auto*const async_stmt = tree::ASTNodeHelper::createWithSourceInfo<tree::ExpressionStmt>(block, async_expr);

    auto*const func_id_expr = tree::ASTNodeHelper::createWithSourceInfo<tree::FunctionIdLiteral>(call_expr, L"implicit_async_target");

    ResolvedSymbol::set(func_id_expr, callee_decl);

    if (expr_kept != nullptr)
        new_block->appendObjects(
            {
                tree::ASTNodeHelper::createWithSourceInfo<tree::ExpressionStmt>(*expr_kept, expr_kept),
                async_stmt
            }
        );
    else
        new_block->appendObject(async_stmt);

    async_call->appendParameter(assignee_expr);
    async_call->appendParameter( func_id_expr);

    if (optional_expr_obj)
    {
        auto* expr_obj = *optional_expr_obj;

        if (expr_obj == nullptr)
            expr_obj = tree::ASTNodeHelper::createWithSourceInfo<tree::ObjectLiteral>(call_expr, tree::ObjectLiteral::LiteralType::THIS_OBJECT);

        async_call->appendParameter(expr_obj);
    }

    async_call->appendParameters(call_expr.parameters);

    block.parent->replaceUseWith(block, *new_block);

    if (!callee_decl->hasAnnotation(L"export"))
        callee_decl->addAnnotation(
            tree::ASTNodeHelper::createWithSourceInfo<tree::Annotation>(
                *callee_decl,
                tree::ASTNodeHelper::createWithSourceInfo<tree::SimpleIdentifier>(
                    *callee_decl,
                    L"export"
                )
            )
        );
}

}

bool AsyncIdAccepterContextTraits::isValid(tree::VariableDecl* node)
{
    return node != nullptr;
}

std::uint32_t get_async_flags(const tree::AsyncBlock& block)
{
    if (!block.is_implicit)
        return async_invalid_flags::OK;

    const auto& info = get_implicit_async_info(block);

    return get_implicit_async_flags_impl(block, info);
}

void verify_async(const tree::AsyncBlock& async_block)
{
    const auto& flags = get_async_flags(async_block);

    if (flags == async_invalid_flags::OK)
        return;

    BOOST_ASSERT(!async_block.objects.empty() && "async should contain at least one statement");

    if (flags & async_invalid_flags::NOT_A_CALL    ) LOG_MESSAGE(INVALID_IMPLCIT_ASYNC_CALL_NOT_FOUND        , async_block.objects.front());
    if (flags & async_invalid_flags::NOT_A_ASSIGN  ) LOG_MESSAGE(INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN, async_block.objects.front());
    if (flags & async_invalid_flags::INVALID_ASSIGN) LOG_MESSAGE(INVALID_IMPLCIT_ASYNC_INVALID_LHS           , async_block.objects.front());
    if (flags & async_invalid_flags::FOUND_CAST    ) LOG_MESSAGE(INVALID_IMPLCIT_ASYNC_FOUND_CAST            , async_block.objects.front());
}

std::tuple<
    const tree::VariableDecl*, // declaration of assignee
    tree::Expression*,         // LHS expression
    tree::Expression*          // RHS expression
> extract_assignment_info_from_asynced_stmt(tree::Statement& stmt)
{
    using tree::cast;
    using tree::cast_or_null;

    auto*const expr_stmt = cast<tree::ExpressionStmt>(&stmt);

    if (expr_stmt == nullptr)
        return std::make_tuple(nullptr, nullptr, nullptr);

    NOT_NULL(expr_stmt->expr);

    auto*const expr_binary = cast<tree::BinaryExpr>(expr_stmt->expr);

    if (expr_binary == nullptr)
        return std::make_tuple(nullptr, nullptr, nullptr);

    if (expr_binary->opcode != tree::BinaryExpr::OpCode::ASSIGN)
        return std::make_tuple(nullptr, nullptr, nullptr);

    NOT_NULL(expr_binary->left);

    const auto*const assignee_symbol = cast_or_null<tree::VariableDecl>(tree::ASTNodeHelper::getCanonicalSymbol(expr_binary->left));

    if (assignee_symbol == nullptr)
        return std::make_tuple(nullptr, nullptr, nullptr);

    if (assignee_symbol->isGlobal() || assignee_symbol->is_member)
        return std::make_tuple(nullptr, nullptr, nullptr);

    return std::make_tuple(assignee_symbol, expr_binary->left, expr_binary->right);
}

CallExpr* get_implicit_async_call(const tree::AsyncBlock& block)
{
    const auto& info = get_implicit_async_info(block);
    auto*const  call = std::get<1>(info);

    return call;
}

void set_async_alternative_id_var(tree::AsyncBlock& block, tree::VariableDecl& var)
{
    AsyncIdAccepterContext::set(&block, &var);
}

void build_local_async_call(tree::AsyncBlock& block)
{
    if (block.is_implicit)
    {
        const auto& info = get_implicit_async_info(block);

        if (get_implicit_async_flags_impl(block, info) == async_invalid_flags::OK)
        {
            auto*const call   = std::get<1>(info);
            auto*const binary = std::get<2>(info);

            NOT_NULL(call);

            build_local_implicit_async_call(block, *call, binary == nullptr ? nullptr : binary->left);
        }
    }
    else
    {
        build_local_adapted_async_call(block);
    }
}

/**
 *  Transform the following code
 *  @code
 *  function caller(): void {
 *      ...
 *      <async-block>(target){
 *          ...
 *      }
 *      ...
 *  }
 *  @endcode
 *  into
 *  @code
 *  @export
 *  function caller_remote_adaptor(decoder: thor.lang.ReplicationDecoder): void {
 *      var is_success: bool = false;
 *
 *      var a;
 *      ...
 *      var z;
 *
 *      is_success, a = decoder.get(a); if (!is_success) return;
 *      ...
 *      is_success, z = decoder.get(z); if (!is_success) return;
 *
 *      if (!decoder.is_ended())
 *          return;
 *
 *      caller_remote_impl(a, ..., z);
 *  }
 *  function caller_remote_adapted(a: type_a, ..., z: type_z): void {
 *      ...
 *  }
 *  function caller(): void {
 *      ...
 *      {
 *          var encoder = new thor.lang.ReplicationEncoder();
 *
 *          if (encoder.put(a) &&
 *              ...
 *              encoder.put(z))
 *          {
 *              thor.lang.__invokeRemotely(
 *                  target,
 *                  $remote_invoker_id$,
 *                  <funciton-id-literal>(caller_remote),
 *                  encoder
 *              );
 *          }
 *      }
 *      ...
 *  }
 *  @endcode
 */
void build_remote_async_call(tree::AsyncBlock& block, tree::Expression& target_expr)
{
    BOOST_ASSERT(block.getTarget() == &target_expr && "incorrect target of remote aysnc!");

    std::unordered_set<tree::VariableDecl*> captured_vars;
    tree::ClassDecl*                        captured_cls = nullptr;
    const auto&                             may_conflict = may_adaptor_decl_conflict(block);

    std::tie(captured_vars, captured_cls) = visitor::restructure_helper::getCaptureInfo(block, [&target_expr](tree::ASTNode& node) { return &target_expr != &node; });

    const auto append_declare = [&block, captured_cls](tree::FunctionDecl* decl)
    {
        if (captured_cls != nullptr)
        {
            captured_cls->addFunction(decl);
        }
        else
        {
            tree::Source* source = block.getOwner<tree::Source>();

            BOOST_ASSERT(source && "block is not under any source!");

            source->addDeclare(decl);
        }
    };

    const auto& actual_arch = block.getActualArch();

    tree::FunctionDecl* adapted = create_remote_adapted(actual_arch, captured_cls, block, captured_vars);
    append_declare(adapted);

    tree::FunctionDecl* adaptor = create_remote_adaptor(actual_arch, block, captured_cls, *adapted, captured_vars);
    append_declare(adaptor);

    if (may_conflict)
    {
        adapted->may_conflict = true;
        adaptor->may_conflict = true;
    }

    create_encoding_block(captured_cls, block, target_expr, *adaptor, captured_vars);
}

} } } }
