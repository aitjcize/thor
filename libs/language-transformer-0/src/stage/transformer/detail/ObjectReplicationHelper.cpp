/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <set>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include <boost/assert.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/scope_exit.hpp>

#include "language/context/TransformerContext.h"
#include "language/stage/transformer/detail/RestructureHelper.h"
#include "language/stage/transformer/detail/ObjectReplicationHelper.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Block.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/TypenameDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/expression/CallExpr.h"
#include "language/tree/expression/UnaryExpr.h"
#include "language/tree/module/Tangle.h"
#include "language/tree/statement/BranchStmt.h"

namespace zillians { namespace language { namespace stage { namespace visitor { namespace detail { namespace object_replication {

namespace {

tree::Identifier* create_use_id_for_class(const tree::ClassDecl& cls);

template<typename context_type, typename policy_type>
struct policy_base
{
    static bool is_processed(const tree::ClassDecl& cls);
    static void set_context(tree::ClassDecl& cls, std::vector<tree::FunctionDecl*>&& refs);
    static std::vector<tree::FunctionDecl*>* get_context(const tree::ClassDecl& cls);
};

struct creator_policy : public policy_base<ReplicatorCreatorContext, creator_policy>
{
    static constexpr const wchar_t* get_method_name();
    static constexpr unsigned       get_method_parameter_count();

    static tree::Type*              get_return_canonical_type(const tree::Tangle& tangle);
    static std::vector<tree::Type*> get_parameter_canonical_types(const tree::Tangle& tangle, tree::ClassDecl& cls);

    static tree::FunctionDecl* generate(tree::ClassDecl& cls);
};

struct serializer_policy : public policy_base<ReplicatorSerializerContext, serializer_policy>
{
    static constexpr const wchar_t* get_method_name();
    static constexpr unsigned       get_method_parameter_count();

    static constexpr const wchar_t* get_encoder_class_name();
    static constexpr const wchar_t* get_encoder_variable_name();
    static constexpr const wchar_t* get_instance_variable_name();

    static tree::Type*              get_return_canonical_type(const tree::Tangle& tangle);
    static std::vector<tree::Type*> get_parameter_canonical_types(const tree::Tangle& tangle, tree::ClassDecl& cls);

    static tree::FunctionDecl* generate(tree::ClassDecl& cls);

private:
    static tree::FunctionDecl* generate_declaration(const tree::ClassDecl& cls);
    static tree::Expression*   generate_base_serializer(const tree::ClassDecl& cls);
    static tree::Expression*   generate_member_variable_serializer(const tree::VariableDecl& member_variable);
};

struct deserializer_policy : public policy_base<ReplicatorDeserializerContext, deserializer_policy>
{
    static constexpr const wchar_t* get_method_name();
    static constexpr unsigned       get_method_parameter_count();

    static constexpr const wchar_t* get_decoder_class_name();
    static constexpr const wchar_t* get_decoder_variable_name();
    static constexpr const wchar_t* get_instance_variable_name();

    static tree::Type*              get_return_canonical_type(const tree::Tangle& tangle);
    static std::vector<tree::Type*> get_parameter_canonical_types(const tree::Tangle& tangle, tree::ClassDecl& cls);

    static tree::FunctionDecl* generate(tree::ClassDecl& cls); // not implemented

private:
    static tree::FunctionDecl* generate_declaration(const tree::ClassDecl& cls);
    static tree::Expression*   generate_base_deserializer(const tree::ClassDecl& cls);
    static tree::Expression*   generate_member_variable_deserializer(const tree::VariableDecl& member_variable);
    static tree::Statement*    generate_deserializing_unpacker(const tree::VariableDecl& flag, const tree::VariableDecl& member);
};

template<typename context_type, typename policy_type>
bool policy_base<context_type, policy_type>::is_processed(const tree::ClassDecl& cls)
{
    return context_type::is_inited(&cls);
}

template<typename context_type, typename policy_type>
void policy_base<context_type, policy_type>::set_context(tree::ClassDecl& cls, std::vector<tree::FunctionDecl*>&& refs)
{
    context_type::set(&cls, std::move(refs));
}

template<typename context_type, typename policy_type>
std::vector<tree::FunctionDecl*>* policy_base<context_type, policy_type>::get_context(const tree::ClassDecl& cls)
{
    return context_type::get(&cls);
}

constexpr const wchar_t* creator_policy::get_method_name()
{
    return L"__create";
}

constexpr unsigned creator_policy::get_method_parameter_count()
{
    return 0u;
}

tree::Type* creator_policy::get_return_canonical_type(const tree::Tangle& tangle)
{
    ClassDecl* thor_lang_object = tangle.findThorLangObject();
    BOOST_ASSERT( thor_lang_object != nullptr && "thor.lang.Object must be exist" );

    return thor_lang_object->getType();
}

std::vector<tree::Type*> creator_policy::get_parameter_canonical_types(const tree::Tangle& tangle, tree::ClassDecl& cls)
{
    return {};
}

tree::FunctionDecl* creator_policy::generate(tree::ClassDecl& cls)
{
    const tree::Tangle* tangle = cls.getOwner<tree::Tangle>();
    BOOST_ASSERT(tangle && "null pointer exception");

    tree::Identifier* class_use_id = create_use_id_for_class(cls);

    tree::UnaryExpr* new_expr =
        new tree::UnaryExpr(
            tree::UnaryExpr::OpCode::NEW,
            new tree::IdExpr(
                class_use_id
            )
        )
    ;

    tree::Block* creator_body = new tree::NormalBlock();
    creator_body->appendObject(
        new tree::BranchStmt(
            tree::BranchStmt::OpCode::RETURN,
            new_expr
        )
    );

    ClassDecl* thor_lang_object = tangle->findThorLangObject();
    BOOST_ASSERT(thor_lang_object != nullptr && "can not find thor.lang.Object declaration");

    tree::FunctionDecl* creator_decl = new tree::FunctionDecl(
        new tree::SimpleIdentifier(get_method_name()),
        new tree::NamedSpecifier( create_use_id_for_class(*thor_lang_object) ),
        true,
        true,
        false,
        false,
        tree::Declaration::VisibilitySpecifier::PUBLIC,
        creator_body
    );

    cls.addFunction(creator_decl);

    restructure_helper::restructureUnaryNewDirect(*new_expr);

    ResolvedType::set(creator_decl->type, thor_lang_object->getType());

    // nodes in creator have same source info as class
    ASTNodeHelper::foreachApply<ASTNode>( cls, [&cls]( ASTNode & node ) {
        ASTNodeHelper::propogateSourceInfo( node, cls );
    } );

    return creator_decl;
}

constexpr const wchar_t* serializer_policy::get_method_name()
{
    return L"serialize";
}

constexpr unsigned serializer_policy::get_method_parameter_count()
{
    return 2u;
}

constexpr const wchar_t* serializer_policy::get_encoder_class_name()
{
    return L"ReplicationEncoder";
}

constexpr const wchar_t* serializer_policy::get_encoder_variable_name()
{
    return L"encoder";
}

constexpr const wchar_t* serializer_policy::get_instance_variable_name()
{
    return L"instance";
}

tree::Type* serializer_policy::get_return_canonical_type(const tree::Tangle& tangle)
{
    BOOST_ASSERT(tangle.internal && "null pointer exception");
    BOOST_ASSERT(tangle.internal->getPrimitiveType(PrimitiveKind::BOOL_TYPE) && "null pointer exception");

    return tangle.internal->getPrimitiveType(PrimitiveKind::BOOL_TYPE);
}

std::vector<tree::Type*> serializer_policy::get_parameter_canonical_types(const tree::Tangle& tangle, tree::ClassDecl& cls)
{
    return  {
                tangle.findClass(L"thor.lang", get_encoder_class_name())->getType(),
                cls.getType()
            };
}

tree::FunctionDecl* serializer_policy::generate(tree::ClassDecl& cls)
{
    tree::FunctionDecl* serializer_decl = generate_declaration(cls);

    BOOST_ASSERT(serializer_decl && "null pointer exception");
    BOOST_ASSERT(serializer_decl->block && "null pointer exception");

    tree::Block* serializer_body = serializer_decl->block;

    tree::Expression* condition = new tree::NumericLiteral(true);

    const auto condition_ander = [&condition](tree::Expression* next_expression)
    {
        BOOST_ASSERT(next_expression && "null pointer exception");

        condition =
            new tree::BinaryExpr(
                tree::BinaryExpr::OpCode::LOGICAL_AND,
                condition,
                next_expression
            )
        ;
    };

    if (cls.base)
    {
        condition_ander(generate_base_serializer(cls));
    }

    for (const tree::VariableDecl* member_variable: cls.member_variables)
    {
        BOOST_ASSERT(member_variable && "null pointer exception");

        condition_ander(generate_member_variable_serializer(*member_variable));
    }

    serializer_body->appendObject(new tree::BranchStmt(tree::BranchStmt::OpCode::RETURN, condition));

    cls.addFunction(serializer_decl);

    // nodes in serializer have same source info as class
    ASTNodeHelper::foreachApply<ASTNode>( cls, [&cls]( ASTNode & node ) {
        ASTNodeHelper::propogateSourceInfo( node, cls );
    } );

    return serializer_decl;
}

tree::FunctionDecl* serializer_policy::generate_declaration(const tree::ClassDecl& cls)
{
    tree::FunctionDecl* serializer_decl = new tree::FunctionDecl(
        new tree::SimpleIdentifier(get_method_name()),
        new tree::PrimitiveSpecifier(tree::PrimitiveKind::BOOL_TYPE),
        true,
        true,
        false,
        false,
        tree::Declaration::VisibilitySpecifier::PROTECTED,
        new tree::NormalBlock()
    );

    tree::Identifier* encoder_id = ASTNodeHelper::createThorLangId(&cls, new tree::SimpleIdentifier(get_encoder_class_name()));

    auto parameters =
    {
        std::make_tuple(get_encoder_variable_name(), encoder_id),
        std::make_tuple(get_instance_variable_name(), create_use_id_for_class(cls))
    };

    for (const auto& parameter: parameters)
    {
        const wchar_t*    parameter_name       = std::get<0>(parameter);
        tree::Identifier* parameter_identifier = std::get<1>(parameter);

        serializer_decl->appendParameter(
            new tree::VariableDecl(
                new tree::SimpleIdentifier(parameter_name),
                new tree::NamedSpecifier(parameter_identifier),
                false,
                false,
                false,
                tree::Declaration::VisibilitySpecifier::DEFAULT
            )
        );
    }

    return serializer_decl;
}

tree::Expression* serializer_policy::generate_base_serializer(const tree::ClassDecl& cls)
{
    BOOST_ASSERT( cls.base != nullptr && "should have base class, at least thor.lang.Object" );

    tree::Identifier* base_identifier = cls.base->getName();
    BOOST_ASSERT( base_identifier != nullptr && "base class should have a name" );

    tree::CallExpr* call_base_serializer =
        new tree::CallExpr(
            new tree::MemberExpr(
                new tree::IdExpr( base_identifier->clone() ),
                new tree::SimpleIdentifier( get_method_name() )
            )
        )
    ;

    call_base_serializer->appendParameter(tree::Expression::create({get_encoder_variable_name()}));
    call_base_serializer->appendParameter(new tree::IdExpr(new tree::SimpleIdentifier(get_instance_variable_name())));

    return call_base_serializer;
}

tree::Expression* serializer_policy::generate_member_variable_serializer(const tree::VariableDecl& member_variable)
{
    tree::CallExpr* member_serializer =
        new tree::CallExpr(
            new tree::MemberExpr(
                tree::Expression::create({get_encoder_variable_name()}),
                new tree::SimpleIdentifier(L"put")
            )
        )
    ;

    BOOST_ASSERT(member_variable.name && "null pointer exception");
    BOOST_ASSERT(member_variable.name->getSimpleId() && "null pointer exception");

    const tree::SimpleIdentifier* member_variable_sid = member_variable.name->getSimpleId();
    member_serializer->appendParameter(tree::Expression::create({get_instance_variable_name(), member_variable_sid->name}));

    return member_serializer;
}

template<typename policy_type>
bool need_decl_impl(tree::ClassDecl& cls)
{
    BOOST_ASSERT(is_really_replicable(cls) && "you must ensure the class is replicable before using this function");

    bool                             has_member_variable_with_same_name = true;
    std::vector<tree::FunctionDecl*> conflict_member_functions;

    BOOST_SCOPE_EXIT_TPL(&has_member_variable_with_same_name, &conflict_member_functions, &cls)
    {
             if (has_member_variable_with_same_name) policy_type::set_context(cls, {});
        else if (!conflict_member_functions.empty()) policy_type::set_context(cls, std::move(conflict_member_functions));
    }
    BOOST_SCOPE_EXIT_END;

    has_member_variable_with_same_name = std::any_of(
        cls.member_variables.begin(),
        cls.member_variables.end(),
        [](const tree::VariableDecl* member_varaible)
        {
            BOOST_ASSERT(member_varaible && "null pointer exception");
            BOOST_ASSERT(member_varaible->name && "null pointer exception");

            const tree::SimpleIdentifier* member_varaible_sid = cast<const tree::SimpleIdentifier>(member_varaible->name);

            BOOST_ASSERT(member_varaible_sid && "null pointer exception");

            return member_varaible_sid->name == policy_type::get_method_name();
        }
    );

    if (has_member_variable_with_same_name)
        return false;

    for (tree::FunctionDecl* member_function: cls.member_functions)
    {
        BOOST_ASSERT(member_function && "null pointer exception");
        BOOST_ASSERT(member_function->name && "null pointer exception");

        const tree::SimpleIdentifier*    name_sid = member_function->name->getSimpleId();
        const tree::TemplatedIdentifier* name_tid = member_function->name->getTemplateId();

        BOOST_ASSERT(name_sid && "null pointer exception");

        const bool is_template_member_function = name_tid != nullptr;

        // FIXME implement more advanced logic to handle function overload during resolution
        const bool is_conflict =
            member_function->parameters.size() == policy_type::get_method_parameter_count() &&
            name_sid->name == policy_type::get_method_name() &&
            !is_template_member_function
        ;

        if (is_conflict)
        {
            conflict_member_functions.emplace_back(member_function);
        }
    }

    if (!conflict_member_functions.empty())
        return false;

    return true;
}

template<typename policy_type>
void create_decl_impl(tree::ClassDecl& cls)
{
    tree::FunctionDecl* generated_decl = policy_type::generate(cls);

    BOOST_ASSERT(generated_decl && "null pointer exception");

    ASTNodeHelper::foreachApply<ASTNode>(
        *generated_decl,
        [&cls](ASTNode& node)
        {
            if (!SourceInfoContext::get(&node))
            {
                ASTNodeHelper::propogateSourceInfo(node, cls);
            }
        }
    );

    policy_type::set_context(cls, {generated_decl});
}

template<typename policy_type>
verification_result::result_type verify_impl(std::set<tree::ClassDecl*>& persistent_state, tree::ClassDecl& cls)
{
    typedef verification_result::tag_state_non_changed      tag_state_non_changed;
    typedef verification_result::tag_variable_name_conflict tag_variable_name_conflict;

    bool is_verified = false;

    BOOST_SCOPE_EXIT_TPL(&is_verified, &persistent_state, &cls)
    {
        if (is_verified) persistent_state.insert(&cls);
    }
    BOOST_SCOPE_EXIT_END;

    if (persistent_state.count(&cls) != 0u)
        return tag_state_non_changed();

    const auto*const refs = policy_type::get_context(cls);

    if (refs == nullptr)
    {
        is_verified = true;

        return tag_state_non_changed();
    }

    if (refs->empty())
    {
        is_verified = true;

        return tag_variable_name_conflict();
    }

    if (refs->size() > 1)
    {
        is_verified = true;

        return *refs;
    }

    tree::FunctionDecl*const func = refs->front();

    BOOST_ASSERT(func && "null pointer exception");

    if (!func->is_static)
    {
        is_verified = true;

        return func;
    }

    const auto*const found_return_type = func->type == nullptr ? nullptr : func->type->getCanonicalType();

    if (found_return_type == nullptr)
        return tag_state_non_changed();

    std::vector<tree::Type*> found_parameter_types;

    for (const auto*const parameter: func->parameters)
    {
        BOOST_ASSERT(parameter && "null pointer exception");

        auto*const found_parameter_type = parameter->getCanonicalType();

        if (found_parameter_type == nullptr)
            return tag_state_non_changed();

        found_parameter_types.emplace_back(found_parameter_type);
    }

    const auto*const tangle = func->getOwner<Tangle>();
    BOOST_ASSERT(tangle && "function is not under tangle");

    const auto*const expect_return_type     = policy_type::get_return_canonical_type(*tangle);
    const auto&      expect_parameter_types = policy_type::get_parameter_canonical_types(*tangle,cls);

    const auto& is_return_and_parameters_matched =
        found_return_type     == expect_return_type     &&
        found_parameter_types == expect_parameter_types
    ;

    is_verified = true;

    if (is_return_and_parameters_matched)
        return tag_state_non_changed();
    else
        return func;
}

tree::Identifier* create_use_id_for_class(const tree::ClassDecl& cls)
{
    BOOST_ASSERT(cls.name && "null pointer exception");

    tree::Identifier* use_id = nullptr;

    const tree::SimpleIdentifier*    cls_decl_sid = cls.name->getSimpleId();
    const tree::TemplatedIdentifier* cls_decl_tid = cls.name->getTemplateId();

    BOOST_ASSERT(cls_decl_sid && "null pointer exception");

    if(cls_decl_tid == nullptr)
    {
        use_id = new tree::SimpleIdentifier(cls_decl_sid->name);
    }
    else
    {
        tree::TemplatedIdentifier* cls_use_tid = new tree::TemplatedIdentifier(
            tree::TemplatedIdentifier::Usage::ACTUAL_ARGUMENT,
            new tree::SimpleIdentifier(cls_decl_sid->name)
        );

        for(const tree::TypenameDecl* cls_decl_tid_td: cls_decl_tid->templated_type_list)
        {
            BOOST_ASSERT(cls_decl_tid_td && "null pointer exception");
            BOOST_ASSERT(cls_decl_tid_td->name && "null pointer exception");
            BOOST_ASSERT(cls_decl_tid_td->name->getSimpleId() && "null pointer exception");

            const auto& template_parameter_name = cls_decl_tid_td->name->getSimpleId()->name;

            cls_use_tid->append(
                new tree::TypenameDecl(
                    new tree::SimpleIdentifier(L"_"),
                    new tree::NamedSpecifier(
                        new tree::SimpleIdentifier(
                            template_parameter_name
                        )
                    )
                )
            );
        }

        use_id = cls_use_tid;
    }

    BOOST_ASSERT(use_id && "use ID is not generated!");

    return use_id;
}

} // enclose anonymous namespace

boost::tribool is_really_replicable(const tree::ClassDecl& cls_decl)
{
    if (!cls_decl.isReplicable())
        return false;

    if (cls_decl.base == nullptr)
        return true;

    auto && base_cls_type = cls_decl.base->getCanonicalType();

    if (base_cls_type == nullptr)
        return boost::indeterminate;

    BOOST_ASSERT(base_cls_type->isRecordType() && "base class is not a class");

    return true;
}

bool need_creator(tree::ClassDecl& cls)
{
    return need_decl_impl<creator_policy>(cls);
}

void create_creator(tree::ClassDecl& cls)
{
    create_decl_impl<creator_policy>(cls);
}

bool need_serializer(tree::ClassDecl& cls)
{
    return need_decl_impl<serializer_policy>(cls);
}

void create_serializer(tree::ClassDecl& cls)
{
    create_decl_impl<serializer_policy>(cls);
}

bool need_deserializer(tree::ClassDecl& cls)
{
    return need_decl_impl<deserializer_policy>(cls);
}

void create_deserializer(tree::ClassDecl& cls)
{
    create_decl_impl<deserializer_policy>(cls);
}

verification_result continue_verification(verification_state& state, tree::ClassDecl& cls)
{
    verification_result result;

    result.     creator_result = verify_impl<     creator_policy>(state.verified_creators     , cls);
    result.  serializer_result = verify_impl<  serializer_policy>(state.verified_serializers  , cls);
    result.deserializer_result = verify_impl<deserializer_policy>(state.verified_deserializers, cls);

    return result;
}

constexpr const wchar_t* deserializer_policy::get_method_name()
{
    return L"deserialize";
}

constexpr unsigned deserializer_policy::get_method_parameter_count()
{
    return 2u;
}

constexpr const wchar_t* deserializer_policy::get_decoder_class_name()
{
    return L"ReplicationDecoder";
}

constexpr const wchar_t* deserializer_policy::get_decoder_variable_name()
{
    return L"decoder";
}

constexpr const wchar_t* deserializer_policy::get_instance_variable_name()
{
    return L"instance";
}

tree::Type* deserializer_policy::get_return_canonical_type(const tree::Tangle& tangle)
{
    BOOST_ASSERT(tangle.internal && "null pointer exception");
    BOOST_ASSERT(tangle.internal->getPrimitiveType(PrimitiveKind::BOOL_TYPE) && "null pointer exception");

    return tangle.internal->getPrimitiveType(PrimitiveKind::BOOL_TYPE);
}

std::vector<tree::Type*> deserializer_policy::get_parameter_canonical_types(const tree::Tangle& tangle, tree::ClassDecl& cls)
{
    return  {
                tangle.findClass(L"thor.lang", get_decoder_class_name())->getType(),
                cls.getType()
            };
}

tree::FunctionDecl* deserializer_policy::generate_declaration(const tree::ClassDecl& cls)
{
    tree::FunctionDecl* deserializer_decl = new tree::FunctionDecl(
        new tree::SimpleIdentifier(get_method_name()),
        new tree::PrimitiveSpecifier(tree::PrimitiveKind::BOOL_TYPE),
        true,
        true,
        false,
        false,
        tree::Declaration::VisibilitySpecifier::PROTECTED,
        new tree::NormalBlock()
    );

    tree::Identifier* decoder_id = ASTNodeHelper::createThorLangId(&cls, new tree::SimpleIdentifier(get_decoder_class_name()));

    auto parameters =
    {
        std::make_tuple(get_decoder_variable_name(), decoder_id),
        std::make_tuple(get_instance_variable_name(), create_use_id_for_class(cls))
    };

    for (const auto& parameter: parameters)
    {
        const wchar_t*    parameter_name       = std::get<0>(parameter);
        tree::Identifier* parameter_identifier = std::get<1>(parameter);

        deserializer_decl->appendParameter(
            new tree::VariableDecl(
                new tree::SimpleIdentifier(parameter_name),
                new tree::NamedSpecifier(parameter_identifier),
                false,
                false,
                false,
                tree::Declaration::VisibilitySpecifier::DEFAULT
            )
        );
    }

    return deserializer_decl;
}

tree::Expression* deserializer_policy::generate_base_deserializer(const tree::ClassDecl& cls)
{
    BOOST_ASSERT( cls.base != nullptr && "should have base class, at least thor.lang.Object" );

    tree::Identifier* base_identifier = cls.base->getName();
    BOOST_ASSERT( base_identifier != nullptr && "base class should have a name" );

    tree::CallExpr* call_base_deserializer =
        new tree::CallExpr(
            new tree::MemberExpr(
                new tree::IdExpr( base_identifier->clone() ),
                new tree::SimpleIdentifier( get_method_name() )
            )
        )
    ;

    call_base_deserializer->appendParameter(tree::Expression::create({get_decoder_variable_name()}));
    call_base_deserializer->appendParameter(new tree::IdExpr(new tree::SimpleIdentifier(get_instance_variable_name())));

    return call_base_deserializer;
}

tree::Expression* deserializer_policy::generate_member_variable_deserializer(const tree::VariableDecl& member_variable)
{
    tree::CallExpr* member_deserializer =
        new tree::CallExpr(
            new tree::MemberExpr(
                tree::Expression::create({get_decoder_variable_name()}),
                new tree::SimpleIdentifier(L"get")
            )
        )
    ;

    BOOST_ASSERT(member_variable.name && "null pointer exception");
    BOOST_ASSERT(member_variable.name->getSimpleId() && "null pointer exception");

    const tree::SimpleIdentifier* member_variable_sid = member_variable.name->getSimpleId();
    member_deserializer->appendParameter(tree::Expression::create({get_instance_variable_name(), member_variable_sid->name}));

    return member_deserializer;
}

tree::Statement* deserializer_policy::generate_deserializing_unpacker(const tree::VariableDecl& flag,
                                                                      const tree::VariableDecl& member)
{
    BOOST_ASSERT(flag.name && "null pointer exception");
    BOOST_ASSERT(flag.name->getSimpleId() && "null pointer exception");

    BOOST_ASSERT(member.name && "null pointer exception");
    BOOST_ASSERT(member.name->getSimpleId() && "null pointer exception");

    const auto & flag_name = flag.name->getSimpleId()->name;
    const auto & member_name = member.name->getSimpleId()->name;

    auto flag_and_member = new tree::TieExpr( {
        tree::Expression::create( {flag_name} ),
        tree::Expression::create( {get_instance_variable_name(), member_name} )
    } );

    return new tree::ExpressionStmt(
        new tree::BinaryExpr(
            tree::BinaryExpr::OpCode::ASSIGN,
            flag_and_member,
            generate_member_variable_deserializer( member )
        )
    );
}

template < bool Value >
inline tree::Statement* return_boolean()
{
    return new tree::BranchStmt(
        tree::BranchStmt::OpCode::RETURN,
        new tree::NumericLiteral(Value)
    );
}

template < bool Value >
inline tree::Statement* return_boolean_if_not( tree::Expression * expect )
{
    auto inverted_expect = new tree::UnaryExpr(
        tree::UnaryExpr::OpCode::LOGICAL_NOT,
        expect
    );

    auto return_false = new tree::NormalBlock();
    return_false->appendObject( return_boolean<Value>() );

    return new tree::IfElseStmt(
        new Selection(
            inverted_expect,
            return_false
        )
    );
}

template < bool Value >
inline tree::Statement* return_boolean_if_not( const wchar_t * identifier )
{
    return return_boolean_if_not<Value>( tree::Expression::create({identifier}) );
}

tree::FunctionDecl* deserializer_policy::generate(tree::ClassDecl& cls)
{
    tree::FunctionDecl* deserializer_decl = generate_declaration(cls);

    BOOST_ASSERT(deserializer_decl && "null pointer exception");
    BOOST_ASSERT(deserializer_decl->block && "null pointer exception");

    tree::Block* deserializer_body = deserializer_decl->block;

    // (1) generate code: 'if( !super.deserialize(decoder, instance) )'
    //                    '    return false;                          '
    if (cls.base)
    {
        deserializer_body->appendObject(
            return_boolean_if_not<false>( generate_base_deserializer(cls) )
        );
    }

    // terminate generating directly if class has no members
    if ( !cls.member_variables.empty() )
    {
        auto flag_name = L"is_success";
        // (2) generate code: 'var is_success: bool = false;'
        auto flag_decl = new tree::VariableDecl(
            new tree::SimpleIdentifier( flag_name ),
            new tree::PrimitiveSpecifier( tree::PrimitiveKind::BOOL_TYPE ),
            false, false, false, // not member, static, nor const
            tree::Declaration::VisibilitySpecifier::DEFAULT,
            new NumericLiteral(false)
        );

            deserializer_body->appendObject(
            new tree::DeclarativeStmt( flag_decl )
        );

        ASTNodeHelper::propogateSourceInfo( *flag_decl, cls );
        // split declaration into two lines
        visitor::restructure_helper::splitVariableInitializer( *flag_decl );

        // (3) generate code: 'is_success, instance.member = decoder.get( instance.member );'
        //                    'if (!is_success) return false;                               '
        //                    '... and so on                                                '
        for (const auto member : cls.member_variables)
        {
            deserializer_body->appendObject( generate_deserializing_unpacker(*flag_decl, *member) );
            deserializer_body->appendObject( return_boolean_if_not<false>(flag_name) );
        }
    }

    // (4) generate code: 'reture true;'
    deserializer_body->appendObject( return_boolean<true>() );

    cls.addFunction( deserializer_decl );

    // nodes in deserializer have same source info as class
    ASTNodeHelper::foreachApply<ASTNode>( cls, [&cls]( ASTNode & node ) {
        ASTNodeHelper::propogateSourceInfo( node, cls );
    } );

    return deserializer_decl;
}

} } } } } }
