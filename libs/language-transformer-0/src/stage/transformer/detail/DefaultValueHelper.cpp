/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <set>

#include <boost/assert.hpp>
#include <boost/mpl/end.hpp>
#include <boost/mpl/insert_range.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/range/adaptor/indirected.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/iterator_range.hpp>

#include "language/context/ResolverContext.h"
#include "language/stage/transformer/detail/DefaultValueHelper.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/TypenameDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/expression/CallExpr.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace stage { namespace visitor { namespace default_value_helper {

namespace {

const tree::FunctionDecl& findGenericDeclaration(const tree::FunctionDecl& decl)
{
    BOOST_ASSERT(decl.name && "null pointer exception");
    BOOST_ASSERT(decl.name->getTemplateId() && "you can't find generic declaration for non-template declaration!");

    const tree::Declaration* decl_source = nullptr;

    if(tree::Declaration* instantiated_from_decl = InstantiatedFrom::get(&decl))
    {
        decl_source = instantiated_from_decl;
    }
    else
    {
        decl_source = &decl;
    }

    BOOST_ASSERT(decl_source && "null pointer exception");

    const tree::FunctionDecl* generic_decl = nullptr;

    if(tree::Declaration* specialized_from_decl = SpecializationOf::get(decl_source))
    {
        generic_decl = cast<tree::FunctionDecl>(specialized_from_decl);
    }
    else
    {
        generic_decl = cast<tree::FunctionDecl>(decl_source);
    }

    BOOST_ASSERT(generic_decl && "null pointer exception");
    BOOST_ASSERT(
        std::all_of(
            generic_decl->name->getTemplateId()->templated_type_list.begin(),
            generic_decl->name->getTemplateId()->templated_type_list.end(),
            [](tree::TypenameDecl* td) -> bool
            {
                BOOST_ASSERT(td && "null pointer exception");

                return td->specialized_type == nullptr;
            }
        ) && "non-generic version found!"
    );

    return *generic_decl;
}

tree::Expression* clone_parameter_default_value(tree::VariableDecl& param_decl)
{
    using ClonedContexts = boost::mpl::insert_range<
                        tree::detail::ContextToCloneTypeList       ,
        boost::mpl::end<tree::detail::ContextToCloneTypeList>::type,
        boost::mpl::vector<
            ResolvedSymbol,
            ResolvedType
        >
    >::type;

    BOOST_ASSERT(param_decl.initializer && "null pointer exception");

    tree::Expression*const        default_value = param_decl.initializer;
    tree::Expression*const cloned_default_value = ASTNodeHelper::clone<tree::Expression, ClonedContexts>(default_value);

    DefaultValueFrom::set(cloned_default_value, &param_decl);

    tree::ASTNodeHelper::foreachApply<tree::ASTNode>(
        *cloned_default_value,
        [](tree::ASTNode& node)
        {
            const auto*const symbol = ResolvedSymbol::get(&node);

            if (symbol == nullptr)
                return;

            bool need_clear = false;

            if (const auto*const cls_symbol = cast<tree::ClassDecl>(symbol))
                need_clear = !cls_symbol->isCompleted();
            else if (const auto*const func_symbol = cast<tree::FunctionDecl>(symbol))
                need_clear = !func_symbol->isCompleted();
            else
                need_clear = false;

            if (need_clear)
            {
                ResolvedSymbol::set(&node, nullptr);
                ResolvedType  ::set(&node, nullptr);
            }
        }
    );

    return cloned_default_value;
}

}

void append_values(const tree::FunctionDecl& func_decl, tree::CallExpr& call_expr)
{
    BOOST_ASSERT(call_expr.parameters.size() < func_decl.parameters.size() && "no need to append default parameter!");
    BOOST_ASSERT(func_decl.name && "null pointer exception");
    BOOST_ASSERT(func_decl.isCompleted() && "we cannot do this on non-complete function!");

    const tree::FunctionDecl& generic_func_decl = func_decl.name->getTemplateId() != nullptr ? findGenericDeclaration(func_decl) : func_decl;

    BOOST_ASSERT(generic_func_decl.parameters.size() == func_decl.parameters.size() && "parameter count of generic and resolved one is not match!");

    call_expr.appendParameters(
          boost::make_iterator_range(generic_func_decl.parameters, call_expr.parameters.size(), 0)
        | boost::adaptors::indirected
        | boost::adaptors::transformed(&clone_parameter_default_value)
    );
}

default_state_t is_defaulted(tree::Expression& expr)
{
    auto*const var_decl = DefaultValueFrom::get(&expr);

    if (var_decl == nullptr)
        return NOT_DEFAULTED;

    BOOST_ASSERT(var_decl->isParameter() && "default value is not created from parameter!?");

          auto& cloned_default_values = DefaultValueAs::init(var_decl)->value;
    const auto& insertion_result      = cloned_default_values.insert(&expr);
    const auto& is_inserted           = insertion_result.second;

    if (is_inserted)
        return NEW_DEFAULTED;
    else
        return ALREADY_DEFAULTED;
}

const std::set<tree::Expression*>* get_extra_processings(const tree::VariableDecl& var)
{
    const auto*const exprs = DefaultValueAs::get(&var);

    return exprs;
}

} } } } }
