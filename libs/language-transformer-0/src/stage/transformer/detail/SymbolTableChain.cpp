/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#include <boost/foreach.hpp>
#include <boost/logic/tribool.hpp>
#include "core/Prerequisite.h"
#include "language/stage/transformer/detail/SymbolTableChain.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/module/Import.h"
#include "language/resolver/SymbolTable.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

SymbolTableChain::SymbolTableChain() : chained_tables() {}

bool SymbolTableChain::is_valid() const
{
    return !chained_tables.empty();
}

void SymbolTableChain::chain(SymbolTable& new_table)
{
    chained_tables.push_back(&new_table);
}

void SymbolTableChain::unchain(SymbolTable& old_table)
{
    BOOST_ASSERT(is_valid() && "stack for chained symbol table is empty!");
    BOOST_ASSERT(&old_table == &last_chained() && "table to be unchained is not the last one!");

    chained_tables.pop_back();
}

SymbolTable::InsertResult SymbolTableChain::insert_symbol(tree::Import& node)
{
    SymbolTable& last_chained_table = last_chained();

    BOOST_ASSERT(chained_tables.size() == 1 && "we should has only 1 symbol table now");

    return last_chained_table.insert(&node);
}

SymbolTable::InsertResult SymbolTableChain::insert_symbol(tree::Declaration& node)
{
    BOOST_ASSERT(!tree::isa<tree::TemplatedIdentifier>(node.name) && "Templated identifier should not be insert into here");

    SymbolTable& last_chained_table = last_chained();

    const std::wstring name = node.name->toString();

    BOOST_FOREACH(SymbolTable* chained_symbol_table, std::make_pair(chained_tables.begin(), boost::prior(chained_tables.end())))
    {
        if(chained_symbol_table->has(name))
        {
            return SymbolTable::InsertResult(SymbolTable::InsertResult::SHADOW, name);
        }
    }

    return last_chained_table.insert(&node);
}

void SymbolTableChain::enter_scope(tree::ASTNode& t_id)
{
    last_chained().enter(&t_id);
}

void SymbolTableChain::leave_scope(tree::ASTNode& t_id)
{
    last_chained().leave(&t_id);
}

void SymbolTableChain::enter_scope(tree::ClassDecl& cls_decl, SymbolTable& new_symbol_table)
{
    chain(cls_decl.symbol_table);
    chain(new_symbol_table);
}

void SymbolTableChain::leave_scope(tree::ClassDecl& cls_decl, SymbolTable& old_symbol_table)
{
    unchain(old_symbol_table);
    unchain(cls_decl.symbol_table);
}

SymbolTable::FindResult SymbolTableChain::find(const std::wstring& id) const
{
    BOOST_ASSERT(is_valid() && "stack for chained symbol table is empty!");

    SymbolTable::FindResult find_result;

    BOOST_REVERSE_FOREACH(SymbolTable* symbol_table, chained_tables)
    {
        find_result = symbol_table->find(id);

        if(find_result.is_found || boost::indeterminate(find_result.is_found))
        {
            break;
        }
    }

    return find_result;
}

SymbolTable& SymbolTableChain::last_chained() const
{
    BOOST_ASSERT(is_valid() && "stack for chained symbol table is empty!");

    return *chained_tables.back();
}

} } } }
