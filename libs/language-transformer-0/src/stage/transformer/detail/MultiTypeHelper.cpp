/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <array>
#include <functional>
#include <string>

#include <boost/assert.hpp>
#include <boost/functional/factory.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/next_prior.hpp>
#include <boost/optional/optional.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/join.hpp>

#include "utility/RangeUtil.h"
#include "utility/UnicodeUtil.h"
#include "utility/UUIDUtil.h"

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Block.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/Type.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/expression/BinaryExpr.h"
#include "language/tree/expression/BlockExpr.h"
#include "language/tree/expression/CallExpr.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/expression/TieExpr.h"
#include "language/tree/expression/UnpackExpr.h"
#include "language/tree/statement/DeclarativeStmt.h"
#include "language/tree/statement/ExpressionStmt.h"
#include "language/tree/statement/Statement.h"

#include "language/stage/transformer/detail/MultiTypeHelper.h"

namespace zillians { namespace language { namespace stage { namespace visitor { namespace multi_type_helper {

namespace {

SimpleIdentifier* createUnpackVariable(const ASTNode& source_ref, Block& block, Type* type)
{
    BOOST_ASSERT(type && "null pointer exception");

    const std::wstring &temp_name = L"unpack_temp_" + s_to_ws(UUID::random().toString('_'));

    SimpleIdentifier* temp_decl_id = new SimpleIdentifier(temp_name);
    TypeSpecifier*    temp_decl_ts = ASTNodeHelper::createTypeSpecifierFrom(type);

    BOOST_ASSERT(temp_decl_ts && "null pointer exception");

    VariableDecl*    temp_decl      = new VariableDecl(temp_decl_id, temp_decl_ts, false, false, false, VariableDecl::VisibilitySpecifier::DEFAULT);
    DeclarativeStmt* temp_decl_stmt = new DeclarativeStmt(temp_decl);

    block.appendObject(temp_decl_stmt);

    ASTNodeHelper::propogateSourceInfo(*temp_decl_id  , source_ref);
    ASTNodeHelper::propogateSourceInfo(*temp_decl_ts  , source_ref);
    //ASTNodeHelper::foreachApply<ASTNode>( *temp_decl_ts, [&source_ref]( ASTNode &n ){
    //    ASTNodeHelper::propogateSourceInfo( n, source_ref );
    //} );
    ASTNodeHelper::propogateSourceInfo(*temp_decl     , source_ref);
    ASTNodeHelper::propogateSourceInfo(*temp_decl_stmt, source_ref);

    SimpleIdentifier* temp_decl_ref_id = new SimpleIdentifier(temp_name);

    ResolvedType  ::set(temp_decl_ref_id, type     );
    ResolvedSymbol::set(temp_decl_ref_id, temp_decl);

    ASTNodeHelper::propogateSourceInfo(*temp_decl_ref_id, source_ref);

    return temp_decl_ref_id;
}

UnpackExpr* createUnpacker(const ASTNode& source_ref, Block& block, CallExpr& rhs_expr)
{
    using boost::adaptors::transformed;

    const MultiType* rhs_expr_type_multi = cast_or_null<MultiType>(rhs_expr.getCanonicalType());

    BOOST_ASSERT(rhs_expr_type_multi && "we should not enter here if canonical type is not ready");

    UnpackExpr* unpacker = new UnpackExpr(&rhs_expr);

    unpacker->appendNames(
          rhs_expr_type_multi->types
        | transformed(std::bind(createUnpackVariable, std::cref(source_ref), std::ref(block), std::placeholders::_1))
    );

    ExpressionStmt* unpacker_stmt = new ExpressionStmt(unpacker);

    block.appendObject(unpacker_stmt);

    ASTNodeHelper::propogateSourceInfo(*unpacker     , source_ref);
    ASTNodeHelper::propogateSourceInfo(*unpacker_stmt, source_ref);

    return unpacker;
}

TieExpr* mergeUnpackedVariables(const ASTNode& source_ref, const BinaryExpr::OpCode::type op, Block& block, const TieExpr& tied, UnpackExpr& unpacker)
{
    BOOST_ASSERT(tied.tied_expressions.size() == unpacker.unpack_list.size() && "size mismatch");

    TieExpr* merged_expr = new TieExpr();

    for (const auto &item : make_zip_range(tied.tied_expressions, unpacker.unpack_list))
    {
        Expression*       lhs_expr     = boost::get<0>(item);
        SimpleIdentifier* rhs_name_ref = boost::get<1>(item);

        BOOST_ASSERT(lhs_expr && "null pointer exception");
        BOOST_ASSERT(rhs_name_ref && "null pointer exception");

        SimpleIdentifier* rhs_name = rhs_name_ref->clone();

        BOOST_ASSERT(rhs_name && "null pointer exception");

        IdExpr*     rhs_expr = new IdExpr(rhs_name);
        BinaryExpr* new_expr = new BinaryExpr(op, lhs_expr, rhs_expr);

        merged_expr->tieExpression(new_expr);

        ASTNodeHelper::propogateSourceInfo(*rhs_name, source_ref);
        ASTNodeHelper::propogateSourceInfo(*rhs_expr, source_ref);
        ASTNodeHelper::propogateSourceInfo(*new_expr, source_ref);
    }

    ExpressionStmt* merged_stmt = new ExpressionStmt(merged_expr);

    block.appendObject(merged_stmt);

    ASTNodeHelper::propogateSourceInfo(*merged_expr, source_ref);
    ASTNodeHelper::propogateSourceInfo(*merged_stmt, source_ref);

    return merged_expr;
}

TieExpr* createUnpackedTie(const ASTNode& source_ref, Block& block, const UnpackExpr& unpacker)
{
    std::vector<Expression*> unpack_exprs;

    for (SimpleIdentifier* unpack_name : unpacker.unpack_list)
    {
        BOOST_ASSERT(unpack_name && "null pointer exception");

        SimpleIdentifier* unpack_name_cloned = unpack_name->clone();
        IdExpr*           unpack_expr        = new IdExpr(unpack_name_cloned);

        unpack_exprs.emplace_back(unpack_expr);

        ASTNodeHelper::propogateSourceInfo(*unpack_name_cloned, source_ref);
        ASTNodeHelper::propogateSourceInfo(*unpack_expr       , source_ref);
    }

    TieExpr*        unpack_tie  = new TieExpr(std::move(unpack_exprs));
    ExpressionStmt* unpack_stmt = new ExpressionStmt(unpack_tie);

    block.appendObject(unpack_stmt);

    ASTNodeHelper::propogateSourceInfo(*unpack_tie , source_ref);
    ASTNodeHelper::propogateSourceInfo(*unpack_stmt, source_ref);

    return unpack_tie;
}

template<typename RangeType = std::array<Statement*, 0>>
void unpackCallImpl(BinaryExpr& node, TieExpr& lhs_expr, CallExpr& rhs_expr, const RangeType& prepends = RangeType())
{
    Block*     block    = new NormalBlock();
    BlockExpr* new_expr = new BlockExpr(block, "multi-type-restructure");

    ASTNodeHelper::propogateSourceInfo(*block   , node);
    ASTNodeHelper::propogateSourceInfo(*new_expr, node);

    BOOST_ASSERT( SourceInfoContext::get(&node) );

    block->appendObjects(prepends);

    UnpackExpr* unpacker = createUnpacker(node, *block, rhs_expr);

    BOOST_ASSERT(unpacker && "null pointer exception");

    mergeUnpackedVariables(node, node.opcode, *block, lhs_expr, *unpacker);

    BOOST_ASSERT(node.parent && "null pointer exception");

    node.parent->replaceUseWith(node, *new_expr);
}

void unpackCallImpl(const ASTNode& source_ref, CallExpr& ret_expr)
{
    Block*     block    = new NormalBlock();
    BlockExpr* new_expr = new BlockExpr(block, "multi-type-restructure");

    ASTNodeHelper::propogateSourceInfo(*block   , source_ref);
    ASTNodeHelper::propogateSourceInfo(*new_expr, source_ref);

    BOOST_ASSERT(ret_expr.parent && "null pointer exception");

    ret_expr.parent->replaceUseWith(ret_expr, *new_expr);

    UnpackExpr* unpacker = createUnpacker(source_ref, *block, ret_expr);

    BOOST_ASSERT(unpacker && "null pointer exception");

    createUnpackedTie(source_ref, *block, *unpacker);
}

/**
 *  Transforms "a, b = 1, 2" into "(a = 1), (b = 2)"
 */
TieExpr* mergeTie(const ASTNode& source_info, const BinaryExpr::OpCode::type op, TieExpr& lhs_expr, TieExpr& rhs_expr)
{
    BOOST_ASSERT(lhs_expr.tied_expressions.size() == rhs_expr.tied_expressions.size() && "amount of tied expression mismatch");

    TieExpr* new_expr = new TieExpr();

    ASTNodeHelper::propogateSourceInfo(*new_expr, source_info);

    for (const auto& item : make_zip_range(lhs_expr.tied_expressions, rhs_expr.tied_expressions))
    {
        Expression* lhs_tied_expr = boost::get<0>(item);
        Expression* rhs_tied_expr = boost::get<1>(item);

        BOOST_ASSERT(lhs_tied_expr && "null pointer exception");
        BOOST_ASSERT(rhs_tied_expr && "null pointer exception");

        BinaryExpr* new_tied_expr = new BinaryExpr(op, lhs_tied_expr, rhs_tied_expr);

        new_expr->tieExpression(new_tied_expr);

        ASTNodeHelper::propogateSourceInfo(*new_tied_expr, source_info);
    }

    return new_expr;
}

std::size_t getTypeCount(const TieExpr& expr)
{
    return expr.tied_expressions.size();
}

template<typename NodeType>
boost::optional<std::size_t> getTypeCount(const NodeType& expr)
{
    const Type* rhs_expr_type = expr.getCanonicalType();

    if (rhs_expr_type == nullptr)
        return boost::optional<std::size_t>();

    const MultiType* rhs_expr_type_multi = rhs_expr_type->getAsMultiType();

    if (rhs_expr_type_multi == nullptr)
        return boost::optional<std::size_t>(1);

    return rhs_expr_type_multi->types.size();
}

template<typename NodeType1, typename NodeType2>
boost::tribool isTypeCountMatched(const NodeType1& expr1, const NodeType2& expr2)
{
    const boost::optional<std::size_t> &expr1_count = getTypeCount(expr1);
    const boost::optional<std::size_t> &expr2_count = getTypeCount(expr2);

    if (expr1_count && expr2_count)
        return *expr1_count == *expr2_count;
    else
        return boost::indeterminate;
}

}

TieExpr& findEndingTieExpr(BlockExpr& expr)
{
    BOOST_ASSERT(expr.tag == "multi-type-restructure" && "unexpected tag from block expression");

    BOOST_ASSERT(expr.block && "null pointer exception");
    BOOST_ASSERT(!expr.block->objects.empty() && "empty block expression");
    BOOST_ASSERT(expr.block->objects.back() && "null pointer exception");

    ExpressionStmt* tie_expr_stmt = cast<ExpressionStmt>(expr.block->objects.back());

    BOOST_ASSERT(tie_expr_stmt && "last statement of block expression is not a expression");
    BOOST_ASSERT(tie_expr_stmt->expr && "null pointer exception");

    TieExpr* tie_expr = cast<TieExpr>(tie_expr_stmt->expr);

    BOOST_ASSERT(tie_expr && "last expression is not a TieExpr after MultiType restruction");

    return *tie_expr;
}

bool isValidReturn(const Type& type)
{
    if (const MultiType* type_multi = type.getAsMultiType())
    {
        return std::none_of(type_multi->types.begin(), type_multi->types.end(), static_cast<bool(*)(const ASTNode*)>(&tree::isa<MultiType>));
    }

    return true;
}

boost::tribool isMergable(const tree::TieExpr&   lhs_expr, const tree::BlockExpr& rhs_expr) { return isTypeCountMatched(lhs_expr, rhs_expr); }
boost::tribool isMergable(const tree::TieExpr&   lhs_expr, const tree::CallExpr&  rhs_expr) { return isTypeCountMatched(lhs_expr, rhs_expr); }
boost::tribool isMergable(const tree::TieExpr&   lhs_expr, const tree::TieExpr&   rhs_expr) { return isTypeCountMatched(lhs_expr, rhs_expr); }
boost::tribool isMergable(const tree::BlockExpr& lhs_expr, const tree::BlockExpr& rhs_expr) { return isTypeCountMatched(lhs_expr, rhs_expr); }
boost::tribool isMergable(const tree::BlockExpr& lhs_expr, const tree::CallExpr&  rhs_expr) { return isTypeCountMatched(lhs_expr, rhs_expr); }
boost::tribool isMergable(const tree::BlockExpr& lhs_expr, const tree::TieExpr&   rhs_expr) { return isTypeCountMatched(lhs_expr, rhs_expr); }

void unpackCall(BinaryExpr& node, TieExpr& lhs_expr, CallExpr& rhs_expr)
{
    unpackCallImpl(node, lhs_expr, rhs_expr);
}

void unpackCall(BinaryExpr& node, BlockExpr& lhs_expr, CallExpr& rhs_expr)
{
    TieExpr& lhs_expr_tie = findEndingTieExpr(lhs_expr);

    unpackCallImpl(node, lhs_expr_tie, rhs_expr, boost::make_iterator_range(lhs_expr.block->objects.begin(), boost::prior(lhs_expr.block->objects.end())));
}

void unpackCall(BranchStmt& node, CallExpr& ret_expr)
{
    BOOST_ASSERT(node.opcode == BranchStmt::OpCode::RETURN && "invalid operation on BranchStmt");
    BOOST_ASSERT(&ret_expr == node.result && "incorrect result expression");

    unpackCallImpl(node, ret_expr);
}

void mergeExpr(BinaryExpr& node, TieExpr& lhs_expr, TieExpr& rhs_expr)
{
    BOOST_ASSERT(&lhs_expr == node.left  && "incorrect LHS expression");
    BOOST_ASSERT(&rhs_expr == node.right && "incorrect RHS expression");

    TieExpr* new_expr = mergeTie(node, node.opcode, lhs_expr, rhs_expr);

    BOOST_ASSERT(new_expr && "null pointer exception");
    BOOST_ASSERT(node.parent && "null pointer exception");

    node.parent->replaceUseWith(node, *new_expr);
}

void mergeExpr(BinaryExpr& node, TieExpr& lhs_expr, BlockExpr& rhs_expr)
{
    BOOST_ASSERT(&lhs_expr == node.left  && "incorrect LHS expression");
    BOOST_ASSERT(&rhs_expr == node.right && "incorrect RHS expression");

    TieExpr& rhs_expr_tie = findEndingTieExpr(rhs_expr);
    TieExpr* lhs_expr_new = mergeTie(node, node.opcode, lhs_expr, rhs_expr_tie);

    BOOST_ASSERT(lhs_expr_new && "null pointer exception");
    BOOST_ASSERT(rhs_expr_tie.parent && "null pointer exception");
    BOOST_ASSERT(node.parent && "null pointer exception");

    rhs_expr_tie.parent->replaceUseWith(rhs_expr_tie, *lhs_expr_new);
    node.parent->replaceUseWith(node, rhs_expr);

    ResolvedType::set(&rhs_expr, nullptr);
}

void mergeExpr(BinaryExpr& node, BlockExpr& lhs_expr, TieExpr& rhs_expr)
{
    BOOST_ASSERT(&lhs_expr == node.left  && "incorrect LHS expression");
    BOOST_ASSERT(&rhs_expr == node.right && "incorrect RHS expression");

    TieExpr& lhs_expr_tie = findEndingTieExpr(lhs_expr);
    TieExpr* rhs_expr_new = mergeTie(node, node.opcode, lhs_expr_tie, rhs_expr);

    BOOST_ASSERT(rhs_expr_new && "null pointer exception");
    BOOST_ASSERT(lhs_expr_tie.parent && "null pointer exception");
    BOOST_ASSERT(node.parent && "null pointer exception");

    lhs_expr_tie.parent->replaceUseWith(lhs_expr_tie, *rhs_expr_new);
    node.parent->replaceUseWith(node, lhs_expr);

    ResolvedType::set(&lhs_expr, nullptr);
}

void mergeExpr(BinaryExpr& node, BlockExpr& lhs_expr, BlockExpr& rhs_expr)
{
    BOOST_ASSERT(&lhs_expr == node.left  && "incorrect LHS expression");
    BOOST_ASSERT(&rhs_expr == node.right && "incorrect RHS expression");

    TieExpr& lhs_expr_tie = findEndingTieExpr(lhs_expr);
    TieExpr& rhs_expr_tie = findEndingTieExpr(rhs_expr);

    TieExpr* new_expr_tie = mergeTie(node, node.opcode, lhs_expr_tie, rhs_expr_tie);

    BOOST_ASSERT(new_expr_tie && "null pointer exception");

    ExpressionStmt* new_expr_stmt = new ExpressionStmt(new_expr_tie);

    ASTNodeHelper::propogateSourceInfo(*new_expr_stmt, node);

    Block*     new_expr_block = new NormalBlock();
    BlockExpr* new_expr       = new BlockExpr(new_expr_block, "multi-type-restructure");

    ASTNodeHelper::propogateSourceInfo(*new_expr_block, node);
    ASTNodeHelper::propogateSourceInfo(*new_expr      , node);

    BOOST_ASSERT(lhs_expr.block && "null pointer exception");
    BOOST_ASSERT(rhs_expr.block && "null pointer exception");
    BOOST_ASSERT(!lhs_expr.block->objects.empty() && "empty block expression");
    BOOST_ASSERT(!rhs_expr.block->objects.empty() && "empty block expression");

    new_expr_block->appendObjects(
        boost::range::join(
            boost::make_iterator_range(lhs_expr.block->objects.begin(), boost::prior(lhs_expr.block->objects.end())),
            boost::make_iterator_range(rhs_expr.block->objects.begin(), boost::prior(rhs_expr.block->objects.end()))
        )
    );
    new_expr_block->appendObject(new_expr_stmt);

    BOOST_ASSERT(node.parent && "null pointer exception");

    node.parent->replaceUseWith(node, *new_expr);
}

} } } } }
