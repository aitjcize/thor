/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <utility>

#include <boost/logic/tribool.hpp>

#include "language/logging/StringTable.h"
#include "language/context/ManglingStageContext.h"
#include "language/stage/transformer/detail/EnumDependencyWalker.h"
#include "language/stage/transformer/detail/EnumValueResolver.h"


namespace zillians { namespace language { namespace stage { namespace visitor {

namespace detail {

namespace {
    // utility to get value from literal
    std::pair<
        bool     // if literal has int64 type
        , int64  // get value of the literal
    >
    get_value(Expression* expr)
    {
        auto* literal = expr->getConstantNumericLiteral();
        BOOST_ASSERT(literal != nullptr && "invalid argument");
        BOOST_ASSERT(literal->primitive_kind.isBoolType() || literal->primitive_kind.isIntegerType());

        return std::make_pair(
            literal->primitive_kind == PrimitiveKind::INT64_TYPE
            , literal->get<int64>()
        );
    }
} // anonymous namespace

EnumValueResolver::EnumValueResolver()
: use_int64_as_underlying_type(false)
  , previous_enumerator_has_value(true)
  , has_unresolved_value(false)
  , current_enumerated_value(0)
{ }

std::pair<bool, boost::tribool> EnumValueResolver::resolve(Enumerator* enumerator)
{
    // skip if enumerator had been set a value
    if(auto* enumerator_had_been_set = EnumIdManglingContext::get(enumerator))
    {
        current_enumerated_value = enumerator_had_been_set->value;
        previous_enumerator_has_value = true;

        return {false, true};
    }

    // try get value from initializer
    if(enumerator->initializer)
    {
        // initializer is literal, or folded expression
        if(enumerator->initializer->getConstantNumericLiteral())
        {
            std::tie(use_int64_as_underlying_type, current_enumerated_value) = get_value(enumerator->initializer);
        }
        else
        {
            // the type of initializer is unknown yet, skip enumerating
            auto* initializer_type = enumerator->initializer->getCanonicalType();
            if(!initializer_type)
            {
                has_unresolved_value = true;
                previous_enumerator_has_value = false;

                return {true, boost::indeterminate};
            }
            // report error while meet non-integral expressions
            else if(!initializer_type->isBoolType() && !initializer_type->isIntegerType())
            {
                // TODO log illegal value type for enumeration
                LOG_MESSAGE(INVALID_VALUE_FOR_ENUM, enumerator);
                has_unresolved_value = true;

                return {true, false};
            }

            // initializer has integral type
            auto* primitive_type = initializer_type->getAsPrimitiveType();
            use_int64_as_underlying_type = (primitive_type->getKind() == PrimitiveKind::INT64_TYPE);

            // check if all the depended enum values are determined and try
            // to calculate results from constant literals
            EnumDependencyWalker finder;
            bool all_determined, found_circular_dependency, folded;
            std::tie(all_determined, found_circular_dependency, folded) = finder.walk(enumerator);

            // report error while count a circle in dependency graph
            if(found_circular_dependency)
            {
                LOG_MESSAGE(CIRCULAR_ENUM_DEPEND, finder.self_referenced);
                has_unresolved_value = true;

                return {true, false};
            }

            // report error if cannot get the value of expression
            if(all_determined && !folded)
            {
                LOG_MESSAGE(INVALID_VALUE_FOR_ENUM, enumerator);
                has_unresolved_value = true;

                return {true, false};
            }

            if(!all_determined)
            {
                has_unresolved_value = true;
                previous_enumerator_has_value = false;

                return {true, boost::indeterminate};
            }

            // try to set enum value from folded experssion
            BOOST_ASSERT(enumerator->initializer->getConstantNumericLiteral() && "failure to fold expressions");
            std::tie(use_int64_as_underlying_type, current_enumerated_value) = get_value(enumerator->initializer);
        }
    }
    // enumerator has no initializer and previous enumerator also has no value
    else if(!previous_enumerator_has_value)
    {
        return {true, boost::indeterminate}; // just skip
    }

    EnumIdManglingContext::set(enumerator, new EnumIdManglingContext(current_enumerated_value));
    previous_enumerator_has_value = true;

    // check if the value range exceed maximmum int32 can hold
    using limits_type = std::numeric_limits<int32>;
    constexpr int64 int32_min = limits_type::min();
    constexpr int64 int32_max = limits_type::max();
    if(current_enumerated_value < int32_min || int32_max < current_enumerated_value)
        use_int64_as_underlying_type = true;

    ++current_enumerated_value;

    return {true, true};
}

PrimitiveKind EnumValueResolver::getUnderlyingType() const
{
    return use_int64_as_underlying_type ? PrimitiveKind::INT64_TYPE
                                        : PrimitiveKind::INT32_TYPE;
}

bool EnumValueResolver::hasUnresolvedValue() const
{
    return has_unresolved_value;
}

} // namespace detail

} } } } // namespace zillians::language::stage::visitor
