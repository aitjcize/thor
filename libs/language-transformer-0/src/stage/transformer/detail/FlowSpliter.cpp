/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <iomanip>
#include <iterator>
#include <memory>
#include <set>
#include <stack>
#include <string>
#include <sstream>
#include <tuple>
#include <utility>
#include <vector>

#include <boost/algorithm/cxx11/all_of.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/mpl/end.hpp>
#include <boost/mpl/insert_range.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/range/algorithm/find_if.hpp>
#include <boost/range/algorithm/set_algorithm.hpp>
#include <boost/range/algorithm/transform.hpp>
#include <boost/range/size.hpp>
#include <boost/variant/get.hpp>
#include <boost/variant/static_visitor.hpp>
#include <boost/ptr_container/ptr_vector.hpp>

#include "core/Types.h"
#include "utility/UnicodeUtil.h"
#include "utility/RangeUtil.h"
#include "utility/MemoryUtil.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/stage/transformer/detail/FlowSpliter.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor { namespace detail {

namespace {

std::wstring source_to_html_table(const std::wstring& s)
{
    std::wstring result;
    std::wstringstream ss(s);
    std::wstring line;
    while(std::getline(ss, line, L'\n')) {
        boost::replace_all(line, L"&", "&amp;");
        boost::replace_all(line, L"<", "&lt;");
        boost::replace_all(line, L">", "&gt;");
        line = L"<tr><td align=\"left\">" + line + L"</td></tr>";
        result += line;
    }
    return result;
}

std::wstring var_decls_str(const std::set<tree::VariableDecl*>& vds)
{
    std::wstring result;
    for(tree::VariableDecl* v : vds) {
        if(v != *vds.begin())
            result += L" ";
        result += v->name->toString();
    }
    return result;
}

VarSetPtr used_vars_or_null(BasicBlock* b) noexcept
{
    if(b == nullptr) {
        return nullptr;
    } else {
        return &b->get_used_variables();
    }
}

template<typename type>
bool has_transition        (const BasicBlock& block) { return boost::get<type               >(&block.get_transition()) != nullptr; }
bool has_transition_case   (const BasicBlock& block) { return boost::get<CaseTransition     >(&block.get_transition()) != nullptr; }
bool has_transition_cond   (const BasicBlock& block) { return boost::get<ConditionTransition>(&block.get_transition()) != nullptr; }
bool has_transition_flow   (const BasicBlock& block) { return boost::get<FlowTransition     >(&block.get_transition()) != nullptr; }
bool has_transition_merge  (const BasicBlock& block) { return boost::get<MergeTransition    >(&block.get_transition()) != nullptr; }
bool has_transition_normal (const BasicBlock& block) { return boost::get<NormalTransition   >(&block.get_transition()) != nullptr; }
bool has_transition_null   (const BasicBlock& block) { return boost::get<NullTransition     >(&block.get_transition()) != nullptr; }
bool has_transition_unknown(const BasicBlock& block) { return boost::get<UnknownTransition  >(&block.get_transition()) != nullptr; }

template<typename type>
bool is_transition        (const TransitionType& transition) { return boost::get<type               >(&transition) != nullptr; }
bool is_transition_case   (const TransitionType& transition) { return boost::get<CaseTransition     >(&transition) != nullptr; }
bool is_transition_cond   (const TransitionType& transition) { return boost::get<ConditionTransition>(&transition) != nullptr; }
bool is_transition_flow   (const TransitionType& transition) { return boost::get<FlowTransition     >(&transition) != nullptr; }
bool is_transition_merge  (const TransitionType& transition) { return boost::get<MergeTransition    >(&transition) != nullptr; }
bool is_transition_normal (const TransitionType& transition) { return boost::get<NormalTransition   >(&transition) != nullptr; }
bool is_transition_null   (const TransitionType& transition) { return boost::get<NullTransition     >(&transition) != nullptr; }
bool is_transition_unknown(const TransitionType& transition) { return boost::get<UnknownTransition  >(&transition) != nullptr; }

using ContextToCloneList =
    boost::mpl::insert_range<
                        tree::detail::ContextToCloneTypeList       ,
        boost::mpl::end<tree::detail::ContextToCloneTypeList>::type,
        boost::mpl::vector<
            zillians::language::ResolvedType,
            zillians::language::ResolvedSymbol,
            zillians::language::SplitReferenceContext,
            zillians::language::SplitInverseReferenceContext
        >
    >::type;

}

struct FlowGraphGeneratorVisitor : public zillians::language::tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(flowGraphInvoker, generate);

    explicit FlowGraphGeneratorVisitor(const std::wstring& func_name) : flow_graph(zillians::make_unique<FlowGraph>(func_name))
    {
        REGISTER_ALL_VISITABLE_ASTNODE(flowGraphInvoker);
    }

    void generate(tree::ASTNode& node)
    {
        revisit(node);
    }

    void mark_flow_tree(tree::FunctionDecl& func)
    {
        flow_tree_nodes.clear();

        ASTNodeHelper::foreachApply<tree::FlowBlock>(func, [this, &func](tree::FlowBlock& fb){
            tree::ASTNode* current_node = &fb;
            do {
                flow_tree_nodes.insert(current_node);
                current_node = current_node->parent;
            } while (current_node != &func);
        });

        ASTNodeHelper::foreachApply<tree::BranchStmt>(func, [this, &func](tree::BranchStmt& bs){
            tree::ASTNode* current_node = &bs;
            tree::ASTNode* loop_parent = nullptr;

            switch(bs.opcode) {
                case tree::BranchStmt::OpCode::BREAK:
                case tree::BranchStmt::OpCode::CONTINUE:
                    loop_parent = bs.getAnyOwner<tree::ForStmt, tree::WhileStmt>();
                    if(loop_parent != nullptr && on_flow_tree(*loop_parent)) {
                        do {
                            flow_tree_nodes.insert(current_node);
                            current_node = current_node->parent;
                        } while (current_node != loop_parent);
                    }
                    break;
                case tree::BranchStmt::OpCode::RETURN:
                    break;
            }
        });
    }

    void generate(tree::FunctionDecl& node)
    {
        if(!node.isCompleted()) {
            return;
        }

        mark_flow_tree(node);

        auto*const entry_block = create_block(BasicBlock::Tag::FUNC_INIT);
        auto*const   end_block = create_block(BasicBlock::Tag::FUNC_END );

        flow_graph->set_entry(entry_block);

        function_end_block = end_block;
        function_end_block->init_transition<NullTransition>();

        current_block = entry_block;
        if(node.block)
            visit(*node.block);
        ensure_dead_code();
        current_block->init_transition<NormalTransition>(current_block, function_end_block);

        current_block = nullptr;
    }

    void generate(tree::ForStmt& node)
    {
        if(!on_flow_tree(node)) {
            append_statement(node);
            return;
        }

        auto*const cond_block = create_block(BasicBlock::Tag::FOR_COND);
        auto*const step_block = create_block(BasicBlock::Tag::FOR_STEP);
        auto*const body_block = create_block(BasicBlock::Tag::FOR_BODY);
        auto*const  end_block = create_block(BasicBlock::Tag::FOR_END );

        push_continue_break_block({cond_block, end_block});

        // initialize
        ensure_dead_code();
        current_block->init_transition<NormalTransition>(current_block, cond_block);

        // condition
        current_block = cond_block;
        current_block->init_transition<ConditionTransition>(current_block, node.cond, body_block, end_block);

        // loop body
        current_block = body_block;
        visit(*node.block);
        ensure_dead_code();
        current_block->init_transition<NormalTransition>(current_block, step_block);

        // step expression
        current_block = step_block;
        current_block->insert_statement(new tree::ExpressionStmt(ASTNodeHelper::clone<tree::Expression, ContextToCloneList>(node.step)));
        current_block->init_transition<NormalTransition>(current_block, cond_block);

        // loop end
        current_block = end_block;

        pop_continue_break_block();
    }

    void generate(tree::IfElseStmt& node)
    {
        if(!on_flow_tree(node)) {
            append_statement(node);
            return;
        }

        std::vector<
            std::tuple<
                tree::Selection*, // condition branch
                BasicBlock*     , // condition block
                BasicBlock*     , // body block
                BasicBlock*       // next block
            >
        > condition_infos = {
            std::make_tuple(
                node.if_branch,
                create_block(BasicBlock::Tag::IF_COND),
                create_block(BasicBlock::Tag::IF_TRUE),
                nullptr
            )
        };

        for (auto*const elseif_branch : node.elseif_branches)
        {
            auto*&     prev_false_block = std::get<3>(condition_infos.back());
            auto*const elseif_block     = create_block(BasicBlock::Tag::IF_COND);

            BOOST_ASSERT(prev_false_block == nullptr && "false block is already set!?");

            prev_false_block = elseif_block;

            condition_infos.emplace_back(
                elseif_branch,
                elseif_block,
                create_block(BasicBlock::Tag::IF_ELIF),
                nullptr
            );
        }

        auto*const first_block = std::get<1>(condition_infos.front());
        auto*const  else_block = create_block(BasicBlock::Tag::IF_FALSE);
        auto*const   end_block = create_block(BasicBlock::Tag::IF_END);

        std::get<3>(condition_infos.back()) = else_block;

        // initialize
        ensure_dead_code();
        current_block->init_transition<NormalTransition>(current_block, first_block);

        // conditions
        for (const auto& condition_info : condition_infos)
        {
            auto*const cond_branch = std::get<0>(condition_info);
            auto*const cond_block  = std::get<1>(condition_info);
            auto*const body_block  = std::get<2>(condition_info);
            auto*const next_block  = std::get<3>(condition_info);

            // condition
            current_block = cond_block;
            current_block->init_transition<ConditionTransition>(current_block, cond_branch->cond, body_block, next_block);

            // block body
            current_block = body_block;
            if (cond_branch->block != nullptr)
            {
                visit(*cond_branch->block);
                ensure_dead_code();
            }
            current_block->init_transition<NormalTransition>(current_block, end_block);

            // block next
            current_block = next_block;
        }

        // else block
        BOOST_ASSERT(current_block == else_block && "for-loop should be leaved until reaching else block");

        current_block = else_block;
        if (node.else_block != nullptr)
        {
            visit(*node.else_block);
            ensure_dead_code();
        }
        current_block->init_transition<NormalTransition>(current_block, end_block);

        // if-else end
        current_block = end_block;
    }

    void generate(tree::FlowBlock& node)
    {
        has_flow_ = true;

        auto*const init_block = create_block(BasicBlock::Tag::FLOW_INIT);
        auto*const  end_block = create_block(BasicBlock::Tag::FLOW_END );

        // initialize
        ensure_dead_code();
        current_block->init_transition<NormalTransition>(current_block, init_block);

        // flow init
              current_block = init_block;
        auto& flow_trans    = current_block->init_transition<FlowTransition>(current_block);

        // prepare sub blocks
        for(auto*const stmt : node.objects) {
            auto*const sub_block = create_block(BasicBlock::Tag::FLOW_SUB);

            // sub block
            current_block = sub_block;
            current_block->insert_statement(stmt);
            current_block->init_transition<MergeTransition>(current_block, end_block);

            flow_trans.add_target(sub_block);
        }

        // flow end
        current_block = end_block;
    }

    void generate(tree::Statement& node)
    {
        if(!on_flow_tree(node)) {
            append_statement(node);
            return;
        }

        revisit(node);
    }

    void generate(tree::WhileStmt& node)
    {
        if(!on_flow_tree(node)) {
            append_statement(node);
            return;
        }

        auto*const cond_block = create_block(BasicBlock::Tag::WHILE_COND);
        auto*const body_block = create_block(BasicBlock::Tag::WHILE_BODY);
        auto*const  end_block = create_block(BasicBlock::Tag::WHILE_END );

        auto*const init_block = node.style == tree::WhileStmt::Style::WHILE ? cond_block : body_block;

        push_continue_break_block({init_block, end_block});

        // initialize
        ensure_dead_code();
        current_block->init_transition<NormalTransition>(current_block, init_block);

        // condition
        current_block = cond_block;
        current_block->init_transition<ConditionTransition>(current_block, node.cond, body_block, end_block);

        // loop body
        current_block = body_block;
        visit(*node.block);
        ensure_dead_code();
        current_block->init_transition<NormalTransition>(current_block, cond_block);

        // loop end
        current_block = end_block;

        pop_continue_break_block();
    }

    void generate(tree::SwitchStmt& node)
    {
        if(!on_flow_tree(node)) {
            append_statement(node);
            return;
        }

        auto*const cond_block = create_block(BasicBlock::Tag::SWITCH_COND);
        auto*const  end_block = create_block(BasicBlock::Tag::SWITCH_END );

        push_continue_break_block({nullptr, end_block});

        // initialize
        ensure_dead_code();
        current_block->init_transition<NormalTransition>(current_block, cond_block);

        // switch condition
              current_block = cond_block;
        auto& case_trans    = current_block->init_transition<CaseTransition>(current_block, node.node);

        // prepare case blocks
        for(auto*const one_case : node.cases) {
            auto*const case_block = create_block(BasicBlock::Tag::SWITCH_CASE);

            // case
            current_block = case_block;
            if(one_case->block != nullptr)
                visit(*one_case->block);
            ensure_dead_code();
            current_block->init_transition<NormalTransition>(current_block, end_block);

            case_trans.add_case_trans(one_case->cond, case_block);
        }

        // default case
        if(node.default_block != nullptr)
        {
            auto*const default_block = create_block(BasicBlock::Tag::SWITCH_DEFAULT);

            current_block = default_block;
            visit(*node.default_block);
            ensure_dead_code();
            current_block->init_transition<NormalTransition>(current_block, end_block);

            case_trans.add_case_trans(nullptr, default_block);
        }

        // switch end
        current_block = end_block;

        pop_continue_break_block();
    }

    void generate(tree::BranchStmt& node)
    {
        switch(node.opcode) {
            case tree::BranchStmt::OpCode::BREAK: {
                auto*const end_block = get_continue_break_block().second;

                ensure_dead_code();
                current_block->init_transition<NormalTransition>(current_block, end_block);
                break;
            }
            case tree::BranchStmt::OpCode::CONTINUE: {
                auto*const end_block = get_continue_break_block().first;

                ensure_dead_code();
                current_block->init_transition<NormalTransition>(current_block, end_block);
                break;
            }
            case tree::BranchStmt::OpCode::RETURN:
                append_statement(node);
                current_block->init_transition<NullTransition>();
                break;
        }
    }

    BasicBlock* get_entry() {
        return flow_graph->get_entry();
    }

    std::unique_ptr<FlowGraph> steal_flow_graph() {
        return std::move(flow_graph);
    }

    bool has_flow() const noexcept {
        return has_flow_;
    }

private:

    using ContinueBreakBlocks = std::pair<BasicBlock*, BasicBlock*>;

    void push_continue_break_block(const ContinueBreakBlocks& cbb) {
        continue_break_block_stack.push(cbb);
    }

    void pop_continue_break_block() {
        continue_break_block_stack.pop();
    }

    ContinueBreakBlocks get_continue_break_block() {
        return continue_break_block_stack.top();
    }

    bool on_flow_tree(tree::ASTNode& node) {
        return flow_tree_nodes.count(&node) != 0;
    }

    void append_statement(tree::Statement& stmt) {
        ensure_dead_code();

        current_block->insert_statement(&stmt);
    }

    BasicBlock* create_block(BasicBlock::Tag tag) {
        return flow_graph->create_block(tag);
    }

    void ensure_dead_code() {
        if(has_transition_unknown(*current_block))
            return;

        current_block = create_block(BasicBlock::Tag::DEAD_CODE);
    }

    bool has_flow_ = false;

    std::stack<ContinueBreakBlocks> continue_break_block_stack;
    std::unique_ptr<FlowGraph> flow_graph;
    std::set<tree::ASTNode*> flow_tree_nodes;

    BasicBlock* current_block        = nullptr;
    BasicBlock* function_end_block   = nullptr;
};

//////////////////////////////////////////////////////////////////////////////
// Transitions implementation
//////////////////////////////////////////////////////////////////////////////

NormalTransition::NormalTransition(BasicBlock* source, BasicBlock* target)
    : source(source)
    , target(target)
{
    target->add_predecessor(source);
}

VarSetPtr NormalTransition::get_pass_var() const noexcept
{
    return &target->get_used_variables();
}

bool NormalTransition::replace_target(BasicBlock* from, BasicBlock* to)
{
    bool has_change = false;
    if(target == from) { target = to; has_change = true; }
    return has_change;
}

ConditionTransition::ConditionTransition(
    BasicBlock* source,
    tree::Expression* cond,
    BasicBlock* true_block,
    BasicBlock* false_block
)
    : source(source)
    , cond(cond)
    , true_block(true_block)
    , false_block(false_block)
{
    true_block->add_predecessor(source);
    false_block->add_predecessor(source);
}

VarSetPtrVec ConditionTransition::get_pass_var() const
{
    return { used_vars_or_null(true_block), used_vars_or_null(false_block) };
}

bool ConditionTransition::replace_target(BasicBlock* from, BasicBlock* to)
{
    bool has_change = false;
    if( true_block == from) {  true_block = to; has_change = true; }
    if(false_block == from) { false_block = to; has_change = true; }
    return has_change;
}

CaseTransition::CaseTransition(
    BasicBlock* source,
    tree::Expression* cond
)
    : source(source)
    , cond(cond)
{
}

void CaseTransition::add_case_trans(
    tree::Expression* expr,
    BasicBlock* target)
{
    cases_targets.emplace_back(expr, target);
    target->add_predecessor(source);
}

VarSetPtrVec CaseTransition::get_pass_var() const
{
    VarSetPtrVec result;
    for(auto& ct : get_case_targets()) {
        result.push_back(used_vars_or_null(ct.second));
    }
    return result;
}

bool CaseTransition::replace_target(BasicBlock* from, BasicBlock* to)
{
    bool has_change = false;
    for(auto& ct : cases_targets) {
        if(ct.second == from) { ct.second = to; has_change = true; }
    }
    return has_change;
}

FlowTransition::FlowTransition(
    BasicBlock* source
)
    : source(source)
{
}

void FlowTransition::add_target(BasicBlock* target)
{
    targets.push_back(target);
    target->add_predecessor(source);
}

VarSetPtrVec FlowTransition::get_pass_var() const
{
    VarSetPtrVec result;
    for(auto& t : get_targets()) {
        result.push_back(used_vars_or_null(t));
    }
    return result;
}

bool FlowTransition::replace_target(BasicBlock* from, BasicBlock* to)
{
    bool has_change = false;
    for(auto& target : targets) {
        if(target == from) { target = to; has_change = true; }
    }
    return has_change;
}

MergeTransition::MergeTransition(
    BasicBlock* source,
    BasicBlock* target
)
    : source(source)
    , target(target)
{
    target->add_predecessor(source);
}

VarSet MergeTransition::get_pass_var() const
{
    VarSet result;

    boost::set_intersection(
        get_source()->get_used_variables(),
        get_target()->get_used_variables(),
        std::inserter(result, result.end())
    );

    return std::move(result);
}

bool MergeTransition::replace_target(BasicBlock* from, BasicBlock* to)
{
    bool has_change = false;
    if(target == from) { target = to; has_change = true; }
    return has_change;
}

//////////////////////////////////////////////////////////////////////////////
// BasicBlock implementation
//////////////////////////////////////////////////////////////////////////////

BasicBlock::BasicBlock(Tag t, int serial_id) : serial_id(serial_id), tag(t)
{
}

const TransitionType& BasicBlock::get_transition() const noexcept
{
    return transition;
}

TransitionType& BasicBlock::get_transition() noexcept
{
    return transition;
}

void BasicBlock::set_transition(const TransitionType& t)
{
    transition = t;
}

void BasicBlock::set_transition(TransitionType&& t)
{
    transition = std::move(t);
}

template<typename TransType, typename... ArgTypes>
TransType& BasicBlock::init_transition(ArgTypes&&... args)
{
    BOOST_ASSERT(is_transition_unknown(transition) && "double initialization");

    transition = TransType(std::forward<ArgTypes>(args)...);

    return boost::get<TransType>(transition);
}

BasicBlock::StatementRangeType BasicBlock::get_statements() const
{
    return StatementRangeType(statements);
}

void BasicBlock::insert_statement(tree::Statement* stmt)
{
    statements.push_back(stmt);
}

bool BasicBlock::empty() const noexcept
{
    return get_statements().empty();
}

bool BasicBlock::is_empty_null_transition() const noexcept
{
    return empty() && boost::get<NullTransition>(&get_transition()) == nullptr;
}

BasicBlock::Tag BasicBlock::get_tag() const noexcept
{
    return tag;
}

int BasicBlock::id() const noexcept
{
    return serial_id;
}

std::wstring BasicBlock::get_name() const
{
    switch(tag) {
        case BasicBlock::Tag::FUNC_INIT     : return L"FUNC_INIT"        ; break;
        case BasicBlock::Tag::FUNC_END      : return L"FUNC_END"         ; break;
        case BasicBlock::Tag::FOR_INIT      : return L"FOR_INIT"         ; break;
        case BasicBlock::Tag::FOR_COND      : return L"FOR_COND"         ; break;
        case BasicBlock::Tag::FOR_STEP      : return L"FOR_STEP"         ; break;
        case BasicBlock::Tag::FOR_BODY      : return L"FOR_BODY"         ; break;
        case BasicBlock::Tag::FOR_END       : return L"FOR_END"          ; break;
        case BasicBlock::Tag::IF_COND       : return L"IF_COND"          ; break;
        case BasicBlock::Tag::IF_TRUE       : return L"IF_TRUE"          ; break;
        case BasicBlock::Tag::IF_ELIF       : return L"IF_ELIF"          ; break;
        case BasicBlock::Tag::IF_FALSE      : return L"IF_FALSE"         ; break;
        case BasicBlock::Tag::IF_END        : return L"IF_END"           ; break;
        case BasicBlock::Tag::FLOW_INIT     : return L"FLOW_INIT"        ; break;
        case BasicBlock::Tag::FLOW_SUB      : return L"FLOW_SUB"         ; break;
        case BasicBlock::Tag::FLOW_END      : return L"FLOW_END"         ; break;
        case BasicBlock::Tag::WHILE_COND    : return L"WHILE_COND"       ; break;
        case BasicBlock::Tag::WHILE_BODY    : return L"WHILE_BODY"       ; break;
        case BasicBlock::Tag::WHILE_END     : return L"WHILE_END"        ; break;
        case BasicBlock::Tag::SWITCH_COND   : return L"SWITCH_COND"      ; break;
        case BasicBlock::Tag::SWITCH_CASE   : return L"SWITCH_CASE"      ; break;
        case BasicBlock::Tag::SWITCH_DEFAULT: return L"SWITCH_DEFAULT"   ; break;
        case BasicBlock::Tag::SWITCH_END    : return L"SWITCH_END"       ; break;
        case BasicBlock::Tag::DEAD_CODE     : return L"DEAD_CODE"        ; break;
    }

    return L"<unknown>";
}

std::wstring BasicBlock::get_source_string() const
{
    std::wstring result;
    for(tree::Statement* stmt : statements) {
        if(stmt != statements.front())
            result += L"\n";
        result += stmt->toSource(0);
    }
    return result;
}

std::wstring BasicBlock::get_used_var_str() const
{
    return var_decls_str(used_variables);
}

std::wstring BasicBlock::get_decl_var_str() const
{
    return var_decls_str(decl_vars);
}

void BasicBlock::construct_decl_var()
{
    // defs in this basic block
    for(tree::Statement* stmt : statements) {
        ASTNodeHelper::foreachApply<tree::VariableDecl>(*stmt, [this](tree::VariableDecl& vd) {
            this->decl_vars.insert(&vd);
        });
    }
}

void BasicBlock::init_used_variables()
{
    std::set<tree::VariableDecl*> refs;

    // defs in this basic block
    construct_decl_var();

    // refs in this basic block
    for(tree::Statement* stmt : statements) {
        ASTNodeHelper::foreachApply<tree::IdExpr>(*stmt, [this](tree::IdExpr& id) {
            tree::ASTNode* node = ASTNodeHelper::getCanonicalSymbol(&id);
            if(tree::VariableDecl* vd = tree::cast_or_null<tree::VariableDecl>(node)) {
                if(vd->isLocal()) {
                    this->used_variables.insert(vd);
                }
            }
        });
    }

    // refs in ConditionTransition tree::Expression
    if(ConditionTransition* ct = boost::get<ConditionTransition>(&get_transition())) {
        ASTNodeHelper::foreachApply<tree::IdExpr>(*ct->get_cond(), [this](tree::IdExpr& id) {
            tree::ASTNode* node = ASTNodeHelper::getCanonicalSymbol(&id);
            if(tree::VariableDecl* vd = tree::cast_or_null<tree::VariableDecl>(node)) {
                if(vd->isLocal()) {
                    this->used_variables.insert(vd);
                }
            }
        });
    }

    // refs in CaseTransition tree::Expression
    if(CaseTransition* ct = boost::get<CaseTransition>(&get_transition())) {
        ASTNodeHelper::foreachApply<tree::IdExpr>(*ct->get_condition(), [this](tree::IdExpr& id) {
            tree::ASTNode* node = ASTNodeHelper::getCanonicalSymbol(&id);
            if(tree::VariableDecl* vd = tree::cast_or_null<tree::VariableDecl>(node)) {
                if(vd->isLocal()) {
                    this->used_variables.insert(vd);
                }
            }
        });
    }

    // in = refs - decl_vars
    for(auto v : decl_vars) {
        used_variables.erase(v);
    }
}

const std::set<tree::VariableDecl*>& BasicBlock::get_used_variables() const noexcept
{
    return used_variables;
}

std::set<tree::VariableDecl*>& BasicBlock::get_used_variables() noexcept
{
    return used_variables;
}

void BasicBlock::add_used_variable(tree::VariableDecl* var_decl)
{
    used_variables.insert(var_decl);
}

void BasicBlock::sub_used_variable(tree::VariableDecl* var_decl)
{
    used_variables.erase(var_decl);
}

void BasicBlock::add_used_variables(const std::set<tree::VariableDecl*>& s)
{
    const auto& diff_range = ::zillians::make_set_difference_range(s, this->decl_vars);
    used_variables.insert(boost::begin(diff_range), boost::end(diff_range));
}

const VarSet& BasicBlock::get_decl_variables() const noexcept
{
    return used_variables;
}

VarSet& BasicBlock::get_decl_variables() noexcept
{
    return used_variables;
}

bool BasicBlock::propagate_used_variables()
{
    const auto num_orig_used_var = used_variables.size();

    struct IterateUsedVarVisitor : public boost::static_visitor<>
    {
        IterateUsedVarVisitor(BasicBlock* pred) : from(pred) {}
    public:
        void operator()(const UnknownTransition&) {
            UNREACHABLE_CODE();
        }
        void operator()(const NullTransition&) {
            // null transition has nothing to do
        }
        void operator()(const NormalTransition& t) {
            from->add_used_variables(t.get_target()->get_used_variables());
        }
        void operator()(const ConditionTransition& t) {
            from->add_used_variables(t.get_true_block()->get_used_variables());
            from->add_used_variables(t.get_false_block()->get_used_variables());
        }
        void operator()(const CaseTransition& t) {
            for(const auto& case_target : t.get_case_targets()) {
                from->add_used_variables(case_target.second->get_used_variables());
            }
        }
        void operator()(const FlowTransition& t) {
            for(const auto& target : t.get_targets()) {
                from->add_used_variables(target->get_used_variables());
            }
        }
        void operator()(const MergeTransition&) {
        }
    private:
        BasicBlock* from;
    } ;

    IterateUsedVarVisitor vstr(this);
    boost::apply_visitor(vstr, this->get_transition());

    if(used_variables.size() != num_orig_used_var) {
        return true;
    } else {
        return false;
    }
}

void BasicBlock::add_predecessor(BasicBlock* b)
{
    pred.insert(b);
}

void BasicBlock::erase_predecessor(BasicBlock* b)
{
    pred.erase(b);
}

void BasicBlock::replace_predecessor(BasicBlock* from, BasicBlock* to)
{
    erase_predecessor(from);
    add_predecessor(to);
}

void BasicBlock::clear_predecessor()
{
    pred.clear();
}

const std::set<BasicBlock*>& BasicBlock::get_predecessors() const noexcept
{
    return pred;
}

std::set<BasicBlock*> BasicBlock::get_successors() const
{
    std::set<BasicBlock*> result;
    struct SuccessorCollector : boost::static_visitor<> {
        SuccessorCollector(std::set<BasicBlock*>& r) : r(r) {}
        void operator()(const UnknownTransition   &  ) { UNREACHABLE_CODE(); }
        void operator()(const NullTransition      &  ) { }
        void operator()(const NormalTransition    & t) { r.insert(t.get_target()); }
        void operator()(const ConditionTransition & t) { r.insert(t.get_true_block()); r.insert(t.get_false_block()); }
        void operator()(const CaseTransition      & t) { for(auto i : t.get_case_targets()) r.insert(i.second); }
        void operator()(const FlowTransition      & t) { for(auto i : t.get_targets()) r.insert(i); }
        void operator()(const MergeTransition     & t) { r.insert(t.get_target()); }
        std::set<BasicBlock*>& r;
    } ;
    SuccessorCollector sc(result);
    get_transition().apply_visitor(sc);
    return std::move(result);
}

tree::VariableDecl* BasicBlock::get_flow_assign_var() const noexcept
{
    const MergeTransition* t = boost::get<MergeTransition>(&get_transition());
    if(t == nullptr)
        return nullptr;

    tree::Statement* stmt = get_statements().front();
    BOOST_ASSERT(stmt != nullptr);

    tree::ExpressionStmt* es = tree::cast<tree::ExpressionStmt>(stmt);
    BOOST_ASSERT(es != nullptr);

    tree::BinaryExpr* be = tree::cast<tree::BinaryExpr>(es->expr);
    if(be == nullptr)
        return nullptr;

    BOOST_ASSERT(be->isAssignment());

    tree::ASTNode* resolved_node = ASTNodeHelper::getCanonicalSymbol(be->left);
    tree::VariableDecl* vd = tree::cast<tree::VariableDecl>(resolved_node);
    BOOST_ASSERT(vd != nullptr);

    return vd;
}

int BasicBlock::num_predecessor() const noexcept
{
    return get_predecessors().size();
}

int BasicBlock::num_successor() const noexcept
{
    struct SuccessorNumberVisistor : public boost::static_visitor<int>
    {
        int operator()(const UnknownTransition   &  ) { UNREACHABLE_CODE(); return -1; }
        int operator()(const NullTransition      &  ) { return 0; }
        int operator()(const NormalTransition    &  ) { return 1; }
        int operator()(const ConditionTransition &  ) { return 2; }
        int operator()(const CaseTransition      & t) { return t.size(); }
        int operator()(const FlowTransition      & t) { return t.size(); }
        int operator()(const MergeTransition     &  ) { return 1; }
    } ;

    SuccessorNumberVisistor svn;
    return get_transition().apply_visitor(svn);
}

//////////////////////////////////////////////////////////////////////////////
// FlowGraph implementation
//////////////////////////////////////////////////////////////////////////////

FlowGraph::FlowGraph(const std::wstring& func_name)
    : entry(nullptr)
    , blocks()
    , func_name(func_name)
    , need_this_(false)
    , serial_id_counter(0)
{}

BasicBlock* FlowGraph::get_entry() const noexcept
{
    return entry;
}

void FlowGraph::set_entry(BasicBlock* b)
{
    entry = b;
}

BasicBlock* FlowGraph::create_block(BasicBlock::Tag tag)
{
    BasicBlock* new_block = new BasicBlock(tag, serial_id_counter);
    ++serial_id_counter;
    blocks.push_back(new_block);
    return new_block;
}

void FlowGraph::erase_block(BasicBlock* bbp)
{
    BOOST_ASSERT_MSG(bbp->num_predecessor() == 0, "Erased block must be unreachable");

    // if erased block is entry, assign new entry with its successor
    if(bbp == get_entry()) {
        NormalTransition* nt = boost::get<NormalTransition>(&bbp->get_transition());
        BOOST_ASSERT_MSG(nt != nullptr, "While erase entry block, can not determine new entry");
        set_entry(nt->get_target());
    }

    auto i = boost::find_if(blocks, [&bbp](const BasicBlock& b){ return &b == bbp; });
    blocks.erase(i);
}

boost::ptr_vector<BasicBlock>::size_type FlowGraph::size() const noexcept
{
    return get_blocks().size();
}

const boost::ptr_vector<BasicBlock>& FlowGraph::get_blocks() const noexcept {
    return blocks;
}

boost::ptr_vector<BasicBlock>& FlowGraph::get_blocks() noexcept {
    return blocks;
}

const std::wstring& FlowGraph::get_func_name() const noexcept {
    return func_name;
}

void FlowGraph::init_used_variables()
{
    for(auto& b : blocks) {
        b.init_used_variables();
    }
}

bool FlowGraph::propagate_used_variables()
{
    bool any_block_modified = false;

    for(auto& b : blocks) {
        if(FlowTransition* ft = boost::get<FlowTransition>(&b.get_transition())) {
            std::vector<BasicBlock*>& targets = ft->get_targets();
            std::vector<int> orig_used_num(targets.size());
            boost::transform(targets, orig_used_num.begin(), [](BasicBlock* b) { return b->get_used_variables().size(); });
            // propagate all var throgh first sub-flow block
            BasicBlock* first_sub_flow = targets[0];
            BasicBlock* end_flow = boost::get<MergeTransition>(first_sub_flow->get_transition()).get_target();
            first_sub_flow->add_used_variables(end_flow->get_used_variables());
            // copy or move used variable to non-first sub-flow block
            for(std::vector<BasicBlock*>::size_type i = 1; i < targets.size(); ++i) {
                BasicBlock* sub_flow_block = targets[i];
                tree::VariableDecl* assign_vd = sub_flow_block->get_flow_assign_var();
                if(assign_vd == nullptr) {
                    continue;
                }
                if(assign_vd == first_sub_flow->get_flow_assign_var()) {
                    sub_flow_block->add_used_variable(assign_vd);
                } else {
                    sub_flow_block->add_used_variable(assign_vd);
                    first_sub_flow->sub_used_variable(assign_vd);
                }
            }
            std::vector<int> new_used_num(targets.size());
            boost::transform(targets, new_used_num.begin(), [](BasicBlock* b) { return b->get_used_variables().size(); });
            if(new_used_num != orig_used_num) {
                any_block_modified = true;
            }
        }
        // propagate all blocks except the flow-merge block
        const bool one_block_modified = b.propagate_used_variables();
        if(one_block_modified) {
            any_block_modified = true;
        }
    }

    return any_block_modified;
}

std::wstring transition_color(BasicBlock* b)
{
    if(b == nullptr) {
        return L"red";
    } else {
        return L"black";
    }
}

static std::wstring pred_str(const BasicBlock& b)
{
    std::wostringstream oss;
    oss << b.get_predecessors().size() << L"[";
    for(auto i = b.get_predecessors().begin(); i != b.get_predecessors().end(); ++i) {
        if(i != b.get_predecessors().begin()) oss << L",";
        oss << (*i)->id();
    }
    oss << L"]";
    return oss.str();
}

static std::wstring pass_var_str(const VarSet& vsp)
{
    std::wostringstream oss;
    oss << L"[";
    for(auto i = vsp.begin(); i != vsp.end(); ++i) {
        if(i != vsp.begin()) oss << L",";
        tree::VariableDecl* vd = *i;
        oss << vd->name->toString();
    }
    oss << L"]";
    return oss.str();
}

void FlowGraph::gen_graphviz(const std::wstring& file_name) const
{
    // FIXME currently, c++ standard does not support wide path
    // Fix this onec any standard solution availiable
    std::string fn = zillians::ws_to_s(file_name);
    std::wofstream fout(fn.c_str());
    fout << L"digraph G {" << std::endl;
    fout << L"// nodes" << std::endl;
    fout << L"    n0[shape=\"rectangle\" color=\"red\"]" << std::endl;
    for(const auto& b : blocks) {
        fout << L"    n" << std::hex << &b
             << L" ["
                << L"shape=none fontname=\"Monospace\" margin=\"0\" "
                << (&b == entry ? L"color=\"green\"" : (b.get_transition().type() == typeid(NullTransition) ? L"color=\"red\"" : L"") )
                << L"label=<<table border=\"1\" cellborder=\"0\" cellpadding=\"0\">"
                    << L"<tr><td cellpadding=\"4\" align=\"left\" bgcolor=\"gray\">" << std::dec << b.id() << L" " << b.get_name() << L" " << pred_str(b) << L"</td></tr>"
                    << L"<tr><td align=\"left\" bgcolor=\"pink\">" << b.get_decl_var_str() << L"</td></tr>"
                    << L"<tr><td align=\"left\" bgcolor=\"lightblue\">" << b.get_used_var_str() << L"</td></tr>"
                    << source_to_html_table(b.get_source_string())
                << L"</table>>"
             << L"];" << std::endl;
    }
    fout << L"// edges" << std::endl;

    struct TransitionVisitor : public boost::static_visitor<>
    {
        TransitionVisitor(const BasicBlock& b, std::wofstream& fout) : from(b), fout(fout) {}
    public:
        void operator()(const UnknownTransition&) {
            UNREACHABLE_CODE();
        }
        void operator()(const NullTransition&) {
        }
        void operator()(const NormalTransition& t) {
            fout << L"    n" << std::hex << &from << L"-> n" << std::hex << t.get_target() << L";" << std::endl;
        }
        void operator()(const ConditionTransition& t) {
            fout << L"    n" << std::hex << &from << L"-> n" << std::hex << t.get_true_block() << L" [label=\"true\"];" << std::endl;
            fout << L"    n" << std::hex << &from << L"-> n" << std::hex << t.get_false_block() << L" [label=\"false\"];" << std::endl;
        }
        void operator()(const CaseTransition& t) {
            for(const auto& case_target : t.get_case_targets()) {
                fout << L"    n" << std::hex << &from << L"-> n" << std::hex << case_target.second << L";" << std::endl;
            }
        }
        void operator()(const FlowTransition& t) {
            for(const auto& target : t.get_targets()) {
                fout << L"    n" << std::hex << &from << L"-> n" << std::hex << target << L";" << std::endl;
            }
        }
        void operator()(const MergeTransition& t) {
            fout << L"    n" << std::hex << &from << L"-> n" << std::hex << t.get_target()
                  << L"[label=\"" << pass_var_str(t.get_pass_var()) << L"\"]"
                  << L";" << std::endl;
        }
    private:
        const BasicBlock& from;
        std::wofstream& fout;
    } ;

    for(const auto& b : blocks) {
        TransitionVisitor tv(b, fout);
        boost::apply_visitor(tv, b.get_transition());
    }

    fout << L"}" << std::endl;
    fout.close();
}

void FlowGraph::construct_pred()
{
    struct PredecessorVisitor : public boost::static_visitor<>
    {
        PredecessorVisitor(BasicBlock& b) : from(b) {}
    public:
        void operator()(const UnknownTransition&) {
        }
        void operator()(const NullTransition&) {
        }
        void operator()(const NormalTransition& t) {
            t.get_target()->add_predecessor(&from);
        }
        void operator()(const ConditionTransition& t) {
            t.get_true_block ()->add_predecessor(&from);
            t.get_false_block()->add_predecessor(&from);
        }
        void operator()(const CaseTransition& t) {
            for(const auto& case_target : t.get_case_targets()) {
                case_target.second->add_predecessor(&from);
            }
        }
        void operator()(const FlowTransition& t) {
            for(const auto& target : t.get_targets()) {
                target->add_predecessor(&from);
            }
        }
        void operator()(const MergeTransition& t) {
            t.get_target()->add_predecessor(&from);
        }
    private:
        BasicBlock& from;
    } ;

    for(auto& b : blocks) {
        PredecessorVisitor pv(b);
        b.get_transition().apply_visitor(pv);
    }
}

bool FlowGraph::replace_entry(BasicBlock* from, BasicBlock* to)
{
    if(entry == from) {
        entry = to;
        return true;
    } else {
        return false;
    }
}

struct ReplacePredecessorVisitor : public boost::static_visitor<bool>
{
    ReplacePredecessorVisitor(BasicBlock* pred, BasicBlock* from, BasicBlock* to) : pred(pred), from(from), to(to) {}
public:
    bool operator()(UnknownTransition&) {
        UNREACHABLE_CODE();
        return false;
    }
    bool operator()(NullTransition&) {
        return false;
    }
    template <typename T>
    bool operator()(T& t) {
        static_assert(!std::is_const<T>::value, "T can not be const");
        if(to != nullptr)
            to->replace_predecessor(from, pred);
        return t.replace_target(from, to);
    }
private:
    BasicBlock* pred;
    BasicBlock* from;
    BasicBlock* to;
} ;

bool FlowGraph::remove_empty_blocks()
{
    // optimize out empty node
    std::vector<boost::ptr_vector<BasicBlock>::iterator> erased_blocks_iters;
    for(boost::ptr_vector<BasicBlock>::iterator i = blocks.begin(); i != blocks.end(); ++i) {
        auto& b = *i;

        // don't touch flow blocks
        if(b.get_tag() == BasicBlock::Tag::FLOW_SUB || b.get_tag() == BasicBlock::Tag::FLOW_END) {
            continue;
        }

        // don't touch non normal transition blocks
        const NormalTransition*const normal_trans = boost::get<NormalTransition>(&b.get_transition());
        if(normal_trans == nullptr) {
            continue;
        }

        // don't touch non empty block
        if(!b.empty()) {
            continue;
        }

        BasicBlock* old_target = &b;
        BasicBlock* new_target = normal_trans->get_target();
        replace_entry(old_target, new_target);
        auto& preds = b.get_predecessors();

        if(preds.empty()) { // func-init block
            new_target->erase_predecessor(&b);
        } else { // non-func-init block
            for(auto& pred : preds) {
                ReplacePredecessorVisitor rv(pred, old_target, new_target);
                pred->get_transition().apply_visitor(rv);
            }
        }
        b.clear_predecessor();

        erased_blocks_iters.push_back(i);
    }

    for(auto i = erased_blocks_iters.rbegin(); i != erased_blocks_iters.rend(); ++i) {
        erase_block(&**i);
    }

    const bool has_optimize = !erased_blocks_iters.empty();

    return has_optimize;
}

bool FlowGraph::remove_null_transition_block()
{
    bool has_optimize = false;
    while(remove_null_transition_block_one_level()) {
        has_optimize = true;
    }
    return has_optimize;
}

struct SetTargetToNullVisitor : public boost::static_visitor<>
{
    SetTargetToNullVisitor(BasicBlock& b, BasicBlock* orig) : b(b), orig_target(orig) {}
public:
    void operator()(UnknownTransition&) {
        UNREACHABLE_CODE();
    }
    void operator()(NullTransition&) {
        UNREACHABLE_CODE();
    }
    void operator()(NormalTransition&) {
        b.set_transition(NullTransition());
    }
    void operator()(ConditionTransition& t) {
        if(t.get_true_block () == orig_target) { t.set_true_block (nullptr); }
        if(t.get_false_block() == orig_target) { t.set_false_block(nullptr); }
        if(t.get_true_block() == nullptr && t.get_false_block() == nullptr) {
            b.set_transition(NullTransition());
        }
    }
    void operator()(CaseTransition& t) {
        for(auto& ct : t.get_case_targets()) {
            if(ct.second == orig_target) {
                ct.second = nullptr;
            }
        }
        bool is_all_null = boost::algorithm::all_of(t.get_case_targets(), [](const std::pair<tree::Expression*, BasicBlock*>& ct) {
            return ct.second == nullptr;
        });
        if(is_all_null) {
            b.set_transition(NullTransition());
        }
    }
    void operator()(FlowTransition&) {
        UNREACHABLE_CODE();
    }
    void operator()(MergeTransition&) {
        UNREACHABLE_CODE();
    }
private:
    BasicBlock& b;
    BasicBlock* orig_target;
} ;

bool FlowGraph::remove_null_transition_block_one_level()
{
    //// TODO check existance of cyclic unreachables blocks

    std::set<BasicBlock*> erased_blocks;
    for(boost::ptr_vector<BasicBlock>::iterator i = blocks.begin(); i != blocks.end(); ++i) {
        auto& b = *i;
        if(b.get_tag() == BasicBlock::Tag::FLOW_END || b.get_tag() == BasicBlock::Tag::FLOW_SUB) {
            continue;
        }
        if(b.get_transition().type() != typeid(NullTransition)) {
            continue;
        }
        if(&b == get_entry()) {
            continue;
        }
        if(!b.get_statements().empty()) {
            continue;
        }
        for(auto pred : b.get_predecessors()) {
            SetTargetToNullVisitor v(*pred, &b);
            pred->get_transition().apply_visitor(v);
        }
        erased_blocks.insert(&b);
        b.clear_predecessor();
    }

    for(auto b = erased_blocks.rbegin(); b != erased_blocks.rend(); ++b) {
        erase_block(*b);
    }

    return !erased_blocks.empty();
}

bool FlowGraph::optimize()
{
    bool has_optimize = false;
    has_optimize |= remove_empty_blocks();
    has_optimize |= remove_null_transition_block();
    has_optimize |= merge_blocks();
    
    // TODO logic merge

    return has_optimize;
}

void FlowGraph::set_need_this() noexcept
{
    need_this_ = true;
}

bool FlowGraph::need_this() const noexcept
{
    return need_this_;
}

bool FlowGraph::try_merge_block(BasicBlock* b)
{
    // currently, don't try to merge flow blocks. TBD
    if(b->get_tag() == BasicBlock::Tag::FLOW_SUB)
        return false;
    if(b->get_tag() == BasicBlock::Tag::FLOW_END)
        return false;

    // block has only one predecessor
    if(b->num_predecessor() != 1)
        return false;

    // and the predecessor has only one successor
    BasicBlock* pred = *b->get_predecessors().begin();
    if(pred->num_successor() != 1)
        return false;

    // ready to merge
    for(tree::Statement* stmt : b->get_statements()) {
        pred->insert_statement(stmt);
    }

    // re-wire predecessor
    auto successors = b->get_successors();
    for(auto suc : successors) {
        ReplacePredecessorVisitor rv(pred, b, suc);
        pred->get_transition().apply_visitor(rv);
    }

    // re-wire transition
    pred->set_transition(b->get_transition());

    // erase the block
    b->clear_predecessor();

    return true;
}

void FlowGraph::err_status() const
{
    const auto get_block_info = [](const BasicBlock& block)
    {
        std::wostringstream stream;

        stream << L"#" << std::setw(2) << std::setfill(L'0') << block.id() << L" " << block.get_name();

        return stream.str();
    };

    const auto get_trans_info = [get_block_info](const BasicBlock& block)
    {
        struct TransitionInfoVisitor : boost::static_visitor<std::deque<std::wstring>>
        {
            explicit TransitionInfoVisitor(std::function<std::wstring (const BasicBlock &)> func)
                : get_block_info(func)
            {
            }

            result_type operator()(const UnknownTransition &) const { return {L"unknown"}; }
            result_type operator()(const    NullTransition &) const { return {L"null"   }; }

            result_type operator()(const NormalTransition &trans) const
            {
                const auto *const source = trans.get_source();
                const auto *const target = trans.get_target();

                return {
                    L"normal",
                    get_block_info(*source) + L" -> " + get_block_info(*target)
                };
            }

            result_type operator()(const ConditionTransition &trans) const
            {
                const auto *const source    = trans.get_source();
                const auto *const cond_expr = trans.get_cond();
                const auto *const on_true   = trans.get_true_block();
                const auto *const on_false  = trans.get_false_block();

                return {
                    L"condition[" + cond_expr->toSource(0) + L']',
                    get_block_info(*source) + L"[true ] -> " + get_block_info(*on_true ),
                    get_block_info(*source) + L"[false] -> " + get_block_info(*on_false)
                };
            }

            result_type operator()(const CaseTransition &trans) const
            {
                const auto *const source    = trans.get_source();
                const auto *const cond_expr = trans.get_condition();

                result_type result = {L"case[" + cond_expr->toSource(0) + L'['};

                for (const auto &each : trans.get_case_targets())
                {
                    const auto *const case_expr  = each.first;
                    const auto *const case_block = each.second;

                    if (case_expr == nullptr)
                        result.emplace_back(
                            get_block_info(*source) + L"[default] -> " + get_block_info(*case_block)
                        );
                    else
                        result.emplace_back(
                            get_block_info(*source) + L'[' + case_expr->toSource(0) + L"] -> " + get_block_info(*case_block)
                        );
                }

                return result;
            }

            result_type operator()(const FlowTransition &trans) const
            {
                const auto *const source = trans.get_source();

                result_type result = {L"flow"};

                for (const auto *const flowed_block : trans.get_targets())
                {
                    result.emplace_back(
                        get_block_info(*source) + L" -> " + get_block_info(*flowed_block)
                    );
                }

                return result;
            }

            result_type operator()(const MergeTransition &trans) const
            {
                const auto *const source = trans.get_source();
                const auto *const target = trans.get_target();

                return {
                    L"merge",
                    get_block_info(*source) + L" -> " + get_block_info(*target)
                };
            }

        private:
            std::function<std::wstring (const BasicBlock &)> get_block_info;
        } visitor(get_block_info);

        const auto &trans = block.get_transition();

        return trans.apply_visitor(visitor);
    };

    BOOST_ASSERT(entry != nullptr);

    std::wcerr << L"entry: " << get_block_info(*entry) << std::endl;

    for (const auto &block : blocks)
    {
        const auto &trans_info = get_trans_info(block);
        const auto &stmt_count = boost::size(block.get_statements());
        const auto &preds      = block.get_predecessors();
        const auto &succs      = block.get_successors();

        std::wcerr << L"    " << get_block_info(block) << L", statement count = " << stmt_count << L", transition = " << trans_info.front() << std::endl;

        for (const auto &info : boost::make_iterator_range(trans_info, 1, 0))
            std::wcerr << L"        " << info << std::endl;

        if (!preds.empty())
        {
            std::wcerr << L"        predecessors:" << std::endl;

            for (const auto *const pred : preds)
                std::wcerr << L"            " << get_block_info(*pred) << std::endl;
        }

        if (!succs.empty())
        {
            std::wcerr << L"        successors:" << std::endl;

            for (const auto *const succ : succs)
                std::wcerr << L"            " << get_block_info(*succ) << std::endl;
        }
    }
}

bool FlowGraph::merge_blocks()
{
    std::set<BasicBlock*> merged_blocks;

    for(BasicBlock& b : get_blocks()) {
        if(try_merge_block(&b))
            merged_blocks.insert(&b);
    }

    for(auto& b : merged_blocks) {
        erase_block(b);
    }

    return !merged_blocks.empty();
}

//////////////////////////////////////////////////////////////////////////////
// flow split stand alone function
//////////////////////////////////////////////////////////////////////////////

std::unique_ptr<FlowGraph> flow_split(tree::FunctionDecl& func)
{
    if(!func.is_task) {
        return nullptr;
    }

    FlowGraphGeneratorVisitor flow_graph_generator(func.name->toString());
    flow_graph_generator.visit(func);

    if(!flow_graph_generator.has_flow()) {
        return nullptr;
    }

    std::unique_ptr<FlowGraph> fg = flow_graph_generator.steal_flow_graph();

    // TODO optimize need_this(), current, all blocks splited from member function are need_this.
    if(func.is_member && !func.is_static) {
        fg->set_need_this();
    }

    fg->init_used_variables();
    while(fg->propagate_used_variables());

    // FIXME FlowGraph::optimize is buggy, enable optimization after it's stable
    //fg->optimize();

    // DEBUG output graphviz
    //if(func.name->toString() == L"main")
    //    fg->gen_graphviz(func.name->toString() + L".dot");
    return std::move(fg);
}

} } } } }

