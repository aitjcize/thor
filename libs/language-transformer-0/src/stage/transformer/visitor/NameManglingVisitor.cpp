/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "utility/UnicodeUtil.h"
#include "core/Prerequisite.h"
#include "core/Platform.h"
#include "core/Visitor.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/tree/basic/PrimitiveType.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/NameManglingVisitor.h"

namespace zillians { namespace language { namespace tree { namespace visitor {

#define PUSH_NESTED_STATE() \
        current_nested_depth_stack.push(current_nested_depth); \
        current_nested_depth = 0; \
        max_nested_depth_stack.push(max_nested_depth); \
        max_nested_depth = 0;

#define POP_NESTED_STATE() \
        current_nested_depth = current_nested_depth_stack.top(); \
        current_nested_depth_stack.pop(); \
        max_nested_depth = max_nested_depth_stack.top(); \
        max_nested_depth_stack.pop();

#define ENTER_NESTED_COMPONENT() \
        current_nested_depth++; \
        max_nested_depth++;

#define MARK_NESTED_COMPONENT_HEAD() \
        if(max_nested_depth > 1) stream << "N";

#define LEAVE_NESTED_COMPONENT() \
        current_nested_depth--; \
        if(max_nested_depth > 1 && current_nested_depth == 0) stream << "E"; \

NameManglingVisitor::NameManglingVisitor()
{
    REGISTER_ALL_VISITABLE_ASTNODE(mangleInvoker);
    reset();
}

void NameManglingVisitor::mangle(ASTNode& node)
{
    UNREACHABLE_CODE();
}

void NameManglingVisitor::mangle(SimpleIdentifier& node)
{
    stream << node.name.length() << encode(node.name);
}

void NameManglingVisitor::mangle(TemplatedIdentifier& node)
{
    BOOST_ASSERT(node.isFullySpecialized() && "mangling non-fully-specialized template identifier");

    // do not generate function name for "new<>"
    auto function_decl = node.getOwner<FunctionDecl>();
    if (!function_decl || !function_decl->isConstructor())
    {
        if(mangle_identifier_name)
        {
            visit(*node.id);
        }
        else
        {
            mangle_identifier_name = true;
        }
    }

    addSubstitution(&node); // the template identifier should be also added into substitution table

    stream << "I";
    {
        int j = 0;
        for(auto* template_id : node.templated_type_list)
        {
            TemplatedIdentifierContext* ctx = new TemplatedIdentifierContext();
            ctx->template_index = j;
            ctx->substitution_index = -1;
            template_id->set<TemplatedIdentifierContext>(ctx);

            //cleanups.push_back([template_id] {
            //    template_id->set<TemplatedIdentifierContext>(NULL);
            //});

            ++j;
        }

        bool previous_flag_value = class_as_scope_mode;
        class_as_scope_mode = false;
        for(auto* template_id : node.templated_type_list)
        {
            PUSH_NESTED_STATE();
            visit(*template_id->specialized_type);
            POP_NESTED_STATE();
        }
        class_as_scope_mode = previous_flag_value;
    }
    stream << "E";
}

void NameManglingVisitor::mangle(NestedIdentifier& node)
{
    UNREACHABLE_CODE();
}

void NameManglingVisitor::mangle(TypeSpecifier& node)
{
    UNREACHABLE_CODE();
}

void NameManglingVisitor::mangle(FunctionSpecifier& node)
{
    stream << "PF";

    // TODO handle function pointer type
    UNIMPLEMENTED_CODE();

    stream << "E";
}

void NameManglingVisitor::mangle(MultiSpecifier& node)
{
}

void NameManglingVisitor::mangle(NamedSpecifier& node)
{
    Type* resolved_type = ResolvedType::get(&node);
    BOOST_ASSERT(resolved_type && "invalid resolved type for variable declaration");

    PUSH_NESTED_STATE();
    visit(*resolved_type->getAsDecl());
    POP_NESTED_STATE();
}

void NameManglingVisitor::mangle(PrimitiveSpecifier& node)
{
    switch(node.getKind())
    {
    case PrimitiveKind::VOID_TYPE:    stream << "v"; break;
    case PrimitiveKind::BOOL_TYPE:    stream << "b"; break;
    case PrimitiveKind::INT8_TYPE:    stream << "a"; break;
    case PrimitiveKind::INT16_TYPE:   stream << "s"; break;
    case PrimitiveKind::INT32_TYPE:   stream << "i"; /* or 'l' ? */ break;
#ifdef __PLATFORM_MAC__
    case PrimitiveKind::INT64_TYPE:   stream << "x"; break;
#else
    case PrimitiveKind::INT64_TYPE:   stream << "l"; break;
#endif
    case PrimitiveKind::FLOAT32_TYPE: stream << "f"; break;
    case PrimitiveKind::FLOAT64_TYPE: stream << "d"; break;
    default: UNREACHABLE_CODE(); break;
    }
}

void NameManglingVisitor::mangle(Package& node)
{
    if(!node.isRoot())
    {
        ENTER_NESTED_COMPONENT();

        // if it's not root package (empty identifier), try to find if the current package (as a component) in the substitution table
        // if found, then we use the substituted symbol
        // otherwise we encode the package identifier and add it to substitution table
        int index = findSubstitution(&node);
        if(index == -1)
        {
            visit(*node.parent);
            visit(*node.id);
            addSubstitution(&node);
        }
        else
        {
            MARK_NESTED_COMPONENT_HEAD();
            stream << getSubstitution("S", index);
        }

        LEAVE_NESTED_COMPONENT();
    }
    else
    {
        // if it's the root package (empty identifier), we may need to output "N" if it's a nested identifier
        // so we use markedAsNested static local variable to determine that state
        MARK_NESTED_COMPONENT_HEAD();
    }
}

void NameManglingVisitor::mangle(Source& node)
{
    visit(*node.parent);
}

void NameManglingVisitor::mangle(EnumDecl& node)
{
    ENTER_NESTED_COMPONENT();

    int index = findSubstitution(&node);
    if(index == -1)
    {
        visit(*node.parent);
        visit(*node.name);
        addSubstitution(&node);
    }
    else
    {
        MARK_NESTED_COMPONENT_HEAD();
        stream << getSubstitution("S", index);
    }

    LEAVE_NESTED_COMPONENT();
}

void NameManglingVisitor::mangle(TypenameDecl& node)
{
    // if the typename declaration does not belong to a function declaration, which must belong to a class declaration
    // in which case, we should mangle its actual type instead of use TXXX_ (since it's an "instantiation", so all member functions are instantiated to a specialized instance)
    if(node.getOwner<FunctionDecl>())
    {
        TemplatedIdentifierContext* ctx = node.get<TemplatedIdentifierContext>();
        if(ctx->substitution_index == -1)
        {
            ctx->substitution_index = addSubstitution(&node);

            stream << getSubstitution("T", ctx->template_index);

            addSubstitution(NULL); // append the pointer version
        }
        else
        {
            stream << getSubstitution("S", ctx->substitution_index);
            // TODO if the specialized type is resolved to yet another typename declaration, use template index for it
            // TODO find out how to handle nested template? (use class template parameter within a member function declaration)
        }

        MARK_NESTED_COMPONENT_HEAD();
    }
    else
    {
        visit(*node.specialized_type);
    }
}

void NameManglingVisitor::mangle(TypedefDecl& node)
{
    // for typedef declaration, just follow the type specifier
    visit(*node.type);
}

void NameManglingVisitor::mangle(ClassDecl& node)
{
    if(isa<TemplatedIdentifier>(node.name) && !cast<TemplatedIdentifier>(node.name)->isFullySpecialized())
    {
        UNREACHABLE_CODE();
        return;
    }

    ENTER_NESTED_COMPONENT();

    bool mangle_pointer_version = !class_as_scope_mode;
    int index = findSubstitution(&node);

    ClassManglingContext* ctx = node.get<ClassManglingContext>();
    if(!ctx)
    {
        ctx = new ClassManglingContext();
        node.set<ClassManglingContext>(ctx);
        cleanups.push_back([&] { node.set<ClassManglingContext>(NULL); });
    }

    if(index == -1)
    {
        // if the class declaration is a wrapper of it
        bool is_special_wrapper_class = false;
        if(ASTNodeHelper::hasSystemLinkage(&node))
        {
            std::wstring class_name = node.name->toString();
            if(class_name == L"ptr_")
            {
                stream << "P"; is_special_wrapper_class = true;
            }
            else if(class_name == L"ref_")
            {
                stream << "R"; is_special_wrapper_class = true;
            }
            else if(class_name == L"const_")
            {
                stream << "K"; is_special_wrapper_class = true;
            }
            else if(class_name == L"volatile_")
            {
                stream << "V"; is_special_wrapper_class = true;
            }
        }

        if(mangle_pointer_version)
            stream << "P"; // since we pass all classes as pointer when interfacing with C++, pointer to class is assumed here

        if(is_special_wrapper_class)
        {
            visit(*cast<TemplatedIdentifier>(node.name)->templated_type_list[0]->specialized_type);
            addSubstitution(&node);
        }
        else
        {
            visit(*node.parent);
            visit(*node.name);
            addSubstitution(&node);
        }

        if(mangle_pointer_version)
            ctx->pointer_type_mangling_index = addSubstitution(NULL);
    }
    else
    {
        // Append "N" if the resolved class is within scope
        const auto* location = substitution_table[index]->getOwner<Package>();
        BOOST_ASSERT(location != nullptr && "class is not under any package");

        const bool is_templated_identifier = isa<TemplatedIdentifier>(substitution_table[index]);
        const bool at_root                 = location->isRoot();
        const bool need_scope_identifier   = is_templated_identifier && !at_root;

        if(mangle_pointer_version)
        {
            if(ctx->pointer_type_mangling_index == -1)
            {
                stream << "P";
                if(need_scope_identifier) stream << "N";
                stream << getSubstitution("S", index);
                ctx->pointer_type_mangling_index = addSubstitution(NULL);
            }
            else
            {
                stream << getSubstitution("S", ctx->pointer_type_mangling_index);
            }
        }
        else
        {
            if(need_scope_identifier) stream << "N";
            stream << getSubstitution("S", index);
        }

        // If we match a TemplatedIdentifier, we still need to add the ClassDecl into the table
        // And we need to continue to traverse tid
        if(is_templated_identifier)
        {
            mangle_identifier_name = false;
            visit(*node.name);
            addSubstitution(&node);

            if(!at_root) stream << "E";
        }

        //MARK_NESTED_COMPONENT_HEAD();
    }

    LEAVE_NESTED_COMPONENT();
}

void NameManglingVisitor::mangle(FunctionDecl& node)
{
    if(isa<TemplatedIdentifier>(node.name) && !cast<TemplatedIdentifier>(node.name)->isFullySpecialized())
    {
        UNREACHABLE_CODE();
        return;
    }

    if(node.is_lambda)
    {
        // no need to mangling lambda name
        visit(*node.name);
        return;
    }

    ENTER_NESTED_COMPONENT();

    {
        bool s = class_as_scope_mode;
        class_as_scope_mode = true;
        visit(*node.parent);
        class_as_scope_mode = s;
    }

    // handle special member function names
    bool is_special_function = false;
    bool is_template_constructor = false;
    if(node.is_member)
    {
        std::wstring function_name = node.name->toString();
        if(node.isConstructor())
        {
            stream << "C1";
            is_special_function = true;
            is_template_constructor = isa<TemplatedIdentifier>(node.name);
        }
        // TODO where's allocating constructor?
        else if(node.isDestructor())
        {
            // TODO make sure which dtor is this
            stream << "D1";
            is_special_function = true;
        }
        else if(function_name == L"__cxa_delete")
        {
            // TODO make sure which dtor is this
            stream << "D0";
            is_special_function = true;
        }
    }

    if(!is_special_function || is_template_constructor)
    {
        bool s = class_as_scope_mode;
        class_as_scope_mode = true;
        visit(*node.name);
        class_as_scope_mode = s;
    }

    LEAVE_NESTED_COMPONENT();

    // for non-template function, the return type is ignored
    if(isa<TemplatedIdentifier>(node.name) && !is_special_function)
        visit(*node.type);

    if(node.parameters.size() > 0)
    {
        for(auto* param : node.parameters)
        {
            PUSH_NESTED_STATE();
            visit(*(param->type));
            POP_NESTED_STATE();
        }
    }
    else
    {
        stream << "v"; // if there's no parameter, we should mangle it as void
    }
}

void NameManglingVisitor::mangle(VariableDecl& node)
{
    if(isa<TemplatedIdentifier>(node.name) && !cast<TemplatedIdentifier>(node.name)->isFullySpecialized())
    {
        UNREACHABLE_CODE();
        return;
    }

    if (auto parent = node.getOwner<FunctionDecl>())
    {
        stream << "Z";
        visit(*parent);
        stream << "E";
        visit(*node.name);
    }
    else if (auto parent = node.getOwner<ClassDecl>())
    {
        ENTER_NESTED_COMPONENT();
        bool s = class_as_scope_mode;

        class_as_scope_mode = true;
        visit(*parent);
        visit(*node.name);

        class_as_scope_mode = s;
        LEAVE_NESTED_COMPONENT();
    }
    else if (auto parent = node.getOwner<Package>())
    {
        ENTER_NESTED_COMPONENT();

        visit(*parent);
        visit(*node.name);

        LEAVE_NESTED_COMPONENT();
    }
}

std::string NameManglingVisitor::encode(const std::wstring ucs4)
{
    auto toAsciiNumber = [](char c) -> std::string
        {
            static char buffer[4];
            snprintf(buffer, 3, "%d", c);
            return buffer;
        };

    std::string ucs4_to_utf8_temp;
    std::string utf8_to_llvm_temp;

    // first we should covert UCS-4 to UTF-8
    ucs4_to_utf8(ucs4, ucs4_to_utf8_temp);

    // because LLVM only accept identifier of the form: '[%@][a-zA-Z$._][a-zA-Z$._0-9]*'
    // we have to convert illegal identifier into legal one
    for(std::string::const_iterator i = ucs4_to_utf8_temp.begin(), e = ucs4_to_utf8_temp.end(); i != e; ++i)
    {
        char c = *i;
        if( ((i == ucs4_to_utf8_temp.begin()) ? false : isdigit(c)) || isalpha(c) || (c == '_') || (c == '.') )
            utf8_to_llvm_temp.push_back(c);
        else
        {
            utf8_to_llvm_temp.push_back('$');
            utf8_to_llvm_temp.append(toAsciiNumber(c));
            utf8_to_llvm_temp.push_back('$');
        }
    }

    return utf8_to_llvm_temp;
}

void NameManglingVisitor::reset()
{
    current_nested_depth = 0;
    BOOST_ASSERT(current_nested_depth_stack.size() == 0 && "dirty nested flag stack");
    max_nested_depth = 0;
    BOOST_ASSERT(max_nested_depth_stack.size() == 0 && "dirty nested flag stack");

    class_as_scope_mode = false;
    mangle_identifier_name = true;

    substitution_table.clear();

    for(auto& func : cleanups) func();
    cleanups.clear();

    stream.str("");
    stream << "_Z";
}

void NameManglingVisitor::mangleClassAsScope(bool flag)
{
    class_as_scope_mode = flag;
}

bool NameManglingVisitor::isEqualPackage(Package* lhs, ASTNode* table_item)
{
    if(lhs == NULL)
        return false;

    if(table_item == NULL)
        return false;

    Package* rhs = cast<Package>(table_item);
    if(rhs == NULL)
        return false;

    if(lhs->id->name != rhs->id->name)
        return false;

    std::vector<Package*> lhs_packages = ASTNodeHelper::getParentScopePackages(lhs);
    std::vector<Package*> rhs_packages = ASTNodeHelper::getParentScopePackages(rhs);
    if(lhs_packages.size() != rhs_packages.size())
        return false;

    for(std::vector<Package*>::size_type i = 0; i != lhs_packages.size(); ++i)
    {
        if(lhs_packages[i]->id->name != rhs_packages[i]->id->name)
            return false;
    }
    return true;
}

int NameManglingVisitor::findSubstitution(TemplatedIdentifier* component)
{
    ClassDecl* source_class = component->getOwner<ClassDecl>();
    if(source_class == nullptr) return -1;

    int index = 0;
    for(auto i = substitution_table.begin(); i != substitution_table.end(); ++i, ++index)
    {
        if(*i == nullptr) continue;
        if(TemplatedIdentifier* target_tid = cast<TemplatedIdentifier>(*i))
        {
            ClassDecl* substitution_class = target_tid->getOwner<ClassDecl>();
            if(substitution_class == nullptr) continue;

            if(InstantiatedFrom::get(source_class) == InstantiatedFrom::get(substitution_class))
            {
                return index;
            }
        }
    }
    return -1;
}

int NameManglingVisitor::findSubstitution(ClassDecl* component, bool fully_match)
{
    // Find ClassDecl first
    int index = 0;
    for(auto i = substitution_table.begin(); i != substitution_table.end(); ++i, ++index)
    {
        if(*i == nullptr) continue;
        if(ClassDecl* substitution_class = cast<ClassDecl>(*i))
        {
            if(component == substitution_class)
            {
                return index;
            }
        }
    }

    if (!fully_match)
    {
        // Find TemplatedIdentifier then
        if(TemplatedIdentifier* tid = component->name->getTemplateId())
        {
            return findSubstitution(tid);
        }
    }
    return -1;
}

int NameManglingVisitor::findSubstitution(ASTNode* component)
{
    // we have not merge our package, so we have to process package specially
    if(Package* component_package = cast<Package>(component))
    {
        int index = 0;
        for(auto i = substitution_table.begin(); i != substitution_table.end(); ++i, ++index)
        {
            if(*i == nullptr) continue;
            if(isEqualPackage(component_package, *i))
                return index;
        }
        return -1;
    }
    else if(TemplatedIdentifier* component_tid = cast<TemplatedIdentifier>(component))
    {
        return findSubstitution(component_tid);
    }
    else if(ClassDecl* component_class = cast<ClassDecl>(component))
    {
        return findSubstitution(component_class, false);
    }
    else
    {
        int index = 0;
        for(auto i = substitution_table.begin(); i != substitution_table.end(); ++i, ++index)
        {
            if(*i == component)
                return index;
        }
        return -1;
    }
}

int NameManglingVisitor::addSubstitution(ASTNode* component)
{
    // note that NULL component are used as dummy place-holder to represent the value type of a component when used with call-by-value semantics
    if(component != NULL)
    {
        if(ClassDecl* component_class = cast<ClassDecl>(component))
        {
            if(int index = findSubstitution(component_class, true) != -1)
            {
                return index;
            }
        }
        else 
        {
            if(int index = findSubstitution(component) != -1) 
            {
                return index;
            }
        }
    }

    substitution_table.push_back(component);

    return substitution_table.size() - 1;
}

std::string NameManglingVisitor::getSubstitution(const std::string& catalog, int index)
{
    if(index == 0) return catalog + "_";
    else
    {
        index -= 1; // ignore index == 0 as we have a shorthand for it
        std::string s;
        do
        {
            const char encoding[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            s = encoding[index % 36] + s;
            index /= 36;
        } while(index > 0);
        s = catalog + s + "_";
        return s;
    }
}


} } } }

