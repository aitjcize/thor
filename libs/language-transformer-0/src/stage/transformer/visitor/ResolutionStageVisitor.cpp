/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include <boost/assert.hpp>
#include <boost/functional/value_factory.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/next_prior.hpp>
#include <boost/optional.hpp>
#include <boost/range/algorithm/for_each.hpp>
#include <boost/regex.hpp>
#include <boost/variant.hpp>

#include "core/Prerequisite.h"

#include "language/logging/LoggerWrapper.h"
#include "language/logging/StringTable.h"
#include "language/resolver/Resolver.h"
#include "language/resolver/Specialization.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/PrimitiveKind.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/transformer/detail/DefaultValueHelper.h"
#include "language/stage/transformer/detail/MultiTypeHelper.h"
#include "language/stage/transformer/detail/ObjectReplicationHelper.h"
#include "language/stage/transformer/detail/OperatorOverloadingConfig.h"
#include "language/stage/transformer/detail/RestructureHelper.h"
#include "language/stage/transformer/detail/EnumValueResolver.h"
#include "language/stage/transformer/visitor/ResolutionStageVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

using namespace ::zillians::language::tree;

namespace
{

struct IsNullContext{};
struct FunctionDeclWithoutReturnTypeContext{};
struct FunctionReturnValueInferErrorContext{};

bool isNotNullClassType(FunctionDecl* func_decl)
{
    BOOST_ASSERT(func_decl != nullptr);
    BOOST_ASSERT(func_decl->type != nullptr);
    return func_decl->get<IsNullContext>() == nullptr && func_decl->type->getCanonicalType()->isRecordType();
}

bool isThisInNonStaticMemberFunction(ObjectLiteral& node)
{
    FunctionDecl* funcDecl = node.getOwner<FunctionDecl>();
    if(funcDecl == nullptr)
        return false;
    ClassDecl* classDecl = node.getOwner<ClassDecl>();
    if(classDecl == nullptr)
        return false;
    if(funcDecl->is_static)
        return false;
    return true;
}

bool needToResolvedFunction(FunctionDecl& node)
{
    if(isa<TemplatedIdentifier>(node.name) && !cast<TemplatedIdentifier>(node.name)->isFullySpecialized())
        return false;

    ClassDecl* classDecl = node.getOwner<ClassDecl>();
    if(classDecl == NULL)
        return true;

    if(isa<TemplatedIdentifier>(classDecl->name) && !cast<TemplatedIdentifier>(classDecl->name)->isFullySpecialized())
        return false;

    return true;
}

bool isReturnAndAllParametersTypeResolvedAndFullySpecialized(FunctionDecl& node)
{
    if(TemplatedIdentifier* tid = cast<TemplatedIdentifier>(node.name))
    {
        if(!tid->isFullySpecialized())
            return false;
    }

    if(ClassDecl* classDecl = node.getOwner<ClassDecl>())
    {
        if(TemplatedIdentifier* tid = cast<TemplatedIdentifier>(classDecl->name))
        {
            if(!tid->isFullySpecialized())
                return false;
        }
    }

    if(node.type == NULL)
        return false;

    if(node.type->getCanonicalType() == NULL)
        return false;

    for(auto* param : node.parameters)
    {
        if(param->getCanonicalType() == NULL)
            return false;
    }
    return true;
}

template<typename... ArgTypes>
FunctionType* addFunctionTypeToInternal(ArgTypes&&... args)
{
    auto*const internal = getParserContext().tangle->internal;

    return internal->addFunctionType(std::forward<ArgTypes>(args)...);
}

bool isArithmeticCapableType(Type* type)
{
    return type->isEnumType() || type->isArithmeticCapable();
}

boost::optional<SymbolTable*> getSymbolTable(ASTNode& depend)
{
    SymbolTable* symbol_table = NULL;

    if(Package* depend_package = ResolvedPackage::get(&depend))
    {
        return symbol_table = &depend_package->symbol_table;
    }
    else
    {
        Type* resolved_type = nullptr;

        if(ASTNode* resolved_symbol = ASTNodeHelper::getCanonicalSymbol(&depend))
        {
            resolved_type = ASTNodeHelper::getCanonicalType(resolved_symbol);
        }
        else
        {
            resolved_type = ASTNodeHelper::getCanonicalType(&depend);
        }

        if(resolved_type == NULL)
        {
            return boost::optional<SymbolTable*>();
        }
        else if(ClassDecl* cls_decl = resolved_type->getAsClassDecl())
        {
            symbol_table = &cls_decl->symbol_table;
        }
        else if(EnumDecl* enum_decl = resolved_type->getAsEnumDecl())
        {
            symbol_table = &enum_decl->symbol_table;
        }
        else
        {
            symbol_table = NULL;
        }
    }

    return symbol_table;
}

template<typename container_type>
unsigned push_object_replication_transforms(tree::ClassDecl& cls, container_type& transforms, std::set<ClassDecl*>& ready_set)
{
    using detail::object_replication::create_creator;
    using detail::object_replication::create_serializer;
    using detail::object_replication::create_deserializer;
    using detail::object_replication::is_really_replicable;
    using detail::object_replication::need_creator;
    using detail::object_replication::need_serializer;
    using detail::object_replication::need_deserializer;

    unsigned transform_count = 0;

    BOOST_ASSERT(ready_set.count(&cls) == 0 && "we not should enter here again if symbol table of class is ready");

    if (!cls.isReplicable())
    {
        ready_set.insert(&cls);

        return 0u;
    }

    static_assert(std::is_same<decltype(is_really_replicable(cls)), boost::tribool>::value, "we expect the result is a tri-state boolean");

    const boost::tribool is_cls_really_replicable = is_really_replicable(cls);

    if (boost::indeterminate(is_cls_really_replicable))
    {
        return 0u;
    }

    if (!is_cls_really_replicable)
    {
        BOOST_ASSERT(cls.name && "null pointer exception");

        LOG_MESSAGE(
            REPLICABLE_CLASS_NOT_REALLY_REPLICABLE, &cls,
            _class_name(cls.name->toString())
        );

        ready_set.insert(&cls);

        return 0u;
    }

    static_assert(std::is_same<decltype(need_creator   (cls)),   bool>::value, "we expect the result should be a decidable boolean value (not tri-state boolean)");
    static_assert(std::is_same<decltype(need_serializer(cls)),   bool>::value, "we expect the result should be a decidable boolean value (not tri-state boolean)");
    static_assert(std::is_same<decltype(need_deserializer(cls)), bool>::value, "we expect the result should be a decidable boolean value (not tri-state boolean)");

    const bool generate_creator      = need_creator   (cls);
    const bool generate_serializer   = need_serializer(cls);
    const bool generate_deserializer = need_deserializer(cls);

    const bool generate_any =
        generate_creator     ||
        generate_serializer  ||
        generate_deserializer
    ;

    if (!generate_any)
    {
        ready_set.insert(&cls);
        return 0u;
    }

    transforms.push_back(
        [generate_creator, generate_serializer, generate_deserializer, &cls, &ready_set]()
        {
            if (generate_creator   )   create_creator   (cls);
            if (generate_serializer)   create_serializer(cls);
            if (generate_deserializer) create_deserializer(cls);

            ready_set.insert(&cls);
        }
    );

    return 1u;
}

void verify_object_replication_transforms(detail::object_replication::verification_state& persistent_state, ClassDecl& cls)
{
    typedef detail::object_replication::verification_result::tag_state_non_changed      tag_state_non_changed;
    typedef detail::object_replication::verification_result::tag_variable_name_conflict tag_variable_name_conflict;

    struct transform_reporter : public boost::static_visitor<>
    {
        transform_reporter(ClassDecl& cls, const wchar_t* member_name, const wchar_t* proto_type)
            : cls(&cls)
            , member_name(member_name)
            , proto_type(proto_type)
        {
        }

        result_type operator()(const tag_state_non_changed&) const {}

        result_type operator()(const tag_variable_name_conflict&) const
        {
            LOG_MESSAGE(
                REPLICABLE_CLASS_WITH_CONFLICTED_MEMBER, cls,
                _class_name(cls->toString()),
                _member_name(member_name)
            );
        }

        result_type operator()(const FunctionDecl* func) const
        {
            LOG_MESSAGE(
                REPLICABLE_CLASS_WITH_MISMATCH_PROTOTYPE, cls,
                _class_name(cls->toString()),
                _member_name(member_name),
                _proto_type(proto_type)
            );
        }

        result_type operator()(const std::vector<FunctionDecl*>& multi_funcs) const
        {
            LOG_MESSAGE(
                REPLICABLE_CLASS_WITH_MULTI_MEMBER, cls,
                _class_name(cls->toString()),
                _member_name(member_name)
            );

            for (FunctionDecl* func: multi_funcs)
            {
                LOG_MESSAGE(
                    REPLICABLE_CLASS_WITH_MULTI_MEMBER_INFO, func
                );
            }
        }

    private:
        ClassDecl*     cls;
        const wchar_t* member_name;
        const wchar_t* proto_type;
    };

    // FIXME enhance error message
    const auto this_verification_result = detail::object_replication::continue_verification(persistent_state, cls);

    boost::apply_visitor(transform_reporter(cls, L"__create"   , L"static function():thor.lang.Object"                  ), this_verification_result.     creator_result);
    boost::apply_visitor(transform_reporter(cls, L"serialize"  , L"static function(ReplicationEncoder, <TheClass>):bool"), this_verification_result.  serializer_result);
    boost::apply_visitor(transform_reporter(cls, L"deserialize", L"static function(ReplicationDecoder, <TheClass>):bool"), this_verification_result.deserializer_result);
}

} // anonymous namespace

ResolutionStageVisitor::ResolutionStageVisitor(PersistantState& state)
    : symbol_table_chain()
    , class_content_ready_set(state.class_content_ready_set)
    , verification_state(state.verification_state)
    , call_parameters_map()
    , instantiater()
    , specialization_grouper(state.specialization_grouper)
    , resolved_count(0)
    , unresolved_count(0)
    , failed_nodes(state.failed_nodes)
{
    REGISTER_ALL_VISITABLE_ASTNODE(resolveInvoker);
}

void ResolutionStageVisitor::resolve(ASTNode& node)
{
    revisit(node);
}

void ResolutionStageVisitor::resolve(Annotations& node)
{
    return;
}

void ResolutionStageVisitor::resolve(NumericLiteral& node)
{
    revisit(node);

    if(!ResolvedType::get(&node))
    {
        ResolvedType::set(&node, Type::getBy(node.primitive_kind, node));
        markResolved(&node);
    }
}

void ResolutionStageVisitor::resolve(ObjectLiteral& node)
{
    revisit(node);

    if(ResolvedType::get(&node) != NULL)
        return;

    switch(node.type)
    {
    case ObjectLiteral::LiteralType::THIS_OBJECT:
    {
        if(!isThisInNonStaticMemberFunction(node))
        {
            LOG_MESSAGE(INVALID_PLACE_OF_USE_FOR_THIS, &node);
        }
        else
        {
            ClassDecl* classDecl = node.getOwner<ClassDecl>();
            BOOST_ASSERT_MSG(classDecl != nullptr, "classDecl should not be null, since \"isThisInNonStaticMemberFunction\" return true.");
            ResolvedType::set(&node, classDecl->getType());
            markResolved(&node);
        }
        break;
    }
    case ObjectLiteral::LiteralType::SUPER_OBJECT:
    {
        ClassDecl* classDecl = node.getOwner<ClassDecl>();
        if(classDecl == NULL)
        {
            LOG_MESSAGE(INVALID_PLACE_OF_USE_FOR_SUPER, &node);
        }
        else
        {
            TypeSpecifier* thisBase = classDecl->base;
            if(thisBase == NULL)
            {
                LOG_MESSAGE(SUPER_WITHOUT_BASE_CLASS, &node);
            }
            else
            {
                Type* thisBaseResolved = ResolvedType::get(thisBase);
                ResolvedType::set(&node, thisBaseResolved);
                markResolved(&node);
            }
        }
        break;
    }
    case ObjectLiteral::LiteralType::NULL_OBJECT:
    {
        // the null object will be bound to the Object type
        static ClassDecl* class_object = NULL;
        if(!class_object)
            class_object = node.getOwner<Tangle>()->findThorLangObject();

        if (class_object)
        {
            ResolvedType::set(&node, class_object->getType());
            markResolved(&node);
        }
        // NOTE there are some test cases that use --no-system function so we cannot find thor.lang.Object and leads to infinite loop
        // so this is a work-around for it, and we will create
//          else
//          {
//              markUnknown(&node);
//          }
        break;
    }
    default:
        UNREACHABLE_CODE();
        break;
    }
}

template<
    typename NodeType,
    typename std::enable_if<
        std::is_same<NodeType, TypeIdLiteral    >::value ||
        std::is_same<NodeType, SymbolIdLiteral  >::value ||
        std::is_same<NodeType, FunctionIdLiteral>::value
    >::type */* = nullptr*/
>
void ResolutionStageVisitor::resolve(NodeType& node)
{
    revisit(node);

    if (ResolvedType::get(&node) != nullptr)
        return;

    ResolvedType::set(&node, Type::getBy(PrimitiveKind::INT64_TYPE, node));
    markResolved(&node);
}

void ResolutionStageVisitor::resolve(TemplatedIdentifier& node)
{
    if(isa<ClassDecl>(node.parent) || isa<FunctionDecl>(node.parent))
    {
        // on declaration

        BOOST_FOREACH(TypenameDecl* typename_decl, node.templated_type_list)
        {
            insert_symbol(*typename_decl);
        }
    }

    revisit(node);
}

void ResolutionStageVisitor::resolve(Block& node)
{
    symbol_table_chain.enter_scope(node);

    // and then visit the sub-elements of this block, which could be a bunch of statements
    // note that there can be unresolved type specifier in those statements,
    // for example, for type resolution, DecarlativeStmt pointing to a VariableDecl, so the type of that VariableDecl should be properly resolved
    // another example, for symbol resolution, PrimaryExpr must be resolved to a symbol, which can be pointing to a VariableDecl
    revisit(node);

    symbol_table_chain.leave_scope(node);
}

void ResolutionStageVisitor::resolve(Source& node)
{
    BOOST_ASSERT(!symbol_table_chain.is_valid() && "chain of symbol table must be empty now!");
    BOOST_ASSERT(node.hasOwner<Package>() && "source is not under package!");

    auto& package_symbol_table = node.getOwner<Package>()->symbol_table;

    package_symbol_table.set_import_status(SymbolTable::ImportStatus::IMPORTED);
    symbol_table_chain.chain(package_symbol_table);
    symbol_table_chain.enter_scope(node);

    BOOST_FOREACH(Import* import, node.imports)
    {
        insert_symbol(*import);
    }

    BOOST_FOREACH(Declaration* decl, node.declares)
    {
        visit(*decl);
    }

    symbol_table_chain.leave_scope(node);
    symbol_table_chain.unchain(package_symbol_table);
    package_symbol_table.set_import_status(SymbolTable::ImportStatus::NOT_IMPORTED);

    BOOST_ASSERT(!symbol_table_chain.is_valid() && "chain of symbol table must be empty now!");
}

void ResolutionStageVisitor::resolve(TypeSpecifier& node)
{
    UNREACHABLE_CODE();
}

void ResolutionStageVisitor::resolve(FunctionSpecifier& node)
{
    revisit(node);

    if (ResolvedType::get(&node) != nullptr)
        return;

    auto*const function_type = addFunctionTypeToInternal(node);

    if (function_type == nullptr)
    {
        markUnknown(&node);
    }
    else
    {
        ResolvedType::set(&node, function_type);
        markResolved(&node);
    }
}

void ResolutionStageVisitor::resolve(MultiSpecifier& node)
{
    revisit(node);

    if (ResolvedType::get(&node) != nullptr)
        return;

    BOOST_ASSERT(hasParserContext() && "no parser context for compilation");
    BOOST_ASSERT(getParserContext().tangle && "missing tangle");

    Internal* internal = getParserContext().tangle->internal;
    BOOST_ASSERT(internal && "no internal node");

    MultiType* multi_type = internal->addMultiType(node);

    if (multi_type != nullptr)
    {
        ResolvedType::set(&node, multi_type);
        markResolved(&node);
    }
    else
    {
        markUnknown(&node);
    }
}

void ResolutionStageVisitor::resolve(NamedSpecifier& node)
{
    revisit(node);

    if (ResolvedType::get(&node) != nullptr)
        return;

    bool             need_resolution = true;
    Identifier*const name            = node.getName();

    BOOST_ASSERT(name && "null pointer exception");

    // if the the type specifier is specifying a template type, we have to visit the template identifier
    if(isa<TemplatedIdentifier>(name) && !cast<TemplatedIdentifier>(name)->isFullySpecialized())
    {
        need_resolution = false;
    }

    if(need_resolution)
    {
        boost::tribool no_error = true;

        if(NestedIdentifier* nested_id = cast<NestedIdentifier>(name))
        {
            Identifier* prev_id = NULL;

            for(auto i = nested_id->identifier_list.begin(), iend = boost::prior(nested_id->identifier_list.end()); i != iend && no_error; ++i)
            {
                Identifier* id = *i;

                no_error = resolveType(*id, *id, prev_id);

                prev_id = id;
            }

            if(no_error)
            {
                no_error = resolveType(node, *nested_id->identifier_list.back(), prev_id);
            }
        }
        else
        {
            no_error = resolveType(node, *name);
        }

        if(no_error)
        {
            if(!ResolvedType::get(&node))
            {
                markFailedType(&node);
            }
        }
    }
}

void ResolutionStageVisitor::resolve(PrimitiveSpecifier& node)
{
    revisit(node);

    if (ResolvedType::get(&node) != nullptr)
        return;

    const auto&      kind          = node.getKind();
          auto*const resolved_type = Type::getBy(kind, node);

    ResolvedType::set(&node, resolved_type);
    markResolved(&node);
}

void ResolutionStageVisitor::resolve(ClassDecl& node)
{
    BOOST_ASSERT(node.name != NULL && "class with no name!");
    BOOST_ASSERT(
        (
            !node.isTemplated() ||
            isa<TemplatedIdentifier>(node.name)
        ) && "non-template class has no TemplatedIdentifier!"
    );

    TemplatedIdentifier* t_id = cast<TemplatedIdentifier>(node.name);

    if(t_id != NULL)
    {
        symbol_table_chain.enter_scope(*t_id);
    }

    visit(*node.name);

    {
        if(node.base) visit(*node.base);
        for (TypeSpecifier* implement: node.implements) visit(*implement);

        if (!is_class_content_ready(node))
        {
            push_object_replication_transforms(node, transforms, class_content_ready_set);
        }

        prepare_symbol_table(node);

        SymbolTable cls_scope_symbol_table;
        cls_scope_symbol_table.set_clean();

        symbol_table_chain.enter_scope(node, cls_scope_symbol_table);
        for (FunctionDecl* member_function: node.member_functions) visit(*member_function);
        for (VariableDecl* member_variable: node.member_variables) visit(*member_variable);
        symbol_table_chain.leave_scope(node, cls_scope_symbol_table);

        if (is_class_content_ready(node))
        {
            verify_object_replication_transforms(verification_state, node);
        }
    }

    if(t_id != NULL)
    {
        symbol_table_chain.leave_scope(*t_id);
    }
}

void ResolutionStageVisitor::resolve(FunctionDecl& node)
{
    BOOST_ASSERT(node.name != NULL && "function with no name!");

    TemplatedIdentifier* t_id = cast<TemplatedIdentifier>(node.name);

    if(t_id != nullptr)
    {
        symbol_table_chain.enter_scope(*t_id);
    }

    visit(*node.name);

    symbol_table_chain.enter_scope(node);

    // try to resolve return type of the function
    if(node.type)
        visit(*node.type);

    // try to resolve parameters, which contains type specifier needed to be resolved
    for(auto* param : node.parameters)
        visit(*param);

    // visit all statements in the function block, which might contain resolvable type or symbol
    if(needToResolvedFunction(node))
    {
        if(node.block)
            visit(*node.block);
    }

    if(isReturnAndAllParametersTypeResolvedAndFullySpecialized(node))
    {
        if(ResolvedType::get(&node) == nullptr)
        {
            auto*const function_type = addFunctionTypeToInternal(node);

            if(function_type == nullptr)
            {
                markUnknown(&node);
            }
            else
            {
                ResolvedType::set(&node, function_type);
                markResolved(&node);
            }
        }
    }

    // leaving FunctionDecl scope
    symbol_table_chain.leave_scope(node);
    if(t_id != nullptr)
    {
        symbol_table_chain.leave_scope(*t_id);
    }
}

void ResolutionStageVisitor::tryResolveEnumValues(EnumDecl& node)
{
    if(EnumDeclManglingContext::get(&node) != nullptr)
        return;

    detail::EnumValueResolver finder;

    boost::for_each(
        node.values,
        [&node, &finder, this](VariableDecl* value)
        {
            if (isFailed(value))
                return;

            bool           is_updated = false;
            boost::tribool result     = false;

            std::tie(is_updated, result) = finder.resolve(value);

            if (is_updated)
                return;

            if (boost::indeterminate(result))
                markUnknown(value);
            else if (result)
                markResolved(value);
            else
                markFailed(value);
        }
    );

    if(finder.hasUnresolvedValue())
    {
        markUnknown(&node);
    }
    else
    {
        // if it's 64bit enum, we should update all enum id context
        EnumDeclManglingContext::set(
            &node,
            new EnumDeclManglingContext(
                finder.getUnderlyingType() == PrimitiveKind::INT64_TYPE
            )
        );
        markResolved(&node);
    }
}

void ResolutionStageVisitor::resolve(EnumDecl& node)
{
    symbol_table_chain.enter_scope(node);
    for(auto* value : node.values)
    {
        visit(*value);
    }
    symbol_table_chain.leave_scope(node);

    tryResolveEnumValues(node);
}

void ResolutionStageVisitor::resolve(VariableDecl& node)
{
    BOOST_ASSERT(
        (
            node.initializer == NULL ||
            isa<FunctionDecl>(node.parent) || // function parameter default value will not be restructured
            isa<EnumDecl>(node.parent) // enum member custom assigned value
        ) && "all initializer will be restructured to a stand alone assignment statement"
    );

    if (const auto*const exprs = default_value_helper::get_extra_processings(node))
        for(Expression*const expr : *exprs)
            visit(*expr);

    // the following variable declarations will not rely on current symbol table
    // - member variables
    // - global variables
    // - enumeration values
    if(!node.is_member && !node.isGlobal() && !isa<EnumDecl>(node.parent))
    {
        insert_symbol(node);
    }

    if(node.type) // note that VariableDecl within EnumDecl does not have type member
    {
        visit(*node.type);

        if (ResolvedType::get(&node) == nullptr)
        {
            if (auto*const var_decl_type = node.getCanonicalType())
            {
                ResolvedType::set(&node, var_decl_type);
                markResolved(&node);
            }
        }
    }

    if(node.initializer)
        visit(*node.initializer); // note that only VariableDecl within EnumDecl have type member (others will be restructed in R0 stage)
}

void ResolutionStageVisitor::resolve(BranchStmt& node)
{
    revisit(node);

    if (restructure_helper::restructureMultiType(node, transforms))
        return;

    // return type inference
    returnTypeInference(node);
}

void ResolutionStageVisitor::resolve(DeclarativeStmt& node)
{
    revisit(node);
}

void ResolutionStageVisitor::resolve(ExpressionStmt& node)
{
    revisit(node);
}

void ResolutionStageVisitor::resolve(ForeachStmt& node)
{
    // we have to enter scope for ForeachStmt because variable can be declared in the iterator
    symbol_table_chain.enter_scope(node);

    revisit(node);

    symbol_table_chain.leave_scope(node);
}

void ResolutionStageVisitor::resolve(UnaryExpr& node)
{
    if( isFailed( &node ) )
        return;

    revisit(node);

    if( OperatorOverloading::Unary::supports(node) )
    {
         replaceByMethodCall(node);
         return;
    }

    resolveExprImpl(node);
}

void ResolutionStageVisitor::resolve(BinaryExpr& node)
{
    if( isFailed(&node) )
        return;

    revisit(node);

    if( OperatorOverloading::Binary::supports(node) )
    {
        replaceByMethodCall(node);
        return;
    }

    if (restructure_helper::restructureMultiType(node, transforms))
        return;

    if (node.isAssignment())
        inferVariableType(node);

    resolveExprImpl(node);
}

void ResolutionStageVisitor::resolve(TernaryExpr& node)
{
    revisit(node);

    if(!ResolvedType::get(&node))
    {
        if(node.true_node->isNullLiteral() || node.false_node->isNullLiteral())
        {
            auto*const another_node = node.true_node->isNullLiteral() ? node.false_node : node.true_node;

            Type* another_type = another_node->getCanonicalType();
            if(another_type != nullptr)
            {
                PrimitiveType*const another_type_arithmetic = another_type->getArithmeticCompatibleType();
                if(another_type_arithmetic == nullptr)
                {
                    ResolvedType::set(&node, another_type);
                    markResolved(&node);
                }
                else
                {
                    markFailed(&node);
                }
            }
        }
        else
        {
            // get synthesized type from LHS and RHS
            Type*  true_type = node.true_node->getCanonicalType();
            Type* false_type = node.false_node->getCanonicalType();

            if(true_type && false_type)
            {
                if(true_type == false_type)
                {
                    ResolvedType::set(&node, true_type);
                    markResolved(&node);
                }
                else
                {
                    PrimitiveType*const  true_type_arithmetic =  true_type->getArithmeticCompatibleType();
                    PrimitiveType*const false_type_arithmetic = false_type->getArithmeticCompatibleType();

                    if(true_type_arithmetic && false_type_arithmetic)
                    {
                        const auto promoted_kind = true_type_arithmetic->promote(false_type_arithmetic);
                        ResolvedType::set(&node, Type::getBy(promoted_kind, node));
                        markResolved(&node);
                    }
                }
            }
        }
    }
}

// There are two kinds of flows:
// a) node.node is     a identifier,                   for example: a.b.c();
// b) node.node is not a identifier, but a expression, for example: g()(); where g() return a function(lambda)
void ResolutionStageVisitor::resolve(CallExpr& node)
{
    {
        put_call_parameters(node, *node.node, node.parameters);

        if(node.node)
        {
            visit(*node.node);
        }

        bool has_new_defaulted = false;

        for(Expression* parameter: node.parameters)
        {
            BOOST_ASSERT(parameter && "null pointer exception");

            const auto& defaulted_state = default_value_helper::is_defaulted(*parameter);

            if(defaulted_state == default_value_helper::NOT_DEFAULTED)
                visit(*parameter);

            if(defaulted_state == default_value_helper::NEW_DEFAULTED)
                has_new_defaulted = true;
        }

        if (has_new_defaulted)
            transforms.emplace_back([]{}); // dummy transform to report progress

        drop_call_parameters(node, *node.node);
    }

    typedef bool LogError;

    if(isFailed(node.node))
    {
        markFailedType(&node, LogError(false));
        return;
    }

    if(isUnknown(node.node))
    {
        markUnknown(&node);
        return;
    }

    if(!ResolvedType::get(&node))
    {
        //TypeSpecifier* return_type = NULL;
        //if(Type* resolved_type = ResolvedType::get(node.node))
        //{
        //    Type* canonical_resolved_type = node.node->getCanonicalType();

        //    BOOST_ASSERT(canonical_resolved_type != NULL && "null pointer exception");

        //    if(canonical_resolved_type->isRecordType()))
        //    {
        //        transforms.push_back([&]() {
        //            Expression* expression = node.node;

        //            MemberExpr* member_expr = new MemberExpr(expression, new SimpleIdentifier(L"invoke"));

        //            node.node = member_expr;
        //            member_expr->parent = &node;
        //        });
        //    }
        //    else
        //    {
        //        if(FunctionDecl* func_decl = resolved_type->getAsFunctionDecl())
        //        {
        //            BOOST_ASSERT(func_decl->parameters.size() >= node.parameters.size() && "parameter cound from caller side must not be larger then callee");

        //            if(func_decl->parameters.size() != node.parameters.size())
        //            {
        //                transforms.push_back([&, func_decl]()
        //                {
        //                    BOOST_ASSERT(func_decl && "null pointer exception");

        //                    default_value_state.append_values(*func_decl, node);
        //                });
        //            }

        //            return_type = func_decl->type;
        //        }
        //        else if(FunctionType* func_type = resolved_type->getAsFunctionType())
        //        {
        //            return_type = func_type->return_type;
        //        }
        //        else if(TypeSpecifier* specifier = cast<TypeSpecifier>(resolved_type))
        //        {
        //            BOOST_ASSERT(specifier->isFunctionType() && "non-function call should be filtered by resolver");

        //            return_type = specifier->referred.function_type->return_type;
        //        }
        //        else if(VariableDecl* var_decl = cast<VariableDecl>(resolved_type))
        //        {
        //            BOOST_ASSERT(var_decl->type->isFunctionType() && "non-function call should be filtered by resolver");

        //            return_type = var_decl->type->referred.function_type->return_type;
        //        }
        //    }
        //}

        Type* resolved_type = node.node->getCanonicalType();
        if(resolved_type == nullptr)
        {
            markUnknown(&node);
        }
        else
        {
            Type* return_type = nullptr;
            if(resolved_type->isRecordType())
            {
                transforms.push_back([&]() {
                    Expression* expression = node.node;
                    MemberExpr* member_expr = new MemberExpr(expression, new SimpleIdentifier(L"invoke"));
                    node.node = member_expr;
                    member_expr->parent = &node;
                });
            }
            else if(const auto*const function_type = resolved_type->getAsFunctionType())
            {
                return_type = resolved_type->getAsFunctionType()->return_type->getCanonicalType();

                if (function_type->parameter_types.size() != node.parameters.size())
                {
                    auto*const callee_node = tree::ASTNodeHelper::getCanonicalSymbol(node.node);
                    auto*const callee_decl = cast_or_null<tree::FunctionDecl>(callee_node);

                    BOOST_ASSERT(callee_decl != nullptr && "no function declaration for function default parameter");

                    transforms.push_back(
                        [this, callee_decl, &node]
                        {
                            BOOST_ASSERT(callee_decl != nullptr && "null pointer exception");

                            default_value_helper::append_values(*callee_decl, node);
                        }
                    );
                }
            }

            if(return_type != NULL)
            {
                ResolvedType::set(&node, return_type);
                markResolved(&node);
            }
            else
            {
                markUnknown(&node);
            }
        }
    }
}

static std::pair<boost::tribool, Type*> inferArrayLiteralElemType(ArrayExpr& node)
{
    // explicit specify array element type
    TypeSpecifier* specified_elem_type = node.element_type;
    Type* target_elem_type = (specified_elem_type == nullptr) ? nullptr : specified_elem_type->getCanonicalType();
    if(target_elem_type != nullptr)
    {
        for(size_t i = 0; i < node.elements.size(); ++i)
        {
            Expression* expr = node.elements[i];
            Type* elem_type = expr->getCanonicalType();
            if(elem_type == nullptr)
            {
                return { boost::indeterminate, nullptr };
            }
            Type::ConversionRank conv_rank = elem_type->getConversionRank(*target_elem_type);
            if(conv_rank == Type::ConversionRank::UnknownYet)
            {
                return { boost::indeterminate, nullptr };
            }
            else if(conv_rank == Type::ConversionRank::NotMatch)
            {
                LOG_MESSAGE(ARRAY_ELEM_TYPE_NOT_MATCH,
                            &node, _elem_index(std::to_wstring(i + 1)),
                            _unmatch_type_str(elem_type->toString()),
                            _expect_type_str(target_elem_type->toString()) );
                return { false, nullptr };
            }
        }
    }
    else
    {
        if(node.elements.empty())
        {
            LOG_MESSAGE(ARRAY_NEED_ELEM_OR_TYPE, &node);
            return { false, nullptr };
        }
        // implicit infer array element type
        target_elem_type = node.elements[0]->getCanonicalType();
        for(size_t i = 1; i < node.elements.size(); ++i)
        {
            Expression* expr = node.elements[i];
            Type* elem_type = expr->getCanonicalType();
            if(elem_type == nullptr)
            {
                return { boost::indeterminate, nullptr };
            }

            Type::ConversionRank conv_rank = elem_type->getConversionRank(*target_elem_type);
            // primitive type promote to large
            if(elem_type->isPrimitiveType() && conv_rank == Type::ConversionRank::StandardConversion)
            {
                target_elem_type = elem_type;
            }
            // class type shrink to base
            else if(!elem_type->isPrimitiveType() && conv_rank == Type::ConversionRank::StandardConversion)
            {
            }
            // unknown
            else if(conv_rank == Type::ConversionRank::UnknownYet)
            {
                return { boost::indeterminate, nullptr };
                break;
            }
            // not match
            else if(conv_rank == Type::ConversionRank::NotMatch)
            {
                LOG_MESSAGE(ARRAY_ELEM_TYPE_NOT_COMPATIBLE,
                            &node, _elem_index(std::to_wstring(i + 1)),
                            _uncompatible_type_str(elem_type->toString()),
                            _expect_type_str(target_elem_type->toString()) );
                return { false, nullptr };
                break;
            }
            // match
            else
            {
            }
        }
    }

    return { true, target_elem_type };
}

static std::pair<Type*, int> getTypeSpecifierForElement(Type* elem_type)
{
    // check is in package 'thor.lang'
    // WHY?? what is this code for =____= I write this, but I forget the intention
    std::vector<Package*> packages = ASTNodeHelper::getParentScopePackages(elem_type);
    if(packages.size() != 2 ||
       packages[0]->id->name != L"thor" ||
       packages[1]->id->name != L"lang")
    {
        return { elem_type, 0 };
    }

    // check is class 'Array...<T>'
    ClassDecl* arrayClassDecl = elem_type->getAsClassDecl();
    if(arrayClassDecl == nullptr)
    {
        return { elem_type, 0 };
    }
    std::wstring elem_type_str = arrayClassDecl->name->toString();
    boost::wregex re(LR"(Array((\d+)D)?<.+>)");
    boost::wsmatch match_result;

    if(!boost::regex_match(elem_type_str, match_result, re))
    {
        return { elem_type, 0 };
    }

    // get dimension
    BOOST_ASSERT(match_result.size() == 3);
    int orig_dimension = 0;
    if(match_result[2].str().empty())
        orig_dimension = 1;
    else
        orig_dimension = boost::lexical_cast<int>(match_result[2].str());

    // get element type specifier
    BOOST_ASSERT(arrayClassDecl != nullptr);
    TemplatedIdentifier* tid = cast<TemplatedIdentifier>(arrayClassDecl->name);
    BOOST_ASSERT(tid != nullptr);
    BOOST_ASSERT(tid->templated_type_list.size() == 1);
    BOOST_ASSERT(tid->templated_type_list[0]->specialized_type != nullptr);
    return { tid->templated_type_list[0]->specialized_type->getCanonicalType(), orig_dimension };
}

// if elem_type is not 'Array{N-D}<T>', create TypeSpecifier 'Array<T>'
// if elem type is     'Array{N-D}<T>', create TypeSpecifier 'Array{N+1-D}<T>'
//
// Two tasks:
// 1. get TypeSpecifier for base element, and get the origin dimension
// 2. Synthesis thor.lang.Array{N-D}
static std::pair<NamedSpecifier*, size_t> synthesisArrayTypeDim(Type* elem_type, ArrayExpr& source_info_node)
{
    auto type_dim = getTypeSpecifierForElement(elem_type);
    std::wstring ArrayND_str = L"Array";
    if(type_dim.second != 0)
        ArrayND_str += boost::lexical_cast<std::wstring>(type_dim.second + 1) + L"D";

    NestedIdentifier* result_array_id = new NestedIdentifier {
        new SimpleIdentifier(L"thor"),
        new SimpleIdentifier(L"lang"),
        new TemplatedIdentifier(new SimpleIdentifier(ArrayND_str), { ASTNodeHelper::createTypeSpecifierFrom(type_dim.first) })
    };

    NamedSpecifier* result_array_ts = new NamedSpecifier(result_array_id);
    ASTNodeHelper::foreachApply<ASTNode>(*result_array_ts, [&source_info_node](ASTNode& n) {
        ASTNodeHelper::propogateSourceInfo(n, source_info_node);
    });

    return { result_array_ts, type_dim.second } ;
}

void ResolutionStageVisitor::resolve(ArrayExpr& node)
{
    revisit(node);

    if(ResolvedType::get(&node) != nullptr)
        return;

    // There two phase while resolving a ArrayExpr
    //
    // 1. We have to determine the underlying element type,
    //    AND construct a TypeSpecifier 'thor.lang.Array<T>' saved in array_expr->array_type
    //
    // 2. Resolving array_expr->array_type.
    //
    // The two phase will be proceed in two seperated resolution iteration.
    //
    // NOTE:
    //
    // The data member ArrayExpr::array_type is only for resolution purpose,
    // since we can not trigger another resolution process to resolve the
    // synthesized TypeSpecifier Array<T> while we are resolving The resolution
    // is a iterated process, So we need some way to keep the synthesized
    // TypeSpecifier, and the TypeSpecifier need to be visited be visitor, so I
    // add this member function. if you find a better solution, Do not hesutate
    // to refactor it, just make sure the test pass.

    // if arra_expr->array_type is null, try to infer element type, and synthesis array_type
    ////////////////////////////////////////////////////////////////////////////
    // phase 1
    ////////////////////////////////////////////////////////////////////////////
    if(node.array_type == nullptr)
    {
        // try to infer type of element
        std::pair<boost::tribool, Type*> elem_type_infer_result = inferArrayLiteralElemType(node);
        if     (boost::indeterminate(elem_type_infer_result.first)) { markUnknown   (&node); return; }
        else if(!elem_type_infer_result.first                     ) { markFailedType(&node); return; }

        // synthesis TypeSpecifier of array
        auto arrayType_elemDim = synthesisArrayTypeDim(elem_type_infer_result.second, node);
        node.setArrayType(arrayType_elemDim.first);
        node.setDimension(arrayType_elemDim.second + 1); // array_dim is elem_dim + 1
        markUnknown(&node);
    }
    ////////////////////////////////////////////////////////////////////////////
    // phase 2
    ////////////////////////////////////////////////////////////////////////////
    else
    {
        Type* resolved_array_type = node.array_type->getCanonicalType();
        ResolutionStageVisitor::propogateType(node, *node.array_type);
        ResolvedType::set(&node, resolved_array_type);
        markResolved(&node);
    }
}

void ResolutionStageVisitor::resolve(MemberExpr& node)
{
    visit(*node.node);

    if(isUnknown(node.node))
    {
        markUnknown(&node);
    }
    else
    {
        visit(*node.member);

        resolveSymbol(node, *node.member, node.node);
    }
}

void ResolutionStageVisitor::resolve(IdExpr& node)
{
    revisit(node);

    resolveSymbol(node, *node.getId());
}

void ResolutionStageVisitor::resolve(LambdaExpr& node)
{
    revisit(node);

    if (!ResolvedType::get(&node))
        propogateType(node, *node.getLambda());
}

void ResolutionStageVisitor::resolve(BlockExpr& node)
{
    revisit(node);

    if (ResolvedType::get(&node) != nullptr)
        return;

    if (node.block->objects.empty())
    {
        ResolvedType::set(&node, Type::getBy(PrimitiveKind::VOID_TYPE, node));
        markResolved(&node);
    }
    else
    {
        const auto*const last_stmt = cast<ExpressionStmt>(node.block->objects.back());
        if (last_stmt != nullptr)
        {
            if (!node.getCanonicalType())
                propogateType(node, *last_stmt->expr);
        }
        else
        {
            ResolvedType::set(&node, Type::getBy(PrimitiveKind::VOID_TYPE, node));
            markResolved(&node);
        }
    }
}

void ResolutionStageVisitor::resolve(CastExpr& node)
{
    revisit(node);

    if(isFailed(node.node) || isFailed(node.type))
    {
        markFailedType(&node, false);
    }
    else if(isUnknown(node.node) || isUnknown(node.type))
    {
        markUnknown(&node);
    }
    else if(ResolvedType::get(&node) == nullptr)
    {
        if(Type* casted_type = node.type->getCanonicalType())
        {
            ResolvedType::set(&node, casted_type);
            markResolved(&node);
        }
    }
}

void ResolutionStageVisitor::resolve(IsaExpr& node)
{
    revisit(node);

    if (ResolvedType::get(&node) != nullptr)
        return;

    auto*const bool_type = Type::getBy(PrimitiveKind::BOOL_TYPE, node);

    ResolvedType::set(&node, bool_type);
    markResolved(&node);
}

void ResolutionStageVisitor::resolve(TieExpr& node)
{
    revisit(node);

    if (ResolvedType::get(&node) == nullptr)
    {
        Internal* internal = getParserContext().tangle->internal;

        BOOST_ASSERT(internal && "null pointer exception");

        MultiType* multi_type = internal->addMultiType(node);

        if (multi_type != nullptr)
        {
            ResolvedType::set(&node, multi_type);
            markResolved(&node);
        }
        else
        {
            markUnknown(&node);
        }
    }
}

void ResolutionStageVisitor::resolve(StringizeExpr& node)
{
    revisit(node);

    if(node.node->getCanonicalType() == nullptr)
    {
        markUnknown(&node);
    }
    else
    {
        transforms.push_back([&]() {
            Expression * sub_node = node.node;
            Expression * replace_target = nullptr;

            if(sub_node->getCanonicalType()->isPrimitiveType())
            {
                replace_target=sub_node;
            }
            else
            {
                SimpleIdentifier * member = new SimpleIdentifier(L"toString");
                ASTNodeHelper::propogateSourceInfo(*member, *sub_node);

                MemberExpr * function_name = new MemberExpr(sub_node, member);
                ASTNodeHelper::propogateSourceInfo(*function_name, *sub_node);

                replace_target = new CallExpr(function_name);
                ASTNodeHelper::propogateSourceInfo(*replace_target, *sub_node);
            }
            node.parent->replaceUseWith(node, *replace_target);
        });
    }
}

void ResolutionStageVisitor::resolve(SystemCallExpr& node)
{
    revisit(node);

    if(ResolvedType::get(&node) != nullptr)
        return;

    auto kind = PrimitiveKind::VOID_TYPE;

    switch (node.call_type)
    {
    case SystemCallExpr::CallType::ASYNC: kind = PrimitiveKind::INT32_TYPE; break;
    default                             : UNREACHABLE_CODE();               break;
    }

    auto*const tangle = getParserContext().tangle;

    NOT_NULL(tangle);

    auto*const internal = tangle->internal;

    NOT_NULL(internal);

    auto*const resolved_type = internal->getPrimitiveType(kind);
    ResolvedType::set(&node, resolved_type);
    markResolved(&node);
}

std::size_t ResolutionStageVisitor::getResolvedCount()
{
    return resolved_nodes.size();
}

std::size_t ResolutionStageVisitor::getFailedCount()
{
    return failed_nodes.size();
}

std::size_t ResolutionStageVisitor::getUnknownCount()
{
    return unknown_nodes.size();
}

bool ResolutionStageVisitor::isResolved(ASTNode* node)
{
    return resolved_nodes.count(node) != 0;
}

bool ResolutionStageVisitor::isFailed(ASTNode* node)
{
    return failed_nodes.count(node) != 0;
}

bool ResolutionStageVisitor::isUnknown(ASTNode* node)
{
    return unknown_nodes.count(node) != 0;
}

void ResolutionStageVisitor::reset()
{
    resolved_nodes.clear();
      //failed_nodes.clear();
     unknown_nodes.clear();
}

bool ResolutionStageVisitor::hasTransforms()
{
    return !transforms.empty() || instantiater.hasRequest();
}

void ResolutionStageVisitor::applyTransforms()
{
    for(auto& transform : transforms)
        transform();

    transforms.clear();

    instantiater.instantiate();
}

void ResolutionStageVisitor::markResolved(ASTNode* node)
{
    resolved_nodes.insert(node);
      failed_nodes.erase (node);
     unknown_nodes.erase (node);
}

void ResolutionStageVisitor::markFailed(ASTNode* node)
{
    if(isFailed(node))
        return;
    resolved_nodes.erase (node);
      failed_nodes.insert(node);
     unknown_nodes.erase (node);
}

void ResolutionStageVisitor::markFailedSymbol(ASTNode* node, Identifier* id, bool log_error/* = true*/)
{
    if(isFailed(node))
        return;
    resolved_nodes.erase (node);
      failed_nodes.insert(node);
     unknown_nodes.erase (node);
    if(log_error)
        LOG_MESSAGE(UNDEFINED_SYMBOL_INFO, node, _id(id->toString()));
}

void ResolutionStageVisitor::markFailedType(ASTNode* node, bool log_error/* = true*/)
{
    if(isFailed(node))
        return;
    resolved_nodes.erase (node);
      failed_nodes.insert(node);
     unknown_nodes.erase (node);
    if(log_error)
        LOG_MESSAGE(UNDEFINED_TYPE_INFO  , node, _id(ASTNodeHelper::getNodeName(node)));
}

void ResolutionStageVisitor::markFailedExpr(ASTNode* node, ASTNode* resolved)
{
    if(isFailed(node))
        return;
    resolved_nodes.erase (node);
    failed_nodes.insert(node);
    unknown_nodes.erase (node);

    LOG_MESSAGE(INVALID_EXPRESSION, node, _name(ASTNodeHelper::getNodeName(resolved)));
}

void ResolutionStageVisitor::markUnknown(ASTNode* node)
{
    resolved_nodes.erase (node);
      failed_nodes.erase (node);
     unknown_nodes.insert(node);
}

SymbolTable::FindResult ResolutionStageVisitor::findSymbol(Identifier& id, ASTNode* depend)
{
    const std::wstring simple_id = id.getSimpleId()->toString();

    if(depend != NULL)
    {
        const boost::optional<SymbolTable*> symbol_table_op = getSymbolTable(*depend);

        if(symbol_table_op.is_initialized())
        {
            if(SymbolTable* symbol_table = *symbol_table_op)
            {
                return symbol_table->find(simple_id);
            }
            else
            {
                return SymbolTable::FindResult(false);
            }
        }
        else
        {
            return SymbolTable::FindResult(boost::indeterminate);
        }
    }
    else
    {
        return symbol_table_chain.find(simple_id);
    }
}

boost::tribool ResolutionStageVisitor::resolveType(ASTNode& attach, Identifier& id, ASTNode* depend/* = NULL*/)
{
    using resolution::ChainedResolver;
    using resolution::PackageResolver;
    using resolution::TypeResolver;

    typedef ChainedResolver<PackageResolver, TypeResolver> Resolver;

    Resolver resolver(
        boost::value_factory<PackageResolver>(),
        [=]()
        {
            return TypeResolver(instantiater);
        }
    );

    auto on_success = [](ASTNode& attach, Identifier& id, ASTNode* depend) -> bool {
        UNUSED_ARGUMENT(attach);
        UNUSED_ARGUMENT(id);
        UNUSED_ARGUMENT(depend);

        return true;
    };

    auto on_failure = [this](ASTNode& attach, Identifier& id, ASTNode* depend) {
        markFailedType(&attach);
    };

    const auto is_resolved = resolveImpl(resolver, on_success, on_failure, attach, id, depend);

    return is_resolved;
}

boost::tribool ResolutionStageVisitor::resolveSymbol(ASTNode& attach, Identifier& id, ASTNode* depend/* = NULL*/)
{
    using resolution::ChainedResolver;
    using resolution::PackageResolver;
    using resolution::SymbolResolver;

    typedef ChainedResolver<PackageResolver, SymbolResolver> Resolver;

    const std::vector<Expression*>* parameters = get_call_parameters(*attach.parent, attach);

    Resolver resolver(
        boost::value_factory<PackageResolver>(),
        [=]()
        {
            return SymbolResolver(instantiater, parameters);
        }
    );

    auto on_success = [this, parameters](ASTNode& attach, Identifier& id, ASTNode* depend) -> bool {
        UNUSED_ARGUMENT(depend);

        if(parameters != nullptr)
        {
            return is_valid_symbol_for_call(attach, id);
        }
        else if(!isa<MemberExpr>(attach.parent))
        {
            return is_valid_symbol_for_top_expr(attach);
        }
        else
        {
            return true;
        }
    };

    auto on_failure = [this](ASTNode& attach, Identifier& id, ASTNode* depend) {
        markFailedSymbol(&attach, &id);
    };

    const auto is_resolved = resolveImpl(resolver, on_success, on_failure, attach, id, depend);

    return is_resolved;
}

template<typename Resolver, typename OnResolved, typename OnFailure>
boost::tribool ResolutionStageVisitor::resolveImpl(Resolver& resolver, OnResolved onResolved, OnFailure onFailure, ASTNode& attach, Identifier& id, ASTNode* depend)
{
    if(isFailed(&attach))
    {
        return false;
    }

    if(resolver.isResolved(attach))
    {
        return true;
    }

    boost::tribool is_resolved = false;
    const SymbolTable::FindResult symbols = findSymbol(id, depend);

    if(symbols.is_found)
    {
        is_resolved = resolver.resolve(attach, id, symbols.candidates);
    }
    else
    {
        is_resolved = symbols.is_found;
    }

    if(is_resolved)
    {
        const bool is_valid = onResolved(attach, id, depend);

        if(is_valid)
        {
            markResolved(&attach);
        }
        else
        {
            markFailedSymbol(&attach, &id, false);

            is_resolved = false;
        }
    }
    else if(!is_resolved)
    {
        onFailure(attach, id, depend);
    }
    else
    {
        BOOST_ASSERT(boost::indeterminate(is_resolved));

        markUnknown(&attach);
    }

    return is_resolved;
}

bool ResolutionStageVisitor::is_valid_symbol_for_call(ASTNode& node, Identifier& id)
{
    bool is_valid = true;

    if(Package* package = ResolvedPackage::get(&node))
    {
        // resolving function call but resolved to package (from PackageResolver)
        LOG_MESSAGE(CALL_NONFUNC, &node, _id(id.toString()));

        markFailedSymbol(&node, &id, false);

        is_valid = false;
    }

    return is_valid;
}

bool ResolutionStageVisitor::is_valid_symbol_for_top_expr(ASTNode& node)
{
    bool is_valid = true;

    if(Package* package = ResolvedPackage::get(&node))
    {
        markFailedExpr(&node, package);

        is_valid = false;
    }
    else if(ASTNode* symbol = ResolvedSymbol::get(&node))
    {
        // class/type name as expression
        if(isa<ClassDecl, TypenameDecl, TypedefDecl, EnumDecl>(symbol))
        {
            markFailedExpr(&node, symbol);

            is_valid = false;
        }
    }

    return is_valid;
}

void ResolutionStageVisitor::propogateType(ASTNode& to, ASTNode& from)
{
    Type* resolved_type_from = ASTNodeHelper::getCanonicalType(&from);
    Type* resolved_type_to   = ASTNodeHelper::getCanonicalType(&to);

    if(!resolved_type_to)
    {
        if(!resolved_type_from)
        {
            //markFailedType(&to);
        }
        else
        {
            ResolvedType::set(&to, resolved_type_from);
            markResolved(&to);
        }
    }
}

void ResolutionStageVisitor::propogateType(ASTNode& to, TypeSpecifier& from)
{
    Type* resolved_type_from = from.getCanonicalType();
    Type* resolved_type_to = ResolvedType::get(&to);

    if(!resolved_type_to)
    {
        if(!resolved_type_from)
        {
            //markFailedType(&to);
        }
        else
        {
            ResolvedType::set(&to, resolved_type_from);
            markResolved(&to);
        }
    }
}

void ResolutionStageVisitor::propogateSymbol(ASTNode& to, ASTNode& from)
{
    ASTNode* resolved_symbol_from = ResolvedSymbol::get(&from);
    ASTNode* resolved_symbol_to   = ResolvedSymbol::get(&to);

    if(!resolved_symbol_to)
    {
        if(!resolved_symbol_from)
        {
            //markFailedSymbol(&to);
        }
        else
        {
            ResolvedSymbol::set(&to, resolved_symbol_from);
            //markResolved(&to);
        }
    }
}

template<typename NodeType>
void ResolutionStageVisitor::resolveExprImpl(NodeType& node)
{
    if (ResolvedType::get(&node) != nullptr)
        return;

    boost::tribool is_success   = boost::indeterminate;
    bool           not_reported = false;
    Type*          new_type     = nullptr;

    std::tie(is_success, not_reported, new_type) = getExprType(node);

    static_assert(
        std::is_same<
            std::tuple<
                decltype(is_success  ),
                decltype(not_reported),
                decltype(new_type    )
            >,
            decltype(getExprType(node))
        >::value,
        "unexpected result type"
    );

    if (boost::indeterminate(is_success)) { markUnknown   (&node              ); return; }
    if (                    !is_success ) { markFailedType(&node, not_reported); return; }

    BOOST_ASSERT(is_success && "failure/unknown type should not be here");

    ResolvedType::set(&node, new_type);
    markResolved(&node);
}

std::tuple<boost::tribool, bool, Type*> ResolutionStageVisitor::getLogicalExprType(UnaryExpr& expr)
{
    auto*const boolean_type = Type::getBy(PrimitiveKind::BOOL_TYPE, expr);

    BOOST_ASSERT(boolean_type && "cannot get boolean type");

    return std::make_tuple(true, false, boolean_type);
}

std::tuple<boost::tribool, bool, Type*> ResolutionStageVisitor::getArithmeticOrBinaryExprType(UnaryExpr& expr)
{
    BOOST_ASSERT(expr.node && "null pointer exception");

    Type*const expr_node_type = expr.node->getCanonicalType();

    if (expr_node_type == nullptr)
        return std::make_tuple(boost::indeterminate, false, nullptr);

    PrimitiveType*const expr_node_type_arithmetic = expr_node_type->getArithmeticCompatibleType();

    if (expr_node_type_arithmetic == nullptr)
        return std::make_tuple(false, true, nullptr);

    if (expr.isBinary() && expr_node_type_arithmetic->isFloatType())
    {
        LOG_MESSAGE(INVALID_BIT_COMPLEMENT, &expr);

        return std::make_tuple(false, false, nullptr);
    }

    const auto& promoted_kind = expr_node_type_arithmetic->promote(expr_node_type_arithmetic);
    auto*const  promoted_type = Type::getBy(promoted_kind, expr);

    return std::make_tuple(true, false, promoted_type);
}

std::tuple<boost::tribool, bool, Type*> ResolutionStageVisitor::getAssignExprType(BinaryExpr& expr)
{
    BOOST_ASSERT(expr.left && "null pointer exception");

    if (isFailed(expr.left))
        return std::make_tuple(false, false, nullptr);

    Type*const lhs_type = expr.left->getCanonicalType();

    if (lhs_type == nullptr)
        return std::make_tuple(boost::indeterminate, false, nullptr);

    if (expr.opcode != BinaryExpr::OpCode::ASSIGN && lhs_type->isMultiType())
    {
        LOG_MESSAGE(INVALID_EXPRESSION_OF_MULTI_VALUE, &expr, _type(ASTNodeHelper::getNodeName(&expr)));

        return std::make_tuple(false, false, nullptr);
    }

    switch (expr.opcode)
    {
    case BinaryExpr::OpCode::LSHIFT_ASSIGN:
    case BinaryExpr::OpCode::RSHIFT_ASSIGN:
    case BinaryExpr::OpCode::   AND_ASSIGN:
    case BinaryExpr::OpCode::    OR_ASSIGN:
    case BinaryExpr::OpCode::   XOR_ASSIGN:
        {
            const Type*const rhs_type = expr.right->getCanonicalType();

            if (rhs_type == nullptr)
                return std::make_tuple(boost::indeterminate, false, nullptr);

            if (lhs_type->isFloatType() || rhs_type->isFloatType())
            {
                LOG_MESSAGE(
                    INVALID_BINARY_OP, &expr,
                    _lhs_type    (lhs_type->toString()),
                    _rhs_type    (rhs_type->toString()),
                    _operator_str(BinaryExpr::OpCode::toString(expr.opcode))
                );

                return std::make_tuple(false, false, nullptr);
            }
        }

        break;
    }

    propogateSymbol(expr, *expr.left);

    return std::make_tuple(true, false, lhs_type);
}

std::tuple<boost::tribool, bool, Type*> ResolutionStageVisitor::getArithmeticOrBianryExprType(BinaryExpr& expr)
{
    BOOST_ASSERT(expr.left  && "null pointer exception");
    BOOST_ASSERT(expr.right && "null pointer exception");

    if (isFailed(expr.left) || isFailed(expr.right))
        return std::make_tuple(false, false, nullptr);

    if (isUnknown(expr.left) || isUnknown(expr.right))
        return std::make_tuple(boost::indeterminate, false, nullptr);

    Type*const lhs_type = expr.left ->getCanonicalType();
    Type*const rhs_type = expr.right->getCanonicalType();

    if (lhs_type == nullptr || rhs_type == nullptr)
        return std::make_tuple(boost::indeterminate, false, nullptr);

    PrimitiveType*const lhs_type_arithmetic = lhs_type->getArithmeticCompatibleType();
    PrimitiveType*const rhs_type_arithmetic = rhs_type->getArithmeticCompatibleType();

    if (lhs_type_arithmetic == nullptr || rhs_type_arithmetic == nullptr)
    {
        LOG_MESSAGE(INVALID_ARITHMETIC, &expr);

        if (lhs_type_arithmetic == nullptr) LOG_MESSAGE(INVALID_ARITHMETIC_INFO, &expr, _id(ASTNodeHelper::getNodeName(expr.left )));
        if (rhs_type_arithmetic == nullptr) LOG_MESSAGE(INVALID_ARITHMETIC_INFO, &expr, _id(ASTNodeHelper::getNodeName(expr.right)));

        return std::make_tuple(false, false, nullptr);
    }

    if (lhs_type_arithmetic->isFloatType() || rhs_type_arithmetic->isFloatType())
    {
        if (expr.isBinary())
        {
            LOG_MESSAGE(
                INVALID_BINARY_OP, &expr,
                _lhs_type    (lhs_type_arithmetic->toString()),
                _rhs_type    (rhs_type_arithmetic->toString()),
                _operator_str(BinaryExpr::OpCode::toString(expr.opcode))
            );

            return std::make_tuple(false, false, nullptr);
        }
    }

    const auto& promoted_kind = lhs_type_arithmetic->promote(expr.isShift() ? lhs_type_arithmetic /* reuse LHS to trigger int32 promotion */ : rhs_type_arithmetic);
    auto*const  promoted_type = Type::getBy(promoted_kind, expr);

    return std::make_tuple(true, false, promoted_type);
}

std::tuple<boost::tribool, bool, Type*> ResolutionStageVisitor::getComparisonOrLogicalExprType(BinaryExpr& expr)
{
    BOOST_ASSERT(expr.left  && "null pointer exception");
    BOOST_ASSERT(expr.right && "null pointer exception");

    const auto is_lhs_valid_type = !expr.left ->isMultiValue();
    const auto is_rhs_valid_type = !expr.right->isMultiValue();

    static_assert(std::is_same<boost::tribool, std::remove_cv<decltype(is_lhs_valid_type)>::type>::value, "unexpected result type for condition");
    static_assert(std::is_same<boost::tribool, std::remove_cv<decltype(is_rhs_valid_type)>::type>::value, "unexpected result type for condition");

    if (static_cast<bool>(!is_lhs_valid_type) || static_cast<bool>(!is_rhs_valid_type))
    {
        LOG_MESSAGE(INVALID_EXPRESSION_OF_MULTI_VALUE, &expr, _type(expr.toSource()));

        return std::make_tuple(false, false, nullptr);
    }
    else if (boost::indeterminate(is_lhs_valid_type) || boost::indeterminate(is_rhs_valid_type))
    {
        return std::make_tuple(boost::indeterminate, false, nullptr);
    }

    auto*const boolean_type = Type::getBy(PrimitiveKind::BOOL_TYPE, expr);

    BOOST_ASSERT(boolean_type && "cannot get boolean type");

    return std::make_tuple(true, false, boolean_type);
}

std::tuple<boost::tribool, bool, Type*> ResolutionStageVisitor::getExprType(UnaryExpr& expr)
{
    const auto is_expr_valid = !expr.node->isMultiValue();

    if (!is_expr_valid)
    {
        LOG_MESSAGE(INVALID_EXPRESSION_OF_MULTI_VALUE, &expr, _type(ASTNodeHelper::getNodeName(&expr)));

        return std::make_tuple(false, true, nullptr);
    }
    else if (boost::indeterminate(is_expr_valid))
    {
        return std::make_tuple(boost::indeterminate, false, nullptr);
    }

    if (expr.isLogical())
        return getLogicalExprType(expr);
    else if (expr.isArithmetic() || expr.isBinary())
        return getArithmeticOrBinaryExprType(expr);

    Type*const           expr_node_type = expr.node->getCanonicalType();
    const boost::tribool is_success     = expr_node_type == nullptr ? static_cast<boost::tribool>(boost::indeterminate)
                                                                    : static_cast<boost::tribool>(true                );

    return std::make_tuple(
        is_success,
        false,
        expr_node_type
    );
}

std::tuple<boost::tribool, bool, Type*> ResolutionStageVisitor::getExprType(BinaryExpr& expr)
{
    if (expr.isAssignment())
        return getAssignExprType(expr);
    else if (expr.isArithmetic() || expr.isBinary())
        return getArithmeticOrBianryExprType(expr);
    else if (expr.isComparison() || expr.isLogical())
        return getComparisonOrLogicalExprType(expr);

    UNIMPLEMENTED_CODE();

    return std::make_tuple(false, true, nullptr);
}

boost::optional<VariableDecl*> ResolutionStageVisitor::canInferVariableType(BinaryExpr& node)
{
    IdExpr*       id_expr         = cast<IdExpr>(node.left);                    if(id_expr         == NULL) return boost::optional<VariableDecl*>();
    ASTNode*      resolved_symbol = ASTNodeHelper::getCanonicalSymbol(id_expr); if(resolved_symbol == NULL) return boost::optional<VariableDecl*>();
    VariableDecl* var_decl        = cast<VariableDecl>(resolved_symbol);        if(var_decl        == NULL) return boost::optional<VariableDecl*>();
    if(var_decl->type != NULL)                                                                              return boost::optional<VariableDecl*>();
    if(node.right->isNullLiteral())                                                                         return boost::optional<VariableDecl*>();

    return boost::optional<VariableDecl*>(var_decl);
}

void ResolutionStageVisitor::inferVariableType(BinaryExpr& node)
{
    if(boost::optional<VariableDecl*> opt_var_decl = canInferVariableType(node))
    {
        VariableDecl* var_decl = *opt_var_decl;
        Type* resolved_type = node.right->getCanonicalType();
        if(resolved_type != NULL)
        {
            if(resolved_type->isMultiType())
            {
                LOG_MESSAGE(INVALID_EXPRESSION_OF_MULTI_VALUE, &node, _type(resolved_type->toString()));
                markFailed(&node);

                return;
            }

            TypeSpecifier* type_spec = ASTNodeHelper::createTypeSpecifierFrom(resolved_type);
            var_decl->type = type_spec;
            type_spec->parent = var_decl;
            ASTNodeHelper::propogateSourceInfo(*type_spec, *var_decl);
            ResolvedType::set(var_decl->type, resolved_type);
            ResolvedType::set(var_decl      , resolved_type);
            markResolved(var_decl->type);
            markResolved(var_decl      );
        }
        else
        {
            markUnknown(&node);
        }
    }
}

// There are 3 different situation:
//
// 1. function don't need "return type inference"(RTI)
//
//      just return
//
// 2. function need RTI, AND the function's return type has not infered yet
//
//      just set
//
// 3. function need RTI, AND there is already an infered type
//
//      check the compability
//      3.1 if two type is the same, OK, do nothing
//      3.2 prev is not-null-class, new is null, do nothing
//      3.3 prev is null, new is not-null-class, set as that class
//      3.4 else, error
//
void ResolutionStageVisitor::returnTypeInference(BranchStmt& node)
{
    if (node.opcode != BranchStmt::OpCode::RETURN)
        return;

    FunctionDecl* func_decl = cast<FunctionDecl>(node.getAnyOwner<FunctionDecl, AsyncBlock>());

    if(func_decl == nullptr)
        return;

    if(func_decl->get<FunctionReturnValueInferErrorContext>() != nullptr)
        return;

    // every function first get here will be marked as
    //
    //     "Function Without Return type"
    //
    // The func_decl->type can *NOT* indicate whether a function need return
    // type infer, because the infered type will be set to func_decl->type
    // during resolution. Once the return type is infered, func_decl->will not
    // be null. But we still need to check
    if(func_decl->type == nullptr) {
        func_decl->init<FunctionDeclWithoutReturnTypeContext>();
    }

    // 1.
    if(func_decl->get<FunctionDeclWithoutReturnTypeContext>() == nullptr) {
        return;
    }

    TypeSpecifier* clone_result_type = NULL;
    if(node.result != NULL)
    {
        Type* result_type = node.result->getCanonicalType();
        if(result_type == NULL)
            return;

        if(!multi_type_helper::isValidReturn(*result_type))
        {
            LOG_MESSAGE(INVALID_EXPRESSION_OF_MULTI_VALUE, &node, _type(result_type->toString()));
            markFailed(&node);

            return;
        }

        clone_result_type = ASTNodeHelper::createTypeSpecifierFrom(result_type);
    }
    else
    {
        clone_result_type = new PrimitiveSpecifier(PrimitiveKind::VOID_TYPE);
    }

    ASTNodeHelper::propogateSourceInfo(*clone_result_type, *func_decl);

    // 2.
    if(func_decl->type == nullptr)
    {
        func_decl->setReturnType(clone_result_type);
        if(node.result->isNullLiteral()) {
            func_decl->init<IsNullContext>();
        }
    }
    // 3.
    else
    {
        // 3.1
        if(func_decl->type->getCanonicalType() == clone_result_type->getCanonicalType()) {
            return;
        }
        // 3.2
        else if(isNotNullClassType(func_decl) && node.result->isNullLiteral()) {
            return;
        }
        // 3.3
        else if(func_decl->get<IsNullContext>() != nullptr && (!node.result->isNullLiteral() && clone_result_type->getCanonicalType()->isRecordType())) {
            func_decl->setReturnType(clone_result_type);
            func_decl->reset<IsNullContext>();
        }
        // 3.4
        else {
            LOG_MESSAGE(FUNCTION_RETURN_TYPE_INFER_CONFLICT,
                        &node,
                        _new_type(clone_result_type->toString()),
                        _old_type(func_decl->type->toString()));
            func_decl->init<FunctionReturnValueInferErrorContext>();
        }
    }
}

template<typename NodeType>
void ResolutionStageVisitor::insert_symbol(NodeType& node)
{
    const SymbolTable::InsertResult result = symbol_table_chain.insert_symbol(node);

    if(result.is_shadow())
    {
        // TODO SymbolTableTODO log error for name shadowing
    }
    else if(result.is_dupe())
    {
        // TODO SymbolTableTODO log error for duplicated name
    }
    else
    {
        BOOST_ASSERT(result.is_ok());
    }
}

void ResolutionStageVisitor::prepare_symbol_table(ClassDecl& cls_decl)
{
    if(class_content_ready_set.count(&cls_decl) != 0u && !cls_decl.symbol_table.is_clean() && cls_decl.isCompleted())
    {
        auto is_base_ready = [](TypeSpecifier* ts) -> bool
        {
            if(ts != NULL)
            {
                if(Type* base_node = ts->getCanonicalType())
                {
                    ClassDecl* base_cls = base_node->getAsClassDecl();

                    BOOST_ASSERT(base_cls != NULL && "base class is not a ClassDecl!");

                    return base_cls->symbol_table.is_clean();
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        };

        if(is_base_ready(cls_decl.base))
        {
            bool all_ifaces_ready = true;

            BOOST_FOREACH(TypeSpecifier* iface, cls_decl.implements)
            {
                if(!is_base_ready(iface))
                {
                    all_ifaces_ready = false;

                    break;
                }
            }

            if(all_ifaces_ready)
            {
                prepare_symbol_table_impl(cls_decl);
            }
        }
    }
}

void ResolutionStageVisitor::prepare_symbol_table_impl(ClassDecl& cls_decl)
{
    BOOST_ASSERT(!cls_decl.symbol_table.is_clean() && "re-generating symbol table!");

    auto& symbol_table = cls_decl.symbol_table;

    {
        std::set<std::wstring> duplicated_names;

        auto inserter = [&symbol_table, &duplicated_names](Declaration& decl)
        {
            const SymbolTable::InsertResult insert_result = symbol_table.insert(&decl);

            BOOST_ASSERT(
                (
                    insert_result.is_ok() ||
                    insert_result.is_dupe()
                ) &&
                "there should be ok or duplicated name when constructing symbol table for class declaration"
            );

            if(insert_result.is_dupe())
            {
                BOOST_ASSERT(isa<TemplatedIdentifier>(decl.name) || isa<SimpleIdentifier>(decl.name));

                const std::wstring id = (isa<TemplatedIdentifier>(decl.name) ? cast<TemplatedIdentifier>(decl.name)->id : decl.name)->toString();

                if(duplicated_names.count(id) != 0)
                {
                    const auto prev_found = symbol_table.find(id);

                    BOOST_ASSERT(prev_found.candidates.size() == 1 && "there must be exactly one declaration so symbol insertion is failed at second insertion");

                    LOG_MESSAGE(DUPE_NAME, prev_found.candidates.front(), _id(id));
                }

                LOG_MESSAGE(DUPE_NAME, &decl, _id(id));
            }
        };

        BOOST_FOREACH(VariableDecl* member_variable, cls_decl.member_variables)
        {
            inserter(*member_variable);
        }

        BOOST_FOREACH(FunctionDecl* member_function, cls_decl.member_functions)
        {
            inserter(*member_function);

            specialization_grouper.add(*member_function);
        }
    }

    auto symbol_table_add_base = [&symbol_table](TypeSpecifier* ts)
    {
        if(ts != NULL)
        {
            Type* base_node = ts->getCanonicalType();

            BOOST_ASSERT(
                base_node &&
                base_node->getAsClassDecl() &&
                base_node->getAsClassDecl()->symbol_table.is_clean() &&
                "base class is not resolved or its symbol table is not clean!"
            );

            symbol_table.add_base(&base_node->getAsClassDecl()->symbol_table);
        }
    };

    symbol_table_add_base(cls_decl.base);
    BOOST_FOREACH(TypeSpecifier* iface, cls_decl.implements)
    {
        symbol_table_add_base(iface);
    }

    symbol_table.set_clean();
    transforms.emplace_back([]{}); // dummy transform to trigger next iteration
}

bool ResolutionStageVisitor::is_class_content_ready(ClassDecl& cls_decl) const
{
    return class_content_ready_set.count(&cls_decl) != 0;
}

void ResolutionStageVisitor::put_call_parameters(ASTNode& owner, ASTNode& self, const std::vector<Expression*>& parameters)
{
    BOOST_ASSERT(call_parameters_map.count(std::make_pair(&owner, &self)) == 0 && "store parameters into map more than once!");

    call_parameters_map.insert(std::make_pair(std::make_pair(&owner, &self), &parameters));
}

void ResolutionStageVisitor::drop_call_parameters(ASTNode& owner, ASTNode& self)
{
    BOOST_ASSERT(call_parameters_map.count(std::make_pair(&owner, &self)) != 0 && "dropping not stored parameters!");

    call_parameters_map.erase(std::make_pair(&owner, &self));
}

const std::vector<Expression*>* ResolutionStageVisitor::get_call_parameters(ASTNode& owner, ASTNode& self)
{
    const auto parameters_pos = call_parameters_map.find(std::make_pair(&owner, &self));

    if(parameters_pos == call_parameters_map.end())
    {
        return NULL;
    }
    else
    {
        BOOST_ASSERT(parameters_pos->second != NULL && "null pointer exception");

        return parameters_pos->second;
    }
}

void ResolutionStageVisitor::replaceByMethodCall(BinaryExpr& node)
{
    if( !OperatorOverloading::Binary::supports(node) )
        return;

    // replace operator by method call
    transforms.push_back([&]() {

        CallExpr* method_call = new CallExpr(new MemberExpr( node.left,
                                                             new SimpleIdentifier( OperatorOverloading::Binary::getReservedNameOf(node.opcode) )
                                                           )
                                            );

        method_call->appendParameter( node.right );

        ASTNodeHelper::foreachApply<ASTNode>( *method_call, [&](ASTNode& i){ ASTNodeHelper::propogateSourceInfo(i, node); });

        node.parent->replaceUseWith( node, *method_call );
    });
}

void ResolutionStageVisitor::replaceByMethodCall(UnaryExpr& node)
{
    if( !OperatorOverloading::Unary::supports(node) )
        return;

    // replace operator by method call
    transforms.push_back([&]() {

        CallExpr* method_call = new CallExpr(new MemberExpr( node.node,
                                                             new SimpleIdentifier( OperatorOverloading::Unary::getReservedNameOf(node.opcode) )
                                                           )
                                            );

        ASTNodeHelper::foreachApply<ASTNode>( *method_call, [&](ASTNode& i){ ASTNodeHelper::propogateSourceInfo(i, node); });

        node.parent->replaceUseWith( node, *method_call );
    });
}

} } } }
