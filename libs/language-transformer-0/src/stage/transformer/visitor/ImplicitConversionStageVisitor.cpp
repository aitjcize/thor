/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "utility/RangeUtil.h"

#include "language/context/TransformerContext.h"
#include "language/logging/LoggerWrapper.h"
#include "language/logging/StringTable.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Type.h"
#include "language/stage/transformer/detail/MultiTypeHelper.h"
#include "language/stage/transformer/visitor/ImplicitConversionStageVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

namespace {

template<typename ContainerType>
struct CastPolicyBase
{
    explicit CastPolicyBase(ContainerType &cast_exprs)
        : cast_exprs(cast_exprs)
    {
    }

    template<typename Member> void insertCast(Member member, Type& targetType);
    template<typename Member> void reportInvalidConversion(Member member, Type& targetType);
    template<typename Member> void reportUnexpectedReturn(Member member, Type& targetType);
    template<typename Member> void reportPrecisionLoss(Member member);

    ContainerType &cast_exprs;
};

template<typename ContainerType>
struct CastPolicy : public CastPolicyBase<ContainerType>
{
    explicit CastPolicy(ContainerType& cast_exprs)
        : CastPolicyBase<ContainerType>(cast_exprs)
    {
    }

    template<typename Member> void convertExactMatch        (Member member, Type& targetType) {}
    template<typename Member> void convertPromotion         (Member member, Type& targetType) { this->insertCast(member, targetType); }
    template<typename Member> void convertStandardConversion(Member member, Type& targetType);
    template<typename Member> void convertNotMatch          (Member member, Type& targetType);

private:
    template<typename Member> static bool isPrecisionLoss(Member member, const Type& targetType);

    static bool isPossibleOverflow(const Type& sourceType, const Type& targetType);
    static bool isLiteralOverflow(const NumericLiteral& literal, const Type& targetType);
};

template<typename ContainerType>
struct CastBoolPolicy : public CastPolicyBase<ContainerType>
{
    explicit CastBoolPolicy(ContainerType& cast_exprs)
        : CastPolicyBase<ContainerType>(cast_exprs)
    {
    }

    template<typename Member> void convertExactMatch        (Member member, Type& targetType) {}
    template<typename Member> void convertPromotion         (Member member, Type& targetType) { this->insertCast(member, targetType); }
    template<typename Member> void convertStandardConversion(Member member, Type& targetType) { this->insertCast(member, targetType); }
    template<typename Member> void convertNotMatch          (Member member, Type& targetType) { this->reportInvalidConversion(member, targetType); }
};

template<typename ContainerType>
template<typename Member>
void CastPolicyBase<ContainerType>::insertCast(Member member, Type& targetType)
{
    auto origin_parent = member->parent;

    CastExpr* castExpr = new CastExpr(member, ASTNodeHelper::createTypeSpecifierFrom(&targetType), CastExpr::CastMethod::STATIC);
    ASTNodeHelper::propogateSourceInfo(*castExpr, *member);
    origin_parent->replaceUseWith(*member, *castExpr, false);
    castExpr->parent = origin_parent;
    ResolvedType::set(castExpr, &targetType);

    cast_exprs.insert(castExpr);
}

template<typename ContainerType>
template<typename Member>
void CastPolicyBase<ContainerType>::reportInvalidConversion(Member member, Type& targetType)
{
    LOG_MESSAGE(
        INVALID_CONV,
        member,
        _rhs_type(member->getCanonicalType()->toString()),
        _lhs_type(targetType.getCanonicalType()->toString())
    );
}

template<typename ContainerType>
template<typename Member>
void CastPolicyBase<ContainerType>::reportUnexpectedReturn(Member member, Type& targetType)
{
    auto*const func = member->template getOwner<FunctionDecl>();
    auto*const stmt = member->template getOwner<BranchStmt  >();

    BOOST_ASSERT(func != nullptr && "not under any function");
    BOOST_ASSERT(stmt != nullptr && "not under any return statement");

    LOG_MESSAGE(
        UNEXPECTED_RETURN_VALUE,
        stmt,
        _function_name(func->name->toString()),
        _return_type  (func->type->toString())
    );
}

template<typename ContainerType>
template<typename Member>
void CastPolicyBase<ContainerType>::reportPrecisionLoss(Member member)
{
    LOG_MESSAGE(IMPLICIT_CAST_PRECISION_LOSS, member);
}

template<typename ContainerType>
template<typename Member>
void CastPolicy<ContainerType>::convertStandardConversion(Member member, Type& targetType)
{
    if (member->isNullLiteral())
    {
        // HACK for null literal, the resolved type is set directly to the desired type
        ResolvedType::set(member, &targetType);

        return;
    }

    // User defined class up-cast is not a warning
    if (!targetType.isDeclType())
    {
        if (isPrecisionLoss(member, targetType))
            this->reportPrecisionLoss(member);
    }

    this->insertCast(member, targetType);
}

template<typename ContainerType>
template<typename Member>
void CastPolicy<ContainerType>::convertNotMatch(Member member, Type& targetType)
{
    if(!isa<BranchStmt>(member->parent))
        this->reportInvalidConversion(member, targetType);
    else
        this->reportUnexpectedReturn(member, targetType);
}

template<typename ContainerType>
bool CastPolicy<ContainerType>::isPossibleOverflow(const Type& source_type, const Type& target_type)
{
    const auto*const source_type_primitive = source_type.getAsPrimitiveType();
    const auto*const target_type_primitive = target_type.getAsPrimitiveType();

    if (source_type_primitive == nullptr || target_type_primitive == nullptr)
        return false;

    return source_type_primitive->getKind() > target_type_primitive->getKind();
}

template<typename ContainerType>
bool CastPolicy<ContainerType>::isLiteralOverflow(const NumericLiteral& literal, const Type& target_type)
{
    const PrimitiveType* primitive_type = target_type.getAsPrimitiveType();

    if (primitive_type == nullptr)
        return false;

    const PrimitiveKind primitive_kind = primitive_type->getKind();

    if (primitive_kind >= literal.primitive_kind)
        return false;

    if (primitive_kind == PrimitiveKind::FLOAT32_TYPE)
    {
        const double literal_value = literal.get<double>();

        return literal_value < std::numeric_limits<float>::min() || std::numeric_limits<float>::max() < literal_value;
    }
    else if (primitive_kind.isIntegerType())
    {
        const int64 literal_value = literal.get<int64>();

        switch (primitive_kind)
        {
        case PrimitiveKind::BOOL_TYPE : /* do nothing here */ return false;
        case PrimitiveKind::INT8_TYPE : return literal_value < std::numeric_limits<int8 >::min() || std::numeric_limits<int8 >::max() < literal_value;
        case PrimitiveKind::INT16_TYPE: return literal_value < std::numeric_limits<int16>::min() || std::numeric_limits<int16>::max() < literal_value;
        case PrimitiveKind::INT32_TYPE: return literal_value < std::numeric_limits<int32>::min() || std::numeric_limits<int32>::max() < literal_value;
        default                       : UNREACHABLE_CODE(); return false;
        }
    }

    return false;
}

template<typename ContainerType>
template<typename Member>
bool CastPolicy<ContainerType>::isPrecisionLoss(Member member, const Type& target_type)
{
    const auto*const literal = member->getConstantNumericLiteral();

    if (literal != nullptr)
        return isLiteralOverflow(*literal, target_type);

    const Type*const source_type = member->getCanonicalType();

    BOOST_ASSERT(source_type && "canonical type should be ready before implicit conversion");

    return isPossibleOverflow(*source_type, target_type);
}

template<typename Policy, typename Member>
void castDispatcher(Policy& policy, Member member, Type& targetType)
{
    const auto rank = ASTNodeHelper::getConversionRank(*member, targetType);

    BOOST_ASSERT(rank != Type::ConversionRank::UnknownYet && "conversion rank is indeterminable due to incomplete resolution");

    switch (rank)
    {
    case Type::ConversionRank::ExactMatch        : policy.convertExactMatch        (member, targetType); break;
    case Type::ConversionRank::Promotion         : policy.convertPromotion         (member, targetType); break;
    case Type::ConversionRank::StandardConversion: policy.convertStandardConversion(member, targetType); break;
    case Type::ConversionRank::NotMatch          : policy.convertNotMatch          (member, targetType); break;
    default                                      : UNREACHABLE_CODE()                                  ; break;
    }
}

template<typename Member, typename ContainerType>
void castTo(Member member, Type& targetType, ContainerType& cast_exprs)
{
    CastPolicy<ContainerType> policy(cast_exprs);

    castDispatcher(policy, member, targetType);
}

template<typename Member, typename ContainerType>
void castToBool(Member member, ContainerType& cast_exprs)
{
    Internal* internal = getParserContext().tangle->internal;

    BOOST_ASSERT(internal && "null pointer exception");

    Type* bool_type = internal->getPrimitiveType(PrimitiveKind::BOOL_TYPE);

    BOOST_ASSERT(bool_type && "null pointer exception");

    CastBoolPolicy<ContainerType> policy(cast_exprs);

    castDispatcher(policy, member, *bool_type);
}

template<typename ContainerType>
bool castMultiImpl(TieExpr& member_tie, MultiType& target_type, ContainerType& cast_exprs)
{
    if (member_tie.tied_expressions.size() != target_type.types.size())
        return false;

    for (const auto& item : make_zip_range(member_tie.tied_expressions, target_type.types))
    {
        Expression* sub_member      = boost::get<0>(item);
        Type*       sub_target_type = boost::get<1>(item);

        BOOST_ASSERT(sub_member && "null pointer exception");
        BOOST_ASSERT(sub_target_type && "null pointer exception");

        castTo(sub_member, *sub_target_type, cast_exprs);
    }

    ResolvedType::set(&member_tie, &target_type);

    return true;
}

template<typename ContainerType>
bool castMultiImpl(BlockExpr& member_block, MultiType& target_type, ContainerType& cast_exprs)
{
    BOOST_ASSERT(member_block.tag == "multi-type-restructure" && "unexpected tag from block expression");

    TieExpr&   member_tie = multi_type_helper::findEndingTieExpr(member_block);
    const bool is_casted  = castMultiImpl(member_tie, target_type, cast_exprs);

    ResolvedType::set(&member_block, &target_type);

    return is_casted;
}

template<typename Member, typename ContainerType>
bool castMulti(Member member, Type& target_type, ContainerType& cast_exprs)
{
    MultiType* target_type_multi = cast<MultiType>(&target_type);

    if (target_type_multi == nullptr)
        return false;

    bool is_compatible = false;

         if (TieExpr*   member_tie   = cast<TieExpr  >(member)) is_compatible = castMultiImpl(*member_tie  , *target_type_multi, cast_exprs);
    else if (BlockExpr* member_block = cast<BlockExpr>(member)) is_compatible = castMultiImpl(*member_block, *target_type_multi, cast_exprs);
    else                                                        UNREACHABLE_CODE();

    if (!is_compatible)
    {
        LOG_MESSAGE(
            INVALID_CONV,
            member,
            _rhs_type(member->getCanonicalType()->toString()),
            _lhs_type(target_type.getCanonicalType()->toString())
        );
    }

    return true;
}

}

ImplicitConversionStageVisitor::ImplicitConversionStageVisitor()
{
    REGISTER_ALL_VISITABLE_ASTNODE(convertInvoker);
}

void ImplicitConversionStageVisitor::convert(ASTNode& node)
{
    revisit(node);
}

void ImplicitConversionStageVisitor::convert(ClassDecl& node)
{
    TemplatedIdentifier* tid = cast<TemplatedIdentifier>(node.name);
    if(tid != NULL && !tid->isFullySpecialized())
        return;
    revisit(node);
}

void ImplicitConversionStageVisitor::convert(FunctionDecl& node)
{
    TemplatedIdentifier* tid = cast<TemplatedIdentifier>(node.name);
    if(tid != NULL && !tid->isFullySpecialized())
        return;
    revisit(node);
}

void ImplicitConversionStageVisitor::convert(VariableDecl& node)
{
    if (node.isParameter() && node.initializer != nullptr)
    {
        Type*const type = node.getCanonicalType();

        castTo(node.initializer , *type, cast_expr_collection);
    }

    revisit(node);
}

void ImplicitConversionStageVisitor::convert(Annotations& node)
{
    return;
}

void ImplicitConversionStageVisitor::convert(UnaryExpr& node)
{
    if(node.opcode == UnaryExpr::OpCode::LOGICAL_NOT)
    {
        castToBool(node.node, cast_expr_collection);
    }
    revisit(node);
}

void ImplicitConversionStageVisitor::convert(BinaryExpr& node)
{
    if(node.isAssignment())
    {
        Type* lType = node.left->getCanonicalType();

        BOOST_ASSERT(lType != nullptr && "invalid canonical type during implicit conversion for assignment");

        castTo(node.right, *lType, cast_expr_collection);
    }
    else if(node.isLogical())
    {
        castToBool(node.left, cast_expr_collection);
        castToBool(node.right, cast_expr_collection);
    }
    else if(node.isArithmetic() || node.isBinary())
    {
        Type* type = node.getCanonicalType();

        castTo(node.left , *type, cast_expr_collection);
        castTo(node.right, *type, cast_expr_collection);
    }
    else if(node.isComparison())
    {
        Type* lType = node.left->getCanonicalType();
        Type* rType = node.right->getCanonicalType();

        // make sure both are numeric
        if(lType != rType)
        {
            if(lType->isPrimitiveType() && rType->isPrimitiveType())
            {
                PrimitiveType* lPrimitive = lType->getAsPrimitiveType();
                PrimitiveType* rPrimitive = rType->getAsPrimitiveType();
                PrimitiveKind t = lPrimitive->promote(rPrimitive);
                Type* mType = getParserContext().tangle->internal->getPrimitiveType(t);

                castTo(node.left , *mType, cast_expr_collection);
                castTo(node.right, *mType, cast_expr_collection);
            }
            else if(lType->isRecordType() && rType->isRecordType())
            {
                ClassDecl* lClass = lType->getAsClassDecl();
                ClassDecl* rClass = rType->getAsClassDecl();

                BOOST_ASSERT(lClass && rClass && "null pointer exception");

                if(ASTNodeHelper::isInheritedFrom(lClass, rClass))
                {
                    castTo(node.left, *rType, cast_expr_collection);
                }
                else
                {
                    castTo(node.right, *lType, cast_expr_collection);
                }
            }
            else
            {
                Tangle* tangle = node.getOwner<Tangle>();

                BOOST_ASSERT(tangle && "no tangle!");

                ASTNode* object_node = tangle->findThorLangObject();

                BOOST_ASSERT(object_node && "no object node");

                if(object_node == lType)
                {
                    if(rType->isRecordType())
                    {
                        castTo(node.left, *rType, cast_expr_collection);
                    }
                }
                else if(object_node == rType)
                {
                    if(lType->isRecordType())
                    {
                        castTo(node.right, *lType, cast_expr_collection);
                    }
                }
            }
        }
    }

    revisit(node);
}

void ImplicitConversionStageVisitor::convert(TernaryExpr& node)
{
    if(Type* resultType = node.getCanonicalType())
    {
        castTo(node.true_node , *resultType, cast_expr_collection);
        castTo(node.false_node, *resultType, cast_expr_collection);
    }

    castToBool(node.cond, cast_expr_collection);

    revisit(node);
}

void ImplicitConversionStageVisitor::convert(CallExpr& node)
{
    Type* resolvedType  = node.node->getCanonicalType();
    BOOST_ASSERT(resolvedType != nullptr);

    // Well, skip handling callable class, expect it to restruct to function later
    if(resolvedType->isRecordType()) return;

    BOOST_ASSERT(resolvedType->isFunctionType());

    // for each argument, apply promotion/standrad conversion
    for(size_t i=0; i != node.parameters.size(); ++i)
    {
        FunctionType* funcType                = resolvedType->getAsFunctionType();
        Expression*   argumentExpr            = node.parameters[i];
        Type*         resolved_parameter_type = funcType->parameter_types[i]->getCanonicalType();
        BOOST_ASSERT(resolved_parameter_type != nullptr);

        if(ASTNodeHelper::getConversionRank(*argumentExpr, *resolved_parameter_type) == Type::ConversionRank::ExactMatch)
            continue;

        castTo(argumentExpr, *resolved_parameter_type, cast_expr_collection);
    }

    revisit(node);
}

PrimitiveKind getPrimitiveKind(Type* t)
{
    BOOST_ASSERT_MSG(t->isEnumType() || t->isPrimitiveType(), "only enum of primitive support");
    if(EnumDecl* enumDecl = t->getAsEnumDecl())
    {
        return enumDecl->getUnderlyingType();
    }
    else
    {
        return t->getAsPrimitiveType()->getKind();
    }
}

void ImplicitConversionStageVisitor::convert(CastExpr& node)
{
    if(node.method == CastExpr::CastMethod::UNKNOWN)
    {
        auto cast_method = CastExpr::CastMethod::UNKNOWN;

        BOOST_ASSERT(node.node && "null pointer exception");
        BOOST_ASSERT(node.type && "null pointer exception");

        switch(ASTNodeHelper::getConversionRank(*node.node, *node.type, Type::ConversionPolicy::Explicitly))
        {
        case Type::ConversionRank::ExactMatch:
        case Type::ConversionRank::Promotion:
        case Type::ConversionRank::StandardConversion:
            if(node.node->getCanonicalType()->isEnumType() || node.type->getCanonicalType()->isEnumType())
            {
                bool precision_loss = false;
                PrimitiveKind from = getPrimitiveKind(node.node->getCanonicalType());
                PrimitiveKind to   = getPrimitiveKind(node.type->getCanonicalType());
                from.isImplicitConvertible(to, precision_loss);
                if(precision_loss)
                    LOG_MESSAGE(EXPLICIT_CAST_PRECISION_LOSS, node.type);
            }
            cast_method = CastExpr::CastMethod::STATIC;
            break;
        case Type::ConversionRank::NotMatch:
            {
                Type* source_type_node = node.node->getCanonicalType();
                Type* target_type_node = node.type->getCanonicalType();

                if( source_type_node &&
                    target_type_node &&
                    source_type_node->isRecordType() &&
                    target_type_node->isRecordType())
                {
                    cast_method = CastExpr::CastMethod::DYNAMIC;
                }
            }

            break;
        default:
            UNREACHABLE_CODE();
            break;
        }

        if(cast_method == CastExpr::CastMethod::UNKNOWN)
        {
            LOG_MESSAGE(INVALID_CONV, node.node,
                    _rhs_type(node.node->getCanonicalType()->toString()),
                    _lhs_type(node.type->getCanonicalType()->toString()));
        }
        else
        {
            node.method = cast_method;
        }
    }

    revisit(node);
}

void ImplicitConversionStageVisitor::convert(BranchStmt& node)
{
    if(node.opcode != BranchStmt::OpCode::RETURN)
        return;

    FunctionDecl* func = node.getOwner<FunctionDecl>();
    BOOST_ASSERT(func != NULL);
    BOOST_ASSERT(func->type != NULL);
    Type* resolved_return_type = func->type->getCanonicalType();

    BOOST_ASSERT(resolved_return_type && "null pointer exception");

    if(node.result == nullptr)
    {
        if(!resolved_return_type->isVoidType())
            LOG_MESSAGE(
                UNEXPECTED_RETURN_VALUE,
                &node,
                _function_name(func->name->toString()),
                _return_type  (func->type->toString())
            );

        return;
    }

    if(!castMulti(node.result, *resolved_return_type, cast_expr_collection))
        castTo(node.result, *resolved_return_type, cast_expr_collection);

    revisit(node);
}

void ImplicitConversionStageVisitor::convert(IfElseStmt& node)
{
    castToBool(node.if_branch->cond, cast_expr_collection);

    for(Selection* elseif_branch: node.elseif_branches)
        castToBool(elseif_branch->cond, cast_expr_collection);

    revisit(node);
}

void ImplicitConversionStageVisitor::convert(SwitchStmt& node)
{
    Type* resolved_type = node.node->getCanonicalType();
    if(!resolved_type->isPrimitiveType())
    {
        // invalid switch statement but we don't log here
        // check and throw error in semantic-verification-1
        return;
    }

    // type for each case expression should be the same as switch expression
    for (Selection* case_: node.cases)
    {
        auto* condition = case_->cond;
        BOOST_ASSERT(case_->cond != nullptr && "have no condition expressioncon");

        auto* condition_type = condition->getCanonicalType();
        BOOST_ASSERT(condition_type != nullptr && "no associated condition type");

        // leave floating point expressions to report error later
        if (!condition_type->isFloatType())
            castTo(case_->cond, *resolved_type, cast_expr_collection);
    }

    revisit(node);
}

void ImplicitConversionStageVisitor::convert(ForStmt& node)
{
    castToBool(node.cond, cast_expr_collection);

    revisit(node);
}

void ImplicitConversionStageVisitor::convert(WhileStmt& node)
{
    castToBool(node.cond, cast_expr_collection);

    revisit(node);
}

std::unordered_set<CastExpr*> ImplicitConversionStageVisitor::getCastExprCollection()
{
    return cast_expr_collection;
}

} } } }
