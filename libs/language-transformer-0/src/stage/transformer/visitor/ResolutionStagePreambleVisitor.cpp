/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/foreach.hpp>
#include "core/Prerequisite.h"
#include "language/resolver/Specialization.h"
#include "language/stage/transformer/visitor/ResolutionStagePreambleVisitor.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

using namespace ::zillians::language::tree;

ResolutionStagePreambleVisitor::ResolutionStagePreambleVisitor(resolution::specialization::Grouper& specialization_grouper)
    : specialization_grouper(specialization_grouper)
    , current_symbol_table(NULL)
{
    REGISTER_ALL_VISITABLE_ASTNODE(resolveInvoker);
}

void ResolutionStagePreambleVisitor::resolve(ASTNode& node)
{
    UNUSED_ARGUMENT(node);
}

void ResolutionStagePreambleVisitor::resolve(Source& node)
{
    BOOST_ASSERT(current_symbol_table != NULL && "no symbol table when entering Source!");

    revisit(node);
}

void ResolutionStagePreambleVisitor::resolve(Tangle& node)
{
    revisit(node);
}

void ResolutionStagePreambleVisitor::resolve(ClassDecl& node)
{
    resolve(static_cast<Declaration&>(node));

    specialization_grouper.add(node);
}

void ResolutionStagePreambleVisitor::resolve(EnumDecl& node)
{
    BOOST_ASSERT(current_symbol_table != NULL && "no symbol table for processing");
    BOOST_ASSERT(node.isGlobal() && "this visitor handles global declarations only, except enumerations");

    // pass handling (for insertion) to general one
    resolve(static_cast<Declaration&>(node));

    // Prepare symbol table
    if(!node.symbol_table.is_clean())
    {
        auto origin_symbol_table = current_symbol_table;
        current_symbol_table = &node.symbol_table;

        revisit(node);

        current_symbol_table->set_clean();
        current_symbol_table = origin_symbol_table;
    }
}

void ResolutionStagePreambleVisitor::resolve(FunctionDecl& node)
{
    resolve(static_cast<Declaration&>(node));

    if(node.isGlobal())
    {
        BOOST_ASSERT(isa<Source>(node.parent) && "global function but parent is not a source");

        specialization_grouper.add(node);
    }
}

void ResolutionStagePreambleVisitor::resolve(Package& node)
{
    if(!node.symbol_table.is_clean())
    {
        auto origin_symbol_table = current_symbol_table;
        current_symbol_table = &node.symbol_table;
        current_symbol_table->set_clean();

        BOOST_FOREACH(Source* source, node.sources)
        {
            visit(*source);
        }

        BOOST_FOREACH(Package* child, node.children)
        {
            visit(*child);

            current_symbol_table->insert(child);
        }

        current_symbol_table = origin_symbol_table;
    }
}

void ResolutionStagePreambleVisitor::resolve(Declaration& node)
{
    BOOST_ASSERT(current_symbol_table != NULL && "no symbol table for processing");
    BOOST_ASSERT(
        (
            node.isGlobal() ||
            (isa<VariableDecl>(&node) && isa<EnumDecl>(node.parent))
        ) && "this visitor handles global declarations only, except enumerations"
    );

    const SymbolTable::InsertResult insert_result = current_symbol_table->insert(&node);

    BOOST_ASSERT(
        (
            insert_result.is_ok() ||
            insert_result.is_dupe()
        ) &&
        "global name should come with success or duplicated name"
    );

    // TODO SymbolTableTODO handle name duplicated name
}

} } } }
