/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <tuple>

#include <boost/next_prior.hpp>

#include "core/Prerequisite.h"

#include "utility/StringUtil.h"
#include "utility/UnicodeUtil.h"

#include "language/context/TransformerContext.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/verifier/context/SemanticHackContext.h"
#include "language/stage/transformer/detail/AsyncHelper.h"
#include "language/stage/transformer/detail/RestructureHelper.h"
#include "language/stage/transformer/visitor/RestructureStageLambdaVisitor1.h"

using namespace zillians::language::tree;

namespace zillians { namespace language { namespace stage { namespace visitor {

namespace {

template <typename T>
T* cloneWithContext(T* from)
{
    typedef boost::mpl::vector<
        zillians::language::stage::SourceInfoContext,
        zillians::language::NameManglingContext,
        zillians::language::SemanticHackContext,
        zillians::language::ResolvedType,
        zillians::language::ResolvedSymbol
    > ContextToCloneTypeList;

    return ASTNodeHelper::clone<T, ContextToCloneTypeList>(from);
}

}

RestructureStageLambdaVisitor1::RestructureStageLambdaVisitor1(bool no_system_bundle) : no_system_bundle(no_system_bundle), transform_lambda(false)
{
    REGISTER_ALL_VISITABLE_ASTNODE(restructInvoker);
}

void RestructureStageLambdaVisitor1::restruct(ASTNode& node)
{
    revisit(node);
}

void RestructureStageLambdaVisitor1::restruct(tree::AsyncBlock& node)
{
    if (transform_lambda)
        return;

    bool has_async_transform = false;

    if (detail::get_async_flags(node) == detail::async_invalid_flags::OK)
    {
        auto*const target = node.getTarget();
        auto*const grid   = node.getGrid();
        auto*const block  = node.getBlock();

        if (target == nullptr && grid == nullptr && block == nullptr)
        {
            transforms.emplace_back(
                [&node]
                {
                    detail::build_local_async_call(node);
                }
            );

            has_async_transform = true;
        }
        else if (target != nullptr && grid == nullptr && block == nullptr)
        {
            transforms.emplace_back(
                [&node, target]()
                {
                    BOOST_ASSERT(target != nullptr && "null pointer exception");

                    detail::build_remote_async_call(node, *target);
                }
            );

            has_async_transform = true;
        }
    }

    if (has_async_transform)
    {
        ++status.async_restruct_count;

        transform_lambda = true;
        revisit(node);
        transform_lambda = false;
    }
    else
    {
        revisit(node);
    }
}

void RestructureStageLambdaVisitor1::restruct(ClassDecl& node)
{
    if (!node.isCompleted())
        return;

    revisit(node);
}

void RestructureStageLambdaVisitor1::restruct(FunctionDecl& node)
{
    if(isa<TemplatedIdentifier>(node.name) && !cast<TemplatedIdentifier>(node.name)->isFullySpecialized())
        return;

    revisit(node);
}

void RestructureStageLambdaVisitor1::restruct(BlockExpr& node)
{
    if (node.tag != "lambda_restruct_partial")
    {
        revisit(node);
        return;
    }

    // we only handle one level lambda at a time
    if (transform_lambda)
        return;

    // var x = {
    //             var t : Lambda<void, int32>;
    //             t = null;
    //             lambda(a: int32) : void {};
    //             t;
    //         }
    //
    // ==> x = {
    //             var t : Lambda<void, int32>;
    //             t = null;
    //             var tt;
    //             tt = new lambda_uuid(... captured ...); // will be a new block directly
    //             t = new Lambda<void, int32>(tt);        // will be a new block directly
    //             tt.set_captured(... captured ...)
    //             t;
    //         }

    BOOST_ASSERT(node.block && "null pointer exception");
    BOOST_ASSERT(
        (
            node.block->objects.size() == 3 ||
            node.block->objects.size() == 4
        ) && "we expect there are four statements in a partial restructed lambda block"
    );

    DeclarativeStmt* t_decl_stmt      = cast<DeclarativeStmt>(*             node.block->objects.cbegin()    );
    ExpressionStmt*  lambda_expr_stmt = cast< ExpressionStmt>(*boost::prior(node.block->objects.cend  (), 2));

    BOOST_ASSERT(
        t_decl_stmt      != nullptr &&
        lambda_expr_stmt != nullptr &&
        "we got mismatched form of partial restructed lambda block"
    );

    VariableDecl* t_decl      = cast<VariableDecl>(t_decl_stmt->declaration);
    LambdaExpr*   lambda_expr = cast<LambdaExpr>(lambda_expr_stmt->expr);

    BOOST_ASSERT(
        t_decl != nullptr &&
        "cannot find declaration of 't' in partial restructed lambda block"
    );

    BOOST_ASSERT(
        lambda_expr != nullptr &&
        lambda_expr->getLambda() &&
        "weired, you say you are in partial restructed lambda block but you have no lambda expression"
    );

    status.lambda_class_construct_count++;

    transform_lambda = true;
    revisit(node);
    transform_lambda = false;

    transforms.push_back([&, t_decl, lambda_expr, lambda_expr_stmt]() {
        // construct lambda class
        auto lambda_function = lambda_expr->getLambda();

        std::unordered_set<VariableDecl*> captured_list;
        ClassDecl*                        captured_class = nullptr;

        std::tie(captured_list, captured_class) = restructure_helper::getCaptureInfo(*lambda_function);
        auto* lambda_class = createLambdaClass(*lambda_function, captured_list, captured_class);

        // var tt;
        VariableDecl* tt_decl = NULL;
        {
            UUID uuid = UUID::random();
            std::wstring wstr_id = L"lambda_temp_" + s_to_ws(uuid.toString('_'));

            tt_decl = new VariableDecl(
                            new SimpleIdentifier(wstr_id),
                            NULL,
                            false, false, false,
                            Declaration::VisibilitySpecifier::DEFAULT);

            node.block->insertObjectBefore(lambda_expr_stmt, new DeclarativeStmt(tt_decl));
        }

        // tt = new _lambda_uuid_();
        {
            CallExpr* call_expr = new CallExpr(new IdExpr(cloneWithContext(lambda_class->name)));
            UnaryExpr* new_lambda_class = new UnaryExpr(UnaryExpr::OpCode::NEW, call_expr);
            BinaryExpr* assignment = new BinaryExpr(BinaryExpr::OpCode::ASSIGN, new IdExpr(tt_decl->name->clone()), new_lambda_class);
            node.block->insertObjectBefore(lambda_expr_stmt, new ExpressionStmt(assignment));

            // now, apply restruct directly one New operation
            restructure_helper::restructureUnaryNewDirect(*new_lambda_class);
        }

        // t = new Lambda<>( tt );
        {
            // Lambda<blah, blah...>
            auto nid = createSystemLambdaIdentifierFromFunction(*lambda_function);

            // Lambda<blah, blah...>(t)
            CallExpr* call_expr = new CallExpr(new IdExpr(nid));
            call_expr->appendParameter(new IdExpr(tt_decl->name->clone()));

            // new Lambda<blah, blah...>(t)
            UnaryExpr* new_lambda_class = new UnaryExpr(UnaryExpr::OpCode::NEW, call_expr);

            // t = new Lambda<blah, blah..>(t)
            BinaryExpr* assignment = new BinaryExpr(
                    BinaryExpr::OpCode::ASSIGN,
                    new IdExpr(t_decl->name->clone()),
                    new_lambda_class);
            node.block->insertObjectBefore(lambda_expr_stmt, new ExpressionStmt(assignment));

            // now, apply restruct directly one New operation
            restructure_helper::restructureUnaryNewDirect(*new_lambda_class);
        }

        // tt.set_captured(... captured ...)
        {
            MemberExpr* member_expr = new MemberExpr(new IdExpr(tt_decl->name->clone()), new SimpleIdentifier(L"set_captured"));
            CallExpr* call_expr = new CallExpr(member_expr);

            if (captured_class)
            {
                // the first parameter will be *this*
                call_expr->appendParameter(new ObjectLiteral(ObjectLiteral::LiteralType::THIS_OBJECT));
            }

            for (VariableDecl* captured: captured_list)
            {
                BOOST_ASSERT(captured && "null pointer exception");
                BOOST_ASSERT(captured->name && "null pointer exception");

                if (isRecursiveLambda(*lambda_function, *captured))
                    call_expr->appendParameter(new IdExpr(t_decl->name->clone()));
                else
                    call_expr->appendParameter(new IdExpr(captured->name->clone()));
            }

            node.block->insertObjectBefore(lambda_expr_stmt, new ExpressionStmt(call_expr), true);
        }


        ASTNodeHelper::foreachApply<ASTNode>(node, [&](ASTNode& n) {
            ASTNodeHelper::propogateSourceInfo(n, node);
        });

        // well, another new block to replace the original one
        node.tag = "lambda_restruct";
    });
}

bool RestructureStageLambdaVisitor1::hasTransforms()
{
    return (transforms.size() > 0) || (defer_clear.size() >0);
}

void RestructureStageLambdaVisitor1::applyTransforms()
{
    for(auto& transform : transforms)
    {
        transform();
    }
    transforms.clear();

    for(auto& func : defer_clear)
    {
        func();
    }
    defer_clear.clear();
}

void RestructureStageLambdaVisitor1::getStatus(unsigned& lambda_class_construct_count, unsigned& async_restruct_count)
{
    lambda_class_construct_count = status.lambda_class_construct_count;
    async_restruct_count         = status.async_restruct_count;
}

void RestructureStageLambdaVisitor1::deferClearResolution(ASTNode& node, const std::unordered_set<VariableDecl*>& captured_list)
{
    defer_clear.push_back([&, captured_list]() {
        // Reset resolved types and symbols of the subtree
        ASTNodeHelper::foreachApply<ASTNode>(node, [&](ASTNode& n) {
            auto* resolved_symbol = ResolvedSymbol::get(&n);

            if (resolved_symbol != nullptr && captured_list.count(cast<VariableDecl>(resolved_symbol)) != 0) ResolvedSymbol::set(&n, nullptr);
        });

        // Since the resolved types of the ancestors may change after we replace some child node,
        // we have to set the resolved types of all ancestors to NULL and expect ResolutionStage to give a right resolved
        for (ASTNode* p = node.parent; p != nullptr; p = p->parent)
        {
            ResolvedType  ::set(p, nullptr);
            ResolvedSymbol::set(p, nullptr);
        }
    });
}

void RestructureStageLambdaVisitor1::replaceUseWith(ASTNode& parent, ASTNode& replaced, ASTNode& replace_with, const std::unordered_set<VariableDecl*>& captured_list)
{
    parent.replaceUseWith(replaced, replace_with);
    deferClearResolution(replace_with, captured_list);
}

/*
 * The idea behind the scene is to use SplitReferenceContext to see if we captured the node that is the same as
 * the lambda variable declaration.
 *
 * That is,
 *          var f = lambda() : void { f(); };
 *
 *          the f in lambda body will be resolved to variable declaration f. If we know they are the same, it must
 *          be recursive lambda case. And we rely on the split reference contxt in binary expression (assign).
 */
bool RestructureStageLambdaVisitor1::isRecursiveLambda(FunctionDecl& lambda, VariableDecl& captured_node)
{
    auto block = lambda.getOwner<BlockExpr>();   // this should be lambda_restruct_partial
    BOOST_ASSERT(block->tag == "lambda_restruct_partial");

    auto binary_expr = block->getOwner<BinaryExpr>();
    if (binary_expr && binary_expr->opcode == BinaryExpr::OpCode::ASSIGN)
    {
        ASTNode* ref = SplitReferenceContext::get(binary_expr);
        if (ref == &captured_node)
        {
            return true;
        }
    }

    return false;
}

/***
 * By given a FunctionDecl, extract the return type and the input parameters, we will have a
 * corresponding Lambda<R, ...> templated identifier.
 */
Identifier* RestructureStageLambdaVisitor1::createSystemLambdaIdentifierFromFunction(FunctionDecl& node)
{
    Type* resolved_type = node.getCanonicalType();
    BOOST_ASSERT(resolved_type && resolved_type->isFunctionType() && "The function declaration does not resolve to a function type");

    FunctionType& function_type = *resolved_type->getAsFunctionType();

    std::wstring argument_count_string = StringUtil::utf8ToWstr( StringUtil::itoa(function_type.parameter_types.size(), 10) );
    auto tid = new TemplatedIdentifier(TemplatedIdentifier::Usage::ACTUAL_ARGUMENT, new SimpleIdentifier(L"Lambda" + argument_count_string ));

    FunctionDecl* function_decl = &node;
    tid->append(new TypenameDecl(new SimpleIdentifier(L"_"), cloneWithContext(function_decl->type)));
    for(auto* param_decl : function_decl->parameters)
    {
        tid->append(new TypenameDecl(new SimpleIdentifier(L"_"), cloneWithContext((param_decl->type))));
    }

    return ASTNodeHelper::createThorLangId(&node, tid);
}

ClassDecl* RestructureStageLambdaVisitor1::createLambdaClass(FunctionDecl& node, const std::unordered_set<VariableDecl*>& captured_list, ClassDecl* captured_class)
{
    BOOST_ASSERT(isa<SimpleIdentifier>(node.name) && "invalid function identifier");
    // prepare randomized name for lambda class
    auto* randomized_identifier = cloneWithContext(node.name);

    auto unique_name = UUID::random().toString('_');
    randomized_identifier->getSimpleId()->name = L"lambda_" + s_to_ws(unique_name);

    // for lambda function, we need to generate a lambda class
    ClassDecl* lambda_class = new ClassDecl(randomized_identifier, false);

    {
        const auto*const owner_function = node.getOwner<FunctionDecl>();
        BOOST_ASSERT(owner_function != nullptr && "lambda is not under any function!?");

        if (owner_function->may_conflict)
            lambda_class->may_conflict = true;
    }

    // the lambda class should inherit from thor.lang.Object
    lambda_class->setBase(new NamedSpecifier(ASTNodeHelper::createThorLangId(&node, new SimpleIdentifier(L"Object"))));

    /* insert constructor */
    // Implementation
    Block* set_captured_block = new NormalBlock();

    FunctionDecl* set_captured = new FunctionDecl(new SimpleIdentifier(L"set_captured"),
                                                  new PrimitiveSpecifier(PrimitiveKind::VOID_TYPE),
                                                  true, false, false, false, Declaration::VisibilitySpecifier::PUBLIC, set_captured_block);

    if (captured_class)
    {
        VariableDecl* member_variable = new VariableDecl(
                new SimpleIdentifier(L"__captured_this"),
                new NamedSpecifier(cloneWithContext(captured_class->name)),
                true, false, false,
                Declaration::VisibilitySpecifier::PUBLIC);
        lambda_class->addVariable(member_variable);

        std::wstring argument = L"__in_captured_this";
        set_captured->appendParameter(new SimpleIdentifier(argument), cloneWithContext(member_variable->type));
        set_captured_block->appendObject(
                new ExpressionStmt(
                    new BinaryExpr(BinaryExpr::OpCode::ASSIGN,
                        new IdExpr(cloneWithContext(member_variable->name)),  // left: member variable
                        new IdExpr(new SimpleIdentifier(argument))            // right: argument
                    )
                )
            );
    }

    auto add_member_variable = [this, lambda_class, set_captured, set_captured_block](VariableDecl* node) {
        // Create member variables according to the captured variable
        VariableDecl* member_variable = new VariableDecl(
                node->name->clone(),
                cloneWithContext(node->type),
                true, false, false, Declaration::VisibilitySpecifier::PUBLIC);

        lambda_class->addVariable(member_variable);

        // To prevent from shadowing rule (the names of arguments collide with the member variables') we slightly modify
        // the argument name
        std::wstring argument = L"__in_" + node->name->toString();
        set_captured->appendParameter(new SimpleIdentifier(argument), cloneWithContext(node->type));

        // We simply assign the member variable from the specific input parameter
        set_captured_block->appendObject(
                new ExpressionStmt(
                    new BinaryExpr(BinaryExpr::OpCode::ASSIGN,
                        new IdExpr(node->name->clone()),            // left: member variable
                        new IdExpr(new SimpleIdentifier(argument))  // right: argument
                    )
                )
            );
    };

    for (VariableDecl* captured: captured_list)
    {
        add_member_variable(captured);
    }

    /* insert invoke function */
    // if we have captured class, we need to modify original lambda function
    if (captured_class)
    {
        transformLambdaBody(node, captured_list, *captured_class, *node.block);
    }

    FunctionDecl* invoke_function = new FunctionDecl(
            new SimpleIdentifier(L"invoke"),
            node.type,
            true, false, false, false,
            Declaration::VisibilitySpecifier::PUBLIC,
            node.block
        );

    // append parameters for invoke function, coming from lambda function parameters
    for (VariableDecl* parameter: node.parameters)
    {
        BOOST_ASSERT(parameter && "null pointer exception");

        invoke_function->appendParameter(parameter);
    }

    // insert member functions and variables
    lambda_class->addFunction(invoke_function);
    lambda_class->addFunction(set_captured);

    // set source information for debug usage
    ASTNodeHelper::propogateSourceInfoRecursively(*lambda_class, node);
    // set architecture, from enclosed block or project default
    auto* parent_block = node.getOwner<Block>();
    BOOST_ASSERT(parent_block != nullptr && "should be enclosed in block");
    lambda_class->arch = parent_block->getActualArch();

    restructure_helper::tryGenerateSpecialMethods(*lambda_class);

    auto source = node.getOwner<Source>();
    source->addDeclare(lambda_class);

    // add to defer clear all resolution type and symbol
    deferClearResolution(*lambda_class, captured_list);

    return lambda_class;
}

void RestructureStageLambdaVisitor1::transformLambdaBody(FunctionDecl& node, const std::unordered_set<VariableDecl*>& captured_list, ClassDecl& captured_class, Block& lambda_body)
{
    const auto replacer_impl = [&](ASTNode& node, ASTNode* captured_this_expr, ASTNode* semantic_hack_node)
    {
        ASTNodeHelper::foreachApply<ASTNode>(
            *captured_this_expr,
            [&](ASTNode& n) {
                if (!SourceInfoContext::get(&n))
                    ASTNodeHelper::propogateSourceInfo(n, node);
            }
        );

        if (semantic_hack_node)
        {
            SemanticHackContext* semantic_hack = new SemanticHackContext();
            semantic_hack->skip_private_access_check = true;
            semantic_hack->skip_protected_access_check = true;
            SemanticHackContext::set(semantic_hack_node, semantic_hack);
        }

        replaceUseWith(*node.parent, node, *captured_this_expr, captured_list);
    };

    ASTNodeHelper::foreachApply<IdExpr>(
        lambda_body,
        [&](IdExpr& id_expr)
        {
            auto id_replacer_impl = [&]()
            {
                auto*const captured_this_expr = new MemberExpr(new IdExpr(new SimpleIdentifier(L"__captured_this")), cloneWithContext(id_expr.getId()));
                auto*const semantic_hack_node = captured_this_expr;

                replacer_impl(id_expr, captured_this_expr, semantic_hack_node);
            };

            // Note that we should not use primary_expr.getCanonicalType() since our target is VariableDecl
            ASTNode* resolved = ASTNodeHelper::getCanonicalSymbol(&id_expr);

            if (!resolved || ASTNodeHelper::hasAncestor(resolved, &id_expr)) return;

            if (VariableDecl* resolved_variable = cast<VariableDecl>(resolved))
            {
                if (resolved_variable->is_member && resolved_variable->getOwner<ClassDecl>() == &captured_class && !resolved_variable->is_static)
                {
                    id_replacer_impl();
                }
            }
            else if (FunctionDecl* resolved_function = cast<FunctionDecl>(resolved))
            {
                if (resolved_function->is_member && resolved_function->getOwner<ClassDecl>() == &captured_class && !resolved_function->is_static)
                {
                    id_replacer_impl();
                }
            }

        }
    );

    ASTNodeHelper::foreachApply<Literal>(
        lambda_body,
        [&replacer_impl](Literal& literal)
        {
            // Note that we should not use primary_expr.getCanonicalType() since our target is VariableDecl
            ObjectLiteral* object_literal = cast<ObjectLiteral>(&literal);

            if (object_literal && object_literal->isThisLiteral())
            {
                auto*const captured_this_expr = new IdExpr(new SimpleIdentifier(L"__captured_this"));
                auto*const semantic_hack_node = isa<MemberExpr>(literal.parent) ? literal.parent : nullptr;

                replacer_impl(literal, captured_this_expr, semantic_hack_node);
            }
        }
    );
}

} } } }
