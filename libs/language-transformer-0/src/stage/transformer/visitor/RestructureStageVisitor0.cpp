/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

#include "utility/StringUtil.h"
#include "utility/UnicodeUtil.h"

#include "language/Architecture.h"
#include "language/context/ManglingStageContext.h"
#include "language/context/TransformerContext.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/UserDefinedLiteral.h"

#include "language/stage/transformer/detail/RestructureHelper.h"
#include "language/stage/transformer/detail/RestructureLambda0.h"
#include "language/stage/transformer/visitor/RestructureStageVisitor0.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

namespace {

Expression* createIdOrMemberExprFromString(const std::wstring& s)
{
    BOOST_ASSERT_MSG(!s.empty(), "Place holder string should not be empty.");
    std::vector<std::wstring> ss;
    boost::split(ss, s, [](wchar_t c){ return c == L'.'; });
    BOOST_ASSERT_MSG(!ss.empty(), "Place holder string should not be empty.");

    for(auto& v : ss) {
        boost::trim(v);
    }

    return Expression::create(ss);
}

bool hasReturnNonEmptyExpr(FunctionDecl& node)
{
    bool result = false;

    ASTNodeHelper::foreachApply<BranchStmt>(
        node,
        [&result](BranchStmt& branch_stmt)
        {
            if(branch_stmt.opcode == BranchStmt::OpCode::RETURN)
                if(branch_stmt.result != NULL)
                    result = true;
        },
        [](const ASTNode& node)
        {
            return !isa<AsyncBlock>(node);
        }
    );

    return result;
}

void specifyVoidIfNoReturnExpr(FunctionDecl& node)
{
    if(node.type != NULL)
        return;

    if(!hasReturnNonEmptyExpr(node))
    {
        node.type = new PrimitiveSpecifier(PrimitiveKind::VOID_TYPE);
        node.type->parent = &node;
        return;
    }
}

}

RestructureStageVisitor0::RestructureStageVisitor0(bool no_system_bundle)
    : no_system_bundle(no_system_bundle)
    , restructure_lambda_helper(transforms)
{
    REGISTER_ALL_VISITABLE_ASTNODE(restructInvoker);
}

void RestructureStageVisitor0::restruct(ASTNode& node)
{
    revisit(node);
}

void RestructureStageVisitor0::restruct(Annotations& node)
{
    return;
}

void RestructureStageVisitor0::restruct(FunctionSpecifier& node)
{
    restructure_lambda_helper.restruct(node);

    revisit(node);
}

void RestructureStageVisitor0::restruct(LockBlock& node)
{
    revisit(node);

    std::set<Expression*> read_exprs;
    std::set<Expression*> write_exprs;

    // collect read
    ASTNodeHelper::foreachApply<IdExpr>(node, [&node, &read_exprs, this](IdExpr& e) -> void {
        read_exprs.insert(&e);
    });
    ASTNodeHelper::foreachApply<MemberExpr>(node, [&node, &read_exprs, this](MemberExpr& e) -> void {
        read_exprs.insert(&e);
    });

    // collect write
    ASTNodeHelper::foreachApply<BinaryExpr>(node, [&node, &read_exprs, &write_exprs, this](BinaryExpr& bi) -> void {
        if(!bi.isAssignment()) {
            return;
        }

        BOOST_ASSERT(isa<IdExpr>(bi.left) || isa<MemberExpr>(bi.left));
        write_exprs.insert(bi.left);
        if(bi.opcode == BinaryExpr::OpCode::ASSIGN)
        {
            read_exprs.erase(bi.left);
        }
    });

    Statement*   lock_stmt = nullptr;
    Statement* unlock_stmt = nullptr;

    // create call of "lock(r1, 1, r2, 1, ..., w1, 0);"
    {
        CallExpr* call_expr = new CallExpr(Expression::create({L"lock"}));

        ASTNodeHelper::propogateSourceInfo(*call_expr, node);

        for (Expression* e : read_exprs)
        {
            call_expr->appendParameter(ASTNodeHelper::clone(e));
            call_expr->appendParameter(new NumericLiteral(1));
        }

        for (Expression* e : write_exprs)
        {
            call_expr->appendParameter(ASTNodeHelper::clone(e));
            call_expr->appendParameter(new NumericLiteral(0));
        }

        lock_stmt = new ExpressionStmt(call_expr);

        ASTNodeHelper::propogateSourceInfo(*lock_stmt, node);
    }

    // create call of unlock();
    {
        CallExpr* call_expr = new CallExpr(Expression::create({L"unlock"}));

        ASTNodeHelper::propogateSourceInfo(*call_expr, node);

        unlock_stmt = new ExpressionStmt(call_expr);

        ASTNodeHelper::propogateSourceInfo(*unlock_stmt, node);
    }

    BOOST_ASSERT(lock_stmt && "null pointer exception");
    BOOST_ASSERT(unlock_stmt && "null pointer exception");

    Block* normal_block = new NormalBlock(node.arch);

    normal_block->appendObject(lock_stmt);
    normal_block->appendObjects(node.objects);
    normal_block->appendObject(unlock_stmt);

    node.parent->replaceUseWith(node, *normal_block);
}

void RestructureStageVisitor0::restruct(Source& node)
{
    if(!node.is_imported)
    {
        revisit(node);
    }
}

void RestructureStageVisitor0::restruct(ClassDecl& class_)
{
    ASTNodeHelper::propogateArchitecture(class_);

    // since we don't need to append default ctor nor dtor, we can just skip it
    if(!class_.is_interface)
    {
        // if there's no extended base then make it extends from base Object
        if(!no_system_bundle && !class_.base)
        {
            transforms.push_back([&class_](){
                class_.setBase(new NamedSpecifier(new SimpleIdentifier(L"Object")));
            });
        }

        restructure_helper::tryGenerateSpecialMethods(class_);
    }

    // Well, move the revisit(node) to the end of the function, because the visited node in ClassDecl may depend on the 
    // default constructors.
    revisit(class_);
}

void RestructureStageVisitor0::restruct(FunctionDecl& node)
{
    ASTNodeHelper::propogateArchitecture(node);

    revisit(node);

    // note that 'prepend this' will be done at code gen stage
    specifyVoidIfNoReturnExpr(node);
}

void RestructureStageVisitor0::restruct(EnumDecl& node)
{
    ASTNodeHelper::propogateArchitecture(node);
    revisit(node);
}

void RestructureStageVisitor0::restruct(TypedefDecl& node)
{
    ASTNodeHelper::propogateArchitecture(node);
    revisit(node);
}

void RestructureStageVisitor0::restruct(VariableDecl& node)
{
    if(!isa<FunctionDecl>(node.parent))
    {
        ASTNodeHelper::propogateArchitecture(node);
    }

    revisit(node);

    if(node.initializer)
    {
        restructure_helper::splitVariableInitializer(node, transforms);
    }
}

void RestructureStageVisitor0::restruct(CallExpr& node)
{
    revisit(node);

    if (ObjectLiteral* literal = cast<ObjectLiteral>(node.node))
    {
        if (literal->type == ObjectLiteral::LiteralType::SUPER_OBJECT)
        {
            /*
               super() call, which calls parent constructor, will be

                         call_expr
                        /        \
                       ..     object_literal
                                   |
                                (super)

                 transformed to:

                         call_expr
                        /        \
                       ..      member_expr
                               /         \
                    simple_identifier     object_literal
                            |                  |
                          (new)             (super)
             */
            transforms.push_back([&node, literal](){
                ObjectLiteral* new_literal = literal->clone();
                SimpleIdentifier* new_identifier = new SimpleIdentifier(L"new");
                MemberExpr* new_member_expr = new MemberExpr(new_literal, new_identifier);

                restructure_helper::setSplitReference(node, new_member_expr);
                node.replaceUseWith(*node.node, *new_member_expr, true);

                ASTNodeHelper::propogateSourceInfo(*new_member_expr, node); // propagate the source info
            });
        }
    }
}

void RestructureStageVisitor0::restruct(IndexExpr& node)
{
    revisit(node);

    /*
        transform all array subscript into get or set function call (depending on the enclosing expression



        for the reset of the case, when looking at a[b]

        will be transformed into a.get();

        for a more advanced example: a[b[c[d]]] = b[c[d[e]]];

        will be transformed into: a.set(b.get(c.get(d)), b.get(c.get(d.get(e))));

        for a more advanced example: a[b[c[d]]++] = b[c[d[e]]];

        will be transformed into: { var t = b.get(c.get(d.get(e)))); a.set( { var t = b.get(c.get(d)); b.set(c.get(d), t+1); t; }, t); t; }

        for another example: f(a[b]);

        will be transformed into: f(a.get(b));
     */
    bool transformed = false;

    if(BinaryExpr* parent_binary_expr = cast<BinaryExpr>(node.parent))
    {
        if(parent_binary_expr->isAssignment() && parent_binary_expr->left == &node)
        {
            /*
                for example: a[b] = c;

                       =
                      / \
                     []  c
                    /  \
                   a    b

                will be transformed into: { var t = c; a.set(b, t); t; };

                                 ---------------------------+---------block_expr---------+--------------------------------------
                                /                           |                            |                                      \
                           [0] /                        [1] |                        [2] |                                   [3] \
                              /                             |                            |                                        \
                          decl_stmt                     expr_stmt                    expr_stmt                                 expr_stmt
                              |                             |                            |                                         |
                        variable_decl                 binary_expr(=)                 call_expr                                primary_expr
                        /           \                /              \               /         +-------------+                      |
                       id     type_specifier  primary_expr           c        member_expr      \             \                    id
                       |             |             |                         /           \      \ param[0]    \ param[1]           |
                      't'          NULL           id                        a            id      \             \                  't'
                           (infer by resolution)   |                                      |       b         primary_expr
                                                  't'                                   'set'                   |
                                                                                                                id
                                                                                                                |
                                                                                                               't'

                for a more complicated example: a[b] = a[c] = a[d];

                           =
                          / \
                         /   \
                        /     \
                       []      =
                      /  \    / \
                     a    d  /   \
                            /     \
                           []     []
                          /  \   /  \
                         a    b a    c

                will be transformed into: { var tt; tt = { var t; t = a.get(d); a.set(c, t); t; }; a.set(b, tt); tt };

             */

            transforms.push_back([&]() {

                UUID uuid = UUID::random();
                std::wstring wstr_id = L"new_temp_" + s_to_ws(uuid.toString('_'));

                BinaryExpr* anchor = cast<BinaryExpr>(node.parent);
                BlockExpr* block_expr = new BlockExpr(new NormalBlock(), "subscript_set_restruct");

                // var t = c;
                VariableDecl* t_decl = NULL;
                {
                    // prepare variable t
                    t_decl = new VariableDecl(
                            new SimpleIdentifier(wstr_id + L"-t"),
                            NULL,
                            false, false, false,
                            Declaration::VisibilitySpecifier::DEFAULT);

                    t_decl->setInitializer(anchor->right);

                    block_expr->block->appendObject(new DeclarativeStmt(t_decl));
                }
                Type* t_decl_type = t_decl->getCanonicalType();

                // a.set(b, t);
                {
                    CallExpr* call_expr_set = new CallExpr(new MemberExpr(node.array, new SimpleIdentifier(L"set")));
                    call_expr_set->appendParameter(node.index);

                    // prepare t
                    IdExpr* t_use = new IdExpr(new SimpleIdentifier(wstr_id + L"-t"));
                    ResolvedSymbol::set(t_use, t_decl);
                    ResolvedType::set(t_use, t_decl_type);
                    call_expr_set->appendParameter(t_use);

                    block_expr->block->appendObject(new ExpressionStmt(call_expr_set));
                }

                // t;
                {
                    // prepare t
                    IdExpr* t_use = new IdExpr(new SimpleIdentifier(wstr_id + L"-t"));
                    ResolvedSymbol::set(t_use, t_decl);
                    ResolvedType::set(t_use, t_decl_type);

                    block_expr->block->appendObject(new ExpressionStmt(t_use));
                }

                ASTNodeHelper::foreachApply<ASTNode>(*block_expr, [&](ASTNode& i){ ASTNodeHelper::propogateSourceInfo(i, node); });

                // var t = c; => var t; t = c;
                {
                    restructure_helper::splitVariableInitializer(*t_decl, transforms, false);
                }

                anchor->parent->replaceUseWith(*anchor, *block_expr);
            });
            transformed = true;
        }
    }

    if(!transformed)
    {
        if(UnaryExpr* parent_unary_expr = cast<UnaryExpr>(node.parent))
        {
            if( parent_unary_expr->opcode == UnaryExpr::OpCode::DECREMENT  ||
                parent_unary_expr->opcode == UnaryExpr::OpCode::INCREMENT )
            {
                /*
                    for another example: a[b]++;

                    unary_expr(++)
                         |
                         []
                        /  \
                       a    b

                    will be transformed into: { var $a = node.left("a"); var $b = node.right("b"); var $t = $a.get($b); $a.set($b, $t+1); $t; };

                 */
                transforms.push_back([&]() {

                    UUID uuid = UUID::random();
                    std::wstring wstr_id = L"new_temp_" + s_to_ws(uuid.toString('_'));

                    UnaryExpr* anchor = cast<UnaryExpr>(node.parent);
                    BlockExpr* block_expr = new BlockExpr(new NormalBlock(), "subscript_with_incr_or_decr_restruct");

                    // var a = node.left;
                    VariableDecl* a_decl = NULL;
                    {
                        // prepare variable t
                        a_decl = new VariableDecl(
                                new SimpleIdentifier(wstr_id + L"-a"),
                                NULL,
                                false, false, false,
                                Declaration::VisibilitySpecifier::DEFAULT);

                        a_decl->setInitializer(node.array);

                        block_expr->block->appendObject(new DeclarativeStmt(a_decl));
                    }
                    Type* a_decl_type = a_decl->getCanonicalType();

                    // var b = node.right;
                    VariableDecl* b_decl = NULL;
                    {
                        // prepare variable t
                        b_decl = new VariableDecl(
                                new SimpleIdentifier(wstr_id + L"-b"),
                                NULL,
                                false, false, false,
                                Declaration::VisibilitySpecifier::DEFAULT);

                        b_decl->setInitializer(node.index);

                        block_expr->block->appendObject(new DeclarativeStmt(b_decl));
                    }
                    Type* b_decl_type = b_decl->getCanonicalType();

                    // var t = a.get(b);
                    VariableDecl* t_decl = NULL;
                    {
                        // prepare variable t
                        t_decl = new VariableDecl(
                                new SimpleIdentifier(wstr_id + L"-t"),
                                NULL,
                                false, false, false,
                                Declaration::VisibilitySpecifier::DEFAULT);

                        // prepare a
                        IdExpr* a_use = new IdExpr(new SimpleIdentifier(wstr_id + L"-a"));
                        ResolvedSymbol::set(a_use, a_decl);
                        ResolvedType::set(a_use, a_decl_type);

                        CallExpr* call_expr_get = new CallExpr(new MemberExpr(a_use, new SimpleIdentifier(L"get")));

                        // prepare b
                        IdExpr* b_use = new IdExpr(new SimpleIdentifier(wstr_id + L"-b"));
                        ResolvedSymbol::set(b_use, b_decl);
                        ResolvedType::set(b_use, b_decl_type);
                        call_expr_get->appendParameter(b_use);

                        t_decl->setInitializer(call_expr_get);

                        block_expr->block->appendObject(new DeclarativeStmt(t_decl));
                    }
                    Type* t_decl_type = t_decl->getCanonicalType();

                    // a.set(b, t+1);
                    {
                        // prepare a
                        IdExpr* a_use = new IdExpr(new SimpleIdentifier(wstr_id + L"-a"));
                        ResolvedSymbol::set(a_use, a_decl);
                        ResolvedType::set(a_use, a_decl_type);

                        CallExpr* call_expr_set = new CallExpr(new MemberExpr(a_use, new SimpleIdentifier(L"set")));

                        // prepare b
                        IdExpr* b_use = new IdExpr(new SimpleIdentifier(wstr_id + L"-b"));
                        ResolvedSymbol::set(b_use, b_decl);
                        ResolvedType::set(b_use, b_decl_type);
                        call_expr_set->appendParameter(b_use);

                        // prepare t+1
                        IdExpr* t_use = new IdExpr(new SimpleIdentifier(wstr_id + L"-t"));
                        ResolvedSymbol::set(t_use, t_decl);
                        ResolvedType::set(t_use, t_decl_type);

                        call_expr_set->appendParameter(
                                new UnaryExpr(anchor->opcode, t_use));

                        block_expr->block->appendObject(new ExpressionStmt(call_expr_set));
                    }

                    // t;
                    {
                        // prepare t
                        IdExpr* t_use = new IdExpr(new SimpleIdentifier(wstr_id + L"-t"));
                        ResolvedSymbol::set(t_use, t_decl);
                        ResolvedType::set(t_use, t_decl_type);

                        block_expr->block->appendObject(new ExpressionStmt(t_use));
                    }

                    ASTNodeHelper::foreachApply<ASTNode>(*block_expr, [&](ASTNode& i){ ASTNodeHelper::propogateSourceInfo(i, node); });

                    // var a = node.left; => var a; a = node.left;
                    // var b = node.right; => var b; b = node.right;
                    // var t = c; => var t; t = c;
                    {
                        restructure_helper::splitVariableInitializer(*a_decl, transforms, false);
                        restructure_helper::splitVariableInitializer(*b_decl, transforms, false);
                        restructure_helper::splitVariableInitializer(*t_decl, transforms, false);
                    }

                    anchor->parent->replaceUseWith(*anchor, *block_expr);
                });
                transformed = true;
            }
        }
    }

    if(!transformed)
    {
        // for the rest case, simply transform a[b] into: a.get(b)
        transforms.push_back([&]() {
            CallExpr* call_expr_get = new CallExpr(new MemberExpr(node.array, new SimpleIdentifier(L"get")));
            call_expr_get->appendParameter(node.index);

            ASTNodeHelper::foreachApply<ASTNode>(*call_expr_get, [&](ASTNode& i){ ASTNodeHelper::propogateSourceInfo(i, node); });

            node.parent->replaceUseWith(node, *call_expr_get);
        });
    }
}

void RestructureStageVisitor0::restruct(LambdaExpr& node)
{
    revisit(node);

    restructure_lambda_helper.restruct(node);
}

void RestructureStageVisitor0::restruct(StringLiteral& node)
{
    if(isa<Tangle>(node.parent))
        return;
    
    // for the string literals like this: 
    // 
    //     "hello \{ a } the world!" 
    //
    // which will be restructed to:
    // 
    //               +
    //            /     \
    //         +       " the world!"
    //      /     \
    //  "hello "   StringizeExpr
    //                   |
    //                   a
    // 
    // NOTE: now we only accept simple id in the \{}
    typedef StringLiteral::string_type string_type;
    std::pair<  
              string_type::const_iterator,
              string_type::const_iterator
             > place;

    auto*const origin_parent = node.parent;

    BOOST_ASSERT(origin_parent != nullptr && "null pointer exception");

    auto last_found = node.value.find_last_of( StringLiteral::getInternalPlaceholderBegin() );
    auto placeholder_begin = node.value.cbegin() + last_found;
    if( StringLiteral::searchPlaceholder(placeholder_begin, node.value.cend(), place) ) 
    { 
        auto plus = BinaryExpr::OpCode::ARITHMETIC_ADD;

        // (1) complete the sub expression 
        StringLiteral * left_expr = new StringLiteral( node.value.cbegin(), place.first );
   
        // we only accept simple id now
        string_type id_name = string_type( place.first+1, place.second-1 );
        Expression * sub_node = createIdOrMemberExprFromString(id_name);
        StringizeExpr * middle_expr = new StringizeExpr(sub_node);

        BinaryExpr * sub_expr = new BinaryExpr( plus, left_expr, middle_expr );
        
        // (2) complete the whole expression
        StringLiteral * right_expr = new StringLiteral( place.second, node.value.cend() );

        BinaryExpr * result_expr = new BinaryExpr( plus, sub_expr, right_expr );
       
        // replace the original node 
        node.parent->replaceUseWith(node, *result_expr);

        ASTNodeHelper::propogateSourceInfoRecursively(*result_expr, *origin_parent);

        revisit( *result_expr );
        return;
    }

    SymbolIdLiteral* symbol_id_literal = new SymbolIdLiteral(L"");

    CallExpr* get_string_call = new CallExpr(ASTNodeHelper::useThorLangId(&node, new SimpleIdentifier(L"__getStringLiteral")));
    get_string_call->appendParameter(symbol_id_literal);

    node.parent->replaceUseWith(node, *get_string_call);

    ASTNodeHelper::propogateSourceInfoRecursively(*get_string_call, *origin_parent);

    const auto& insertion_result = getParserContext().tangle->addSymbol(node);
    const auto& offseted_id      = insertion_result.first;

    symbol_id_literal->tangle_uuid = offseted_id.first;
    symbol_id_literal->local_id    = offseted_id.second;

}

void RestructureStageVisitor0::restruct(UnaryExpr& node)
{
    revisit(node);

    if(node.opcode == UnaryExpr::OpCode::NEW)
    {
        restructure_helper::restructureUnaryNew(node, transforms);
    }
}

bool RestructureStageVisitor0::hasTransforms()
{
    return (transforms.size() > 0);
}

void RestructureStageVisitor0::applyTransforms()
{
    for(auto& transform : transforms)
    {
        transform();
    }
    transforms.clear();
}

} } } }
