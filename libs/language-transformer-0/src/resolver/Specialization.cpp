/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <vector>
#include <memory>
#include <type_traits>
#include <unordered_map>

#include <boost/assert.hpp>
#include <boost/foreach.hpp>
#include <boost/iterator/filter_iterator.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/next_prior.hpp>
#include <boost/range/iterator_range.hpp>

#include "language/context/ResolverContext.h"
#include "language/logging/StringTable.h"
#include "language/resolver/Deduction.h"
#include "language/resolver/Helper.h"
#include "language/resolver/Specialization.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/TypenameDecl.h"
#include "language/tree/module/Source.h"

namespace zillians { namespace language { namespace resolution { namespace specialization {

namespace
{

// forward declarations
template<typename ContainerType>
bool is_all_template(const ContainerType& decls);
template<typename ContainerType>
bool is_all_same_template_parameter_count(const ContainerType& decls);

bool is_matched_specialization(tree::ClassDecl& generic_decl, tree::ClassDecl& specialized_decl);
bool is_matched_specialization(tree::FunctionDecl& generic_decl, tree::FunctionDecl& specialized_decl);

template<typename ContainerType>
boost::optional<std::vector<tree::ClassDecl*> > filter_duplicated_specialization(tree::ClassDecl& generic_decl, const ContainerType& specializations);
template<typename ContainerType>
boost::optional<std::vector<tree::FunctionDecl*> > filter_duplicated_specialization(tree::FunctionDecl& FunctionDecl, const ContainerType& specializations);

detail::Item<tree::ClassDecl> generate_dummy_item(tree::ClassDecl& cls_decl);
detail::Item<tree::FunctionDecl> generate_dummy_item(tree::FunctionDecl& func_decl);

template<typename ContainerType, typename DeclType>
void add_impl(ContainerType& groups, DeclType& decl);

void set_specialization_impl(tree::FunctionDecl& generic_decl, tree::FunctionDecl& specialized_decl);
void set_specialization_impl(tree::ClassDecl& generic_decl, tree::ClassDecl& specialized_decl);

void set_empty_specialization(tree::ClassDecl& generic_decl);
void set_empty_specialization(tree::FunctionDecl& generic_decl);

template<typename DeclType, typename ContainerType>
bool set_specialization(DeclType& generic_decl, const ContainerType& specializations);

template<typename ContainerType>
std::vector<typename ContainerType::value_type> find_multi_generic_version(const ContainerType& decls);

template<typename MapType, typename RangeType>
std::vector<typename RangeType::value_type> group_template_specializations(MapType& generic_decls, RangeType& decls);

template<typename ContainerType>
void report_duplicated_declarations(const ContainerType& declarations);

// implementations
template<typename ContainerType>
bool is_all_template(const ContainerType& decls)
{
    BOOST_ASSERT(!decls.empty() && "no declaration to be verified");

    for(tree::Declaration* decl: decls)
    {
        BOOST_ASSERT(decl != nullptr && "null pointer exception");

        if(!decl->name->getTemplateId())
        {
            return false;
        }
    }

    return true;
}

template<typename ContainerType>
bool is_all_same_template_parameter_count(const ContainerType& decls)
{
    BOOST_ASSERT(!decls.empty() && "no declaration to be verified");
    BOOST_ASSERT(is_all_template(decls) && "found non-template declaration!");

    const auto parameter_count = decls.front()->name->getTemplateId()->templated_type_list.size();

    BOOST_FOREACH(tree::Declaration* decl, std::make_pair(boost::next(decls.begin()), decls.end()))
    {
        BOOST_ASSERT(decl != nullptr && decl->name != nullptr && "null pointer exception");

        tree::TemplatedIdentifier* template_id = decl->name->getTemplateId();

        BOOST_ASSERT(template_id != nullptr && "found non-template declaration!");

        if(parameter_count != template_id->templated_type_list.size())
        {
            return false;
        }
    }

    return true;
}

bool is_matched_specialization(tree::ClassDecl& generic_decl, tree::ClassDecl& specialized_decl)
{
    BOOST_ASSERT( helper::is_generic_template(    generic_decl) && "generic_decl is not a generic declaration!");
    BOOST_ASSERT(!helper::is_generic_template(specialized_decl) && "specialized_decl is a generic declaration!");

    // Since there must be exactly one generic template class declaration.
    // There is no need to verify specialization of class declarations

    return true;
}

bool is_matched_specialization(tree::FunctionDecl& generic_decl, tree::FunctionDecl& specialized_decl)
{
    BOOST_ASSERT(generic_decl.name && specialized_decl.name && "null pointer exception");
    BOOST_ASSERT(generic_decl.name->getTemplateId() != nullptr && specialized_decl.name->getTemplateId() != nullptr && "non-template function declaration!");
    BOOST_ASSERT( helper::is_generic_template(    generic_decl) && "generic_decl is not a generic declaration!");
    BOOST_ASSERT(!helper::is_generic_template(specialized_decl) && "specialized_decl is a generic declaration!");

    tree::TemplatedIdentifier* generic_template_id = generic_decl.name->getTemplateId();
    tree::TemplatedIdentifier* specialized_template_id = specialized_decl.name->getTemplateId();

    BOOST_ASSERT(generic_template_id && specialized_template_id && "null pointer exception");
    BOOST_ASSERT(generic_template_id->templated_type_list.size() == specialized_template_id->templated_type_list.size() && "size of declarations to compare must be the same");

    // TODO implement verification of function template specialization

    const auto is_matched = deduction::match_template_recursively(generic_decl, specialized_decl);
    return is_matched;
}

template<typename ContainerType>
boost::optional<std::vector<tree::ClassDecl*> > filter_duplicated_specialization(tree::ClassDecl& generic_decl, const ContainerType& specializations)
{
    static_assert(std::is_same<tree::ClassDecl, typename std::remove_pointer<typename ContainerType::value_type>::type>::value, "type mismatch between generic and specializations");

    std::vector<tree::ClassDecl*> non_duplicated_specializations;
    std::map<tree::ClassDecl*, tree::ClassDecl*> duplicated_specializations;

    for(tree::ClassDecl* specialization: specializations)
    {
        BOOST_ASSERT(specialization && "null pointer exception");

        bool is_duplicated = false;

        for(tree::ClassDecl* current_decl: non_duplicated_specializations)
        {
            BOOST_ASSERT(current_decl && "null pointer exception");

            const boost::tribool is_same = deduction::is_same_specialization(*specialization, *current_decl);

            if(boost::indeterminate(is_same))
            {
                return boost::optional<std::vector<tree::ClassDecl*> >();
            }
            else if(is_same)
            {
                duplicated_specializations.insert(std::make_pair(specialization, current_decl));

                is_duplicated = true;

                break;
            }
        }

        if(!is_duplicated)
        {
            non_duplicated_specializations.emplace_back(specialization);
        }
    }

    if(!duplicated_specializations.empty())
    {
        const std::wstring& generic_decl_name = generic_decl.toString();

        for(const auto& entry: duplicated_specializations)
        {
            tree::ClassDecl* specialization = entry.first;
            tree::ClassDecl* duplicated_specialization = entry.second;

            BOOST_ASSERT(specialization && duplicated_specialization && "null pointer exception");

            LOG_MESSAGE(DUPE_SPECIALIZATION, specialization, _generic_decl_name(generic_decl_name));
            LOG_MESSAGE(DUPE_SPECIALIZATION_PREV, duplicated_specialization, _generic_decl_name(generic_decl_name));
        }
    }

    return non_duplicated_specializations;
}

template<typename ContainerType>
boost::optional<std::vector<tree::FunctionDecl*> > filter_duplicated_specialization(tree::FunctionDecl& generic_decl, const ContainerType& specializations)
{
    static_assert(std::is_same<tree::FunctionDecl, typename std::remove_pointer<typename ContainerType::value_type>::type>::value, "type mismatch between generic and specializations");

    std::vector<tree::FunctionDecl*> non_duplicated_specializations;
    std::map<SpecializedAsTraits::KeyTypeOfMap, tree::FunctionDecl*> unique_signature_map;

    for(tree::FunctionDecl* specialization: specializations)
    {
        BOOST_ASSERT(specialization && "null pointer exception");

              auto  key              = SpecializedAsTraits::generateKey(*specialization);
        const auto& insertion_result = unique_signature_map.emplace(std::move(key), tree::make_relinkable(specialization));

        const bool  is_duplicated = !insertion_result.second;

        if(is_duplicated)
        {
            const auto& duplicated_pos = insertion_result.first;

            BOOST_ASSERT(tree::isa<tree::FunctionDecl>(duplicated_pos->second) && "specialization of function declaration is not a function!");

            const std::wstring& generic_decl_name = generic_decl.toString();
            tree::FunctionDecl* duplicated_specialization = duplicated_pos->second;

            BOOST_ASSERT(duplicated_specialization && "null pointer exception");

            LOG_MESSAGE(DUPE_SPECIALIZATION, specialization, _generic_decl_name(generic_decl_name));
            LOG_MESSAGE(DUPE_SPECIALIZATION_PREV, duplicated_specialization, _generic_decl_name(generic_decl_name));
        }
        else
        {
            non_duplicated_specializations.emplace_back(specialization);
        }
    }

    return non_duplicated_specializations;
}

detail::Item<tree::ClassDecl> generate_dummy_item(tree::ClassDecl& cls_decl)
{
    BOOST_ASSERT(cls_decl.name && "null pointer exception");
    BOOST_ASSERT(isa<tree::Source>(cls_decl.parent) && "class declaration is not under source (not global)!");

    detail::Item<tree::ClassDecl> dummy_value;

    dummy_value.owner = ASTNodeHelper::getDeclarationOwner(cls_decl);
    dummy_value.name  = cls_decl.name->getSimpleId()->toString();

    return dummy_value;
}

detail::Item<tree::FunctionDecl> generate_dummy_item(tree::FunctionDecl& func_decl)
{
    BOOST_ASSERT(func_decl.name && "null pointer exception");
    BOOST_ASSERT(
        (
            isa<tree::Source>(func_decl.parent) ||
            isa<tree::ClassDecl>(func_decl.parent)
        ) && "function declaration is not under source or class (global or member function)!"
    );

    detail::Item<tree::FunctionDecl> dummy_value;

    dummy_value.owner                    = ASTNodeHelper::getDeclarationOwner(func_decl);
    dummy_value.call_parameter_count     = func_decl.parameters.size();
    dummy_value.template_parameter_count = 0;
    dummy_value.name                     = func_decl.name->getSimpleId()->toString();

    if(tree::TemplatedIdentifier* template_id = func_decl.name->getTemplateId())
    {
        BOOST_ASSERT(!template_id->templated_type_list.empty() && "template but no template parameter!");

        dummy_value.template_parameter_count = template_id->templated_type_list.size();
    }

    return dummy_value;
}

template<typename ContainerType, typename DeclType>
void add_impl(ContainerType& groups, DeclType& decl)
{
    static_assert(std::is_base_of<tree::Declaration, DeclType>::value, "DeclType is not a declaration!");

    auto insertion_result = groups.insert(generate_dummy_item(decl));

    groups.modify(insertion_result.first, [&decl](typename ContainerType::value_type& value) -> void
    {
        value.declarations.emplace_back(&decl);
    });
}

void set_specialization_impl(tree::ClassDecl& generic_decl, tree::ClassDecl& specialized_decl)
{
    SpecializationOf::set(&specialized_decl, &generic_decl);
}

void set_specialization_impl(tree::FunctionDecl& generic_decl, tree::FunctionDecl& specialized_decl)
{
    SpecializedAs::set(&generic_decl, &specialized_decl);
    SpecializationOf::set(&specialized_decl, &generic_decl);
}

void set_empty_specialization(tree::ClassDecl& generic_decl)
{
}

void set_empty_specialization(tree::FunctionDecl& generic_decl)
{
    SpecializedAs::init(&generic_decl);
}

template<typename DeclType, typename ContainerType>
bool set_specialization(DeclType& generic_decl, const ContainerType& specializations)
{
    static_assert(std::is_same<DeclType, typename std::remove_pointer<typename ContainerType::value_type>::type>::value, "specialization and generic types are different!");

    if(specializations.empty())
    {
        set_empty_specialization(generic_decl);

        return true;
    }
    else
    {
        const auto& optional_result = filter_duplicated_specialization(generic_decl, specializations);

        if(optional_result.is_initialized())
        {
            const auto& non_duplicated_specializations = *optional_result;

            for(DeclType* specialization: non_duplicated_specializations)
            {
                BOOST_ASSERT(specialization && "null pointer exception");

                if(generic_decl.parent != specialization->parent)
                {
                    BOOST_ASSERT(isa<tree::Source>(generic_decl.parent) && isa<tree::Source>(specialization->parent) && "non-global template specialization won't be here!");

                    LOG_MESSAGE(CROSS_SOURCE_SPECIALIZATION, specialization);
                    LOG_MESSAGE(CROSS_SOURCE_SPECIALIZATION_GENERIC, &generic_decl);
                }
                else
                {
                    set_specialization_impl(generic_decl, *specialization);
                }
            }

            return true;
        }
        else
        {
            return false;
        }
    }
}

template<typename ContainerType>
std::vector<typename ContainerType::value_type> find_multi_generic_version(const ContainerType& decls)
{
    static_assert(std::is_pointer<typename ContainerType::value_type>::value, "we expect all declarations are AST node pointers");

    typedef typename std::remove_pointer<typename ContainerType::value_type>::type DeclType;

    std::vector<DeclType*> generic_decls;

    for(DeclType* decl: decls)
    {
        if(helper::is_generic_template(*decl))
        {
            generic_decls.push_back(decl);
        }
    }

    return generic_decls;
}

template<typename MapType, typename RangeType>
std::vector<typename RangeType::value_type> group_template_specializations(MapType& decl_groups, RangeType& decls)
{
    static_assert(std::is_pointer<typename RangeType::value_type>::value, "we expect all declarations are AST node pointers");

    typedef typename std::remove_pointer<typename RangeType::value_type>::type DeclType;

    BOOST_ASSERT(!decl_groups.empty() && "no group of generic declarations!");

    std::vector<DeclType*> not_grouped_decls;

    for(DeclType* decl: decls)
    {
        bool is_multi_match = false;
        auto specialized_decl_pos = decl_groups.end();

        for(auto i = decl_groups.begin(); i != decl_groups.end(); ++i)
        {
            DeclType* generic_decl = i->first;

            if(is_matched_specialization(*generic_decl, *decl))
            {
                if(specialized_decl_pos == decl_groups.end())
                {
                    specialized_decl_pos = i;
                }
                else
                {
                    is_multi_match = true;
                }
            }
        }

        if(!is_multi_match && specialized_decl_pos != decl_groups.end())
        {
            auto& specialization_group = specialized_decl_pos->second;

            specialization_group.emplace_back(decl);
        }
        else
        {
            not_grouped_decls.emplace_back(decl);
        }
    }

    return not_grouped_decls;
}

template<typename ContainerType>
void report_duplicated_declarations(const ContainerType& declarations)
{
    if(declarations.size() > 1)
    {
        const auto begin = declarations.begin();
        const auto end   = declarations.end();

        const std::wstring& id = declarations.front()->name->getSimpleId()->toString();

        for(auto* decl: boost::make_iterator_range(boost::next(declarations.begin()), declarations.end()))
        {
            BOOST_ASSERT(decl && "null pointer exception");

            LOG_MESSAGE(DUPE_NAME, decl, _id(id));
            LOG_MESSAGE(DUPE_NAME_PREV, declarations.front());
        }
    }
}

}

void Grouper::add(tree::ClassDecl& cls_decl)
{
    add_impl(cls_groups.get<TagOwnerAndName>(), cls_decl);
}

void Grouper::add(tree::FunctionDecl& func_decl)
{
    add_impl(func_groups.get<TagOwnerAndName>(), func_decl);
}

void Grouper::ready()
{
    group_classes();
    group_functions();
}

void Grouper::group_classes()
{
    auto& ordered_by_owner = cls_groups.get<TagOwner>();

    for(auto i = ordered_by_owner.begin(); i != ordered_by_owner.end();)
    {
        const auto& declarations = i->declarations;

        boost::tribool is_grouped = boost::indeterminate;

        BOOST_ASSERT(!declarations.empty() && "no declaration to be grouped");

        if(is_all_template(declarations))
        {
            if(is_all_same_template_parameter_count(declarations))
            {
                const auto generic_decls = find_multi_generic_version(declarations);

                if(!generic_decls.empty())
                {
                    if(generic_decls.size() == 1)
                    {
                        tree::ClassDecl* generic_cls_decl = generic_decls.front();

                        const auto is_not_generic_one = [generic_cls_decl](tree::ClassDecl* cls_decl) -> bool
                        {
                            BOOST_ASSERT(cls_decl && "null pointer exception");

                            return cls_decl != generic_cls_decl;
                        };

                        if(helper::is_all_resolved(declarations))
                        {
                            auto begin = boost::make_filter_iterator(is_not_generic_one, declarations.begin(), declarations.end());
                            auto end   = boost::make_filter_iterator(is_not_generic_one, declarations.end(), declarations.end());

                            auto range = boost::make_iterator_range(begin, end);

                            std::unordered_map<tree::ClassDecl*, std::vector<tree::ClassDecl*>> decl_groups;

                            decl_groups.insert(std::make_pair(generic_cls_decl, std::vector<tree::ClassDecl*>()));

                            const auto not_grouped_decls = group_template_specializations(decl_groups, range);

                            BOOST_ASSERT(decl_groups.size() == 1);
                            BOOST_ASSERT(decl_groups.begin()->first == generic_cls_decl && "generic declaration is changed!");
                            BOOST_ASSERT(decl_groups.begin()->second.size() + not_grouped_decls.size() == declarations.size() - 1 && "some declaration is missing");

                            if(!not_grouped_decls.empty())
                            {
                                for(tree::Declaration* decl: not_grouped_decls)
                                {
                                    LOG_MESSAGE(NO_MATCHED_GENERIC_TEMPLATE, decl);
                                }
                            }

                            {
                                const auto& specializations = decl_groups.begin()->second;

                                set_specialization(*generic_cls_decl, specializations);
                            }

                            is_grouped = true;
                        }
                        else
                        {
                            is_grouped = boost::indeterminate;
                        }
                    }
                    else
                    {
                        for(tree::Declaration* generic_decl: generic_decls)
                        {
                            LOG_MESSAGE(DUPLICATED_GENERIC_TEMPLATES, generic_decl);
                        }

                        is_grouped = false;
                    }
                }
                else
                {
                    for(tree::Declaration* decl: declarations)
                    {
                        LOG_MESSAGE(NO_GENERIC_TEMPLATE, decl);
                    }

                    is_grouped = false;
                }
            }
            else
            {
                for(tree::Declaration* decl: declarations)
                {
                    LOG_MESSAGE(INVALID_TEMPLATE_CLASS_DECLARATIONS, decl);
                }

                is_grouped = false;
            }
        }
        else
        {
            BOOST_ASSERT(declarations.size() >= 1);

            is_grouped = declarations.size() == 1;

            report_duplicated_declarations(declarations);
        }

        if(boost::indeterminate(is_grouped))
        {
            ++i;
        }
        else
        {
            i = ordered_by_owner.erase(i);
        }
    }
}

void Grouper::group_functions()
{
    auto& ordered_by_owner = func_groups.get<TagOwner>();

    for(auto i = ordered_by_owner.begin(); i != ordered_by_owner.end();)
    {
        const auto& item = *i;
        const auto& declarations = item.declarations;

        static_assert(std::is_unsigned<decltype(item.template_parameter_count)>::value, "we expect 'template_parameter_count' is a unsigned integer");

        boost::tribool is_grouped = boost::indeterminate;

        BOOST_ASSERT(!declarations.empty() && "no declaration to be grouped");

        if(item.template_parameter_count != 0)
        {
            BOOST_ASSERT(is_all_same_template_parameter_count(declarations) && "template functions with different template parameter count should not be grouped together");

            const auto generic_decls = find_multi_generic_version(declarations);

            if(!generic_decls.empty())
            {
                BOOST_ASSERT(generic_decls.size() <= declarations.size() && "amount of generic declarations is more than the total");

                if(generic_decls.size() != declarations.size())
                {
                    if(helper::is_all_resolved(declarations))
                    {
                        const auto is_not_generic_one = [](tree::FunctionDecl* decl) -> bool
                        {
                            BOOST_ASSERT(decl && decl->name && "null pointer exception");
                            BOOST_ASSERT(decl->name->getTemplateId() && "non-template function declaration should not enter here!");

                            return !helper::is_generic_template(*decl);
                        };

                        auto begin = boost::make_filter_iterator(is_not_generic_one, declarations.begin(), declarations.end());
                        auto end   = boost::make_filter_iterator(is_not_generic_one, declarations.end(), declarations.end());

                        auto range = boost::make_iterator_range(begin, end);

                        std::unordered_map<tree::FunctionDecl*, std::vector<tree::FunctionDecl*>> decl_groups;

                        for(tree::FunctionDecl* generic_decl: generic_decls)
                        {
                            decl_groups.insert(std::make_pair(generic_decl, std::vector<tree::FunctionDecl*>()));
                        }

                        const auto not_grouped_decls = group_template_specializations(decl_groups, range);

                        BOOST_ASSERT(decl_groups.size() == generic_decls.size());

                        if(!not_grouped_decls.empty())
                        {
                            for(tree::Declaration* decl: not_grouped_decls)
                            {
                                LOG_MESSAGE(NO_MATCHED_GENERIC_TEMPLATE, decl);
                            }
                        }

                        for(const auto& decl_group: decl_groups)
                        {
                            tree::FunctionDecl* generic_decl = decl_group.first;
                            const auto& specializations = decl_group.second;

                            set_specialization(*generic_decl, specializations);
                        }

                        is_grouped = true;
                    }
                    else
                    {
                        is_grouped = boost::indeterminate;
                    }
                }
                else
                {
                    // TODO verify duplicated declaration

                    for(tree::FunctionDecl* generic_decl: generic_decls)
                    {
                        BOOST_ASSERT(generic_decl && "null pointer exception");

                        SpecializedAs::init(generic_decl);
                    }

                    is_grouped = true;
                }
            }
            else
            {
                for(tree::Declaration* decl: declarations)
                {
                    LOG_MESSAGE(NO_GENERIC_TEMPLATE, decl);
                }

                is_grouped = false;
            }
        }
        else
        {
            BOOST_ASSERT(declarations.size() >= 1);

            if(declarations.size() != 1)
            {
                is_grouped = false;

                std::map<std::vector<tree::ASTNode*>, std::vector<tree::FunctionDecl*>> signature_map;

                for(tree::FunctionDecl* func: declarations)
                {
                    BOOST_ASSERT(func && "null pointer exception");

                    std::vector<tree::ASTNode*> parameter_types;

                    parameter_types.reserve(func->parameters.size());

                    for(tree::VariableDecl* parameter: func->parameters)
                    {
                        BOOST_ASSERT(parameter && "null pointer exception");

                        if(Type::ASTNode* canonical_type = parameter->getCanonicalType())
                        {
                            parameter_types.push_back(canonical_type);
                        }
                        else
                        {
                            break;
                        }
                    }

                    if(parameter_types.size() == func->parameters.size())
                    {
                        signature_map[parameter_types].emplace_back(func);
                    }
                    else
                    {
                        is_grouped = boost::indeterminate;
                    }
                }

                if(!boost::indeterminate(is_grouped))
                {
                    for(const auto& pair: signature_map)
                    {
                        report_duplicated_declarations(pair.second);
                    }
                }
            }
            else
            {
                is_grouped = true;
            }
        }

        if(boost::indeterminate(is_grouped))
        {
            ++i;
        }
        else
        {
            i = ordered_by_owner.erase(i);
        }
    }
}

} } } }

