<?xml version="1.0"?>

<!--
This file is part of Thor.
Thor is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License, version 3,
as published by the Free Software Foundation.

Thor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Thor.  If not, see <http://www.gnu.org/licenses/>.

If you want to develop any commercial services or closed-source products with
Thor, to adapt sources of Thor in your own projects without
disclosing sources, purchasing a commercial license is mandatory.

For more information, please contact Zillians, Inc.
<thor@zillians.com>
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="StringTable.base.xsl"/>

<xsl:output method="text"/>

<xsl:template match="table">
  <xsl:copy-of select="$header_prefix"/>

  <xsl:for-each select="parameters/parameter[not(@name = following-sibling::*/@name)]">
    <xsl:call-template name="def_log_tag_creator"/>
  </xsl:for-each>

  <xsl:for-each select="messages/message">
    <xsl:call-template name="def_log_params"/>
  </xsl:for-each>

  <xsl:call-template name="def_string_table"/>
  <xsl:call-template name="def_logger"/>

  <xsl:copy-of select="$header_suffix"/>
</xsl:template>

</xsl:stylesheet>
