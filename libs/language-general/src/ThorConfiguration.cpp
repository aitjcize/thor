/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifdef __linux__
    #include <sys/wait.h>
#endif

#include <cstdlib>

#include <iostream>
#include <stdexcept>

#include <boost/format.hpp>
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "core/Types.h"
#include "utility/Filesystem.h"
#include "utility/UUIDUtil.h"

#include "language/ThorConfiguration.h"
#include "language/tree/CustomizedSerialization.h"

namespace zillians { namespace language {

const char THOR_DEFAULT_MANIFEST_PATH    [] = "manifest.xml";
const char THOR_DEFAULT_MANIFEST_DTD_PATH[] = "manifest.dtd";
const char THOR_DEFAULT_CACHE_PATH       [] = "build/.cache";
const char THOR_DEFAULT_LAST_CACHE_PATH  [] = "build/.last_cache";

const char THOR_BUNDLER [] = "thor-bundle";
const char THOR_COMPILER[] = "thor-compile";
const char THOR_DRIVER  [] = "thorc";
const char THOR_LINKER  [] = "thor-link";
const char THOR_MAKE    [] = "thor-make";
const char THOR_DOC     [] = "thor-doc";
const char THOR_STUB    [] = "thor-stub";
const char THOR_VM      [] = "thor-vm";

#if   defined(__PLATFORM_LINUX__)
    #define PLAT_VALUE_SELECT(linux, mac, windows)  linux
#elif defined(__PLATFORM_MAC__)
    #define PLAT_VALUE_SELECT(linux, mac, windows)  mac
#elif defined(__PLATFORM_WINDOWS__)
    #define PLAT_VALUE_SELECT(linux, mac, windows)  windows
#else
    #error "not supported platform"
#endif

#define PLAT_VALUE(var, linux, mac, windows) \
    const char var[] = PLAT_VALUE_SELECT(linux, mac, windows)

PLAT_VALUE(THOR_DEFAULT_HOME_PATH         , "/usr/local/thor/", "/usr/local/thor/", "\\Thor");
PLAT_VALUE(THOR_DEFAULT_SYSTEM_BUNDLE_PATH, "system.bundle"         , "system.bundle"         , "lib\\system");

PLAT_VALUE(THOR_PREFIX_LIBRARY  , "lib", "lib", "");

PLAT_VALUE(THOR_EXTENSION_SOURCE, ".t"     , ".t"     , ".t"     );
PLAT_VALUE(THOR_EXTENSION_BUNDLE, ".bundle", ".bundle", ".bundle");
PLAT_VALUE(THOR_EXTENSION_ASM   , ".s"     , ".s"     , ".s"     );
PLAT_VALUE(THOR_EXTENSION_PTX   , ".ptx"   , ".ptx"   , ".ptx"   );
PLAT_VALUE(THOR_EXTENSION_CU    , ".cu"    , ".cu"    , ".cu"    );
PLAT_VALUE(THOR_EXTENSION_CUH   , ".cuh"   , ".cuh"   , ".cuh"   );
PLAT_VALUE(THOR_EXTENSION_IL    , ".il"    , ""       , ""       );
PLAT_VALUE(THOR_EXTENSION_CL    , ".cl"    , ""       , ""       );
PLAT_VALUE(THOR_EXTENSION_SO    , ".so"    , ".dylib" , "dll"    );
PLAT_VALUE(THOR_EXTENSION_LIB   , ".a"     , ""       , ""       );
PLAT_VALUE(THOR_EXTENSION_AST   , ".ast"   , ".ast"   , ".ast"   );
PLAT_VALUE(THOR_EXTENSION_BC    , ".bc"    , ".bc"    , ".bc"    );
PLAT_VALUE(THOR_EXTENSION_OBJ   , ".o"     , ".o"     , ".o"     );
PLAT_VALUE(THOR_EXTENSION_CPP   , ".cc"    , ".cc"    , ".cc"    );
PLAT_VALUE(THOR_EXTENSION_H     , ".h"     , ".h"     , ".h"     );
PLAT_VALUE(THOR_JIT_AST_POSTFIX , "_mangle", "_mangle", "_mangle");

PLAT_VALUE(THOR_CPP_NATIVE_COMPILER, "g++-4.8", "g++-4.8", "cl.exe");
PLAT_VALUE(THOR_NATIVE_ARCHIVER    , "ar"     , "ar"     , ""      );

namespace detail {

bool EnvironmentHelper::findHomeDirectory(boost::filesystem::path& home, Method m)
{
    // 1. Environment variable ${THOR_HOME}
    if(m == AUTOMATIC || m == BY_ENVIRONMENT_VARIABLE)
    {
        char* var = std::getenv("THOR_HOME");
        if(var)
        {
            boost::filesystem::path p(var);
            if(!p.empty())
            {
                if(boost::filesystem::exists(p))
                {
                    home = p;
                    return true;
                }
            }
        }
    }

    // 2. Fixed default path defined by THOR_SYSTEM_BUNDLE_DEFAULT_PATH
    if(m == AUTOMATIC || m == BY_COMPILED_STATIC_PATH)
    {
        boost::filesystem::path p(THOR_DEFAULT_HOME_PATH);
        if(boost::filesystem::exists(p) && boost::filesystem::is_directory(p))
        {
            home = p;
            return true;
        }
    }

    // 3. Use parent directory of current executable as ${TS_HOME}
    if(m == AUTOMATIC || m == BY_EXECUTABLE_PATH)
    {
        boost::filesystem::path p = Filesystem::current_executable_path().parent_path().parent_path(); // this is {TS_HOME}/bin
        if(boost::filesystem::exists(p) && boost::filesystem::is_directory(p))
        {
            home = p;
            return true;
        }
    }

    return false;
}

} // namespace 'zillains::language::detail'

XMLValidator::XMLValidator(const boost::filesystem::path& xml_dtd)
: xml_dtd(xml_dtd)
{ }

auto XMLValidator::validate(const boost::filesystem::path& document) const -> result_t
{
    // check if the XML DTD file is exist
    if (!boost::filesystem::exists(xml_dtd) || !boost::filesystem::is_regular_file(xml_dtd))
    {
        return result_t::DTD_NOT_FOUND;
    }

    // check if the XML file is exist
    if (!boost::filesystem::exists(document) || !boost::filesystem::is_regular_file(document))
    {
        return result_t::DOC_NOT_FOUND;
    }

    // check if the tool is exist
    auto tool_path = Filesystem::find_program_by_name(getToolName());
    if (tool_path.empty())
        return result_t::SKIPPED;

    // execute external tool to verify if XML is in valid format
    const auto command = boost::str(
        boost::format("%1% --dtdvalid %2% --noout %3%") % tool_path.string() % xml_dtd % document
    );

    return (!execute(command) ? result_t::SUCCESS : result_t::ILL_FORMED);
}

const char* XMLValidator::getToolName()
{
    return "xmllint";
}

int XMLValidator::execute(const std::string& command)
{
    const auto process_status = std::system(command.c_str());
#ifdef __linux__
    if (WIFEXITED(process_status))
        return WEXITSTATUS(process_status);
    else
        return EXIT_FAILURE;
#else
    return process_status;
#endif
}

ThorManifest::ThorManifest()
    : name("author"),
      version("0.0.0.1"),
      bundle_type(BundleType::JIT),
      no_system(false),
      project_id(UUID::random())
{
#if PLATFORM_BIT_WIDTH == 64
        using_64bit = true;
#else
        using_64bit = false;
#endif
}

bool ThorManifest::load(const boost::filesystem::path& manifest_xml, const boost::filesystem::path& sdk_path, const XMLValidator& validator)
{
    switch(validator.validate(manifest_xml))
    {
    case XMLValidator::result_t::DTD_NOT_FOUND:
        std::cerr << "Error: cannot find meta file to validate configuration file." << std::endl;
        return false;
    case XMLValidator::result_t::DOC_NOT_FOUND:
        std::cerr << "Error: cannot find configuration file." << std::endl;
        return false;
    case XMLValidator::result_t::ILL_FORMED:
        return false;
    case XMLValidator::result_t::SKIPPED:
        std::cerr << "Warning: cannot perform validating, so skip it." << std::endl;
    detault:
        break;
    }

    // load project attributes
    using boost::property_tree::ptree;
    ptree pt;
    boost::property_tree::xml_parser::read_xml(manifest_xml.string(), pt);

    // load and validate required 'project' attribute 'name'
    name = pt.get<std::string>("project.<xmlattr>.name");
    static const boost::regex e("\\w[-_a-zA-Z0-9]*");
    if (!boost::regex_match(name.c_str(), e))
    {
        std::cerr << "Error: project name '" << name << "' is invalid" << std::endl;
        return false;
    }

    // load required 'project' attribute 'author', 'version', 'bundle_type' directly
    author      = pt.get<std::string>("project.<xmlattr>.name");
    version     = pt.get<std::string>("project.<xmlattr>.version");
    bundle_type = (pt.get<std::string>("project.<xmlattr>.bundle_type") == "jit" ? BundleType::JIT : BundleType::NATIVE);

    // load optional 'project' attribute 'signature'
    signature.clear();
    auto exist = pt.get_optional<std::string>("project.<xmlattr>.signature");
    if (exist)
        signature = *exist;

    // clear existing project dependencies
    deps.bundles.clear();
    deps.native_objects.clear();
    deps.native_libraries.clear();
    deps.native_shared_libraries.clear();

    // load optional 'project' attribute 'no_system'
    no_system = false;
    exist = pt.get_optional<std::string>("project.<xmlattr>.no_system");
    if (exist)
    {
        no_system = (*exist == "true");
    }

    if(!no_system)
    {
        if(!importSystemBundle(sdk_path))
            return false;
    }

    // load optional 'project' attribute 'using_64bit'
#if PLATFORM_BIT_WIDTH == 64
        using_64bit = true;
#else
        using_64bit = false;
#endif
    exist = pt.get_optional<std::string>("project.<xmlattr>.using_64bit");
    if (exist)
    {
        using_64bit = (*exist == "true");
    }

    try
    {
        const auto& id_str = pt.get<std::string>("project.<xmlattr>.id");

        project_id = UUID::parse(id_str);
    }
    catch (const std::runtime_error& e)
    {
        std::cerr << "Cannot parse ID from manifest, reason: " << e.what() << std::endl;

        return false;
    }

    // load optional 'platform' attribute 'arch'(architecture)
    static const auto reset_arch_and_report_error = [](Architecture& arch,
                                                       const std::string& arch_string,
                                                       const std::string& node_name) -> bool
    {
        auto success = arch.reset(arch_string, ";");
        if (!success)
        {
            std::cerr << (
                boost::format("Error: invalid architecture string \'%1%\' for %2%.") % arch_string % node_name
            ) << std::endl;
        }

        return success;
    };


    arch.reset();
    auto semicolon_separated_arch = pt.get_optional<std::string>("project.platform.<xmlattr>.arch");
    if (semicolon_separated_arch)
    {
        if (!reset_arch_and_report_error(arch, *semicolon_separated_arch, "platform"))
        {
            return false;
        }
    }

    // for given empty architecture string, use default 'x86'
    if (arch.is_zero())
    {
        // give a default configuration if no platform target is specified
        arch = Architecture::x86();
    }

    Architecture::default_ = arch;

    // load project dependencies from xml parser
    auto dependencies = pt.get_child_optional("project.dependency");
    if (!dependencies)
        return true;

    for (const auto& dependency_entity : *dependencies)
    {
        const auto& node    = dependency_entity.first;
        const auto& subtree = dependency_entity.second;

        if (node == "bundle")
        {
            DependentBundle bundle;
            bundle.name = subtree.get<std::string>("<xmlattr>.name");

            auto lpath  = subtree.get_optional<std::string>("<xmlattr>.lpath");
            if (lpath) bundle.lpath = *lpath;

            deps.bundles.push_back(bundle);
        }
        else if (node == "native_object")
        {
            DependentObject object;
            object.name = subtree.get<std::string>("<xmlattr>.name");

            auto lpath  = subtree.get_optional<std::string>("<xmlattr>.lpath");
            if (lpath) object.lpath = *lpath;

            object.arch = arch;
            auto semicolon_separated_arch = subtree.get_optional<std::string>("<xmlattr>.arch");
            if (semicolon_separated_arch)
            {
                if (!reset_arch_and_report_error(object.arch, *semicolon_separated_arch, object.name))
                {
                    return false;
                }
            }

            deps.native_objects.push_back(object);
        }
        else if (node == "native_library")
        {
            DependentLibrary library;
            library.name = subtree.get<std::string>("<xmlattr>.name");

            auto lpath  = subtree.get_optional<std::string>("<xmlattr>.lpath");
            if (lpath) library.lpath = *lpath;

            library.arch = arch;
            auto semicolon_separated_arch = subtree.get_optional<std::string>("<xmlattr>.arch");
            if (semicolon_separated_arch)
            {
                if (!reset_arch_and_report_error(library.arch, *semicolon_separated_arch, library.name))
                {
                    return false;
                }
            }

            deps.native_libraries.push_back(library);
        }
        else if (node == "native_shared_library")
        {
            DependentSharedLibrary shared_library;
            shared_library.name = subtree.get<std::string>("<xmlattr>.name");

            auto lpath  = subtree.get_optional<std::string>("<xmlattr>.lpath");
            if (lpath) shared_library.lpath = *lpath;

            shared_library.arch = arch;
            auto semicolon_separated_arch = subtree.get_optional<std::string>("<xmlattr>.arch");
            if (semicolon_separated_arch)
            {
                if (!reset_arch_and_report_error(shared_library.arch, *semicolon_separated_arch, shared_library.name))
                {
                    return false;
                }
            }

            deps.native_shared_libraries.push_back(shared_library);
        }
    }

    return true;
}

bool ThorManifest::save(const boost::filesystem::path& document, const XMLValidator& validator)
{
    // open and save configuration into file
    {
        std::ofstream document_stream(document.string());

        if(!document_stream.is_open())
        {
            std::cerr << "Error: cannot open file \'" << document.string() << "\' to write" << std::endl;
            return false;
        }

        // check if the project name is valid
        static const boost::regex e("\\w[-_a-zA-Z0-9]*");
        if(!boost::regex_match(name.c_str(), e))
        {
            std::cerr << "Error: project name '" << name << "' is invalid" << std::endl;
            return false;
        }

        document_stream << "<project name=\"" << name << "\" author=\"" << author << "\" signature=\"" << signature << "\" version=\"" << version << "\" bundle_type=\"" << ((bundle_type == BundleType::JIT) ? "jit" : "native") << "\"";
        if(no_system)
            document_stream << " no_system=\"" << (no_system ? "true" : "false") << "\"";

        document_stream << " using_64bit=\"" << (using_64bit ? "true" : "false") << "\"";

        document_stream << " id=\"" << project_id.toString() << "\"";

        document_stream << ">\n";

        document_stream << "    <dependency>\n";

        for(auto& bundle : deps.bundles)
        {
            // avoid saving automatic-generated bundle
            if(bundle.name != THOR_DEFAULT_SYSTEM_BUNDLE_PATH)
            {
                document_stream << "        <bundle name=\"" << bundle.name << "\"";
                if(!bundle.lpath.empty()) document_stream << " lpath=\"" << bundle.lpath << "\"";
                document_stream << "/>" << std::endl;
            }
        }

        for(auto& object : deps.native_objects)
        {
            document_stream << "        <native_object name=\"" << object.name << "\"";
            if(!object.lpath.empty()) document_stream << " lpath=\"" << object.lpath << "\"";
            if(object.arch != arch)   document_stream << " arch=\""  << object.arch.toString(";") << "\"";
            document_stream << "/>" << std::endl;
        }

        for(auto& library : deps.native_libraries)
        {
            document_stream << "        <native_library name=\"" << library.name << "\"";
            if(!library.lpath.empty()) document_stream << " lpath=\"" << library.lpath << "\"";
            if(!library.lpath.empty()) document_stream << " lpath=\"" << library.lpath << "\"";
            if(library.arch != arch)   document_stream << " arch=\""  << library.arch.toString(";") << "\"";
            document_stream << "/>" << std::endl;
        }

        for(auto& shared : deps.native_shared_libraries)
        {
            document_stream << "        <native_shared_library name=\"" << shared.name << "\"";
            if(!shared.lpath.empty()) document_stream << " lpath=\"" << shared.lpath << "\"";
            if(!shared.rpath.empty()) document_stream << " rpath=\"" << shared.rpath << "\"";
            if(shared.arch != arch)   document_stream << " arch=\""  << shared.arch.toString(";") << "\"";
            document_stream << "/>" << std::endl;
        }

        document_stream << "    </dependency>\n";

        document_stream << "    <platform arch=\"" << arch.toString(";") << "\"/>\n";

        document_stream << "</project>\n" ;

    } // end writing configuration

    switch(validator.validate(document))
    {
    case XMLValidator::result_t::DTD_NOT_FOUND:
        std::cerr << "Error: cannot find meta file to validate saved configuration file" << std::endl;
        return false;
    case XMLValidator::result_t::DOC_NOT_FOUND:
        std::cerr << "Error: cannot find saved configuration file" << std::endl;
        return false;
    case XMLValidator::result_t::ILL_FORMED:
        std::cerr << "Error: the saved configuration file is ill-formed" << std::endl;
        return false;
    case XMLValidator::result_t::SKIPPED:
        std::cerr << "Warning: cannot perform validating, so skip it" << std::endl;
    default:
        break;
    }

    return true;
}

bool ThorManifest::importSystemBundle(const boost::filesystem::path& sdk_path)
{
    boost::filesystem::path system_bundle_path = sdk_path / "bundle" / (THOR_DEFAULT_SYSTEM_BUNDLE_PATH);
    if(boost::filesystem::exists(system_bundle_path) && boost::filesystem::is_regular_file(system_bundle_path))
    {
        DependentBundle system_bundle;
        system_bundle.name = THOR_DEFAULT_SYSTEM_BUNDLE_PATH;
        system_bundle.lpath = (sdk_path / "bundle").string();
        deps.bundles.push_back(system_bundle);
        return true;
    }
    else
    {
        std::cerr << "Error: failed to locate system bundle, please check your installation" << std::endl;
        return false;
    }
}

ThorBuildConfiguration::ThorBuildConfiguration()
: changed(true),
  verbose(false),
  timing(false)
{ }

std::string ThorBuildConfiguration::getProjectSharedObjectPath(ThorBuildConfiguration& config)
{
    std::string so_name = THOR_PREFIX_LIBRARY + config.manifest.name + THOR_EXTENSION_SO;
    return (config.binary_output_path / so_name).string();
}

bool ThorBuildConfiguration::findProjectRoot(boost::filesystem::path& project_root, boost::filesystem::path current)
{
    boost::filesystem::path p = boost::filesystem::absolute(current);

    while(!p.empty())
    {
        boost::filesystem::path manifest = p / THOR_DEFAULT_MANIFEST_PATH;
        if(boost::filesystem::exists(manifest) && boost::filesystem::is_regular_file(manifest))
        {
            project_root = p;
            return true;
        }
        p = p.parent_path();
    }

    return false;
}

bool ThorBuildConfiguration::load(const boost::filesystem::path& project_path, bool configure_build_parameters, bool configure_manifest)
{
    boost::filesystem::path p;
    if(!findProjectRoot(p, project_path))
    {
        std::cerr << "Error: not under valid project (or any parent up to mount parent " << boost::filesystem::current_path().string() << ")" << std::endl;
        return false;
    }

    if(!configureDefaultPaths(p))
    {
        return false;
    }

    return loadDefault(configure_build_parameters, false, configure_manifest);
}

bool ThorBuildConfiguration::loadDefault(bool configure_build_parameters, bool configure_path, bool configure_manifest)
{
    if(configure_build_parameters)
    {
        changed = true;
        verbose = false;
        dump_ts = false;
        dump_llvm = false;
        dump_graphviz = false;
        timing = false;
        static_test_case = "";
        prepend_package = "";
        max_parallel_jobs = 1;
        debug_tool.clear();
    }

    if(configure_path)
    {
        boost::filesystem::path p;
        if(!findProjectRoot(p, boost::filesystem::current_path()))
        {
            std::cerr << "Error: not under valid project (or any parent up to mount parent " << boost::filesystem::current_path().string() << ")" << std::endl;
            return false;
        }

        if(!configureDefaultPaths(p))
        {
            return false;
        }
    }
    else
    {
        if(!configureDefaultPaths(project_path))
        {
            return false;
        }
    }

    if(configure_manifest)
    {
        const auto manifest_dtd = sdk_executable_path / THOR_DEFAULT_MANIFEST_DTD_PATH;
        XMLValidator validator(manifest_dtd);

        if(manifest.load(manifest_path, sdk_path, validator))
        {
            return true;
        }
        else
        {
            std::cerr << "Error: failed to load project manifest" << std::endl;
            return false;
        }
    }

    return true;
}


bool ThorBuildConfiguration::createDirectoriesIfNecessary()
{
    bool result = true;

    if(!boost::filesystem::exists(build_path) || !boost::filesystem::is_directory(build_path))
        result &= boost::filesystem::create_directories(build_path);

    if(!boost::filesystem::exists(dump_ts_dir) || !boost::filesystem::is_directory(dump_ts_dir))
        result &= boost::filesystem::create_directories(dump_ts_dir);

    if(!boost::filesystem::exists(dump_graphviz_dir) || !boost::filesystem::is_directory(dump_graphviz_dir))
        result &= boost::filesystem::create_directories(dump_graphviz_dir);

    if(!boost::filesystem::exists(source_path) || !boost::filesystem::is_directory(source_path))
        result &= boost::filesystem::create_directories(source_path);

    if(!boost::filesystem::exists(extract_bundle_path) || !boost::filesystem::is_directory(extract_bundle_path))
        result &= boost::filesystem::create_directories(extract_bundle_path);

    if(!boost::filesystem::exists(library_path) || !boost::filesystem::is_directory(library_path))
        result &= boost::filesystem::create_directories(library_path);

    if(!boost::filesystem::exists(binary_output_path) || !boost::filesystem::is_directory(binary_output_path))
        result &= boost::filesystem::create_directories(binary_output_path);

    if(!boost::filesystem::exists(stub_output_path) || !boost::filesystem::is_directory(stub_output_path))
        result &= boost::filesystem::create_directories(stub_output_path);

    return result;
}

bool ThorBuildConfiguration::configureDefaultPaths(const boost::filesystem::path& p)
{
    if(!configureToolPath() || !configureProjectPaths(p))
        return false;
    else
        return true;
}

bool ThorBuildConfiguration::configureToolPath()
{
    if(!detail::EnvironmentHelper::findHomeDirectory(sdk_path))
    {
        std::cerr << "Error: failed to locate SDK installation path, abort" << std::endl;
        return false;
    }
    else
    {
        sdk_executable_path = sdk_path / "bin";
        sdk_bundle_path = sdk_path / "bundle";
        sdk_lib_path = sdk_path / "lib";

        return true;
    }
}

bool ThorBuildConfiguration::configureProjectPaths(const boost::filesystem::path& p)
{
    project_path = p;
    manifest_path = project_path / THOR_DEFAULT_MANIFEST_PATH;

    if(build_path_override.empty())
        build_path = project_path / "build";
    else
        build_path = build_path_override;

    library_path = project_path / "lib";
    source_path = project_path / "src";

    if(extract_bundle_path_override.empty())
        extract_bundle_path = project_path / "dep";
    else
        extract_bundle_path = extract_bundle_path_override;

    dump_ts_dir = build_path / "dump";
    dump_graphviz_dir = build_path / "graphviz";

    if(binary_output_path_override.empty())
        binary_output_path = project_path / "bin";
    else
        binary_output_path = binary_output_path_override;

    stub_output_path = project_path / "stub";

    return true;
}

bool ThorBuildConfiguration::loadFromCache(bool track_change)
{
    changed = false;

    std::ifstream ifs((project_path / THOR_DEFAULT_CACHE_PATH).string());
    if(!ifs.good()) return false;

    // de-serialize through boost archive
    boost::archive::text_iarchive ia(ifs);
    ia >> *this;

    ifs.close();

    if(track_change)
    {
        if(Filesystem::is_regular_file(project_path / THOR_DEFAULT_LAST_CACHE_PATH))
        {
            std::ifstream ifs_last((project_path / THOR_DEFAULT_LAST_CACHE_PATH).string());
            if(!ifs_last.good())
                changed = true;

            ThorBuildConfiguration last;
            boost::archive::text_iarchive ia_last(ifs_last);
            ia_last >> last;

            changed = !compare(last);
        }
        else
        {
            changed = true;
        }
    }

    return true;
}

bool ThorBuildConfiguration::saveToCache(bool track_change)
{
    if(track_change)
    {
        if(Filesystem::is_regular_file(project_path / THOR_DEFAULT_CACHE_PATH))
        {
            boost::filesystem::rename(project_path / THOR_DEFAULT_CACHE_PATH, project_path / THOR_DEFAULT_LAST_CACHE_PATH);
        }
    }

    std::ofstream ofs((project_path / THOR_DEFAULT_CACHE_PATH).string());
    if(!ofs.good()) return false;

    // serialize through boost archive
    boost::archive::text_oarchive oa(ofs);
    oa << *this;

    ofs.close();

    return true;
}

bool ThorBuildConfiguration::compare(const ThorBuildConfiguration& other)
{
    if( verbose == other.verbose &&
        dump_ts == other.dump_ts &&
        dump_llvm == other.dump_llvm &&
        dump_graphviz == other.dump_graphviz &&
        timing == other.timing &&
        static_test_case == other.static_test_case &&
        prepend_package == other.prepend_package &&
        max_parallel_jobs == other.max_parallel_jobs &&
        project_path == other.project_path &&
        manifest_path == other.manifest_path &&
        sdk_path == other.sdk_path &&
        sdk_executable_path == other.sdk_executable_path &&
        sdk_bundle_path == other.sdk_bundle_path &&
        sdk_lib_path == other.sdk_lib_path &&
        library_path == other.library_path &&
        build_path == other.build_path &&
        source_path == other.source_path &&
        extract_bundle_path == other.extract_bundle_path &&
        dump_ts_dir == other.dump_ts_dir &&
        dump_graphviz_dir == other.dump_graphviz_dir &&
        binary_output_path == other.binary_output_path &&
        stub_output_path == other.stub_output_path)
        return true;
    else
        return false;
}

std::istream& operator >> (std::istream& in, ThorBuildConfiguration::CodeGenType& code_gen_type)
{
    std::string val_str;

    in >> val_str;
    if(val_str == "0")
        code_gen_type = ThorBuildConfiguration::CodeGenType::NATIVE;

    else if(val_str == "1")
        code_gen_type = ThorBuildConfiguration::CodeGenType::JIT_TANGLE_AST;

    else if(val_str == "2")
        code_gen_type = ThorBuildConfiguration::CodeGenType::JIT_BUNDLE_AST;

    else
    {
        std::cerr << "Undefined code generation type was given. Default to NATIVE." << std::endl;
        code_gen_type = ThorBuildConfiguration::CodeGenType::NATIVE;
    }

    return in;
}

std::ostream& operator << (std::ostream& os, const ThorBuildConfiguration::CodeGenType& code_gen_type)
{
    os << static_cast<int>(code_gen_type);
    return os;
}

} } // namespace 'zillians::language'
