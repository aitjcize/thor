/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cstring>

#include <iostream>
#include <stdexcept>
#include <regex>
#include <vector>
#include <utility>
#include <map>

// C++11 enum caused failure when linking against boost::filesystem::copy_file in boost 1.53
// boost ticket: https://svn.boost.org/trac/boost/ticket/6779
// current workaround is from here: http://www.ridgesolutions.ie/index.php/2013/05/30/boost-link-error-undefined-reference-to-boostfilesystemdetailcopy_file/
#define BOOST_NO_CXX11_SCOPED_ENUMS
#include <boost/range/end.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/as_literal.hpp>
#include <boost/range/algorithm/for_each.hpp>
#include <boost/range/algorithm/equal.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/program_options.hpp>

#include "utility/StringUtil.h"
#include "utility/sha1.h"
#include "utility/Foreach.h"

#include "language/ThorVM.h"
#include "language/logging/LoggerWrapper.h"
#include "language/ThorToolBase.h"
#include "language/ToolExitCode.h"

#include "runtime/reactor/BasicReactor.h"

namespace zillians { namespace language {

struct ThorVM::virtual_machine_setting
{
    runtime::VMMode     vm_mode     = runtime::VMMode::GenericMode;
    runtime::DomainType domain_type = runtime::DomainType::SingleThreaded;

    int dev_id = 0;

    std::string project_path;
    std::string bundle_path;
    std::string build_path;

    bool show_help    = false;
    bool dump_command = false;

    std::string debug_tool;
    std::string debug_tool_cmd;
    std::string entry;
    std::vector<std::string> args;
};

ThorVM::ThorVM()
{
    namespace po = boost::program_options;

    LoggerWrapper::instance();

    options.add_options()
        ("domain"        , po::value<std::string>()            , "Domain the VM runs on")
        ("dev"           , po::value<int>()                    , "Device id")
        ("server,s"      , po::bool_switch()                   , "Run on server mode")
        ("client,c"      , po::bool_switch()                   , "Run on client mode")
        ("project-path"  , po::value<std::string>()            , "Thor project root")
        ("bundle-path"   , po::value<std::string>()            , "Path where will be bundles extracted to")
        ("build-path"    , po::value<std::string>()            , "Override the default build path")
        ("debug-tool,g"  , po::value<std::string>()            , "Debug specific Thor tool")
        ("debug-tool-cmd", po::value<std::string>()            , "Custom debugger command to debug specific tool from --debug-tool")
        ("help,h"        , po::bool_switch()                   , "Show help manual")
        ("verbose,v"     , po::bool_switch()                   , "Verbose mode, which dumps all commands")
        ("entry"         , po::value<std::string>()->required(), "Entry function name(package names included)")
        ;

    positional.add("entry", 1);
}

ThorVM::~ThorVM()
{
}

auto ThorVM::parse_option(int argc, const char* argv[]) const -> variables_type
{
    namespace po = boost::program_options;

    variables_type variables;
    po::store(
        po::command_line_parser(argc, argv).options(options).positional(positional).run(),
        variables
    );
    po::notify(variables);

    return std::move(variables);
}

bool ThorVM::init_virtual_machine_setting(virtual_machine_setting& setting, const variables_type& variables) const
{
    // setup vm mode
    const auto run_as_server = variables["server"].as<bool>();
    const auto run_as_client = variables["client"].as<bool>();
    if (run_as_server && run_as_client)
    {
        std::cerr << "exclusive option value \'server\' & \'client\'" << std::endl;
        return false;
    }

    if (run_as_server)
    {
        setting.vm_mode = runtime::VMMode::ServerMode;
    }
    else if (run_as_client)
    {
        setting.vm_mode = runtime::VMMode::ClientMode;
    }

    // setup domain type
    static const std::map<std::string, runtime::DomainType>
    domain_types{
        { "st"    , runtime::DomainType::SingleThreaded },
        { "mt"    , runtime::DomainType::MultiThreaded  },
        { "kepler", runtime::DomainType::Kepler         },
        { "tahiti", runtime::DomainType::Tahiti         },
        { "opencl", runtime::DomainType::OpenCL         },
        { "cuda"  , runtime::DomainType::CUDA           },
        { "gpu"   , runtime::DomainType::GPU            },
        { "k10m"  , runtime::DomainType::K10M           }
    };
    if (variables.count("domain"))
    {
        const auto provided_value = variables["domain"].as<std::string>();
        const auto value_and_domain_type = domain_types.find(provided_value);
        if (value_and_domain_type == boost::end(domain_types))
            return false;

        setting.domain_type = std::get<1>(*value_and_domain_type);
    }

    // setup device id
    if (variables.count("dev"))
    {
        setting.dev_id = variables["dev"].as<int>();
    }

    // setup paths
    if (variables.count("project-path"))
    {
        setting.project_path = variables["project-path"].as<std::string>();
    }

    if (variables.count("bundle-path"))
    {
        setting.bundle_path = variables["bundle-path"].as<std::string>();
    }

    if (variables.count("build-path"))
    {
        setting.build_path = variables["build-path"].as<std::string>();
    }

    // setup
    setting.show_help    = variables["help"   ].as<bool>();
    setting.dump_command = variables["verbose"].as<bool>();

    // setup debug tool options
    if (variables.count("debug-tool"))
    {
        setting.debug_tool = variables["debug-tool"].as<std::string>();
    }

    if (variables.count("debug-tool-cmd"))
    {
        setting.debug_tool_cmd = variables["debug-tool-cmd"].as<std::string>();
    }

    setting.entry = variables["entry"].as<std::string>();

    return true;
}

std::string ThorVM::getManglingName(std::string& name)
{
    // we assume the entry function is int32 xxxx(void)
    std::vector<std::string> tokens = StringUtil::tokenize(name, ".");
    BOOST_ASSERT(tokens.size() != 0);

    std::string mangling;
    std::string prefix;
    std::string postfix;

    if (tokens.size() == 1)
    {
        // Well, this should be global function
        prefix = "_Z";
        postfix = "v";
    }
    else
    {
        // Well, the function resides in namespace or is a class static function, or both.
        prefix = "_ZN";
        postfix = "Ev";
    }

    mangling = prefix;
    for (decltype(tokens.size()) i = 0; i < tokens.size(); i++)
    {
        mangling += StringUtil::itoa(tokens[i].size(), 10) + tokens[i] ;
    }
    mangling += postfix;

    return mangling;
}

bool ThorVM::init_virtual_machine_setting(virtual_machine_setting& setting, int argc, const char* argv[]) const
{
    // filter out options after '--args' and store them into setting.args
    auto remain_options = boost::make_iterator_range(argv + 1, argv + argc);
    for (; !remain_options.empty(); remain_options.pop_front())
    {
        const auto* const current_option = remain_options.front();
        if (!std::strcmp("--args", current_option) || !std::strcmp("-a", current_option))
            break;
    }

    argc -= remain_options.size();
    if (!remain_options.empty())
    {
        boost::push_back(setting.args, remain_options.advance_begin(1));
    }

    // parse options before '--args'
    variables_type variables;
    try
    {
        variables = parse_option(argc, argv);
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return false;
    }

    return init_virtual_machine_setting(setting, variables);
}

void ThorVM::dump_help_manual() const
{
    std::cerr <<
        "Usage: thor-vm [options]\n"
        "\n"
        "Allowed options:\n";

    std::cerr << options
              << "  --args ..." << std::endl;
}

int ThorVM::main(int argc, const char** argv)
{
    virtual_machine_setting setting;
    const auto success = init_virtual_machine_setting(setting, argc, argv);
    if(!success)
    {
        std::cerr << "Error: error parsing command line" << std::endl;
        dump_help_manual();
        return -1;
    }
    else if(setting.show_help)
    {
        dump_help_manual();
        return 0;
    }

    if(setting.entry.empty())
    {
        std::cerr << "Error: program entry is not specified" << std::endl;
        dump_help_manual();
        return -1;
    }

    if(!setting.project_path.empty() && !setting.bundle_path.empty())
    {
        std::cerr << "Error: you should specify either '--project-path' or '--bundle-path' but not both" << std::endl;
        dump_help_manual();
        return -1;
    }

    // configure compiler tool path
    if(!config.configureToolPath())
    {
        return -1;
    }

    // override the build path if necessary
    if(!setting.build_path.empty())
        config.build_path_override = setting.build_path;

    if(setting.project_path.empty() && setting.bundle_path.empty()) // neither project-path nor bundle path are specified
    {
        config.loadDefault(false, true, false);

        // set current path to project path
        boost::filesystem::current_path(config.project_path);
    }
    else if(!setting.project_path.empty()) // only project-path is specified,
    {
        config.configureProjectPaths(setting.project_path);

        // set current path to project path
        boost::filesystem::current_path(config.project_path);
    }
    else // only bundle-path is specified,
    {
        // extract the bundle to a temporary location
        boost::filesystem::path p = boost::filesystem::unique_path();
        boost::filesystem::create_directories(p);

        // set current path to project path
        boost::filesystem::current_path(p);

        if(ThorToolBase::shell(config, THOR_BUNDLER, "-x --bundle-path=\'" + setting.bundle_path + "\' --bundle-extract-path=\'" + p.string() + "\'") != 0)
        {
            std::cerr << "Error: failed to extract given bundle, abort" << std::endl;
            return -1;
        }

        config.configureProjectPaths(p);
    }

    // load default the parameters and manifest
    if(!config.loadDefault(true, false, true))
    {
        std::cerr << "Error: failed to load project configuration" << std::endl;
        return -1;
    }

    config.debug_tool     = setting.debug_tool;
    config.debug_tool_cmd = setting.debug_tool_cmd;

    // override dump command
    config.verbose = setting.dump_command;

    // generate the mangled entry name
    mangled_entry_name = getManglingName(setting.entry);

    /// TODO: the args might be complex options which include spaces or backslashes
    ///       need to find ways to recover them back
    return execute(setting.domain_type, setting.vm_mode, setting.dev_id, setting.args);
}

int ThorVM::execute(runtime::DomainType domain_type, runtime::VMMode vm_mode, int dev_id, const std::vector<std::string>& args)
{
    auto jit_codegen = [](const ThorBuildConfiguration& config,
                          const boost::filesystem::path& project_path,
                          const boost::filesystem::path& build_path,
                          bool  is_bundle_ast)
    {
        // Since thorc needs the working directory to be the root of project,
        // we change the working directory to project_path via current_path(),
        // and then change back after thorc is done.
        boost::filesystem::path old_cwd = boost::filesystem::current_path();
        boost::filesystem::current_path(project_path);
        std::stringstream cmd;

        cmd << " build";

        if(config.verbose)
            cmd << " --verbose";
        
        if(is_bundle_ast)
        {
            cmd << " --codegen-type="     << ThorBuildConfiguration::CodeGenType::JIT_BUNDLE_AST;
            cmd << " --extract-bundle-path=" << config.extract_bundle_path;
        }
        else
            cmd << " --codegen-type=" << ThorBuildConfiguration::CodeGenType::JIT_TANGLE_AST;

        if(config.dump_llvm)
            cmd << " --dump-llvm";
        cmd << " --project-path=" << project_path;
        cmd << " --build-path="   << build_path;

        int ec = ThorToolBase::shell(config, THOR_DRIVER, cmd.str());
        boost::filesystem::current_path(old_cwd);
        return ec;
    };

    boost::filesystem::path              main_ast_path;
    boost::filesystem::path              main_runtime_path;
    std::vector<boost::filesystem::path> dep_paths;
    std::vector<boost::filesystem::path> files_to_cleanup;

    ///////////// Step 1. Compile all JIT bundles
    for(auto &dep_bundle : config.manifest.deps.bundles)
    {
        boost::filesystem::path bundle_path = config.extract_bundle_path / dep_bundle.name;
        boost::filesystem::path build_path  = config.build_path / dep_bundle.name;
        std::vector<boost::filesystem::path> bundle_build_results;
        int ec = jit_codegen(config, bundle_path, build_path, true);
        if(ec == ToolExitCode::Success)
        {
            if(domain_type == runtime::DomainType::MultiThreaded || domain_type == runtime::DomainType::SingleThreaded)
            {
                bundle_build_results = Filesystem::collect_files(bundle_path, THOR_EXTENSION_SO, true);
            }
            else if(domain_type == runtime::DomainType::CUDA)
            {
                bundle_build_results = Filesystem::collect_files(bundle_path, THOR_EXTENSION_LIB, true);
            }
            for(auto& bundle_build : bundle_build_results)
            {
                boost::filesystem::path copy_to_file = config.binary_output_path / bundle_build.filename();
                boost::filesystem::copy_file(bundle_build, copy_to_file, boost::filesystem::copy_option::overwrite_if_exists);
                files_to_cleanup.push_back(copy_to_file);
            }
        }
        else if(ec == ToolExitCode::JitIgnore)
        {
            continue;
        }
        else
        {
            std::cerr << "Error: failed to compile dependent JIT bundle. Error code: " << ec << std::endl;
            return ec;
        }
    }
    
    ///////////// Step 2. Collect all dependent bundle paths
    for(auto &dep_bundle : config.manifest.deps.bundles)
    {
        boost::filesystem::path bundle_filename(dep_bundle.name);
        std::string      bundle_name = bundle_filename.stem().string();
        std::string bundle_extension = bundle_filename.extension().string();
        BOOST_ASSERT(bundle_extension == ".bundle" && "Expected bundle name format: <name>.bundle");

        boost::filesystem::path bundle_path = config.binary_output_path;
        if(domain_type == runtime::DomainType::MultiThreaded || domain_type == runtime::DomainType::SingleThreaded)
        {
            bundle_path = bundle_path / (THOR_PREFIX_LIBRARY + bundle_name + THOR_EXTENSION_SO);
        }
        else if(domain_type == runtime::DomainType::CUDA)
        {
            bundle_path = bundle_path / (THOR_PREFIX_LIBRARY + bundle_name + "_cuda" + THOR_EXTENSION_LIB);
        }
        else
        {
            BOOST_ASSERT(false && "Unsupported domain type.");
        }
        dep_paths.push_back(bundle_path);
    }

    ///////////// Step 3. Collect main ast & main runtime object
    if(config.manifest.bundle_type == ThorManifest::BundleType::JIT)
    {
        boost::filesystem::path ast_file_to_read = config.binary_output_path / (config.manifest.name + THOR_EXTENSION_AST);
        main_ast_path = config.build_path / (config.manifest.name + THOR_JIT_AST_POSTFIX + THOR_EXTENSION_AST);

        int ec = jit_codegen(config, boost::filesystem::current_path(), config.build_path, false);
        if(ec != 0)
        {
            std::cerr << "Error: failed to compile into final executable" << std::endl;
            return ec;
        }
        files_to_cleanup.push_back(main_ast_path);
    }
    else
    {
        main_ast_path = config.binary_output_path / (config.manifest.name + THOR_EXTENSION_AST);
    }

    if(domain_type == runtime::DomainType::MultiThreaded || domain_type == runtime::DomainType::SingleThreaded)
    {
        main_runtime_path = config.binary_output_path / (THOR_PREFIX_LIBRARY + config.manifest.name + THOR_EXTENSION_SO);
    }
    else if(domain_type == runtime::DomainType::CUDA)
    {
        main_runtime_path = config.binary_output_path / (THOR_PREFIX_LIBRARY + config.manifest.name + "_cuda" + THOR_EXTENSION_LIB);
    }

    using namespace zillians::runtime;

    int result = 0;

    // use project root path as current working directory
    boost::filesystem::path current_working_dir = boost::filesystem::current_path();
    boost::filesystem::current_path(config.project_path);

    runtime::BasicReactor reactor(domain_type, vm_mode, dev_id, args, config.verbose);
    reactor.initialize();

    if(domain_type == runtime::DomainType::CUDA)
    {
        boost::for_each(
            config.manifest.arch.splitFlags(Architecture::cuda()),
            [&reactor](Architecture arch)
            {
                std::string isa = arch.toString("");
                reactor.configureHardwareArchitecture(isa);
            }
        );
    }

    do
    {
        if(!reactor.loadKernel(main_ast_path, main_runtime_path, dep_paths))
        {
            std::cerr << "Error: failed to load library" << std::endl;
            result = -1;
            break;
        }

        reactor.start();

        if(!reactor.runSystemInit())
        {
            std::cerr << "Error: failed to run system init" << std::endl;
            result = -1;
            break;
        }

        if(!reactor.runInit())
        {
            std::cerr << "Error: failed to init global values" << std::endl;
            result = -1;
            break;
        }

        if(!reactor.runEntry(mangled_entry_name))
        {
            result = -1;
            break;
        }

        result = reactor.waitForCompletion();
    }while(0);

    reactor.stop();
    reactor.finalize();

    // restore the current working directory
    boost::filesystem::current_path(current_working_dir);

    for(auto& to_cleanup : files_to_cleanup)
    {
        boost::filesystem::remove(to_cleanup);
    }

    if(result != 0)
    {
        std::cerr << "Error: program returns non-zero, error code = " << result << std::endl;
    }

    return result;
}

} }
