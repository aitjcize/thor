/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cstdlib>
#include <iostream>
#include <string>

#ifdef __unix__

#include <sys/wait.h>

#endif

#include "language/ThorToolBase.h"
#include "language/ToolExitCode.h"

namespace zillians { namespace language {

namespace {

// TODO allow user to customize preferred debugger
const std::string DEFAULT_DEBUGGER_CMD("cgdb --args");

}

const std::string& ThorToolBase::getDefaultDebugToolCommand()
{
    return DEFAULT_DEBUGGER_CMD;
}

// The return value of system() is implementation-dependent. We need the actuall exit code of a child process, according to:
// http://stackoverflow.com/questions/20193464/how-to-get-the-exit-code-of-program-invoked-by-system-call
// WEXITSTAUS() can do the trick. In case of Windows, system() returns the exit code of the invoked child process from my experience, at least on XP and Windows 7.
int ThorToolBase::shell(const boost::filesystem::path& executable_path, const std::string& parameters, const std::string& debug_tool, const std::string& debug_tool_cmd)
{
    int exit_status = 0;
    if(executable_path.filename().string() != debug_tool)
    {
        exit_status = std::system((executable_path.string() + " " + parameters).c_str());
    }
    else
    {
        exit_status = std::system(((debug_tool_cmd.empty() ? getDefaultDebugToolCommand() : debug_tool_cmd) + " " + executable_path.string() + " " + parameters).c_str());
    }

#ifdef _WIN32
    return exit_status;
#else
    if(WIFEXITED(exit_status))
    {
        return WEXITSTATUS(exit_status);
    }
    else
    {
        std::cerr << "[Driver] invokes '" << executable_path.string() << " " << parameters << "'" << " child process failed to end normally." << std::endl;
        return ToolExitCode::Failed;
    }
#endif
}

int ThorToolBase::shell(const ThorBuildConfiguration& config, const std::string& tool, const std::string& parameters)
{
    const auto& executable_path = config.sdk_executable_path / tool;
    if(config.verbose)
    {
        std::cerr << "[Driver] invokes '" << executable_path.string() << " " << parameters << "'" << std::endl;
    }

    return shell(executable_path, parameters, config.debug_tool, config.debug_tool_cmd);
}

} }
