R"SKELETON_CODE(
; ModuleID = 'KernelMT.gd.cpp'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

define void @_Z15global_dispatchxPcS_(i64 %function_id, i8* %parameter_ptr, i8* %store_to) uwtable {
entry:
  %function_id.addr = alloca i64, align 8
  %parameter_ptr.addr = alloca i8*, align 8
  %store_to.addr = alloca i8*, align 8
  store i64 %function_id, i64* %function_id.addr, align 8
  store i8* %parameter_ptr, i8** %parameter_ptr.addr, align 8
  store i8* %store_to, i8** %store_to.addr, align 8
  %0 = load i64* %function_id.addr, align 8
  switch i64 %0, label %sw.epilog [
    i64 $__object__destruction__$   , label %sw.destroy_objects
    i64 $__remote_invocation__$     , label %sw.remote_invocation
    i64 $__system_initialization__$ , label %sw.system_initialize
    i64 $__global_initialization__$ , label %sw.global_initialize
  ]

sw.destroy_objects:                                            ; preds = %entry
  %1 = load i8** %parameter_ptr.addr, align 8
  call void @_ZN12_GLOBAL__N_115destroy_objectsEPc(i8* %1)
  br label %sw.epilog

sw.remote_invocation:                                           ; preds = %entry
  %2 = load i8** %parameter_ptr.addr, align 8
  call void @_ZN4thor4lang15__remoteInvokerEPc(i8* %2)
  br label %sw.epilog

sw.system_initialize:                                           ; preds = %entry
  call void @_ZN4thor4lang12__initializeEv()
  br label %sw.epilog

sw.global_initialize:                                           ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %entry, %sw.global_initialize, %sw.system_initialize, %sw.remote_invocation, %sw.destroy_objects
  ret void
}

define internal void @_ZN12_GLOBAL__N_115destroy_objectsEPc(i8* %parameter_ptr) uwtable {
entry:
  %parameter_ptr.addr = alloca i8*, align 8
  %object_count = alloca i32, align 4
  %objects_ptr = alloca i8**, align 8
  store i8* %parameter_ptr, i8** %parameter_ptr.addr, align 8
  %0 = load i8** %parameter_ptr.addr, align 8
  %1 = bitcast i8* %0 to i32*
  %2 = load i32* %1, align 4
  store i32 %2, i32* %object_count, align 4
  %3 = load i8** %parameter_ptr.addr, align 8
  %add.ptr = getelementptr inbounds i8* %3, i64 4
  %4 = bitcast i8* %add.ptr to i8***
  %5 = load i8*** %4, align 8
  store i8** %5, i8*** %objects_ptr, align 8
  %6 = load i32* %object_count, align 4
  %7 = load i8*** %objects_ptr, align 8
  call void @_ZN4thor4lang23__destroyObjectInternalEiPPc(i32 %6, i8** %7)
  ret void
}

declare void @_ZN4thor4lang15__remoteInvokerEPc(i8*)

declare void @_ZN4thor4lang12__initializeEv()

declare void @_ZN4thor4lang23__destroyObjectInternalEiPPc(i32, i8**)
)SKELETON_CODE"
