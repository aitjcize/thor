/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/processor/cuda/kernel/KernelApi.h"
#include <iostream>
#include <dlfcn.h>

namespace zillians { namespace framework { namespace processor { namespace cuda {


namespace wrapper {

// CUDA Runtime APIs
static cudaError_t CUDARTAPI (*cudaDeviceReset)(void);
static cudaError_t CUDARTAPI (*cudaDeviceSynchronize)(void);
static cudaError_t CUDARTAPI (*cudaDeviceSetLimit)(enum cudaLimit limit, size_t value);
static cudaError_t CUDARTAPI (*cudaDeviceGetLimit)(size_t *pValue, enum cudaLimit limit);
static cudaError_t CUDARTAPI (*cudaDeviceGetCacheConfig)(enum cudaFuncCache *pCacheConfig);
static cudaError_t CUDARTAPI (*cudaDeviceSetCacheConfig)(enum cudaFuncCache cacheConfig);
static cudaError_t CUDARTAPI (*cudaDeviceGetSharedMemConfig)(enum cudaSharedMemConfig *pConfig);
static cudaError_t CUDARTAPI (*cudaDeviceSetSharedMemConfig)(enum cudaSharedMemConfig config);
static cudaError_t CUDARTAPI (*cudaDeviceGetByPCIBusId)(int *device, char *pciBusId);
static cudaError_t CUDARTAPI (*cudaDeviceGetPCIBusId)(char *pciBusId, int len, int device);
static cudaError_t CUDARTAPI (*cudaIpcGetEventHandle)(cudaIpcEventHandle_t *handle, cudaEvent_t event);
static cudaError_t CUDARTAPI (*cudaIpcOpenEventHandle)(cudaEvent_t *event, cudaIpcEventHandle_t handle);
static cudaError_t CUDARTAPI (*cudaIpcGetMemHandle)(cudaIpcMemHandle_t *handle, void *devPtr);
static cudaError_t CUDARTAPI (*cudaIpcOpenMemHandle)(void **devPtr, cudaIpcMemHandle_t handle, unsigned int flags);
static cudaError_t CUDARTAPI (*cudaIpcCloseMemHandle)(void *devPtr);
static cudaError_t CUDARTAPI (*cudaThreadExit)(void);
static cudaError_t CUDARTAPI (*cudaThreadSynchronize)(void);
static cudaError_t CUDARTAPI (*cudaThreadSetLimit)(enum cudaLimit limit, size_t value);
static cudaError_t CUDARTAPI (*cudaThreadGetLimit)(size_t *pValue, enum cudaLimit limit);
static cudaError_t CUDARTAPI (*cudaThreadGetCacheConfig)(enum cudaFuncCache *pCacheConfig);
static cudaError_t CUDARTAPI (*cudaThreadSetCacheConfig)(enum cudaFuncCache cacheConfig);
static cudaError_t CUDARTAPI (*cudaGetLastError)(void);
static cudaError_t CUDARTAPI (*cudaPeekAtLastError)(void);
static const char* CUDARTAPI (*cudaGetErrorString)(cudaError_t error);
static cudaError_t CUDARTAPI (*cudaGetDeviceCount)(int *count);
static cudaError_t CUDARTAPI (*cudaGetDeviceProperties)(struct cudaDeviceProp *prop, int device);
//static cudaError_t CUDARTAPI (*cudaDeviceGetAttribute)(int *value, enum cudaDeviceAttr attr, int device);
static cudaError_t CUDARTAPI (*cudaChooseDevice)(int *device, const struct cudaDeviceProp *prop);
static cudaError_t CUDARTAPI (*cudaSetDevice)(int device);
static cudaError_t CUDARTAPI (*cudaGetDevice)(int *device);
static cudaError_t CUDARTAPI (*cudaSetValidDevices)(int *device_arr, int len);
static cudaError_t CUDARTAPI (*cudaSetDeviceFlags)( unsigned int flags );
static cudaError_t CUDARTAPI (*cudaStreamCreate)(cudaStream_t *pStream);
static cudaError_t CUDARTAPI (*cudaStreamCreateWithFlags)(cudaStream_t *pStream, unsigned int flags);
static cudaError_t CUDARTAPI (*cudaStreamDestroy)(cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaStreamWaitEvent)(cudaStream_t stream, cudaEvent_t event, unsigned int flags);
static cudaError_t CUDARTAPI (*cudaStreamAddCallback)(cudaStream_t stream, cudaStreamCallback_t callback, void *userData, unsigned int flags);
static cudaError_t CUDARTAPI (*cudaStreamSynchronize)(cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaStreamQuery)(cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaEventCreate)(cudaEvent_t *event);
static cudaError_t CUDARTAPI (*cudaEventCreateWithFlags)(cudaEvent_t *event, unsigned int flags);
static cudaError_t CUDARTAPI (*cudaEventRecord)(cudaEvent_t event, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaEventQuery)(cudaEvent_t event);
static cudaError_t CUDARTAPI (*cudaEventSynchronize)(cudaEvent_t event);
static cudaError_t CUDARTAPI (*cudaEventDestroy)(cudaEvent_t event);
static cudaError_t CUDARTAPI (*cudaEventElapsedTime)(float *ms, cudaEvent_t start, cudaEvent_t end);
static cudaError_t CUDARTAPI (*cudaConfigureCall)(dim3 gridDim, dim3 blockDim, size_t sharedMem, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaSetupArgument)(const void *arg, size_t size, size_t offset);
static cudaError_t CUDARTAPI (*cudaFuncSetCacheConfig)(const void *func, enum cudaFuncCache cacheConfig);
static cudaError_t CUDARTAPI (*cudaFuncSetSharedMemConfig)(const void *func, enum cudaSharedMemConfig config);
static cudaError_t CUDARTAPI (*cudaLaunch)(const void *entry);
static cudaError_t CUDARTAPI (*cudaFuncGetAttributes)(struct cudaFuncAttributes *attr, const void *func);
static cudaError_t CUDARTAPI (*cudaSetDoubleForDevice)(double *d);
static cudaError_t CUDARTAPI (*cudaSetDoubleForHost)(double *d);
static cudaError_t CUDARTAPI (*cudaMalloc)(void **devPtr, size_t size);
static cudaError_t CUDARTAPI (*cudaMallocHost)(void **ptr, size_t size);
static cudaError_t CUDARTAPI (*cudaMallocPitch)(void **devPtr, size_t *pitch, size_t width, size_t height);
static cudaError_t CUDARTAPI (*cudaMallocArray)(cudaArray_t *array, const struct cudaChannelFormatDesc *desc, size_t width, size_t height, unsigned int flags);
static cudaError_t CUDARTAPI (*cudaFree)(void *devPtr);
static cudaError_t CUDARTAPI (*cudaFreeHost)(void *ptr);
static cudaError_t CUDARTAPI (*cudaFreeArray)(cudaArray_t array);
static cudaError_t CUDARTAPI (*cudaFreeMipmappedArray)(cudaMipmappedArray_t mipmappedArray);
static cudaError_t CUDARTAPI (*cudaHostAlloc)(void **pHost, size_t size, unsigned int flags);
static cudaError_t CUDARTAPI (*cudaHostRegister)(void *ptr, size_t size, unsigned int flags);
static cudaError_t CUDARTAPI (*cudaHostUnregister)(void *ptr);
static cudaError_t CUDARTAPI (*cudaHostGetDevicePointer)(void **pDevice, void *pHost, unsigned int flags);
static cudaError_t CUDARTAPI (*cudaHostGetFlags)(unsigned int *pFlags, void *pHost);
static cudaError_t CUDARTAPI (*cudaMalloc3D)(struct cudaPitchedPtr* pitchedDevPtr, struct cudaExtent extent);
static cudaError_t CUDARTAPI (*cudaMalloc3DArray)(cudaArray_t *array, const struct cudaChannelFormatDesc* desc, struct cudaExtent extent, unsigned int flags);
static cudaError_t CUDARTAPI (*cudaMallocMipmappedArray)(cudaMipmappedArray_t *mipmappedArray, const struct cudaChannelFormatDesc* desc, struct cudaExtent extent, unsigned int numLevels, unsigned int flags);
static cudaError_t CUDARTAPI (*cudaGetMipmappedArrayLevel)(cudaArray_t *levelArray, cudaMipmappedArray_const_t mipmappedArray, unsigned int level);
static cudaError_t CUDARTAPI (*cudaMemcpy3D)(const struct cudaMemcpy3DParms *p);
static cudaError_t CUDARTAPI (*cudaMemcpy3DPeer)(const struct cudaMemcpy3DPeerParms *p);
static cudaError_t CUDARTAPI (*cudaMemcpy3DAsync)(const struct cudaMemcpy3DParms *p, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaMemcpy3DPeerAsync)(const struct cudaMemcpy3DPeerParms *p, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaMemGetInfo)(size_t *free, size_t *total);
static cudaError_t CUDARTAPI (*cudaArrayGetInfo)(struct cudaChannelFormatDesc *desc, struct cudaExtent *extent, unsigned int *flags, cudaArray_t array);
static cudaError_t CUDARTAPI (*cudaMemcpy)(void *dst, const void *src, size_t count, enum cudaMemcpyKind kind);
static cudaError_t CUDARTAPI (*cudaMemcpyPeer)(void *dst, int dstDevice, const void *src, int srcDevice, size_t count);
static cudaError_t CUDARTAPI (*cudaMemcpyToArray)(cudaArray_t dst, size_t wOffset, size_t hOffset, const void *src, size_t count, enum cudaMemcpyKind kind);
static cudaError_t CUDARTAPI (*cudaMemcpyFromArray)(void *dst, cudaArray_const_t src, size_t wOffset, size_t hOffset, size_t count, enum cudaMemcpyKind kind);
static cudaError_t CUDARTAPI (*cudaMemcpyArrayToArray)(cudaArray_t dst, size_t wOffsetDst, size_t hOffsetDst, cudaArray_const_t src, size_t wOffsetSrc, size_t hOffsetSrc, size_t count, enum cudaMemcpyKind kind);
static cudaError_t CUDARTAPI (*cudaMemcpy2D)(void *dst, size_t dpitch, const void *src, size_t spitch, size_t width, size_t height, enum cudaMemcpyKind kind);
static cudaError_t CUDARTAPI (*cudaMemcpy2DToArray)(cudaArray_t dst, size_t wOffset, size_t hOffset, const void *src, size_t spitch, size_t width, size_t height, enum cudaMemcpyKind kind);
static cudaError_t CUDARTAPI (*cudaMemcpy2DFromArray)(void *dst, size_t dpitch, cudaArray_const_t src, size_t wOffset, size_t hOffset, size_t width, size_t height, enum cudaMemcpyKind kind);
static cudaError_t CUDARTAPI (*cudaMemcpy2DArrayToArray)(cudaArray_t dst, size_t wOffsetDst, size_t hOffsetDst, cudaArray_const_t src, size_t wOffsetSrc, size_t hOffsetSrc, size_t width, size_t height, enum cudaMemcpyKind kind);
static cudaError_t CUDARTAPI (*cudaMemcpyToSymbol)(const void *symbol, const void *src, size_t count, size_t offset, enum cudaMemcpyKind kind);
static cudaError_t CUDARTAPI (*cudaMemcpyFromSymbol)(void *dst, const void *symbol, size_t count, size_t offset, enum cudaMemcpyKind kind);
static cudaError_t CUDARTAPI (*cudaMemcpyAsync)(void *dst, const void *src, size_t count, enum cudaMemcpyKind kind, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaMemcpyPeerAsync)(void *dst, int dstDevice, const void *src, int srcDevice, size_t count, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaMemcpyToArrayAsync)(cudaArray_t dst, size_t wOffset, size_t hOffset, const void *src, size_t count, enum cudaMemcpyKind kind, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaMemcpyFromArrayAsync)(void *dst, cudaArray_const_t src, size_t wOffset, size_t hOffset, size_t count, enum cudaMemcpyKind kind, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaMemcpy2DAsync)(void *dst, size_t dpitch, const void *src, size_t spitch, size_t width, size_t height, enum cudaMemcpyKind kind, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaMemcpy2DToArrayAsync)(cudaArray_t dst, size_t wOffset, size_t hOffset, const void *src, size_t spitch, size_t width, size_t height, enum cudaMemcpyKind kind, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaMemcpy2DFromArrayAsync)(void *dst, size_t dpitch, cudaArray_const_t src, size_t wOffset, size_t hOffset, size_t width, size_t height, enum cudaMemcpyKind kind, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaMemcpyToSymbolAsync)(const void *symbol, const void *src, size_t count, size_t offset, enum cudaMemcpyKind kind, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaMemcpyFromSymbolAsync)(void *dst, const void *symbol, size_t count, size_t offset, enum cudaMemcpyKind kind, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaMemset)(void *devPtr, int value, size_t count);
static cudaError_t CUDARTAPI (*cudaMemset2D)(void *devPtr, size_t pitch, int value, size_t width, size_t height);
static cudaError_t CUDARTAPI (*cudaMemset3D)(struct cudaPitchedPtr pitchedDevPtr, int value, struct cudaExtent extent);
static cudaError_t CUDARTAPI (*cudaMemsetAsync)(void *devPtr, int value, size_t count, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaMemset2DAsync)(void *devPtr, size_t pitch, int value, size_t width, size_t height, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaMemset3DAsync)(struct cudaPitchedPtr pitchedDevPtr, int value, struct cudaExtent extent, cudaStream_t stream);
static cudaError_t CUDARTAPI (*cudaGetSymbolAddress)(void **devPtr, const void *symbol);
static cudaError_t CUDARTAPI (*cudaGetSymbolSize)(size_t *size, const void *symbol);
static cudaError_t CUDARTAPI (*cudaPointerGetAttributes)(struct cudaPointerAttributes *attributes, const void *ptr);
static cudaError_t CUDARTAPI (*cudaDeviceCanAccessPeer)(int *canAccessPeer, int device, int peerDevice);
static cudaError_t CUDARTAPI (*cudaDeviceEnablePeerAccess)(int peerDevice, unsigned int flags);
static cudaError_t CUDARTAPI (*cudaDeviceDisablePeerAccess)(int peerDevice);
static cudaError_t CUDARTAPI (*cudaGetChannelDesc)(struct cudaChannelFormatDesc *desc, cudaArray_const_t array);
static struct cudaChannelFormatDesc CUDARTAPI (*cudaCreateChannelDesc)(int x, int y, int z, int w, enum cudaChannelFormatKind f);
static cudaError_t CUDARTAPI (*cudaBindTexture)(size_t *offset, const struct textureReference *texref, const void *devPtr, const struct cudaChannelFormatDesc *desc, size_t size);
static cudaError_t CUDARTAPI (*cudaBindTexture2D)(size_t *offset, const struct textureReference *texref, const void *devPtr, const struct cudaChannelFormatDesc *desc, size_t width, size_t height, size_t pitch);
static cudaError_t CUDARTAPI (*cudaBindTextureToArray)(const struct textureReference *texref, cudaArray_const_t array, const struct cudaChannelFormatDesc *desc);
static cudaError_t CUDARTAPI (*cudaBindTextureToMipmappedArray)(const struct textureReference *texref, cudaMipmappedArray_const_t mipmappedArray, const struct cudaChannelFormatDesc *desc);
static cudaError_t CUDARTAPI (*cudaUnbindTexture)(const struct textureReference *texref);
static cudaError_t CUDARTAPI (*cudaGetTextureAlignmentOffset)(size_t *offset, const struct textureReference *texref);
static cudaError_t CUDARTAPI (*cudaGetTextureReference)(const struct textureReference **texref, const void *symbol);
static cudaError_t CUDARTAPI (*cudaBindSurfaceToArray)(const struct surfaceReference *surfref, cudaArray_const_t array, const struct cudaChannelFormatDesc *desc);
static cudaError_t CUDARTAPI (*cudaGetSurfaceReference)(const struct surfaceReference **surfref, const void *symbol);
static cudaError_t CUDARTAPI (*cudaCreateTextureObject)(cudaTextureObject_t *pTexObject, const struct cudaResourceDesc *pResDesc, const struct cudaTextureDesc *pTexDesc, const struct cudaResourceViewDesc *pResViewDesc);
static cudaError_t CUDARTAPI (*cudaDestroyTextureObject)(cudaTextureObject_t texObject);
static cudaError_t CUDARTAPI (*cudaGetTextureObjectResourceDesc)(struct cudaResourceDesc *pResDesc, cudaTextureObject_t texObject);
static cudaError_t CUDARTAPI (*cudaGetTextureObjectTextureDesc)(struct cudaTextureDesc *pTexDesc, cudaTextureObject_t texObject);
static cudaError_t CUDARTAPI (*cudaGetTextureObjectResourceViewDesc)(struct cudaResourceViewDesc *pResViewDesc, cudaTextureObject_t texObject);
static cudaError_t CUDARTAPI (*cudaCreateSurfaceObject)(cudaSurfaceObject_t *pSurfObject, const struct cudaResourceDesc *pResDesc);
static cudaError_t CUDARTAPI (*cudaDestroySurfaceObject)(cudaSurfaceObject_t surfObject);
static cudaError_t CUDARTAPI (*cudaGetSurfaceObjectResourceDesc)(struct cudaResourceDesc *pResDesc, cudaSurfaceObject_t surfObject);
static cudaError_t CUDARTAPI (*cudaDriverGetVersion)(int *driverVersion);
static cudaError_t CUDARTAPI (*cudaRuntimeGetVersion)(int *runtimeVersion);
static cudaError_t CUDARTAPI (*cudaGetExportTable)(const void **ppExportTable, const cudaUUID_t *pExportTableId);

// CUDA Driver APIs
static CUresult CUDAAPI (*cuInit)(unsigned int Flags);
static CUresult CUDAAPI (*cuDriverGetVersion)(int *driverVersion);
static CUresult CUDAAPI (*cuDeviceGet)(CUdevice *device, int ordinal);
static CUresult CUDAAPI (*cuDeviceGetCount)(int *count);
static CUresult CUDAAPI (*cuDeviceGetName)(char *name, int len, CUdevice dev);
static CUresult CUDAAPI (*cuDeviceTotalMem)(size_t *bytes, CUdevice dev);
static CUresult CUDAAPI (*cuDeviceGetAttribute)(int *pi, CUdevice_attribute attrib, CUdevice dev);
static CUresult CUDAAPI (*cuDeviceGetProperties)(CUdevprop *prop, CUdevice dev);
static CUresult CUDAAPI (*cuDeviceComputeCapability)(int *major, int *minor, CUdevice dev);
static CUresult CUDAAPI (*cuCtxCreate)(CUcontext *pctx, unsigned int flags, CUdevice dev);
static CUresult CUDAAPI (*cuCtxDestroy)(CUcontext ctx);
static CUresult CUDAAPI (*cuCtxPushCurrent)(CUcontext ctx);
static CUresult CUDAAPI (*cuCtxPopCurrent)(CUcontext *pctx);
static CUresult CUDAAPI (*cuCtxSetCurrent)(CUcontext ctx);
static CUresult CUDAAPI (*cuCtxGetCurrent)(CUcontext *pctx);
static CUresult CUDAAPI (*cuCtxGetDevice)(CUdevice *device);
static CUresult CUDAAPI (*cuCtxSynchronize)(void);
static CUresult CUDAAPI (*cuCtxSetLimit)(CUlimit limit, size_t value);
static CUresult CUDAAPI (*cuCtxGetLimit)(size_t *pvalue, CUlimit limit);
static CUresult CUDAAPI (*cuCtxGetCacheConfig)(CUfunc_cache *pconfig);
static CUresult CUDAAPI (*cuCtxSetCacheConfig)(CUfunc_cache config);
static CUresult CUDAAPI (*cuCtxGetSharedMemConfig)(CUsharedconfig *pConfig);
static CUresult CUDAAPI (*cuCtxSetSharedMemConfig)(CUsharedconfig config);
static CUresult CUDAAPI (*cuCtxGetApiVersion)(CUcontext ctx, unsigned int *version);
static CUresult CUDAAPI (*cuCtxAttach)(CUcontext *pctx, unsigned int flags);
static CUresult CUDAAPI (*cuCtxDetach)(CUcontext ctx);
static CUresult CUDAAPI (*cuModuleLoad)(CUmodule *module, const char *fname);
static CUresult CUDAAPI (*cuModuleLoadData)(CUmodule *module, const void *image);
static CUresult CUDAAPI (*cuModuleLoadDataEx)(CUmodule *module, const void *image, unsigned int numOptions, CUjit_option *options, void **optionValues);
static CUresult CUDAAPI (*cuModuleLoadFatBinary)(CUmodule *module, const void *fatCubin);
static CUresult CUDAAPI (*cuModuleUnload)(CUmodule hmod);
static CUresult CUDAAPI (*cuModuleGetFunction)(CUfunction *hfunc, CUmodule hmod, const char *name);
static CUresult CUDAAPI (*cuModuleGetGlobal)(CUdeviceptr *dptr, size_t *bytes, CUmodule hmod, const char *name);
static CUresult CUDAAPI (*cuModuleGetTexRef)(CUtexref *pTexRef, CUmodule hmod, const char *name);
static CUresult CUDAAPI (*cuModuleGetSurfRef)(CUsurfref *pSurfRef, CUmodule hmod, const char *name);
static CUresult CUDAAPI (*cuMemGetInfo)(size_t *free, size_t *total);
static CUresult CUDAAPI (*cuMemAlloc)(CUdeviceptr *dptr, size_t bytesize);
static CUresult CUDAAPI (*cuMemAllocPitch)(CUdeviceptr *dptr, size_t *pPitch, size_t WidthInBytes, size_t Height, unsigned int ElementSizeBytes);
static CUresult CUDAAPI (*cuMemFree)(CUdeviceptr dptr);
static CUresult CUDAAPI (*cuMemGetAddressRange)(CUdeviceptr *pbase, size_t *psize, CUdeviceptr dptr);
static CUresult CUDAAPI (*cuMemAllocHost)(void **pp, size_t bytesize);
static CUresult CUDAAPI (*cuMemFreeHost)(void *p);
static CUresult CUDAAPI (*cuMemHostAlloc)(void **pp, size_t bytesize, unsigned int Flags);
static CUresult CUDAAPI (*cuMemHostGetDevicePointer)(CUdeviceptr *pdptr, void *p, unsigned int Flags);
static CUresult CUDAAPI (*cuMemHostGetFlags)(unsigned int *pFlags, void *p);
static CUresult CUDAAPI (*cuDeviceGetByPCIBusId)(CUdevice *dev, char *pciBusId);
static CUresult CUDAAPI (*cuDeviceGetPCIBusId)(char *pciBusId, int len, CUdevice dev);
static CUresult CUDAAPI (*cuIpcGetEventHandle)(CUipcEventHandle *pHandle, CUevent event);
static CUresult CUDAAPI (*cuIpcOpenEventHandle)(CUevent *phEvent, CUipcEventHandle handle);
static CUresult CUDAAPI (*cuIpcGetMemHandle)(CUipcMemHandle *pHandle, CUdeviceptr dptr);
static CUresult CUDAAPI (*cuIpcOpenMemHandle)(CUdeviceptr *pdptr, CUipcMemHandle handle, unsigned int Flags);
static CUresult CUDAAPI (*cuIpcCloseMemHandle)(CUdeviceptr dptr);
static CUresult CUDAAPI (*cuMemHostRegister)(void *p, size_t bytesize, unsigned int Flags);
static CUresult CUDAAPI (*cuMemHostUnregister)(void *p);
static CUresult CUDAAPI (*cuMemcpy)(CUdeviceptr dst, CUdeviceptr src, size_t ByteCount);
static CUresult CUDAAPI (*cuMemcpyPeer)(CUdeviceptr dstDevice, CUcontext dstContext, CUdeviceptr srcDevice, CUcontext srcContext, size_t ByteCount);
static CUresult CUDAAPI (*cuMemcpyHtoD)(CUdeviceptr dstDevice, const void *srcHost, size_t ByteCount);
static CUresult CUDAAPI (*cuMemcpyDtoH)(void *dstHost, CUdeviceptr srcDevice, size_t ByteCount);
static CUresult CUDAAPI (*cuMemcpyDtoD)(CUdeviceptr dstDevice, CUdeviceptr srcDevice, size_t ByteCount);
static CUresult CUDAAPI (*cuMemcpyDtoA)(CUarray dstArray, size_t dstOffset, CUdeviceptr srcDevice, size_t ByteCount);
static CUresult CUDAAPI (*cuMemcpyAtoD)(CUdeviceptr dstDevice, CUarray srcArray, size_t srcOffset, size_t ByteCount);
static CUresult CUDAAPI (*cuMemcpyHtoA)(CUarray dstArray, size_t dstOffset, const void *srcHost, size_t ByteCount);
static CUresult CUDAAPI (*cuMemcpyAtoH)(void *dstHost, CUarray srcArray, size_t srcOffset, size_t ByteCount);
static CUresult CUDAAPI (*cuMemcpyAtoA)(CUarray dstArray, size_t dstOffset, CUarray srcArray, size_t srcOffset, size_t ByteCount);
static CUresult CUDAAPI (*cuMemcpy2D)(const CUDA_MEMCPY2D *pCopy);
static CUresult CUDAAPI (*cuMemcpy2DUnaligned)(const CUDA_MEMCPY2D *pCopy);
static CUresult CUDAAPI (*cuMemcpy3D)(const CUDA_MEMCPY3D *pCopy);
static CUresult CUDAAPI (*cuMemcpy3DPeer)(const CUDA_MEMCPY3D_PEER *pCopy);
static CUresult CUDAAPI (*cuMemcpyAsync)(CUdeviceptr dst, CUdeviceptr src, size_t ByteCount, CUstream hStream);
static CUresult CUDAAPI (*cuMemcpyPeerAsync)(CUdeviceptr dstDevice, CUcontext dstContext, CUdeviceptr srcDevice, CUcontext srcContext, size_t ByteCount, CUstream hStream);
static CUresult CUDAAPI (*cuMemcpyHtoDAsync)(CUdeviceptr dstDevice, const void *srcHost, size_t ByteCount, CUstream hStream);
static CUresult CUDAAPI (*cuMemcpyDtoHAsync)(void *dstHost, CUdeviceptr srcDevice, size_t ByteCount, CUstream hStream);
static CUresult CUDAAPI (*cuMemcpyDtoDAsync)(CUdeviceptr dstDevice, CUdeviceptr srcDevice, size_t ByteCount, CUstream hStream);
static CUresult CUDAAPI (*cuMemcpyHtoAAsync)(CUarray dstArray, size_t dstOffset, const void *srcHost, size_t ByteCount, CUstream hStream);
static CUresult CUDAAPI (*cuMemcpyAtoHAsync)(void *dstHost, CUarray srcArray, size_t srcOffset, size_t ByteCount, CUstream hStream);
static CUresult CUDAAPI (*cuMemcpy2DAsync)(const CUDA_MEMCPY2D *pCopy, CUstream hStream);
static CUresult CUDAAPI (*cuMemcpy3DAsync)(const CUDA_MEMCPY3D *pCopy, CUstream hStream);
static CUresult CUDAAPI (*cuMemcpy3DPeerAsync)(const CUDA_MEMCPY3D_PEER *pCopy, CUstream hStream);
static CUresult CUDAAPI (*cuMemsetD8)(CUdeviceptr dstDevice, unsigned char uc, size_t N);
static CUresult CUDAAPI (*cuMemsetD16)(CUdeviceptr dstDevice, unsigned short us, size_t N);
static CUresult CUDAAPI (*cuMemsetD32)(CUdeviceptr dstDevice, unsigned int ui, size_t N);
static CUresult CUDAAPI (*cuMemsetD2D8)(CUdeviceptr dstDevice, size_t dstPitch, unsigned char uc, size_t Width, size_t Height);
static CUresult CUDAAPI (*cuMemsetD2D16)(CUdeviceptr dstDevice, size_t dstPitch, unsigned short us, size_t Width, size_t Height);
static CUresult CUDAAPI (*cuMemsetD2D32)(CUdeviceptr dstDevice, size_t dstPitch, unsigned int ui, size_t Width, size_t Height);
static CUresult CUDAAPI (*cuMemsetD8Async)(CUdeviceptr dstDevice, unsigned char uc, size_t N, CUstream hStream);
static CUresult CUDAAPI (*cuMemsetD16Async)(CUdeviceptr dstDevice, unsigned short us, size_t N, CUstream hStream);
static CUresult CUDAAPI (*cuMemsetD32Async)(CUdeviceptr dstDevice, unsigned int ui, size_t N, CUstream hStream);
static CUresult CUDAAPI (*cuMemsetD2D8Async)(CUdeviceptr dstDevice, size_t dstPitch, unsigned char uc, size_t Width, size_t Height, CUstream hStream);
static CUresult CUDAAPI (*cuMemsetD2D16Async)(CUdeviceptr dstDevice, size_t dstPitch, unsigned short us, size_t Width, size_t Height, CUstream hStream);
static CUresult CUDAAPI (*cuMemsetD2D32Async)(CUdeviceptr dstDevice, size_t dstPitch, unsigned int ui, size_t Width, size_t Height, CUstream hStream);
static CUresult CUDAAPI (*cuArrayCreate)(CUarray *pHandle, const CUDA_ARRAY_DESCRIPTOR *pAllocateArray);
static CUresult CUDAAPI (*cuArrayGetDescriptor)(CUDA_ARRAY_DESCRIPTOR *pArrayDescriptor, CUarray hArray);
static CUresult CUDAAPI (*cuArrayDestroy)(CUarray hArray);
static CUresult CUDAAPI (*cuArray3DCreate)(CUarray *pHandle, const CUDA_ARRAY3D_DESCRIPTOR *pAllocateArray);
static CUresult CUDAAPI (*cuArray3DGetDescriptor)(CUDA_ARRAY3D_DESCRIPTOR *pArrayDescriptor, CUarray hArray);
static CUresult CUDAAPI (*cuMipmappedArrayCreate)(CUmipmappedArray *pHandle, const CUDA_ARRAY3D_DESCRIPTOR *pMipmappedArrayDesc, unsigned int numMipmapLevels);
static CUresult CUDAAPI (*cuMipmappedArrayGetLevel)(CUarray *pLevelArray, CUmipmappedArray hMipmappedArray, unsigned int level);
static CUresult CUDAAPI (*cuMipmappedArrayDestroy)(CUmipmappedArray hMipmappedArray);
static CUresult CUDAAPI (*cuPointerGetAttribute)(void *data, CUpointer_attribute attribute, CUdeviceptr ptr);
static CUresult CUDAAPI (*cuStreamCreate)(CUstream *phStream, unsigned int Flags);
static CUresult CUDAAPI (*cuStreamWaitEvent)(CUstream hStream, CUevent hEvent, unsigned int Flags);
static CUresult CUDAAPI (*cuStreamAddCallback)(CUstream hStream, CUstreamCallback callback, void *userData, unsigned int flags);
static CUresult CUDAAPI (*cuStreamQuery)(CUstream hStream);
static CUresult CUDAAPI (*cuStreamSynchronize)(CUstream hStream);
static CUresult CUDAAPI (*cuStreamDestroy)(CUstream hStream);
static CUresult CUDAAPI (*cuEventCreate)(CUevent *phEvent, unsigned int Flags);
static CUresult CUDAAPI (*cuEventRecord)(CUevent hEvent, CUstream hStream);
static CUresult CUDAAPI (*cuEventQuery)(CUevent hEvent);
static CUresult CUDAAPI (*cuEventSynchronize)(CUevent hEvent);
static CUresult CUDAAPI (*cuEventDestroy)(CUevent hEvent);
static CUresult CUDAAPI (*cuEventElapsedTime)(float *pMilliseconds, CUevent hStart, CUevent hEnd);
static CUresult CUDAAPI (*cuFuncGetAttribute)(int *pi, CUfunction_attribute attrib, CUfunction hfunc);
static CUresult CUDAAPI (*cuFuncSetCacheConfig)(CUfunction hfunc, CUfunc_cache config);
static CUresult CUDAAPI (*cuFuncSetSharedMemConfig)(CUfunction hfunc, CUsharedconfig config);
static CUresult CUDAAPI (*cuLaunchKernel)(CUfunction f,
                                unsigned int gridDimX,
                                unsigned int gridDimY,
                                unsigned int gridDimZ,
                                unsigned int blockDimX,
                                unsigned int blockDimY,
                                unsigned int blockDimZ,
                                unsigned int sharedMemBytes,
                                CUstream hStream,
                                void **kernelParams,
                                void **extra);
static CUresult CUDAAPI (*cuFuncSetBlockShape)(CUfunction hfunc, int x, int y, int z);
static CUresult CUDAAPI (*cuFuncSetSharedSize)(CUfunction hfunc, unsigned int bytes);
static CUresult CUDAAPI (*cuParamSetSize)(CUfunction hfunc, unsigned int numbytes);
static CUresult CUDAAPI (*cuParamSeti)(CUfunction hfunc, int offset, unsigned int value);
static CUresult CUDAAPI (*cuParamSetf)(CUfunction hfunc, int offset, float value);
static CUresult CUDAAPI (*cuParamSetv)(CUfunction hfunc, int offset, void *ptr, unsigned int numbytes);
static CUresult CUDAAPI (*cuLaunch)(CUfunction f);
static CUresult CUDAAPI (*cuLaunchGrid)(CUfunction f, int grid_width, int grid_height);
static CUresult CUDAAPI (*cuLaunchGridAsync)(CUfunction f, int grid_width, int grid_height, CUstream hStream);
static CUresult CUDAAPI (*cuParamSetTexRef)(CUfunction hfunc, int texunit, CUtexref hTexRef);
static CUresult CUDAAPI (*cuTexRefSetArray)(CUtexref hTexRef, CUarray hArray, unsigned int Flags);
static CUresult CUDAAPI (*cuTexRefSetMipmappedArray)(CUtexref hTexRef, CUmipmappedArray hMipmappedArray, unsigned int Flags);
static CUresult CUDAAPI (*cuTexRefSetAddress)(size_t *ByteOffset, CUtexref hTexRef, CUdeviceptr dptr, size_t bytes);
static CUresult CUDAAPI (*cuTexRefSetAddress2D)(CUtexref hTexRef, const CUDA_ARRAY_DESCRIPTOR *desc, CUdeviceptr dptr, size_t Pitch);
static CUresult CUDAAPI (*cuTexRefSetFormat)(CUtexref hTexRef, CUarray_format fmt, int NumPackedComponents);
static CUresult CUDAAPI (*cuTexRefSetAddressMode)(CUtexref hTexRef, int dim, CUaddress_mode am);
static CUresult CUDAAPI (*cuTexRefSetFilterMode)(CUtexref hTexRef, CUfilter_mode fm);
static CUresult CUDAAPI (*cuTexRefSetMipmapFilterMode)(CUtexref hTexRef, CUfilter_mode fm);
static CUresult CUDAAPI (*cuTexRefSetMipmapLevelBias)(CUtexref hTexRef, float bias);
static CUresult CUDAAPI (*cuTexRefSetMipmapLevelClamp)(CUtexref hTexRef, float minMipmapLevelClamp, float maxMipmapLevelClamp);
static CUresult CUDAAPI (*cuTexRefSetMaxAnisotropy)(CUtexref hTexRef, unsigned int maxAniso);
static CUresult CUDAAPI (*cuTexRefSetFlags)(CUtexref hTexRef, unsigned int Flags);
static CUresult CUDAAPI (*cuTexRefGetAddress)(CUdeviceptr *pdptr, CUtexref hTexRef);
static CUresult CUDAAPI (*cuTexRefGetArray)(CUarray *phArray, CUtexref hTexRef);
static CUresult CUDAAPI (*cuTexRefGetMipmappedArray)(CUmipmappedArray *phMipmappedArray, CUtexref hTexRef);
static CUresult CUDAAPI (*cuTexRefGetAddressMode)(CUaddress_mode *pam, CUtexref hTexRef, int dim);
static CUresult CUDAAPI (*cuTexRefGetFilterMode)(CUfilter_mode *pfm, CUtexref hTexRef);
static CUresult CUDAAPI (*cuTexRefGetFormat)(CUarray_format *pFormat, int *pNumChannels, CUtexref hTexRef);
static CUresult CUDAAPI (*cuTexRefGetMipmapFilterMode)(CUfilter_mode *pfm, CUtexref hTexRef);
static CUresult CUDAAPI (*cuTexRefGetMipmapLevelBias)(float *pbias, CUtexref hTexRef);
static CUresult CUDAAPI (*cuTexRefGetMipmapLevelClamp)(float *pminMipmapLevelClamp, float *pmaxMipmapLevelClamp, CUtexref hTexRef);
static CUresult CUDAAPI (*cuTexRefGetMaxAnisotropy)(int *pmaxAniso, CUtexref hTexRef);
static CUresult CUDAAPI (*cuTexRefGetFlags)(unsigned int *pFlags, CUtexref hTexRef);
static CUresult CUDAAPI (*cuTexRefCreate)(CUtexref *pTexRef);
static CUresult CUDAAPI (*cuTexRefDestroy)(CUtexref hTexRef);
static CUresult CUDAAPI (*cuSurfRefSetArray)(CUsurfref hSurfRef, CUarray hArray, unsigned int Flags);
static CUresult CUDAAPI (*cuSurfRefGetArray)(CUarray *phArray, CUsurfref hSurfRef);
static CUresult CUDAAPI (*cuTexObjectCreate)(CUtexObject *pTexObject, const CUDA_RESOURCE_DESC *pResDesc, const CUDA_TEXTURE_DESC *pTexDesc, const CUDA_RESOURCE_VIEW_DESC *pResViewDesc);
static CUresult CUDAAPI (*cuTexObjectDestroy)(CUtexObject texObject);
static CUresult CUDAAPI (*cuTexObjectGetResourceDesc)(CUDA_RESOURCE_DESC *pResDesc, CUtexObject texObject);
static CUresult CUDAAPI (*cuTexObjectGetTextureDesc)(CUDA_TEXTURE_DESC *pTexDesc, CUtexObject texObject);
static CUresult CUDAAPI (*cuTexObjectGetResourceViewDesc)(CUDA_RESOURCE_VIEW_DESC *pResViewDesc, CUtexObject texObject);
static CUresult CUDAAPI (*cuSurfObjectCreate)(CUsurfObject *pSurfObject, const CUDA_RESOURCE_DESC *pResDesc);
static CUresult CUDAAPI (*cuSurfObjectDestroy)(CUsurfObject surfObject);
static CUresult CUDAAPI (*cuSurfObjectGetResourceDesc)(CUDA_RESOURCE_DESC *pResDesc, CUsurfObject surfObject);
static CUresult CUDAAPI (*cuDeviceCanAccessPeer)(int *canAccessPeer, CUdevice dev, CUdevice peerDev);
static CUresult CUDAAPI (*cuCtxEnablePeerAccess)(CUcontext peerContext, unsigned int Flags);
static CUresult CUDAAPI (*cuCtxDisablePeerAccess)(CUcontext peerContext);
static CUresult CUDAAPI (*cuGetExportTable)(const void **ppExportTable, const CUuuid *pExportTableId);

}

const KernelApi::stream_type KernelApi::default_stream = 0;

KernelApi::KernelApi() : cuda_lib_handle(NULL), cuda_rt_lib_handle(NULL)
{
	initialize();
}

KernelApi::~KernelApi()
{
	finalize();
}

bool KernelApi::good()
{
	if(cuda_lib_handle && cuda_rt_lib_handle)
		return true;
	return false;
}

bool KernelApi::initialize()
{
	cuda_lib_handle = dlopen("libcuda.so", RTLD_LAZY | RTLD_LOCAL);
	cuda_rt_lib_handle = dlopen("libcudart.so", RTLD_LAZY | RTLD_LOCAL);
	if(!cuda_lib_handle || !cuda_rt_lib_handle)
	{
		if(!cuda_lib_handle) dlclose(cuda_lib_handle);
		if(!cuda_rt_lib_handle) dlclose(cuda_rt_lib_handle);
		return false;
	}

	wrapper::cudaDeviceReset = (decltype(wrapper::cudaDeviceReset))dlsym(cuda_rt_lib_handle, "cudaDeviceReset");
	wrapper::cudaDeviceSynchronize = (decltype(wrapper::cudaDeviceSynchronize))dlsym(cuda_rt_lib_handle, "cudaDeviceSynchronize");
	wrapper::cudaDeviceSetLimit = (decltype(wrapper::cudaDeviceSetLimit))dlsym(cuda_rt_lib_handle, "cudaDeviceSetLimit");
	wrapper::cudaDeviceGetLimit = (decltype(wrapper::cudaDeviceGetLimit))dlsym(cuda_rt_lib_handle, "cudaDeviceGetLimit");
	wrapper::cudaDeviceGetCacheConfig = (decltype(wrapper::cudaDeviceGetCacheConfig))dlsym(cuda_rt_lib_handle, "cudaDeviceGetCacheConfig");
	wrapper::cudaDeviceSetCacheConfig = (decltype(wrapper::cudaDeviceSetCacheConfig))dlsym(cuda_rt_lib_handle, "cudaDeviceSetCacheConfig");
	wrapper::cudaDeviceGetSharedMemConfig = (decltype(wrapper::cudaDeviceGetSharedMemConfig))dlsym(cuda_rt_lib_handle, "cudaDeviceGetSharedMemConfig");
	wrapper::cudaDeviceSetSharedMemConfig = (decltype(wrapper::cudaDeviceSetSharedMemConfig))dlsym(cuda_rt_lib_handle, "cudaDeviceSetSharedMemConfig");
	wrapper::cudaDeviceGetByPCIBusId = (decltype(wrapper::cudaDeviceGetByPCIBusId))dlsym(cuda_rt_lib_handle, "cudaDeviceGetByPCIBusId");
	wrapper::cudaDeviceGetPCIBusId = (decltype(wrapper::cudaDeviceGetPCIBusId))dlsym(cuda_rt_lib_handle, "cudaDeviceGetPCIBusId");
	wrapper::cudaIpcGetEventHandle = (decltype(wrapper::cudaIpcGetEventHandle))dlsym(cuda_rt_lib_handle, "cudaIpcGetEventHandle");
	wrapper::cudaIpcOpenEventHandle = (decltype(wrapper::cudaIpcOpenEventHandle))dlsym(cuda_rt_lib_handle, "cudaIpcOpenEventHandle");
	wrapper::cudaIpcGetMemHandle = (decltype(wrapper::cudaIpcGetMemHandle))dlsym(cuda_rt_lib_handle, "cudaIpcGetMemHandle");
	wrapper::cudaIpcOpenMemHandle = (decltype(wrapper::cudaIpcOpenMemHandle))dlsym(cuda_rt_lib_handle, "cudaIpcOpenMemHandle");
	wrapper::cudaIpcCloseMemHandle = (decltype(wrapper::cudaIpcCloseMemHandle))dlsym(cuda_rt_lib_handle, "cudaIpcCloseMemHandle");
	wrapper::cudaThreadExit = (decltype(wrapper::cudaThreadExit))dlsym(cuda_rt_lib_handle, "cudaThreadExit");
	wrapper::cudaThreadSynchronize = (decltype(wrapper::cudaThreadSynchronize))dlsym(cuda_rt_lib_handle, "cudaThreadSynchronize");
	wrapper::cudaThreadSetLimit = (decltype(wrapper::cudaThreadSetLimit))dlsym(cuda_rt_lib_handle, "cudaThreadSetLimit");
	wrapper::cudaThreadGetLimit = (decltype(wrapper::cudaThreadGetLimit))dlsym(cuda_rt_lib_handle, "cudaThreadGetLimit");
	wrapper::cudaThreadGetCacheConfig = (decltype(wrapper::cudaThreadGetCacheConfig))dlsym(cuda_rt_lib_handle, "cudaThreadGetCacheConfig");
	wrapper::cudaThreadSetCacheConfig = (decltype(wrapper::cudaThreadSetCacheConfig))dlsym(cuda_rt_lib_handle, "cudaThreadSetCacheConfig");
	wrapper::cudaGetLastError = (decltype(wrapper::cudaGetLastError))dlsym(cuda_rt_lib_handle, "cudaGetLastError");
	wrapper::cudaPeekAtLastError = (decltype(wrapper::cudaPeekAtLastError))dlsym(cuda_rt_lib_handle, "cudaPeekAtLastError");
	wrapper::cudaGetErrorString = (decltype(wrapper::cudaGetErrorString))dlsym(cuda_rt_lib_handle, "cudaGetErrorString");
	wrapper::cudaGetDeviceCount = (decltype(wrapper::cudaGetDeviceCount))dlsym(cuda_rt_lib_handle, "cudaGetDeviceCount");
	wrapper::cudaGetDeviceProperties = (decltype(wrapper::cudaGetDeviceProperties))dlsym(cuda_rt_lib_handle, "cudaGetDeviceProperties");
	//wrapper::cudaDeviceGetAttribute = (decltype(wrapper::cudaDeviceGetAttribute))dlsym(cuda_rt_lib_handle, "cudaDeviceGetAttribute");
	wrapper::cudaChooseDevice = (decltype(wrapper::cudaChooseDevice))dlsym(cuda_rt_lib_handle, "cudaChooseDevice");
	wrapper::cudaSetDevice = (decltype(wrapper::cudaSetDevice))dlsym(cuda_rt_lib_handle, "cudaSetDevice");
	wrapper::cudaGetDevice = (decltype(wrapper::cudaGetDevice))dlsym(cuda_rt_lib_handle, "cudaGetDevice");
	wrapper::cudaSetValidDevices = (decltype(wrapper::cudaSetValidDevices))dlsym(cuda_rt_lib_handle, "cudaSetValidDevices");
	wrapper::cudaSetDeviceFlags = (decltype(wrapper::cudaSetDeviceFlags))dlsym(cuda_rt_lib_handle, "cudaSetDeviceFlags");
	wrapper::cudaStreamCreate = (decltype(wrapper::cudaStreamCreate))dlsym(cuda_rt_lib_handle, "cudaStreamCreate");
	wrapper::cudaStreamCreateWithFlags = (decltype(wrapper::cudaStreamCreateWithFlags))dlsym(cuda_rt_lib_handle, "cudaStreamCreateWithFlags");
	wrapper::cudaStreamDestroy = (decltype(wrapper::cudaStreamDestroy))dlsym(cuda_rt_lib_handle, "cudaStreamDestroy");
	wrapper::cudaStreamWaitEvent = (decltype(wrapper::cudaStreamWaitEvent))dlsym(cuda_rt_lib_handle, "cudaStreamWaitEvent");
	wrapper::cudaStreamAddCallback = (decltype(wrapper::cudaStreamAddCallback))dlsym(cuda_rt_lib_handle, "cudaStreamAddCallback");
	wrapper::cudaStreamSynchronize = (decltype(wrapper::cudaStreamSynchronize))dlsym(cuda_rt_lib_handle, "cudaStreamSynchronize");
	wrapper::cudaStreamQuery = (decltype(wrapper::cudaStreamQuery))dlsym(cuda_rt_lib_handle, "cudaStreamQuery");
	wrapper::cudaEventCreate = (decltype(wrapper::cudaEventCreate))dlsym(cuda_rt_lib_handle, "cudaEventCreate");
	wrapper::cudaEventCreateWithFlags = (decltype(wrapper::cudaEventCreateWithFlags))dlsym(cuda_rt_lib_handle, "cudaEventCreateWithFlags");
	wrapper::cudaEventRecord = (decltype(wrapper::cudaEventRecord))dlsym(cuda_rt_lib_handle, "cudaEventRecord");
	wrapper::cudaEventQuery = (decltype(wrapper::cudaEventQuery))dlsym(cuda_rt_lib_handle, "cudaEventQuery");
	wrapper::cudaEventSynchronize = (decltype(wrapper::cudaEventSynchronize))dlsym(cuda_rt_lib_handle, "cudaEventSynchronize");
	wrapper::cudaEventDestroy = (decltype(wrapper::cudaEventDestroy))dlsym(cuda_rt_lib_handle, "cudaEventDestroy");
	wrapper::cudaEventElapsedTime = (decltype(wrapper::cudaEventElapsedTime))dlsym(cuda_rt_lib_handle, "cudaEventElapsedTime");
	wrapper::cudaConfigureCall = (decltype(wrapper::cudaConfigureCall))dlsym(cuda_rt_lib_handle, "cudaConfigureCall");
	wrapper::cudaSetupArgument = (decltype(wrapper::cudaSetupArgument))dlsym(cuda_rt_lib_handle, "cudaSetupArgument");
	wrapper::cudaFuncSetCacheConfig = (decltype(wrapper::cudaFuncSetCacheConfig))dlsym(cuda_rt_lib_handle, "cudaFuncSetCacheConfig");
	wrapper::cudaFuncSetSharedMemConfig = (decltype(wrapper::cudaFuncSetSharedMemConfig))dlsym(cuda_rt_lib_handle, "cudaFuncSetSharedMemConfig");
	wrapper::cudaLaunch = (decltype(wrapper::cudaLaunch))dlsym(cuda_rt_lib_handle, "cudaLaunch");
	wrapper::cudaFuncGetAttributes = (decltype(wrapper::cudaFuncGetAttributes))dlsym(cuda_rt_lib_handle, "cudaFuncGetAttributes");
	wrapper::cudaSetDoubleForDevice = (decltype(wrapper::cudaSetDoubleForDevice))dlsym(cuda_rt_lib_handle, "cudaSetDoubleForDevice");
	wrapper::cudaSetDoubleForHost = (decltype(wrapper::cudaSetDoubleForHost))dlsym(cuda_rt_lib_handle, "cudaSetDoubleForHost");
	wrapper::cudaMalloc = (decltype(wrapper::cudaMalloc))dlsym(cuda_rt_lib_handle, "cudaMalloc");
	wrapper::cudaMallocHost = (decltype(wrapper::cudaMallocHost))dlsym(cuda_rt_lib_handle, "cudaMallocHost");
	wrapper::cudaMallocPitch = (decltype(wrapper::cudaMallocPitch))dlsym(cuda_rt_lib_handle, "cudaMallocPitch");
	wrapper::cudaMallocArray = (decltype(wrapper::cudaMallocArray))dlsym(cuda_rt_lib_handle, "cudaMallocArray");
	wrapper::cudaFree = (decltype(wrapper::cudaFree))dlsym(cuda_rt_lib_handle, "cudaFree");
	wrapper::cudaFreeHost = (decltype(wrapper::cudaFreeHost))dlsym(cuda_rt_lib_handle, "cudaFreeHost");
	wrapper::cudaFreeArray = (decltype(wrapper::cudaFreeArray))dlsym(cuda_rt_lib_handle, "cudaFreeArray");
	wrapper::cudaFreeMipmappedArray = (decltype(wrapper::cudaFreeMipmappedArray))dlsym(cuda_rt_lib_handle, "cudaFreeMipmappedArray");
	wrapper::cudaHostAlloc = (decltype(wrapper::cudaHostAlloc))dlsym(cuda_rt_lib_handle, "cudaHostAlloc");
	wrapper::cudaHostRegister = (decltype(wrapper::cudaHostRegister))dlsym(cuda_rt_lib_handle, "cudaHostRegister");
	wrapper::cudaHostUnregister = (decltype(wrapper::cudaHostUnregister))dlsym(cuda_rt_lib_handle, "cudaHostUnregister");
	wrapper::cudaHostGetDevicePointer = (decltype(wrapper::cudaHostGetDevicePointer))dlsym(cuda_rt_lib_handle, "cudaHostGetDevicePointer");
	wrapper::cudaHostGetFlags = (decltype(wrapper::cudaHostGetFlags))dlsym(cuda_rt_lib_handle, "cudaHostGetFlags");
	wrapper::cudaMalloc3D = (decltype(wrapper::cudaMalloc3D))dlsym(cuda_rt_lib_handle, "cudaMalloc3D");
	wrapper::cudaMalloc3DArray = (decltype(wrapper::cudaMalloc3DArray))dlsym(cuda_rt_lib_handle, "cudaMalloc3DArray");
	wrapper::cudaMallocMipmappedArray = (decltype(wrapper::cudaMallocMipmappedArray))dlsym(cuda_rt_lib_handle, "cudaMallocMipmappedArray");
	wrapper::cudaGetMipmappedArrayLevel = (decltype(wrapper::cudaGetMipmappedArrayLevel))dlsym(cuda_rt_lib_handle, "cudaGetMipmappedArrayLevel");
	wrapper::cudaMemcpy3D = (decltype(wrapper::cudaMemcpy3D))dlsym(cuda_rt_lib_handle, "cudaMemcpy3D");
	wrapper::cudaMemcpy3DPeer = (decltype(wrapper::cudaMemcpy3DPeer))dlsym(cuda_rt_lib_handle, "cudaMemcpy3DPeer");
	wrapper::cudaMemcpy3DAsync = (decltype(wrapper::cudaMemcpy3DAsync))dlsym(cuda_rt_lib_handle, "cudaMemcpy3DAsync");
	wrapper::cudaMemcpy3DPeerAsync = (decltype(wrapper::cudaMemcpy3DPeerAsync))dlsym(cuda_rt_lib_handle, "cudaMemcpy3DPeerAsync");
	wrapper::cudaMemGetInfo = (decltype(wrapper::cudaMemGetInfo))dlsym(cuda_rt_lib_handle, "cudaMemGetInfo");
	wrapper::cudaArrayGetInfo = (decltype(wrapper::cudaArrayGetInfo))dlsym(cuda_rt_lib_handle, "cudaArrayGetInfo");
	wrapper::cudaMemcpy = (decltype(wrapper::cudaMemcpy))dlsym(cuda_rt_lib_handle, "cudaMemcpy");
	wrapper::cudaMemcpyPeer = (decltype(wrapper::cudaMemcpyPeer))dlsym(cuda_rt_lib_handle, "cudaMemcpyPeer");
	wrapper::cudaMemcpyToArray = (decltype(wrapper::cudaMemcpyToArray))dlsym(cuda_rt_lib_handle, "cudaMemcpyToArray");
	wrapper::cudaMemcpyFromArray = (decltype(wrapper::cudaMemcpyFromArray))dlsym(cuda_rt_lib_handle, "cudaMemcpyFromArray");
	wrapper::cudaMemcpyArrayToArray = (decltype(wrapper::cudaMemcpyArrayToArray))dlsym(cuda_rt_lib_handle, "cudaMemcpyArrayToArray");
	wrapper::cudaMemcpy2D = (decltype(wrapper::cudaMemcpy2D))dlsym(cuda_rt_lib_handle, "cudaMemcpy2D");
	wrapper::cudaMemcpy2DToArray = (decltype(wrapper::cudaMemcpy2DToArray))dlsym(cuda_rt_lib_handle, "cudaMemcpy2DToArray");
	wrapper::cudaMemcpy2DFromArray = (decltype(wrapper::cudaMemcpy2DFromArray))dlsym(cuda_rt_lib_handle, "cudaMemcpy2DFromArray");
	wrapper::cudaMemcpy2DArrayToArray = (decltype(wrapper::cudaMemcpy2DArrayToArray))dlsym(cuda_rt_lib_handle, "cudaMemcpy2DArrayToArray");
	wrapper::cudaMemcpyToSymbol = (decltype(wrapper::cudaMemcpyToSymbol))dlsym(cuda_rt_lib_handle, "cudaMemcpyToSymbol");
	wrapper::cudaMemcpyFromSymbol = (decltype(wrapper::cudaMemcpyFromSymbol))dlsym(cuda_rt_lib_handle, "cudaMemcpyFromSymbol");
	wrapper::cudaMemcpyAsync = (decltype(wrapper::cudaMemcpyAsync))dlsym(cuda_rt_lib_handle, "cudaMemcpyAsync");
	wrapper::cudaMemcpyPeerAsync = (decltype(wrapper::cudaMemcpyPeerAsync))dlsym(cuda_rt_lib_handle, "cudaMemcpyPeerAsync");
	wrapper::cudaMemcpyToArrayAsync = (decltype(wrapper::cudaMemcpyToArrayAsync))dlsym(cuda_rt_lib_handle, "cudaMemcpyToArrayAsync");
	wrapper::cudaMemcpyFromArrayAsync = (decltype(wrapper::cudaMemcpyFromArrayAsync))dlsym(cuda_rt_lib_handle, "cudaMemcpyFromArrayAsync");
	wrapper::cudaMemcpy2DAsync = (decltype(wrapper::cudaMemcpy2DAsync))dlsym(cuda_rt_lib_handle, "cudaMemcpy2DAsync");
	wrapper::cudaMemcpy2DToArrayAsync = (decltype(wrapper::cudaMemcpy2DToArrayAsync))dlsym(cuda_rt_lib_handle, "cudaMemcpy2DToArrayAsync");
	wrapper::cudaMemcpy2DFromArrayAsync = (decltype(wrapper::cudaMemcpy2DFromArrayAsync))dlsym(cuda_rt_lib_handle, "cudaMemcpy2DFromArrayAsync");
	wrapper::cudaMemcpyToSymbolAsync = (decltype(wrapper::cudaMemcpyToSymbolAsync))dlsym(cuda_rt_lib_handle, "cudaMemcpyToSymbolAsync");
	wrapper::cudaMemcpyFromSymbolAsync = (decltype(wrapper::cudaMemcpyFromSymbolAsync))dlsym(cuda_rt_lib_handle, "cudaMemcpyFromSymbolAsync");
	wrapper::cudaMemset = (decltype(wrapper::cudaMemset))dlsym(cuda_rt_lib_handle, "cudaMemset");
	wrapper::cudaMemset2D = (decltype(wrapper::cudaMemset2D))dlsym(cuda_rt_lib_handle, "cudaMemset2D");
	wrapper::cudaMemset3D = (decltype(wrapper::cudaMemset3D))dlsym(cuda_rt_lib_handle, "cudaMemset3D");
	wrapper::cudaMemsetAsync = (decltype(wrapper::cudaMemsetAsync))dlsym(cuda_rt_lib_handle, "cudaMemsetAsync");
	wrapper::cudaMemset2DAsync = (decltype(wrapper::cudaMemset2DAsync))dlsym(cuda_rt_lib_handle, "cudaMemset2DAsync");
	wrapper::cudaMemset3DAsync = (decltype(wrapper::cudaMemset3DAsync))dlsym(cuda_rt_lib_handle, "cudaMemset3DAsync");
	wrapper::cudaGetSymbolAddress = (decltype(wrapper::cudaGetSymbolAddress))dlsym(cuda_rt_lib_handle, "cudaGetSymbolAddress");
	wrapper::cudaGetSymbolSize = (decltype(wrapper::cudaGetSymbolSize))dlsym(cuda_rt_lib_handle, "cudaGetSymbolSize");
	wrapper::cudaPointerGetAttributes = (decltype(wrapper::cudaPointerGetAttributes))dlsym(cuda_rt_lib_handle, "cudaPointerGetAttributes");
	wrapper::cudaDeviceCanAccessPeer = (decltype(wrapper::cudaDeviceCanAccessPeer))dlsym(cuda_rt_lib_handle, "cudaDeviceCanAccessPeer");
	wrapper::cudaDeviceEnablePeerAccess = (decltype(wrapper::cudaDeviceEnablePeerAccess))dlsym(cuda_rt_lib_handle, "cudaDeviceEnablePeerAccess");
	wrapper::cudaDeviceDisablePeerAccess = (decltype(wrapper::cudaDeviceDisablePeerAccess))dlsym(cuda_rt_lib_handle, "cudaDeviceDisablePeerAccess");
	wrapper::cudaGetChannelDesc = (decltype(wrapper::cudaGetChannelDesc))dlsym(cuda_rt_lib_handle, "cudaGetChannelDesc");
	wrapper::cudaCreateChannelDesc = (decltype(wrapper::cudaCreateChannelDesc))dlsym(cuda_rt_lib_handle, "cudaCreateChannelDesc");
	wrapper::cudaBindTexture = (decltype(wrapper::cudaBindTexture))dlsym(cuda_rt_lib_handle, "cudaBindTexture");
	wrapper::cudaBindTexture2D = (decltype(wrapper::cudaBindTexture2D))dlsym(cuda_rt_lib_handle, "cudaBindTexture2D");
	wrapper::cudaBindTextureToArray = (decltype(wrapper::cudaBindTextureToArray))dlsym(cuda_rt_lib_handle, "cudaBindTextureToArray");
	wrapper::cudaBindTextureToMipmappedArray = (decltype(wrapper::cudaBindTextureToMipmappedArray))dlsym(cuda_rt_lib_handle, "cudaBindTextureToMipmappedArray");
	wrapper::cudaUnbindTexture = (decltype(wrapper::cudaUnbindTexture))dlsym(cuda_rt_lib_handle, "cudaUnbindTexture");
	wrapper::cudaGetTextureAlignmentOffset = (decltype(wrapper::cudaGetTextureAlignmentOffset))dlsym(cuda_rt_lib_handle, "cudaGetTextureAlignmentOffset");
	wrapper::cudaGetTextureReference = (decltype(wrapper::cudaGetTextureReference))dlsym(cuda_rt_lib_handle, "cudaGetTextureReference");
	wrapper::cudaBindSurfaceToArray = (decltype(wrapper::cudaBindSurfaceToArray))dlsym(cuda_rt_lib_handle, "cudaBindSurfaceToArray");
	wrapper::cudaGetSurfaceReference = (decltype(wrapper::cudaGetSurfaceReference))dlsym(cuda_rt_lib_handle, "cudaGetSurfaceReference");
	wrapper::cudaCreateTextureObject = (decltype(wrapper::cudaCreateTextureObject))dlsym(cuda_rt_lib_handle, "cudaCreateTextureObject");
	wrapper::cudaDestroyTextureObject = (decltype(wrapper::cudaDestroyTextureObject))dlsym(cuda_rt_lib_handle, "cudaDestroyTextureObject");
	wrapper::cudaGetTextureObjectResourceDesc = (decltype(wrapper::cudaGetTextureObjectResourceDesc))dlsym(cuda_rt_lib_handle, "cudaGetTextureObjectResourceDesc");
	wrapper::cudaGetTextureObjectTextureDesc = (decltype(wrapper::cudaGetTextureObjectTextureDesc))dlsym(cuda_rt_lib_handle, "cudaGetTextureObjectTextureDesc");
	wrapper::cudaGetTextureObjectResourceViewDesc = (decltype(wrapper::cudaGetTextureObjectResourceViewDesc))dlsym(cuda_rt_lib_handle, "cudaGetTextureObjectResourceViewDesc");
	wrapper::cudaCreateSurfaceObject = (decltype(wrapper::cudaCreateSurfaceObject))dlsym(cuda_rt_lib_handle, "cudaCreateSurfaceObject");
	wrapper::cudaDestroySurfaceObject = (decltype(wrapper::cudaDestroySurfaceObject))dlsym(cuda_rt_lib_handle, "cudaDestroySurfaceObject");
	wrapper::cudaGetSurfaceObjectResourceDesc = (decltype(wrapper::cudaGetSurfaceObjectResourceDesc))dlsym(cuda_rt_lib_handle, "cudaGetSurfaceObjectResourceDesc");
	wrapper::cudaDriverGetVersion = (decltype(wrapper::cudaDriverGetVersion))dlsym(cuda_rt_lib_handle, "cudaDriverGetVersion");
	wrapper::cudaRuntimeGetVersion = (decltype(wrapper::cudaRuntimeGetVersion))dlsym(cuda_rt_lib_handle, "cudaRuntimeGetVersion");
	wrapper::cudaGetExportTable = (decltype(wrapper::cudaGetExportTable))dlsym(cuda_rt_lib_handle, "cudaGetExportTable");


	wrapper::cuInit = (decltype(wrapper::cuInit))dlsym(cuda_lib_handle, "cuInit");
	wrapper::cuDriverGetVersion = (decltype(wrapper::cuDriverGetVersion))dlsym(cuda_lib_handle, "cuDriverGetVersion");
	wrapper::cuDeviceGet = (decltype(wrapper::cuDeviceGet))dlsym(cuda_lib_handle, "cuDeviceGet");
	wrapper::cuDeviceGetCount = (decltype(wrapper::cuDeviceGetCount))dlsym(cuda_lib_handle, "cuDeviceGetCount");
	wrapper::cuDeviceGetName = (decltype(wrapper::cuDeviceGetName))dlsym(cuda_lib_handle, "cuDeviceGetName");
	wrapper::cuDeviceTotalMem = (decltype(wrapper::cuDeviceTotalMem))dlsym(cuda_lib_handle, "cuDeviceTotalMem_v2");
	wrapper::cuDeviceGetAttribute = (decltype(wrapper::cuDeviceGetAttribute))dlsym(cuda_lib_handle, "cuDeviceGetAttribute");
	wrapper::cuDeviceGetProperties = (decltype(wrapper::cuDeviceGetProperties))dlsym(cuda_lib_handle, "cuDeviceGetProperties");
	wrapper::cuDeviceComputeCapability = (decltype(wrapper::cuDeviceComputeCapability))dlsym(cuda_lib_handle, "cuDeviceComputeCapability");
	wrapper::cuCtxCreate = (decltype(wrapper::cuCtxCreate))dlsym(cuda_lib_handle, "cuCtxCreate_v2");
	wrapper::cuCtxDestroy = (decltype(wrapper::cuCtxDestroy))dlsym(cuda_lib_handle, "cuCtxDestroy_v2");
	wrapper::cuCtxPushCurrent = (decltype(wrapper::cuCtxPushCurrent))dlsym(cuda_lib_handle, "cuCtxPushCurrent_v2");
	wrapper::cuCtxPopCurrent = (decltype(wrapper::cuCtxPopCurrent))dlsym(cuda_lib_handle, "cuCtxPopCurrent_v2");
	wrapper::cuCtxSetCurrent = (decltype(wrapper::cuCtxSetCurrent))dlsym(cuda_lib_handle, "cuCtxSetCurrent");
	wrapper::cuCtxGetCurrent = (decltype(wrapper::cuCtxGetCurrent))dlsym(cuda_lib_handle, "cuCtxGetCurrent");
	wrapper::cuCtxGetDevice = (decltype(wrapper::cuCtxGetDevice))dlsym(cuda_lib_handle, "cuCtxGetDevice");
	wrapper::cuCtxSynchronize = (decltype(wrapper::cuCtxSynchronize))dlsym(cuda_lib_handle, "cuCtxSynchronize");
	wrapper::cuCtxSetLimit = (decltype(wrapper::cuCtxSetLimit))dlsym(cuda_lib_handle, "cuCtxSetLimit");
	wrapper::cuCtxGetLimit = (decltype(wrapper::cuCtxGetLimit))dlsym(cuda_lib_handle, "cuCtxGetLimit");
	wrapper::cuCtxGetCacheConfig = (decltype(wrapper::cuCtxGetCacheConfig))dlsym(cuda_lib_handle, "cuCtxGetCacheConfig");
	wrapper::cuCtxSetCacheConfig = (decltype(wrapper::cuCtxSetCacheConfig))dlsym(cuda_lib_handle, "cuCtxSetCacheConfig");
	wrapper::cuCtxGetSharedMemConfig = (decltype(wrapper::cuCtxGetSharedMemConfig))dlsym(cuda_lib_handle, "cuCtxGetSharedMemConfig");
	wrapper::cuCtxSetSharedMemConfig = (decltype(wrapper::cuCtxSetSharedMemConfig))dlsym(cuda_lib_handle, "cuCtxSetSharedMemConfig");
	wrapper::cuCtxGetApiVersion = (decltype(wrapper::cuCtxGetApiVersion))dlsym(cuda_lib_handle, "cuCtxGetApiVersion");
	wrapper::cuCtxAttach = (decltype(wrapper::cuCtxAttach))dlsym(cuda_lib_handle, "cuCtxAttach");
	wrapper::cuCtxDetach = (decltype(wrapper::cuCtxDetach))dlsym(cuda_lib_handle, "cuCtxDetach");
	wrapper::cuModuleLoad = (decltype(wrapper::cuModuleLoad))dlsym(cuda_lib_handle, "cuModuleLoad");
	wrapper::cuModuleLoadData = (decltype(wrapper::cuModuleLoadData))dlsym(cuda_lib_handle, "cuModuleLoadData");
	wrapper::cuModuleLoadDataEx = (decltype(wrapper::cuModuleLoadDataEx))dlsym(cuda_lib_handle, "cuModuleLoadDataEx");
	wrapper::cuModuleLoadFatBinary = (decltype(wrapper::cuModuleLoadFatBinary))dlsym(cuda_lib_handle, "cuModuleLoadFatBinary");
	wrapper::cuModuleUnload = (decltype(wrapper::cuModuleUnload))dlsym(cuda_lib_handle, "cuModuleUnload");
	wrapper::cuModuleGetFunction = (decltype(wrapper::cuModuleGetFunction))dlsym(cuda_lib_handle, "cuModuleGetFunction");
	wrapper::cuModuleGetGlobal = (decltype(wrapper::cuModuleGetGlobal))dlsym(cuda_lib_handle, "cuModuleGetGlobal_v2");
	wrapper::cuModuleGetTexRef = (decltype(wrapper::cuModuleGetTexRef))dlsym(cuda_lib_handle, "cuModuleGetTexRef");
	wrapper::cuModuleGetSurfRef = (decltype(wrapper::cuModuleGetSurfRef))dlsym(cuda_lib_handle, "cuModuleGetSurfRef");
	wrapper::cuMemGetInfo = (decltype(wrapper::cuMemGetInfo))dlsym(cuda_lib_handle, "cuMemGetInfo_v2");
	wrapper::cuMemAlloc = (decltype(wrapper::cuMemAlloc))dlsym(cuda_lib_handle, "cuMemAlloc_v2");
	wrapper::cuMemAllocPitch = (decltype(wrapper::cuMemAllocPitch))dlsym(cuda_lib_handle, "cuMemAllocPitch_v2");
	wrapper::cuMemFree = (decltype(wrapper::cuMemFree))dlsym(cuda_lib_handle, "cuMemFree_v2");
	wrapper::cuMemGetAddressRange = (decltype(wrapper::cuMemGetAddressRange))dlsym(cuda_lib_handle, "cuMemGetAddressRange_v2");
	wrapper::cuMemAllocHost = (decltype(wrapper::cuMemAllocHost))dlsym(cuda_lib_handle, "cuMemAllocHost_v2");
	wrapper::cuMemFreeHost = (decltype(wrapper::cuMemFreeHost))dlsym(cuda_lib_handle, "cuMemFreeHost");
	wrapper::cuMemHostAlloc = (decltype(wrapper::cuMemHostAlloc))dlsym(cuda_lib_handle, "cuMemHostAlloc");
	wrapper::cuMemHostGetDevicePointer = (decltype(wrapper::cuMemHostGetDevicePointer))dlsym(cuda_lib_handle, "cuMemHostGetDevicePointer_v2");
	wrapper::cuMemHostGetFlags = (decltype(wrapper::cuMemHostGetFlags))dlsym(cuda_lib_handle, "cuMemHostGetFlags");
	wrapper::cuDeviceGetByPCIBusId = (decltype(wrapper::cuDeviceGetByPCIBusId))dlsym(cuda_lib_handle, "cuDeviceGetByPCIBusId");
	wrapper::cuDeviceGetPCIBusId = (decltype(wrapper::cuDeviceGetPCIBusId))dlsym(cuda_lib_handle, "cuDeviceGetPCIBusId");
	wrapper::cuIpcGetEventHandle = (decltype(wrapper::cuIpcGetEventHandle))dlsym(cuda_lib_handle, "cuIpcGetEventHandle");
	wrapper::cuIpcOpenEventHandle = (decltype(wrapper::cuIpcOpenEventHandle))dlsym(cuda_lib_handle, "cuIpcOpenEventHandle");
	wrapper::cuIpcGetMemHandle = (decltype(wrapper::cuIpcGetMemHandle))dlsym(cuda_lib_handle, "cuIpcGetMemHandle");
	wrapper::cuIpcOpenMemHandle = (decltype(wrapper::cuIpcOpenMemHandle))dlsym(cuda_lib_handle, "cuIpcOpenMemHandle");
	wrapper::cuIpcCloseMemHandle = (decltype(wrapper::cuIpcCloseMemHandle))dlsym(cuda_lib_handle, "cuIpcCloseMemHandle");
	wrapper::cuMemHostRegister = (decltype(wrapper::cuMemHostRegister))dlsym(cuda_lib_handle, "cuMemHostRegister");
	wrapper::cuMemHostUnregister = (decltype(wrapper::cuMemHostUnregister))dlsym(cuda_lib_handle, "cuMemHostUnregister");
	wrapper::cuMemcpy = (decltype(wrapper::cuMemcpy))dlsym(cuda_lib_handle, "cuMemcpy");
	wrapper::cuMemcpyPeer = (decltype(wrapper::cuMemcpyPeer))dlsym(cuda_lib_handle, "cuMemcpyPeer");
	wrapper::cuMemcpyHtoD = (decltype(wrapper::cuMemcpyHtoD))dlsym(cuda_lib_handle, "cuMemcpyHtoD_v2");
	wrapper::cuMemcpyDtoH = (decltype(wrapper::cuMemcpyDtoH))dlsym(cuda_lib_handle, "cuMemcpyDtoH_v2");
	wrapper::cuMemcpyDtoD = (decltype(wrapper::cuMemcpyDtoD))dlsym(cuda_lib_handle, "cuMemcpyDtoD_v2");
	wrapper::cuMemcpyDtoA = (decltype(wrapper::cuMemcpyDtoA))dlsym(cuda_lib_handle, "cuMemcpyDtoA_v2");
	wrapper::cuMemcpyAtoD = (decltype(wrapper::cuMemcpyAtoD))dlsym(cuda_lib_handle, "cuMemcpyAtoD_v2");
	wrapper::cuMemcpyHtoA = (decltype(wrapper::cuMemcpyHtoA))dlsym(cuda_lib_handle, "cuMemcpyHtoA_v2");
	wrapper::cuMemcpyAtoH = (decltype(wrapper::cuMemcpyAtoH))dlsym(cuda_lib_handle, "cuMemcpyAtoH_v2");
	wrapper::cuMemcpyAtoA = (decltype(wrapper::cuMemcpyAtoA))dlsym(cuda_lib_handle, "cuMemcpyAtoA_v2");
	wrapper::cuMemcpy2D = (decltype(wrapper::cuMemcpy2D))dlsym(cuda_lib_handle, "cuMemcpy2D_v2");
	wrapper::cuMemcpy2DUnaligned = (decltype(wrapper::cuMemcpy2DUnaligned))dlsym(cuda_lib_handle, "cuMemcpy2DUnaligned_v2");
	wrapper::cuMemcpy3D = (decltype(wrapper::cuMemcpy3D))dlsym(cuda_lib_handle, "cuMemcpy3D_v2");
	wrapper::cuMemcpy3DPeer = (decltype(wrapper::cuMemcpy3DPeer))dlsym(cuda_lib_handle, "cuMemcpy3DPeer");
	wrapper::cuMemcpyAsync = (decltype(wrapper::cuMemcpyAsync))dlsym(cuda_lib_handle, "cuMemcpyAsync");
	wrapper::cuMemcpyPeerAsync = (decltype(wrapper::cuMemcpyPeerAsync))dlsym(cuda_lib_handle, "cuMemcpyPeerAsync");
	wrapper::cuMemcpyHtoDAsync = (decltype(wrapper::cuMemcpyHtoDAsync))dlsym(cuda_lib_handle, "cuMemcpyHtoDAsync_v2");
	wrapper::cuMemcpyDtoHAsync = (decltype(wrapper::cuMemcpyDtoHAsync))dlsym(cuda_lib_handle, "cuMemcpyDtoHAsync_v2");
	wrapper::cuMemcpyDtoDAsync = (decltype(wrapper::cuMemcpyDtoDAsync))dlsym(cuda_lib_handle, "cuMemcpyDtoDAsync_v2");
	wrapper::cuMemcpyHtoAAsync = (decltype(wrapper::cuMemcpyHtoAAsync))dlsym(cuda_lib_handle, "cuMemcpyHtoAAsync_v2");
	wrapper::cuMemcpyAtoHAsync = (decltype(wrapper::cuMemcpyAtoHAsync))dlsym(cuda_lib_handle, "cuMemcpyAtoHAsync_v2");
	wrapper::cuMemcpy2DAsync = (decltype(wrapper::cuMemcpy2DAsync))dlsym(cuda_lib_handle, "cuMemcpy2DAsync_v2");
	wrapper::cuMemcpy3DAsync = (decltype(wrapper::cuMemcpy3DAsync))dlsym(cuda_lib_handle, "cuMemcpy3DAsync_v2");
	wrapper::cuMemcpy3DPeerAsync = (decltype(wrapper::cuMemcpy3DPeerAsync))dlsym(cuda_lib_handle, "cuMemcpy3DPeerAsync");
	wrapper::cuMemsetD8 = (decltype(wrapper::cuMemsetD8))dlsym(cuda_lib_handle, "cuMemsetD8_v2");
	wrapper::cuMemsetD16 = (decltype(wrapper::cuMemsetD16))dlsym(cuda_lib_handle, "cuMemsetD16_v2");
	wrapper::cuMemsetD32 = (decltype(wrapper::cuMemsetD32))dlsym(cuda_lib_handle, "cuMemsetD32_v2");
	wrapper::cuMemsetD2D8 = (decltype(wrapper::cuMemsetD2D8))dlsym(cuda_lib_handle, "cuMemsetD2D8_v2");
	wrapper::cuMemsetD2D16 = (decltype(wrapper::cuMemsetD2D16))dlsym(cuda_lib_handle, "cuMemsetD2D16_v2");
	wrapper::cuMemsetD2D32 = (decltype(wrapper::cuMemsetD2D32))dlsym(cuda_lib_handle, "cuMemsetD2D32_v2");
	wrapper::cuMemsetD8Async = (decltype(wrapper::cuMemsetD8Async))dlsym(cuda_lib_handle, "cuMemsetD8Async");
	wrapper::cuMemsetD16Async = (decltype(wrapper::cuMemsetD16Async))dlsym(cuda_lib_handle, "cuMemsetD16Async");
	wrapper::cuMemsetD32Async = (decltype(wrapper::cuMemsetD32Async))dlsym(cuda_lib_handle, "cuMemsetD32Async");
	wrapper::cuMemsetD2D8Async = (decltype(wrapper::cuMemsetD2D8Async))dlsym(cuda_lib_handle, "cuMemsetD2D8Async");
	wrapper::cuMemsetD2D16Async = (decltype(wrapper::cuMemsetD2D16Async))dlsym(cuda_lib_handle, "cuMemsetD2D16Async");
	wrapper::cuMemsetD2D32Async = (decltype(wrapper::cuMemsetD2D32Async))dlsym(cuda_lib_handle, "cuMemsetD2D32Async");
	wrapper::cuArrayCreate = (decltype(wrapper::cuArrayCreate))dlsym(cuda_lib_handle, "cuArrayCreate_v2");
	wrapper::cuArrayGetDescriptor = (decltype(wrapper::cuArrayGetDescriptor))dlsym(cuda_lib_handle, "cuArrayGetDescriptor_v2");
	wrapper::cuArrayDestroy = (decltype(wrapper::cuArrayDestroy))dlsym(cuda_lib_handle, "cuArrayDestroy");
	wrapper::cuArray3DCreate = (decltype(wrapper::cuArray3DCreate))dlsym(cuda_lib_handle, "cuArray3DCreate");
	wrapper::cuArray3DGetDescriptor = (decltype(wrapper::cuArray3DGetDescriptor))dlsym(cuda_lib_handle, "cuArray3DGetDescriptor_v2");
	wrapper::cuMipmappedArrayCreate = (decltype(wrapper::cuMipmappedArrayCreate))dlsym(cuda_lib_handle, "cuMipmappedArrayCreate");
	wrapper::cuMipmappedArrayGetLevel = (decltype(wrapper::cuMipmappedArrayGetLevel))dlsym(cuda_lib_handle, "cuMipmappedArrayGetLevel");
	wrapper::cuMipmappedArrayDestroy = (decltype(wrapper::cuMipmappedArrayDestroy))dlsym(cuda_lib_handle, "cuMipmappedArrayDestroy");
	wrapper::cuPointerGetAttribute = (decltype(wrapper::cuPointerGetAttribute))dlsym(cuda_lib_handle, "cuPointerGetAttribute");
	wrapper::cuStreamCreate = (decltype(wrapper::cuStreamCreate))dlsym(cuda_lib_handle, "cuStreamCreate");
	wrapper::cuStreamWaitEvent = (decltype(wrapper::cuStreamWaitEvent))dlsym(cuda_lib_handle, "cuStreamWaitEvent");
	wrapper::cuStreamAddCallback = (decltype(wrapper::cuStreamAddCallback))dlsym(cuda_lib_handle, "cuStreamAddCallback");
	wrapper::cuStreamQuery = (decltype(wrapper::cuStreamQuery))dlsym(cuda_lib_handle, "cuStreamQuery");
	wrapper::cuStreamSynchronize = (decltype(wrapper::cuStreamSynchronize))dlsym(cuda_lib_handle, "cuStreamSynchronize");
	wrapper::cuStreamDestroy = (decltype(wrapper::cuStreamDestroy))dlsym(cuda_lib_handle, "cuStreamDestroy_v2");
	wrapper::cuEventCreate = (decltype(wrapper::cuEventCreate))dlsym(cuda_lib_handle, "cuEventCreate");
	wrapper::cuEventRecord = (decltype(wrapper::cuEventRecord))dlsym(cuda_lib_handle, "cuEventRecord");
	wrapper::cuEventQuery = (decltype(wrapper::cuEventQuery))dlsym(cuda_lib_handle, "cuEventQuery");
	wrapper::cuEventSynchronize = (decltype(wrapper::cuEventSynchronize))dlsym(cuda_lib_handle, "cuEventSynchronize");
	wrapper::cuEventDestroy = (decltype(wrapper::cuEventDestroy))dlsym(cuda_lib_handle, "cuEventDestroy_v2");
	wrapper::cuEventElapsedTime = (decltype(wrapper::cuEventElapsedTime))dlsym(cuda_lib_handle, "cuEventElapsedTime");
	wrapper::cuFuncGetAttribute = (decltype(wrapper::cuFuncGetAttribute))dlsym(cuda_lib_handle, "cuFuncGetAttribute");
	wrapper::cuFuncSetCacheConfig = (decltype(wrapper::cuFuncSetCacheConfig))dlsym(cuda_lib_handle, "cuFuncSetCacheConfig");
	wrapper::cuFuncSetSharedMemConfig = (decltype(wrapper::cuFuncSetSharedMemConfig))dlsym(cuda_lib_handle, "cuFuncSetSharedMemConfig");
	wrapper::cuLaunchKernel = (decltype(wrapper::cuLaunchKernel))dlsym(cuda_lib_handle, "cuLaunchKernel");
	wrapper::cuFuncSetBlockShape = (decltype(wrapper::cuFuncSetBlockShape))dlsym(cuda_lib_handle, "cuFuncSetBlockShape");
	wrapper::cuFuncSetSharedSize = (decltype(wrapper::cuFuncSetSharedSize))dlsym(cuda_lib_handle, "cuFuncSetSharedSize");
	wrapper::cuParamSetSize = (decltype(wrapper::cuParamSetSize))dlsym(cuda_lib_handle, "cuParamSetSize");
	wrapper::cuParamSeti = (decltype(wrapper::cuParamSeti))dlsym(cuda_lib_handle, "cuParamSeti");
	wrapper::cuParamSetf = (decltype(wrapper::cuParamSetf))dlsym(cuda_lib_handle, "cuParamSetf");
	wrapper::cuParamSetv = (decltype(wrapper::cuParamSetv))dlsym(cuda_lib_handle, "cuParamSetv");
	wrapper::cuLaunch = (decltype(wrapper::cuLaunch))dlsym(cuda_lib_handle, "cuLaunch");
	wrapper::cuLaunchGrid = (decltype(wrapper::cuLaunchGrid))dlsym(cuda_lib_handle, "cuLaunchGrid");
	wrapper::cuLaunchGridAsync = (decltype(wrapper::cuLaunchGridAsync))dlsym(cuda_lib_handle, "cuLaunchGridAsync");
	wrapper::cuParamSetTexRef = (decltype(wrapper::cuParamSetTexRef))dlsym(cuda_lib_handle, "cuParamSetTexRef");
	wrapper::cuTexRefSetArray = (decltype(wrapper::cuTexRefSetArray))dlsym(cuda_lib_handle, "cuTexRefSetArray");
	wrapper::cuTexRefSetMipmappedArray = (decltype(wrapper::cuTexRefSetMipmappedArray))dlsym(cuda_lib_handle, "cuTexRefSetMipmappedArray");
	wrapper::cuTexRefSetAddress = (decltype(wrapper::cuTexRefSetAddress))dlsym(cuda_lib_handle, "cuTexRefSetAddress_v2");
	wrapper::cuTexRefSetAddress2D = (decltype(wrapper::cuTexRefSetAddress2D))dlsym(cuda_lib_handle, "cuTexRefSetAddress2D_v3");
	wrapper::cuTexRefSetFormat = (decltype(wrapper::cuTexRefSetFormat))dlsym(cuda_lib_handle, "cuTexRefSetFormat");
	wrapper::cuTexRefSetAddressMode = (decltype(wrapper::cuTexRefSetAddressMode))dlsym(cuda_lib_handle, "cuTexRefSetAddressMode");
	wrapper::cuTexRefSetFilterMode = (decltype(wrapper::cuTexRefSetFilterMode))dlsym(cuda_lib_handle, "cuTexRefSetFilterMode");
	wrapper::cuTexRefSetMipmapFilterMode = (decltype(wrapper::cuTexRefSetMipmapFilterMode))dlsym(cuda_lib_handle, "cuTexRefSetMipmapFilterMode");
	wrapper::cuTexRefSetMipmapLevelBias = (decltype(wrapper::cuTexRefSetMipmapLevelBias))dlsym(cuda_lib_handle, "cuTexRefSetMipmapLevelBias");
	wrapper::cuTexRefSetMipmapLevelClamp = (decltype(wrapper::cuTexRefSetMipmapLevelClamp))dlsym(cuda_lib_handle, "cuTexRefSetMipmapLevelClamp");
	wrapper::cuTexRefSetMaxAnisotropy = (decltype(wrapper::cuTexRefSetMaxAnisotropy))dlsym(cuda_lib_handle, "cuTexRefSetMaxAnisotropy");
	wrapper::cuTexRefSetFlags = (decltype(wrapper::cuTexRefSetFlags))dlsym(cuda_lib_handle, "cuTexRefSetFlags");
	wrapper::cuTexRefGetAddress = (decltype(wrapper::cuTexRefGetAddress))dlsym(cuda_lib_handle, "cuTexRefGetAddress_v2");
	wrapper::cuTexRefGetArray = (decltype(wrapper::cuTexRefGetArray))dlsym(cuda_lib_handle, "cuTexRefGetArray");
	wrapper::cuTexRefGetMipmappedArray = (decltype(wrapper::cuTexRefGetMipmappedArray))dlsym(cuda_lib_handle, "cuTexRefGetMipmappedArray");
	wrapper::cuTexRefGetAddressMode = (decltype(wrapper::cuTexRefGetAddressMode))dlsym(cuda_lib_handle, "cuTexRefGetAddressMode");
	wrapper::cuTexRefGetFilterMode = (decltype(wrapper::cuTexRefGetFilterMode))dlsym(cuda_lib_handle, "cuTexRefGetFilterMode");
	wrapper::cuTexRefGetFormat = (decltype(wrapper::cuTexRefGetFormat))dlsym(cuda_lib_handle, "cuTexRefGetFormat");
	wrapper::cuTexRefGetMipmapFilterMode = (decltype(wrapper::cuTexRefGetMipmapFilterMode))dlsym(cuda_lib_handle, "cuTexRefGetMipmapFilterMode");
	wrapper::cuTexRefGetMipmapLevelBias = (decltype(wrapper::cuTexRefGetMipmapLevelBias))dlsym(cuda_lib_handle, "cuTexRefGetMipmapLevelBias");
	wrapper::cuTexRefGetMipmapLevelClamp = (decltype(wrapper::cuTexRefGetMipmapLevelClamp))dlsym(cuda_lib_handle, "cuTexRefGetMipmapLevelClamp");
	wrapper::cuTexRefGetMaxAnisotropy = (decltype(wrapper::cuTexRefGetMaxAnisotropy))dlsym(cuda_lib_handle, "cuTexRefGetMaxAnisotropy");
	wrapper::cuTexRefGetFlags = (decltype(wrapper::cuTexRefGetFlags))dlsym(cuda_lib_handle, "cuTexRefGetFlags");
	wrapper::cuTexRefCreate = (decltype(wrapper::cuTexRefCreate))dlsym(cuda_lib_handle, "cuTexRefCreate");
	wrapper::cuTexRefDestroy = (decltype(wrapper::cuTexRefDestroy))dlsym(cuda_lib_handle, "cuTexRefDestroy");
	wrapper::cuSurfRefSetArray = (decltype(wrapper::cuSurfRefSetArray))dlsym(cuda_lib_handle, "cuSurfRefSetArray");
	wrapper::cuSurfRefGetArray = (decltype(wrapper::cuSurfRefGetArray))dlsym(cuda_lib_handle, "cuSurfRefGetArray");
	wrapper::cuTexObjectCreate = (decltype(wrapper::cuTexObjectCreate))dlsym(cuda_lib_handle, "cuTexObjectCreate");
	wrapper::cuTexObjectDestroy = (decltype(wrapper::cuTexObjectDestroy))dlsym(cuda_lib_handle, "cuTexObjectDestroy");
	wrapper::cuTexObjectGetResourceDesc = (decltype(wrapper::cuTexObjectGetResourceDesc))dlsym(cuda_lib_handle, "cuTexObjectGetResourceDesc");
	wrapper::cuTexObjectGetTextureDesc = (decltype(wrapper::cuTexObjectGetTextureDesc))dlsym(cuda_lib_handle, "cuTexObjectGetTextureDesc");
	wrapper::cuTexObjectGetResourceViewDesc = (decltype(wrapper::cuTexObjectGetResourceViewDesc))dlsym(cuda_lib_handle, "cuTexObjectGetResourceViewDesc");
	wrapper::cuSurfObjectCreate = (decltype(wrapper::cuSurfObjectCreate))dlsym(cuda_lib_handle, "cuSurfObjectCreate");
	wrapper::cuSurfObjectDestroy = (decltype(wrapper::cuSurfObjectDestroy))dlsym(cuda_lib_handle, "cuSurfObjectDestroy");
	wrapper::cuSurfObjectGetResourceDesc = (decltype(wrapper::cuSurfObjectGetResourceDesc))dlsym(cuda_lib_handle, "cuSurfObjectGetResourceDesc");
	wrapper::cuDeviceCanAccessPeer = (decltype(wrapper::cuDeviceCanAccessPeer))dlsym(cuda_lib_handle, "cuDeviceCanAccessPeer");
	wrapper::cuCtxEnablePeerAccess = (decltype(wrapper::cuCtxEnablePeerAccess))dlsym(cuda_lib_handle, "cuCtxEnablePeerAccess");
	wrapper::cuCtxDisablePeerAccess = (decltype(wrapper::cuCtxDisablePeerAccess))dlsym(cuda_lib_handle, "cuCtxDisablePeerAccess");
	wrapper::cuGetExportTable = (decltype(wrapper::cuGetExportTable))dlsym(cuda_lib_handle, "cuGetExportTable");

	return true;
}

void KernelApi::finalize()
{
	if(cuda_rt_lib_handle)
	{
		dlclose(cuda_rt_lib_handle);
		cuda_rt_lib_handle = NULL;
	}

	if(cuda_lib_handle)
	{
		dlclose(cuda_lib_handle);
		cuda_lib_handle = NULL;
	}
}

void KernelApi::throwOnError(const cudaError_t& error)
{
	if(error != cudaSuccess)
	{
		printf("RuntimeApi exception: %s\n", wrapper::cudaGetErrorString(error));
		throw std::runtime_error(cudaGetErrorString(error));
	}
}

int KernelApi::DeviceManagement::countAllDevices() const
{
	int count = 0;

	cudaError_t error = wrapper::cudaGetDeviceCount(&count);
	if(error == cudaErrorNoDevice)
		return 0;

	throwOnError(error);

	return count;
}

void KernelApi::DeviceManagement::use(int dev) const
{
	cudaError_t error = wrapper::cudaSetDevice(dev);
	throwOnError(error);
}

int KernelApi::DeviceManagement::current() const
{
	int dev = 0;

	cudaError_t error = wrapper::cudaGetDevice(&dev);

	if(error == cudaErrorNoDevice)
		return -1;

	throwOnError(error);

	return dev;
}

void KernelApi::DeviceManagement::enableMappedMemory() const
{
	cudaError_t error = wrapper::cudaSetDeviceFlags(cudaDeviceMapHost);
	throwOnError(error);
}

void KernelApi::DeviceManagement::Properties::probe(int dev) const
{
	cudaError_t error = wrapper::cudaDriverGetVersion(&propDriverVersion);
	throwOnError(error);

	error = wrapper::cudaRuntimeGetVersion(&propRuntimeVersion);
	throwOnError(error);

	error = wrapper::cudaGetDeviceProperties(&prop, dev);
	throwOnError(error);
}

std::string KernelApi::DeviceManagement::Properties::name() const
{
	return prop.name;
}

std::size_t KernelApi::DeviceManagement::Properties::globalMemorySize() const
{
	return prop.totalGlobalMem;
}

std::size_t KernelApi::DeviceManagement::Properties::sharedMemorySizePerBlock() const
{
	return prop.sharedMemPerBlock;
}

std::size_t KernelApi::DeviceManagement::Properties::registerMemorySizePerBlock() const
{
	return prop.regsPerBlock*4;
}

std::size_t KernelApi::DeviceManagement::Properties::constantMemorySize() const
{
	return prop.totalConstMem;
}

int KernelApi::DeviceManagement::Properties::warpSize() const
{
	return prop.warpSize;
}

int KernelApi::DeviceManagement::Properties::maxThreadsPerBlock() const
{
	return prop.maxThreadsPerBlock;
}

int KernelApi::DeviceManagement::Properties::maxBlockDimX() const
{
	return prop.maxThreadsDim[0];
}

int KernelApi::DeviceManagement::Properties::maxBlockDimY() const
{
	return prop.maxThreadsDim[1];
}

int KernelApi::DeviceManagement::Properties::maxBlockDimZ() const
{
	return prop.maxThreadsDim[2];
}

int KernelApi::DeviceManagement::Properties::maxGridDimX() const
{
	return prop.maxGridSize[0];
}

int KernelApi::DeviceManagement::Properties::maxGridDimY() const
{
	return prop.maxGridSize[1];
}

int KernelApi::DeviceManagement::Properties::maxGridDimZ() const
{
	return prop.maxGridSize[2];
}

int KernelApi::DeviceManagement::Properties::clockRate() const
{
	return prop.clockRate;
}

std::pair<int,int> KernelApi::DeviceManagement::Properties::driverVersion() const
{
	return std::make_pair(propDriverVersion/1000, propDriverVersion%100);
}

std::pair<int,int> KernelApi::DeviceManagement::Properties::runtimeVersion() const
{
	return std::make_pair(propRuntimeVersion/1000, propRuntimeVersion%100);
}

std::pair<int,int> KernelApi::DeviceManagement::Properties::isaVersion() const
{
	if(propDriverVersion == 3020)
		return std::make_pair(2,2);
	else if(propDriverVersion == 3010)
		return std::make_pair(2,1);
	else
		return std::make_pair(1,4);
}

std::pair<int,int> KernelApi::DeviceManagement::Properties::computeCapability() const
{
	return std::make_pair(prop.major, prop.minor);
}

bool KernelApi::DeviceManagement::Properties::supportOverlappedExecution() const
{
	return prop.deviceOverlap > 0 ? true : false;
}

int KernelApi::DeviceManagement::Properties::numMultiProcessors() const
{
	return prop.multiProcessorCount;
}

int KernelApi::DeviceManagement::Properties::numCorePerMultiProcessor() const
{
    // Defines for GPU Architecture types (using the SM version to determine the # of cores per SM
    typedef struct {
        int SM; // 0xMm (hexidecimal notation), M = SM Major version, and m = SM minor version
        int Cores;
    } sSMtoCores;

	sSMtoCores nGpuArchCoresPerSM[] =
	{ { 0x10,  8 },
	  { 0x11,  8 },
	  { 0x12,  8 },
	  { 0x13,  8 },
	  { 0x20, 32 },
	  { 0x21, 48 },
	  {   -1, -1 }
	};

    int index = 0;
    while (nGpuArchCoresPerSM[index].SM != -1) {
        if (nGpuArchCoresPerSM[index].SM == ((prop.major << 4) + prop.minor) ) {
            return nGpuArchCoresPerSM[index].Cores;
        }
        index++;
    }

    throwOnError(cudaErrorInvalidConfiguration);
    return -1;
}

bool KernelApi::DeviceManagement::Properties::hasKernelExecutionTimeLimit() const
{
	return prop.kernelExecTimeoutEnabled > 0 ? true : false;
}

bool KernelApi::DeviceManagement::Properties::supportMappedMemory() const
{
	return prop.canMapHostMemory > 0 ? true : false;
}

bool KernelApi::DeviceManagement::Properties::supportECCMemory() const
{
	return prop.ECCEnabled > 0 ? true : false;
}

bool KernelApi::DeviceManagement::Properties::supportConcurrentExecution() const
{
	return prop.concurrentKernels > 0 ? true : false;
}

void KernelApi::EventManagement::create(event_type* e) const
{
	cudaError_t error = wrapper::cudaEventCreate(e);
	throwOnError(error);
}

void KernelApi::EventManagement::destroy(const event_type& e) const
{
	cudaError_t error = wrapper::cudaEventDestroy(const_cast<event_type&>(e));
	throwOnError(error);
}

void KernelApi::EventManagement::record(const event_type& e, const stream_type& s) const
{
	cudaError_t error = wrapper::cudaEventRecord(const_cast<event_type&>(e), const_cast<stream_type&>(s));
	throwOnError(error);
}

bool KernelApi::EventManagement::queryCompleted(const event_type& e) const
{
	cudaError_t error = wrapper::cudaEventQuery(const_cast<event_type&>(e));

	if(error == cudaSuccess)
	{
		return true;
	}
	else if(error == cudaErrorNotReady)
	{
		return false;
	}
	else
	{
		throwOnError(error);
		return false;
	}
}

void KernelApi::EventManagement::synchronize(const event_type& e) const
{
	cudaError_t error = wrapper::cudaEventSynchronize(const_cast<event_type&>(e));
	throwOnError(error);
}

float KernelApi::EventManagement::queryElapsed(const event_type& start, const event_type& stop) const
{
	float elapsed = 0.0f;

	cudaError_t error = wrapper::cudaEventElapsedTime(&elapsed, start, stop);
	throwOnError(error);

	return elapsed;
}

void KernelApi::StreamManagement::create(stream_type* s) const
{
	cudaError_t error = wrapper::cudaStreamCreate(s);
	throwOnError(error);
}

void KernelApi::StreamManagement::destroy(const stream_type& s) const
{
	cudaError_t error = wrapper::cudaStreamDestroy(const_cast<stream_type&>(s));
	throwOnError(error);
}

bool KernelApi::StreamManagement::queryCompleted(const stream_type& s) const
{
	cudaError_t error = wrapper::cudaStreamQuery(const_cast<stream_type&>(s));

	if(error == cudaSuccess)
	{
		return true;
	}
	else if(error == cudaErrorNotReady)
	{
		return false;
	}
	else
	{
		throwOnError(error);
		return false;
	}
}

void KernelApi::StreamManagement::synchronize(const stream_type& s) const
{
	cudaError_t error = wrapper::cudaStreamSynchronize(const_cast<stream_type&>(s));
	throwOnError(error);
}

KernelApi::MemoryManagement::HostMemoryManagement::HostMemoryManagement()
{
	// About the allocators/reservedMemories array...
	// [0]: pagelock = false
	// [1]: pagelock = true, portable = false, mapped = false, writecombined = false
	// [2]: pagelock = true, portable = false, mapped = false, writecombined = true
	// [3]: pagelock = true, portable = false, mapped = true,  writecombined = false
	// [4]: pagelock = true, portable = false, mapped = true,  writecombined = true
	// [5]: pagelock = true, portable = true,  mapped = false, writecombined = false
	// [6]: pagelock = true, portable = true,  mapped = false, writecombined = true
	// [7]: pagelock = true, portable = true,  mapped = true,  writecombined = false
	// [8]: pagelock = true, portable = true,  mapped = true,  writecombined = true

	allocators[0] = NULL;
	allocators[1] = NULL;
	allocators[2] = NULL;
	allocators[3] = NULL;
	allocators[4] = NULL;
	allocators[5] = NULL;
	allocators[6] = NULL;
	allocators[7] = NULL;
	allocators[8] = NULL;

	reservedMemories[0] = NULL;
	reservedMemories[1] = NULL;
	reservedMemories[2] = NULL;
	reservedMemories[3] = NULL;
	reservedMemories[4] = NULL;
	reservedMemories[5] = NULL;
	reservedMemories[6] = NULL;
	reservedMemories[7] = NULL;
	reservedMemories[8] = NULL;
}

KernelApi::MemoryManagement::HostMemoryManagement::~HostMemoryManagement()
{
	SAFE_DELETE(allocators[0]);
	SAFE_DELETE(allocators[1]);
	SAFE_DELETE(allocators[2]);
	SAFE_DELETE(allocators[3]);
	SAFE_DELETE(allocators[4]);
	SAFE_DELETE(allocators[5]);
	SAFE_DELETE(allocators[6]);
	SAFE_DELETE(allocators[7]);
	SAFE_DELETE(allocators[8]);

	if(reservedMemories[0]) freeRaw(reservedMemories[0], false);
	if(reservedMemories[1]) freeRaw(reservedMemories[1], true);
	if(reservedMemories[2]) freeRaw(reservedMemories[2], true);
	if(reservedMemories[3]) freeRaw(reservedMemories[3], true);
	if(reservedMemories[4]) freeRaw(reservedMemories[4], true);
	if(reservedMemories[5]) freeRaw(reservedMemories[5], true);
	if(reservedMemories[6]) freeRaw(reservedMemories[6], true);
	if(reservedMemories[7]) freeRaw(reservedMemories[7], true);
	if(reservedMemories[8]) freeRaw(reservedMemories[8], true);
}

void KernelApi::MemoryManagement::HostMemoryManagement::reserve(std::size_t size, bool pagelocked, bool portable, bool mapped, bool writecombined) const
{
	uint8 key = keyForReserved(pagelocked, portable, mapped, writecombined);

	SAFE_DELETE(allocators[key]);
	if(reservedMemories[key]) freeRaw(reservedMemories[key], pagelocked); reservedMemories[key] = NULL;

	if(size > 0)
	{
		allocateRaw(&reservedMemories[key], size, pagelocked, portable, mapped, writecombined);
		allocators[key] = new FragmentAllocator(reservedMemories[key], size);
	}
}

void KernelApi::MemoryManagement::HostMemoryManagement::allocate(MutablePointer** pointer, std::size_t size, bool from_reserved, bool pagelocked, bool portable, bool mapped, bool writecombined) const
{
	BOOST_ASSERT(pointer != NULL);

	if(from_reserved)
	{
		uint8 key = keyForReserved(pagelocked, portable, mapped, writecombined);
		FragmentAllocator* allocator = allocators[key];
		if(UNLIKELY(!allocator))
		{
			throw std::runtime_error("Invalid allocator, must call reserved() before using memory from pool");
		}

		if(!allocator->allocate(pointer, size))
		{
			throw std::runtime_error("Fail to allocate from pool, out of memory");
		}
	}
	else
	{
		byte* data = NULL;
		allocateRaw(&data, size, pagelocked, portable, mapped, writecombined);
		*pointer = new MutablePointer(data);
	}
}

void KernelApi::MemoryManagement::HostMemoryManagement::allocateRaw(byte** pointer, std::size_t size, bool pagelocked, bool portable, bool mapped, bool writecombined) const
{
	BOOST_ASSERT(pointer != NULL);

	if(pagelocked)
	{
		unsigned int flags = cudaHostAllocDefault;

		if(portable)      flags |= cudaHostAllocPortable;
		if(mapped)        flags |= cudaHostAllocMapped;
		if(writecombined) flags |= cudaHostAllocWriteCombined;

		cudaError_t error = wrapper::cudaHostAlloc(reinterpret_cast<void**>(pointer), size, flags);
		throwOnError(error);
	}
	else
	{
		BOOST_ASSERT(!mapped);
		BOOST_ASSERT(!writecombined);

		*pointer = reinterpret_cast<byte*>(::malloc(size));
	}
}

void KernelApi::MemoryManagement::HostMemoryManagement::free(MutablePointer* pointer, bool from_reserved, bool pagelocked, bool portable, bool mapped, bool writecombined) const
{
	BOOST_ASSERT(pointer != NULL);

	if(from_reserved)
	{
		uint8 key = keyForReserved(pagelocked, portable, mapped, writecombined);
		FragmentAllocator* allocator = allocators[key];
		if(UNLIKELY(!allocator))
		{
			throw std::runtime_error("Invalid allocator, must call reserved() before using memory from pool");
		}

		if(!allocator->deallocate(pointer))
		{
			throw std::runtime_error("Failed to deallocate, might be invalid pointer");
		}
	}
	else
	{
		freeRaw(pointer->data(), pagelocked);
		delete pointer;
	}
}

void KernelApi::MemoryManagement::HostMemoryManagement::freeRaw(byte* pointer, bool pagelocked) const
{
	BOOST_ASSERT(pointer != NULL);

	if(pagelocked)
	{
		cudaError_t error = wrapper::cudaFreeHost(pointer);
		throwOnError(error);
	}
	else
	{
		::free(reinterpret_cast<void*>(pointer));
	}
}

void KernelApi::MemoryManagement::HostMemoryManagement::set(byte* data, int value, std::size_t n) const
{
	::memset(data, value, n);
}

void KernelApi::MemoryManagement::HostMemoryManagement::mapToDevice(MutablePointer** device, MutablePointer* host) const
{
	byte* pointer = NULL;
	mapToDeviceRaw(&pointer, host->data());
	*device = new MutablePointer(pointer);
}

void KernelApi::MemoryManagement::HostMemoryManagement::mapToDeviceRaw(byte** device, byte* host) const
{
	cudaError_t error = wrapper::cudaHostGetDevicePointer(reinterpret_cast<void**>(device), reinterpret_cast<void*>(host), 0);
	throwOnError(error);
}
uint8 KernelApi::MemoryManagement::HostMemoryManagement::keyForReserved(bool pagelocked, bool portable, bool mapped, bool writecombined) const
{
	uint8 key = 0;
	if(pagelocked) key += 1; if(writecombined) key += 1; if(mapped) key += 2; if(portable) key += 4;
	return key;
}

KernelApi::MemoryManagement::DeviceMemoryManagement::DeviceMemoryManagement()
{
	allocator = NULL;
	reservedMemory = NULL;
}

KernelApi::MemoryManagement::DeviceMemoryManagement::~DeviceMemoryManagement()
{
	SAFE_DELETE(allocator);
	if(reservedMemory) freeRaw(reservedMemory); reservedMemory = NULL;
}

void KernelApi::MemoryManagement::DeviceMemoryManagement::reserve(std::size_t size) const
{
	SAFE_DELETE(allocator);
	if(reservedMemory) freeRaw(reservedMemory); reservedMemory = NULL;

	if(size > 0)
	{
		allocateRaw(&reservedMemory, size);
		allocator = new FragmentAllocator(reservedMemory, size);
	}
}

void KernelApi::MemoryManagement::DeviceMemoryManagement::allocate(MutablePointer** pointer, std::size_t size, bool from_reserved) const
{
	if(from_reserved)
	{
		if(UNLIKELY(!allocator))
		{
			throw std::runtime_error("Invalid allocator, must call reserved() before using memory from pool");
		}

		if(!allocator->allocate(pointer, size))
		{
			throw std::runtime_error("Fail to allocate from pool, out of memory");
		}
	}
	else
	{
		byte* data = NULL;
		allocateRaw(&data, size);
		*pointer = new MutablePointer(data);
	}
}

// CODEREVIEW: Why need this? Need more documentation or just remove it!
void KernelApi::MemoryManagement::DeviceMemoryManagement::allocateRaw(byte** pointer, std::size_t size) const
{
	cudaError_t error = wrapper::cudaMalloc(reinterpret_cast<void**>(pointer), size);
	throwOnError(error);
}

void KernelApi::MemoryManagement::DeviceMemoryManagement::free(MutablePointer* pointer, bool from_reserved) const
{
	if(from_reserved)
	{
		if(UNLIKELY(!allocator))
		{
			throw std::runtime_error("Invalid allocator, must call reserved() before using memory from pool");
		}

		if(!allocator->deallocate(pointer))
		{
			throw std::runtime_error("Failed to deallocate, might be invalid pointer");
		}
	}
	else
	{
		freeRaw(pointer->data());
		delete pointer;
	}
}

// CODEREVIEW: Why need this? Need more documentation or just remove it!
void KernelApi::MemoryManagement::DeviceMemoryManagement::freeRaw(byte* pointer) const
{
	cudaError_t error = wrapper::cudaFree(reinterpret_cast<void*>(pointer));
	throwOnError(error);
}

void KernelApi::MemoryManagement::DeviceMemoryManagement::set(MutablePointer* pointer, int value, std::size_t n) const
{
	setRaw(pointer->data(), value, n);
}

void KernelApi::MemoryManagement::DeviceMemoryManagement::setRaw(byte* pointer, int value, std::size_t n) const
{
	cudaError_t error = wrapper::cudaMemset(reinterpret_cast<void*>(pointer), value, n);
	throwOnError(error);
}

void KernelApi::MemoryManagement::copyToSymbolImpl(const char* symbol_name, const void* src, std::size_t size) const
{
	void* dev_ptr = NULL;
	cudaError_t error = wrapper::cudaGetSymbolAddress(&dev_ptr, symbol_name);
	throwOnError(error);
	error = wrapper::cudaMemcpyToSymbol(dev_ptr, src, size, 0, cudaMemcpyHostToDevice);
	throwOnError(error);
}

void KernelApi::MemoryManagement::copyToSymbolAsyncImpl(const char* symbol_name, const void* src, std::size_t size, const stream_type& stream) const
{
	void* dev_ptr = NULL;
	cudaError_t error = wrapper::cudaGetSymbolAddress(&dev_ptr, symbol_name);
	throwOnError(error);
	error = wrapper::cudaMemcpyToSymbolAsync(dev_ptr, &src, size, 0, cudaMemcpyHostToDevice, stream);
	throwOnError(error);
}

void KernelApi::MemoryManagement::copy(MutablePointer* dest, MutablePointer* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type) const
{
	copyRaw(dest->data(), src->data(), dest_offset, src_offset, size, dest_type, src_type);
}

void KernelApi::MemoryManagement::copyRaw(byte* dest, const byte* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type) const
{
	BOOST_ASSERT(size > 0);

	if(dest == src)
		return;

	cudaMemcpyKind kind;

	// find out the copy type by the source and destination memory location
	if(dest_type == location::host)
	{
		if(src_type == location::host)
		{
			kind = cudaMemcpyHostToHost;
		}
		else // src_type == location::device
		{
			kind = cudaMemcpyDeviceToHost;
		}
	}
	else // dest_type == location::device
	{
		if(src_type == location::host)
		{
			kind = cudaMemcpyHostToDevice;
		}
		else // src_type == location::device
		{
			kind = cudaMemcpyDeviceToDevice;
		}
	}

	// since the memory copy does not support overlapped memory region, we need to handle this carefully
	// the limit only apply in same memory context (i.e. host to host or device to device)
	if(kind == cudaMemcpyDeviceToDevice || kind == cudaMemcpyHostToHost)
	{
		if(src < dest && src + size > dest)
		{
			throw std::range_error("Overlapped memory copy is not supported");
		}
		else if(dest < src && dest + size > src)
		{
			throw std::range_error("Overlapped memory copy is not supported");
		}
	}

	cudaError_t error = wrapper::cudaMemcpy(dest + dest_offset, src + src_offset, size, kind);
	throwOnError(error);
}

void KernelApi::MemoryManagement::copyAsync(MutablePointer* dest, MutablePointer* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type, const stream_type& stream) const
{
	copyAsyncRaw(dest->data(), src->data(), dest_offset, src_offset, size, dest_type, src_type, stream);
}

void KernelApi::MemoryManagement::copyAsyncRaw(byte* dest, const byte* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type, const stream_type& stream) const
{
	BOOST_ASSERT(size > 0);

	if(dest == src)
		return;

	cudaMemcpyKind kind;

	// find out the copy type by the source and destination memory location
	if(dest_type == location::host)
	{
		if(src_type == location::host)
		{
			kind = cudaMemcpyHostToHost;
		}
		else // src_type == location::device
		{
			kind = cudaMemcpyDeviceToHost;
		}
	}
	else // dest_type == location::device
	{
		if(src_type == location::host)
		{
			kind = cudaMemcpyHostToDevice;
		}
		else // src_type == location::device
		{
			kind = cudaMemcpyDeviceToDevice;
		}
	}

	// since the memory copy does not support overlapped memory region, we need to handle this carefully
	// the limit only apply in same memory context (i.e. host to host or device to device)
	if(kind == cudaMemcpyDeviceToDevice || kind == cudaMemcpyHostToHost)
	{
		if(src < dest && src + size > dest)
		{
			throw std::range_error("Overlapped memory copy is not supported");
		}
		else if(dest < src && dest + size > src)
		{
			throw std::range_error("Overlapped memory copy is not supported");
		}
	}

	cudaError_t error = wrapper::cudaMemcpyAsync(dest + dest_offset, src + src_offset, size, kind, stream);
	throwOnError(error);
}

void KernelApi::ExecutionManagement::synchronize() const
{
	cudaError_t error = wrapper::cudaDeviceSynchronize();
	throwOnError(error);
}

void KernelApi::ExecutionManagement::configureKernel(const dim3& grid, const dim3& block, std::size_t shared_memory, const stream_type& s) const
{
	cudaError_t error = wrapper::cudaConfigureCall(grid, block, shared_memory, s);
	throwOnError(error);
}

void KernelApi::ExecutionManagement::setupParameter(const byte* args, std::size_t size, std::size_t offset) const
{
	cudaError_t error = wrapper::cudaSetupArgument(args, size, offset);
	throwOnError(error);
}

void KernelApi::ExecutionManagement::launchKernel(const void* entry) const
{
	cudaError_t error = wrapper::cudaLaunch(entry);
	throwOnError(error);
}

void KernelApi::ExecutionManagement::FunctionTraits::probe(const char* name) const
{
	cudaError_t error = wrapper::cudaFuncGetAttributes(&attr, name);
	throwOnError(error);
}

std::size_t KernelApi::ExecutionManagement::FunctionTraits::constantMemorySize() const
{
	return attr.constSizeBytes;
}

std::size_t KernelApi::ExecutionManagement::FunctionTraits::sharedMemorySize() const
{
	return attr.sharedSizeBytes;
}

std::size_t KernelApi::ExecutionManagement::FunctionTraits::localMemorySize() const
{
	return attr.localSizeBytes;
}

int KernelApi::ExecutionManagement::FunctionTraits::maxThreadsPerBlock() const
{
	return attr.maxThreadsPerBlock;
}

int KernelApi::ExecutionManagement::FunctionTraits::registerUsed() const
{
	return attr.numRegs;
}


KernelApi& GetKernelApi()
{
	static KernelApi api;
	return api;
}


} } } }

