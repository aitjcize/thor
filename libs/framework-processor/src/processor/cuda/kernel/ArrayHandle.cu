/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <core/Types.h>

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail {

template<typename T>
__global__ void ArrayHandle_DeviceFillConstantImpl(T* array, const T constant, uint elements)
{
	uint threadId = blockIdx.x * blockDim.x + threadIdx.x;

	if(threadId < elements)
	{
		array[threadId] = constant;
	}
}

template<typename T>
bool ArrayHandle_DeviceFillConstant(T* array, const T& constant, uint elements)
{
	uint threads = 256;
	uint blocks = (elements / 256) + ((elements % 256 == 0) ? 0 : 1);
	ArrayHandle_DeviceFillConstantImpl<T><<<blocks, threads>>>(array, constant, elements);

	cudaError err = cudaThreadSynchronize();

	if(cudaSuccess != err)
		return false;

	return true;
}

bool ArrayHandle_DeviceFillConstant8(byte* array, byte* constant, uint elements)
{
	uchar1 c = *((uchar1*)constant);
	if(elements % 16 == 0)
	{
		ulong2 value;
		((uchar1*)&value)[0] = c;
		((uchar1*)&value)[1] = c;
		((uchar1*)&value)[2] = c;
		((uchar1*)&value)[3] = c;
		((uchar1*)&value)[4] = c;
		((uchar1*)&value)[5] = c;
		((uchar1*)&value)[6] = c;
		((uchar1*)&value)[7] = c;
		((uchar1*)&value)[8] = c;
		((uchar1*)&value)[9] = c;
		((uchar1*)&value)[10] = c;
		((uchar1*)&value)[11] = c;
		((uchar1*)&value)[12] = c;
		((uchar1*)&value)[13] = c;
		((uchar1*)&value)[14] = c;
		((uchar1*)&value)[15] = c;
		return ArrayHandle_DeviceFillConstant<ulong2>((ulong2*)array, value, elements/16);
	}
	else if(elements % 8 == 0)
	{
		ulong1 value;
		((uchar1*)&value)[0] = c;
		((uchar1*)&value)[1] = c;
		((uchar1*)&value)[2] = c;
		((uchar1*)&value)[3] = c;
		((uchar1*)&value)[4] = c;
		((uchar1*)&value)[5] = c;
		((uchar1*)&value)[6] = c;
		((uchar1*)&value)[7] = c;
		return ArrayHandle_DeviceFillConstant<ulong1>((ulong1*)array, value, elements/8);
	}
	else if(elements % 4 == 0)
	{
		uint1 value;
		((uchar1*)&value)[0] = c;
		((uchar1*)&value)[1] = c;
		((uchar1*)&value)[2] = c;
		((uchar1*)&value)[3] = c;
		return ArrayHandle_DeviceFillConstant<uint1>((uint1*)array, value, elements/4);
	}
	else if(elements % 2 == 0)
	{
		ushort1 value;
		((uchar1*)&value)[0] = c;
		((uchar1*)&value)[1] = c;
		return ArrayHandle_DeviceFillConstant<ushort1>((ushort1*)array, value, elements/2);
	}
	else
	{
		return ArrayHandle_DeviceFillConstant<uchar1>((uchar1*)array, c, elements);
	}
}

bool ArrayHandle_DeviceFillConstant16(byte* array, byte* constant, uint elements)
{
	ushort1 c = *((ushort1*)constant);
	if(elements % 16 == 0)
	{
		ulong2 value;
		((ushort1*)&value)[0] = c;
		((ushort1*)&value)[1] = c;
		((ushort1*)&value)[2] = c;
		((ushort1*)&value)[3] = c;
		((ushort1*)&value)[4] = c;
		((ushort1*)&value)[5] = c;
		((ushort1*)&value)[6] = c;
		((ushort1*)&value)[7] = c;
		return ArrayHandle_DeviceFillConstant<ulong2>((ulong2*)array, value, elements/8);
	}
	else if(elements % 8 == 0)
	{
		ulong1 value;
		((ushort1*)&value)[0] = c;
		((ushort1*)&value)[1] = c;
		((ushort1*)&value)[2] = c;
		((ushort1*)&value)[3] = c;
		return ArrayHandle_DeviceFillConstant<ulong1>((ulong1*)array, value, elements/4);
	}
	else if(elements % 4 == 0)
	{
		uint1 value;
		((ushort1*)&value)[0] = c;
		((ushort1*)&value)[1] = c;
		return ArrayHandle_DeviceFillConstant<uint1>((uint1*)array, value, elements/2);
	}
	else
	{
		return ArrayHandle_DeviceFillConstant<ushort1>((ushort1*)array, c, elements);
	}
}

bool ArrayHandle_DeviceFillConstant32(byte* array, byte* constant, uint elements)
{
	uint1 c = *((uint1*)constant);
	if(elements % 16 == 0)
	{
		ulong2 value;
		((uint1*)&value)[0] = c;
		((uint1*)&value)[1] = c;
		((uint1*)&value)[2] = c;
		((uint1*)&value)[3] = c;
		return ArrayHandle_DeviceFillConstant<ulong2>((ulong2*)array, value, elements/4);
	}
	else if(elements % 8 == 0)
	{
		ulong1 value;
		((uint1*)&value)[0] = c;
		((uint1*)&value)[1] = c;
		return ArrayHandle_DeviceFillConstant<ulong1>((ulong1*)array, value, elements/2);
	}
	else
	{
		return ArrayHandle_DeviceFillConstant<uint1>((uint1*)array, c, elements);
	}
}
bool ArrayHandle_DeviceFillConstant64(byte* array, byte* constant, uint elements)
{
	ulong1 c = *((ulong1*)constant);
	if(elements % 16 == 0)
	{
		ulong2 value;
		((ulong1*)&value)[0] = c;
		((ulong1*)&value)[1] = c;
		return ArrayHandle_DeviceFillConstant<ulong2>((ulong2*)array, value, elements/2);
	}
	else
	{
		return ArrayHandle_DeviceFillConstant<ulong1>((ulong1*)array, c, elements);
	}
}
bool ArrayHandle_DeviceFillConstant128(byte* array, byte* constant, uint elements)
{
	ulong2 c = *((ulong2*)constant);
	return ArrayHandle_DeviceFillConstant<ulong2>((ulong2*)array, c, elements);
}

} } } } }
