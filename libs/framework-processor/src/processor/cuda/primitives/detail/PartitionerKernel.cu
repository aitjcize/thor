/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/processor/cuda/primitives/detail/CudaKernelCommon.h"

#define PARTITIONER_THREAD_BLOCK_SIZE 256

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail {

__global__ void PartitionerKernel_CreateDifferentFlagImpl(/*IN*/ uint32* keys, /*OUT*/ uint32* flags, uint32 elements)
{
	__shared__ uint32 shmKeys[PARTITIONER_THREAD_BLOCK_SIZE];

	uint32 threadId = blockIdx.x * blockDim.x + threadIdx.x;

	if(threadId < elements)
	{
		shmKeys[threadIdx.x] = keys[threadId];
	}

	__syncthreads();

	uint32 tl = 0, tr = 0;
	if(threadId < elements - 1)
	{
		tl = shmKeys[threadIdx.x];
		tr = (threadIdx.x < blockDim.x - 1) ? shmKeys[threadIdx.x + 1] : keys[threadId + 1];
	}

	__syncthreads();

	if(threadId < elements)
	{
		if(threadId != elements - 1)
			flags[threadId] = (tl == tr) ? 0 : 1;
		else
			flags[threadId] = 1;
	}
}

bool PartitionerKernel_CreateDifferentFlags(/*IN*/ uint32* keys, /*OUT*/ uint32 *flags, uint32 elements)
{
	uint32 threads = PARTITIONER_THREAD_BLOCK_SIZE;
	uint32 blocks  = elements / threads + ((elements % threads == 0) ? 0 : 1);
	uint32 shm     = sizeof(uint32) * PARTITIONER_THREAD_BLOCK_SIZE;

	PartitionerKernel_CreateDifferentFlagImpl<<<blocks, threads, shm>>>(keys, flags, elements);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

__global__ void PartitionerKernel_CreateDifferentIndicesImpl(/*IN*/ uint32* flags, /*IN*/ uint32* flagsScanned, /*OUT*/ uint32* indices, uint32 elements)
{
	uint32 threadId = blockIdx.x * blockDim.x + threadIdx.x;

	if(threadId < elements)
	{
		uint flag = flags[threadId];

		if(flag > 0)
		{
			uint addr = flagsScanned[threadId];
			indices[addr] = threadId;
		}
	}
}

bool PartitionerKernel_CreateDifferentIndices(/*IN*/ uint32* flags, /*IN*/ uint32* flagsScanned, /*OUT*/ uint32* indices, uint32 elements)
{
	uint32 threads = PARTITIONER_THREAD_BLOCK_SIZE;
	uint32 blocks  = elements / threads + ((elements % threads == 0) ? 0 : 1);
	uint32 shm     = 0;

	PartitionerKernel_CreateDifferentIndicesImpl<<<blocks, threads, shm>>>(flags, flagsScanned, indices, elements);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

__global__ void PartitionerKernel_CreatePartitionRangesImpl(/*IN*/ uint32* indices, /*OUT*/ uint32* fromIndices, /*OUT*/ uint32* toIndices, uint32 elements)
{
	__shared__ uint shmDiffIndices[PARTITIONER_THREAD_BLOCK_SIZE];

	uint threadId = blockIdx.x * blockDim.x + threadIdx.x;

	if(threadId < elements)
	{
		shmDiffIndices[threadIdx.x] = indices[threadId];
	}

	int rr = 0;
	int rl = 0;

	__syncthreads();

	if(threadId < elements)
	{
		rr = shmDiffIndices[threadIdx.x];

		if(threadIdx.x > 0)
		{
			rl = shmDiffIndices[threadIdx.x - 1] + 1;
		}
		else
		{
			rl = (threadId > 0) ? indices[threadId - 1] + 1 : 0;
		}
	}
	__syncthreads();

	if(threadId < elements)
	{
		fromIndices[threadId] = rl;
		toIndices[threadId] = rr;
	}
}

bool PartitionerKernel_CreatePartitionRanges(/*IN*/ uint32* indices, /*OUT*/ uint32* fromIndices, /*OUT*/ uint32* toIndices, uint32 elements)
{
	uint32 threads = PARTITIONER_THREAD_BLOCK_SIZE;
	uint32 blocks  = elements / threads + ((elements % threads == 0) ? 0 : 1);
	uint32 shm     = sizeof(uint32) * PARTITIONER_THREAD_BLOCK_SIZE;

	PartitionerKernel_CreatePartitionRangesImpl<<<blocks, threads, shm>>>(indices, fromIndices, toIndices, elements);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

} } } } }
