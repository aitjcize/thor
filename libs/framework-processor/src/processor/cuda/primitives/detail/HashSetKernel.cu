/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/processor/cuda/primitives/detail/CudaKernelCommon.h"
#include "framework/processor/cuda/primitives/HashSetApi.h"

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail {

__global__ void HashSetKernel_KeyRedistributeIntoBucket_Impl(uint32 keyCount, uint32* keys, uint32 bucketCount, uint32* bucketSizes, uint32* keysInBucket, uint32 bucketHash0, uint32 bucketHash1)
{
	const uint threadId = blockIdx.x * blockDim.x + threadIdx.x;

	if(threadId < keyCount)
	{
		uint k = keys[threadId];
		uint h = ((bucketHash0 + bucketHash1 * k) % 1900813) % bucketCount;
		uint offset = atomicAdd(&(bucketSizes[h]), 1);
		uint start = h * 512;
		keysInBucket[start + offset] = k;
	}
}

bool HashSetKernel_KeyRedistributeIntoBucket(uint32 keyCount, uint32* keys, uint32 bucketCount, uint32* bucketSizes, uint32* keysInBucket, uint32 bucketHash0, uint32 bucketHash1)
{
	uint threads = 512;
	uint blocks  = keyCount / threads + ((keyCount % threads == 0) ? 0 : 1);

	HashSetKernel_KeyRedistributeIntoBucket_Impl<<<blocks, threads>>>(
			keyCount, keys, bucketCount, bucketSizes, keysInBucket, bucketHash0, bucketHash1);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

#define WARP_SIZE				32
#define _ENABLE_DEBUG			0
#define _ENABLE_FAST_REDUCTION	1

__global__ void HashSetKernel_BuildCuckooHashingInBucket_Impl(uint32 bucketCount, uint32* bucketSizes, uint32* keysInBucket, uint32 cuckooHash, uint32* hashTables, uint32* buildErrors)
{
#if _ENABLE_FAST_REDUCTION
	uint32 positioned;
#else
	volatile __shared__ uint32 positioned[512];
#endif

#if _ENABLE_DEBUG
	volatile __shared__ uint32 positionedState[512];
#endif
	volatile __shared__ uint32 sharedKeys[3*192];
	volatile __shared__ uint32 done[512/WARP_SIZE];
	volatile __shared__ uint32 flag;
	volatile __shared__ uint32 size;

	const uint32 threadId = blockIdx.x * blockDim.x + threadIdx.x;

	// compute all hash function variable ci0 and ci1 by XORing with hash seed
	uint32 c00 = cuckooHash & 0x2B780D61;
	uint32 c01 = cuckooHash & 0xF07BE535;
	uint32 c10 = cuckooHash & 0x2B07D70A;
	uint32 c11 = cuckooHash & 0xABFB9D52;
	uint32 c20 = cuckooHash & 0x9D52B07D;
	uint32 c21 = cuckooHash & 0x8F5C28F6;

	// zero all elements in the tables/values
	if(threadIdx.x < 192)
	{
		sharedKeys[threadIdx.x + 192 * 0] = UINT_MAX;
		sharedKeys[threadIdx.x + 192 * 1] = UINT_MAX;
		sharedKeys[threadIdx.x + 192 * 2] = UINT_MAX;
	}

	// the first thread read the size of the bucket and broadcast to all other threads
	if(threadIdx.x == 0)
	{
		size = bucketSizes[blockIdx.x];
	}

	__syncthreads();

	// read the keys from global memory (note that invalid keys are UINT_MAX)
	uint32 key = UINT_MAX;
	if(threadIdx.x < size)
	{
		key = keysInBucket[threadId];
	}

	bool valid = (key == UINT_MAX) ? false : true;

#if _ENABLE_FAST_REDUCTION
	positioned = (key == UINT_MAX) ? 1 : 0;
#else
	positioned[threadIdx.x] = (key == UINT_MAX) ? 1 : 0;
#endif

#if _ENABLE_DEBUG
	positionedState[threadIdx.x] = (key == UINT_MAX) ? 0 : UINT_MAX;
#endif

	int i;
	for(i=0;i<25;++i)
	{
		uint32 h;
		uint32 check;

		// first table
		{
#if _ENABLE_FAST_REDUCTION
			if(valid && positioned == 0)
#else
			if(valid && positioned[threadIdx.x] == 0)
#endif
			{
				h = ((c00 + c01 * key) % 1900813) % 192;
				sharedKeys[(192*0)+h] = key;
			}

			__syncthreads();
#if _ENABLE_FAST_REDUCTION
			if(valid && (positioned == 1 || positioned == 0))
#else
			if(valid && (positioned[threadIdx.x] == 1 || positioned[threadIdx.x] == 0))
#endif
			{
				check = sharedKeys[(192*0)+h];

				if(check == key)
				{
#if _ENABLE_FAST_REDUCTION
					positioned = 1;
#else
					positioned[threadIdx.x] = 1;
#endif
#if _ENABLE_DEBUG
					positionedState[threadIdx.x] = (192*0)+h;
#endif
				}
				else
				{
#if _ENABLE_FAST_REDUCTION
					positioned = 0;
#else
					positioned[threadIdx.x] = 0;
#endif
#if _ENABLE_DEBUG
					positionedState[threadIdx.x] = UINT_MAX;
#endif
				}
			}
		}

#if _ENABLE_FAST_REDUCTION
		__syncthreads();

		if(__all(positioned) > 0)
		{
			if(threadIdx.x % WARP_SIZE == 0)
				done[threadIdx.x / WARP_SIZE] = 1;
		}
		else
		{
			if(threadIdx.x % WARP_SIZE == 0)
				done[threadIdx.x / WARP_SIZE] = 0;
		}

		__syncthreads();

		if(threadIdx.x == 0)
		{
			flag = 1;
			for(int i=0;i<512/WARP_SIZE;++i)
			{
				if(done[i] == 0)
				{
					flag = 0; break;
				}
			}
		}

		__syncthreads();

		if(flag == 1)
			break;
#else
		__syncthreads();

		if(threadIdx.x == 0)
		{
			flag = 1;
			for(int i=0;i<512;++i)
			{
				if(positioned[i] == 0)
				{
					flag = 0; break;
				}
			}
		}

		__syncthreads();

		if(flag == 1)
			break;
#endif

		// second table
		{
#if _ENABLE_FAST_REDUCTION
			if(valid && positioned == 0)
#else
			if(valid && positioned[threadIdx.x] == 0)
#endif
			{
				h = ((c10 + c11 * key) % 1900813) % 192;
				sharedKeys[(192*1)+h] = key;
			}

			__syncthreads();

#if _ENABLE_FAST_REDUCTION
			if(valid && (positioned == 2 || positioned == 0))
#else
			if(valid && (positioned[threadIdx.x] == 2 || positioned[threadIdx.x] == 0))
#endif
			{
				check = sharedKeys[(192*1)+h];

				if(check == key)
				{
#if _ENABLE_FAST_REDUCTION
					positioned = 2;
#else
					positioned[threadIdx.x] = 2;
#endif

#if _ENABLE_DEBUG
					positionedState[threadIdx.x] = (192*1)+h;
#endif
				}
				else
				{
#if _ENABLE_FAST_REDUCTION
					positioned = 0;
#else
					positioned[threadIdx.x] = 0;
#endif
#if _ENABLE_DEBUG
					positionedState[threadIdx.x] = UINT_MAX;
#endif
				}
			}
		}

#if _ENABLE_FAST_REDUCTION
		__syncthreads();

		if(__all(positioned) > 0)
		{
			if(threadIdx.x % WARP_SIZE == 0)
				done[threadIdx.x / WARP_SIZE] = 1;
		}
		else
		{
			if(threadIdx.x % WARP_SIZE == 0)
				done[threadIdx.x / WARP_SIZE] = 0;
		}

		__syncthreads();

		if(threadIdx.x == 0)
		{
			flag = 1;
			for(int i=0;i<512/WARP_SIZE;++i)
			{
				if(done[i] == 0)
				{
					flag = 0; break;
				}
			}
		}

		__syncthreads();

		if(flag == 1)
			break;
#else
		__syncthreads();

		if(threadIdx.x == 0)
		{
			flag = 1;
			for(int i=0;i<512;++i)
			{
				if(positioned[i] == 0)
				{
					flag = 0; break;
				}
			}
		}

		__syncthreads();

		if(flag == 1)
			break;
#endif

		// third table
		{
#if _ENABLE_FAST_REDUCTION
			if(valid && positioned == 0)
#else
			if(valid && positioned[threadIdx.x] == 0)
#endif
			{
				h = ((c20 + c21 * key) % 1900813) % 192;
				sharedKeys[(192*2)+h] = key;
			}

			__syncthreads();

#if _ENABLE_FAST_REDUCTION
			if(valid && (positioned == 3 || positioned == 0))
#else
			if(valid && (positioned[threadIdx.x] == 3 || positioned[threadIdx.x] == 0))
#endif
			{
				check = sharedKeys[(192*2)+h];

				if(check == key)
				{
#if _ENABLE_FAST_REDUCTION
					positioned = 3;
#else
					positioned[threadIdx.x] = 3;
#endif
#if _ENABLE_DEBUG
					positionedState[threadIdx.x] = (192*2)+h;
#endif
				}
				else
				{
#if _ENABLE_FAST_REDUCTION
					positioned = 0;
#else
					positioned[threadIdx.x] = 0;
#endif
#if _ENABLE_DEBUG
					positionedState[threadIdx.x] = UINT_MAX;
#endif
				}
			}
		}

#if _ENABLE_FAST_REDUCTION
		__syncthreads();

		if(__all(positioned) > 0)
		{
			if(threadIdx.x % WARP_SIZE == 0)
				done[threadIdx.x / WARP_SIZE] = 1;
		}
		else
		{
			if(threadIdx.x % WARP_SIZE == 0)
				done[threadIdx.x / WARP_SIZE] = 0;
		}

		__syncthreads();

		if(threadIdx.x == 0)
		{
			flag = 1;
			for(int i=0;i<512/WARP_SIZE;++i)
			{
				if(done[i] == 0)
				{
					flag = 0; break;
				}
			}
		}

		__syncthreads();

		if(flag == 1)
			break;
#else
		__syncthreads();

		if(threadIdx.x == 0)
		{
			flag = 1;
			for(int i=0;i<512;++i)
			{
				if(positioned[i] == 0)
				{
					flag = 0; break;
				}
			}
		}

		__syncthreads();

		if(flag == 1)
			break;
#endif
	}

	__syncthreads();

	if(i >= 25 || flag == 0)
	{
		if(threadIdx.x == 0)
		{
			buildErrors[blockIdx.x] = 1;
		}
		return;
	}

	// write the result out
	if(threadIdx.x < 192)
	{
		hashTables[blockIdx.x * 576 + 192 * 0 + threadIdx.x] = sharedKeys[threadIdx.x + 192 * 0];
		hashTables[blockIdx.x * 576 + 192 * 1 + threadIdx.x] = sharedKeys[threadIdx.x + 192 * 1];
		hashTables[blockIdx.x * 576 + 192 * 2 + threadIdx.x] = sharedKeys[threadIdx.x + 192 * 2];
	}
}

bool HashSetKernel_BuildCuckooHashingInBucket(uint32 bucketCount, uint32* bucketSizes, uint32* keysInBucket, uint32 cuckooHash, uint32* hashTables, uint32* buildErrors)
{
	uint threads = 512;
	uint blocks  = bucketCount;

	HashSetKernel_BuildCuckooHashingInBucket_Impl<<<blocks, threads>>>(
			bucketCount, bucketSizes, keysInBucket, cuckooHash, hashTables, buildErrors);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}


__global__ void HashSetKernel_FindInParallel_Impl(uint32 keyCount, uint32* keys, uint32 bucketCount, uint32 bucketHash0, uint32 bucketHash1, uint cuckooHash, uint32* hashTables, uint32* positionFound)
{
	const uint threadId = blockIdx.x * blockDim.x + threadIdx.x;

	if(threadId < keyCount)
	{
		uint k = keys[threadId];
		uint bucketId = ((bucketHash0 + bucketHash1 * k) % 1900813) % bucketCount;

		uint c00 = cuckooHash & 0x2B780D61;
		uint c01 = cuckooHash & 0xF07BE535;
		uint c10 = cuckooHash & 0x2B07D70A;
		uint c11 = cuckooHash & 0xABFB9D52;
		uint c20 = cuckooHash & 0x9D52B07D;
		uint c21 = cuckooHash & 0x8F5C28F6;

		uint p0 = (bucketId * 576 + 192 * 0) + (((c00 + c01 * k) % 1900813) % 192);
		uint p1 = (bucketId * 576 + 192 * 1) + (((c10 + c11 * k) % 1900813) % 192);
		uint p2 = (bucketId * 576 + 192 * 2) + (((c20 + c21 * k) % 1900813) % 192);

		if(hashTables[p0] == k)
		{
			positionFound[threadId] = p0;
		}
		else if(hashTables[p1] == k)
		{
			positionFound[threadId] = p1;
		}
		else if(hashTables[p2] == k)
		{
			positionFound[threadId] = p2;
		}
		else
		{
			positionFound[threadId] = UINT_MAX;
		}
	}
}

bool HashSetKernel_FindInParallel(uint32 keyCount, uint32* keys, uint32 bucketCount, uint32 bucketHash0, uint32 bucketHash1, uint cuckooHash, uint32* hashTables, uint32* positionFound)
{
	uint threads = 256;
	uint blocks  = keyCount / threads + ((keyCount % threads == 0) ? 0 : 1);

	HashSetKernel_FindInParallel_Impl<<<blocks, threads>>>(
			keyCount, keys, bucketCount, bucketHash0, bucketHash1, cuckooHash, hashTables, positionFound);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

} } } } }
