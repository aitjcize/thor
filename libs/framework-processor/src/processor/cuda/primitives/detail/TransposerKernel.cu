/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/processor/cuda/primitives/detail/CudaKernelCommon.h"

#define TRANSPOSER_THREAD_BLOCK_SIZE 	256
#define TRANSPOSER_BLOCK_DIM 			16

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail {

template<typename T>
__global__ void TransposerKernelImpl(T *odata, T *idata, uint32 width, uint32 height)
{
    __shared__ T block[TRANSPOSER_BLOCK_DIM][TRANSPOSER_BLOCK_DIM+1];

    // read the matrix tile into shared memory
    uint32 xIndex = blockIdx.x * TRANSPOSER_BLOCK_DIM + threadIdx.x;
    uint32 yIndex = blockIdx.y * TRANSPOSER_BLOCK_DIM + threadIdx.y;
    if((xIndex < width) && (yIndex < height))
    {
    	uint32 index_in = yIndex * width + xIndex;
        block[threadIdx.y][threadIdx.x] = idata[index_in];
    }

    __syncthreads();

    // write the transposed matrix tile to global memory
    xIndex = blockIdx.y * TRANSPOSER_BLOCK_DIM + threadIdx.x;
    yIndex = blockIdx.x * TRANSPOSER_BLOCK_DIM + threadIdx.y;
    if((xIndex < height) && (yIndex < width))
    {
        uint32 index_out = yIndex * height + xIndex;
        odata[index_out] = block[threadIdx.x][threadIdx.y];
    }
}

template<typename T>
__global__ void ExtendedTransposerKernelImpl(T *odata, T *idata, uint32 total_width, uint32 total_height, uint32 width, uint32 height)
{
    __shared__ T block[TRANSPOSER_BLOCK_DIM][TRANSPOSER_BLOCK_DIM+1];

    // read the matrix tile into shared memory
    uint32 xIndex = blockIdx.x * TRANSPOSER_BLOCK_DIM + threadIdx.x;
    uint32 yIndex = blockIdx.y * TRANSPOSER_BLOCK_DIM + threadIdx.y;
    if((xIndex < width) && (yIndex < height))
    {
    	uint32 index_in = yIndex * total_width + xIndex;
        block[threadIdx.y][threadIdx.x] = idata[index_in];
//        if(idata[index_in] < 32)
//        printf("(%d,%d)(%d,%d) shm[%d][%d] = idata[%d] = %d\n", blockIdx.x, blockIdx.y, threadIdx.x, threadIdx.y, threadIdx.y, threadIdx.x, index_in, idata[index_in]);
    }

    __syncthreads();

    // write the transposed matrix tile to global memory
    xIndex = blockIdx.y * TRANSPOSER_BLOCK_DIM + threadIdx.x;
    yIndex = blockIdx.x * TRANSPOSER_BLOCK_DIM + threadIdx.y;
    if((xIndex < height) && (yIndex < width))
    {
        uint32 index_out = yIndex * total_height + xIndex;
        odata[index_out] = block[threadIdx.x][threadIdx.y];
//        if(block[threadIdx.x][threadIdx.y] < 32)
//        printf("(%d,%d)(%d,%d) odata[%d] = shm[%d][%d] = %d\n", blockIdx.x, blockIdx.y, threadIdx.x, threadIdx.y, index_out, threadIdx.x, threadIdx.y, block[threadIdx.x][threadIdx.y]);
    }
}

// This naive transpose kernel suffers from completely non-coalesced writes.
// It can be up to 10x slower than the kernel above for large matrices.
template<typename T>
__global__ void TransposerKernelNaiveImpl(T *odata, T* idata, uint32 width, uint32 height)
{
   unsigned int xIndex = blockDim.x * blockIdx.x + threadIdx.x;
   unsigned int yIndex = blockDim.y * blockIdx.y + threadIdx.y;

   if (xIndex < width && yIndex < height)
   {
       uint32 index_in  = xIndex + width * yIndex;
       uint32 index_out = yIndex + height * xIndex;
       odata[index_out] = idata[index_in];
   }
}

bool TransposerKernel8(byte *output, byte* input, uint32 width, uint32 height)
{
	dim3 blocks;
	dim3 threads;

	blocks.x = width / TRANSPOSER_BLOCK_DIM + ((width % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.y = height / TRANSPOSER_BLOCK_DIM + ((height % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.z = 1;

	threads.x = TRANSPOSER_BLOCK_DIM;
	threads.y = TRANSPOSER_BLOCK_DIM;
	threads.z = 1;

	TransposerKernelImpl<uint8> <<<blocks, threads>>>((uint8*)output, (uint8*)input, width, height);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

bool ExtendedTransposerKernel8(byte *output, byte* input, uint32 total_width, uint32 total_height, uint32 width, uint32 height)
{
	dim3 blocks;
	dim3 threads;

	blocks.x = width / TRANSPOSER_BLOCK_DIM + ((width % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.y = height / TRANSPOSER_BLOCK_DIM + ((height % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.z = 1;

	threads.x = TRANSPOSER_BLOCK_DIM;
	threads.y = TRANSPOSER_BLOCK_DIM;
	threads.z = 1;

	ExtendedTransposerKernelImpl<uint8> <<<blocks, threads>>>((uint8*)output, (uint8*)input, total_width, total_height, width, height);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

bool TransposerKernel16(byte *output, byte* input, uint32 width, uint32 height)
{
	dim3 blocks;
	dim3 threads;

	blocks.x = width / TRANSPOSER_BLOCK_DIM + ((width % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.y = height / TRANSPOSER_BLOCK_DIM + ((height % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.z = 1;

	threads.x = TRANSPOSER_BLOCK_DIM;
	threads.y = TRANSPOSER_BLOCK_DIM;
	threads.z = 1;

	TransposerKernelImpl<uint16> <<<blocks, threads>>>((uint16*)output, (uint16*)input, width, height);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

bool ExtendedTransposerKernel16(byte *output, byte* input, uint32 total_width, uint32 total_height, uint32 width, uint32 height)
{
	dim3 blocks;
	dim3 threads;

	blocks.x = width / TRANSPOSER_BLOCK_DIM + ((width % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.y = height / TRANSPOSER_BLOCK_DIM + ((height % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.z = 1;

	threads.x = TRANSPOSER_BLOCK_DIM;
	threads.y = TRANSPOSER_BLOCK_DIM;
	threads.z = 1;

	ExtendedTransposerKernelImpl<uint16> <<<blocks, threads>>>((uint16*)output, (uint16*)input, total_width, total_height, width, height);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

bool TransposerKernel32(byte *output, byte* input, uint32 width, uint32 height)
{
	dim3 blocks;
	dim3 threads;

	blocks.x = width / TRANSPOSER_BLOCK_DIM + ((width % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.y = height / TRANSPOSER_BLOCK_DIM + ((height % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.z = 1;

	threads.x = TRANSPOSER_BLOCK_DIM;
	threads.y = TRANSPOSER_BLOCK_DIM;
	threads.z = 1;

	TransposerKernelImpl<uint32> <<<blocks, threads>>>((uint32*)output, (uint32*)input, width, height);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

bool ExtendedTransposerKernel32(byte *output, byte* input, uint32 total_width, uint32 total_height, uint32 width, uint32 height)
{
	dim3 blocks;
	dim3 threads;

	blocks.x = width / TRANSPOSER_BLOCK_DIM + ((width % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.y = height / TRANSPOSER_BLOCK_DIM + ((height % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.z = 1;

	threads.x = TRANSPOSER_BLOCK_DIM;
	threads.y = TRANSPOSER_BLOCK_DIM;
	threads.z = 1;

	ExtendedTransposerKernelImpl<uint32> <<<blocks, threads>>>((uint32*)output, (uint32*)input, total_width, total_height, width, height);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

bool TransposerKernel64(byte *output, byte* input, uint32 width, uint32 height)
{
	dim3 blocks;
	dim3 threads;

	blocks.x = width / TRANSPOSER_BLOCK_DIM + ((width % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.y = height / TRANSPOSER_BLOCK_DIM + ((height % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.z = 1;

	threads.x = TRANSPOSER_BLOCK_DIM;
	threads.y = TRANSPOSER_BLOCK_DIM;
	threads.z = 1;

	TransposerKernelImpl<uint64> <<<blocks, threads>>>((uint64*)output, (uint64*)input, width, height);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

bool ExtendedTransposerKernel64(byte *output, byte* input, uint32 total_width, uint32 total_height, uint32 width, uint32 height)
{
	dim3 blocks;
	dim3 threads;

	blocks.x = width / TRANSPOSER_BLOCK_DIM + ((width % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.y = height / TRANSPOSER_BLOCK_DIM + ((height % TRANSPOSER_BLOCK_DIM == 0) ? 0 : 1);
	blocks.z = 1;

	threads.x = TRANSPOSER_BLOCK_DIM;
	threads.y = TRANSPOSER_BLOCK_DIM;
	threads.z = 1;

	ExtendedTransposerKernelImpl<uint64> <<<blocks, threads>>>((uint64*)output, (uint64*)input, total_width, total_height, width, height);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

} } } } }
