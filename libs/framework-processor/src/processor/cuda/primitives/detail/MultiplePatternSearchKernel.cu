/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/processor/cuda/primitives/detail/CudaKernelCommon.h"
#include "framework/processor/cuda/primitives/detail/MultiplePatternSearcherKernel.h"
#include "framework/processor/cuda/primitives/HashMapApi.h"

//#define PRINT_DEBUG

HASHMAPAPI_DECLARE_GLOBAL;

//namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail {
extern "C"
{

union DummyConversion
{
#if BLOCK_STRIPE == 8
	zillians::uint64 u64;
#endif
	zillians::uint32 u32[2];
	zillians::uint16 u16[4];
	zillians::uint8  u8[8];
};

#if BLOCK_STRIPE == 4

__global__ void MultiplePatternSearchKernel_RabinKarp_Impl_NoSHM(
		HASHMAPAPI_DECLARE_PARAMETERS,
		zillians::uint32* textData, zillians::uint32 textLength, zillians::uint32 patternLength,
		zillians::uint32 hashComponent, zillians::uint32 hashComponentMax,
		zillians::uint32 matchedCountMax, zillians::uint32* matchedCount, zillians::uint32* matchedTextPositions, zillians::uint32* matchedPatternPosition)
{
#if ENABLE_SHARED_MEMORY_CONVERSION
	__shared__ DummyConversion conversion1[256];
	__shared__ DummyConversion conversion2[256];
#else
	zillians::uint32 block1;
	zillians::uint32 block2;
#endif

	const zillians::uint32 threadId = blockIdx.x * blockDim.x + threadIdx.x;

	// initialize hash map look-up
	HASHMAPAPI_KERNEL_INIT;

	// calculate the base hash
	zillians::uint32 currentHash = 0;

	if(threadIdx.x == 1)
	{
		currentHash = 0;
	}

	zillians::uint32 blockIndex1 = 0;
	zillians::uint32 blockIndex2 = patternLength / BLOCK_STRIPE;
	zillians::uint32 characterIndex1 = 0;
	zillians::uint32 characterIndex2 = patternLength % BLOCK_STRIPE;
	zillians::uint32 offsetIndex1 = 0;
	zillians::uint32 offsetIndex2 = 0;

	zillians::uint32 availableLength;
	zillians::uint32 charactersPerThread = BLOCK_PER_THREAD * BLOCK_STRIPE;
	zillians::uint32 base = threadId * charactersPerThread;
	zillians::uint32 mc = 0;
	availableLength = min( max((zillians::int32)textLength - (zillians::int32)base - (zillians::int32)patternLength, 0), charactersPerThread);

	for(zillians::uint32 i=0;i<availableLength;++i)
	{
		if(i==0)
		{
			zillians::uint32 component = 1;

			// note that patternLength is always greater then zero
			if(patternLength % BLOCK_STRIPE == 0)
			{
				zillians::uint32 currentBlock = blockIndex2;
				do
				{
#if ENABLE_SHARED_MEMORY_CONVERSION
					conversion1[threadIdx.x].u32 = textData[gridDim.x * blockDim.x * currentBlock + threadId];

					if(currentBlock == blockIndex2) conversion2[threadIdx.x].u32 = conversion1[threadIdx.x].u32;

					currentHash += conversion1[threadIdx.x].u8[3] * component;	component *= hashComponent;
					currentHash += conversion1[threadIdx.x].u8[2] * component;	component *= hashComponent;
					currentHash += conversion1[threadIdx.x].u8[1] * component;	component *= hashComponent;
					currentHash += conversion1[threadIdx.x].u8[0] * component;	component *= hashComponent;
#else
					block1 = textData[gridDim.x * blockDim.x * currentBlock + threadId];

					if(currentBlock == blockIndex2) block2 = block1;

					currentHash += (zillians::uint32)((block1 >> 24) & 0xFF) * component;	component *= hashComponent;
					currentHash += (zillians::uint32)((block1 >> 16) & 0xFF) * component;	component *= hashComponent;
					currentHash += (zillians::uint32)((block1 >> 8 ) & 0xFF) * component;	component *= hashComponent;
					currentHash += (zillians::uint32)((block1 >> 0 ) & 0xFF) * component;	component *= hashComponent;
#endif
					--currentBlock;
				}
				while(currentBlock != blockIndex1);
			}
			else
			{
				// handle the last block
				{
					zillians::uint32 current_base = blockIndex2 * BLOCK_STRIPE;
					zillians::uint32 x = patternLength - current_base;
#if ENABLE_SHARED_MEMORY_CONVERSION
					conversion2[threadIdx.x].u32 = textData[gridDim.x * blockDim.x * blockIndex2 + threadId];
					conversion1[threadIdx.x].u32 = conversion2[threadIdx.x].u32;

					if(x > 3) { currentHash += conversion1[threadIdx.x].u8[3] * component;	component *= hashComponent; }
					if(x > 2) { currentHash += conversion1[threadIdx.x].u8[2] * component;	component *= hashComponent; }
					if(x > 1) { currentHash += conversion1[threadIdx.x].u8[1] * component;	component *= hashComponent; }
					if(x > 0) { currentHash += conversion1[threadIdx.x].u8[0] * component;	component *= hashComponent; }	// Obvious, since patternLength % BLOCK_STRIPE != 0
#else
					block2 = textData[gridDim.x * blockDim.x * blockIndex2 + threadId];
					block1 = block2;

					if(x > 3) { currentHash += (zillians::uint32)((block1 >> 24) & 0xFF) * component; component *= hashComponent; }
					if(x > 2) { currentHash += (zillians::uint32)((block1 >> 16) & 0xFF) * component; component *= hashComponent; }
					if(x > 1) { currentHash += (zillians::uint32)((block1 >> 8 ) & 0xFF) * component; component *= hashComponent; }
					if(x > 0) { currentHash += (zillians::uint32)((block1 >> 0 ) & 0xFF) * component; component *= hashComponent; }	// Obvious, since patternLength % BLOCK_STRIPE != 0
#endif
				}

				if(blockIndex2 != blockIndex1)
				{
					zillians::uint32 currentBlock = blockIndex2;
					do
					{
						--currentBlock;

#if ENABLE_SHARED_MEMORY_CONVERSION
						conversion1[threadIdx.x].u32 = textData[gridDim.x * blockDim.x * currentBlock + threadId];

						currentHash += conversion1[threadIdx.x].u8[3] * component;	component *= hashComponent;
						currentHash += conversion1[threadIdx.x].u8[2] * component;	component *= hashComponent;
						currentHash += conversion1[threadIdx.x].u8[1] * component;	component *= hashComponent;
						currentHash += conversion1[threadIdx.x].u8[0] * component;	component *= hashComponent;
#else
						block1 = textData[gridDim.x * blockDim.x * currentBlock + threadId];

						currentHash += (zillians::uint32)((block1 >> 24) & 0xFF) * component;	component *= hashComponent;
						currentHash += (zillians::uint32)((block1 >> 16) & 0xFF) * component;	component *= hashComponent;
						currentHash += (zillians::uint32)((block1 >> 8 ) & 0xFF) * component;	component *= hashComponent;
						currentHash += (zillians::uint32)((block1 >> 0 ) & 0xFF) * component;	component *= hashComponent;
#endif
					}
					while(currentBlock != blockIndex1);
				}
			}
		}
		else
		{
			// shifting the patterns to match
#if ENABLE_SHARED_MEMORY_CONVERSION
			currentHash -= conversion1[threadIdx.x].u8[characterIndex1] * hashComponentMax;
			currentHash *= hashComponent;
			currentHash += conversion2[threadIdx.x].u8[characterIndex2];
#else
			zillians::uint8 c1 = ((block1 >> (characterIndex1*8)) & 0xFF);
			zillians::uint8 c2 = ((block2 >> (characterIndex2*8)) & 0xFF);
			currentHash -= (zillians::uint32)c1 * hashComponentMax;
			currentHash *= hashComponent;
			currentHash += (zillians::uint32)c2;
#endif
			// advance to the next character (and read next block if necessary)
			++characterIndex1; ++characterIndex2;
			if(characterIndex1 >= BLOCK_STRIPE)
			{
				++blockIndex1; characterIndex1 = 0;
				if(blockIndex1 >= BLOCK_PER_THREAD)
				{
					blockIndex1 = 0;
					++offsetIndex1;
				}

#if ENABLE_SHARED_MEMORY_CONVERSION
				conversion1[threadIdx.x].u32 = textData[gridDim.x * blockDim.x * blockIndex1 + threadId + offsetIndex1];
#else
				block1 = textData[gridDim.x * blockDim.x * blockIndex1 + threadId + offsetIndex1];
#endif
			}
			if(characterIndex2 >= BLOCK_STRIPE)
			{
				++blockIndex2; characterIndex2 = 0;
				if(blockIndex2 >= BLOCK_PER_THREAD)
				{
					blockIndex2 = 0;
					++offsetIndex2;
				}

#if ENABLE_SHARED_MEMORY_CONVERSION
				conversion2[threadIdx.x].u32 = textData[gridDim.x * blockDim.x * blockIndex2 + threadId + offsetIndex2];
#else
				block2 = textData[gridDim.x * blockDim.x * blockIndex2 + threadId + offsetIndex2];
#endif
			}
		}

		// check for matched
		zillians::uint32 value;
		zillians::uint32 position;
		HASHMAPAPI_FIND(currentHash, value, position);
		if(position != UINT_MAX)
		{
			// save the matched (possibly) position
			++mc;
			zillians::uint32 offset = atomicAdd(matchedCount, 1);
			if(offset < matchedCountMax)
			{
				matchedTextPositions[offset] = i + base;
				matchedPatternPosition[offset] = value;
			}
		}
	}
}

bool MultiplePatternSearchKernel_RabinKarp(
		HASHMAPAPI_DECLARE_PARAMETERS,
		zillians::uint32* textData, zillians::uint32 textLength, zillians::uint32 patternLength,
		zillians::uint32 hashComponent, zillians::uint32 hashComponentMax,
		zillians::uint32 matchedCountMax, zillians::uint32* matchedCount, zillians::uint32* matchedTextPositions, zillians::uint32* matchedPatternPosition)
{
	HASHMAPAPI_BEFORE_KERNEL_INIT;

	zillians::uint32 threads = 256;
	zillians::uint32 charactersPerThread = BLOCK_PER_THREAD * BLOCK_STRIPE;
	zillians::uint32 totalThreadsToProcess = textLength / charactersPerThread + ((textLength % charactersPerThread == 0) ? 0 : 1);
	zillians::uint32 blocks = totalThreadsToProcess / threads + ((totalThreadsToProcess % threads == 0) ? 0 : 1);

	// TODO choose different version (shared memory or no shared memory) based upon workingLength and patternLength
	MultiplePatternSearchKernel_RabinKarp_Impl_NoSHM<<<blocks, threads>>>(
			HASHMAPAPI_FORWARD_PARAMETERS,
			textData, textLength, patternLength,
			hashComponent, hashComponentMax,
			matchedCountMax, matchedCount, matchedTextPositions, matchedPatternPosition);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

#elif BLOCK_STRIPE == 8
__global__ void MultiplePatternSearchKernel_RabinKarp_Impl_NoSHM(
		HASHMAPAPI_DECLARE_PARAMETERS,
		zillians::uint64* textData, zillians::uint32 textLength, zillians::uint32 patternLength,
		zillians::uint32 hashComponent, zillians::uint32 hashComponentMax,
		zillians::uint32 matchedCountMax, zillians::uint32* matchedCount, zillians::uint32* matchedTextPositions, zillians::uint32* matchedPatternPosition)
{
#if ENABLE_SHARED_MEMORY_CONVERSION
	__shared__ DummyConversion conversion1[256];
	__shared__ DummyConversion conversion2[256];
#else
	zillians::uint64 block1;
	zillians::uint64 block2;
#endif

	const zillians::uint32 threadId = blockIdx.x * blockDim.x + threadIdx.x;

	// initialize hash map look-up
	HASHMAPAPI_KERNEL_INIT;

	// calculate the base hash
	zillians::uint32 currentHash = 0;

	zillians::uint32 blockIndex1 = 0;
	zillians::uint32 blockIndex2 = patternLength / BLOCK_STRIPE;
	zillians::uint32 characterIndex1 = 0;
	zillians::uint32 characterIndex2 = patternLength % BLOCK_STRIPE;
	zillians::uint32 offsetIndex1 = 0;
	zillians::uint32 offsetIndex2 = 0;

	zillians::uint32 availableLength;
	zillians::uint32 charactersPerThread = BLOCK_PER_THREAD * BLOCK_STRIPE;
	zillians::uint32 base = threadId * charactersPerThread;
	zillians::uint32 mc = 0;
	availableLength = min( max((zillians::int32)textLength - (zillians::int32)base - (zillians::int32)patternLength, 0), charactersPerThread);

#ifdef PRINT_DEBUG
	printf("[threadId = %d] textLength = %d, base = %d, patternLength = %d, availableLength = %d\n", threadId, textLength, base, patternLength, availableLength);
#endif

	// the last thread should handle one more elements
	if(availableLength > 0 && availableLength < charactersPerThread)
	{
		++availableLength;
	}

	if(threadId == 0 && availableLength == 0 && textLength == patternLength)
	{
		availableLength = 1;
	}

	// loop over
	for(zillians::uint32 i=0;i<availableLength;++i)
	{
		if(i==0)
		{
			zillians::uint32 component = 1;

#ifdef PRINT_DEBUG
			printf("[threadId = %d] base text = ", threadId);
#endif

			// note that patternLength is always greater then zero
			if(patternLength % BLOCK_STRIPE == 0)
			{
				zillians::uint32 currentBlock = blockIndex2;
				do
				{
#if ENABLE_SHARED_MEMORY_CONVERSION
					conversion1[threadIdx.x].u64 = textData[gridDim.x * blockDim.x * currentBlock + threadId];

					if(currentBlock == blockIndex2) conversion2[threadIdx.x].u64 = conversion1[threadIdx.x].u64;

					currentHash += conversion1[threadIdx.x].u8[7] * component;	component *= hashComponent;
					currentHash += conversion1[threadIdx.x].u8[6] * component;	component *= hashComponent;
					currentHash += conversion1[threadIdx.x].u8[5] * component;	component *= hashComponent;
					currentHash += conversion1[threadIdx.x].u8[4] * component;	component *= hashComponent;
					currentHash += conversion1[threadIdx.x].u8[3] * component;	component *= hashComponent;
					currentHash += conversion1[threadIdx.x].u8[2] * component;	component *= hashComponent;
					currentHash += conversion1[threadIdx.x].u8[1] * component;	component *= hashComponent;
					currentHash += conversion1[threadIdx.x].u8[0] * component;	component *= hashComponent;

#ifdef PRINT_DEBUG
					printf("%c%c%c%c%c%c%c%c",
							conversion1[threadIdx.x].u8[0],
							conversion1[threadIdx.x].u8[1],
							conversion1[threadIdx.x].u8[2],
							conversion1[threadIdx.x].u8[3],
							conversion1[threadIdx.x].u8[4],
							conversion1[threadIdx.x].u8[5],
							conversion1[threadIdx.x].u8[6],
							conversion1[threadIdx.x].u8[7]);
#endif

#else
					block1 = textData[gridDim.x * blockDim.x * currentBlock + threadId];

					if(currentBlock == blockIndex2) block2 = block1;

					currentHash += (zillians::uint32)((block1 >> 56) & 0xFF) * component;	component *= hashComponent;
					currentHash += (zillians::uint32)((block1 >> 48) & 0xFF) * component;	component *= hashComponent;
					currentHash += (zillians::uint32)((block1 >> 40) & 0xFF) * component;	component *= hashComponent;
					currentHash += (zillians::uint32)((block1 >> 32) & 0xFF) * component;	component *= hashComponent;
					currentHash += (zillians::uint32)((block1 >> 24) & 0xFF) * component;	component *= hashComponent;
					currentHash += (zillians::uint32)((block1 >> 16) & 0xFF) * component;	component *= hashComponent;
					currentHash += (zillians::uint32)((block1 >> 8 ) & 0xFF) * component;	component *= hashComponent;
					currentHash += (zillians::uint32)((block1 >> 0 ) & 0xFF) * component;	component *= hashComponent;
#endif
					--currentBlock;
				}
				while(currentBlock != blockIndex1);
			}
			else
			{
				// handle the last block
				{
					zillians::uint32 current_base = blockIndex2 * BLOCK_STRIPE;
					zillians::uint32 x = patternLength - current_base;
#if ENABLE_SHARED_MEMORY_CONVERSION
					conversion2[threadIdx.x].u64 = textData[gridDim.x * blockDim.x * blockIndex2 + threadId];
					conversion1[threadIdx.x].u64 = conversion2[threadIdx.x].u64;

					if(x > 7) { currentHash += conversion1[threadIdx.x].u8[7] * component;	component *= hashComponent; }
					if(x > 6) { currentHash += conversion1[threadIdx.x].u8[6] * component;	component *= hashComponent; }
					if(x > 5) { currentHash += conversion1[threadIdx.x].u8[5] * component;	component *= hashComponent; }
					if(x > 4) { currentHash += conversion1[threadIdx.x].u8[4] * component;	component *= hashComponent; }
					if(x > 3) { currentHash += conversion1[threadIdx.x].u8[3] * component;	component *= hashComponent; }
					if(x > 2) { currentHash += conversion1[threadIdx.x].u8[2] * component;	component *= hashComponent; }
					if(x > 1) { currentHash += conversion1[threadIdx.x].u8[1] * component;	component *= hashComponent; }
					if(x > 0) { currentHash += conversion1[threadIdx.x].u8[0] * component;	component *= hashComponent; }	// Obvious, since patternLength % BLOCK_STRIPE != 0

#ifdef PRINT_DEBUG
//					printf("%x:", conversion1[threadIdx.x].u64);
					if(x > 0) { printf("%c", conversion1[threadIdx.x].u8[0]); }
					if(x > 1) { printf("%c", conversion1[threadIdx.x].u8[1]); }
					if(x > 2) { printf("%c", conversion1[threadIdx.x].u8[2]); }
					if(x > 3) { printf("%c", conversion1[threadIdx.x].u8[3]); }
					if(x > 4) { printf("%c", conversion1[threadIdx.x].u8[4]); }
					if(x > 5) { printf("%c", conversion1[threadIdx.x].u8[5]); }
					if(x > 6) { printf("%c", conversion1[threadIdx.x].u8[6]); }
					if(x > 7) { printf("%c", conversion1[threadIdx.x].u8[7]); }	// Obvious, since patternLength % BLOCK_STRIPE != 0
#endif

#else
					block2 = textData[gridDim.x * blockDim.x * blockIndex2 + threadId];
					block1 = block2;

					if(x > 7) { currentHash += (zillians::uint32)((block1 >> 56) & 0xFF) * component; component *= hashComponent; }
					if(x > 6) { currentHash += (zillians::uint32)((block1 >> 48) & 0xFF) * component; component *= hashComponent; }
					if(x > 5) { currentHash += (zillians::uint32)((block1 >> 40) & 0xFF) * component; component *= hashComponent; }
					if(x > 4) { currentHash += (zillians::uint32)((block1 >> 32) & 0xFF) * component; component *= hashComponent; }
					if(x > 3) { currentHash += (zillians::uint32)((block1 >> 24) & 0xFF) * component; component *= hashComponent; }
					if(x > 2) { currentHash += (zillians::uint32)((block1 >> 16) & 0xFF) * component; component *= hashComponent; }
					if(x > 1) { currentHash += (zillians::uint32)((block1 >> 8 ) & 0xFF) * component; component *= hashComponent; }
					if(x > 0) { currentHash += (zillians::uint32)((block1 >> 0 ) & 0xFF) * component; component *= hashComponent; }	// Obvious, since patternLength % BLOCK_STRIPE != 0
#endif
				}

				if(blockIndex2 != blockIndex1)
				{
					zillians::uint32 currentBlock = blockIndex2;
					do
					{
						--currentBlock;

#if ENABLE_SHARED_MEMORY_CONVERSION
						conversion1[threadIdx.x].u64 = textData[gridDim.x * blockDim.x * currentBlock + threadId];

						currentHash += conversion1[threadIdx.x].u8[7] * component;	component *= hashComponent;
						currentHash += conversion1[threadIdx.x].u8[6] * component;	component *= hashComponent;
						currentHash += conversion1[threadIdx.x].u8[5] * component;	component *= hashComponent;
						currentHash += conversion1[threadIdx.x].u8[4] * component;	component *= hashComponent;
						currentHash += conversion1[threadIdx.x].u8[3] * component;	component *= hashComponent;
						currentHash += conversion1[threadIdx.x].u8[2] * component;	component *= hashComponent;
						currentHash += conversion1[threadIdx.x].u8[1] * component;	component *= hashComponent;
						currentHash += conversion1[threadIdx.x].u8[0] * component;	component *= hashComponent;

#ifdef PRINT_DEBUG
//						printf("%x:", conversion1[threadIdx.x].u64);
						printf("%c%c%c%c%c%c%c%c",
								conversion1[threadIdx.x].u8[0],
								conversion1[threadIdx.x].u8[1],
								conversion1[threadIdx.x].u8[2],
								conversion1[threadIdx.x].u8[3],
								conversion1[threadIdx.x].u8[4],
								conversion1[threadIdx.x].u8[5],
								conversion1[threadIdx.x].u8[6],
								conversion1[threadIdx.x].u8[7]);
#endif

#else
						block1 = textData[gridDim.x * blockDim.x * currentBlock + threadId];

						currentHash += (zillians::uint32)((block1 >> 56) & 0xFF) * component;	component *= hashComponent;
						currentHash += (zillians::uint32)((block1 >> 48) & 0xFF) * component;	component *= hashComponent;
						currentHash += (zillians::uint32)((block1 >> 40) & 0xFF) * component;	component *= hashComponent;
						currentHash += (zillians::uint32)((block1 >> 32) & 0xFF) * component;	component *= hashComponent;
						currentHash += (zillians::uint32)((block1 >> 24) & 0xFF) * component;	component *= hashComponent;
						currentHash += (zillians::uint32)((block1 >> 16) & 0xFF) * component;	component *= hashComponent;
						currentHash += (zillians::uint32)((block1 >> 8 ) & 0xFF) * component;	component *= hashComponent;
						currentHash += (zillians::uint32)((block1 >> 0 ) & 0xFF) * component;	component *= hashComponent;
#endif
					}
					while(currentBlock != blockIndex1);
				}
			}
#ifdef PRINT_DEBUG
			printf("\n");
#endif
		}
		else
		{
			// shifting the patterns to match
#if ENABLE_SHARED_MEMORY_CONVERSION
			currentHash -= conversion1[threadIdx.x].u8[characterIndex1] * hashComponentMax;
			currentHash *= hashComponent;
			currentHash += conversion2[threadIdx.x].u8[characterIndex2];
#ifdef PRINT_DEBUG
			printf("[threadId = %d] blockIndex1 = %d, blockIndex2 = %d, characterIndex1 = %d, characterIndex2 = %d\n", threadId, blockIndex1, blockIndex2, characterIndex1, characterIndex2);
			printf("[threadId = %d] prev character = %c, next character = %c\n", threadId, conversion1[threadIdx.x].u8[characterIndex1], conversion2[threadIdx.x].u8[characterIndex2]);
#endif
#else
			zillians::uint8 c1 = ((block1 >> (characterIndex1*8)) & 0xFF);
			zillians::uint8 c2 = ((block2 >> (characterIndex2*8)) & 0xFF);
			currentHash -= (zillians::uint32)c1 * hashComponentMax;
			currentHash *= hashComponent;
			currentHash += (zillians::uint32)c2;
#endif
			// advance to the next character (and read next block if necessary)
			++characterIndex1; ++characterIndex2;
			if(characterIndex1 >= BLOCK_STRIPE)
			{
				++blockIndex1; characterIndex1 = 0;
				if(blockIndex1 >= BLOCK_PER_THREAD)
				{
					blockIndex1 = 0;
					++offsetIndex1;
				}

#if ENABLE_SHARED_MEMORY_CONVERSION
				conversion1[threadIdx.x].u64 = textData[gridDim.x * blockDim.x * blockIndex1 + threadId + offsetIndex1];
#else
				block1 = textData[gridDim.x * blockDim.x * blockIndex1 + threadId + offsetIndex1];
#endif
			}
			if(characterIndex2 >= BLOCK_STRIPE)
			{
				++blockIndex2; characterIndex2 = 0;
				if(blockIndex2 >= BLOCK_PER_THREAD)
				{
					blockIndex2 = 0;
					++offsetIndex2;
				}

#if ENABLE_SHARED_MEMORY_CONVERSION
				conversion2[threadIdx.x].u64 = textData[gridDim.x * blockDim.x * blockIndex2 + threadId + offsetIndex2];
#else
				block2 = textData[gridDim.x * blockDim.x * blockIndex2 + threadId + offsetIndex2];
#endif
			}
		}

		// check for matched
		zillians::uint32 value;
		zillians::uint32 position;
		HASHMAPAPI_FIND(currentHash, value, position);

#ifdef PRINT_DEBUG
		printf("[threadId = %d] i= %d, availableLength = %d, currentHash = %d, position = %d\n", threadId, i, availableLength, currentHash, position);
#endif

		if(position != UINT_MAX)
		{
			// save the matched (possibly) position
			++mc;
			zillians::uint32 offset = atomicAdd(matchedCount, 1);
			if(offset < matchedCountMax)
			{
				matchedTextPositions[offset] = i + base;
				matchedPatternPosition[offset] = value;
			}
		}
	}
}

bool MultiplePatternSearchKernel_RabinKarp(
		HASHMAPAPI_DECLARE_PARAMETERS,
		zillians::uint64* textData, zillians::uint32 textLength, zillians::uint32 patternLength,
		zillians::uint32 hashComponent, zillians::uint32 hashComponentMax,
		zillians::uint32 matchedCountMax, zillians::uint32* matchedCount, zillians::uint32* matchedTextPositions, zillians::uint32* matchedPatternPosition)
{
	HASHMAPAPI_BEFORE_KERNEL_INIT;

	zillians::uint32 threads = 256;
	zillians::uint32 charactersPerThread = BLOCK_PER_THREAD * BLOCK_STRIPE;
	zillians::uint32 totalThreadsToProcess = textLength / charactersPerThread + ((textLength % charactersPerThread == 0) ? 0 : 1);
	zillians::uint32 blocks = totalThreadsToProcess / threads + ((totalThreadsToProcess % threads == 0) ? 0 : 1);

//	printf("textLength = %d, blocks = %d\n", textLength, blocks);

	// TODO choose different version (shared memory or no shared memory) based upon workingLength and patternLength
	MultiplePatternSearchKernel_RabinKarp_Impl_NoSHM<<<blocks, threads>>>(
			HASHMAPAPI_FORWARD_PARAMETERS,
			textData, textLength, patternLength,
			hashComponent, hashComponentMax,
			matchedCountMax, matchedCount, matchedTextPositions, matchedPatternPosition);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s\n", cudaGetErrorString(error));
		return false;
	}

	return true;
}

#endif

__global__ void MultiplePatternSearchKernel_FilterExactMatch_Impl(
		zillians::uint64* textData, zillians::uint32 textLength, zillians::uint32 patternLength,
		zillians::uint32 matchedCount, zillians::uint32* matchedTextPositions, zillians::uint32* matchedPatternPosition)
{
	//const zillians::uint32 threadId = blockIdx.x * blockDim.x + threadIdx.x;

}

bool MultiplePatternSearchKernel_FilterExactMatch(
		zillians::uint64* textData, zillians::uint32 textLength, zillians::uint32 patternLength,
		zillians::uint32 matchedCount, zillians::uint32* matchedTextPositions, zillians::uint32* matchedPatternPosition)
{
	zillians::uint32 threads = 256;
	zillians::uint32 blocks = matchedCount / threads + ((matchedCount % threads == 0) ? 0 : 1);

	MultiplePatternSearchKernel_FilterExactMatch_Impl<<<blocks, threads>>>(textData, textLength, patternLength, matchedCount, matchedTextPositions, matchedPatternPosition);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

//} } } } }
}
