/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/processor/cuda/primitives/detail/KNNSearchKernel.h"
#include <iostream>
#include <stdint.h>

//#define DEBUG
#define MAX_THREADS_PER_BLOCK          512 // must hard-code, limited by arch (threads per block   <= 512 ?? )
#define MAX_JOBS_PER_BLOCK_EDGE        16  // must hard-code, limited by arch (block shared memory <= 16k ?? )
#define MIN(a, b)                      (((a)<(b))?(a):(b))
#define ILLEGAL_INDEX                  (-1)
#define ALIGN_UP(offset, alignment)    ((offset) = ((offset)+(alignment)-1) & ~((alignment)-1))
#define OFFSET2D(minor, major, stride) ((major)*(stride)+(minor))
#define OFFSET3D(minor, major, super_major, stride, super_stride) \
    ((super_major)*(super_stride)+OFFSET2D(minor, major, stride))

#define ENABLE_BLOCK_PADDING

//=================
// Index
//=================

// 0. Utility API
// 1. Add Kernel
// 2. Remove Kernel
// 3. Update Kernel
// 4. Step Kernel
// 4.0 - Step Kernel 0: Naive Implementation
//       << Defunct >>
// 4.1 - Step Kernel 1: Two Kernels
// 4.2 - Step Kernel 2: Shared Memory + Two Kernels
// 4.3 - Step Kernel 3: Dynamic Shared Memory + Two Kernels
// 4.4 - Step Kernel 4: Hybrid Kernel (no dedicated kernel)
// 5. Search Kernel
// 5.0 - Search Kernel 0: Naive Implementation
// 5.1 - Search Kernel 1: Dynamic Shared Memory
//       << Defunct >>
// 5.2 - Search Kernel 2: Dynamic Shared Memory + 3D Input Iterator
// 5.3 - Search Kernel 3: Dynamic Shared Memory + 3D Input Iterator + Coalesced Write
// 5.4 - Search Kernel 4: Texture Memory + 3D Input Iterator + Coalesced Write
// 6. Texture Memory Writeback Kernel
// 7. Kernel Wrapper

// NOTE: "texture" must reside in global namespace
//       --> http://forums.nvidia.com/index.php?showtopic=163545
texture<float2,   2, cudaReadModeElementType> CellPosTex(  false, cudaFilterModePoint, cudaAddressModeClamp);
texture<uint32_t, 2, cudaReadModeElementType> CellIDTex(   false, cudaFilterModePoint, cudaAddressModeClamp);
texture<uint32_t, 2, cudaReadModeElementType> CellFreeTex( false, cudaFilterModePoint, cudaAddressModeClamp);
texture<uint32_t, 2, cudaReadModeElementType> InputTex(    false, cudaFilterModePoint, cudaAddressModeClamp);

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail {

//=====================================
// 0. Utility API
//=====================================

template<class T>
__device__ inline void Swap(T &a, T &b)
{
    T c = a;
    a = b;
    b = c;
}

template<class T>
__device__ inline void UpdateIndexPtrArr(
    uint64 *pIndexPtrArr, T Index, T NewIndex
    )
{
    if(pIndexPtrArr[Index] != 0x0)
        *(reinterpret_cast<T*>(pIndexPtrArr[Index])) = NewIndex;
}

//=====================================
// 1. Add Kernel
//=====================================

__global__ void KNNSearchKernel_Add_Impl(
    /*IN*/    uint32 *pAddCellIDArr,          // array of IDs              corresponding to add requests
    /*IN*/    float2 *pAddCellPosArr,         // array of positions        corresponding to add requests
    /*IN*/    uint64 *pAddCellIndexPtrArr,    // external array of indices corresponding to add requests
              uint32  AddCount,               // # of add requests
    /*OUT*/   uint32 *pState_CellIDArr,       // internal state
    /*OUT*/   float2 *pState_CellPosArr,      // internal state
    /*INOUT*/ uint64 *pState_CellIndexPtrArr, // internal state
    /*INOUT*/ uint32 *pState_CellFreeArr,     // internal state
              uint32  State_CellCount,        // internal state
    /*IN*/    uint32 *pTemp_PrefixSumArr,     // temp
    /*INOUT*/ uint32 *pTemp_WriteIndexArr     // temp
    )
{
    uint32 AddIndex = OFFSET2D(threadIdx.x, blockIdx.x, blockDim.x);
    bool IsValidCellIndex = AddIndex<State_CellCount;
    // Step 3: generate WriteIndex array from PrefixSum array
    if(IsValidCellIndex && pState_CellFreeArr[AddIndex] == 1)
        pTemp_WriteIndexArr[pTemp_PrefixSumArr[AddIndex]] = AddIndex;
    __syncthreads();
    // Step 4: write to cell using WriteIndex array
    if(IsValidCellIndex && AddIndex<AddCount)
    {
        uint32 WriteIndex = pTemp_WriteIndexArr[AddIndex];
        if(pState_CellFreeArr[WriteIndex] == 1) // if free cell
        {
            pState_CellIDArr       [WriteIndex] = pAddCellIDArr       [AddIndex];
            pState_CellPosArr      [WriteIndex] = pAddCellPosArr      [AddIndex];
            pState_CellIndexPtrArr [WriteIndex] = pAddCellIndexPtrArr [AddIndex]; // set pointer
            UpdateIndexPtrArr(
                pState_CellIndexPtrArr,
                WriteIndex, WriteIndex
                ); // set memory (must verify)
            pState_CellFreeArr[WriteIndex] = 0; // occupy it
        }
    }
}

//=====================================
// 2. Remove Kernel
//=====================================

__global__ void KNNSearchKernel_Remove_Impl(
    /*IN*/    uint32 *pRemoveCellIndexArr,    // array of indices corresponding to remove requests
              uint32  RemoveCount,            // # of remove requests
              bool    IsUpdateCellIndexPtr,   // flag indicating whether to update external array
    /*OUT*/   uint32 *pState_CellIDArr,       // internal state
    /*OUT*/   float2 *pState_CellPosArr,      // internal state
    /*INOUT*/ uint64 *pState_CellIndexPtrArr, // internal state
    /*INOUT*/ uint32 *pState_CellFreeArr,     // internal state
              uint32  State_CellCount         // internal state
    )
{
    uint32 RemoveIndex = OFFSET2D(threadIdx.x, blockIdx.x, blockDim.x);
    if(RemoveIndex >= State_CellCount)
        return;
    if(RemoveIndex >= RemoveCount)
        return;
    uint32 WriteIndex = pRemoveCellIndexArr[RemoveIndex];
    if(pState_CellFreeArr[WriteIndex] != 0) // if occupied cell
        return;
    pState_CellIDArr  [WriteIndex]   = 0;
    pState_CellPosArr [WriteIndex].x = 0;
    pState_CellPosArr [WriteIndex].y = 0;
    if(IsUpdateCellIndexPtr)
        UpdateIndexPtrArr(
            pState_CellIndexPtrArr,
            WriteIndex, static_cast<uint32>(ILLEGAL_INDEX)
            ); // reset memory (must verify)
    pState_CellIndexPtrArr [WriteIndex] = 0x0; // reset pointer
    pState_CellFreeArr     [WriteIndex] = 1;   // free it
}

//=====================================
// 3. Update Kernel
//=====================================

__global__ void KNNSearchKernel_Update_Impl(
    /*IN*/  uint32 *pUpdateCellIndexArr, // array of indices   corresponding to update requests
    /*IN*/  float2 *pUpdateCellPosArr,   // array of positions corresponding to update requests
            uint32  UpdateCount,         // # of update requests
    /*OUT*/ float2 *pState_CellPosArr,   // internal state
            uint32  State_CellCount      // internal state
    )
{
    uint32 UpdateIndex = OFFSET2D(threadIdx.x, blockIdx.x, blockDim.x);
    if(UpdateIndex >= State_CellCount)
        return;
    if(UpdateIndex<UpdateCount)
        pState_CellPosArr[pUpdateCellIndexArr[UpdateIndex]] = pUpdateCellPosArr[UpdateIndex];
}

//=====================================
// 4. Step Kernel
//=====================================

//=============================================================================
// 4.0 - Step Kernel 0: Naive Implementation
//=============================================================================

// NOTE: fails for large input (max: 176x176, 1024x1)

// [Kernel Summary]
// - input iterator dimension ?                ... 2-d
// - use threadIdx/blockIdx instead of loops ? ... yes

__global__ void KNNSearchKernel_Step_Naive_Impl(
    /*INOUT*/ uint32 *pState_CellIDArr,       // internal state
    /*INOUT*/ float2 *pState_CellPosArr,      // internal state
    /*INOUT*/ uint64 *pState_CellIndexPtrArr, // internal state
    /*INOUT*/ uint32 *pState_CellFreeArr,     // internal state
              uint32  State_ColCount,         // internal state
              uint32  State_RowCount          // internal state
    )
{
    uint32 ColIndex     = OFFSET2D(threadIdx.x, blockIdx.x, blockDim.x);
    uint32 RowIndex     = OFFSET2D(threadIdx.y, blockIdx.y, blockDim.y);
    uint32 MemCellIndex = OFFSET2D(ColIndex, RowIndex, State_ColCount);
    bool IsValidColIndex = ColIndex<State_ColCount;
    bool IsValidRowIndex = RowIndex<State_RowCount;
    if(
        IsValidRowIndex &&        // bounds test
        ColIndex%2 == 0 &&        // sort even columns
        ColIndex<State_ColCount-1 // ignore final column
        )
    {
        uint32 LeftIndex  = MemCellIndex; //OFFSET2D(ColIndex+0, RowIndex, State_ColCount);
        uint32 RightIndex = LeftIndex+1;  //OFFSET2D(ColIndex+1, RowIndex, State_ColCount);
        if(pState_CellPosArr[LeftIndex].x>pState_CellPosArr[RightIndex].x) // if need sort
        {
            Swap(pState_CellIDArr       [LeftIndex], pState_CellIDArr       [RightIndex]);
            Swap(pState_CellPosArr      [LeftIndex], pState_CellPosArr      [RightIndex]);
            Swap(pState_CellIndexPtrArr [LeftIndex], pState_CellIndexPtrArr [RightIndex]);
            Swap(pState_CellFreeArr     [LeftIndex], pState_CellFreeArr     [RightIndex]);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, LeftIndex,  LeftIndex);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, RightIndex, RightIndex);
        }
    }
    __syncthreads();
    if(
        IsValidRowIndex &&        // bounds test
        ColIndex%2 == 1 &&        // sort odd columns
        ColIndex<State_ColCount-1 // ignore final column
        )
    {
        uint32 LeftIndex  = MemCellIndex; //OFFSET2D(ColIndex+0, RowIndex, State_ColCount);
        uint32 RightIndex = LeftIndex+1;  //OFFSET2D(ColIndex+1, RowIndex, State_ColCount);
        if(pState_CellPosArr[LeftIndex].x>pState_CellPosArr[RightIndex].x) // if need sort
        {
            Swap(pState_CellIDArr       [LeftIndex], pState_CellIDArr       [RightIndex]);
            Swap(pState_CellPosArr      [LeftIndex], pState_CellPosArr      [RightIndex]);
            Swap(pState_CellIndexPtrArr [LeftIndex], pState_CellIndexPtrArr [RightIndex]);
            Swap(pState_CellFreeArr     [LeftIndex], pState_CellFreeArr     [RightIndex]);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, LeftIndex,  LeftIndex);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, RightIndex, RightIndex);
        }
    }
    __syncthreads();
    if(
        IsValidColIndex &&        // bounds test
        RowIndex%2 == 0 &&        // sort even rows
        RowIndex<State_RowCount-1 // ignore final row
        )
    {
        uint32 LeftIndex  = MemCellIndex;             //OFFSET2D(ColIndex, RowIndex+0, State_ColCount);
        uint32 RightIndex = LeftIndex+State_ColCount; //OFFSET2D(ColIndex, RowIndex+1, State_ColCount);
        if(pState_CellPosArr[LeftIndex].y>pState_CellPosArr[RightIndex].y) // if need sort
        {
            Swap(pState_CellIDArr       [LeftIndex], pState_CellIDArr       [RightIndex]);
            Swap(pState_CellPosArr      [LeftIndex], pState_CellPosArr      [RightIndex]);
            Swap(pState_CellIndexPtrArr [LeftIndex], pState_CellIndexPtrArr [RightIndex]);
            Swap(pState_CellFreeArr     [LeftIndex], pState_CellFreeArr     [RightIndex]);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, LeftIndex,  LeftIndex);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, RightIndex, RightIndex);
        }
    }
    __syncthreads();
    if(
        IsValidColIndex &&        // bounds test
        RowIndex%2 == 1 &&        // sort odd rows
        RowIndex<State_RowCount-1 // ignore final row
        )
    {
        uint32 LeftIndex  = MemCellIndex;             //OFFSET2D(ColIndex, RowIndex+0, State_ColCount);
        uint32 RightIndex = LeftIndex+State_ColCount; //OFFSET2D(ColIndex, RowIndex+1, State_ColCount);
        if(pState_CellPosArr[LeftIndex].y>pState_CellPosArr[RightIndex].y) // if need sort
        {
            Swap(pState_CellIDArr       [LeftIndex], pState_CellIDArr       [RightIndex]);
            Swap(pState_CellPosArr      [LeftIndex], pState_CellPosArr      [RightIndex]);
            Swap(pState_CellIndexPtrArr [LeftIndex], pState_CellIndexPtrArr [RightIndex]);
            Swap(pState_CellFreeArr     [LeftIndex], pState_CellFreeArr     [RightIndex]);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, LeftIndex,  LeftIndex);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, RightIndex, RightIndex);
        }
    }
}

//=============================================================================
// 4.1 - Step Kernel 1: Two Kernels
//=============================================================================

// [Kernel Summary]
// - input iterator dimension ?                ... 2-d
// - use threadIdx/blockIdx instead of loops ? ... yes

template<bool SwapEven>
__global__ void KNNSearchKernel_Step_1_Col_Impl(
    /*INOUT*/ uint32 *pState_CellIDArr,       // internal state
    /*INOUT*/ float2 *pState_CellPosArr,      // internal state
    /*INOUT*/ uint64 *pState_CellIndexPtrArr, // internal state
    /*INOUT*/ uint32 *pState_CellFreeArr,     // internal state
              uint32  State_ColCount,         // internal state
              uint32  State_RowCount          // internal state
    )
{
    uint32 ColIndex     = OFFSET2D(threadIdx.x, blockIdx.x, blockDim.x);
    uint32 RowIndex     = OFFSET2D(threadIdx.y, blockIdx.y, blockDim.y);
    uint32 MemCellIndex = OFFSET2D(ColIndex, RowIndex, State_ColCount);
    if(RowIndex >= State_RowCount) // bounds test
        return;
    if(
        ColIndex%2 == (SwapEven?0:1) && // sort even/odd columns
        ColIndex<State_ColCount-1       // ignore final column
        )
    {
        uint32 LeftIndex  = MemCellIndex; //OFFSET2D(ColIndex+0, RowIndex, State_ColCount);
        uint32 RightIndex = LeftIndex+1;  //OFFSET2D(ColIndex+1, RowIndex, State_ColCount);
        if(pState_CellPosArr[LeftIndex].x>pState_CellPosArr[RightIndex].x) // if need sort
        {
            Swap(pState_CellIDArr       [LeftIndex], pState_CellIDArr       [RightIndex]);
            Swap(pState_CellPosArr      [LeftIndex], pState_CellPosArr      [RightIndex]);
            Swap(pState_CellIndexPtrArr [LeftIndex], pState_CellIndexPtrArr [RightIndex]);
            Swap(pState_CellFreeArr     [LeftIndex], pState_CellFreeArr     [RightIndex]);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, LeftIndex,  LeftIndex);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, RightIndex, RightIndex);
        }
    }
}
template<bool SwapEven>
__global__ void KNNSearchKernel_Step_1_Row_Impl(
    /*INOUT*/ uint32 *pState_CellIDArr,       // internal state
    /*INOUT*/ float2 *pState_CellPosArr,      // internal state
    /*INOUT*/ uint64 *pState_CellIndexPtrArr, // internal state
    /*INOUT*/ uint32 *pState_CellFreeArr,     // internal state
              uint32  State_ColCount,         // internal state
              uint32  State_RowCount          // internal state
    )
{
    uint32 ColIndex     = OFFSET2D(threadIdx.x, blockIdx.x, blockDim.x);
    uint32 RowIndex     = OFFSET2D(threadIdx.y, blockIdx.y, blockDim.y);
    uint32 MemCellIndex = OFFSET2D(ColIndex, RowIndex, State_ColCount);
    if(ColIndex >= State_ColCount) // bounds test
        return;
    if(
        RowIndex%2 == (SwapEven?0:1) && // sort even/odd rows
        RowIndex<State_RowCount-1       // ignore final row
        )
    {
        uint32 LeftIndex  = MemCellIndex;             //OFFSET2D(ColIndex, RowIndex+0, State_ColCount);
        uint32 RightIndex = LeftIndex+State_ColCount; //OFFSET2D(ColIndex, RowIndex+1, State_ColCount);
        if(pState_CellPosArr[LeftIndex].y>pState_CellPosArr[RightIndex].y) // if need sort
        {
            Swap(pState_CellIDArr       [LeftIndex], pState_CellIDArr       [RightIndex]);
            Swap(pState_CellPosArr      [LeftIndex], pState_CellPosArr      [RightIndex]);
            Swap(pState_CellIndexPtrArr [LeftIndex], pState_CellIndexPtrArr [RightIndex]);
            Swap(pState_CellFreeArr     [LeftIndex], pState_CellFreeArr     [RightIndex]);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, LeftIndex,  LeftIndex);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, RightIndex, RightIndex);
        }
    }
}

//=============================================================================
// 4.2 - Step Kernel 2: Shared Memory + Two Kernels
//=============================================================================

// [Kernel Summary]
// - input iterator dimension ?                ... 2-d
// - use threadIdx/blockIdx instead of loops ? ... yes

template<uint32 JOBS_PER_BLOCK_EDGE, bool SwapEven>
__global__ void KNNSearchKernel_Step_2_Col_Impl(
    /*INOUT*/ uint32 *pState_CellIDArr,       // internal state
    /*INOUT*/ float2 *pState_CellPosArr,      // internal state
    /*INOUT*/ uint64 *pState_CellIndexPtrArr, // internal state
    /*INOUT*/ uint32 *pState_CellFreeArr,     // internal state
              uint32  State_ColCount,         // internal state
              uint32  State_RowCount          // internal state
    )
{
    // NOTE: each array has own memory
    __shared__ uint32 SharedCellIDMtx       [JOBS_PER_BLOCK_EDGE][JOBS_PER_BLOCK_EDGE];
    __shared__ float2 SharedCellPosMtx      [JOBS_PER_BLOCK_EDGE][JOBS_PER_BLOCK_EDGE];
    __shared__ uint64 SharedCellIndexPtrMtx [JOBS_PER_BLOCK_EDGE][JOBS_PER_BLOCK_EDGE];
    __shared__ uint32 SharedCellFreeMtx     [JOBS_PER_BLOCK_EDGE][JOBS_PER_BLOCK_EDGE];
    uint32 ColIndex     = OFFSET2D(threadIdx.x, blockIdx.x, blockDim.x)+(SwapEven?0:1);
    uint32 RowIndex     = OFFSET2D(threadIdx.y, blockIdx.y, blockDim.y);
    uint32 MemCellIndex = OFFSET2D(ColIndex, RowIndex, State_ColCount);
    bool IsValidRowIndex = RowIndex<State_RowCount;
    bool IsInsideMatrix  = (ColIndex<State_ColCount && IsValidRowIndex);
    bool IsNeedSwap      = false;
    if(IsInsideMatrix) // bounds test
    {
        SharedCellIDMtx       [threadIdx.x][threadIdx.y] = pState_CellIDArr       [MemCellIndex];
        SharedCellPosMtx      [threadIdx.x][threadIdx.y] = pState_CellPosArr      [MemCellIndex];
        SharedCellIndexPtrMtx [threadIdx.x][threadIdx.y] = pState_CellIndexPtrArr [MemCellIndex];
        SharedCellFreeMtx     [threadIdx.x][threadIdx.y] = pState_CellFreeArr     [MemCellIndex];
    }
    __syncthreads();
    if(
        IsValidRowIndex           && // bounds test
        threadIdx.x%2 == 0        && // sort even columns
        ColIndex<State_ColCount-1 && // ignore final column
        SharedCellPosMtx[threadIdx.x+0][threadIdx.y].x>SharedCellPosMtx[threadIdx.x+1][threadIdx.y].x // if need sort
        )
    {
        Swap(SharedCellIDMtx       [threadIdx.x+0][threadIdx.y], SharedCellIDMtx       [threadIdx.x+1][threadIdx.y]);
        Swap(SharedCellPosMtx      [threadIdx.x+0][threadIdx.y], SharedCellPosMtx      [threadIdx.x+1][threadIdx.y]);
        Swap(SharedCellIndexPtrMtx [threadIdx.x+0][threadIdx.y], SharedCellIndexPtrMtx [threadIdx.x+1][threadIdx.y]);
        Swap(SharedCellFreeMtx     [threadIdx.x+0][threadIdx.y], SharedCellFreeMtx     [threadIdx.x+1][threadIdx.y]);
        IsNeedSwap = true;
    }
    __syncthreads();
    if(IsInsideMatrix) // bounds test
    {
        pState_CellIDArr       [MemCellIndex] = SharedCellIDMtx       [threadIdx.x][threadIdx.y];
        pState_CellPosArr      [MemCellIndex] = SharedCellPosMtx      [threadIdx.x][threadIdx.y];
        pState_CellIndexPtrArr [MemCellIndex] = SharedCellIndexPtrMtx [threadIdx.x][threadIdx.y];
        pState_CellFreeArr     [MemCellIndex] = SharedCellFreeMtx     [threadIdx.x][threadIdx.y];
        if(IsNeedSwap)
        {
            uint32 LeftIndex  = MemCellIndex; //OFFSET2D(ColIndex+0, RowIndex, State_ColCount);
            uint32 RightIndex = LeftIndex+1;  //OFFSET2D(ColIndex+1, RowIndex, State_ColCount);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, LeftIndex,  LeftIndex);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, RightIndex, RightIndex);
        }
    }
}
template<uint32 JOBS_PER_BLOCK_EDGE, bool SwapEven>
__global__ void KNNSearchKernel_Step_2_Row_Impl(
    /*INOUT*/ uint32 *pState_CellIDArr,       // internal state
    /*INOUT*/ float2 *pState_CellPosArr,      // internal state
    /*INOUT*/ uint64 *pState_CellIndexPtrArr, // internal state
    /*INOUT*/ uint32 *pState_CellFreeArr,     // internal state
              uint32  State_ColCount,         // internal state
              uint32  State_RowCount          // internal state
    )
{
    // NOTE: each array has own memory
    __shared__ uint32 SharedCellIDMtx       [JOBS_PER_BLOCK_EDGE][JOBS_PER_BLOCK_EDGE];
    __shared__ float2 SharedCellPosMtx      [JOBS_PER_BLOCK_EDGE][JOBS_PER_BLOCK_EDGE];
    __shared__ uint64 SharedCellIndexPtrMtx [JOBS_PER_BLOCK_EDGE][JOBS_PER_BLOCK_EDGE];
    __shared__ uint32 SharedCellFreeMtx     [JOBS_PER_BLOCK_EDGE][JOBS_PER_BLOCK_EDGE];
    uint32 ColIndex     = OFFSET2D(threadIdx.x, blockIdx.x, blockDim.x);
    uint32 RowIndex     = OFFSET2D(threadIdx.y, blockIdx.y, blockDim.y)+(SwapEven?0:1);
    uint32 MemCellIndex = OFFSET2D(ColIndex, RowIndex, State_ColCount);
    bool IsValidColIndex = ColIndex<State_ColCount;
    bool IsInsideMatrix  = (IsValidColIndex && RowIndex<State_RowCount);
    bool IsNeedSwap      = false;
    if(IsInsideMatrix) // bounds test
    {
        SharedCellIDMtx       [threadIdx.x][threadIdx.y] = pState_CellIDArr       [MemCellIndex];
        SharedCellPosMtx      [threadIdx.x][threadIdx.y] = pState_CellPosArr      [MemCellIndex];
        SharedCellIndexPtrMtx [threadIdx.x][threadIdx.y] = pState_CellIndexPtrArr [MemCellIndex];
        SharedCellFreeMtx     [threadIdx.x][threadIdx.y] = pState_CellFreeArr     [MemCellIndex];
    }
    __syncthreads();
    if(
        IsValidColIndex           && // bounds test
        threadIdx.y%2 == 0        && // sort even rows
        RowIndex<State_RowCount-1 && // ignore final row
        SharedCellPosMtx[threadIdx.x][threadIdx.y+0].y>SharedCellPosMtx[threadIdx.x][threadIdx.y+1].y // if need sort
        )
    {
        Swap(SharedCellIDMtx       [threadIdx.x][threadIdx.y+0], SharedCellIDMtx       [threadIdx.x][threadIdx.y+1]);
        Swap(SharedCellPosMtx      [threadIdx.x][threadIdx.y+0], SharedCellPosMtx      [threadIdx.x][threadIdx.y+1]);
        Swap(SharedCellIndexPtrMtx [threadIdx.x][threadIdx.y+0], SharedCellIndexPtrMtx [threadIdx.x][threadIdx.y+1]);
        Swap(SharedCellFreeMtx     [threadIdx.x][threadIdx.y+0], SharedCellFreeMtx     [threadIdx.x][threadIdx.y+1]);
        IsNeedSwap = true;
    }
    __syncthreads();
    if(IsInsideMatrix) // bounds test
    {
        pState_CellIDArr       [MemCellIndex] = SharedCellIDMtx       [threadIdx.x][threadIdx.y];
        pState_CellPosArr      [MemCellIndex] = SharedCellPosMtx      [threadIdx.x][threadIdx.y];
        pState_CellIndexPtrArr [MemCellIndex] = SharedCellIndexPtrMtx [threadIdx.x][threadIdx.y];
        pState_CellFreeArr     [MemCellIndex] = SharedCellFreeMtx     [threadIdx.x][threadIdx.y];
        if(IsNeedSwap)
        {
            uint32 LeftIndex  = MemCellIndex;             //OFFSET2D(ColIndex, RowIndex+0, State_ColCount);
            uint32 RightIndex = LeftIndex+State_ColCount; //OFFSET2D(ColIndex, RowIndex+1, State_ColCount);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, LeftIndex,  LeftIndex);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, RightIndex, RightIndex);
        }
    }
}

//=============================================================================
// 4.3 - Step Kernel 3: Dynamic Shared Memory + Two Kernels
//=============================================================================

// [Kernel Summary]
// - input iterator dimension ?                ... 2-d
// - use threadIdx/blockIdx instead of loops ? ... yes

template<uint32 JOBS_PER_BLOCK_EDGE, bool SwapEven>
__global__ void KNNSearchKernel_Step_3_Col_Impl(
    /*INOUT*/ uint32 *pState_CellIDArr,       // internal state
    /*INOUT*/ float2 *pState_CellPosArr,      // internal state
    /*INOUT*/ uint64 *pState_CellIndexPtrArr, // internal state
    /*INOUT*/ uint32 *pState_CellFreeArr,     // internal state
              uint32  State_ColCount,         // internal state
              uint32  State_RowCount          // internal state
    )
{
    extern __shared__ char pArray[];
    // NOTE: same memory shared among arrays -- similar to c++ union
    uint32 *pSharedCellIDArr       = reinterpret_cast<uint32*>(&pArray[0]);
    float2 *pSharedCellPosArr      = reinterpret_cast<float2*>(&pArray[0]);
    uint64 *pSharedCellIndexPtrArr = reinterpret_cast<uint64*>(&pArray[0]);
    uint32 *pSharedCellFreeArr     = reinterpret_cast<uint32*>(&pArray[0]);
    uint32 ColIndex       = OFFSET2D(threadIdx.x, blockIdx.x, blockDim.x)+(SwapEven?0:1);
    uint32 RowIndex       = OFFSET2D(threadIdx.y, blockIdx.y, blockDim.y);
    uint32 MemCellIndex   = OFFSET2D(ColIndex, RowIndex, State_ColCount);
    uint32 CacheCellIndex = OFFSET2D(threadIdx.x, threadIdx.y, JOBS_PER_BLOCK_EDGE);
    uint32 LeftIndex  = 0;
    uint32 RightIndex = 0;
    bool IsValidRowIndex = (RowIndex<State_RowCount);
    bool IsInsideMatrix  = false;
    bool IsNeedSwap      = false;
    //============================================================================= // CellPos (NOTE: must go first)
    if(ColIndex<State_ColCount && IsValidRowIndex) // bounds test
    {
        pSharedCellPosArr[CacheCellIndex] = pState_CellPosArr[MemCellIndex];
        IsInsideMatrix = true; // store this decision
    }
    __syncthreads();
    if(
        IsValidRowIndex    &&     // bounds test
        threadIdx.x%2 == 0 &&     // sort even columns
        ColIndex<State_ColCount-1 // ignore final column
        )
    {
        LeftIndex  = CacheCellIndex; //OFFSET2D(threadIdx.x+0, threadIdx.y, JOBS_PER_BLOCK_EDGE);
        RightIndex = LeftIndex+1;    //OFFSET2D(threadIdx.x+1, threadIdx.y, JOBS_PER_BLOCK_EDGE);
        if(pSharedCellPosArr[LeftIndex].x>pSharedCellPosArr[RightIndex].x) // if need sort
        {
            Swap(pSharedCellPosArr[LeftIndex], pSharedCellPosArr[RightIndex]);
            IsNeedSwap = true; // store this decision
        }
    }
    __syncthreads();
    if(IsInsideMatrix) // bounds test
        pState_CellPosArr[MemCellIndex] = pSharedCellPosArr[CacheCellIndex];
    //============================================================================= // CellID
    __syncthreads();
    if(IsInsideMatrix)
        pSharedCellIDArr[CacheCellIndex] = pState_CellIDArr[MemCellIndex];
    __syncthreads();
    if(IsNeedSwap)
        Swap(pSharedCellIDArr[LeftIndex], pSharedCellIDArr[RightIndex]);
    __syncthreads();
    if(IsInsideMatrix)
        pState_CellIDArr[MemCellIndex] = pSharedCellIDArr[CacheCellIndex];
    //============================================================================= // CellIndexPtr
    __syncthreads();
    if(IsInsideMatrix)
        pSharedCellIndexPtrArr[CacheCellIndex] = pState_CellIndexPtrArr[MemCellIndex];
    __syncthreads();
    if(IsNeedSwap)
        Swap(pSharedCellIndexPtrArr[LeftIndex], pSharedCellIndexPtrArr[RightIndex]);
    __syncthreads();
    if(IsInsideMatrix)
        pState_CellIndexPtrArr[MemCellIndex] = pSharedCellIndexPtrArr[CacheCellIndex];
    //============================================================================= // CellFree
    __syncthreads();
    if(IsInsideMatrix)
        pSharedCellFreeArr[CacheCellIndex] = pState_CellFreeArr[MemCellIndex];
    __syncthreads();
    if(IsNeedSwap)
        Swap(pSharedCellFreeArr[LeftIndex], pSharedCellFreeArr[RightIndex]);
    __syncthreads();
    if(IsInsideMatrix)
    {
        pState_CellFreeArr[MemCellIndex] = pSharedCellFreeArr[CacheCellIndex];
        if(IsNeedSwap)
        {
            LeftIndex  = MemCellIndex; //OFFSET2D(ColIndex+0, RowIndex, State_ColCount);
            RightIndex = LeftIndex+1;  //OFFSET2D(ColIndex+1, RowIndex, State_ColCount);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, LeftIndex,  LeftIndex);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, RightIndex, RightIndex);
        }
    }
    //=============================================================================
}
template<uint32 JOBS_PER_BLOCK_EDGE, bool SwapEven>
__global__ void KNNSearchKernel_Step_3_Row_Impl(
    /*INOUT*/ uint32 *pState_CellIDArr,       // internal state
    /*INOUT*/ float2 *pState_CellPosArr,      // internal state
    /*INOUT*/ uint64 *pState_CellIndexPtrArr, // internal state
    /*INOUT*/ uint32 *pState_CellFreeArr,     // internal state
              uint32  State_ColCount,         // internal state
              uint32  State_RowCount          // internal state
    )
{
    extern __shared__ char pArray[];
    // NOTE: same memory shared among arrays -- similar to c++ union
    uint32 *pSharedCellIDArr       = reinterpret_cast<uint32*>(&pArray[0]);
    float2 *pSharedCellPosArr      = reinterpret_cast<float2*>(&pArray[0]);
    uint64 *pSharedCellIndexPtrArr = reinterpret_cast<uint64*>(&pArray[0]);
    uint32 *pSharedCellFreeArr     = reinterpret_cast<uint32*>(&pArray[0]);
    uint32 ColIndex       = OFFSET2D(threadIdx.x, blockIdx.x, blockDim.x);
    uint32 RowIndex       = OFFSET2D(threadIdx.y, blockIdx.y, blockDim.y)+(SwapEven?0:1);
    uint32 MemCellIndex   = OFFSET2D(ColIndex, RowIndex, State_ColCount);
    uint32 CacheCellIndex = OFFSET2D(threadIdx.x, threadIdx.y, JOBS_PER_BLOCK_EDGE);
    uint32 LeftIndex  = 0;
    uint32 RightIndex = 0;
    bool IsValidColIndex = ColIndex<State_ColCount;
    bool IsInsideMatrix  = false;
    bool IsNeedSwap      = false;
    //============================================================================= // CellPos (NOTE: must go first)
    if(IsValidColIndex && RowIndex<State_RowCount) // bounds test
    {
        pSharedCellPosArr[CacheCellIndex] = pState_CellPosArr[MemCellIndex];
        IsInsideMatrix = true; // store this decision
    }
    __syncthreads();
    if(
        IsValidColIndex    &&     // bounds test
        threadIdx.y%2 == 0 &&     // sort even rows
        RowIndex<State_RowCount-1 // ignore final row
        )
    {
        LeftIndex  = CacheCellIndex;                //OFFSET2D(threadIdx.x, threadIdx.y+0, JOBS_PER_BLOCK_EDGE);
        RightIndex = LeftIndex+JOBS_PER_BLOCK_EDGE; //OFFSET2D(threadIdx.x, threadIdx.y+1, JOBS_PER_BLOCK_EDGE);
        if(pSharedCellPosArr[LeftIndex].y>pSharedCellPosArr[RightIndex].y) // if need sort
        {
            Swap(pSharedCellPosArr[LeftIndex], pSharedCellPosArr[RightIndex]);
            IsNeedSwap = true; // store this decision
        }
    }
    __syncthreads();
    if(IsInsideMatrix) // bounds test
        pState_CellPosArr[MemCellIndex] = pSharedCellPosArr[CacheCellIndex];
    //============================================================================= // CellID
    __syncthreads();
    if(IsInsideMatrix)
        pSharedCellIDArr[CacheCellIndex] = pState_CellIDArr[MemCellIndex];
    __syncthreads();
    if(IsNeedSwap)
        Swap(pSharedCellIDArr[LeftIndex], pSharedCellIDArr[RightIndex]);
    __syncthreads();
    if(IsInsideMatrix)
        pState_CellIDArr[MemCellIndex] = pSharedCellIDArr[CacheCellIndex];
    //============================================================================= // CellIndexPtr
    __syncthreads();
    if(IsInsideMatrix)
        pSharedCellIndexPtrArr[CacheCellIndex] = pState_CellIndexPtrArr[MemCellIndex];
    __syncthreads();
    if(IsNeedSwap)
        Swap(pSharedCellIndexPtrArr[LeftIndex], pSharedCellIndexPtrArr[RightIndex]);
    __syncthreads();
    if(IsInsideMatrix)
        pState_CellIndexPtrArr[MemCellIndex] = pSharedCellIndexPtrArr[CacheCellIndex];
    //============================================================================= // CellFree
    __syncthreads();
    if(IsInsideMatrix)
        pSharedCellFreeArr[CacheCellIndex] = pState_CellFreeArr[MemCellIndex];
    __syncthreads();
    if(IsNeedSwap)
        Swap(pSharedCellFreeArr[LeftIndex], pSharedCellFreeArr[RightIndex]);
    __syncthreads();
    if(IsInsideMatrix)
    {
        pState_CellFreeArr[MemCellIndex] = pSharedCellFreeArr[CacheCellIndex];
        if(IsNeedSwap)
        {
            LeftIndex  = MemCellIndex;             //OFFSET2D(ColIndex, RowIndex+0, State_ColCount);
            RightIndex = LeftIndex+State_ColCount; //OFFSET2D(ColIndex, RowIndex+1, State_ColCount);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, LeftIndex,  LeftIndex);
            UpdateIndexPtrArr(pState_CellIndexPtrArr, RightIndex, RightIndex);
        }
    }
    //=============================================================================
}

//=====================================
// 5. Search Kernel
//=====================================

//=============================================================================
// 5.0 - Search Kernel 0: Naive Implementation
//=============================================================================

// [Kernel Summary]
// - input iterator dimension ?                ... 1-d
// - use threadIdx/blockIdx instead of loops ? ... no

__global__ void KNNSearchKernel_Search_Naive_Impl(
    /*IN*/  uint32 *pQueryCellIndexArr,      // array of indices corresponding to query requests
    /*IN*/  float  *pQueryRadiusArr,         // array of radii corresponding to query requests
            uint32  QueryCount,              // # of query requests
            uint32  Query_k,                 // # of neighbors to return
            uint32  QueryPadded_k,           // padded "Query_k"
            uint32  QueryCellRadius,         // cell radius of search
    /*OUT*/ uint32 *pNeighborCellIDArr,      // array of IDs containing search results
    /*OUT*/ uint32 *pNeighborCellIDCountArr, // array of search result counts per query
    /*IN*/  uint32 *pState_CellIDArr,        // internal state
    /*IN*/  float2 *pState_CellPosArr,       // internal state
    /*IN*/  uint32 *pState_CellFreeArr,      // internal state
            uint32  State_ColCount,          // internal state
            uint32  State_RowCount           // internal state
    )
{
    // [Read Order]
    //
    // pState_CellPosArr:
    // pState_CellIDArr:
    // pState_CellFreeArr:
    // +---+---+---+---+---+---+---+---+---+
    // |   |   |   |   |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 2 | 3 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 4 |[5]| 6 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 7 | 8 | 9 |   | 1 | 2 | 3 |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   |   |   |   |   | 4 |[5]| 6 |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 2 | 3 |   | 7 | 8 | 9 |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 4 |[5]| 6 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 7 | 8 | 9 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   |   |   |   |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+

    // [Write Order]
    //
    // pNeighborCellIDArr:                pNeighborCellIDCountArr:
    // +---+---+---+---+---+---+---+---+  +---+
    // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |  | 1 |  <-- QueryIndex == 0
    // +---+---+---+---+---+---+---+---+  +---+
    // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |  | 1 |  <-- QueryIndex == 1
    // +---+---+---+---+---+---+---+---+  +---+
    // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |  | 1 |  <-- QueryIndex == 2
    // +---+---+---+---+---+---+---+---+  +---+

    uint32 QueryIndex = OFFSET2D(threadIdx.x, blockIdx.x, blockDim.x);
    if(QueryIndex >= QueryCount) // bounds test
        return;
    uint32 QueryMemCellIndex = pQueryCellIndexArr[QueryIndex]; // self
    float  QueryRadiusSqr    = pQueryRadiusArr[QueryIndex]*pQueryRadiusArr[QueryIndex];
    //=============================================================================
    // calc sampling area
    uint32 QueryMemColIndex = QueryMemCellIndex%State_ColCount;
    uint32 QueryMemRowIndex = QueryMemCellIndex/State_ColCount;
    int X1 = static_cast<int>(QueryMemColIndex)-static_cast<int>(QueryCellRadius);
    int Y1 = static_cast<int>(QueryMemRowIndex)-static_cast<int>(QueryCellRadius);
    int X2 = static_cast<int>(QueryMemColIndex)+static_cast<int>(QueryCellRadius);
    int Y2 = static_cast<int>(QueryMemRowIndex)+static_cast<int>(QueryCellRadius);
    //=============================================================================
    // limit sampling area to legal extent
    if(X1<0)                X1 = 0;
    if(Y1<0)                Y1 = 0;
    if(X2>State_ColCount-1) X2 = State_ColCount-1;
    if(Y2>State_RowCount-1) Y2 = State_RowCount-1;
    //=============================================================================
    // sample cells within radius within sampling area
    uint32 BaseWriteIndex = QueryIndex*QueryPadded_k;
    pNeighborCellIDCountArr[QueryIndex] = 0;
    for(uint32 y = Y1; y <= Y2; y++)
        for(uint32 x = X1; x <= X2; x++)
        {
            uint32 MemCellIndex = OFFSET2D(x, y, State_ColCount);
            if(
                pState_CellFreeArr[MemCellIndex] == 1 || // make sure cell is occupied
                MemCellIndex == QueryMemCellIndex        // make sure not comparing with self
                ) continue; // non-candidate cell
            float DeltaX = pState_CellPosArr[MemCellIndex].x-pState_CellPosArr[QueryMemCellIndex].x;
            float DeltaY = pState_CellPosArr[MemCellIndex].y-pState_CellPosArr[QueryMemCellIndex].y;
            if(DeltaX*DeltaX+DeltaY*DeltaY<QueryRadiusSqr) // distance test
            {
                pNeighborCellIDArr[
                    BaseWriteIndex+pNeighborCellIDCountArr[QueryIndex]
                    ] = pState_CellIDArr[MemCellIndex];
                pNeighborCellIDCountArr[QueryIndex]++;
                if(pNeighborCellIDCountArr[QueryIndex] >= Query_k) // stop condition
                {
                    x = X2;
                    y = Y2;
                }
            }
        }
    //=============================================================================
}

//=============================================================================
// 5.1 - Search Kernel 1: Dynamic Shared Memory
//=============================================================================

// NOTE: fails for large QueryCount (max: 32)

// [Kernel Summary]
// - input iterator dimension ?                ... 1-d
// - use threadIdx/blockIdx instead of loops ? ... yes

__global__ void KNNSearchKernel_Search_1_Impl(
    /*IN*/  uint32 *pQueryCellIndexArr,      // array of indices corresponding to query requests
    /*IN*/  float  *pQueryRadiusArr,         // array of radii corresponding to query requests
            uint32  QueryCount,              // # of query requests
            uint32  Query_k,                 // # of neighbors to return
            uint32  QueryPadded_k,           // padded "Query_k"
            uint32  QueryCellRadius,         // cell radius of search
            uint32  JobsPerQueryEdge,        // sampling edge size
            uint32  JobsPerQuery,            // sampling area
    /*OUT*/ uint32 *pNeighborCellIDArr,      // array of IDs containing search results
    /*OUT*/ uint32 *pNeighborCellIDCountArr, // array of search result counts per query
    /*IN*/  uint32 *pState_CellIDArr,        // internal state
    /*IN*/  float2 *pState_CellPosArr,       // internal state
    /*IN*/  uint32 *pState_CellFreeArr,      // internal state
            uint32  State_ColCount,          // internal state
            uint32  State_RowCount           // internal state
    )
{
    // [Read Order]
    //
    // pState_CellPosArr:
    // pState_CellIDArr:
    // pState_CellFreeArr:
    // +---+---+---+---+---+---+---+---+---+
    // |   |   |   |   |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 |[ ]| 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   | 1 | 1 | 1 |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   |   |   |   |   | 1 |[ ]| 1 |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   | 1 | 1 | 1 |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 |[ ]| 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   |   |   |   |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+

    // [Write Order]
    //
    // pNeighborCellIDArr:                pNeighborCellIDCountArr:
    // +---+---+---+---+---+---+---+---+  +---+
    // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |  | 1 |  <-- QueryIndex == 0
    // +---+---+---+---+---+---+---+---+  +---+
    // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |  | 1 |  <-- QueryIndex == 1
    // +---+---+---+---+---+---+---+---+  +---+
    // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |  | 1 |  <-- QueryIndex == 2
    // +---+---+---+---+---+---+---+---+  +---+

    uint32 SampleCellCount = QueryCount*Query_k;

    extern __shared__ char pArray[];
    uint32 *pSharedCellIDArr;
    uint32 *pSharedPrefixSumArr;
    uint32  Offset = 0;

    ALIGN_UP(Offset, __alignof(pSharedCellIDArr));
    pSharedCellIDArr = reinterpret_cast<uint32*>(&pArray[Offset]);
    Offset += SampleCellCount*sizeof(*pSharedCellIDArr);

    ALIGN_UP(Offset, __alignof(pSharedPrefixSumArr));
    pSharedPrefixSumArr = reinterpret_cast<uint32*>(&pArray[Offset]);

    uint32 SampleCellIndex = OFFSET2D(threadIdx.x, blockIdx.x, blockDim.x);
    bool IsValidSampleCellIndex = (SampleCellIndex<SampleCellCount);
    if(IsValidSampleCellIndex) // bounds test
    {
        uint32 QueryIndex     = SampleCellIndex/Query_k;
        uint32 CacheCellIndex = SampleCellIndex%Query_k;
        #ifdef ENABLE_BLOCK_PADDING

            // 4x4:
            // +---+---+---+---+
            // | 0 | 1 | 2 | 3 |
            // +---+---+---+---+
            // | 4 |[5]|   |   |
            // +---+---+---+---+
            // |   |   |   |   |
            // +---+---+---+---+
            // |   |   |   |   |
            // +---+---+---+---+

            if(CacheCellIndex >= 5)
                CacheCellIndex++; // skip self
        #else

            // 3x3:           5x5:
            // +---+---+---+  +---+---+---+---+---+
            // | 0 | 1 | 2 |  | 0 | 1 | 2 | 3 | 4 |
            // +---+---+---+  +---+---+---+---+---+
            // | 3 |[4]|   |  | 5 | 6 | 7 | 8 | 9 |
            // +---+---+---+  +---+---+---+---+---+
            // |   |   |   |  | A | B |[C]|   |   |
            // +---+---+---+  +---+---+---+---+---+
            //                |   |   |   |   |   |
            //                +---+---+---+---+---+
            //                |   |   |   |   |   |
            //                +---+---+---+---+---+

            if(CacheCellIndex >= JobsPerQuery/2)
                CacheCellIndex++; // skip self
        #endif
        uint32 QueryMemCellIndex = pQueryCellIndexArr[QueryIndex]; // self
        float  QueryRadiusSqr    = pQueryRadiusArr[QueryIndex]*pQueryRadiusArr[QueryIndex];
        //=============================================================================
        // calc sampling coord
        uint32 QueryMemColIndex = QueryMemCellIndex%State_ColCount;
        uint32 QueryMemRowIndex = QueryMemCellIndex/State_ColCount;
        uint32 CacheColIndex    = CacheCellIndex%JobsPerQueryEdge;
        uint32 CacheRowIndex    = CacheCellIndex/JobsPerQueryEdge;
        int X = QueryMemColIndex+(
            static_cast<int>(CacheColIndex)-static_cast<int>(QueryCellRadius)
            );
        int Y = QueryMemRowIndex+(
            static_cast<int>(CacheRowIndex)-static_cast<int>(QueryCellRadius)
            );
        //=============================================================================
        // sample "cell" within radius at sampling coord (from global memory)
        if(
            X >= 0 && X<State_ColCount &&
            Y >= 0 && Y<State_RowCount
            ) // only consider sampling coord with legal extent
        {
            uint32 MemCellIndex = OFFSET2D(X, Y, State_ColCount);
            if(
                pState_CellFreeArr[MemCellIndex] == 0 && // make sure cell is occupied
                MemCellIndex != QueryMemCellIndex        // make sure not comparing with self
                )
            {
                float DeltaX = pState_CellPosArr[MemCellIndex].x-pState_CellPosArr[QueryMemCellIndex].x;
                float DeltaY = pState_CellPosArr[MemCellIndex].y-pState_CellPosArr[QueryMemCellIndex].y;
                if(DeltaX*DeltaX+DeltaY*DeltaY<QueryRadiusSqr) // distance test
                {
                    pSharedCellIDArr    [SampleCellIndex] = pState_CellIDArr[MemCellIndex];
                    pSharedPrefixSumArr [SampleCellIndex] = 1;
                }
                else // outside search radius
                {
                    pSharedCellIDArr    [SampleCellIndex] = 0;
                    pSharedPrefixSumArr [SampleCellIndex] = 0;
                }
            }
            else // non-candidate cell
            {
                pSharedCellIDArr    [SampleCellIndex] = 0;
                pSharedPrefixSumArr [SampleCellIndex] = 0;
            }
        }
        else // illegal coord
        {
            pSharedCellIDArr    [SampleCellIndex] = 0;
            pSharedPrefixSumArr [SampleCellIndex] = 0;
        }
        //=============================================================================
    }
    #if 0
        __syncthreads(); // do we need this ?
    #endif
    if(IsValidSampleCellIndex) // bounds test
    {
        uint32 QueryIndex     = SampleCellIndex/Query_k;
        uint32 CacheCellIndex = SampleCellIndex%Query_k;
        if(CacheCellIndex == 0)
        {
            //=============================================================================
            // calc prefix sum in serial and write result (to global memory)
            uint32 BaseWriteIndex = QueryIndex*QueryPadded_k;
            uint32 Sum = 0;
            for(uint32 i = 0; i<Query_k; i++)
            {
                uint32 SampleCellIndex = BaseWriteIndex+i;
                //=========================================================
                // calc prefix sum
                uint32 Temp = pSharedPrefixSumArr[SampleCellIndex];
                pSharedPrefixSumArr[SampleCellIndex] = Sum;
                Sum += Temp;
                //=========================================================
                if(Temp == 1)
                    pNeighborCellIDArr[
                        BaseWriteIndex+pSharedPrefixSumArr[SampleCellIndex]
                        ] = pSharedCellIDArr[SampleCellIndex]; // compact write index
                if(Sum >= Query_k) // stop condition
                    break;
            }
            pNeighborCellIDCountArr[QueryIndex] = Sum; // neighbor count is final prefix sum
            //=============================================================================
        }
    }
}

//=============================================================================
// 5.2 - Search Kernel 2: Dynamic Shared Memory + 3D Input Iterator
//=============================================================================

// [Kernel Summary]
// - input iterator dimension ?                ... 3-d
// - use threadIdx/blockIdx instead of loops ? ... yes

__global__ void KNNSearchKernel_Search_2_Impl(
    /*IN*/  uint32 *pQueryCellIndexArr,      // array of indices corresponding to query requests
    /*IN*/  float  *pQueryRadiusArr,         // array of radii corresponding to query requests
            uint32  QueryCount,              // # of query requests
            uint32  Query_k,                 // # of neighbors to return
            uint32  QueryPadded_k,           // padded "Query_k"
            uint32  QueryCellRadius,         // cell radius of search
            uint32  JobsPerQueryEdge,        // sampling edge size
            uint32  JobsPerQuery,            // sampling area
    /*OUT*/ uint32 *pNeighborCellIDArr,      // array of IDs containing search results
    /*OUT*/ uint32 *pNeighborCellIDCountArr, // array of search result counts per query
    /*IN*/  uint32 *pState_CellIDArr,        // internal state
    /*IN*/  float2 *pState_CellPosArr,       // internal state
    /*IN*/  uint32 *pState_CellFreeArr,      // internal state
            uint32  State_ColCount,          // internal state
            uint32  State_RowCount           // internal state
    )
{
    // [Read Order]
    //
    // pState_CellPosArr:
    // pState_CellIDArr:
    // pState_CellFreeArr:
    // +---+---+---+---+---+---+---+---+---+
    // |   |   |   |   |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 |[ ]| 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   | 1 | 1 | 1 |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   |   |   |   |   | 1 |[ ]| 1 |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   | 1 | 1 | 1 |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 |[ ]| 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   |   |   |   |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+

    // [Write Order]
    //
    // pNeighborCellIDArr:                pNeighborCellIDCountArr:
    // +---+---+---+---+---+---+---+---+  +---+
    // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |  | 1 |  <-- QueryIndex == 0
    // +---+---+---+---+---+---+---+---+  +---+
    // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |  | 1 |  <-- QueryIndex == 1
    // +---+---+---+---+---+---+---+---+  +---+
    // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |  | 1 |  <-- QueryIndex == 2
    // +---+---+---+---+---+---+---+---+  +---+

    extern __shared__ char pArray[];
    uint32 *pSharedCellIDArr;
    uint32 *pSharedPrefixSumArr;
    uint32  Offset = 0;

    ALIGN_UP(Offset, __alignof(pSharedCellIDArr));
    pSharedCellIDArr = reinterpret_cast<uint32*>(&pArray[Offset]);
    Offset += blockDim.z*JobsPerQuery*sizeof(*pSharedCellIDArr);

    ALIGN_UP(Offset, __alignof(pSharedPrefixSumArr));
    pSharedPrefixSumArr = reinterpret_cast<uint32*>(&pArray[Offset]);

    uint32 QueryIndex = OFFSET2D(threadIdx.z, blockIdx.x, blockDim.z);
    bool IsValidQueryIndex = (QueryIndex<QueryCount);
    if(
        IsValidQueryIndex &&                                         // bounds test
        threadIdx.x<JobsPerQueryEdge && threadIdx.y<JobsPerQueryEdge // bounds test
        )
    {
        uint32 QueryMemCellIndex = pQueryCellIndexArr[QueryIndex]; // self
        float  QueryRadiusSqr    = pQueryRadiusArr[QueryIndex]*pQueryRadiusArr[QueryIndex];
        //=============================================================================
        // calc sampling coord
        uint32 QueryMemColIndex = QueryMemCellIndex%State_ColCount;
        uint32 QueryMemRowIndex = QueryMemCellIndex/State_ColCount;
        int X = QueryMemColIndex+(
            static_cast<int>(threadIdx.x)-static_cast<int>(QueryCellRadius)
            );
        int Y = QueryMemRowIndex+(
            static_cast<int>(threadIdx.y)-static_cast<int>(QueryCellRadius)
            );
        //=============================================================================
        // sample "cell" within radius at sampling coord (from global memory)
        uint32 SampleCellIndex =
            OFFSET3D(threadIdx.x, threadIdx.y, threadIdx.z, JobsPerQueryEdge, JobsPerQuery);
        if(
            X >= 0 && X<State_ColCount &&
            Y >= 0 && Y<State_RowCount
            ) // only consider sampling coord with legal extent
        {
            uint32 MemCellIndex = OFFSET2D(X, Y, State_ColCount); // current cell
            if(
                pState_CellFreeArr[MemCellIndex] == 0 && // make sure cell is occupied
                MemCellIndex != QueryMemCellIndex        // make sure not comparing with self
                )
            {
                float DeltaX = pState_CellPosArr[MemCellIndex].x-pState_CellPosArr[QueryMemCellIndex].x;
                float DeltaY = pState_CellPosArr[MemCellIndex].y-pState_CellPosArr[QueryMemCellIndex].y;
                if(DeltaX*DeltaX+DeltaY*DeltaY<QueryRadiusSqr) // distance test
                {
                    pSharedCellIDArr    [SampleCellIndex] = pState_CellIDArr[MemCellIndex];
                    pSharedPrefixSumArr [SampleCellIndex] = 1;
                }
                else // outside search radius
                {
                    pSharedCellIDArr    [SampleCellIndex] = 0;
                    pSharedPrefixSumArr [SampleCellIndex] = 0;
                }
            }
            else // non-candidate cell
            {
                pSharedCellIDArr    [SampleCellIndex] = 0;
                pSharedPrefixSumArr [SampleCellIndex] = 0;
            }
        }
        else // illegal coord
        {
            pSharedCellIDArr    [SampleCellIndex] = 0;
            pSharedPrefixSumArr [SampleCellIndex] = 0;
        }
        //=============================================================================
    }
    #if 0
        __syncthreads(); // do we need this ?
    #endif
    if(
        IsValidQueryIndex &&                 // bounds test
        threadIdx.x == 0 && threadIdx.y == 0 // bounds test
        )
    {
        //=============================================================================
        // calc prefix sum in serial and write result (to global memory)
        uint32 BaseWriteIndex = QueryIndex*QueryPadded_k;
        uint32 Sum = 0;
        for(uint32 y = 0; y<JobsPerQueryEdge; y++)
            for(uint32 x = 0; x<JobsPerQueryEdge; x++)
            {
                uint32 SampleCellIndex =
                    OFFSET3D(x, y, threadIdx.z, JobsPerQueryEdge, JobsPerQuery);
                //=========================================================
                // calc prefix sum
                uint32 Temp = pSharedPrefixSumArr[SampleCellIndex];
                pSharedPrefixSumArr[SampleCellIndex] = Sum;
                Sum += Temp;
                //=========================================================
                if(Temp == 1) // distance test
                    pNeighborCellIDArr[
                        BaseWriteIndex+pSharedPrefixSumArr[SampleCellIndex]
                        ] = pSharedCellIDArr[SampleCellIndex]; // compact write index
                if(Sum >= Query_k) // stop condition
                    x = y = JobsPerQueryEdge;
            }
        pNeighborCellIDCountArr[QueryIndex] = Sum; // neighbor count is final prefix sum
        //=============================================================================
    }
}

//=============================================================================
// 5.3 - Search Kernel 3: Dynamic Shared Memory + 3D Input Iterator + Coalesced Write
//=============================================================================

// [Kernel Summary]
// - input iterator dimension ?                ... 3-d
// - use threadIdx/blockIdx instead of loops ? ... yes

__global__ void KNNSearchKernel_Search_3_Impl(
    /*IN*/  uint32 *pQueryCellIndexArr,      // array of indices corresponding to query requests
    /*IN*/  float  *pQueryRadiusArr,         // array of radii corresponding to query requests
            uint32  QueryCount,              // # of query requests
            uint32  Query_k,                 // # of neighbors to return
            uint32  QueryPadded_k,           // padded "Query_k"
            uint32  QueryCellRadius,         // cell radius of search
            uint32  JobsPerQueryEdge,        // sampling edge size
            uint32  JobsPerQuery,            // sampling area
    /*OUT*/ uint32 *pNeighborCellIDArr,      // array of IDs containing search results
    /*OUT*/ uint32 *pNeighborCellIDCountArr, // array of search result counts per query
    /*IN*/  uint32 *pState_CellIDArr,        // internal state
    /*IN*/  float2 *pState_CellPosArr,       // internal state
    /*IN*/  uint32 *pState_CellFreeArr,      // internal state
            uint32  State_ColCount,          // internal state
            uint32  State_RowCount           // internal state
    )
{
    // [Read Order]
    //
    // pState_CellPosArr:
    // pState_CellIDArr:
    // pState_CellFreeArr:
    // +---+---+---+---+---+---+---+---+---+
    // |   |   |   |   |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 |[ ]| 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   | 1 | 1 | 1 |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   |   |   |   |   | 1 |[ ]| 1 |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   | 1 | 1 | 1 |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 |[ ]| 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   |   |   |   |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+

    // [Write Order]
    //
    // pNeighborCellIDArr:                pNeighborCellIDCountArr:
    // +---+---+---+---+---+---+---+---+  +---+
    // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |  | 1 |  <-- QueryIndex == 0
    // +---+---+---+---+---+---+---+---+  +---+
    // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |  | 1 |  <-- QueryIndex == 1
    // +---+---+---+---+---+---+---+---+  +---+
    // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |  | 1 |  <-- QueryIndex == 2
    // +---+---+---+---+---+---+---+---+  +---+

    extern __shared__ char pArray[];
    uint32 *pSharedPrefixSumArr;
    uint32  Offset = 0;

    ALIGN_UP(Offset, __alignof(pSharedPrefixSumArr));
    pSharedPrefixSumArr = reinterpret_cast<uint32*>(&pArray[Offset]);

    uint32 QueryIndex = OFFSET2D(threadIdx.z, blockIdx.x, blockDim.z);
    int X = 0;
    int Y = 0;
    uint32 SampleCellIndex = 0;
    uint32 MemCellIndex    = 0;
    bool IsValidQueryIndex = (QueryIndex<QueryCount);
    bool IsValidIndex = (
        IsValidQueryIndex &&
        threadIdx.x<JobsPerQueryEdge && threadIdx.y<JobsPerQueryEdge
        );
    if(IsValidIndex) // bounds test
    {
        uint32 QueryMemCellIndex = pQueryCellIndexArr[QueryIndex]; // self
        float  QueryRadiusSqr    = pQueryRadiusArr[QueryIndex]*pQueryRadiusArr[QueryIndex];
        //=============================================================================
        // calc sampling coord
        uint32 QueryMemColIndex = QueryMemCellIndex%State_ColCount;
        uint32 QueryMemRowIndex = QueryMemCellIndex/State_ColCount;
        X = QueryMemColIndex+(
            static_cast<int>(threadIdx.x)-static_cast<int>(QueryCellRadius)
            );
        Y = QueryMemRowIndex+(
            static_cast<int>(threadIdx.y)-static_cast<int>(QueryCellRadius)
            );
        //=============================================================================
        // sample "cell" within radius at sampling coord (from global memory)
        SampleCellIndex =
            OFFSET3D(threadIdx.x, threadIdx.y, threadIdx.z, JobsPerQueryEdge, JobsPerQuery);
        if(
            X >= 0 && X<State_ColCount &&
            Y >= 0 && Y<State_RowCount
            ) // only consider sampling coord with legal extent
        {
            MemCellIndex = OFFSET2D(X, Y, State_ColCount); // current cell
            if(
                pState_CellFreeArr[MemCellIndex] == 0 && // make sure cell is occupied
                MemCellIndex != QueryMemCellIndex        // make sure not comparing with self
                )
            {
                float DeltaX = pState_CellPosArr[MemCellIndex].x-pState_CellPosArr[QueryMemCellIndex].x;
                float DeltaY = pState_CellPosArr[MemCellIndex].y-pState_CellPosArr[QueryMemCellIndex].y;
                // distance test
                pSharedPrefixSumArr[SampleCellIndex] = (DeltaX*DeltaX+DeltaY*DeltaY<QueryRadiusSqr);
            }
            else
                pSharedPrefixSumArr[SampleCellIndex] = 0; // non-candidate cell
        }
        else
            pSharedPrefixSumArr[SampleCellIndex] = 0; // illegal coord
        //=============================================================================
    }
    #if 1
        __syncthreads(); // do we need this ? -- yes
    #endif
    if(
        IsValidQueryIndex &&                 // bounds test
        threadIdx.x == 0 && threadIdx.y == 0 // bounds test
        )
    {
        //=============================================================================
        // calc prefix sum in serial
        uint32 BaseWriteIndex = QueryIndex*QueryPadded_k;
        uint32 Sum = 0;
        for(uint32 y = 0; y<JobsPerQueryEdge; y++)
            for(uint32 x = 0; x<JobsPerQueryEdge; x++)
            {
                uint32 SampleCellIndex = OFFSET3D(x, y, threadIdx.z, JobsPerQueryEdge, JobsPerQuery);
                //=========================================================
                // calc prefix sum
                if(
                    pSharedPrefixSumArr[SampleCellIndex] == 1 && // distance test
                    Sum<Query_k                                  // stop condition
                    )
                {
                    // add one to preserve distance test validity
                    pSharedPrefixSumArr[SampleCellIndex] = BaseWriteIndex+Sum+1;
                    Sum++;
                }
                else
                    pSharedPrefixSumArr[SampleCellIndex] = 0; // nullify distance test result
                //=========================================================
            }
        pNeighborCellIDCountArr[QueryIndex] = Sum; // neighbor count is final prefix sum
        //=============================================================================
    }
    if(
        IsValidIndex &&                           // bounds test
        pSharedPrefixSumArr[SampleCellIndex] != 0 // distance test
        )
    {
        //=============================================================================
        // write result (to global memory)
        // X and Y guaranteed legal after distance test
        pNeighborCellIDArr[
            pSharedPrefixSumArr[SampleCellIndex]-1 // minus one to restore original value
            ] = pState_CellIDArr[MemCellIndex];    // compact write index
        //=============================================================================
    }
}

//=============================================================================
// 5.4 - Search Kernel 4: Texture Memory + 3D Input Iterator + Coalesced Write
//=============================================================================

// [Kernel Summary]
// - input iterator dimension ?                ... 3-d
// - use threadIdx/blockIdx instead of loops ? ... yes

__global__ void KNNSearchKernel_Search_4_Impl(
    /*IN*/  uint32 *pQueryCellIndexArr,      // array of indices corresponding to query requests
    /*IN*/  float  *pQueryRadiusArr,         // array of radii corresponding to query requests
            uint32  QueryCount,              // # of query requests
            uint32  Query_k,                 // # of neighbors to return
            uint32  QueryPadded_k,           // padded "Query_k"
            uint32  QueryCellRadius,         // cell radius of search
            uint32  JobsPerQueryEdge,        // sampling edge size
            uint32  JobsPerQuery,            // sampling area
    /*OUT*/ uint32 *pNeighborCellIDArr,      // array of IDs containing search results
    /*OUT*/ uint32 *pNeighborCellIDCountArr, // array of search result counts per query
            uint32  State_ColCount,          // internal state
            uint32  State_RowCount           // internal state
    )
{
    // [Read Order]
    //
    // pState_CellPosArr:
    // pState_CellIDArr:
    // pState_CellFreeArr:
    // +---+---+---+---+---+---+---+---+---+
    // |   |   |   |   |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 |[ ]| 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   | 1 | 1 | 1 |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   |   |   |   |   | 1 |[ ]| 1 |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   | 1 | 1 | 1 |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 |[ ]| 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   | 1 | 1 | 1 |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+
    // |   |   |   |   |   |   |   |   |   |
    // +---+---+---+---+---+---+---+---+---+

    // [Write Order]
    //
    // pNeighborCellIDArr:                pNeighborCellIDCountArr:
    // +---+---+---+---+---+---+---+---+  +---+
    // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |  | 1 |  <-- QueryIndex == 0
    // +---+---+---+---+---+---+---+---+  +---+
    // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |  | 1 |  <-- QueryIndex == 1
    // +---+---+---+---+---+---+---+---+  +---+
    // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |  | 1 |  <-- QueryIndex == 2
    // +---+---+---+---+---+---+---+---+  +---+

    extern __shared__ char pArray[];
    uint32 *pSharedPrefixSumArr;
    uint32  Offset = 0;

    ALIGN_UP(Offset, __alignof(pSharedPrefixSumArr));
    pSharedPrefixSumArr = reinterpret_cast<uint32*>(&pArray[Offset]);

    uint32 QueryIndex = OFFSET2D(threadIdx.z, blockIdx.x, blockDim.z);
    int X = 0;
    int Y = 0;
    uint32 SampleCellIndex = 0;
    bool IsValidQueryIndex = (QueryIndex<QueryCount);
    bool IsValidIndex = (
        IsValidQueryIndex &&
        threadIdx.x<JobsPerQueryEdge && threadIdx.y<JobsPerQueryEdge
        );
    if(IsValidIndex) // bounds test
    {
        uint32 QueryMemCellIndex = pQueryCellIndexArr[QueryIndex]; // self
        float  QueryRadiusSqr    = pQueryRadiusArr[QueryIndex]*pQueryRadiusArr[QueryIndex];
        //=============================================================================
        // calc sampling coord
        uint32 QueryMemColIndex = QueryMemCellIndex%State_ColCount;
        uint32 QueryMemRowIndex = QueryMemCellIndex/State_ColCount;
        float2 QueryCellPos = tex2D(CellPosTex, QueryMemColIndex, QueryMemRowIndex);
        X = QueryMemColIndex+(
            static_cast<int>(threadIdx.x)-static_cast<int>(QueryCellRadius)
            );
        Y = QueryMemRowIndex+(
            static_cast<int>(threadIdx.y)-static_cast<int>(QueryCellRadius)
            );
        //=============================================================================
        // sample "cell" within radius at sampling coord (from global memory)
        SampleCellIndex =
            OFFSET3D(threadIdx.x, threadIdx.y, threadIdx.z, JobsPerQueryEdge, JobsPerQuery);
        if(
            X >= 0 && X<State_ColCount &&
            Y >= 0 && Y<State_RowCount
            ) // only consider sampling coord with legal extent
        {
            if(
                tex2D(CellFreeTex, X, Y) == 0 &&                  // make sure cell is occupied
                !(X == QueryMemColIndex && Y == QueryMemRowIndex) // make sure not comparing with self
                )
            {
                float2 CellPos = tex2D(CellPosTex, X, Y);
                float  DeltaX  = CellPos.x-QueryCellPos.x;
                float  DeltaY  = CellPos.y-QueryCellPos.y;
                // distance test
                pSharedPrefixSumArr[SampleCellIndex] = (DeltaX*DeltaX+DeltaY*DeltaY<QueryRadiusSqr);
            }
            else
                pSharedPrefixSumArr[SampleCellIndex] = 0; // non-candidate cell
        }
        else
            pSharedPrefixSumArr[SampleCellIndex] = 0; // illegal coord
        //=============================================================================
    }
    #if 1
        __syncthreads(); // do we need this ? -- yes
    #endif
    if(
        IsValidQueryIndex &&                 // bounds test
        threadIdx.x == 0 && threadIdx.y == 0 // bounds test
        )
    {
        //=============================================================================
        // calc prefix sum in serial
        uint32 BaseWriteIndex = QueryIndex*QueryPadded_k;
        uint32 Sum = 0;
        for(uint32 y = 0; y<JobsPerQueryEdge; y++)
            for(uint32 x = 0; x<JobsPerQueryEdge; x++)
            {
                uint32 SampleCellIndex = OFFSET3D(x, y, threadIdx.z, JobsPerQueryEdge, JobsPerQuery);
                //=========================================================
                // calc prefix sum
                if(
                    pSharedPrefixSumArr[SampleCellIndex] == 1 && // distance test
                    Sum<Query_k                                  // stop condition
                    )
                {
                    // add one to preserve distance test validity
                    pSharedPrefixSumArr[SampleCellIndex] = BaseWriteIndex+Sum+1;
                    Sum++;
                }
                else
                    pSharedPrefixSumArr[SampleCellIndex] = 0; // nullify distance test result
                //=========================================================
            }
        pNeighborCellIDCountArr[QueryIndex] = Sum; // neighbor count is final prefix sum
        //=============================================================================
    }
    if(
        IsValidIndex &&                           // bounds test
        pSharedPrefixSumArr[SampleCellIndex] != 0 // distance test
        )
    {
        //=============================================================================
        // write result (to global memory)
        // X and Y guaranteed legal after distance test
        pNeighborCellIDArr[
            pSharedPrefixSumArr[SampleCellIndex]-1 // minus one to restore original value
            ] = tex2D(CellIDTex, X, Y); // compact write index
        //=============================================================================
    }
}

//=============================================================================
// 6. Texture Memory Writeback Kernel
//=============================================================================

__global__ void KNNSearchKernel_TextureTest_Impl(
    /*IN*/  uint32 *pInputArr,
    /*OUT*/ uint32 *pOutputArr,
            uint32  ColCount,
            uint32  RowCount
    )
{
    uint32 CellIndex = OFFSET2D(threadIdx.x, blockIdx.x, blockDim.x);
    uint32 ColIndex = CellIndex%ColCount;
    uint32 RowIndex = CellIndex/ColCount;
    uint32 CellCount = ColCount*RowCount;
    if(CellIndex >= CellCount) // bounds test
        return;
    pOutputArr[CellIndex] = tex2D(InputTex, ColIndex, RowIndex)*2;
}

//=====================================
// 7. Kernel Wrapper
//=====================================

void KNNSearchKernel_TextureTest(
    /*IN*/  uint32 *pInputArr,
    /*OUT*/ uint32 *pOutputArr,
            uint32  ColCount,
            uint32  RowCount
    )
{
    uint32 CellCount       = ColCount*RowCount;
    uint32 JobCount        = CellCount;
    uint32 ThreadsPerBlock = MIN(JobCount, MAX_THREADS_PER_BLOCK);
    uint32 BlocksPerGrid   = (JobCount-1)/ThreadsPerBlock+1;
    KNNSearchKernel_TextureTest_Impl
        <<<
        BlocksPerGrid,
        ThreadsPerBlock
        >>>(
        /*IN*/  pInputArr,
        /*OUT*/ pOutputArr,
                ColCount,
                RowCount
        );
}
void KNNSearchKernel_Add(
    /*IN*/    uint32 *pAddCellIDArr,
    /*IN*/    float2 *pAddCellPosArr,
    /*IN*/    uint64 *pAddCellIndexPtrArr,
              uint32  AddCount,
    /*OUT*/   uint32 *pState_CellIDArr,
    /*OUT*/   float2 *pState_CellPosArr,
    /*INOUT*/ uint64 *pState_CellIndexPtrArr,
    /*INOUT*/ uint32 *pState_CellFreeArr,
              uint32  State_CellCount,
    /*IN*/    uint32 *pTemp_PrefixSumArr,
    /*INOUT*/ uint32 *pTemp_WriteIndexArr
    )
{
    uint32 JobCount        = State_CellCount;
    uint32 ThreadsPerBlock = MIN(JobCount, MAX_THREADS_PER_BLOCK);
    uint32 BlocksPerGrid   = (JobCount-1)/ThreadsPerBlock+1;
    #ifdef DEBUG
        std::cout << std::string(20, '=')                   << std::endl;
        std::cout << "JobCount: "        << JobCount        << std::endl;
        std::cout << "BlocksPerGrid: "   << BlocksPerGrid   << std::endl;
        std::cout << "ThreadsPerBlock: " << ThreadsPerBlock << std::endl;
        std::cout << std::string(20, '=')                   << std::endl;
    #endif
    KNNSearchKernel_Add_Impl
        <<<
        BlocksPerGrid,
        ThreadsPerBlock
        >>>(
        /*IN*/    pAddCellIDArr,
        /*IN*/    pAddCellPosArr,
        /*IN*/    pAddCellIndexPtrArr,
                  AddCount,
        /*OUT*/   pState_CellIDArr,
        /*OUT*/   pState_CellPosArr,
        /*INOUT*/ pState_CellIndexPtrArr,
        /*INOUT*/ pState_CellFreeArr,
                  State_CellCount,
        /*IN*/    pTemp_PrefixSumArr,
        /*INOUT*/ pTemp_WriteIndexArr
        );
    cudaError Error = cudaThreadSynchronize();
    if(Error != cudaSuccess)
    {
        #ifdef DEBUG
            std::cout << "CUDA Exception: " << cudaGetErrorString(Error) <<
                std::endl;
        #endif
    }
}
void KNNSearchKernel_Remove(
    /*IN*/    uint32 *pRemoveCellIndexArr,
              uint32  RemoveCount,
              bool    IsUpdateCellIndexPtr,
    /*OUT*/   uint32 *pState_CellIDArr,
    /*OUT*/   float2 *pState_CellPosArr,
    /*INOUT*/ uint64 *pState_CellIndexPtrArr,
    /*INOUT*/ uint32 *pState_CellFreeArr,
              uint32  State_CellCount
    )
{
    uint32 JobCount        = RemoveCount;
    uint32 ThreadsPerBlock = MIN(JobCount, MAX_THREADS_PER_BLOCK);
    uint32 BlocksPerGrid   = (JobCount-1)/ThreadsPerBlock+1;
    #ifdef DEBUG
        std::cout << std::string(20, '=')                   << std::endl;
        std::cout << "JobCount: "        << JobCount        << std::endl;
        std::cout << "BlocksPerGrid: "   << BlocksPerGrid   << std::endl;
        std::cout << "ThreadsPerBlock: " << ThreadsPerBlock << std::endl;
        std::cout << std::string(20, '=')                   << std::endl;
    #endif
    KNNSearchKernel_Remove_Impl
        <<<
        BlocksPerGrid,
        ThreadsPerBlock
        >>>(
        /*IN*/    pRemoveCellIndexArr,
                  RemoveCount,
                  IsUpdateCellIndexPtr,
        /*OUT*/   pState_CellIDArr,
        /*OUT*/   pState_CellPosArr,
        /*INOUT*/ pState_CellIndexPtrArr,
        /*INOUT*/ pState_CellFreeArr,
                  State_CellCount
        );
    cudaError Error = cudaThreadSynchronize();
    if(Error != cudaSuccess)
    {
        #ifdef DEBUG
            std::cout << "CUDA Exception: " << cudaGetErrorString(Error) <<
                std::endl;
        #endif
    }
}
void KNNSearchKernel_Update(
    /*IN*/  uint32 *pUpdateCellIndexArr,
    /*IN*/  float2 *pUpdateCellPosArr,
            uint32  UpdateCount,
    /*OUT*/ float2 *pState_CellPosArr,
            uint32  State_CellCount
    )
{
    uint32 JobCount        = UpdateCount;
    uint32 ThreadsPerBlock = MIN(JobCount, MAX_THREADS_PER_BLOCK);
    uint32 BlocksPerGrid   = (JobCount-1)/ThreadsPerBlock+1;
    #ifdef DEBUG
        std::cout << std::string(20, '=')                   << std::endl;
        std::cout << "JobCount: "        << JobCount        << std::endl;
        std::cout << "BlocksPerGrid: "   << BlocksPerGrid   << std::endl;
        std::cout << "ThreadsPerBlock: " << ThreadsPerBlock << std::endl;
        std::cout << std::string(20, '=')                   << std::endl;
    #endif
    KNNSearchKernel_Update_Impl
        <<<
        BlocksPerGrid,
        ThreadsPerBlock
        >>>(
        /*IN*/  pUpdateCellIndexArr,
        /*IN*/  pUpdateCellPosArr,
                UpdateCount,
        /*OUT*/ pState_CellPosArr,
                State_CellCount
        );
    cudaError Error = cudaThreadSynchronize();
    if(Error != cudaSuccess)
    {
        #ifdef DEBUG
            std::cout << "CUDA Exception: " << cudaGetErrorString(Error) <<
                std::endl;
        #endif
    }
}
void KNNSearchKernel_Step(
              uint32  StepMode,
    /*INOUT*/ uint32 *pState_CellIDArr,
    /*INOUT*/ float2 *pState_CellPosArr,
    /*INOUT*/ uint64 *pState_CellIndexPtrArr,
    /*INOUT*/ uint32 *pState_CellFreeArr,
              uint32  State_ColCount,
              uint32  State_RowCount
    )
{
    dim3 JobCount(
        State_ColCount,
        State_RowCount
        );
    dim3 BlocksPerGrid(
        (JobCount.x-1)/MAX_JOBS_PER_BLOCK_EDGE+1,
        (JobCount.y-1)/MAX_JOBS_PER_BLOCK_EDGE+1
        );
    uint32 MaxThreadsPerBlockEdge =
        static_cast<uint32>(sqrt(
            static_cast<double>(MAX_THREADS_PER_BLOCK)
            ));
    // NOTE: permit no more threads (per block) than jobs available (per block)
    if(MaxThreadsPerBlockEdge>MAX_JOBS_PER_BLOCK_EDGE)
        MaxThreadsPerBlockEdge = MAX_JOBS_PER_BLOCK_EDGE;
    dim3 ThreadsPerBlock(
        MaxThreadsPerBlockEdge,
        MaxThreadsPerBlockEdge
        );
    #ifdef DEBUG
        std::cout << std::string(20, '=')                                  << std::endl;
        std::cout << "MaxJobsPerBlockEdge: "    << MAX_JOBS_PER_BLOCK_EDGE << std::endl;
        std::cout << "JobCount.x: "             << JobCount.x              << std::endl;
        std::cout << "JobCount.y: "             << JobCount.y              << std::endl;
        std::cout << "BlocksPerGrid.x: "        << BlocksPerGrid.x         << std::endl;
        std::cout << "BlocksPerGrid.y: "        << BlocksPerGrid.y         << std::endl;
        std::cout << "MaxThreadsPerBlockEdge: " << MaxThreadsPerBlockEdge  << std::endl;
        std::cout << std::string(20, '=')                                  << std::endl;
    #endif
    switch(StepMode)
    {
    case 0:
        //=====================================
        // Call Step Kernel 0
        //=====================================
        KNNSearchKernel_Step_Naive_Impl
            <<<
            BlocksPerGrid,
            ThreadsPerBlock
            >>>(
            /*INOUT*/ pState_CellIDArr,
            /*INOUT*/ pState_CellPosArr,
            /*INOUT*/ pState_CellIndexPtrArr,
            /*INOUT*/ pState_CellFreeArr,
                      State_ColCount,
                      State_RowCount
            );
        break;
    case 1:
        //=====================================
        // Call Step Kernel 1
        //=====================================
        KNNSearchKernel_Step_1_Col_Impl<true>
            <<<
            BlocksPerGrid,
            ThreadsPerBlock
            >>>(
            /*INOUT*/ pState_CellIDArr,
            /*INOUT*/ pState_CellPosArr,
            /*INOUT*/ pState_CellIndexPtrArr,
            /*INOUT*/ pState_CellFreeArr,
                      State_ColCount,
                      State_RowCount
            );
        KNNSearchKernel_Step_1_Col_Impl<false>
            <<<
            BlocksPerGrid,
            ThreadsPerBlock
            >>>(
            /*INOUT*/ pState_CellIDArr,
            /*INOUT*/ pState_CellPosArr,
            /*INOUT*/ pState_CellIndexPtrArr,
            /*INOUT*/ pState_CellFreeArr,
                      State_ColCount,
                      State_RowCount
            );
        KNNSearchKernel_Step_1_Row_Impl<true>
            <<<
            BlocksPerGrid,
            ThreadsPerBlock
            >>>(
            /*INOUT*/ pState_CellIDArr,
            /*INOUT*/ pState_CellPosArr,
            /*INOUT*/ pState_CellIndexPtrArr,
            /*INOUT*/ pState_CellFreeArr,
                      State_ColCount,
                      State_RowCount
            );
        KNNSearchKernel_Step_1_Row_Impl<false>
            <<<
            BlocksPerGrid,
            ThreadsPerBlock
            >>>(
            /*INOUT*/ pState_CellIDArr,
            /*INOUT*/ pState_CellPosArr,
            /*INOUT*/ pState_CellIndexPtrArr,
            /*INOUT*/ pState_CellFreeArr,
                      State_ColCount,
                      State_RowCount
            );
        break;
    case 2:
        //=====================================
        // Call Step Kernel 2
        //=====================================
        KNNSearchKernel_Step_2_Col_Impl<MAX_JOBS_PER_BLOCK_EDGE, true>
            <<<
            BlocksPerGrid,
            ThreadsPerBlock
            >>>(
            /*INOUT*/ pState_CellIDArr,
            /*INOUT*/ pState_CellPosArr,
            /*INOUT*/ pState_CellIndexPtrArr,
            /*INOUT*/ pState_CellFreeArr,
                      State_ColCount,
                      State_RowCount
            );
        KNNSearchKernel_Step_2_Col_Impl<MAX_JOBS_PER_BLOCK_EDGE, false>
            <<<
            BlocksPerGrid,
            ThreadsPerBlock
            >>>(
            /*INOUT*/ pState_CellIDArr,
            /*INOUT*/ pState_CellPosArr,
            /*INOUT*/ pState_CellIndexPtrArr,
            /*INOUT*/ pState_CellFreeArr,
                      State_ColCount,
                      State_RowCount
            );
        KNNSearchKernel_Step_2_Row_Impl<MAX_JOBS_PER_BLOCK_EDGE, true>
            <<<
            BlocksPerGrid,
            ThreadsPerBlock
            >>>(
            /*INOUT*/ pState_CellIDArr,
            /*INOUT*/ pState_CellPosArr,
            /*INOUT*/ pState_CellIndexPtrArr,
            /*INOUT*/ pState_CellFreeArr,
                      State_ColCount,
                      State_RowCount
            );
        KNNSearchKernel_Step_2_Row_Impl<MAX_JOBS_PER_BLOCK_EDGE, false>
            <<<
            BlocksPerGrid,
            ThreadsPerBlock
            >>>(
            /*INOUT*/ pState_CellIDArr,
            /*INOUT*/ pState_CellPosArr,
            /*INOUT*/ pState_CellIndexPtrArr,
            /*INOUT*/ pState_CellFreeArr,
                      State_ColCount,
                      State_RowCount
            );
        break;
    case 3:
        //=====================================
        // Call Step Kernel 3
        //=====================================
        {
            uint32 SharedMemorySize =
                sizeof(uint64)*MAX_JOBS_PER_BLOCK_EDGE*MAX_JOBS_PER_BLOCK_EDGE;
            KNNSearchKernel_Step_3_Col_Impl<MAX_JOBS_PER_BLOCK_EDGE, true>
                <<<
                BlocksPerGrid,
                ThreadsPerBlock,
                SharedMemorySize
                >>>(
                /*INOUT*/ pState_CellIDArr,
                /*INOUT*/ pState_CellPosArr,
                /*INOUT*/ pState_CellIndexPtrArr,
                /*INOUT*/ pState_CellFreeArr,
                          State_ColCount,
                          State_RowCount
                );
            KNNSearchKernel_Step_3_Col_Impl<MAX_JOBS_PER_BLOCK_EDGE, false>
                <<<
                BlocksPerGrid,
                ThreadsPerBlock,
                SharedMemorySize
                >>>(
                /*INOUT*/ pState_CellIDArr,
                /*INOUT*/ pState_CellPosArr,
                /*INOUT*/ pState_CellIndexPtrArr,
                /*INOUT*/ pState_CellFreeArr,
                          State_ColCount,
                          State_RowCount
                );
            KNNSearchKernel_Step_3_Row_Impl<MAX_JOBS_PER_BLOCK_EDGE, true>
                <<<
                BlocksPerGrid,
                ThreadsPerBlock,
                SharedMemorySize
                >>>(
                /*INOUT*/ pState_CellIDArr,
                /*INOUT*/ pState_CellPosArr,
                /*INOUT*/ pState_CellIndexPtrArr,
                /*INOUT*/ pState_CellFreeArr,
                          State_ColCount,
                          State_RowCount
                );
            KNNSearchKernel_Step_3_Row_Impl<MAX_JOBS_PER_BLOCK_EDGE, false>
                <<<
                BlocksPerGrid,
                ThreadsPerBlock,
                SharedMemorySize
                >>>(
                /*INOUT*/ pState_CellIDArr,
                /*INOUT*/ pState_CellPosArr,
                /*INOUT*/ pState_CellIndexPtrArr,
                /*INOUT*/ pState_CellFreeArr,
                          State_ColCount,
                          State_RowCount
                );
        }
        break;
    case 4:
        //=====================================
        // Call Step Kernel 4
        //=====================================
        {
            uint32 SharedMemorySize =
                sizeof(uint64)*MAX_JOBS_PER_BLOCK_EDGE*MAX_JOBS_PER_BLOCK_EDGE;
            KNNSearchKernel_Step_3_Col_Impl<MAX_JOBS_PER_BLOCK_EDGE, true>
                <<<
                BlocksPerGrid,
                ThreadsPerBlock,
                SharedMemorySize
                >>>(
                /*INOUT*/ pState_CellIDArr,
                /*INOUT*/ pState_CellPosArr,
                /*INOUT*/ pState_CellIndexPtrArr,
                /*INOUT*/ pState_CellFreeArr,
                          State_ColCount,
                          State_RowCount
                );
            KNNSearchKernel_Step_3_Col_Impl<MAX_JOBS_PER_BLOCK_EDGE, false>
                <<<
                BlocksPerGrid,
                ThreadsPerBlock,
                SharedMemorySize
                >>>(
                /*INOUT*/ pState_CellIDArr,
                /*INOUT*/ pState_CellPosArr,
                /*INOUT*/ pState_CellIndexPtrArr,
                /*INOUT*/ pState_CellFreeArr,
                          State_ColCount,
                          State_RowCount
                );
        }
        KNNSearchKernel_Step_1_Row_Impl<true>
            <<<
            BlocksPerGrid,
            ThreadsPerBlock
            >>>(
            /*INOUT*/ pState_CellIDArr,
            /*INOUT*/ pState_CellPosArr,
            /*INOUT*/ pState_CellIndexPtrArr,
            /*INOUT*/ pState_CellFreeArr,
                      State_ColCount,
                      State_RowCount
            );
        KNNSearchKernel_Step_1_Row_Impl<false>
            <<<
            BlocksPerGrid,
            ThreadsPerBlock
            >>>(
            /*INOUT*/ pState_CellIDArr,
            /*INOUT*/ pState_CellPosArr,
            /*INOUT*/ pState_CellIndexPtrArr,
            /*INOUT*/ pState_CellFreeArr,
                      State_ColCount,
                      State_RowCount
            );
        break;
    default:
        break;
    }
    cudaError Error = cudaThreadSynchronize();
    if(Error != cudaSuccess)
    {
        #ifdef DEBUG
            std::cout << "CUDA Exception: " << cudaGetErrorString(Error) <<
                std::endl;
        #endif
    }
}
void KNNSearchKernel_Search(
            uint32  QueryMode,
    /*IN*/  uint32 *pQueryCellIndexArr,
    /*IN*/  float  *pQueryRadiusArr,
            uint32  QueryCount,
            uint32  Query_k,
            uint32  QueryPadded_k,
            uint32  QueryCellRadius,
            uint32  JobsPerQueryEdge,
            uint32  JobsPerQuery,
    /*OUT*/ uint32 *pNeighborCellIDArr,
    /*OUT*/ uint32 *pNeighborCellIDCountArr,
    /*IN*/  uint32 *pState_CellIDArr,
    /*IN*/  float2 *pState_CellPosArr,
    /*IN*/  uint32 *pState_CellFreeArr,
            uint32  State_CellCount,
            uint32  State_ColCount,
            uint32  State_RowCount
    )
{
    switch(QueryMode)
    {
    case 0:
        //=====================================
        // Call Search Kernel 0
        //=====================================
        {
            uint32 JobCount        = QueryCount;
            uint32 ThreadsPerBlock = MIN(JobCount, MAX_THREADS_PER_BLOCK);
            uint32 BlocksPerGrid   = (JobCount-1)/ThreadsPerBlock+1;
            #ifdef DEBUG
                std::cout << std::string(20, '=')                   << std::endl;
                std::cout << "JobCount: "        << JobCount        << std::endl;
                std::cout << "BlocksPerGrid: "   << BlocksPerGrid   << std::endl;
                std::cout << "ThreadsPerBlock: " << ThreadsPerBlock << std::endl;
                std::cout << std::string(20, '=')                   << std::endl;
            #endif
            KNNSearchKernel_Search_Naive_Impl
                <<<
                BlocksPerGrid,
                ThreadsPerBlock
                >>>(
                /*IN*/  pQueryCellIndexArr,
                /*IN*/  pQueryRadiusArr,
                        QueryCount,
                        Query_k,
                        QueryPadded_k,
                        QueryCellRadius,
                /*OUT*/ pNeighborCellIDArr,
                /*OUT*/ pNeighborCellIDCountArr,
                /*IN*/  pState_CellIDArr,
                /*IN*/  pState_CellPosArr,
                /*IN*/  pState_CellFreeArr,
                        State_ColCount,
                        State_RowCount
                );
        }
        break;
    case 1:
        //=====================================
        // Call Search Kernel 1
        //=====================================
        {
            uint32 SampleCellCount = QueryCount*Query_k;
            uint32 JobCount        = SampleCellCount;
            uint32 ThreadsPerBlock = MIN(JobCount, MAX_THREADS_PER_BLOCK);
            uint32 BlocksPerGrid   = (JobCount-1)/ThreadsPerBlock+1;
            #ifdef DEBUG
                std::cout << std::string(20, '=')                   << std::endl;
                std::cout << "JobCount: "        << JobCount        << std::endl;
                std::cout << "BlocksPerGrid: "   << BlocksPerGrid   << std::endl;
                std::cout << "ThreadsPerBlock: " << ThreadsPerBlock << std::endl;
                std::cout << std::string(20, '=')                   << std::endl;
            #endif
            uint32 SharedMemorySize = SampleCellCount*2*sizeof(uint32);
            KNNSearchKernel_Search_1_Impl
                <<<
                BlocksPerGrid,
                ThreadsPerBlock,
                SharedMemorySize
                >>>(
                /*IN*/  pQueryCellIndexArr,
                /*IN*/  pQueryRadiusArr,
                        QueryCount,
                        Query_k,
                        QueryPadded_k,
                        QueryCellRadius,
                        JobsPerQueryEdge,
                        JobsPerQuery,
                /*OUT*/ pNeighborCellIDArr,
                /*OUT*/ pNeighborCellIDCountArr,
                /*IN*/  pState_CellIDArr,
                /*IN*/  pState_CellPosArr,
                /*IN*/  pState_CellFreeArr,
                        State_ColCount,
                        State_RowCount
                );
        }
        break;
    case 2:
        //=====================================
        // Call Search Kernel 2
        //=====================================
        {
            uint32 JobCount          = QueryCount;
            uint32 MaxThreadsPerJobZ = MAX_THREADS_PER_BLOCK/JobsPerQuery;
            dim3 ThreadsPerBlock(
                JobsPerQueryEdge, JobsPerQueryEdge,
                MIN(JobCount, MaxThreadsPerJobZ)
                );
            dim3 BlocksPerGrid(
                (JobCount-1)/ThreadsPerBlock.z+1,
                1, 1
                );
            #ifdef DEBUG
                std::cout << std::string(20, '=')                       << std::endl;
                std::cout << "JobCount: "          << JobCount          << std::endl;
                std::cout << "BlocksPerGrid.x: "   << BlocksPerGrid.x   << std::endl;
                std::cout << "BlocksPerGrid.y: "   << BlocksPerGrid.y   << std::endl;
                std::cout << "BlocksPerGrid.z: "   << BlocksPerGrid.z   << std::endl;
                std::cout << "ThreadsPerBlock.x: " << ThreadsPerBlock.x << std::endl;
                std::cout << "ThreadsPerBlock.y: " << ThreadsPerBlock.y << std::endl;
                std::cout << "ThreadsPerBlock.z: " << ThreadsPerBlock.z << std::endl;
                std::cout << std::string(20, '=')                       << std::endl;
            #endif
            uint32 SharedMemorySize = ThreadsPerBlock.z*JobsPerQuery*2*sizeof(uint32);
            KNNSearchKernel_Search_2_Impl
                <<<
                BlocksPerGrid,
                ThreadsPerBlock,
                SharedMemorySize
                >>>(
                /*IN*/  pQueryCellIndexArr,
                /*IN*/  pQueryRadiusArr,
                        QueryCount,
                        Query_k,
                        QueryPadded_k,
                        QueryCellRadius,
                        JobsPerQueryEdge,
                        JobsPerQuery,
                /*OUT*/ pNeighborCellIDArr,
                /*OUT*/ pNeighborCellIDCountArr,
                /*IN*/  pState_CellIDArr,
                /*IN*/  pState_CellPosArr,
                /*IN*/  pState_CellFreeArr,
                        State_ColCount,
                        State_RowCount
                );
        }
        break;
    case 3:
        //=====================================
        // Call Search Kernel 3
        //=====================================
        {
            uint32 JobCount          = QueryCount;
            uint32 MaxThreadsPerJobZ = MAX_THREADS_PER_BLOCK/JobsPerQuery;
            dim3 ThreadsPerBlock(
                JobsPerQueryEdge, JobsPerQueryEdge,
                MIN(JobCount, MaxThreadsPerJobZ)
                );
            dim3 BlocksPerGrid(
                (JobCount-1)/ThreadsPerBlock.z+1,
                1, 1
                );
            #ifdef DEBUG
                std::cout << std::string(20, '=')                       << std::endl;
                std::cout << "JobCount: "          << JobCount          << std::endl;
                std::cout << "BlocksPerGrid.x: "   << BlocksPerGrid.x   << std::endl;
                std::cout << "BlocksPerGrid.y: "   << BlocksPerGrid.y   << std::endl;
                std::cout << "BlocksPerGrid.z: "   << BlocksPerGrid.z   << std::endl;
                std::cout << "ThreadsPerBlock.x: " << ThreadsPerBlock.x << std::endl;
                std::cout << "ThreadsPerBlock.y: " << ThreadsPerBlock.y << std::endl;
                std::cout << "ThreadsPerBlock.z: " << ThreadsPerBlock.z << std::endl;
                std::cout << std::string(20, '=')                       << std::endl;
            #endif
            uint32 SharedMemorySize = ThreadsPerBlock.z*JobsPerQuery*sizeof(uint32);
            KNNSearchKernel_Search_3_Impl
                <<<
                BlocksPerGrid,
                ThreadsPerBlock,
                SharedMemorySize
                >>>(
                /*IN*/  pQueryCellIndexArr,
                /*IN*/  pQueryRadiusArr,
                        QueryCount,
                        Query_k,
                        QueryPadded_k,
                        QueryCellRadius,
                        JobsPerQueryEdge,
                        JobsPerQuery,
                /*OUT*/ pNeighborCellIDArr,
                /*OUT*/ pNeighborCellIDCountArr,
                /*IN*/  pState_CellIDArr,
                /*IN*/  pState_CellPosArr,
                /*IN*/  pState_CellFreeArr,
                        State_ColCount,
                        State_RowCount
                );
        }
        break;
    case 4:
        //=====================================
        // Call Search Kernel 4
        //=====================================
        {
            uint32 JobCount          = QueryCount;
            uint32 MaxThreadsPerJobZ = MAX_THREADS_PER_BLOCK/JobsPerQuery;
            dim3 ThreadsPerBlock(
                JobsPerQueryEdge, JobsPerQueryEdge,
                MIN(JobCount, MaxThreadsPerJobZ)
                );
            dim3 BlocksPerGrid(
                (JobCount-1)/ThreadsPerBlock.z+1,
                1, 1
                );
            #ifdef DEBUG
                std::cout << std::string(20, '=')                       << std::endl;
                std::cout << "JobCount: "          << JobCount          << std::endl;
                std::cout << "BlocksPerGrid.x: "   << BlocksPerGrid.x   << std::endl;
                std::cout << "BlocksPerGrid.y: "   << BlocksPerGrid.y   << std::endl;
                std::cout << "BlocksPerGrid.z: "   << BlocksPerGrid.z   << std::endl;
                std::cout << "ThreadsPerBlock.x: " << ThreadsPerBlock.x << std::endl;
                std::cout << "ThreadsPerBlock.y: " << ThreadsPerBlock.y << std::endl;
                std::cout << "ThreadsPerBlock.z: " << ThreadsPerBlock.z << std::endl;
                std::cout << std::string(20, '=')                       << std::endl;
            #endif
            uint32 SharedMemorySize = ThreadsPerBlock.z*JobsPerQuery*sizeof(uint32);
            KNNSearchKernel_Search_4_Impl
                <<<
                BlocksPerGrid,
                ThreadsPerBlock,
                SharedMemorySize
                >>>(
                /*IN*/  pQueryCellIndexArr,
                /*IN*/  pQueryRadiusArr,
                        QueryCount,
                        Query_k,
                        QueryPadded_k,
                        QueryCellRadius,
                        JobsPerQueryEdge,
                        JobsPerQuery,
                /*OUT*/ pNeighborCellIDArr,
                /*OUT*/ pNeighborCellIDCountArr,
                        State_ColCount,
                        State_RowCount
                );
        }
        break;
    default:
        break;
    }
    cudaError Error = cudaThreadSynchronize();
    if(Error != cudaSuccess)
    {
        #ifdef DEBUG
            std::cout << "CUDA Exception: " << cudaGetErrorString(Error) <<
                std::endl;
        #endif
    }
}

} } } } } /*zillians::vw::processors::cuda*/
