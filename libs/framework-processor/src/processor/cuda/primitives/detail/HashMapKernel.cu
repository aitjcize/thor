/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/processor/cuda/primitives/detail/CudaKernelCommon.h"
#include "framework/processor/cuda/primitives/HashMapApi.h"

HASHMAPAPI_DECLARE_GLOBAL;

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail {

__global__ void HashMapKernel_RedistributePairsIntoBucket_Impl(uint32 pairCount, uint2* pairs, uint32 bucketCount, uint32* bucketSizes, uint2* pairsInBucket, uint32 bucketHash0, uint32 bucketHash1)
{
	const uint threadId = blockIdx.x * blockDim.x + threadIdx.x;

	if(threadId < pairCount)
	{
		uint2 k = pairs[threadId];
		uint h = ((bucketHash0 + bucketHash1 * k.x) % 1900813) % bucketCount;
		uint offset = atomicAdd(&(bucketSizes[h]), 1);
		uint start = h * 512;
		pairsInBucket[start + offset] = k;
	}
}

bool HashMapKernel_RedistributePairsIntoBucket(uint32 pairCount, uint2* pairs, uint32 bucketCount, uint32* bucketSizes, uint2* pairsInBucket, uint32 bucketHash0, uint32 bucketHash1)
{
	uint threads = 512;
	uint blocks  = pairCount / threads + ((pairCount % threads == 0) ? 0 : 1);

	HashMapKernel_RedistributePairsIntoBucket_Impl<<<blocks, threads>>>(
			pairCount, pairs, bucketCount, bucketSizes, pairsInBucket, bucketHash0, bucketHash1);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

#define WARP_SIZE				32
#define _ENABLE_DEBUG			0
#define _ENABLE_FAST_REDUCTION	1

__global__ void HashMapKernel_BuildCuckooHashingInBucket_Impl(uint32 bucketCount, uint32* bucketSizes, uint2* pairsInBucket, uint32 cuckooHash, uint2* hashTables, uint32* buildErrors)
{
#if _ENABLE_FAST_REDUCTION
	uint32 positioned;
#else
	volatile __shared__ uint32 positioned[512];
#endif

#if _ENABLE_DEBUG
	volatile __shared__ uint32 positionedState[512];
#endif
	volatile __shared__ uint32 sharedKeys[3*192];
	volatile __shared__ uint32 sharedValues[3*192];
	volatile __shared__ uint32 done[512/WARP_SIZE];
	volatile __shared__ uint32 flag;
	volatile __shared__ uint32 size;

	const uint32 threadId = blockIdx.x * blockDim.x + threadIdx.x;

	// compute all hash function variable ci0 and ci1 by XORing with hash seed
	uint32 c00 = cuckooHash & 0x2B780D61;
	uint32 c01 = cuckooHash & 0xF07BE535;
	uint32 c10 = cuckooHash & 0x2B07D70A;
	uint32 c11 = cuckooHash & 0xABFB9D52;
	uint32 c20 = cuckooHash & 0x9D52B07D;
	uint32 c21 = cuckooHash & 0x8F5C28F6;

	// zero all elements in the tables/values
	if(threadIdx.x < 192)
	{
		sharedKeys[threadIdx.x + 192 * 0] = UINT_MAX;
		sharedKeys[threadIdx.x + 192 * 1] = UINT_MAX;
		sharedKeys[threadIdx.x + 192 * 2] = UINT_MAX;
	}

	// the first thread read the size of the bucket and broadcast to all other threads
	if(threadIdx.x == 0)
	{
		size = bucketSizes[blockIdx.x];
	}

	__syncthreads();

	// read the pairs from global memory (note that invalid pairs are x=UINT_MAX)
	uint2 pair = make_uint2(UINT_MAX, UINT_MAX);
	if(threadIdx.x < size)
	{
		pair = pairsInBucket[threadId];
	}

	bool valid = (pair.x == UINT_MAX) ? false : true;

#if _ENABLE_FAST_REDUCTION
	positioned = (pair.x == UINT_MAX) ? 1 : 0;
#else
	positioned[threadIdx.x] = (pair.x == UINT_MAX) ? 1 : 0;
#endif

#if _ENABLE_DEBUG
	positionedState[threadIdx.x] = (pair.x == UINT_MAX) ? 0 : UINT_MAX;
#endif

	int i;
	for(i=0;i<25;++i)
	{
		uint32 h;
		uint32 check;

		// first table
		{
#if _ENABLE_FAST_REDUCTION
			if(valid && positioned == 0)
#else
			if(valid && positioned[threadIdx.x] == 0)
#endif
			{
				h = ((c00 + c01 * pair.x) % 1900813) % 192;
				sharedKeys[(192*0)+h] = pair.x;
			}

			__syncthreads();
#if _ENABLE_FAST_REDUCTION
			if(valid && (positioned == 1 || positioned == 0))
#else
			if(valid && (positioned[threadIdx.x] == 1 || positioned[threadIdx.x] == 0))
#endif
			{
				check = sharedKeys[(192*0)+h];

				if(check == pair.x)
				{
#if _ENABLE_FAST_REDUCTION
					positioned = 1;
#else
					positioned[threadIdx.x] = 1;
#endif
#if _ENABLE_DEBUG
					positionedState[threadIdx.x] = (192*0)+h;
#endif
					sharedValues[(192*0)+h] = pair.y;
				}
				else
				{
#if _ENABLE_FAST_REDUCTION
					positioned = 0;
#else
					positioned[threadIdx.x] = 0;
#endif
#if _ENABLE_DEBUG
					positionedState[threadIdx.x] = UINT_MAX;
#endif
				}
			}
		}

#if _ENABLE_FAST_REDUCTION
		__syncthreads();

		if(__all(positioned) > 0)
		{
			if(threadIdx.x % WARP_SIZE == 0)
				done[threadIdx.x / WARP_SIZE] = 1;
		}
		else
		{
			if(threadIdx.x % WARP_SIZE == 0)
				done[threadIdx.x / WARP_SIZE] = 0;
		}

		__syncthreads();

		if(threadIdx.x == 0)
		{
			flag = 1;
			for(int i=0;i<512/WARP_SIZE;++i)
			{
				if(done[i] == 0)
				{
					flag = 0; break;
				}
			}
		}

		__syncthreads();

		if(flag == 1)
			break;
#else
		__syncthreads();

		if(threadIdx.x == 0)
		{
			flag = 1;
			for(int i=0;i<512;++i)
			{
				if(positioned[i] == 0)
				{
					flag = 0; break;
				}
			}
		}

		__syncthreads();

		if(flag == 1)
			break;
#endif

		// second table
		{
#if _ENABLE_FAST_REDUCTION
			if(valid && positioned == 0)
#else
			if(valid && positioned[threadIdx.x] == 0)
#endif
			{
				h = ((c10 + c11 * pair.x) % 1900813) % 192;
				sharedKeys[(192*1)+h] = pair.x;
			}

			__syncthreads();

#if _ENABLE_FAST_REDUCTION
			if(valid && (positioned == 2 || positioned == 0))
#else
			if(valid && (positioned[threadIdx.x] == 2 || positioned[threadIdx.x] == 0))
#endif
			{
				check = sharedKeys[(192*1)+h];

				if(check == pair.x)
				{
#if _ENABLE_FAST_REDUCTION
					positioned = 2;
#else
					positioned[threadIdx.x] = 2;
#endif

#if _ENABLE_DEBUG
					positionedState[threadIdx.x] = (192*1)+h;
#endif
					sharedValues[(192*1)+h] = pair.y;
				}
				else
				{
#if _ENABLE_FAST_REDUCTION
					positioned = 0;
#else
					positioned[threadIdx.x] = 0;
#endif
#if _ENABLE_DEBUG
					positionedState[threadIdx.x] = UINT_MAX;
#endif
				}
			}
		}

#if _ENABLE_FAST_REDUCTION
		__syncthreads();

		if(__all(positioned) > 0)
		{
			if(threadIdx.x % WARP_SIZE == 0)
				done[threadIdx.x / WARP_SIZE] = 1;
		}
		else
		{
			if(threadIdx.x % WARP_SIZE == 0)
				done[threadIdx.x / WARP_SIZE] = 0;
		}

		__syncthreads();

		if(threadIdx.x == 0)
		{
			flag = 1;
			for(int i=0;i<512/WARP_SIZE;++i)
			{
				if(done[i] == 0)
				{
					flag = 0; break;
				}
			}
		}

		__syncthreads();

		if(flag == 1)
			break;
#else
		__syncthreads();

		if(threadIdx.x == 0)
		{
			flag = 1;
			for(int i=0;i<512;++i)
			{
				if(positioned[i] == 0)
				{
					flag = 0; break;
				}
			}
		}

		__syncthreads();

		if(flag == 1)
			break;
#endif

		// third table
		{
#if _ENABLE_FAST_REDUCTION
			if(valid && positioned == 0)
#else
			if(valid && positioned[threadIdx.x] == 0)
#endif
			{
				h = ((c20 + c21 * pair.x) % 1900813) % 192;
				sharedKeys[(192*2)+h] = pair.x;
			}

			__syncthreads();

#if _ENABLE_FAST_REDUCTION
			if(valid && (positioned == 3 || positioned == 0))
#else
			if(valid && (positioned[threadIdx.x] == 3 || positioned[threadIdx.x] == 0))
#endif
			{
				check = sharedKeys[(192*2)+h];

				if(check == pair.x)
				{
#if _ENABLE_FAST_REDUCTION
					positioned = 3;
#else
					positioned[threadIdx.x] = 3;
#endif
#if _ENABLE_DEBUG
					positionedState[threadIdx.x] = (192*2)+h;
#endif
					sharedValues[(192*2)+h] = pair.y;
				}
				else
				{
#if _ENABLE_FAST_REDUCTION
					positioned = 0;
#else
					positioned[threadIdx.x] = 0;
#endif
#if _ENABLE_DEBUG
					positionedState[threadIdx.x] = UINT_MAX;
#endif
				}
			}
		}

#if _ENABLE_FAST_REDUCTION
		__syncthreads();

		if(__all(positioned) > 0)
		{
			if(threadIdx.x % WARP_SIZE == 0)
				done[threadIdx.x / WARP_SIZE] = 1;
		}
		else
		{
			if(threadIdx.x % WARP_SIZE == 0)
				done[threadIdx.x / WARP_SIZE] = 0;
		}

		__syncthreads();

		if(threadIdx.x == 0)
		{
			flag = 1;
			for(int i=0;i<512/WARP_SIZE;++i)
			{
				if(done[i] == 0)
				{
					flag = 0; break;
				}
			}
		}

		__syncthreads();

		if(flag == 1)
			break;
#else
		__syncthreads();

		if(threadIdx.x == 0)
		{
			flag = 1;
			for(int i=0;i<512;++i)
			{
				if(positioned[i] == 0)
				{
					flag = 0; break;
				}
			}
		}

		__syncthreads();

		if(flag == 1)
			break;
#endif
	}

	__syncthreads();

	if(i >= 25 || flag == 0)
	{
		if(threadIdx.x == 0)
		{
			buildErrors[blockIdx.x] = 1;
		}
		return;
	}

	// write the result out
	if(threadIdx.x < 192)
	{
		uint2 value;
		value = make_uint2(sharedKeys[threadIdx.x + 192 * 0], sharedValues[threadIdx.x + 192 * 0]);
		hashTables[blockIdx.x * 576 + 192 * 0 + threadIdx.x] = value;

		value = make_uint2(sharedKeys[threadIdx.x + 192 * 1], sharedValues[threadIdx.x + 192 * 1]);
		hashTables[blockIdx.x * 576 + 192 * 1 + threadIdx.x] = value;

		value = make_uint2(sharedKeys[threadIdx.x + 192 * 2], sharedValues[threadIdx.x + 192 * 2]);
		hashTables[blockIdx.x * 576 + 192 * 2 + threadIdx.x] = value;
	}
}

bool HashMapKernel_BuildCuckooHashingInBucket(uint32 bucketCount, uint32* bucketSizes, uint2* pairsInBucket, uint32 cuckooHash, uint2* hashTables, uint32* buildErrors)
{
	uint threads = 512;
	uint blocks  = bucketCount;

	HashMapKernel_BuildCuckooHashingInBucket_Impl<<<blocks, threads>>>(
			bucketCount, bucketSizes, pairsInBucket, cuckooHash, hashTables, buildErrors);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}


__global__ void HashMapKernel_FindInParallel_Impl(HASHMAPAPI_DECLARE_PARAMETERS, uint32 keyCount, uint32* keys, uint32* valueFound, uint32* positionFound)
{
	const uint threadId = blockIdx.x * blockDim.x + threadIdx.x;

	if(threadId < keyCount)
	{
		uint k = keys[threadId];

		HASHMAPAPI_KERNEL_INIT;

		uint32 value; uint32 position;
		HASHMAPAPI_FIND(k, value, position);

		valueFound[threadId] = value;
		positionFound[threadId] = position;
	}
}

bool HashMapKernel_FindInParallel(HASHMAPAPI_DECLARE_PARAMETERS, uint32 keyCount, uint32* keys, uint32* valueFound, uint32* positionFound)
{
	HASHMAPAPI_BEFORE_KERNEL_INIT;

	uint threads = 256;
	uint blocks  = keyCount / threads + ((keyCount % threads == 0) ? 0 : 1);

	HashMapKernel_FindInParallel_Impl<<<blocks, threads>>>(
			HASHMAPAPI_FORWARD_PARAMETERS, keyCount, keys, valueFound, positionFound);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

} } } } }
