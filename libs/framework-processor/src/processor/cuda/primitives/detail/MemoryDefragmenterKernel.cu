/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/processor/cuda/primitives/detail/MemoryDefragmenterKernel.h"
#include <iostream>
#include <math.h>

//#define DEBUG
#define MAX_THREADS_PER_BLOCK   512 // must hard-code, limited by arch (threads per block   <= 512 ?? )
#define MAX_JOBS_PER_BLOCK_EDGE 16  // must hard-code, limited by arch (block shared memory <= 16k ?? )
#define MIN_JOBS_PER_THREAD     16
#define MIN(a, b)               (((a)<(b))?(a):(b))
#define HI_32(_64)              (static_cast<uint32>((_64) >> 32))
#define LO_32(_64)              (static_cast<uint32>((_64) & 0xffffffff))

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail {

//=============================================================================
// Malloc Benchmark
//=============================================================================

__global__ void resetArrayValue(
    /*OUT*/ uint32* Array,
            uint32  Index,
            uint32  Count
	)
{
	if(Index<Count)
		return;
}

__global__ void addArrayValue(
    /*OUT*/ uint32* Array,
            uint32  Index,
            uint32  Value,
            uint32  Count
	)
{
	if(Index<Count)
		return;
	Array[Index] += Value;
}

//=============================================================================
// Defrag Kernel 0: Naive Implementation
//=============================================================================

// [Kernel Purpose]
// - defrag heap
// - update indirection table

// [Kernel Summary]
// - coalesced memory access ?                 ... no
// - input iterator dimension ?                ... 1-d
// - use threadIdx/blockIdx instead of loops ? ... no
// - bank conflict free ?                      ... n/a (no shared memory)

// [Memory Map Function]
//
// SegmentHeap:      SegmentStrip:
// +---+---+---+     +---+---+---+---+---+---+---+---+---+
// | 1 | 2 | 3 | --> | 1 | 4 | 7 | 2 | 5 | 8 | 3 | 6 | 9 |
// +---+---+---+     +---+---+---+---+---+---+---+---+---+
// | 4 | 5 | 6 |
// +---+---+---+
// | 7 | 8 | 9 |
// +---+---+---+
//
// SegmentStrip:                             IndirectionTable:
// +---+---+---+---+---+---+---+---+---+     +---+---+---+
// | 1 | 4 | 7 | 2 | 5 | 8 | 3 | 6 | 9 | --> | 1 | 2 | 3 |
// +---+---+---+---+---+---+---+---+---+     +---+---+---+

__global__ void MemoryDefragmenterKernel_Compact_Naive_Impl(
    /*IN*/    uint32* SegmentSize,
    /*IN*/    uint32* SegmentStartIndexInHeap, // prefix sum
    /*IN*/    uint64* SegmentHeap,
    /*INOUT*/ uint64* IndirectionTable,
    /*OUT*/   uint64* SegmentStrip,
              uint32  SegmentCount
    )
{
    // [Copy Order]
    //
    // SegmentHeap:      SegmentStrip:
    // +---+---+---+     +---+---+---+---+---+---+---+---+---+
    // | 1 | 1 | 1 | --> | 1 | 2 | 3 | 1 | 2 | 3 | 1 | 2 | 3 |
    // +---+---+---+     +---+---+---+---+---+---+---+---+---+
    // | 2 | 2 | 2 |
    // +---+---+---+
    // | 3 | 3 | 3 |
    // +---+---+---+
    //
    // SegmentStrip:                             IndirectionTable:
    // +---+---+---+---+---+---+---+---+---+     +---+---+---+
    // | 1 | 2 | 3 | 1 | 2 | 3 | 1 | 2 | 3 | --> | 1 | 1 | 1 |
    // +---+---+---+---+---+---+---+---+---+     +---+---+---+

    uint32 SegmentIndex = blockIdx.x*blockDim.x+threadIdx.x;
    if(SegmentIndex >= SegmentCount)
        return;
    uint32 WriteIndexBase = SegmentStartIndexInHeap[SegmentIndex];
    for(uint32 i = 0; i<SegmentSize[SegmentIndex]; i++)
    {
        // ReadIndex = MajorIndex*Stride+MinorIndex
        //
        // MajorIndex: [0,SegmentSize-1]
        // Stride:     SegmentCount
        // MinorIndex: [0,SegmentCount-1]

        // SegmentHeap --> SegmentStrip
        //=========================================================
        SegmentStrip[WriteIndexBase+i] =
            SegmentHeap[i*SegmentCount+SegmentIndex];
        //=========================================================
    }
    uint32 i = 0;
    while(i<SegmentSize[SegmentIndex])
    {
        uint32 ReadIndex = WriteIndexBase+i;
        uint64 &BlockHeader = SegmentStrip[ReadIndex];
        // point to 1st body chunk
        IndirectionTable[HI_32(BlockHeader)] =
            reinterpret_cast<uint64>(&SegmentStrip[ReadIndex+1]);
        i += 1+LO_32(BlockHeader); // skip header and body
    }
}

//=============================================================================
// Defrag Kernel 1: Shared Memory + Multiple Jobs Per Thread
//=============================================================================

// [Kernel Purpose]
// - defrag heap

// [Kernel Summary]
// - coalesced memory access ?                 ... no
// - input iterator dimension ?                ... 2-d
// - use threadIdx/blockIdx instead of loops ? ... partial
// - bank conflict free ?                      ... no

// [Memory Map Function]
//
// SegmentHeap:      SegmentStrip:
// +---+---+---+     +---+---+---+---+---+---+---+---+---+
// | 1 | 2 | 3 | --> | 1 | 4 | 7 | 2 | 5 | 8 | 3 | 6 | 9 |
// +---+---+---+     +---+---+---+---+---+---+---+---+---+
// | 4 | 5 | 6 |
// +---+---+---+
// | 7 | 8 | 9 |
// +---+---+---+

template<uint32 JOBS_PER_BLOCK_EDGE>
__global__ void MemoryDefragmenterKernel_Compact_1_Impl(
            uint32  JobsPerThreadEdge,
    /*IN*/  uint32* SegmentSize,
    /*IN*/  uint32* SegmentStartIndexInHeap, // prefix sum
    /*IN*/  uint64* SegmentHeap,
    /*OUT*/ uint64* SegmentStrip,
            uint32  SegmentCount
    )
{
    // [Read Order]
    //
    // SegmentHeap:          SharedMemory:
    // +-------+-------+     +-------+
    // | 1   2 | 1   2 |     | 1   2 |
    // |       |       | --> |       |
    // | 3   4 | 3   4 |     | 3   4 |
    // +-------+-------+     +-------+
    // | 1   2 | 1   2 |
    // |       |       |
    // | 3   4 | 3   4 |
    // +-------+-------+

    // [Write Order]
    //
    // SharedMemory:         SegmentStrip:
    // +---+---+             +---+---+---+---+---+---+---+---+-
    // | 1   2 | ----------> | 1 | 3 |   |   | 2 | 4 |   |   |  ..
    // |       |             +---+---+---+---+---+---+---+---+-
    // | 3   4 |
    // +---+---+

    uint32 X1 = blockIdx.x*(blockDim.x*JobsPerThreadEdge)+(threadIdx.x*JobsPerThreadEdge);
    uint32 Y1 = blockIdx.y*(blockDim.y*JobsPerThreadEdge)+(threadIdx.y*JobsPerThreadEdge);
    uint32 X2 = X1+JobsPerThreadEdge;
    uint32 Y2 = Y1+JobsPerThreadEdge;
    if(X2>SegmentCount)
        X2 = SegmentCount;
    uint32 ThreadOrigin_X = threadIdx.x*JobsPerThreadEdge;
    uint32 ThreadOrigin_Y = threadIdx.y*JobsPerThreadEdge;
    ////=========================================================
    __shared__ uint32 SharedSegmentStartIndexInHeap[JOBS_PER_BLOCK_EDGE];
    __shared__ uint32 SharedSegmentSize[JOBS_PER_BLOCK_EDGE];
    if(ThreadOrigin_Y == 0)
        for(uint32 SegmentIndex = X1; SegmentIndex<X2; SegmentIndex++) // column
        {
            uint32 LocalIndex_X = ThreadOrigin_X+(SegmentIndex-X1);
            SharedSegmentStartIndexInHeap[LocalIndex_X] = SegmentStartIndexInHeap[SegmentIndex];
            SharedSegmentSize[LocalIndex_X] = SegmentSize[SegmentIndex];
        }
    __syncthreads();
    ////=========================================================
    __shared__ uint64 SharedSegmentMtx[JOBS_PER_BLOCK_EDGE][JOBS_PER_BLOCK_EDGE];
    for(uint32 SegmentCellIndex = Y1; SegmentCellIndex<Y2; SegmentCellIndex++) // row
    {
        uint32 LocalIndex_Y = ThreadOrigin_Y+(SegmentCellIndex-Y1);
        for(uint32 SegmentIndex = X1; SegmentIndex<X2; SegmentIndex++) // column
            if(SegmentCellIndex<SharedSegmentSize[LocalIndex_Y])
            {
                uint32 LocalIndex_X = ThreadOrigin_X+(SegmentIndex-X1);
                // SegmentHeap --> SharedMemory
                //=========================================================
                SharedSegmentMtx[LocalIndex_X][LocalIndex_Y] = SegmentHeap[
                    SegmentCellIndex*SegmentCount+SegmentIndex
                    ];
                //=========================================================
            }
    }
    __syncthreads();
    for(uint32 SegmentCellIndex = Y1; SegmentCellIndex<Y2; SegmentCellIndex++) // row
    {
        uint32 LocalIndex_Y = ThreadOrigin_Y+(SegmentCellIndex-Y1);
        for(uint32 SegmentIndex = X1; SegmentIndex<X2; SegmentIndex++) // column
            if(SegmentCellIndex<SharedSegmentSize[LocalIndex_Y])
            {
                uint32 LocalIndex_X = ThreadOrigin_X+(SegmentIndex-X1);
                // SharedMemory --> SegmentStrip
                //=========================================================
                SegmentStrip[
                    SharedSegmentStartIndexInHeap[LocalIndex_X]+SegmentCellIndex
                    ] = SharedSegmentMtx[LocalIndex_X][LocalIndex_Y];
                //=========================================================
            }
    }
}

//=============================================================================
// Defrag Kernel 2: Shared Memory + One Job Per Thread
//=============================================================================

// [Kernel Purpose]
// - defrag heap

// [Kernel Summary]
// - coalesced memory access ?                 ... yes
// - input iterator dimension ?                ... 2-d
// - use threadIdx/blockIdx instead of loops ? ... yes
// - bank conflict free ?                      ... yes

// [Memory Map Function]
//
// SegmentHeap:      SegmentStrip:
// +---+---+---+     +---+---+---+---+---+---+---+---+---+
// | 1 | 2 | 3 | --> | 1 | 4 | 7 | 2 | 5 | 8 | 3 | 6 | 9 |
// +---+---+---+     +---+---+---+---+---+---+---+---+---+
// | 4 | 5 | 6 |
// +---+---+---+
// | 7 | 8 | 9 |
// +---+---+---+

template<uint32 JOBS_PER_BLOCK_EDGE, uint32 PADDING>
__global__ void MemoryDefragmenterKernel_Compact_2_Impl(
    /*IN*/  uint32* SegmentSize,
    /*IN*/  uint32* SegmentStartIndexInHeap, // prefix sum
    /*IN*/  uint64* SegmentHeap,
    /*OUT*/ uint64* SegmentStrip,
            uint32  SegmentCount,
            uint32  MaxSegmentSize
    )
{
    // [Read Order]
    //
    // SegmentHeap:          SharedMemory:
    // +-------+-------+     +-------+
    // | 1   1 | 1   1 |     | 1   1 |
    // |       |       | --> |       |
    // | 1   1 | 1   1 |     | 1   1 |
    // +-------+-------+     +-------+
    // | 1   1 | 1   1 |
    // |       |       |
    // | 1   1 | 1   1 |
    // +-------+-------+

    // [Write Order]
    //
    // SharedMemory:         SegmentStrip:
    // +---+---+             +---+---+---+---+---+---+---+---+-
    // | 1   1 | ----------> | 1 | 1 |   |   | 1 | 1 |   |   |  ..
    // |       |             +---+---+---+---+---+---+---+---+-
    // | 1   1 |
    // +---+---+

    uint32 SegmentIndex     = blockIdx.x*blockDim.x+threadIdx.x; // column
    uint32 SegmentCellIndex = blockIdx.y*blockDim.y+threadIdx.y; // row
    ////=========================================================
    __shared__ uint32 SharedSegmentStartIndexInHeap[JOBS_PER_BLOCK_EDGE];
    __shared__ uint32 SharedSegmentSize[JOBS_PER_BLOCK_EDGE];
    bool IsValidSegmentIndex = SegmentIndex<SegmentCount;
    if(IsValidSegmentIndex && threadIdx.y == 0)
    {
        SharedSegmentStartIndexInHeap[threadIdx.x] = SegmentStartIndexInHeap[SegmentIndex];
        SharedSegmentSize[threadIdx.x] = SegmentSize[SegmentIndex];
    }
    __syncthreads();
    ////=========================================================
    __shared__ uint64 SharedSegmentMtx[JOBS_PER_BLOCK_EDGE][JOBS_PER_BLOCK_EDGE+PADDING];
    bool IsInsideSegment = IsValidSegmentIndex &&
        SegmentCellIndex<SharedSegmentSize[threadIdx.x];
    if(IsInsideSegment)
    {
        // SegmentHeap --> SharedMemory
        //=========================================================
        SharedSegmentMtx[threadIdx.x][threadIdx.y] = SegmentHeap[
            SegmentCellIndex*SegmentCount+SegmentIndex
            ];
        //=========================================================
    }
    __syncthreads();
    if(IsInsideSegment)
    {
        // SharedMemory --> SegmentStrip
        //=========================================================
        SegmentStrip[
            SharedSegmentStartIndexInHeap[threadIdx.x]+SegmentCellIndex
            ] = SharedSegmentMtx[threadIdx.x][threadIdx.y];
        //=========================================================
    }
}

//=============================================================================
// Defrag Kernel 3: Shared Memory + One Job Per Thread + Diagonal Block
//                  Reordering
//=============================================================================

// [Kernel Purpose]
// - defrag heap

// [Kernel Summary]
// - coalesced memory access ?                 ... yes
// - input iterator dimension ?                ... 2-d
// - use threadIdx/blockIdx instead of loops ? ... yes
// - bank conflict free ?                      ... yes

// [Memory Map Function]
//
// SegmentHeap:      SegmentStrip:
// +---+---+---+     +---+---+---+---+---+---+---+---+---+
// | 1 | 2 | 3 | --> | 1 | 4 | 7 | 2 | 5 | 8 | 3 | 6 | 9 |
// +---+---+---+     +---+---+---+---+---+---+---+---+---+
// | 4 | 5 | 6 |
// +---+---+---+
// | 7 | 8 | 9 |
// +---+---+---+

template<uint32 JOBS_PER_BLOCK_EDGE>
__global__ void MemoryDefragmenterKernel_Compact_3_Impl(
    /*IN*/  uint32* SegmentSize,
    /*IN*/  uint32* SegmentStartIndexInHeap, // prefix sum
    /*IN*/  uint64* SegmentHeap,
    /*OUT*/ uint64* SegmentStrip,
            uint32  SegmentCount,
            uint32  MaxSegmentSize
    )
{
    // [Read Order]
    //
    // SegmentHeap:          SharedMemory:
    // +-------+-------+     +-------+
    // | 1   1 | 1   1 |     | 1   1 |
    // |       |       | --> |       |
    // | 1   1 | 1   1 |     | 1   1 |
    // +-------+-------+     +-------+
    // | 1   1 | 1   1 |
    // |       |       |
    // | 1   1 | 1   1 |
    // +-------+-------+

    // [Write Order]
    //
    // SharedMemory:         SegmentStrip:
    // +---+---+             +---+---+---+---+---+---+---+---+-
    // | 1   1 | ----------> | 1 | 1 |   |   | 1 | 1 |   |   |  ..
    // |       |             +---+---+---+---+---+---+---+---+-
    // | 1   1 |
    // +---+---+

    // diagonal reordering
    uint32 blockIdx_x, blockIdx_y;
    if(SegmentCount == MaxSegmentSize)
    {
        blockIdx_y = blockIdx.x;
        blockIdx_x = (blockIdx.x+blockIdx.y)%gridDim.x;
    }
    else
    {
        uint32 bid = blockIdx.x+gridDim.x*blockIdx.y;
        blockIdx_y = bid%gridDim.y;
        blockIdx_x = ((bid/gridDim.y)+blockIdx_y)%gridDim.x;
    }

    uint32 SegmentIndex     = blockIdx_x*blockDim.x+threadIdx.x; // column
    uint32 SegmentCellIndex = blockIdx_y*blockDim.y+threadIdx.y; // row
    ////=========================================================
    __shared__ uint32 SharedSegmentStartIndexInHeap[JOBS_PER_BLOCK_EDGE];
    __shared__ uint32 SharedSegmentSize[JOBS_PER_BLOCK_EDGE];
    bool IsValidSegmentIndex = SegmentIndex<SegmentCount;
    if(IsValidSegmentIndex && threadIdx.y == 0)
    {
        SharedSegmentStartIndexInHeap[threadIdx.x] = SegmentStartIndexInHeap[SegmentIndex];
        SharedSegmentSize[threadIdx.x] = SegmentSize[SegmentIndex];
    }
    __syncthreads();
    ////=========================================================
    __shared__ uint64 SharedSegmentMtx[JOBS_PER_BLOCK_EDGE][JOBS_PER_BLOCK_EDGE];
    bool IsInsideSegment = IsValidSegmentIndex &&
        SegmentCellIndex<SharedSegmentSize[threadIdx.x];
    if(IsInsideSegment)
    {
        // SegmentHeap --> SharedMemory
        //=========================================================
        SharedSegmentMtx[threadIdx.x][threadIdx.y] = SegmentHeap[
            SegmentCellIndex*SegmentCount+SegmentIndex
            ];
        //=========================================================
    }
    __syncthreads();
    if(IsInsideSegment)
    {
        // SharedMemory --> SegmentStrip
        //=========================================================
        SegmentStrip[
            SharedSegmentStartIndexInHeap[threadIdx.x]+SegmentCellIndex
            ] = SharedSegmentMtx[threadIdx.x][threadIdx.y];
        //=========================================================
    }
}

//=============================================================================
// UpdateIndirectionTable Kernel: Shared Memory + Multiple Jobs Per Thread
//=============================================================================

// [Kernel Purpose]
// - update indirection table

// [Kernel Summary]
// - coalesced memory access ?                 ... no
// - input iterator dimension ?                ... 1-d
// - use threadIdx/blockIdx instead of loops ? ... partial
// - bank conflict free ?                      ... no

// [Memory Map Function]
//
// SegmentStrip:                             IndirectionTable:
// +---+---+---+---+---+---+---+---+---+     +---+---+---+
// | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | --> | 1 | 4 | 7 |
// +---+---+---+---+---+---+---+---+---+     +---+---+---+

__global__ void MemoryDefragmenterKernel_UpdateIndirectionTable_Impl(
    /*IN*/    uint32* SegmentSize,
    /*IN*/    uint32* SegmentStartIndexInHeap, // prefix sum
    /*INOUT*/ uint64* IndirectionTable,
    /*OUT*/   uint64* SegmentStrip,
              uint32  SegmentCount
    )
{
    // [Copy Order]
    //
    // SegmentStrip:                             IndirectionTable:
    // +---+---+---+---+---+---+---+---+---+     +---+---+---+
    // | 1 | 2 | 3 | 1 | 2 | 3 | 1 | 2 | 3 | --> | 1 | 1 | 1 |
    // +---+---+---+---+---+---+---+---+---+     +---+---+---+

    uint32 SegmentIndex = blockIdx.x*blockDim.x+threadIdx.x;
    if(SegmentIndex >= SegmentCount)
        return;
    uint32 i = 0;
    while(i<SegmentSize[SegmentIndex]) // SegmentStrip --> IndirectionTable
    {
        uint32 ReadIndex = SegmentStartIndexInHeap[SegmentIndex]+i;
        uint64 &BlockHeader = SegmentStrip[ReadIndex];
        // point to 1st body chunk
        IndirectionTable[HI_32(BlockHeader)] =
            reinterpret_cast<uint64>(&SegmentStrip[ReadIndex+1]);
        i += 1+LO_32(BlockHeader); // skip header and body
    }
}

//=================
// Kernel Wrapper
//=================

void MemoryDefragmenterKernel_Defrag(
              uint32  DefragMode,
    /*IN*/    uint32* SegmentSize,
    /*IN*/    uint32* SegmentStartIndexInHeap,
    /*IN*/    uint64* SegmentHeap,
    /*INOUT*/ uint64* IndirectionTable,
    /*OUT*/   uint64* SegmentStrip,
              uint32  SegmentCount,
              uint32  CompactedSize,
              uint32  MaxSegmentSize,
              uint32  HeapSize
    )
{
    #ifdef DEBUG
        std::cout << ">> Running Kernel #" << DefragMode << std::endl;
    #endif
    switch(DefragMode)
    {
    case 0:
        //=====================================
        // Call Defrag Kernel 0
        //=====================================
        {
            uint32 JobCount        = SegmentCount;
            uint32 ThreadsPerBlock = MIN(JobCount, MAX_THREADS_PER_BLOCK);
            uint32 BlocksPerGrid   = (JobCount-1)/ThreadsPerBlock+1;
            #ifdef DEBUG
                std::cout << std::string(20, '=')                   << std::endl;
                std::cout << "JobCount: "        << JobCount        << std::endl;
                std::cout << "BlocksPerGrid: "   << BlocksPerGrid   << std::endl;
                std::cout << "ThreadsPerBlock: " << ThreadsPerBlock << std::endl;
                std::cout << std::string(20, '=')                   << std::endl;
            #endif
            MemoryDefragmenterKernel_Compact_Naive_Impl
                <<<
                BlocksPerGrid,
                ThreadsPerBlock
                >>>(
                /*IN*/    SegmentSize,
                /*IN*/    SegmentStartIndexInHeap,
                /*IN*/    SegmentHeap,
                /*INOUT*/ IndirectionTable,
                /*OUT*/   SegmentStrip,
                          SegmentCount
                );
        }
        break;
    case 1:
        //=====================================
        // Call Defrag Kernel 1
        //=====================================
        {
            dim3 JobCount(
                SegmentCount,  // column
                MaxSegmentSize // row
                );
            dim3 BlocksPerGrid(
                (JobCount.x-1)/MAX_JOBS_PER_BLOCK_EDGE+1,
                (JobCount.y-1)/MAX_JOBS_PER_BLOCK_EDGE+1
                );
            uint32 MaxThreadsPerBlockEdge = (uint32) sqrt((double) MAX_THREADS_PER_BLOCK);
            // NOTE: permit no more threads (per block) than jobs available (per block)
            if(MaxThreadsPerBlockEdge>MAX_JOBS_PER_BLOCK_EDGE)
                MaxThreadsPerBlockEdge = MAX_JOBS_PER_BLOCK_EDGE;
            uint32 MaxJobsPerThreadEdge = (MAX_JOBS_PER_BLOCK_EDGE-1)/MaxThreadsPerBlockEdge+1;
            // NOTE: allow no fewer jobs than MIN_JOBS_PER_THREAD (for each thread)
            if(MaxJobsPerThreadEdge*MaxJobsPerThreadEdge<MIN_JOBS_PER_THREAD)
            {
                MaxJobsPerThreadEdge =
                    static_cast<uint32>(sqrt(
                        static_cast<double>(MIN_JOBS_PER_THREAD)
                        ));
                MaxThreadsPerBlockEdge = (MAX_JOBS_PER_BLOCK_EDGE-1)/MaxJobsPerThreadEdge+1;
            }
            dim3 ThreadsPerBlock(
                MaxThreadsPerBlockEdge,
                MaxThreadsPerBlockEdge
                );
            #ifdef DEBUG
                std::cout << std::string(20, '=')                                  << std::endl;
                std::cout << "MaxJobsPerBlockEdge: "    << MAX_JOBS_PER_BLOCK_EDGE << std::endl;
                std::cout << "JobCount.x: "             << JobCount.x              << std::endl;
                std::cout << "JobCount.y: "             << JobCount.y              << std::endl;
                std::cout << "BlocksPerGrid.x: "        << BlocksPerGrid.x         << std::endl;
                std::cout << "BlocksPerGrid.y: "        << BlocksPerGrid.y         << std::endl;
                std::cout << "MaxThreadsPerBlockEdge: " << MaxThreadsPerBlockEdge  << std::endl;
                std::cout << "MaxJobsPerThreadEdge: "   << MaxJobsPerThreadEdge    << std::endl;
                std::cout << std::string(20, '=')                                  << std::endl;
            #endif
            MemoryDefragmenterKernel_Compact_1_Impl<MAX_JOBS_PER_BLOCK_EDGE>
                <<<
                BlocksPerGrid,
                ThreadsPerBlock
                >>>(
                        MaxJobsPerThreadEdge,
                /*IN*/  SegmentSize,
                /*IN*/  SegmentStartIndexInHeap,
                /*IN*/  SegmentHeap,
                /*OUT*/ SegmentStrip,
                        SegmentCount
                );
        }
        break;
    case 2:
        //=====================================
        // Call Defrag Kernel 2
        //=====================================
        {
            dim3 JobCount(
                SegmentCount,  // column
                MaxSegmentSize // row
                );
            dim3 BlocksPerGrid(
                (JobCount.x-1)/MAX_JOBS_PER_BLOCK_EDGE+1,
                (JobCount.y-1)/MAX_JOBS_PER_BLOCK_EDGE+1
                );
            uint32 MaxThreadsPerBlockEdge = (uint32) sqrt((double) MAX_THREADS_PER_BLOCK);
            // NOTE: permit no more threads (per block) than jobs available (per block)
            if(MaxThreadsPerBlockEdge>MAX_JOBS_PER_BLOCK_EDGE)
                MaxThreadsPerBlockEdge = MAX_JOBS_PER_BLOCK_EDGE;
            dim3 ThreadsPerBlock(
                MaxThreadsPerBlockEdge,
                MaxThreadsPerBlockEdge
                );
            #ifdef DEBUG
                std::cout << std::string(20, '=')                                  << std::endl;
                std::cout << "MaxJobsPerBlockEdge: "    << MAX_JOBS_PER_BLOCK_EDGE << std::endl;
                std::cout << "JobCount.x: "             << JobCount.x              << std::endl;
                std::cout << "JobCount.y: "             << JobCount.y              << std::endl;
                std::cout << "BlocksPerGrid.x: "        << BlocksPerGrid.x         << std::endl;
                std::cout << "BlocksPerGrid.y: "        << BlocksPerGrid.y         << std::endl;
                std::cout << "MaxThreadsPerBlockEdge: " << MaxThreadsPerBlockEdge  << std::endl;
                std::cout << std::string(20, '=')                                  << std::endl;
            #endif
            MemoryDefragmenterKernel_Compact_2_Impl<MAX_JOBS_PER_BLOCK_EDGE, 0>
                <<<
                BlocksPerGrid,
                ThreadsPerBlock
                >>>(
                /*IN*/  SegmentSize,
                /*IN*/  SegmentStartIndexInHeap,
                /*IN*/  SegmentHeap,
                /*OUT*/ SegmentStrip,
                        SegmentCount,
                        MaxSegmentSize
                );
        }
        break;
    case 3:
        //=====================================
        // Call Defrag Kernel 3
        //=====================================
        {
            dim3 JobCount(
                SegmentCount,  // column
                MaxSegmentSize // row
                );
            dim3 BlocksPerGrid(
                (JobCount.x-1)/MAX_JOBS_PER_BLOCK_EDGE+1,
                (JobCount.y-1)/MAX_JOBS_PER_BLOCK_EDGE+1
                );
            uint32 MaxThreadsPerBlockEdge = (uint32) sqrt((double) MAX_THREADS_PER_BLOCK);
            // NOTE: permit no more threads (per block) than jobs available (per block)
            if(MaxThreadsPerBlockEdge>MAX_JOBS_PER_BLOCK_EDGE)
                MaxThreadsPerBlockEdge = MAX_JOBS_PER_BLOCK_EDGE;
            dim3 ThreadsPerBlock(
                MaxThreadsPerBlockEdge,
                MaxThreadsPerBlockEdge
                );
            #ifdef DEBUG
                std::cout << std::string(20, '=')                                  << std::endl;
                std::cout << "MaxJobsPerBlockEdge: "    << MAX_JOBS_PER_BLOCK_EDGE << std::endl;
                std::cout << "JobCount.x: "             << JobCount.x              << std::endl;
                std::cout << "JobCount.y: "             << JobCount.y              << std::endl;
                std::cout << "BlocksPerGrid.x: "        << BlocksPerGrid.x         << std::endl;
                std::cout << "BlocksPerGrid.y: "        << BlocksPerGrid.y         << std::endl;
                std::cout << "MaxThreadsPerBlockEdge: " << MaxThreadsPerBlockEdge  << std::endl;
                std::cout << std::string(20, '=')                                  << std::endl;
            #endif
            MemoryDefragmenterKernel_Compact_3_Impl<MAX_JOBS_PER_BLOCK_EDGE>
                <<<
                BlocksPerGrid,
                ThreadsPerBlock
                >>>(
                /*IN*/  SegmentSize,
                /*IN*/  SegmentStartIndexInHeap,
                /*IN*/  SegmentHeap,
                /*OUT*/ SegmentStrip,
                        SegmentCount,
                        MaxSegmentSize
                );
        }
        break;
    case 4:
        //=====================================
        // Call Defrag Kernel 4
        //=====================================
        {
            dim3 JobCount(
                SegmentCount,  // column
                MaxSegmentSize // row
                );
            dim3 BlocksPerGrid(
                (JobCount.x-1)/MAX_JOBS_PER_BLOCK_EDGE+1,
                (JobCount.y-1)/MAX_JOBS_PER_BLOCK_EDGE+1
                );
            uint32 MaxThreadsPerBlockEdge = (uint32) sqrt((double) MAX_THREADS_PER_BLOCK);
            // NOTE: permit no more threads (per block) than jobs available (per block)
            if(MaxThreadsPerBlockEdge>MAX_JOBS_PER_BLOCK_EDGE)
                MaxThreadsPerBlockEdge = MAX_JOBS_PER_BLOCK_EDGE;
            dim3 ThreadsPerBlock(
                MaxThreadsPerBlockEdge,
                MaxThreadsPerBlockEdge
                );
            #ifdef DEBUG
                std::cout << std::string(20, '=')                                  << std::endl;
                std::cout << "MaxJobsPerBlockEdge: "    << MAX_JOBS_PER_BLOCK_EDGE << std::endl;
                std::cout << "JobCount.x: "             << JobCount.x              << std::endl;
                std::cout << "JobCount.y: "             << JobCount.y              << std::endl;
                std::cout << "BlocksPerGrid.x: "        << BlocksPerGrid.x         << std::endl;
                std::cout << "BlocksPerGrid.y: "        << BlocksPerGrid.y         << std::endl;
                std::cout << "MaxThreadsPerBlockEdge: " << MaxThreadsPerBlockEdge  << std::endl;
                std::cout << std::string(20, '=')                                  << std::endl;
            #endif
            MemoryDefragmenterKernel_Compact_2_Impl<MAX_JOBS_PER_BLOCK_EDGE, 1>
                <<<
                BlocksPerGrid,
                ThreadsPerBlock
                >>>(
                /*IN*/  SegmentSize,
                /*IN*/  SegmentStartIndexInHeap,
                /*IN*/  SegmentHeap,
                /*OUT*/ SegmentStrip,
                        SegmentCount,
                        MaxSegmentSize
                );
        }
        break;
    default:
        break;
    }
    cudaError Error = cudaThreadSynchronize();
    if(Error != cudaSuccess)
    {
        #ifdef DEBUG
            std::cout << "CUDA Exception: " << cudaGetErrorString(Error) <<
                std::endl;
        #endif
    }
    switch(DefragMode)
    {
    case 0:
        // NOTE: updates indirection table, doesn't need to call
        //       UpdateIndirectionTable Kernel
        break;
    case 1:
    case 2:
    case 3:
    case 4:
        //=====================================
        // Call UpdateIndirectionTable Kernel
        //=====================================
        {
            uint32 JobCount        = SegmentCount;
            uint32 ThreadsPerBlock = MIN(JobCount, MAX_THREADS_PER_BLOCK);
            uint32 BlocksPerGrid   = (JobCount-1)/ThreadsPerBlock+1;
            #ifdef DEBUG
                std::cout << std::string(20, '=')                   << std::endl;
                std::cout << "JobCount: "        << JobCount        << std::endl;
                std::cout << "BlocksPerGrid: "   << BlocksPerGrid   << std::endl;
                std::cout << "ThreadsPerBlock: " << ThreadsPerBlock << std::endl;
                std::cout << std::string(20, '=')                   << std::endl;
            #endif
            MemoryDefragmenterKernel_UpdateIndirectionTable_Impl
                <<<
                BlocksPerGrid,
                ThreadsPerBlock
                >>>(
                /*IN*/    SegmentSize,
                /*IN*/    SegmentStartIndexInHeap,
                /*INOUT*/ IndirectionTable,
                /*OUT*/   SegmentStrip,
                          SegmentCount
                );
            cudaError Error = cudaThreadSynchronize();
            if(Error != cudaSuccess)
            {
                #ifdef DEBUG
                    std::cout << "CUDA Exception: " << cudaGetErrorString(Error) <<
                        std::endl;
                #endif
            }
        }
        break;
    default:
        break;
    }
}

} } } } } /*zillians::vw::processors::cuda*/
