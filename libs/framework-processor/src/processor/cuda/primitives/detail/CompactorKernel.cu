/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/processor/cuda/primitives/detail/CudaKernelCommon.h"

#define COMPACTOR_THREAD_BLOCK_SIZE		256

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail {

template<class T>
__global__ void CompactorKernel_CompactImpl(/*IN*/ uint32* flags, /*IN*/ uint32* flags_scanned, /*OUT*/ byte* dest, /*IN*/ byte* src, uint32 elements)
{
	uint32 threadId = blockIdx.x * blockDim.x + threadIdx.x;

	if(threadId < elements)
	{
		if(flags[threadId] != 0)
		{
			uint32 readIndex = threadId;
			uint32 writeIndex = flags_scanned[threadId];

			((T*)dest)[writeIndex] = ((T*)src)[readIndex];
		}
	}
}

__global__ void CompactorKernel_CompactImpl32(/*IN*/ uint32* flags, /*IN*/ uint32* flags_scanned, /*OUT*/ byte* dest, /*IN*/ byte* src, uint32 elements)
{
	uint32 threadId = blockIdx.x * blockDim.x + threadIdx.x;

	if(threadId < elements)
	{
		if(flags[threadId] != 0)
		{
			uint32 readIndex = threadId;
			uint32 writeIndex = flags_scanned[threadId];

			uint32 value = ((uint32*)src)[readIndex];
			((uint32*)dest)[writeIndex] = value;
		}
	}
}

__global__ void CompactorKernel_FindCompactIndicesImpl(/*IN*/ uint32* flags, /*IN*/ uint32* flag_scanned, /*OUT*/ uint32* indices, uint32 elements)
{
	uint32 threadId = blockIdx.x * blockDim.x + threadIdx.x;

	if(threadId < elements)
	{
		if(flags[threadId] != 0)
			indices[flag_scanned[threadId]] = threadId;
	}
}

bool CompactorKernel_Compact8(/*IN*/ uint32* flags, /*IN*/ uint32* flags_scanned, /*OUT*/ byte* dest, /*IN*/ byte* src, uint32 elements)
{
	uint32 threads = COMPACTOR_THREAD_BLOCK_SIZE;
	uint32 blocks  = elements / threads + ((elements % threads == 0) ? 0 : 1);
	uint32 shm     = 0;

	CompactorKernel_CompactImpl<uchar1> <<<blocks, threads, shm>>>(
			flags,
			flags_scanned,
			dest, src,
			elements
			);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

bool CompactorKernel_Compact16(/*IN*/ uint32* flags, /*IN*/ uint32* flags_scanned, /*OUT*/byte* dest, /*IN*/ byte* src, uint32 elements)
{
	uint32 threads = COMPACTOR_THREAD_BLOCK_SIZE;
	uint32 blocks  = elements / threads + ((elements % threads == 0) ? 0 : 1);
	uint32 shm     = 0;

	CompactorKernel_CompactImpl<ushort1> <<<blocks, threads, shm>>>(
			flags,
			flags_scanned,
			dest, src,
			elements
			);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

bool CompactorKernel_Compact32(/*IN*/ uint32* flags, /*IN*/ uint32* flags_scanned, /*OUT*/byte* dest, /*IN*/ byte* src, uint32 elements)
{
	uint32 threads = COMPACTOR_THREAD_BLOCK_SIZE;
	uint32 blocks  = elements / threads + ((elements % threads == 0) ? 0 : 1);
	uint32 shm     = 0;

	CompactorKernel_CompactImpl32 <<<blocks, threads, shm>>>(
			flags,
			flags_scanned,
			dest, src,
			elements
			);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

bool CompactorKernel_Compact64(/*IN*/ uint32* flags, /*IN*/ uint32* flags_scanned, /*OUT*/byte* dest, /*IN*/ byte* src, uint32 elements)
{
	uint32 threads = COMPACTOR_THREAD_BLOCK_SIZE;
	uint32 blocks  = elements / threads + ((elements % threads == 0) ? 0 : 1);
	uint32 shm     = 0;

	CompactorKernel_CompactImpl<ulong1> <<<blocks, threads, shm>>>(
			flags,
			flags_scanned,
			dest, src,
			elements
			);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

bool CompactorKernel_Compact128(/*IN*/ uint32* flags, /*IN*/ uint32* flags_scanned, /*OUT*/ byte* dest, /*IN*/ byte* src, uint32 elements)
{
	uint32 threads = COMPACTOR_THREAD_BLOCK_SIZE;
	uint32 blocks  = elements / threads + ((elements % threads == 0) ? 0 : 1);
	uint32 shm     = 0;

	CompactorKernel_CompactImpl<ulong2> <<<blocks, threads, shm>>>(
			flags,
			flags_scanned,
			dest, src,
			elements
			);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

bool CompactorKernel_FindCompactIndices(uint32* flags, uint32* flags_scanned, uint32* indices, uint32 elements)
{
	uint32 threads = COMPACTOR_THREAD_BLOCK_SIZE;
	uint32 blocks  = elements / threads + ((elements % threads == 0) ? 0 : 1);
	uint32 shm     = 0;

	CompactorKernel_FindCompactIndicesImpl <<<blocks, threads, shm>>>(
			flags,
			flags_scanned,
			indices,
			elements
			);

	cudaError error = cudaThreadSynchronize();

	if(error != cudaSuccess)
	{
		printf("CUDA Exception: %s", cudaGetErrorString(error));
		return false;
	}

	return true;
}

} } } } }
