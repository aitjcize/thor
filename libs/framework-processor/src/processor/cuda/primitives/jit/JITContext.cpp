/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/processor/cuda/primitives/jit/JITContext.h"
#include "framework/processor/cuda/kernel/api/DriverApi.h"
#include <string>

//#define DEBUG

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace primitives { namespace jit {

JITContext_t::JITContext_t(const DriverApi& api)
	: mApi(api),
	  mModule(NULL),
	  mFunction(NULL)
{
}
void JITContext_t::compile(
		std::string program,
		std::string entry_point,
		DriverApi::jit_target::type sm_version)
{
	DriverApi::jit_options::type options;
	options.push_back(DriverApi::jit_options::create(DriverApi::jit_option::target, sm_version));
	std::string info_log, error_log;
	mModule = mApi.execution.compile(program.c_str(), options, info_log, error_log);
	#ifdef DEBUG
		std::cout << "[Info Log]"  << info_log  << std::endl;
		std::cout << "[Error Log]" << error_log << std::endl;
	#endif
	mFunction = mModule->getFunction(entry_point.c_str());
}

} } } } } }
