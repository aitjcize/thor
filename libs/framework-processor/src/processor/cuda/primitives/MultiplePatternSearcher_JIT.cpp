/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "core/Prerequisite.h"
#include "framework/processor/cuda/primitives/detail/MultiplePatternSearcherKernel.h"
#include "framework/processor/cuda/kernel/DeviceArray.h"
#include "framework/processor/cuda/kernel/ArrayHandle.h"
#include "framework/processor/cuda/primitives/HashMap.h"
#include "framework/processor/cuda/primitives/Transposer.h"
#include "framework/processor/cuda/kernel/api/DriverApi.h"
#include "framework/processor/cuda/primitives/MultiplePatternSearcher_JIT.h"
#include "framework/processor/cuda/primitives/jit/JITContext.h"
#include <boost/unordered_map.hpp>

namespace zillians { namespace framework { namespace processor { namespace cuda {

namespace zvpcpj = zillians::framework::processor::cuda::primitives::jit;

//=====================================
// RuntimeApi Full Specialization
//=====================================

#if BLOCK_STRIPE == 4

template<>
bool MultiplePatternSearchKernel_RabinKarp_Spec<RuntimeApi, 4>(
		const zvpcpj::JITContext_t* JITContext,
		HASHMAPAPI_DECLARE_PARAMETERS,
		BLOCK_STRIPE_TO_TYPE<4>::type* textData, uint32 textLength, uint32 patternLength,
		uint32 hashComponent, uint32 hashComponentMax,
		uint32 matchedCountMax, uint32* matchedCount, uint32* matchedTextPositions, uint32* matchedPatternPosition)
{
	return MultiplePatternSearchKernel_RabinKarp(
			HASHMAPAPI_FORWARD_PARAMETERS,
			textData, textLength, patternLength,
			hashComponent, hashComponentMax,
			matchedCountMax, matchedCount, matchedTextPositions, matchedPatternPosition
			);
}

#elif BLOCK_STRIPE == 8

template<>
bool MultiplePatternSearchKernel_RabinKarp_Spec<RuntimeApi, 8>(
		const zvpcpj::JITContext_t* JITContext,
		HASHMAPAPI_DECLARE_PARAMETERS,
		BLOCK_STRIPE_TO_TYPE<8>::type* textData, uint32 textLength, uint32 patternLength,
		uint32 hashComponent, uint32 hashComponentMax,
		uint32 matchedCountMax, uint32* matchedCount, uint32* matchedTextPositions, uint32* matchedPatternPosition)
{
	return MultiplePatternSearchKernel_RabinKarp(
			HASHMAPAPI_FORWARD_PARAMETERS,
			textData, textLength, patternLength,
			hashComponent, hashComponentMax,
			matchedCountMax, matchedCount, matchedTextPositions, matchedPatternPosition
			);
}

#endif

//=====================================
// DriverApi Full Specialization
//=====================================

#if BLOCK_STRIPE == 4

template<>
bool MultiplePatternSearchKernel_RabinKarp_Spec<DriverApi, 4>(
		const zvpcpj::JITContext_t* JITContext,
		HASHMAPAPI_DECLARE_PARAMETERS,
		BLOCK_STRIPE_TO_TYPE<BLOCK_STRIPE>::type* textData, uint32 textLength, uint32 patternLength,
		uint32 hashComponent, uint32 hashComponentMax,
		uint32 matchedCountMax, uint32* matchedCount, uint32* matchedTextPositions, uint32* matchedPatternPosition)
{
	// setup calling arguments
	{
		struct
		{
			//HASHMAPAPI_DECLARE_PARAMETERS
			uint2* HashMapParam_HashMap;     // HASHMAPAPI_DECLARE_PARAMETERS.1
			uint32 HashMapParam_BucketCount; // HASHMAPAPI_DECLARE_PARAMETERS.2
			uint32 HashMapParam_BucketHash0; // HASHMAPAPI_DECLARE_PARAMETERS.3
			uint32 HashMapParam_BucketHash1; // HASHMAPAPI_DECLARE_PARAMETERS.4
			uint32 HashMapParam_CuckooHash;  // HASHMAPAPI_DECLARE_PARAMETERS.5

			// other
			BLOCK_STRIPE_TO_TYPE<BLOCK_STRIPE>::type* textData; // other.1
			uint32 textLength;              // other.2
			uint32 patternLength;           // other.3
			uint32 hashComponent;           // other.4
			uint32 hashComponentMax;        // other.5
			uint32 matchedCountMax;         // other.6
			uint32* matchedCount;           // other.7
			uint32* matchedTextPositions;   // other.8
			uint32* matchedPatternPosition; // other.9
		} args;

		//HASHMAPAPI_DECLARE_PARAMETERS
		args.HashMapParam_HashMap     = HashMapParam_HashMap;     // HASHMAPAPI_DECLARE_PARAMETERS.1
		args.HashMapParam_BucketCount = HashMapParam_BucketCount; // HASHMAPAPI_DECLARE_PARAMETERS.2
		args.HashMapParam_BucketHash0 = HashMapParam_BucketHash0; // HASHMAPAPI_DECLARE_PARAMETERS.3
		args.HashMapParam_BucketHash1 = HashMapParam_BucketHash1; // HASHMAPAPI_DECLARE_PARAMETERS.4
		args.HashMapParam_CuckooHash  = HashMapParam_CuckooHash;  // HASHMAPAPI_DECLARE_PARAMETERS.5

		// other
		args.textData               = textData;               // other.1
		args.textLength             = textLength;             // other.2
		args.patternLength          = patternLength;          // other.3
		args.hashComponent          = hashComponent;          // other.4
		args.hashComponentMax       = hashComponentMax;       // other.5
		args.matchedCountMax        = matchedCountMax;        // other.6
		args.matchedCount           = matchedCount;           // other.7
		args.matchedTextPositions   = matchedTextPositions;   // other.8
		args.matchedPatternPosition = matchedPatternPosition; // other.9

		JITContext->mFunction->setupParameterV(&args, sizeof(args), 0);
	}

	// configure grid/block, launch
	{
		HASHMAPAPI_BEFORE_KERNEL_INIT; // NOTE: does nothing
		uint32 threads = 256;
		uint32 charactersPerThread = BLOCK_PER_THREAD * BLOCK_STRIPE;
		uint32 totalThreadsToProcess = textLength / charactersPerThread + ((textLength % charactersPerThread == 0) ? 0 : 1);
		uint32 blocks = totalThreadsToProcess / threads + ((totalThreadsToProcess % threads == 0) ? 0 : 1);
		JITContext->mFunction->configure(
			dim3(blocks,  1, 1),
			dim3(threads, 1, 1),
			0);
		JITContext->mFunction->launch();
	}

	return true;
}

#elif BLOCK_STRIPE == 8

template<>
bool MultiplePatternSearchKernel_RabinKarp_Spec<DriverApi, 8>(
		const zvpcpj::JITContext_t* JITContext,
		HASHMAPAPI_DECLARE_PARAMETERS,
		BLOCK_STRIPE_TO_TYPE<BLOCK_STRIPE>::type* textData, uint32 textLength, uint32 patternLength,
		uint32 hashComponent, uint32 hashComponentMax,
		uint32 matchedCountMax, uint32* matchedCount, uint32* matchedTextPositions, uint32* matchedPatternPosition)
{
	// setup calling arguments
	{
		struct
		{
			//HASHMAPAPI_DECLARE_PARAMETERS
			uint2* HashMapParam_HashMap;     // HASHMAPAPI_DECLARE_PARAMETERS.1
			uint32 HashMapParam_BucketCount; // HASHMAPAPI_DECLARE_PARAMETERS.2
			uint32 HashMapParam_BucketHash0; // HASHMAPAPI_DECLARE_PARAMETERS.3
			uint32 HashMapParam_BucketHash1; // HASHMAPAPI_DECLARE_PARAMETERS.4
			uint32 HashMapParam_CuckooHash;  // HASHMAPAPI_DECLARE_PARAMETERS.5

			// other
			BLOCK_STRIPE_TO_TYPE<BLOCK_STRIPE>::type* textData; // other.1
			uint32 textLength;              // other.2
			uint32 patternLength;           // other.3
			uint32 hashComponent;           // other.4
			uint32 hashComponentMax;        // other.5
			uint32 matchedCountMax;         // other.6
			uint32* matchedCount;           // other.7
			uint32* matchedTextPositions;   // other.8
			uint32* matchedPatternPosition; // other.9
		} args;

		//HASHMAPAPI_DECLARE_PARAMETERS
		args.HashMapParam_HashMap     = HashMapParam_HashMap;     // HASHMAPAPI_DECLARE_PARAMETERS.1
		args.HashMapParam_BucketCount = HashMapParam_BucketCount; // HASHMAPAPI_DECLARE_PARAMETERS.2
		args.HashMapParam_BucketHash0 = HashMapParam_BucketHash0; // HASHMAPAPI_DECLARE_PARAMETERS.3
		args.HashMapParam_BucketHash1 = HashMapParam_BucketHash1; // HASHMAPAPI_DECLARE_PARAMETERS.4
		args.HashMapParam_CuckooHash  = HashMapParam_CuckooHash;  // HASHMAPAPI_DECLARE_PARAMETERS.5

		// other
		args.textData               = textData;               // other.1
		args.textLength             = textLength;             // other.2
		args.patternLength          = patternLength;          // other.3
		args.hashComponent          = hashComponent;          // other.4
		args.hashComponentMax       = hashComponentMax;       // other.5
		args.matchedCountMax        = matchedCountMax;        // other.6
		args.matchedCount           = matchedCount;           // other.7
		args.matchedTextPositions   = matchedTextPositions;   // other.8
		args.matchedPatternPosition = matchedPatternPosition; // other.9

		JITContext->mFunction->setupParameterV(&args, sizeof(args), 0);
	}

	// configure grid/block, launch
	{
		HASHMAPAPI_BEFORE_KERNEL_INIT; // NOTE: does nothing
		uint32 threads = 256;
		uint32 charactersPerThread = BLOCK_PER_THREAD * BLOCK_STRIPE;
		uint32 totalThreadsToProcess = textLength / charactersPerThread + ((textLength % charactersPerThread == 0) ? 0 : 1);
		uint32 blocks = totalThreadsToProcess / threads + ((totalThreadsToProcess % threads == 0) ? 0 : 1);
		JITContext->mFunction->configure(
			dim3(blocks,  1, 1),
			dim3(threads, 1, 1),
			0);
		JITContext->mFunction->launch();
	}

	return true;
}

#endif

} } } }
