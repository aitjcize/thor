/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#include <dlfcn.h>

#include <boost/range/adaptor/map.hpp>
#include <boost/range/adaptor/uniqued.hpp>

#include "llvm/Support/Program.h"

#include "framework/processor/KernelCUDA.h"
#include "framework/processor/KernelCUDA.h"
#include "framework/service/runtime/RuntimeServiceBuffer.h"

#include "utility/Foreach.h"
#include "utility/UnicodeUtil.h"

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/ThorConfiguration.h"
#include "language/stage/generator/detail/CppSourceGenerator.h"
#include "language/context/ManglingStageContext.h"


#define CEILING(x,y) (((x) + (y) - 1) / (y))

namespace zillians { namespace framework { namespace processor { namespace cuda {

using namespace zillians::language;
using namespace zillians::language::tree;

//////////////////////////////////////////////////////////////////////////
Kernel::Kernel(cuda::KernelApi& kernel_api, bool enable_server_function, bool enable_client_function) : KernelBase(enable_server_function, enable_client_function),
        mKernelApi(kernel_api),
        mGlobalDispatcherFunc(NULL),
        mConfigureShuffleIndicesBufferFunc(NULL),
        mConfigureNextInvocationBufferContainerFunc(NULL),
        mConfigureNextInvocationSizeBufferFunc(NULL),
        mConfigureNextInvocationIndexBufferFunc(NULL),
        mConfigureControlFlagsFunc(NULL),
        mConfigureCurrentInvocationBufferFunc(NULL),
        mConfigureNextInvocationBufferFunc(NULL),
        mDispatcherHandle(NULL),
        mLaunchMode(false)
{ }

Kernel::~Kernel()
{ }

bool Kernel::load(const boost::filesystem::path& main_ast_path,
                  const boost::filesystem::path& main_runtime_path,
                  const std::vector<boost::filesystem::path>& dep_paths)
{
    boost::filesystem::path ast_file;
    object_libraries.push_back(main_runtime_path);
    for(auto& path : dep_paths)
    {
        object_libraries.push_back(path);
    }

    if(!setKernel(main_ast_path))
    {
        return false;
    }

    if(!generateGlobalDispatcher())
    {
        return false;
    }
    return true;
}

//////////////////////////////////////////////////////////////////////////
void Kernel::initialize()
{
    KernelBase::initialize();
}

void Kernel::finalize()
{
    KernelBase::finalize();
}

void Kernel::launch(Processor* p)
{
    //BOOST_ASSER(mLaunchMode && "no launch mode");
    BOOST_ASSERT(mGlobalDispatcherFunc != nullptr && "no global dispatcher!?");

    mGlobalDispatcherFunc(mInvocationBlocks, mInvocationThreads);
    mKernelApi.execution.synchronize();
}

void* Kernel::getDispatcherHandle()
{
    return mDispatcherHandle;
}

void Kernel::configureShuffleIndicesBuffer(int32* buffer, int32 size)
{
    mConfigureShuffleIndicesBufferFunc(buffer, size);
}

void Kernel::configureNextInvocationBufferContainer(service::Invocation** container)
{
    mConfigureNextInvocationBufferContainerFunc(container);
}

void Kernel::configureNextInvocationSizeBuffer(int32* size_buffer)
{
    mConfigureNextInvocationSizeBufferFunc(size_buffer);
}

void Kernel::configureNextInvocationIndexBuffer(int32* index_buffer)
{
    mConfigureNextInvocationIndexBufferFunc(index_buffer);
}

void Kernel::configureControlFlags(service::cuda::ControlFlags* flags)
{
    mConfigureControlFlagsFunc(flags);
}

void Kernel::configureCurrentInvocationBuffer(service::Invocation* ptr, int32 size)
{
    mConfigureCurrentInvocationBufferFunc(ptr, size);
}

void Kernel::configureNextInvocationBuffer(int32 index, service::Invocation* ptr, int32 size)
{
    mConfigureNextInvocationBufferFunc(index, ptr, size);
}

void Kernel::configureLaunchMode(bool as_block_or_as_task)
{
    mLaunchMode = as_block_or_as_task;
}

void Kernel::configureLaunchShape(dim3 blocks, dim3 threads)
{
    mInvocationBlocks = blocks;
    mInvocationThreads = threads;
}

void Kernel::configureHardwareArchitecture(const char* architectures)
{
    auto translate_sm_to_compute_version = [](const std::string& sm) -> std::string {
        if(sm == "sm_10") return "compute_10";
        if(sm == "sm_12") return "compute_12";
        if(sm == "sm_13") return "compute_13";
        if(sm == "sm_20") return "compute_20";
        if(sm == "sm_21") return "compute_21";
        if(sm == "sm_22") return "compute_22";
        if(sm == "sm_23") return "compute_23";
        if(sm == "sm_30") return "compute_30";
        if(sm == "sm_35") return "compute_35";

        std::cerr << "unsupported sm = " << sm << std::endl;
        UNIMPLEMENTED_CODE();
        return "";
    };
    std::string code(architectures);
    std::string arch = translate_sm_to_compute_version(code);
    supported_architectures.push_back(std::make_pair(arch, code));
}

bool Kernel::generateGlobalDispatcher()
{
    std::wstringstream fwd;
    std::wstringstream cases;
    std::wstringstream inits_fwd;
    std::wstringstream inits_cases;

    const auto& exported_functions = mFunctionIdMap.right | boost::adaptors::map_keys | boost::adaptors::uniqued;

    std::vector<FunctionDecl*> global_init_funcs;
    ASTNodeHelper::foreachApply<FunctionDecl>(*mTangle, [&](FunctionDecl& node) {

        // skip all non-GPU entry functions
        if(node.arch.is_not_cuda())
            return;

        if(node.is_global_init)
        {
            // skip static initialization functions of non-fully specialized class templates
            if(ClassDecl* parent = cast<ClassDecl>(node.parent))
            {
                if(isa<TemplatedIdentifier>(parent->name) && !cast<TemplatedIdentifier>(parent->name)->isFullySpecialized())
                    return;
            }

            global_init_funcs.push_back(&node);
        }
    });

    if(!global_init_funcs.empty())
    {
        for(auto* func : global_init_funcs)
        {
            const auto*const mangling_ctx =  NameManglingContext::get(func);
            BOOST_ASSERT(mangling_ctx != nullptr && "no mangled name of global initialize function");
            inits_fwd << L"extern __device__ void " << s_to_ws(mangling_ctx->mangled_name )<< L"();\n";
            inits_cases << L"\t\t\t\t" << s_to_ws(mangling_ctx->mangled_name)<< L"();\n";
        }
    }

    Package* last_package = NULL;
    for(const FunctionDecl* exported : exported_functions)
    {
        Package* p = cast<Package>(exported->parent->parent);
        if(p != last_package)
        {
            if(last_package) fwd << stringify(last_package, false, true, false) << std::endl;
            fwd << stringify(p, true, false, false) << std::endl;
            last_package = p;
        }
        fwd << stringify(exported, true, false, false); // export fwd declaration
        fwd << std::endl;
    }

    if(last_package) fwd << stringify(last_package, false, true, false) << std::endl;

    fwd << L"extern \"C\" {\n";
    fwd << inits_fwd.str();
    fwd << L"}\n";

    cases << L"\t\t\tcase 9223372036854775806LL:" << std::endl;
    cases << L"\t\t\t{\n";
    cases << inits_cases.str();
    cases << L"\t\t\t\tbreak;\n";
    cases << L"\t\t\t}\n";

    zillians::language::stage::CppSourceGenerator cppSourceGenerator(true/*cuda_mode*/);

    for (const FunctionDecl* exported : exported_functions)
    {
        const auto& ids = mFunctionIdMap.right.equal_range(exported) | boost::adaptors::map_values;

        if(ids.empty())
        {
            std::cerr << "Error: failed to get late binding context on function: " << ws_to_s(exported->name->toString()) << std::endl;
            return false;
        }

        cases << L"/* " << exported->name->toString() << " */" << std::endl;
        for (const auto& id : ids)
            cases << L"\t\t\tcase " << id << L"LL :" << std::endl;
        cases << L"\t\t\t{\n";

        if(exported->parameters.size() > 0)
            cases << L"\t\t\t\tint32_t offset = 0;\n";

        size_t index = 0;
        zillians::for_each_and_pitch(
            exported->parameters,
            [this,&cases,&index,&cppSourceGenerator](VariableDecl* decl){
                std::wostringstream woss;
                cppSourceGenerator.setWrite(&woss);
                cppSourceGenerator.genNamespaceQualifiedType(true/*addStar*/, decl->type);
                //std::wstring type_str = stringify(decl->type, false);
                std::wstring type_str = woss.str();
                cases << L"\t\t\t\t" << type_str << L" param_" << index << L" = " << L"*(" << type_str << L"*)" << L"&shm[offset];\n";
            },
            [&cases,&index](){
                cases << L"\t\t\t\t" << L"offset += sizeof(param_" << index++ << L");\n";
            }
        );

        Package* p = cast<Package>(exported->parent->parent);
        cases << L"\t\t\t\t";
        if(exported->hasAnnotation(L"entry"))
        {
            cases << "int32_t r = ";
        }
        cases << stringify(p, false, false, true) << stringify(exported->name);
        cases << L"(";
        index = 0;
        zillians::for_each_and_pitch(
            exported->parameters,
            [&cases,&index](VariableDecl* decl){ cases << L"param_" << index++; },
            [&cases](){ cases << L", "; }
        );
        cases << L");\n";

        if(exported->hasAnnotation(L"entry"))
        {
            cases << "\t\t\t\tthor::lang::__setImplicitExitCode(r);\n";
        }
        cases << L"\t\t\tprintf(\"<<<<<<< %d >>>>>>>\\n\", fid);\n";
        cases << L"\t\t\t\tbreak;\n";
        cases << L"\t\t\t}\n";
    }

    std::wstringstream skeleton;
    skeleton << L"#include <cuda_runtime.h>\n"
             << L"#include <stdint.h>\n"
             << L"#include <stdio.h>\n"
             << L"\n"
             << L"#define CUDA_DISPATCHER_THREADS_PER_BLOCK	32\n"
             << L"#define CUDA_DISPATCHER_WARP_SIZE		32\n"
             << L"#define CUDA_DISPATCHER_WARP_MASK		0x3F\n"
             << L"#define CUDA_INVOCATION_PARAMETER_SIZE  	256\n"
             << L"#define CUDA_INVOCATION_PARAMETER_LOAD_SIZE   	8\n"
             << L"\n"
             << L"#if CUDA_INVOCATION_PARAMETER_SIZE % (CUDA_DISPATCHER_WARP_SIZE*CUDA_INVOCATION_PARAMETER_LOAD_SIZE) != 0\n"
             << L"    #error \"parameter size must be multiple of warp load size\"\n"
             << L"#endif\n"
             << L"\n"
             << L"#if __INVOCATION_PARAMETER_ELEMENTS*CUDA_DISPATCHER_THREADS_PER_BLOCK > 16000\n"
             << L"    #error \"required shared memory size too large\"\n"
             << L"#endif\n"
             << L"\n"
             << L"#define __INVOCATION_PARAMETER_ELEMENTS      (CUDA_INVOCATION_PARAMETER_SIZE/CUDA_INVOCATION_PARAMETER_LOAD_SIZE)\n"
             << L"\n"
             << L"typedef char int8;\n"
             << L"typedef short int16;\n"
             << L"typedef int int32;\n"
             << L"typedef long long int64;\n"
             << L"\n"
             << L"template<int Size>\n"
             << L"struct type_selector;\n"
             << L"\n"
             << L"template<>\n"
             << L"struct type_selector<1> {\n"
             << L"	typedef int8 type;\n"
             << L"};\n"
             << L"\n"
             << L"template<>\n"
             << L"struct type_selector<2> {\n"
             << L"	typedef int16 type;\n"
             << L"};\n"
             << L"\n"
             << L"template<>\n"
             << L"struct type_selector<4> {\n"
             << L"	typedef int32 type;\n"
             << L"};\n"
             << L"\n"
             << L"template<>\n"
             << L"struct type_selector<8> {\n"
             << L"	typedef int64 type;\n"
             << L"};\n"
             << L"\n"
             << L"typedef type_selector<CUDA_INVOCATION_PARAMETER_LOAD_SIZE>::type element_type;\n"
             << L"\n"
             << L"namespace zillians { namespace framework { namespace service {\n"
             << L"struct Invocation;\n"
             << L"} } }\n"
             << L"\n"
             << L"namespace thor { namespace lang {\n"
             << L"template <typename T> class Lambda0;\n"
             << L"namespace internal {\n"
             << L"//extern __device__ bool __debugging_cond;\n"
             << L"extern void __setDebugCond(bool cond);\n"
             << L"\n"
             << L"struct TrackerHeader;\n"
             << L"struct Block64;\n"
             << L"struct Block128;\n"
             << L"extern void __configureHeaderBlocks(TrackerHeader* blocks, uint32_t* bit_flags);\n"
             << L"extern void __configureBlocksB64(Block64* blocks, uint32_t* bit_flags);\n"
             << L"extern void __configureBlocksB128(Block128* blocks, uint32_t* bit_flags);\n"
             << L"extern void __configureGlobalHeap(char* heap, uint32_t heap_size, uint32_t* heap_bucket, uint32_t heap_bucket_size, uint32_t* heap_bucket_offset, unsigned long long int heap_next_offset);\n"
             << L"\n"
             << L"extern __device__ bool __isDMAWarp();\n"
             << L"extern __device__ void __runDMAWarp();\n"
             << L"extern __device__ void __initDMA();\n"
             << L"extern __device__ void __completeDMA();\n"
             << L"extern __device__ void __loadParametersToShm();\n"
             << L"\n"
             << L"extern void __setCurrentInvocationBuffer(zillians::framework::service::Invocation* ptr, int32_t size);\n"
             << L"extern void __setShuffleIndicesBuffer(int32_t* ptr, int32_t size);\n"
             << L"\n"
             << L"extern void __setNextInvocationBuffer(int32_t index, zillians::framework::service::Invocation* ptr, int32_t size);\n"
             << L"extern void __setNextInvocationBufferContainer(zillians::framework::service::Invocation** ptr);\n"
             << L"extern void __setNextInvocationSizeBuffer(int32_t* ptr);\n"
             << L"extern void __setNextInvocationIndexBuffer(int32_t* ptr);\n"
             << L"}\n"
             << L"extern __device__ int32_t __getCurrentInvocationId();\n"
             << L"extern __device__ int64_t __getCurrentFunctionId(int32_t invocation_id);\n"
             << L"extern __device__ int64_t __getCurrentSessionId(int32_t invocation_id);\n"
             << L"extern __device__ char* __getCurrentParameterPtr(int32_t invocation_id);\n"
             << L"extern __device__ void  __setImplicitExitCode(int32_t exit_code);\n"
             << L"} }\n"
             << L"\n"
             << L"//////////////////// begin of fwd.str() ////////////////////\n"
             << fwd.str()
             << L"//////////////////// end of fwd.str() ////////////////////\n"
             << L"\n"
             << L"__global__ void __cuda_global_dispatch() {\n"
             << L"\t__shared__ element_type shm[__INVOCATION_PARAMETER_ELEMENTS*CUDA_DISPATCHER_THREADS_PER_BLOCK];\n"
             << L"\tif(thor::lang::internal::__isDMAWarp()) {\n"
             << L"\t\tthor::lang::internal::__runDMAWarp();\n"
             << L"\t}\n"
             << L"\telse {\n"
             << L"\t\tthor::lang::internal::__initDMA();\n"
             << L"\t\tthor::lang::internal::__loadParametersToShm();\n"
             << L"\t\tint32_t cid = thor::lang::__getCurrentInvocationId();\n"
             << L"\t\tint32_t lid = threadIdx.x - " << buffer::cuda_runtime_svc_constants::dma_threads_per_block << ";\n"
             << L"\t\tif(cid != -1) {\n"
             << L"\t\t\tint64_t fid = thor::lang::__getCurrentFunctionId(lid);\n"
             << L"\t\t\tprintf(\"<<<<<<< %d >>>>>>>\\n\", fid);\n"
             << L"\t\t\tint64_t sid = thor::lang::__getCurrentSessionId(lid);\n"
             << L"\t\t\tchar* ptr = thor::lang::__getCurrentParameterPtr(lid);\n"
             << L"\t\t\tptr; // to avoid unused variable warnings \n"
             //<< L"\t\t\tprintf(\"cid=%d, lid=%d, fid=%ld, sid=%ld, ptr=%p\\n\", cid, lid, fid, sid, ptr);\n"
             << L"\t\t\tswitch(fid) {\n"
             << cases.str()
             << L"\t\t\t\tdefault:\n"
             << L"\t\t\t\tbreak;\n"
             << L"\t\t\t}\n"
             << L"\t\t}\n"
             << L"\t\tthor::lang::internal::__completeDMA();\n"
             << L"\t}\n"
             << L"}\n"
             << L"\n"
             << L"void __global_dispatch(dim3 blocks, dim3 threads) {\n"
             << L"\t__cuda_global_dispatch<<<blocks, threads>>>();\n"
             << L"}\n"
             << L"\n"
             << L" void __dummy_ensure_function_export_runtime_service() {\n"
             << L"\tthor::lang::internal::__setCurrentInvocationBuffer(NULL, 0);\n"
             << L"\tthor::lang::internal::__setShuffleIndicesBuffer(NULL, 0);\n"
             << L"\tthor::lang::internal::__setDebugCond(false);\n"
             << L"\n"
             << L"\tthor::lang::internal::__setNextInvocationBuffer(0, NULL, 0);\n"
             << L"\tthor::lang::internal::__setNextInvocationBufferContainer(NULL);\n"
             << L"\tthor::lang::internal::__setNextInvocationSizeBuffer(NULL);\n"
             << L"\tthor::lang::internal::__setNextInvocationIndexBuffer(NULL);\n"
             << L"}\n"
             << L"\n"
             << L" void __dummy_ensure_function_export_object_service() {\n"
             << L"\tthor::lang::internal::__configureHeaderBlocks(NULL, 0);\n"
             << L"\tthor::lang::internal::__configureBlocksB64(NULL, 0);\n"
             << L"\tthor::lang::internal::__configureBlocksB128(NULL, 0);\n"
             << L"\tthor::lang::internal::__configureGlobalHeap(NULL, 0, NULL, 0, NULL, 0);\n"
             << L"}\n"
             << L"\n";

    if(mVerboseMode)
        std::cerr << ws_to_s(skeleton.str());

    // write out the global dispatch cu file
    std::string gd_cu_file;
    {
        gd_cu_file = "/tmp/gd.cu";

        std::wofstream gd_cu_file_handle(gd_cu_file);
        if(!gd_cu_file_handle.is_open())
        {
            std::cerr << "Error: failed to open " << gd_cu_file << "for writing" << std::endl;
            return false;
        }

        gd_cu_file_handle << skeleton.rdbuf();
        gd_cu_file_handle.close();
    }

    // TODO use nvcc to compile and link against the library file and generate shared object
    std::string cpp = llvm::sys::FindProgramByName("c++");
    std::string nvcc = llvm::sys::FindProgramByName("nvcc");

    if(cpp.empty())
    {
        std::cerr << "Error: failed to find gcc, please check" << std::endl;
        return false;
    }
    if(nvcc.empty())
    {
        std::cerr << "Error: failed to find nvcc, please check your CUDA installation" << std::endl;
        return false;
    }

    std::string gen_code_flags;
    {
        std::stringstream ss;
        for(auto& arch : supported_architectures)
        {
            ss << " -gencode arch=" << arch.first << ",code=" << arch.second;
        }
        gen_code_flags = ss.str();
    }

    std::string nvcc_flags = " -Xcompiler -fPIC";

    // TODO dynamically load the generated shared object
    // step #1:
    // nvcc -arch=?? -Xcompiler -fPIC -o gd.o -dc /tmp/gd.cu
    std::string gd_object_file;
    {
        gd_object_file = "/tmp/gd.o";
        std::stringstream ss;
        ss << nvcc;
        ss << gen_code_flags << nvcc_flags;
        ss << " -o " << gd_object_file << " -dc " << gd_cu_file;
        ss << " --gpu-architecture sm_20";

        if(mVerboseMode) std::cerr << "[Kernel] invokes " << ss.str() << std::endl;
        std::string command = ss.str();
        if(std::system(command.c_str()) != 0)
        {
            std::cerr << "Error: failed to compile global dispatcher" << std::endl;
            return false;
        }
    }

    // step #2:
    // nvcc -arch=?? -dlink -o gd_linked.o gd.o {lib}
    std::string linked_gd_object_file;
    std::string all_cuda_libraries;
    {
        linked_gd_object_file = "/tmp/gd_linked.o";
        std::stringstream ss;
        ss << nvcc;
        ss << gen_code_flags << nvcc_flags;
        ss << " -o " << linked_gd_object_file << " -dlink " << gd_object_file;
        ss << " --gpu-architecture sm_20";

        std::stringstream ss_libs;
        for(auto& lib : object_libraries)
        {
            ss_libs << " " << lib.string();
        }
        all_cuda_libraries = ss_libs.str();
        ss << all_cuda_libraries;

        if(mVerboseMode) std::cerr << "[Kernel] invokes " << ss.str() << std::endl;
        std::string command = ss.str();
        if(std::system(command.c_str()) != 0)
        {
            std::cerr << "Error: failed to link global dispatcher" << std::endl;
            return false;
        }
    }

    // step #3:
    // gcc {CCFLAGS} -shared -fPIC -o libgd_cuda.so gd.o {lib} gd_linked.o
    std::string final_so_file;
    {
        final_so_file = "/tmp/libgd.so";
        std::stringstream ss;
        ss << cpp;
        std::string nvcc = llvm::sys::FindProgramByName("nvcc");
        boost::filesystem::path nvcc_path(nvcc.c_str());
        boost::filesystem::path cuda_lib_dir = nvcc_path.parent_path().parent_path() / "lib64";
        ss << " -shared -fPIC -o " << final_so_file << " -L" << cuda_lib_dir << " -lcudart";
        ss << " " << gd_object_file << " -Wl,--whole-archive " << all_cuda_libraries << " -Wl,--no-whole-archive " << linked_gd_object_file;

        if(mVerboseMode) std::cerr << "[Kernel] invokes " << ss.str() << std::endl;
        std::string command = ss.str();
        if(std::system(command.c_str()) != 0)
        {
            std::cerr << "Error: failed to generate global dispatcher shared object" << std::endl;
            return false;
        }
    }

    // TODO locate the global dispatch symbol
    mDispatcherHandle = dlopen(final_so_file.c_str(), RTLD_LAZY | RTLD_GLOBAL);
    if(!mDispatcherHandle)
    {
        std::cerr << "Error: failed to load generated global dispatcher, message = " << dlerror() << std::endl;
        std::cerr << "Error .so file name: " << final_so_file << std::endl;
        return false;
    }

    mGlobalDispatcherFunc = (decltype(mGlobalDispatcherFunc))dlsym(mDispatcherHandle, "_Z17__global_dispatch4dim3S_");
    if(!mGlobalDispatcherFunc)
    {
        std::cerr << "Error: failed to find the entry in generated global dispatcher" << std::endl;
        return false;
    }

    mConfigureShuffleIndicesBufferFunc = (decltype(mConfigureShuffleIndicesBufferFunc))dlsym(mDispatcherHandle, "_ZN4thor4lang8internal25__setShuffleIndicesBufferEPii");

    // get the next invocation buffer container configuration function ptr
    // which is dynamically linked to thor::lang::internal::__setNextInvocationBufferContainer
    mConfigureNextInvocationBufferContainerFunc = (decltype(mConfigureNextInvocationBufferContainerFunc))dlsym(mDispatcherHandle, "_ZN4thor4lang8internal34__setNextInvocationBufferContainerEPPN8zillians9framework7service10InvocationE");

    // get the next invocation size buffer configuration function ptr
    // which is dynamically linked to thor::lang::internal::__setNextInvocationSizeBuffer
    mConfigureNextInvocationSizeBufferFunc = (decltype(mConfigureNextInvocationSizeBufferFunc))dlsym(mDispatcherHandle, "_ZN4thor4lang8internal29__setNextInvocationSizeBufferEPi");

    // get the next invocation index buffer configuration function ptr
    // which is dynamically linked to thor::lang::internal::__setNextInvocationSizeBuffer
    mConfigureNextInvocationIndexBufferFunc = (decltype(mConfigureNextInvocationIndexBufferFunc))dlsym(mDispatcherHandle, "_ZN4thor4lang8internal30__setNextInvocationIndexBufferEPi");

    mConfigureControlFlagsFunc = (decltype(mConfigureControlFlagsFunc))dlsym(mDispatcherHandle, "_ZN4thor4lang8internal17__setControlFlagsEPN8zillians9framework7service4cuda12ControlFlagsE");

    // get the current invocation buffer configuration function ptr
    // which is dynamically linked to thor::lang::internal::__setInvocationBuffer
    mConfigureCurrentInvocationBufferFunc = (decltype(mConfigureCurrentInvocationBufferFunc))dlsym(mDispatcherHandle, "_ZN4thor4lang8internal28__setCurrentInvocationBufferEPN8zillians9framework7service10InvocationEi");

    // get the current invocation buffer configuration function ptr
    // which is dynamically linked to thor::lang::internal::__setNextInvocationBuffer
    mConfigureNextInvocationBufferFunc = (decltype(mConfigureNextInvocationBufferFunc))dlsym(mDispatcherHandle, "_ZN4thor4lang8internal25__setNextInvocationBufferEiPN8zillians9framework7service10InvocationEi");

    std::cerr << "mConfigureShuffleIndicesBufferFunc         " << mConfigureShuffleIndicesBufferFunc          << std::endl;
    std::cerr << "mConfigureNextInvocationBufferContainerFunc" << mConfigureNextInvocationBufferContainerFunc << std::endl;
    std::cerr << "mConfigureNextInvocationSizeBufferFunc     " << mConfigureNextInvocationSizeBufferFunc      << std::endl;
    std::cerr << "mConfigureNextInvocationIndexBufferFunc    " << mConfigureNextInvocationIndexBufferFunc     << std::endl;
    std::cerr << "mConfigureControlFlagsFunc                 " << mConfigureControlFlagsFunc                  << std::endl;
    std::cerr << "mConfigureCurrentInvocationBufferFunc      " << mConfigureCurrentInvocationBufferFunc       << std::endl;
    std::cerr << "mConfigureNextInvocationBufferFunc         " << mConfigureNextInvocationBufferFunc          << std::endl;


    if( !mConfigureShuffleIndicesBufferFunc ||
        !mConfigureNextInvocationBufferContainerFunc ||
        !mConfigureNextInvocationSizeBufferFunc ||
        !mConfigureNextInvocationIndexBufferFunc ||
        !mConfigureControlFlagsFunc ||
        !mConfigureCurrentInvocationBufferFunc ||
        !mConfigureNextInvocationBufferFunc)
    {
        std::cerr << "Error: failed to find configuration function in global dispatcher" << std::endl;
    }

    return true;
}

std::wstring Kernel::stringify(const language::tree::Identifier* id)
{
    if(const SimpleIdentifier* sid = cast<SimpleIdentifier>(id))
    {
        return sid->name;
    }
    else if(const TemplatedIdentifier* tid = cast<TemplatedIdentifier>(id))
    {
        std::wstringstream ss;
        ss << stringify(tid->id);
        ss << L"<";
        zillians::for_each_and_pitch(
            tid->templated_type_list,
            [this,&ss](TypenameDecl* decl){
                if(decl->specialized_type)
                    ss << stringify(decl->specialized_type, true);
                else
                    ss << stringify(decl->name);
            },
            [&ss](){ ss << L", "; }
        );
        ss << L">";
        return ss.str();
    }
    else if(const NestedIdentifier* nid = cast<NestedIdentifier>(id))
    {
        UNREACHABLE_CODE();
        return L"";
    }
    else
    {
        UNREACHABLE_CODE();
        return L"";
    }
}

std::wstring Kernel::stringify(const language::tree::FunctionSpecifier* specifier, bool for_type_or_var_decl)
{
    UNIMPLEMENTED_CODE();
    return L"";
}

std::wstring Kernel::stringify(const language::tree::MultiSpecifier* specifier, bool for_type_or_var_decl)
{
    UNIMPLEMENTED_CODE();
    return L"";
}

std::wstring Kernel::stringify(const language::tree::NamedSpecifier* specifier, bool for_type_or_var_decl)
{
    Type* direct_resolved = ResolvedType::get(specifier);
    Type* resolved = ASTNodeHelper::getCanonicalType(specifier);
    BOOST_ASSERT(resolved && direct_resolved && "resolved type of unspecified type specifier is NULL while generating CUDA skeleton");
    if(direct_resolved->isTypenameType())
    {
        if(for_type_or_var_decl)
            return stringify(direct_resolved->getAsTypenameDecl()->name);
        else
            return stringify(direct_resolved->getAsTypenameDecl()->name) + L"*";
    }
    else if(resolved->isRecordType())
    {
        return stringify(resolved->getAsClassDecl(), false, for_type_or_var_decl);
    }
    else if(resolved->isEnumType())
    {
        return stringify(resolved->getAsEnumDecl());
    }
    else if(resolved->isPrimitiveType())
    {
        return resolved->toString();
    }
    else
    {
        UNREACHABLE_CODE();
        return L"";
    }
}

std::wstring Kernel::stringify(const language::tree::PrimitiveSpecifier* specifier, bool for_type_or_var_decl)
{
    switch(specifier->getKind())
    {
    case PrimitiveKind::VOID_TYPE: return L"void";
    case PrimitiveKind::BOOL_TYPE: return L"bool";
    case PrimitiveKind::INT8_TYPE: return L"int8_t";
    case PrimitiveKind::INT16_TYPE: return L"int16_t";
    case PrimitiveKind::INT32_TYPE: return L"int32_t";
    case PrimitiveKind::INT64_TYPE: return L"int64_t";
    case PrimitiveKind::FLOAT32_TYPE: return L"float";
    case PrimitiveKind::FLOAT64_TYPE: return L"double";
    default:
        UNIMPLEMENTED_CODE();
        return L"";
    }
}

std::wstring Kernel::stringify(const language::tree::TypeSpecifier* specifier, bool for_type_or_var_decl)
{
    if (auto*const specifier_func = language::tree::cast<language::tree::FunctionSpecifier>(specifier))
        return stringify(specifier_func, for_type_or_var_decl);
    else if (auto*const specifier_multi = language::tree::cast<language::tree::MultiSpecifier>(specifier))
        return stringify(specifier_multi, for_type_or_var_decl);
    else if (auto*const specifier_named = language::tree::cast<language::tree::NamedSpecifier>(specifier))
        return stringify(specifier_named, for_type_or_var_decl);
    else if (auto*const specifier_primitive = language::tree::cast<language::tree::PrimitiveSpecifier>(specifier))
        return stringify(specifier_primitive, for_type_or_var_decl);
    else
        return UNREACHABLE_CODE(), L"";
}

std::wstring Kernel::stringify(const language::tree::FunctionDecl* decl, bool export_fwd, bool export_empty_impl, bool export_impl)
{
    std::wostringstream ss;
    zillians::language::stage::CppSourceGenerator cppSourceGenerator(true/*cuda_mode*/);
    cppSourceGenerator.setWrite(&ss);

    if(export_fwd)
        ss << L"extern ";

    ss << L"__device__ ";
    ss << stringify(decl->type, false) << L" ";
    ss << stringify(decl->name);

    ss << L"(";
    zillians::for_each_and_pitch(
        decl->parameters,
        [this,&ss,&cppSourceGenerator](VariableDecl* var_decl){
            if(var_decl->is_const)
                ss << L"const ";

            cppSourceGenerator.genNamespaceQualifiedType(true/*addStar*/, var_decl->type);
            ss << L" ";
            ss << stringify(var_decl->name);
        },
        [&ss](){ ss << L", "; }
    );
    ss << L")";

    if(export_fwd)
        ss << L";";

    if(export_empty_impl)
        ss << L" { }";

    if(export_impl)
    {
        UNIMPLEMENTED_CODE();
    }

    return ss.str();
}

std::wstring Kernel::stringify(const language::tree::ClassDecl* decl, bool for_full_export_or_type_specifier, bool for_type_or_var_decl)
{
    if(for_full_export_or_type_specifier) // for full class definition export
    {
        return L"";
    }
    else
    {
        if(for_type_or_var_decl) // true for type decl
        {
            return stringify(decl->name);
        }
        else // false for variable decl (append "*" at the end)
        {
            return stringify(decl->name) + L"*";
        }
    }
}

std::wstring Kernel::stringify(const language::tree::EnumDecl* decl)
{
    return stringify(decl->name);
}

std::wstring Kernel::stringify(const language::tree::Package* package, bool ns_opening, bool ns_closing, bool ns_using)
{
    if( (ns_opening && !ns_closing && !ns_using) ||
        (!ns_opening && ns_closing && !ns_using) ||
        (!ns_opening && !ns_closing && ns_using) )
    {
        std::wstring ns;
        const Package* current = package;
        while(current && !current->id->name.empty())
        {
            if(ns_opening) ns = L"namespace " + current->id->name + L" { " + ns;
            if(ns_closing) ns = L"} " + ns;
            if(ns_using)   ns = current->id->name + L"::" + ns;
            current = cast<Package>(current->parent);
        }

        return ns;
    }
    else
    {
        UNREACHABLE_CODE();
        return L"";
    }
}

} } } }
