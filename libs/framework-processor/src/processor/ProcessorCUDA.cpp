/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/processor/ProcessorCUDA.h"
#include "framework/processor/KernelCUDA.h"
#include "utility/Foreach.h"

namespace zillians { namespace framework { namespace processor {

//////////////////////////////////////////////////////////////////////////
ProcessorCUDA::ProcessorCUDA(uint32 local_id, uint32 device_id) :
		ProcessorRT(local_id),
		mDeviceId(device_id),
		mKernelApi(cuda::GetKernelApi())
{
	mImplicitPendingCompletion = 0;
	mKernelApi.device.use(device_id);
}

ProcessorCUDA::~ProcessorCUDA()
{ }

//////////////////////////////////////////////////////////////////////////
void ProcessorCUDA::initialize()
{
	BOOST_ASSERT(mKernel == nullptr && "already initialized");

	mKernel = make_shared<cuda::Kernel>(mKernelApi, mEnableServerFunction, mEnableClientFunction);

	ProcessorRT::initialize();
//	int current_device = mCudaApi.device.current();
//	if(current_device != mDeviceId)
//		mRuntimeApi.device.use(mDeviceId);
//
//	mCudaApi.device.enableMappedMemory();
//
//	Processor::initialize();
//
//	if(current_device != mDeviceId)
//		mCudaApi.device.use(mDeviceId);
}

void ProcessorCUDA::finalize()
{
	BOOST_ASSERT(mKernel != nullptr && "not yet initialized");

	ProcessorRT::finalize();

	mKernel = nullptr;
//	int current_device = mCudaApi.device.current();
//	if(current_device != mDeviceId)
//		mCudaApi.device.use(mDeviceId);
//
//	Processor::finalize();
//
//	if(current_device != mDeviceId)
//		mCudaApi.device.use(mDeviceId);
}

void ProcessorCUDA::exit(int32 exit_code)
{
    UNIMPLEMENTED_CODE();
}

int32 ProcessorCUDA::getExitCode() const
{
    //UNIMPLEMENTED_CODE();
    //return -1ll;

    // TODO actually implementation
    return 0;
}

void ProcessorCUDA::run(const shared_ptr<ExecutionMonitor>& monitor)
{
//	int current_device = mCudaApi.device.current();
//	if(current_device != mDeviceId)
//		mCudaApi.device.use(mDeviceId);

	mMonitor = monitor;
	uint32 local_id = getId();
	setMonitor(monitor);

//	AdaptiveWait<
//		100 /*NumberOfSignalsBeforeEnteringWaitState*/, 0 /*NumberOfSignalsBeforeLeavingWaitState*/,
//		10000 /*MinWait*/, 50000 /*MaxWait*/,
//		5000 /*SlowDownStep*/, 10000 /*SpeedUpStep*/> adaptive_wait;

//	bool last_completed = true;

	while(!mStopRequest)
	{
		processITC(false, false);

		bool invocation_pending = false;

		for(auto& service : mServices)
		{
			if((service.second->isInvocationPending()))
				invocation_pending = true;
		}

		// It's likely in the case that this thread is running this function while
		// the kernel is not set and initialized yet.
		if (LIKELY(mKernel && mKernel->isInitialized()))
		{
			if(invocation_pending)
			{
				// send event for "before invocation dispatch" for all services
				for(auto& service : mServices)
					service.second->handleEvent(Service::events::before_invocation_dispatch);

				mKernel->launch(this);

				// send event for "after invocation dispatch" for all services
				for(auto& service : mServices)
					service.second->handleEvent(Service::events::after_invocation_dispatch);
			}
			else
			{
				for(auto& service : mServices)
					service.second->handleEvent(Service::events::idletime_heartbeat);
			}
		}
	}
}

void ProcessorCUDA::pushImplicitPendingCompletion(int count)
{
    if(mImplicitPendingCompletion == 0)
        mMonitor->markIncompleted(getId());

    mImplicitPendingCompletion += count;
}

void ProcessorCUDA::popImplicitPendingCompletion(int count)
{
    mImplicitPendingCompletion -= count;
}

//////////////////////////////////////////////////////////////////////////
bool ProcessorCUDA::isCompleted()
{
	return mMonitor->isCompleted(getId());
}

bool ProcessorCUDA::isRunning()
{
	return mWorker.joinable();
}

uint32 ProcessorCUDA::getDeviceId()
{
	return mDeviceId;
}

} } }
