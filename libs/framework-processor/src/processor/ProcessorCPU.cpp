/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <utility>

#include "core/SharedPtr.h"

#include "threading/AdaptiveWait.h"

#include "framework/processor/KernelMT.h"
#include "framework/processor/ProcessorCPU.h"

namespace zillians { namespace framework { namespace processor {

//////////////////////////////////////////////////////////////////////////
ProcessorCPU::ProcessorCPU(uint32 local_id)
    : ProcessorRT(local_id)
    , mMultiThread(false)
    , mIsExited(false)
    , mExitCode(-1ll)
    , mImplicitPendingCompletion(0)
{
}

ProcessorCPU::~ProcessorCPU()
{
}

bool ProcessorCPU::isMultiThread() const noexcept { return mMultiThread; }
void ProcessorCPU::setMultiThread(bool enable) noexcept { mMultiThread = enable; }

void ProcessorCPU::initialize()
{
    BOOST_ASSERT(mKernel == nullptr && "already initialized");

    mKernel = make_shared<mt::Kernel>(mEnableServerFunction, mEnableClientFunction, mMultiThread);

    ProcessorRT::initialize();
}

void ProcessorCPU::finalize()
{
    BOOST_ASSERT(mKernel != nullptr && "not yet initialized");

    ProcessorRT::finalize();

    mKernel = nullptr;
}

void ProcessorCPU::exit(int32 exit_code)
{
    if (mIsExited.exchange(true))
        return; // already exited, just return

    mExitCode = exit_code;
    mMonitor->markCompleted(mProcessorId);

    // avoid RAII since this function will NOT return
    auto*const mt_kernel = static_cast<mt::Kernel*>(mKernel.get());

    mt_kernel->exitLaunch();
}

int32 ProcessorCPU::getExitCode() const
{
    return mExitCode;
}

void ProcessorCPU::run(const shared_ptr<ExecutionMonitor>& monitor)
{
    setMonitor(monitor);

    while (!mStopRequest)
    {
        runImpl();
    }
}

void ProcessorCPU::pushImplicitPendingCompletion(int count)
{
    if(mImplicitPendingCompletion == 0)
        mMonitor->markIncompleted(getId());

    mImplicitPendingCompletion += count;
}

void ProcessorCPU::popImplicitPendingCompletion(int count)
{
    mImplicitPendingCompletion -= count;
}

bool ProcessorCPU::isCompleted()
{
    return mMonitor->isCompleted(getId());
}

bool ProcessorCPU::isRunning()
{
    return mWorker.joinable();
}

void ProcessorCPU::runImpl()
{
    processITC(false, false);

    static threading::AdaptiveWait<
        1000 /*NumberOfSignalsBeforeEnteringWaitState*/, 0 /*NumberOfSignalsBeforeLeavingWaitState*/,
        10000 /*MinWait*/, 50000 /*MaxWait*/,
//      0 /*MinWait*/, 0 /*MaxWait*/,
        500 /*SlowDownStep*/, 5000 /*SpeedUpStep*/> adaptive_wait;

    bool invocation_pending = false;
    for(auto& service : mServices)
    {
        if(service.second->isInvocationPending())
            invocation_pending = true;
    }

    // It's likely in the case that this thread is running this function while
    // the kernel is not set and initialized yet.
    const auto& kernel = static_pointer_cast<mt::Kernel>(mKernel);
    if (LIKELY(kernel.get() && kernel->isInitialized()))
    {
        if(invocation_pending)
        {
            adaptive_wait.speedup();

            // send event for "before invocation dispatch" for all services
            for(auto& service : mServices)
                service.second->handleEvent(Service::events::before_invocation_dispatch);

            kernel->launch(this);

            // send event for "after invocation dispatch" for all services
            for(auto& service : mServices)
                service.second->handleEvent(Service::events::after_invocation_dispatch);
        }
        else
        {
            adaptive_wait.slowdown();
            adaptive_wait.wait();

            for(auto& service : mServices)
                service.second->handleEvent(Service::events::idletime_heartbeat);
        }
    }
}

} } }
