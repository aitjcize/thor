/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cstdint>
#include <mutex>
#include <thread>
#include <functional>
#include <type_traits>
#include <utility>

#include <boost/asio/error.hpp>
#include <boost/bind.hpp>
#include <boost/optional/optional.hpp>
#include <boost/algorithm/cxx11/any_of.hpp>

#include <msgpack.hpp>

#include "utility/ProtocolParser.h"
#include "utility/FixPointCombinator.h"
#include "utility/MemoryUtil.h"
#include "utility/TimerUtil.h"
#include "utility/UnicodeUtil.h"

#include "threading/AdaptiveWait.h"

#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/VariableDecl.h"

#include "framework/buffer/BufferKernel.h"
#include "framework/ProcessorId.h"
#include "framework/processor/ProcessorGateway.h"
#include "framework/processor/websocket/WebSocketGatewayHandler.h"
#include "framework/service/runtime/RuntimeServiceBuffer.h"

namespace zillians { namespace framework { namespace processor {

namespace {

template <int Version>
struct getProtocol;
template <>
struct getProtocol<4>
{
    operator network::session::TCPSessionEngine::protocol_t() const
    {
        return network::session::TCPSessionEngine::protocol_t::v4();
    }
};
template <>
struct getProtocol<6>
{
    operator network::session::TCPSessionEngine::protocol_t() const
    {
        return network::session::TCPSessionEngine::protocol_t::v6();
    }
};

}

ProcessorGateway::ProcessorGateway(uint32 local_id)
    : Processor(local_id)
    , mImplicitPendingCompletion(0)
    , mListenerId(UUID::nil())
    , mIdSessionMapMutex()
    , mIdSessionMap()
    , mKernel(nullptr)
    , mWebSocketGateway(this)
{
    BufferNetwork::declareReadableLocationsWithDefaultPriority<service::InvocationRequestBuffer>(local_id);
    BufferNetwork::declareWritableLocationsWithDefaultPriority<service::InvocationRequestBuffer>(local_id);
}

ProcessorGateway::~ProcessorGateway()
{ }

void ProcessorGateway::initialize()
{
    Processor::initialize();

    const auto& handler_infos =
    {
        std::make_pair(KernelITC::processor_itc_invocation_request, &ProcessorGateway::handleInvocationITC   ),
        std::make_pair(KernelITC::domain_itc_listen_request       , &ProcessorGateway::handleITCDomainListen ),
        std::make_pair(KernelITC::domain_itc_connect_request      , &ProcessorGateway::handleITCDomainConnect),
        std::make_pair(KernelITC::domain_itc_cancel_request       , &ProcessorGateway::handleITCDomainCancel ),
    };

    for (const auto& handler_info : handler_infos)
        mExecutionITC->registerHandler(
            handler_info.first,
            boost::bind(handler_info.second, this, _1, _2)
        );
}

void ProcessorGateway::finalize()
{
    mExecutionITC->unregisterHandler(KernelITC::domain_itc_cancel_request);
    mExecutionITC->unregisterHandler(KernelITC::domain_itc_connect_request);
    mExecutionITC->unregisterHandler(KernelITC::domain_itc_listen_request);
    mExecutionITC->unregisterHandler(KernelITC::processor_itc_invocation_request);

    Processor::finalize();
}

void ProcessorGateway::setKernel(const shared_ptr<KernelBase>& kernel)
{
    mKernel = kernel;
}

const shared_ptr<KernelBase>& ProcessorGateway::getKernel() const noexcept
{
    return mKernel;
}

void ProcessorGateway::run(const shared_ptr<ExecutionMonitor>& monitor)
{
    using namespace zillians::network::session;

    mMonitor = monitor;

    static threading::AdaptiveWait<
        1000 /*NumberOfSignalsBeforeEnteringWaitState*/, 0 /*NumberOfSignalsBeforeLeavingWaitState*/,
        10000 /*MinWait*/, 50000 /*MaxWait*/,
//        0 /*MinWait*/, 0 /*MaxWait*/,
        500 /*SlowDownStep*/, 5000 /*SpeedUpStep*/> adaptive_wait;

    while(!mStopRequest)
    {
        processITC(false, false);

        KernelITC itc;
        bool any_session_event = false;
        while(mSessionITCs.try_pop(itc))
        {
            mExecutionITC->dispatch(ProcessorId::PROCESSOR_PRINCIPLE, itc);
            any_session_event = true;
        }

        if(!any_session_event)
        {
            adaptive_wait.slowdown();
            adaptive_wait.wait();
            boost::this_thread::yield();
        }
        else
        {
            adaptive_wait.speedup();
        }
    }
}

bool ProcessorGateway::start(const std::shared_ptr<ExecutionMonitor>& monitor)
{
    if (mSessionEngine != nullptr)
        return false;

    auto engine = make_unique<network::session::TCPSessionEngine>();

    engine->addScheduler();

    const bool is_success = Processor::start(monitor);

    if (is_success)
        swap(engine, mSessionEngine);

    return is_success;
}

bool ProcessorGateway::stop()
{
    if (mSessionEngine == nullptr)
        return false;

    mSessionEngine.reset();

    mWebSocketGateway.stop();

    return Processor::stop();
}

void ProcessorGateway::pushImplicitPendingCompletion(int count)
{
    if(mImplicitPendingCompletion == 0)
        mMonitor->markIncompleted(getId());

    mImplicitPendingCompletion += count;
}

void ProcessorGateway::popImplicitPendingCompletion(int count)
{
    mImplicitPendingCompletion -= count;
}

bool ProcessorGateway::isCompleted()
{
    return mMonitor->isCompleted(getId());
}

bool ProcessorGateway::isRunning()
{
    return mWorker.joinable();
}

KernelITC::DomainError ProcessorGateway::listen(uint32 source, const UUID& id, const std::string& endpoint)
{
    BOOST_ASSERT(!id.is_nil() && "incorrect id!");

    if (!mListenerId.is_nil())
        return KernelITC::DomainError::EXCEED_CONCURRENT_LIMIT;

    ProtocolParser<std::string::const_iterator> parser;

    if (!parser.parse(endpoint.begin(), endpoint.end()))
        return KernelITC::DomainError::INVALID_ENDPOINT_SPEC;

    const auto& transport = parser.transport;
    const auto& port      = parser.port;

    static_assert(noexcept(mListenerId = id), "we need no-throw guarantee to provide strong exception safety");

    KernelITC::DomainError error = KernelITC::DomainError::OK;

    if (transport == "tcp" || transport == "tcp4")
        error = listenImplTcp<4>(source, id, port);
    else if (transport == "tcp6")
        error = listenImplTcp<6>(source, id, port);
    else if (transport == "ws")
        error = listenImplWs(source, id, port);
    else
        error = KernelITC::DomainError::UNSUPPORTED_TRANSPORT;

    if (error == KernelITC::DomainError::OK)
        mListenerId = id;

    return error;
}

KernelITC::DomainError ProcessorGateway::connect(uint32 continuation_id, uint32 source, const UUID& id, const std::string& endpoint)
{
    BOOST_ASSERT(!id.is_nil() && "incorrect id!");

    ProtocolParser<std::string::const_iterator> parser;

    if (!parser.parse(endpoint.begin(), endpoint.end()))
        return KernelITC::DomainError::INVALID_ENDPOINT_SPEC;

    const auto& transport = parser.transport;
    const auto& address   = parser.address;
    const auto& port      = parser.port;

    KernelITC::DomainError error = KernelITC::DomainError::OK;

    if (transport == "tcp" || transport == "tcp4")
        error = connectImplTcp<4>(continuation_id, source, id, address, port);
    else if (transport == "tcp6")
        error = connectImplTcp<6>(continuation_id, source, id, address, port);
    else
        error = KernelITC::DomainError::UNSUPPORTED_TRANSPORT;

    return error;
}

KernelITC::DomainError ProcessorGateway::cancel(const UUID& id)
{
    BOOST_ASSERT(!id.is_nil() && "incorrect id!");

    auto error     = KernelITC::DomainError::NO_SUCH_REQUEST;
    auto cancelers = {&ProcessorGateway::cancelImplListen, &ProcessorGateway::cancelImplConnect};

    boost::algorithm::any_of(
        cancelers,
        [this, &id, &error](const decltype(cancelers)::value_type& canceler)
        {
            error = (this->*canceler)(id);

            return error != KernelITC::DomainError::NO_SUCH_REQUEST;
        }
    );

    return error;
}

template<unsigned version>
KernelITC::DomainError ProcessorGateway::listenImplTcp(uint32 source, const UUID& id, const boost::optional<std::uint16_t>& port)
{
    static_assert(version == 4 || version == 6, "invalid IP version!");

    using zillians::network::session::TCPSession;
    using zillians::network::session::TCPSessionEngine;

    if (!port.is_initialized())
        return KernelITC::DomainError::INVALID_ENDPOINT_SPEC;

    const auto& ec = mSessionEngine->listen(getProtocol<version>(), *port);

    if (ec)
        return KernelITC::DomainError::UNKNOWN_ERROR;

    // start accepting new connection
    TCPSession* new_session = mSessionEngine->createSession();
    new_session->setNoDelay(true);
    new_session->setMinimalSend(128);
    new_session->setMinimalRecv(128);

    mSessionEngine->acceptAsync(new_session, boost::bind(&ProcessorGateway::handleAccepted, this, source, id, new_session, _1));

    return KernelITC::DomainError::OK;
}

KernelITC::DomainError ProcessorGateway::listenImplWs(uint32 source, const UUID& id, const boost::optional<std::uint16_t>& port)
{
    BOOST_ASSERT(!mWebSocketGateway.is_started() && "websocket is working!");

    if (!port.is_initialized())
        return KernelITC::DomainError::INVALID_ENDPOINT_SPEC;

    const auto is_listened = mWebSocketGateway.start(source, id, *port);

    return is_listened ? KernelITC::DomainError::OK : KernelITC::DomainError::UNKNOWN_ERROR;
}

template<unsigned version>
KernelITC::DomainError ProcessorGateway::connectImplTcp(uint32 continuation_id, uint32 source, const UUID& id, const std::string& address, const boost::optional<std::uint16_t>& port)
{
    using bimap_type = boost::bimaps::bimap<UUID, boost::bimaps::multiset_of<network::session::TCPSession*>>;

    if (!port.is_initialized())
        return KernelITC::DomainError::INVALID_ENDPOINT_SPEC;

    auto                           new_session = std::unique_ptr<network::session::TCPSession>(mSessionEngine->createSession());
    bimap_type::left_map::iterator request_pos;

    {
        bool                        is_inserted = false;
        std::lock_guard<std::mutex> guard(mIdSessionMapMutex);

        std::tie(request_pos, is_inserted) = mIdSessionMap.left.insert({id, new_session.get()});

        BOOST_ASSERT(is_inserted && "UUID collision");
        BOOST_ASSERT(request_pos != mIdSessionMap.left.end() && "invalid iterator from bimap");

        if (!is_inserted)
            return KernelITC::DomainError::EXCEED_CONCURRENT_LIMIT;
    }

    try
    {
        new_session->setNoDelay(true);
        new_session->setMinimalSend(128);
        new_session->setMinimalRecv(128);

        mSessionEngine->connectAsync(
            new_session.get(),
            getProtocol<version>(),
            address, *port,
            boost::bind(&ProcessorGateway::handleConnected, this, continuation_id, source, id, new_session.get(), _1)
        );

        new_session.release();

        return KernelITC::DomainError::OK;
    }
    catch (const std::exception&)
    {
    }

    BOOST_ASSERT(new_session != nullptr && "invalid state");

    // failure, do rollback
    {
        std::lock_guard<std::mutex> guard(mIdSessionMapMutex);

        mIdSessionMap.left.erase(request_pos);
    }

    return KernelITC::DomainError::UNKNOWN_ERROR;
}

KernelITC::DomainError ProcessorGateway::cancelImplListen(const UUID& id)
{
    if (mListenerId != id)
        return KernelITC::DomainError::NO_SUCH_REQUEST;

    bool is_canceled = false;

    if (mWebSocketGateway.is_started())
        is_canceled = mWebSocketGateway.stop();
    else
        is_canceled = !mSessionEngine->cancel();

    mListenerId = UUID::nil();

    return is_canceled ? KernelITC::DomainError::OK : KernelITC::DomainError::UNKNOWN_ERROR;
}

KernelITC::DomainError ProcessorGateway::cancelImplConnect(const UUID& id)
{
    boost::system::error_code ec;

    {
        std::lock_guard<std::mutex> guard(mIdSessionMapMutex);

        const auto pos = mIdSessionMap.left.find(id);

        if (pos == mIdSessionMap.left.end())
            return KernelITC::DomainError::NO_SUCH_REQUEST;

        pos->second->cancel(ec);
    }

    return ec ? KernelITC::DomainError::UNKNOWN_ERROR : KernelITC::DomainError::OK;
}

void ProcessorGateway::handleAccepted(uint32 source, const UUID& id, network::session::TCPSession* session, const boost::system::error_code& ec)
{
    using namespace zillians::network::session;

    if(ec)
    {
        std::cerr << "error while accepting: " << ec.message() << std::endl;
        session->close();
        delete session;
    }
    else
    {
        DomainContext* context = createDomainContext(session, WebSocketGateway::connection_ptr());

        // create session open itc
        auto itc = KernelITC::make<KernelITC::DomainSignalConnectedRequest>(0, id, context->session_id);

        sendITC(source, itc);

        context->stream->readAsync(context->sequence_message_in, boost::bind(&ProcessorGateway::handleSequenceMessage, this, source, id, context, _1, _2));
    }

    if(ec == boost::asio::error::operation_aborted)
        return;

    // TODO handle error code when scheduler is stopped
    TCPSession* another_session = mSessionEngine->createSession();
    mSessionEngine->acceptAsync(another_session, boost::bind(&ProcessorGateway::handleAccepted, this, source, id, another_session, _1));
}

void ProcessorGateway::handleConnected(uint32 continuation_id, uint32 source, const UUID& id, network::session::TCPSession* session, const boost::system::error_code& ec)
{
    DomainContext*const context = ec ? nullptr : createDomainContext(session, WebSocketGateway::connection_ptr());

    if (context == nullptr)
        std::cerr << "error while connecting: " << ec.message() << std::endl;

    {
        const auto& error = context == nullptr ? KernelITC::DomainError::UNKNOWN_ERROR : KernelITC::DomainError::OK;

        auto response_itc = KernelITC::make<KernelITC::DomainConnectResponse>(continuation_id, id, error);

        sendITC(source, response_itc);
    }

    if (context == nullptr)
    {
        {
            std::lock_guard<std::mutex> guard(mIdSessionMapMutex);

            const auto& erased_count = mIdSessionMap.right.erase(session);

            BOOST_ASSERT(erased_count == 1 && "erase amount is not equal to 1 !");
        }

        boost::system::error_code dummy_ec;
        session->shutdown(network::session::TCPSession::shutdown_both, dummy_ec);
        session->close(dummy_ec);
        delete session;

        return;
    }

    {
        auto response_itc = KernelITC::make<KernelITC::DomainSignalConnectedRequest>(0, id, context->session_id);

        sendITC(source, response_itc);
    }

    context->stream->readAsync(context->sequence_message_in, boost::bind(&ProcessorGateway::handleSequenceMessage, this, source, id, context, _1, _2));
}

void ProcessorGateway::handleSequenceMessage(uint32 source, const UUID& id, DomainContext* context, const boost::system::error_code& ec, std::size_t bytes_transferred)
{
    if(ec)
    {
        // TODO check the error code and take different actions
        std::cerr << "error while reading sequence message: " << ec.message() << std::endl;

        auto itc = KernelITC::make<KernelITC::DomainSignalDisconnectedRequest>(0, id, context->session_id);
        mSessionITCs.push(itc);

        // TODO delete domain context if appropriate
    }
    else
    {
        if(context->sequence_message_in.sequence_count > 0)
        {
            switch(context->sequence_message_in.next_message_type_in_sequence)
            {
            case SequenceMessage::REMOTE_INVOCATION:
                context->stream->readAsync(context->remote_invocation_message_in, boost::bind(&ProcessorGateway::handleInvocationMessage, this, source, id, context, _1, _2));
                break;
            default:
                UNREACHABLE_CODE(); break;
            }
        }
        else
        {
            context->stream->readAsync(context->sequence_message_in, boost::bind(&ProcessorGateway::handleSequenceMessage, this, source, id, context, _1, _2));
        }
    }
}

void ProcessorGateway::handleInvocationMessage(uint32 source, const UUID& id, DomainContext* context, const boost::system::error_code& ec, std::size_t bytes_transferred)
{
    using namespace zillians::framework::service;
    if(ec)
    {
        // TODO check the error code and take different actions
        std::cerr << "error while reading invocation message: " << ec.message() << std::endl;

        auto itc = KernelITC::make<KernelITC::DomainSignalDisconnectedRequest>(0, id, context->session_id);
        mSessionITCs.push(itc);

//        mClosedSessions.push(context);
    }
    else
    {
//        std::cerr << "[ProcessorGateway] [handleInvocationMessage] check point 1: " << TimerUtil::clock_get_time_ms() << std::endl;

        auto& in_buffer = context->remote_invocation_message_in.buffer;

        // push the invocation buffer to invocation queue
        uint32                   local_id = getId();
        InvocationRequestBuffer* buffer   = new InvocationRequestBuffer(local_id);

        uint64 invocation_bytes = 0;

        BOOST_ASSERT(in_buffer.dataSize() > sizeof(invocation_bytes) && "input ended unexpectedly");

        in_buffer.read(invocation_bytes);

        BOOST_ASSERT(invocation_bytes % Invocation::PAYLOAD == 0 && "invalid invocation message size");
        BOOST_ASSERT(in_buffer.dataSize() >= invocation_bytes && "input ended unexpectedly");

        int number_of_invocation = invocation_bytes / Invocation::PAYLOAD;
        BufferNetwork::instance()->create(local_id, *buffer, InvocationRequestBuffer::Dimension(number_of_invocation));

        buffer::memcpy_func_table[buffer->location][buffer::buffer_location_t::host_unpinned](
            buffer->getInvocationRawPtr(0),
            in_buffer.rptr(),
            invocation_bytes
        );

        in_buffer.rskip(invocation_bytes);

        for (int i = 0; i < number_of_invocation; ++i)
            buffer->setSessionId(i, context->session_id);
        buffer->setInvocationCount(number_of_invocation);

        if (buffer->location == buffer_location_t::host_unpinned)
        {
            for (int i = 0; i < number_of_invocation; ++i)
            {
                BOOST_ASSERT(buffer->isRemote(i) && "non-remote invocation should not be here");
                BOOST_ASSERT(buffer->location == buffer_location_t::host_unpinned && "only CPU supports object replication");

                uint64 ui64_data_size = 0;

                BOOST_ASSERT(in_buffer.dataSize() >= sizeof(ui64_data_size) && "input ended unexpectedly");

                in_buffer.read(ui64_data_size);

                BOOST_ASSERT(in_buffer.dataSize() >= ui64_data_size && "input ended unexpectedly");

                ReplicationData* data = new ReplicationData(buffer_location_t::host_unpinned);

                data->write(in_buffer.rptr(), ui64_data_size);
                in_buffer.rskip(ui64_data_size);

                *reinterpret_cast<ReplicationData**>(buffer->getInvocationPtr(i)) = data;
            }
        }

        auto itc = KernelITC::make<KernelITC::InvocationRequest>(0, 0, buffer);

        mSessionITCs.push(itc);

//        std::cerr << "[ProcessorGateway] [handleInvocationMessage] check point 2: " << TimerUtil::clock_get_time_ms() << std::endl;

        in_buffer.clear();

        if(--context->sequence_message_in.sequence_count > 0)
        {
            context->stream->readAsync(context->remote_invocation_message_in, boost::bind(&ProcessorGateway::handleInvocationMessage, this, source, id, context, _1, _2));
        }
        else
        {
            context->stream->readAsync(context->sequence_message_in, boost::bind(&ProcessorGateway::handleSequenceMessage, this, source, id, context, _1, _2));
        }
    }
}

void ProcessorGateway::handleITCDomainListen(uint32 source, KernelITC& request_itc)
{
    BOOST_ASSERT(request_itc.isDomainListenRequest() && "handling unexpected request");

    const auto& request  = request_itc.getDomainListenRequest();

    const auto& id       = request.id;
    const auto& endpoint = request.endpoint;

    const auto& error    = listen(source, id, endpoint);

    auto response_itc = KernelITC::make<KernelITC::DomainListenResponse>(request_itc.continuation_id, id, error);

    sendITC(source, response_itc);
}

void ProcessorGateway::handleITCDomainConnect(uint32 source, KernelITC& request_itc)
{
    BOOST_ASSERT(request_itc.isDomainConnectRequest() && "handling unexpected request");

    const auto& request = request_itc.getDomainConnectRequest();

    const auto& id       = request.id;
    const auto& endpoint = request.endpoint;

    const auto& error    = connect(request_itc.continuation_id, source, id, endpoint);

    if (error == KernelITC::DomainError::OK)
        return; // we connect asynchronously, do not send response here

    auto response_itc = KernelITC::make<KernelITC::DomainConnectResponse>(request_itc.continuation_id, id, error);

    sendITC(source, response_itc);
}

void ProcessorGateway::handleITCDomainCancel(uint32 source, KernelITC& request_itc)
{
    BOOST_ASSERT(request_itc.isDomainCancelRequest() && "handling unexpected request");

    const auto& request = request_itc.getDomainCancelRequest();

    const auto& id      = request.id;

    const auto& error   = cancel(id);

    auto response_itc = KernelITC::make<KernelITC::DomainCancelResponse>(request_itc.continuation_id, id, error);

    sendITC(source, response_itc);
}

static std::string transform_buffer_to_msgpack(const language::tree::FunctionDecl* func_decl, service::Invocation* ivk)
{
    msgpack::sbuffer buffer;
    msgpack::packer<msgpack::sbuffer> pk(&buffer);

    size_t num_consumed_arg_bytes = 0;
    pk.pack_array(func_decl->parameters.size() + 1);
    pk.pack(ws_to_s(func_decl->name->toString()));
    for(size_t i = 0; i != func_decl->parameters.size(); ++i)
    {
        language::tree::VariableDecl* var_decl = func_decl->parameters[i];
        language::tree::PrimitiveSpecifier* ts_primitive = language::tree::cast<language::tree::PrimitiveSpecifier>(var_decl->type);
        size_t arg_bytes = 0;
        if(ts_primitive != nullptr)
        {
            using namespace language::tree;

            switch(ts_primitive->getKind())
            {
                case PrimitiveKind::BOOL_TYPE   : { arg_bytes = 4; bool            v = *reinterpret_cast<bool           *>(&ivk->buffer.parameters[num_consumed_arg_bytes]); pk.pack(v); num_consumed_arg_bytes += arg_bytes; } break;
                case PrimitiveKind::INT8_TYPE   : { arg_bytes = 4; zillians::int8  v = *reinterpret_cast<zillians::int8 *>(&ivk->buffer.parameters[num_consumed_arg_bytes]); pk.pack(v); num_consumed_arg_bytes += arg_bytes; } break;
                case PrimitiveKind::INT16_TYPE  : { arg_bytes = 4; zillians::int16 v = *reinterpret_cast<zillians::int16*>(&ivk->buffer.parameters[num_consumed_arg_bytes]); pk.pack(v); num_consumed_arg_bytes += arg_bytes; } break;
                case PrimitiveKind::INT32_TYPE  : { arg_bytes = 4; zillians::int32 v = *reinterpret_cast<zillians::int32*>(&ivk->buffer.parameters[num_consumed_arg_bytes]); pk.pack(v); num_consumed_arg_bytes += arg_bytes; } break;
                case PrimitiveKind::INT64_TYPE  : { arg_bytes = 8; zillians::int64 v = *reinterpret_cast<zillians::int64*>(&ivk->buffer.parameters[num_consumed_arg_bytes]); pk.pack(v); num_consumed_arg_bytes += arg_bytes; } break;
                case PrimitiveKind::FLOAT32_TYPE: { arg_bytes = 4; float           v = *reinterpret_cast<float          *>(&ivk->buffer.parameters[num_consumed_arg_bytes]); pk.pack(v); num_consumed_arg_bytes += arg_bytes; } break;
                case PrimitiveKind::FLOAT64_TYPE: { arg_bytes = 8; double          v = *reinterpret_cast<double         *>(&ivk->buffer.parameters[num_consumed_arg_bytes]); pk.pack(v); num_consumed_arg_bytes += arg_bytes; } break;
            }
        }
        else
        {
            UNIMPLEMENTED_CODE();
        }
    }
    std::string str_to_send(buffer.data(), buffer.data() + buffer.size());
    return str_to_send;
}

static const std::string base64_chars =
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";


static inline bool is_base64(unsigned char c) {
  return (isalnum(c) || (c == '+') || (c == '/'));
}

std::string base64_encode(const std::string& input) {
    const char* bytes_to_encode = input.data();
    unsigned int in_len = input.size();
    std::string ret;
    int i = 0;
    int j = 0;
    unsigned char char_array_3[3];
    unsigned char char_array_4[4];

    while (in_len--) {
        char_array_3[i++] = *(bytes_to_encode++);
        if (i == 3) {
            char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
            char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
            char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
            char_array_4[3] = char_array_3[2] & 0x3f;

            for(i = 0; (i <4) ; i++)
                ret += base64_chars[char_array_4[i]];
            i = 0;
        }
    }

    if (i)
    {
        for(j = i; j < 3; j++)
            char_array_3[j] = '\0';

        char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
        char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
        char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
        char_array_4[3] = char_array_3[2] & 0x3f;

        for (j = 0; (j < i + 1); j++)
            ret += base64_chars[char_array_4[j]];

        while((i++ < 3))
            ret += '=';

    }
    return ret;
}

void ProcessorGateway::handleInvocationITC(uint32 source, KernelITC& itc)
{
    using namespace zillians::framework::service;

    // find the corresponding DomainContext
    auto invocation_buffer = itc.getInvocationRequest().buffer;
    int32 invocation_count = invocation_buffer->getInvocationCount();
//    std::cerr << "[ProcessorGateway] [handleInvocationITC] invocation count = " << invocation_count << std::endl;
//    std::cerr << "[ProcessorGateway] [handleInvocationITC] check point 1: " << TimerUtil::clock_get_time_ms() << std::endl;
    for(int32 i=0;i<invocation_count;++i)
    {
        int64 session_id = invocation_buffer->getSessionId(i);
        int32 local_id = (int32)(session_id & 0xFFFFFFFFL);
        if(DomainContext* context = mSessionMap[local_id])
        {
            if(context->is_websocket())
            {
                if(!invocation_buffer->isRemote(i))
                {
                    int64 funciton_id = invocation_buffer->getFunctionId(i);
                    const language::tree::FunctionDecl* func_decl = getKernel()->queryFunctionDecl(funciton_id);
                    if(func_decl != nullptr)
                    {
                        std::string str_to_send = transform_buffer_to_msgpack(func_decl, (service::Invocation*)invocation_buffer->getInvocationRawPtr(i));
                        //context->connection_ptr->send(str_to_send, websocketpp::frame::opcode::BINARY);
                        std::string str_to_send_base64 = base64_encode(str_to_send);
                        context->connection_ptr->send(str_to_send_base64);
                        std::cout << "Sent message '" << str_to_send_base64 << "'(" << str_to_send_base64.size() << ") to client." << std::endl;
                    }
                }
            }
            else
            {
                context->sequence_message_out.sequence_count = 1;
                context->sequence_message_out.next_message_type_in_sequence = SequenceMessage::REMOTE_INVOCATION;

                context->remote_invocation_message_out.buffer.clear();

                // coalesce continuous invocation of same session into one invocation message
                int j;
                for(j=i+1;j<invocation_count;++j)
                    if(invocation_buffer->getSessionId(j) != session_id)
                        break;

                int coalesced_invocations = j - i;

                auto& out_buffer = context->remote_invocation_message_out.buffer;

#define RESERVE_MORE(buffer, size)                               \
    if ((buffer).freeSize() < (size))                            \
        (buffer).resize((buffer).allocatedSize() + (size))

    //            std::cerr << "[ProcessorGateway] [handleInvocationITC] check point 2: " << TimerUtil::clock_get_time_ms() << std::endl;

                const std::size_t      invocation_bytes = coalesced_invocations * Invocation::PAYLOAD;
                const uint64      ui64_invocation_bytes = invocation_bytes;

                // make sure enough room to copy invocation buffer
                RESERVE_MORE(out_buffer, ui64_invocation_bytes + invocation_bytes);

                out_buffer.write(ui64_invocation_bytes);

                // copy the invocation buffer to remote
                buffer::memcpy_func_table[buffer_location_t::host_unpinned][invocation_buffer->location](
                    out_buffer.wptr(),
                    invocation_buffer->getInvocationRawPtr(i),
                    invocation_bytes
                );

                out_buffer.wskip(invocation_bytes);

                if (invocation_buffer->location == buffer_location_t::host_unpinned)
                {
                    for (int k = i; k != j; ++k)
                    {
                        BOOST_ASSERT(invocation_buffer->isRemote(k) && "non-remote invocation should not be here");

                        ReplicationData*const      data      = *reinterpret_cast<ReplicationData**>(invocation_buffer->getInvocationPtr(k));
                        const std::size_t          data_size = data->getSize();
                        const uint64          ui64_data_size = data_size;

                        RESERVE_MORE(out_buffer, ui64_data_size + data_size);

                        out_buffer.write(ui64_data_size);

                        data->resetRead();
                        data->read(out_buffer.wptr(), data_size);
                        out_buffer.wskip(data_size);
                    }
                }

#undef RESERVE_MORE

    //            std::cerr << "[ProcessorGateway] [handleInvocationITC] check point 3: " << TimerUtil::clock_get_time_ms() << std::endl;
                context->stream->write(context->sequence_message_out);
                context->stream->write(context->remote_invocation_message_out);

    //            std::cerr << "[ProcessorGateway] [handleInvocationITC] check point 4: " << TimerUtil::clock_get_time_ms() << std::endl;

                // advance to next session
                i = j - 1;
            }
        }
    }
}

} } }
