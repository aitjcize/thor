/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <exception>
#include <iostream>
#include <limits>
#include <mutex>
#include <thread>
#include <vector>

#include <boost/asio/io_service.hpp>
#include <boost/assert.hpp>
#include <boost/range/irange.hpp>

#include "utility/Functional.h"
#include "utility/Foreach.h"

#include "core/IntTypes.h"

#include "framework/processor/detail/SimpleInvocationsLauncher.h"
#include "framework/processor/KernelMT.h"
#include "framework/service/runtime/RuntimeServiceBuffer.h"

namespace zillians { namespace framework { namespace processor { namespace mt { namespace detail {

namespace {

template<typename size_type>
std::vector<boost::integer_range<uint32>> split_invocations(const service::InvocationRequestBuffer& invocations, const size_type& worker_count)
{
    const auto&                               task_count = invocations.getInvocationCount();
    std::vector<boost::integer_range<uint32>> ranges;

    BOOST_ASSERT(  task_count >= 0 && "no task");
    BOOST_ASSERT(worker_count >= 0 && "no worker");

    if (task_count >= worker_count)
    {
        ranges.reserve(worker_count);

              uint32 worker_task_beg        = 0;
              uint32 worker_task_end        = std::numeric_limits<uint32>::max();
        const uint32 worker_task_count      = task_count / worker_count;
              uint32 worker_task_count_more = task_count % worker_count;

        for (const auto& index : zero_to(worker_count))
        {
            if (worker_task_count_more == 0)
            {
                worker_task_end = worker_task_beg + worker_task_count;
            }
            else
            {
                worker_task_end = worker_task_beg + worker_task_count + 1;
                --worker_task_count_more;
            }

            ranges.emplace_back(worker_task_beg, worker_task_end);

            worker_task_beg = worker_task_end;
        }
    }
    else
    {
        ranges.reserve(task_count);

        for (const auto& index : zero_to(task_count))
            ranges.emplace_back(index, index + 1);
    }

    return ranges;
}

}

SimpleInvocationsLauncher::SimpleInvocationsLauncher(bool enable_mt)
    : stack_pool(enable_mt ? std::thread::hardware_concurrency() : 1u)
    , service()
    , workers()
    , unfinished_count(0)
    , unfinished_guard()
    , unfinished_cond()
{
    const auto& concurrency = stack_pool.getConcurrency();

    workers.reserve(concurrency);

    for (const auto& index : zero_to(concurrency))
    {
        workers.emplace_back(
            [this, index]
            {
                // make io_service::run will not return even if there is no task
                boost::asio::io_service::work dummy_work(service);

                while (true)
                {
                    try
                    {
                        service.run();

                        break;
                    }
                    catch (const std::exception& e)
                    {
                        std::cerr << "exception e: " << e.what() << std::endl;
                    }
                }
            }
        );
    }
}

SimpleInvocationsLauncher::~SimpleInvocationsLauncher()
{
    service.stop();

    for (auto& worker : workers)
        if (worker.joinable())
            worker.join();
}

void SimpleInvocationsLauncher::launch(Kernel& kernel, service::InvocationRequestBuffer& invocations)
{
    BOOST_ASSERT(unfinished_count == 0 && "re-launching!");

    const auto& ranges = split_invocations(invocations, workers.size());
    const auto& stacks = stack_pool.getStacks();

    BOOST_ASSERT(stacks.size() >= ranges.size() && "amount of range is out of concurrency");

    unfinished_count = ranges.size();

    for (const auto& index : zero_to(ranges.size()))
    {
        const auto& stack = stacks[index];
        const auto& range = ranges[index];

        service.post(
            [this, &kernel, &invocations, index, stack, range]
            {
                const auto& finished_marker = invoke_on_destroy(
                    [this]
                    {
                        {
                            std::lock_guard<std::mutex> lock(unfinished_guard);

                            --unfinished_count;
                        }

                        unfinished_cond.notify_one();
                    }
                );

                BOOST_ASSERT(!range.empty() && "launch without task!");

                const auto& stack_size = stack_pool.getStackSize();
                const auto& beg        = range.front();
                const auto& end        = range.back() + 1;

                kernel.launchRange(stack, stack_size, index, invocations, beg, end);
            }
        );
    }

    std::unique_lock<std::mutex> lock(unfinished_guard);

    while (unfinished_count != 0)
        unfinished_cond.wait(lock);
}

} } } } }
