/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cstddef>

#include <vector>

#include <boost/assert.hpp>

#include "framework/processor/detail/StackPool.h"

namespace zillians { namespace framework { namespace processor { namespace mt { namespace detail {

StackPool::StackPool()
    : mStackSize(8192 * 1024)
    , mStacks()
    , mAllocator()
#if(BOOST_VERSION >= 105500)
    , mContexts()
#endif
{
}

StackPool::StackPool(std::size_t concurrency)
    : StackPool() // object concstruction is succeeded if delegated ctor is completed.
                  // That is, dtor will be call even if current ctor is thrown.
                  // This ensure the stack will be released even if exception is thron during stack allocation.
{
    mStacks.reserve(concurrency);
#if(BOOST_VERSION >= 105500)
    mContexts.reserve(concurrency);

    for (; concurrency != 0; --concurrency)
    {
        boost::coroutines::stack_context context;
        mAllocator.allocate(context, mStackSize);
        mContexts.emplace_back(std::move(context));
        mStacks.emplace_back(mContexts.back().sp);
    }
#else
    for (; concurrency != 0; --concurrency)
        mStacks.emplace_back(mAllocator.allocate(mStackSize));
#endif
}

StackPool::~StackPool()
{
#if(BOOST_VERSION >= 105500)
    for (auto& context : mContexts)
        mAllocator.deallocate(context);
#else
    for (auto*const stack : mStacks)
        mAllocator.deallocate(stack, mStackSize);
#endif
}

std::size_t               StackPool::getConcurrency() const noexcept { return mStacks.size(); }
const std::vector<void*>& StackPool::getStacks     () const noexcept { return mStacks       ; }
std::size_t               StackPool::getStackSize  () const noexcept { return mStackSize    ; }

} } } } }
