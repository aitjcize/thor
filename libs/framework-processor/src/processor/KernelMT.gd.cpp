/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

namespace thor { namespace lang {

void __destroyObjectInternal(int, char**);

void __remoteInvoker(char*);

void __initialize();

} }

namespace {

void destroy_objects(char* parameter_ptr)
{
    int    object_count = *reinterpret_cast<const int*>(parameter_ptr                       );
    char** objects_ptr  = *reinterpret_cast<char***   >(parameter_ptr + sizeof(object_count));

    ::thor::lang::__destroyObjectInternal(object_count, objects_ptr);
}

}

void global_dispatch(long long function_id, char* parameter_ptr, char* store_to)
{
    switch (function_id)
    {
    case 0 /* destroy objects */:
        destroy_objects(parameter_ptr);
        break;

    case 1 /* remote invocation */:
        ::thor::lang::__remoteInvoker(parameter_ptr);
        break;

    case 2 /* system initialization */:
        ::thor::lang::__initialize();
        break;

    case 3 /* global initialization */:
        break;
    }
}
