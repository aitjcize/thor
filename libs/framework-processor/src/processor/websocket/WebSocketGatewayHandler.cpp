/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cstdint>
#include <functional>
#include <iostream>
#include <string>
#include <vector>

#include <boost/assert.hpp>
#include <boost/utility/in_place_factory.hpp>

#include <websocketpp/websocketpp.hpp>

#include <msgpack.hpp>

#include "framework/buffer/BufferKernel.h"
#include "framework/processor/websocket/WebSocketGatewayHandler.h"
#include "framework/processor/ProcessorGateway.h"

#include "network/session/TCPSession.h"

namespace zillians { namespace framework { namespace processor {

typedef std::map<std::string, msgpack::object> MsgObjectMap;

namespace {

class WebSocketGatewayHandlerProxy : public websocketpp::server::handler
{
public:
    WebSocketGatewayHandlerProxy(
        std::function<void(connection_ptr             )> do_on_open,
        std::function<void(connection_ptr             )> do_on_close,
        std::function<void(connection_ptr, message_ptr)> do_on_message
    )
        : do_on_open   (do_on_open   )
        , do_on_close  (do_on_close  )
        , do_on_message(do_on_message)
    {
    }

    void on_open   (connection_ptr con                 ) { do_on_open   (con     ); }
    void on_close  (connection_ptr con                 ) { do_on_close  (con     ); }
    void on_message(connection_ptr con, message_ptr msg) { do_on_message(con, msg); }

private:
    std::function<void(connection_ptr             )> do_on_open;
    std::function<void(connection_ptr             )> do_on_close;
    std::function<void(connection_ptr, message_ptr)> do_on_message;
};

void push_args_to_ivk(const std::vector<msgpack::object>& args, service::Invocation* ivk)
{
    size_t num_byte_used = 0;
    for(const auto& v : args)
    {
        std::string type_str;
        bool        bool_v   = false;
        int64       int64_v  = 0;
        double      double_v = 0.0;
        std::string str_v;

        if(v.type == msgpack::type::BOOLEAN)
        {
            type_str = "bool";
            bool_v = v.as<bool>();
        }
        else if(v.type == msgpack::type::RAW)
        {
            BOOST_ASSERT_MSG(0, "String can not(and should not) be processed here");
            //type_str = "string";
            //str_v = v.as<std::string>();
        }
        else
        {
            auto primitive_type_wrapper = v.as<MsgObjectMap>();
            BOOST_ASSERT_MSG(primitive_type_wrapper.size() == 1 || primitive_type_wrapper.size() == 2, "Now only support primitive type");
            const auto& arg_pair = *primitive_type_wrapper.begin();
            type_str = arg_pair.first;
            if(arg_pair.second.type == msgpack::type::DOUBLE)
            {
                double_v = arg_pair.second.as<double>();
            }
            else if(type_str == "int64_hi" || type_str == "int64_lo")
            {
                int64 hi;
                int64 lo;
                if(type_str == "int64_hi")
                {
                    hi = primitive_type_wrapper.begin()->second.as<int>();
                    lo = primitive_type_wrapper.rbegin()->second.as<int>();
                }
                else
                {
                    lo = primitive_type_wrapper.begin()->second.as<int>();
                    hi = primitive_type_wrapper.rbegin()->second.as<int>();
                }
                int64_v = hi << 32 | lo;
                type_str = "int64";
            }
            else
            {
                double_v = arg_pair.second.as<int>();
            }
        }

             if(type_str == "bool"   ) { bool            v =   bool_v; *reinterpret_cast<bool           *>(&ivk->buffer.parameters[num_byte_used]) = v; num_byte_used += 4; }
        //else if(type_str == "string" ) { std::string     v =    str_v; *reinterpret_cast<std::string    *>(&ivk->buffer.parameters[num_byte_used]) = v; num_byte_used += 4; }
        else if(type_str == "int8"   ) { zillians::int8  v = double_v; *reinterpret_cast<zillians::int8 *>(&ivk->buffer.parameters[num_byte_used]) = v; num_byte_used += 4; }
        else if(type_str == "int16"  ) { zillians::int16 v = double_v; *reinterpret_cast<zillians::int16*>(&ivk->buffer.parameters[num_byte_used]) = v; num_byte_used += 4; }
        else if(type_str == "int32"  ) { zillians::int32 v = double_v; *reinterpret_cast<zillians::int32*>(&ivk->buffer.parameters[num_byte_used]) = v; num_byte_used += 4; }
        else if(type_str == "int64"  ) { zillians::int64 v =  int64_v; *reinterpret_cast<zillians::int64*>(&ivk->buffer.parameters[num_byte_used]) = v; num_byte_used += 8; }
        else if(type_str == "float32") { float           v = double_v; *reinterpret_cast<float          *>(&ivk->buffer.parameters[num_byte_used]) = v; num_byte_used += 4; }
        else if(type_str == "float64") { double          v = double_v; *reinterpret_cast<double         *>(&ivk->buffer.parameters[num_byte_used]) = v; num_byte_used += 8; }
        else BOOST_ASSERT_MSG(0, "Now only support primitive type");
    }
}

}

WebSocketGateway::WebSocketGateway(ProcessorGateway* proc_gateway)
    : source(0)
    , id(UUID::nil())
    , server()
    , handler()
    , proc_gateway(proc_gateway)
    , con_domain_map()
{
}

bool WebSocketGateway::is_started() noexcept
{
    return server.is_initialized();
}

bool WebSocketGateway::start(uint32 new_source, const UUID& new_id, std::uint16_t port)
{
    if (server.is_initialized())
        return false;

    auto new_handler = websocketpp::server::handler_ptr(
        new WebSocketGatewayHandlerProxy(
            [this](connection_ptr con                 ) { return on_open   (con     ); },
            [this](connection_ptr con                 ) { return on_close  (con     ); },
            [this](connection_ptr con, message_ptr msg) { return on_message(con, msg); }
        )
    );

    server = boost::in_place(new_handler);
    server->start_listen(port);

    source = new_source;
    id     = new_id    ;

    swap(handler, new_handler);

    return true;
}

bool WebSocketGateway::stop()
{
    if (!server.is_initialized())
        return false;

    server->stop_listen(true);
    handler.reset();
    source = 0;
    id     = UUID::nil();

    return true;
}

void WebSocketGateway::on_open(connection_ptr con)
{
    DomainContext* context = proc_gateway->createDomainContext(NULL, con);

    // create session open itc
    auto itc = KernelITC::make<KernelITC::DomainSignalConnectedRequest>(0, id, context->session_id);

    proc_gateway->sendITC(source, itc);

    con_domain_map[con] = context;
}

void WebSocketGateway::on_close(connection_ptr con)
{
}

void WebSocketGateway::on_message(connection_ptr con, message_ptr msg)
{
    DomainContext* domain_context = con_domain_map[con];

    uint32 local_id = proc_gateway->getId();
    service::InvocationRequestBuffer* buffer = new service::InvocationRequestBuffer(local_id);

    msgpack::unpacked unpacked_msg;
    msgpack::unpack(&unpacked_msg, msg->get_payload().data(), msg->get_payload().size());
    msgpack::object obj = unpacked_msg.get();
    //std::cerr << obj << std::endl;

    MsgObjectMap m = obj.as<MsgObjectMap>();
    //for(const auto& p : m) {
    //    std::cerr << "  " << p.first << " : " << p.second.type << " " << p.second << std::endl;
    //}

    std::string function_name = m["function_name"].as<std::string>();
    std::vector<msgpack::object> args = m["args"].as<std::vector<msgpack::object>>();
    shared_ptr<KernelBase> kernel = proc_gateway->getKernel();

    int64 function_id = kernel->queryFunctionId(function_name);
    if(function_id != -1)
        std::cerr << "Function name " << function_name << " function_id " << function_id << std::endl;
    else
        std::cerr << "[ERROR] Function name '" << function_name << "' not found!" << std::endl;

    int number_of_invocation = 1;
    service::Invocation ivk;
    ivk.function_id = function_id;
    ivk.session_id = domain_context->session_id;
    push_args_to_ivk(args, &ivk);

    BufferNetwork::instance()->create(local_id, *buffer, service::InvocationRequestBuffer::Dimension(number_of_invocation));

    buffer::memcpy_func_table[buffer->location][buffer::buffer_location_t::host_unpinned](
            (void*)buffer->getInvocationRawPtr(0),
            (void*)&ivk,
            number_of_invocation * service::Invocation::PAYLOAD);

    for(int i = 0; i < number_of_invocation; ++i)
        buffer->setSessionId(i, domain_context->session_id);

    buffer->setInvocationCount(number_of_invocation);

    auto itc = KernelITC::make<KernelITC::InvocationRequest>(0, 0, buffer);

    proc_gateway->getSessionITCs().push(itc);
}

} } }
