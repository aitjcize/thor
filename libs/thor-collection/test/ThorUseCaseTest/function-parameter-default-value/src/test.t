/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import other;
import pl;

typedef pl.foo<int16> foo16;
typedef pl.foo<int32> foo32;
typedef pl.foo<int64> foo64;

@entry
task test_main(): void {
    if (other.sum_simple(                ) != 6666) exit(-1);
    if (other.sum_simple(1234            ) != 6666) exit(-2);
    if (other.sum_simple(1234, 4321      ) != 6666) exit(-3);
    if (other.sum_simple(1234, 4321, 1111) != 6666) exit(-4);

    if (other.sum_class(                                                 ) != 6666) exit(-5);
    if (other.sum_class(new foo32(1234)                                  ) != 6666) exit(-6);
    if (other.sum_class(new foo32(1234), new foo64(4321)                 ) != 6666) exit(-7);
    if (other.sum_class(new foo32(1234), new foo64(4321), new foo16(1111)) != 6666) exit(-8);

    if (other.sum_nested(                ) != 6666) exit(-9);
    if (other.sum_nested(1234            ) != 6666) exit(-10);
    if (other.sum_nested(1234, 4321      ) != 6666) exit(-11);
    if (other.sum_nested(1234, 4321, 1111) != 6666) exit(-12);

    if (other.sum_template_1<foo32, foo64, foo16>(                                                 ) != 6666) exit(-13);
    if (other.sum_template_1<foo32, foo64, foo16>(new foo32(1234)                                  ) != 6666) exit(-14);
    if (other.sum_template_1<foo32, foo64, foo16>(new foo32(1234), new foo64(4321)                 ) != 6666) exit(-15);
    if (other.sum_template_1<foo32, foo64, foo16>(new foo32(1234), new foo64(4321), new foo16(1111)) != 6666) exit(-16);

    if (other.sum_template_1<foo32, foo64, foo16>(                ) != 6666) exit(-17);
    if (other.sum_template_1<int32, foo64, foo16>(1234            ) != 6666) exit(-18);
    if (other.sum_template_1<int32, int64, foo16>(1234, 4321      ) != 6666) exit(-19);
    if (other.sum_template_1<int32, int64, int16>(1234, 4321, 1111) != 6666) exit(-20);

    if (other.sum_template_2<int32, int64, int16>(                ) != 6666) exit(-21);
    if (other.sum_template_2<int32, int64, int16>(1234            ) != 6666) exit(-22);
    if (other.sum_template_2<int32, int64, int16>(1234, 4321      ) != 6666) exit(-23);
    if (other.sum_template_2<int32, int64, int16>(1234, 4321, 1111) != 6666) exit(-24);

    if (other.sum_template_2<int32, int64, int16>(                                                 ) != 6666) exit(-17);
    if (other.sum_template_2<foo32, int64, int16>(new foo32(1234)                                  ) != 6666) exit(-18);
    if (other.sum_template_2<foo32, foo64, int16>(new foo32(1234), new foo64(4321)                 ) != 6666) exit(-19);
    if (other.sum_template_2<foo32, foo64, foo16>(new foo32(1234), new foo64(4321), new foo16(1111)) != 6666) exit(-20);

    exit(0);
}
