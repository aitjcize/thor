/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
var expected: int32 = 0;

function get_expected(): int32
{
    return expected;
}

function get_not_expected(): int32
{
    return ~expected;
}

task task_func(cond: int32, value: int32)
{
    switch (cond)
    {
    case 1:

    case 2:
        flow -> {
            value = get_expected();
        }

    case 3:
        exit(0);

        flow -> {
            value = get_not_expected();
        }

    default:
        exit(1);
    }

    if (expected == value)
        exit(0);
    else
        exit(2);
}

function test_impl(cond: int32, value: int32, the_expected: int32)
{
    expected = the_expected;

    async -> task_func(cond, value);
}

@entry task test_main_1() { return test_impl(1, 1234, 1234); }
@entry task test_main_2() { return test_impl(2, 1234, 4321); }
@entry task test_main_3() { return test_impl(3, 1234, 1234); }
