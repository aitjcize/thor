/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= thor.util;

var x : int32 = 0;

@entry
task main() : void
{
    var a : AAA = new BBB;
    a.toString();
    if ( x != 2 ) exit(-1);
    Convert.toString( a ); // Convert.toString is a template function in native
    if ( x != 2 ) exit(-1);

    exit(0);
}

class AAA
{
    public virtual function toString() : String
    {
        x = 1;
        return "AAA\n";
    }
}

class BBB extends AAA
{
    public virtual function toString() : String
    {
        x = 2 ;
        return "BBB\n";
    }
}

