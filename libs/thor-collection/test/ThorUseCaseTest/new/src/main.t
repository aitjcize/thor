/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class Compose<T>
{
   private var value : T;

   public function new( v:T ):void
   { 
      value = v;
   }
}

@entry
task main():void
{
   var complex : Compose<int32> = new Compose<int32>( 3 );

   var complex2 : Compose< Compose<int32> >;
   complex2 = new Compose< Compose<int32> >(
                  new Compose<int32>(
                     0
                  )
              );

   var complex3 : Compose< Compose< Compose<int32> > >;
   complex3 = new Compose< Compose< Compose<int32> > >(
                  new Compose< Compose<int32> >(
                      new Compose<int32>(
                         0
                      )
                  )
              );

   exit(0);
}
