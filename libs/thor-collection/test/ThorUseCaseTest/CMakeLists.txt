#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#
zillians_create_test_subject(SUBJECT thor-compiler-usecase-test)
zillians_add_subject_to_subject(PARENT language-compiler-critical CHILD thor-compiler-usecase-test)

ADD_SUBDIRECTORY(call-method-via-new)
ADD_SUBDIRECTORY(call-method-with-member)
ADD_SUBDIRECTORY(cast-to-base)
ADD_SUBDIRECTORY(class-member-initialize)
ADD_SUBDIRECTORY(Dijkstra)
ADD_SUBDIRECTORY(dynamic-cast)
ADD_SUBDIRECTORY(Fibonacci)
ADD_SUBDIRECTORY(function-argument-type-deduction)
ADD_SUBDIRECTORY(GCD)
ADD_SUBDIRECTORY(inheritance-from-class-template)
ADD_SUBDIRECTORY(isa)
ADD_SUBDIRECTORY(nested-constructor-call)
ADD_SUBDIRECTORY(null-to-class-interface)
ADD_SUBDIRECTORY(pass-this-to-static-method)
ADD_SUBDIRECTORY(Queue)
ADD_SUBDIRECTORY(QueueT)
ADD_SUBDIRECTORY(resolve-function-template-type-argument)
ADD_SUBDIRECTORY(shift-arithmetic)
ADD_SUBDIRECTORY(test1)
ADD_SUBDIRECTORY(test10)
ADD_SUBDIRECTORY(test12)
ADD_SUBDIRECTORY(test2)
ADD_SUBDIRECTORY(test3)
ADD_SUBDIRECTORY(test4)
ADD_SUBDIRECTORY(test5)
ADD_SUBDIRECTORY(test6)
ADD_SUBDIRECTORY(test7)
ADD_SUBDIRECTORY(test8)
ADD_SUBDIRECTORY(test9)
ADD_SUBDIRECTORY(ThreeNplus1)
ADD_SUBDIRECTORY(empty-source)
ADD_SUBDIRECTORY(use-attribute-via-new)
ADD_SUBDIRECTORY(class-template-constructor-parameters)
ADD_SUBDIRECTORY(static-local-variable)
ADD_SUBDIRECTORY(static-member-and-global-variable)
ADD_SUBDIRECTORY(switch-case)
ADD_SUBDIRECTORY(switch-case-return)
ADD_SUBDIRECTORY(global-local-cross-reference)
ADD_SUBDIRECTORY(static-function-member-access)
ADD_SUBDIRECTORY(ternary-inside-ifstmt)
ADD_SUBDIRECTORY(null-with-template)
ADD_SUBDIRECTORY(null-arg)
ADD_SUBDIRECTORY(null-in-ternary)
ADD_SUBDIRECTORY(cast-to-bool)
ADD_SUBDIRECTORY(ternary-within-ternary)
ADD_SUBDIRECTORY(new-template-class)
ADD_SUBDIRECTORY(object-default-null)
ADD_SUBDIRECTORY(multiple-constructors)
ADD_SUBDIRECTORY(template-constructor)
ADD_SUBDIRECTORY(sort)
ADD_SUBDIRECTORY(float-comparison)
ADD_SUBDIRECTORY(lambda)
ADD_SUBDIRECTORY(nested-template-class)
ADD_SUBDIRECTORY(functor)
ADD_SUBDIRECTORY(multiple-assignment)
ADD_SUBDIRECTORY(domain)
ADD_SUBDIRECTORY(string-in-template)
ADD_SUBDIRECTORY(new)
ADD_SUBDIRECTORY(instantiation)
ADD_SUBDIRECTORY(short-circuit)
ADD_SUBDIRECTORY(async)
ADD_SUBDIRECTORY(operator-overload)
ADD_SUBDIRECTORY(foreach)
ADD_SUBDIRECTORY(atomic)
ADD_SUBDIRECTORY(string-format)
ADD_SUBDIRECTORY(non-static-function-access-static-variable)
ADD_SUBDIRECTORY(super-call)
ADD_SUBDIRECTORY(enum)
ADD_SUBDIRECTORY(loop-break)
ADD_SUBDIRECTORY(multiple-package)
ADD_SUBDIRECTORY(after-branch)
ADD_SUBDIRECTORY(convert)
ADD_SUBDIRECTORY(multiple-return-value)
ADD_SUBDIRECTORY(serialize)
ADD_SUBDIRECTORY(function-override)
ADD_SUBDIRECTORY(exit)
ADD_SUBDIRECTORY(flow)
ADD_SUBDIRECTORY(function-parameter-default-value)
ADD_SUBDIRECTORY(resolution-stunk-infinite)
ADD_SUBDIRECTORY(nested-modification)
ADD_SUBDIRECTORY(check-null-thunking-on-return)
ADD_SUBDIRECTORY(instantiate-native-template-function)
