/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= thor.container;
import .= thor.util;

function partition<T>(ar : Vector<T>,beg : int32, end : int32) : int32
{
    var pi = beg;
    var pivot = ar[pi];
    var i = beg+1;

    for(var j = i; j <= end; ++j)
    {   
        if(pivot > ar[j])
        {   
            var t = ar[j];
            ar[j] = ar[i];
            ar[i] = t;
            ++i;
        }   
    }   

    var final_pi = i-1;
    var t = ar[pi];
    ar[pi] = ar[final_pi];
    ar[final_pi] = t;

    return final_pi;
}

function qs_work<T>(ar:Vector<T>, lb:int32, rb:int32) : void
{
    if(lb >= rb) 
    {   
        return;
    }   
    var pidx = partition(ar,lb,rb); 
    qs_work(ar,lb,pidx - 1); 
    qs_work(ar,pidx + 1,rb);
}

function qs<T>(ar : Vector<T>) : void
{
    qs_work(ar,0,cast<int32>(ar.size()-1));
}
