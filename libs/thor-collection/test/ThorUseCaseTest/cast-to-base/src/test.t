/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class A {
    public var a : int32;
}

class B extends A {
    public var b : int32;
}

class C extends B {
    public var c : int32;
	public function main() : int32 {
		this.c = 0;
		this.b = 1;
		this.a = 2;
        if(super.b != 1) return 1;
		super.b = 3;
        if(this.b != 3) return 1;
		cast<B>(this).b = 4;
		cast<A>(this).a = 5;
        return 0;
	}
}

@entry
task main():void
{
    var v : C = new C();
    if(v.main() != 0) exit(1);
    if(v.b != 4) exit(1);
    if(v.a != 5) exit(1);
    exit(0);
}
