/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
// user defined type
class foo { }

// unary function
@native { include="test.h" }
function unary<Type>(a: Type): void;

@native { include="test.h" }
function unary_ret<Type>(a: Type): Type;

// binary function
@native { include="test.h" }
function binary_same<Type>(a: Type, b: Type): void; 

@native { include="test.h" }
function binary_same_ret<Type>(a: Type, b: Type): Type; 

@native { include="test.h" }
function binary_diff<First, Second>(a: First, b: Second): void; 

@native { include="test.h" }
function binary_diff_ret1<First, Second>(a: First, b: Second): First; 

@native { include="test.h" }
function binary_diff_ret2<First, Second>(a: First, b: Second): Second; 

@entry
task test(): void
{
    // unary template function
    unary(new foo);
    unary(1);
   
    unary_ret(new foo);
    unary_ret(1);

    // binary tempalte function
    binary_same(new foo, new foo);
    binary_same(1, 2);

    binary_same_ret(new foo, new foo);
    binary_same_ret(1, 2);

    binary_diff(1, new foo);
    binary_diff_ret1(1, new foo);
    binary_diff_ret2(new foo, 1);

    exit(0);
}
