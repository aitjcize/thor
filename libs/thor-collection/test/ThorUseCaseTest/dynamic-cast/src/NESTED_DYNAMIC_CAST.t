/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function test2_for_base(): int32
{
    var  b_new : base   = new base();
    
    var i1_new : iface1 = b_new;
    var i3_new : iface3 = b_new;
    
         if(cast<base>(             cast<iface1>(i1_new) ) !=  b_new) return -1;
    else if(           cast<iface3>(cast<iface1>(i1_new))  != i3_new) return -2;
    else if(cast<base>(cast<iface3>(cast<iface1>(i1_new))) !=  b_new) return -3;
    else                                                              return  0;
}

function test2_for_derived(): int32
{
    var  d_new : derived = new derived();
    
    var i1_new : iface1 = d_new;
    var i2_new : iface2 = d_new;
    
         if(cast<derived>(cast<iface2>(cast<iface1>(i2_new))) !=  d_new) return -4;
    else if(cast<derived>(             cast<iface1>(i2_new) ) !=  d_new) return -5;
    else if(              cast<iface2>(cast<iface1>(i2_new))  != i2_new) return -6;
    else                                                                 return  0;
}

@entry
task test2(): void
{
    var result: int32 = 0;
    
    result = test2_for_base   (); if(result != 0) exit(result);
    result = test2_for_derived(); if(result != 0) exit(result);
    
    exit(0);
}
