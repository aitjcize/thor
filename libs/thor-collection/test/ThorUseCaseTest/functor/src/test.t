/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class Functor1
{
	public function invoke(x: int32) : int32 { return x; }
	public function invoke(x: float64) : int32 { return 8; }
	public function invoke() : int32 { return 10; }
}

@entry
task test1() : void
{
	var f = new Functor1();

	if (f(10) != 10) exit(-1);
	if (f(1.1) != 8) exit(-2);
	if (f() != 10) exit(-3);

	exit(0);
}

class Functor2
{
	public function invoke(x: Functor1) : int32 
	{
		return x(10) * 2;
	}

	public function invoke() : int32
	{
		return 9;
	}
}

@entry
task test2() : void
{
	var f1 = new Functor1();
	var f2 = new Functor2();

	if (f2(f1) != 20) exit(-1);
	exit(0);
}

class Functor3
{
	public function invoke<X, Y>(x : X, y: Y) : int64
	{
		return x() + y();
	}

	public function invoke<X>(x : X) : int32
	{
		return x;
	}
}

@entry
task test3() : void
{
	var f1 = new Functor1();
	var f2 = new Functor2();
	var f3 = new Functor3();

	if (f3(8) != 8) exit(-1);
	if (f3(f1, f2) != 19) exit(-2);

	exit(0);
}
