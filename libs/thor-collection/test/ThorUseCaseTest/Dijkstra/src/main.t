/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import thor.container;
import p1;

@entry
task main():void
{
        var matrix : thor.container.Vector<int32>;
        var i:int32;
        var j:int32;
        i = 0; 
        j = 0;
        matrix = new thor.container.Vector<int32>();
        var size:int32;
        size = 20; 

        for( i = 0; i < size; ++i)
        {
                for(j = 0; j < size; ++j)
                {
                        matrix.pushBack(j+1);
                }
        }

        p1.Dijkstra(matrix, 5);

        exit(0); 
}


