/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
// https://github.com/zillians/platform_language/issues/1297

interface iface_1
{
    public function get_iface_2(): iface_2;
}

interface iface_2
{
    public function get_iface_1(): iface_1;
}

class derived implements iface_1, iface_2
{
    public override function get_iface_1(): derived
    {   
        return null;
    }   

    public override function get_iface_2(): derived
    {   
        return null;
    }   
}

@entry
task test1()
{
    const d = new derived();

    const i1: iface_1 = d;
    const i2: iface_2 = d;

    const get_iface_1_ok = i2.get_iface_1() == null;
    const get_iface_2_ok = i1.get_iface_2() == null;

    if (get_iface_1_ok && get_iface_2_ok)
        exit(0);
    else if (get_iface_1_ok)
        exit(1);
    else if (get_iface_2_ok)
        exit(2);
    else
        exit(3);
}
