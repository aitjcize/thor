/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= thor.util;

class               VirtualBase { public virtual function toString() : String { return "B" ; } }
class            NonVirtualBase { public         function toString() : String { return "B" ; } }

class D1 extends    VirtualBase { public         function toString() : String { return "D1"; } }
class D2 extends    VirtualBase { public virtual function toString() : String { return "D2"; } }

class D3 extends NonVirtualBase { public         function toString() : String { return "D3"; } }
class D4 extends NonVirtualBase { public virtual function toString() : String { return "D4"; } }

function do_test<T>(t: T, expected: String): bool
{
    const got        : String = t.toString();
    const as_expected: bool   = got.isEqual(expected);

    return as_expected;
}

@entry
task test() : void
{
    var as_expected: bool = false;

    as_expected = do_test(new    VirtualBase, "B" ); if (!as_expected) exit(-1);
    as_expected = do_test(new NonVirtualBase, "B" ); if (!as_expected) exit(-2);
    as_expected = do_test(new           D1  , "D1"); if (!as_expected) exit(-3);
    as_expected = do_test(new           D2  , "D2"); if (!as_expected) exit(-4);
    as_expected = do_test(new           D3  , "D3"); if (!as_expected) exit(-5);
    as_expected = do_test(new           D4  , "D4"); if (!as_expected) exit(-6);

    as_expected = do_test<   VirtualBase>(new D1, "D1"); if (!as_expected) exit(-11);
    as_expected = do_test<   VirtualBase>(new D2, "D2"); if (!as_expected) exit(-12);
    as_expected = do_test<NonVirtualBase>(new D3, "B" ); if (!as_expected) exit(-13);
    as_expected = do_test<NonVirtualBase>(new D4, "B" ); if (!as_expected) exit(-14);

    exit(0);
}
