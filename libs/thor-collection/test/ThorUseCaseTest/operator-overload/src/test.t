/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import thor.util;

class Integer
{
    public var value : int32 = 0;

    public function new( i:int32 ) : void
    {
        value = i;
    }

    public function new( other:Integer ) : void
    {
        value = other.value;    
    }

    public function __add__( other:Integer ) : Integer
    {
        var result : Integer = new Integer( 0 );

        result.value = value + other.value;

        return result;
    }

    public function __inc__() : Integer
    {
        ++value;
        return this;
    }

    public function __ilshift__( i:int32 ) : Integer
    {
        value = value << i;

        return this;
    }
}

@entry
task main() : void
{   
    var lhs : Integer = new Integer( 3 );
    var rhs : Integer = new Integer( 9 );

    if( !((lhs + rhs).value == 12) )
        exit(-1);

    if( !((++lhs + rhs).value == 13) )
        exit(-1);

    ++lhs;

    if( !((++++lhs + rhs).value == 16) )
        exit(-1);

    var result : Integer = ((lhs + rhs) <<= 1) <<= 1;
    if( !(result.value == 64) )
        exit(-1);
 
    if( !( (""+1234).isEqual( thor.util.Convert.toString(1234)) ) )
        exit(-1);

    if( !( ("hello"+" world").isEqual("hello world")  ) )
        exit(-1);

    if( !( ("I have "+1.5+" apples and "+3+" oranges").isEqual("I have 1.5 apples and 3 oranges") ) )
        exit(-1);

    exit(0);
}
