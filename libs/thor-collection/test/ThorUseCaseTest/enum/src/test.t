/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
enum Fruit
{
    APPLE,
    ORANGE
}

class Dummy
{
    public var eat_fruit : Fruit = Fruit.ORANGE;
}

@entry
task test1() : void
{
    var d = new Dummy();
    if (d.eat_fruit != Fruit.ORANGE) exit(-1);

    exit(0);
}

@entry
task test2() : void
{
    var d = new Dummy();
    var v = cast<int32>(d.eat_fruit);

    if (v != 1) exit(-1);

    exit(0);
}


// cross reference
// A   B    C D→→┐
//  ↘  ↗↘  ↗↘ ↑  ↓
//   ↘↗  ↘↗  ↘↑  ↓
// A  B   C   D  ↓
// ↑             ↓
// └←←←←←←←←←←←←←┘
enum CrossRef1
{
    A = cast<int32>(CrossRef2.B),
    B = cast<int32>(CrossRef2.C),
    C = cast<int32>(CrossRef2.D),
    D = cast<int32>(CrossRef2.A)
}

enum CrossRef2
{
    A = 1,
    B = cast<int32>(CrossRef1.B),
    C = cast<int32>(CrossRef1.C),
    D = cast<int32>(CrossRef1.D)
}

function verify_cross_reference(): bool
{
    return cast<int32>(CrossRef1.A)    == 1
           && cast<int32>(CrossRef1.B) == 1
           && cast<int32>(CrossRef1.C) == 1
           && cast<int32>(CrossRef1.D) == 1

           && cast<int32>(CrossRef2.A) == 1
           && cast<int32>(CrossRef2.B) == 1
           && cast<int32>(CrossRef2.C) == 1
           && cast<int32>(CrossRef2.D) == 1;
}

enum MissVal
{
    A = cast<int32>(CrossRef1.A),
    B,
    C = 3
}

function verify_missing_value(): bool
{
    return cast<int32>(MissVal.A)    == 1
           && cast<int32>(MissVal.B) == 2
           && cast<int32>(MissVal.C) == 3;
}

enum Compute
{
    A = cast<int32>(MissVal.A) + cast<int32>(Compute.B) + 15,
    B = (cast<int32>(CrossRef1.A) == 1 ? 30 : 50),
    C = cast<int32>(cast<int32>(Compute.A) == 50),
    D = !cast<int32>(Compute.C)
}

function verify_compute(): bool
{
    return cast<int32>(Compute.A)    == 46
           && cast<int32>(Compute.B) == 30
           && cast<int32>(Compute.C) == 0
           && cast<int32>(Compute.D) == 1;
}

@entry
task test3(): void
{
    if(!verify_cross_reference())
        exit(1);

    if(!verify_missing_value())
        exit(2);

    if(!verify_compute())
        exit(3);

    exit(0);
}

