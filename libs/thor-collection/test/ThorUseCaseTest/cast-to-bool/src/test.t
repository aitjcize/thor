/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function checki8() : bool
{
	var pi : int8 = 2;
	var check1 : bool = pi;

	var ni : int8 = -2;
	var check2 : bool = ni;

	return (check1 && check2);
}

function checki16() : bool
{
	var pi : int16 = 330;
	var check1 : bool = pi;

	var ni : int16 = -330;
	var check2 : bool = ni;

	return (check1 && check2);
}

function checki32() : bool
{
	var pi : int32 = 1024;
	var check1 : bool = pi;

	var ni : int32 = -1024;
	var check2 : bool = ni;

	return (check1 && check2);
}

function checki64() : bool
{
	var pi : int64 = 1024;
	var check1 : bool = pi;

	var ni : int64 = -1024;
	var check2 : bool = ni;

	return (check1 && check2);
}

function checkf32() : bool
{
	var pf : float32 = 1.1112;
	var check1 : bool = pf;

	var nf : float32 = -1.1112;
	var check2 : bool = nf;

	return (check1 && check2);
}

function checkf64() : bool
{
	var pf : float64 = 4481.1112;
	var check1 : bool = pf;

	var nf : float64 = -9918882.132;
	var check2 : bool = nf;

	return (check1 && check2);
}

function check_logical_expr() : bool
{
    var pi : float64 = 3.14;
    if((pi && 123) == false) return false;
    if((pi && 0  ) == true ) return false;
    if((pi || 0  ) == false) return false;
    if(( !pi     ) == true ) return false;
    if((!!pi     ) == false) return false;
    return true;
}

@entry
task test1() : void
{
	if (checki8() == false) exit(-1);
	if (checki16() == false) exit(-1);
	if (checki32() == false) exit(-1);
	if (checki64() == false) exit(-1);
	if (checkf32() == false) exit(-1);
	if (checkf64() == false) exit(-1);
	if (check_logical_expr() == false) exit(-1);
	exit(0);
}
