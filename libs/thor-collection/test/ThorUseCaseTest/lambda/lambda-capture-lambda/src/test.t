/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class Dummy
{
	public function check() : int32
	{
		var f1 = lambda(x: int32) : int32
		{
			return x;
		};
	
		var f2 = lambda(y: int32) : int32
		{
			return f1(8) + y;
		};
	
		if (f2(10) != 18) return -1;
		return 0;
	}

	public function check2() : int32
	{
		var f = lambda(x : int32) : int32
		{
			return internal_lambda(x);
		};

		if (f(8) != 18) return -1;
		return 0;
	}

	public var internal_lambda : lambda(int32) : int32;
}


@entry
task test1() : void
{
	var f1 = lambda(x: int32) : int32
	{
		return x;
	};

	var f2 = lambda(y: int32) : int32
	{
		return f1(8) + y;
	};

	if (f2(10) != 18) exit(-1);
	exit(0);
}

@entry
task test2() : void
{
	var d = new Dummy();
	if (d.check() != 0) exit(-1);

	d.internal_lambda = lambda(x : int32) : int32
	{
		return x + 10;
	};

	if (d.check2() != 0) exit(-1);
	exit(0);
}
