/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@native
function assertion(e : int32) : void;

//////////////////////////
// For test 1
class Check
{
    public function new() : void {
        value = 0;
    }
    public var value : int32;
}

class A extends B
{
    function delete() : void
    {
        obj2.value = 2012;
    }
    public function setObj2(o: Check) : void
    {
        obj2 = o;
    }
    public var obj2 : Check;
}

class B 
{
    function delete() : void 
    {
        obj.value = 1982;
    }
    function setObj(o : Check) : void
    {
        obj = o;
    }
    public var obj : Check;
}

// For test2
class C extends D
{
    function delete() : void
    {
        obj2.value = 2012;
    }
    public virtual function setObj2(o: Check) : void
    {
        obj2 = o;
    }
    public var obj2 : Check;
}

class D 
{
    virtual function delete() : void 
    {
        obj.value = 1982;
    }
    function setObj(o : Check) : void
    {
        obj = o;
    }
    public var obj : Check;
}

@native
@entry
task test1() : int32;

@native
@entry
task test2() : int32;
