/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#include <cinttypes>
#include <iostream>
#include "thor/lang/Language.h"

using namespace thor::lang;

////////////////////////
// Utility
void assertion(std::int32_t error)
{
    std::cerr << "Error code: " << error << std::endl;
    throw error;
}

////////////////////////
// Class Declaration
class Check : public Object
{
public:
    Check();
    ~Check();
    std::int32_t value;
};

class B : public Object
{
public:
    B();
    ~B();
    void setObj(Check* o);
    Check* obj;
};

class A : public B
{
public:
    A();
    ~A();
    void setObj2(Check* o);
    Check* obj2;
};

class D : public Object
{
public:
    D();
    virtual ~D();
    void setObj(Check* o);
    Check* obj;
};

class C : public D
{
public:
    C();
    ~C();
    void setObj2(Check* o);
    Check* obj2;
};

////////////////////////
// Testing
std::int32_t test1()
{
    A* a = new A();
    Check* obj = new Check();
    Check* obj2 = new Check();
    a->setObj(obj);
    a->setObj2(obj2);
    delete a;

    if (obj->value != 1982) assertion(1);
    if (obj2->value != 2012) assertion(2);
    delete obj;
    delete obj2;

	exit(0);
	return -1;
}

std::int32_t test2()
{
    C* c = new C();
    Check* obj = new Check();
    Check* obj2 = new Check();
    c->setObj(obj);
    c->setObj2(obj2);

    D* d = static_cast<D*>(c);
    delete d;

    if (obj->value != 1982) assertion(1);
    if (obj2->value != 2012) assertion(2);
    delete obj;
    delete obj2;

	exit(0);
	return -1;
}
