/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@native
function assertion(e : int32) : void;

//////////////////////////
// For test 1
class A extends B implements C
{
    function new() : void
    {
        super(20001);
        va = 32;
    }
    public var va : int32;
}

class B 
{   
    function new() : void
    {
        assertion(1);
    }
    function new(v : int32) : void 
    {
        vb = v;
    }
    public var vb : int32;
}

interface C
{
    function dummy() : void;
}

@native
@entry
task test1() : int32;

@native
@entry
task test2() : int32;
