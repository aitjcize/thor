/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@native
function assertion(e : int32) : void;

//////////////////////////
// For test 1
class A 
{
    public function foo(v : int32) : int32 
    {
        return v;
    }

    public function double_value(v : int32) : int32
    {
        return foo(v)*2;
    }
}

/////////////////////////
// For test2
class B 
{
    public function barImpl() : int32
    {
        return 123;
    }

    public function bar() : int32
    {
        return barImpl();
    }
    public var v : int32;
}

interface I
{
    public function boo() : void;
}


class Test2 extends B implements I
{
    // The purpose to inherit interface I is to make B with offset in Test2 object layout
    public function boo() : void {} 
}


/////////////////////////
// For Test3 -- a more complicated one
class T3B
{
    public function set(v : int32) : void 
    {
        t3b = v;
    }

    public function get() : int32
    {
        return t3b;
    }

    public var t3b : int32;
}

class T3F extends T3B
{
    public var t3f : int32;
}

interface T3I2
{
    public function i2() : void;
}

interface T3I3
{
    public function i3() : void;
}

interface T3I1 extends T3I2, T3I3
{
    public function i1() : void;
}

class T3D extends T3F implements T3I1
{
    public var t3d : int32;
}

////////////////////////
// For Test4
class Test4SuperBase
{   
    public function set(a : int32) : void
    {
        v = a;
    }
    public var v: int32;
}

class Test4Base extends Test4SuperBase
{
    public virtual function dummy() : void {}
    public var vv: int32;
}

class Test4 extends Test4Base
{
    // Well, Test4 and Test4Base will point to the same offset in object layout, while Test4SuperBase points to the next position
    public var vvv: int32;
}


////////////////////////
// For Test5 -- Test super
interface Test5Dummy
{
    // The purpose of this interface to add a vptr, to make
    // none-virtual base has offset
    public function dummy() : void;
}

class Test5Base
{
    public function new() : void { base_value = 1982; }
    public function f(x : int32) : int32
    {
        return x * 2 + base_value;
    }
    public var base_value : int32;
}

class Test5Derive extends Test5Base implements Test5Dummy
{
    public function new() : void { derive_value = 2012; }
    public function f(x : int32) : int32
    {
        return x + derive_value;
    }

    public function test1(x: int32) : int32
    {
        // should return from Test5Base's f()
        return super.f(x);
    }

    public function test2(x: int32) : int32
    {
        // should return my own
        return f(x);
    }
    public var derive_value : int32;
}


////////////////////////
// For Test6 -- Test overloading
interface Test6Dummy
{
    // The purpose of this interface to add a vptr, to make
    // none-virtual base has offset
    public function dummy() : void;
}

class Test6Base
{
    public function new() : void { base_value = 1982; }
    public function f(x : int32) : int32
    {
        return x * 2 + base_value;
    }
    public function g(x : int32) : int32
    {
        return x * 3 + base_value;
    }
    public var base_value : int32;
}

class Test6Derive extends Test6Base implements Test6Dummy
{
    public function new() : void { derive_value = 2012; }
    public function f(x : int32) : int32
    {
        return x + derive_value;
    }

    public function test1(x: int32) : int32
    {
        return g(x);
    }

    public function test2(x: int32) : int32
    {
        // should return my own
        return f(x);
    }
    public var derive_value : int32;
}


////////////////////////
// For Test7 -- Test thunk function
interface Test7Interface
{
	public function inc() : void;
}

class Test7SuperBase
{
	public virtual function object_function() : void {}
}

class Test7Base extends Test7SuperBase implements Test7Interface
{
	public virtual function inc() : void { ++value; }
	public var value : int32 = 1;
}

class Test7Derive extends Test7Base
{
}


/////////////////////////
// Test functions
function thor_test1(a : A) : void
{
    if ( a.foo(2) != 2 )
    {
        assertion(1);
    }

    if ( a.double_value(4) != 8 )
    {
        assertion(2);
    }
}

function thor_test2(a : Test2, b : B) : void
{
    // Test if we could have the correct this pointer adjustment
    if ( a.bar() != 123 ) assertion(1);
    if ( b.bar() != 123 ) assertion(2);
}

function thor_test3(d: T3D, f: T3F, b: T3B) : void
{
    d.set(2147483647);
    if (d.get() != 2147483647) assertion(1);

    f.set(2147483640);
    if (f.get() != 2147483640) assertion(2);

    b.set(2147483631);
    if (b.get() != 2147483631) assertion(3);
}

function thor_test4(d: Test4) : void
{
    if (d.v != 123) assertion(1);
    d.set(456);
}

function thor_test5(a: Test5Derive) : void
{
    if (a.test1(20) != 2022) assertion(1);
    if (a.test2(20) != 2032) assertion(2);
}

function thor_test6(a: Test6Derive) : void
{
    if (a.test1(20) != 2042) assertion(1);
    if (a.test2(20) != 2032) assertion(2);
}

function thor_test7(d: Test7Derive, b: Test7Base) : void
{
	if (d.value != 1) assertion(1);
	if (b.value != 1) assertion(2);

	d.inc();
	b.inc();

	if (d.value != 3) assertion(3);
	if (b.value != 3) assertion(4);
}

@native
@entry
task test1() : int32;

@native
@entry
task test2() : int32;

@native
@entry
task test3() : int32;

@native
@entry
task test4() : int32;

@native
@entry
task test5() : int32;

@native
@entry
task test6() : int32;

@native
@entry
task test7() : int32;
