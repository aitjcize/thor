/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#include <cinttypes>
#include <iostream>
#include "thor/lang/Language.h"
using namespace thor::lang;

////////////////////////
// Utility
void assertion(std::int32_t error)
{
    std::cerr << "Error code: " << error << std::endl;
    throw error;
}

////////////////////////
// Class Declaration
class A : public Object
{
public:
    A();
    std::int32_t foo(std::int32_t v);
    std::int32_t double_value(std::int32_t v);
};

class B : public Object
{
public:
    B();
    std::int32_t barImpl();
    std::int32_t bar();
    std::int32_t v;
};

class I
{
public:
    virtual void boo();
};

class Test2 : public B, public I
{
    void boo();
};

class T3B : public Object
{
public: 
    T3B();
    void set(std::int32_t v);
    std::int32_t get();
    std::int32_t t3b;
};

class T3F : public T3B
{
public:
    T3F();
    std::int32_t t3f;
};

class T3I2
{
public:
    T3I2();
    virtual void i2();
};

class T3I3
{
public:
    T3I3();
    virtual void i3();
};

class T3I1 : public T3I2, public T3I3
{
public:
    T3I1();
    virtual void i1();
};

class T3D : public T3F, public T3I1
{
public:
    T3D();
    std::int32_t t3d;
};


class Test4SuperBase : public Object
{
public:
    Test4SuperBase();
    void set(std::int32_t a);
    std::int32_t v;
};

class Test4Base : public Test4SuperBase
{
public:
    Test4Base();
    virtual void dummy();
    std::int32_t vv;
};

class Test4 : public Test4Base
{
public:
    Test4();
    std::int32_t vvv;
};


class Test5Dummy
{
public:
    Test5Dummy();
    virtual void dummy();
};

class Test5Base : public Object
{
public:
    Test5Base();
    std::int32_t f(std::int32_t x);
    
    std::int32_t base_value;
};

class Test5Derive : public Test5Base, public Test5Dummy
{
public:
    Test5Derive();
    std::int32_t f(std::int32_t x);
    std::int32_t test1(std::int32_t x);
    std::int32_t test2(std::int32_t x);

    std::int32_t derive_value;
};

class Test6Dummy
{
public:
    Test6Dummy();
    virtual void dummy();
};

class Test6Base : public Object
{
public:
    Test6Base();
    std::int32_t f(std::int32_t x);
    std::int32_t g(std::int32_t x);
    
    std::int32_t base_value;
};

class Test6Derive : public Test6Base, public Test6Dummy
{
public:
    Test6Derive();
    std::int32_t f(std::int32_t x);
    std::int32_t test1(std::int32_t x);
    std::int32_t test2(std::int32_t x);

    std::int32_t derive_value;
};

class Test7Interface
{
public:
	Test7Interface();
	virtual void inc() = 0;
};

class Test7SuperBase : public Object
{
public:
	Test7SuperBase();
	virtual void object_function() {}
};

class Test7Base: public Test7SuperBase, public Test7Interface
{
public:
	Test7Base();
	virtual void inc();
	std::int32_t value;
};

class Test7Derive : public Test7Base
{
};

////////////////////////
// Testing
extern void thor_test1(A* a);
std::int32_t test1()
{
    A* a = new A();
    thor_test1(a);

    delete a;
	exit(0);
	return -1;
}

extern void thor_test2(Test2* a, B* b);
std::int32_t test2()
{
    Test2* a = new Test2();
    B* b = (B*) a;
    thor_test2(a, b);

    if ( a->bar() != 123) assertion(3);
    if ( b->bar() != 123) assertion(4);

    delete a;
	exit(0);
	return -1;
}

extern void thor_test3(T3D*, T3F*, T3B*);
std::int32_t test3()
{
    T3D* d = new T3D();
    T3F* f = (T3F*) d;
    T3B* b = (T3B*) d;
    thor_test3(d, f, b);

    d->set(2047483647);
    if (d->get() != 2047483647) assertion(4);

    f->set(2047483640);
    if (f->get() != 2047483640) assertion(5);

    b->set(2047483631);
    if (b->get() != 2047483631) assertion(6);

    delete d;
	exit(0);
	return -1;
}

extern void thor_test4(Test4*);
std::int32_t test4()
{
    Test4* a = new Test4();
    a->set(123);
    thor_test4(a);
    if ( a->v != 456) assertion(2);
    delete a;
	exit(0);
	return -1;
}

extern void thor_test5(Test5Derive*);
std::int32_t test5()
{
    Test5Derive* a = new Test5Derive();
    thor_test5(a);

    if (a->test1(10) != 2002) assertion(3);
    if (a->test2(10) != 2022) assertion(4);

    delete a;
	exit(0);
	return -1;
}

extern void thor_test6(Test6Derive*);
std::int32_t test6()
{
    Test6Derive* a = new Test6Derive();
    thor_test6(a);

    if (a->test1(10) != 2012) assertion(3);
    if (a->test2(10) != 2022) assertion(4);

    delete a;
	exit(0);
	return -1;
}

extern void thor_test7(Test7Derive*, Test7Base*);
std::int32_t test7()
{
    Test7Derive* d = new Test7Derive();
	Test7Base* b = d;
    thor_test7(d, b);

	if (d->value != 3) assertion(5);
	if (b->value != 3) assertion(6);

	d->inc();
	b->inc();

	if (d->value != 5) assertion(7);
	if (b->value != 5) assertion(8);

    delete d;
	exit(0);
	return -1;
}
