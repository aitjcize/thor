/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= thor.container;
import .= thor.gc;

class Foo
{
    public var x : int32 = 123;
}

var count_down : int32 = 100000; // make sure the test will be terminated

task wait()
{
    var x = __getActiveObjects();
    var s = x.size();

    // If all are cleaned up, there sould be only Domain and the Vector return by __getActiveObjects.
    if(x.size() <= 2)  
        exit(0);

    --count_down;

    if(count_down == 0)
        exit(-1);

    async->wait();
}

@entry
task test()
{
    var vec = new Vector<Foo>();
    for(var i : int32 = 0; i < 100000; ++i)
    {
        vec.pushBack(new Foo);
    }

    vec = null;

    async->wait();
}
