/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import . = thor.lang;
import . = thor.container;
import . = thor.gc;

typedef Array<int8> Bytes;

var gTotalBytes: int64 = 100;
var gTotalBytes1: int64 = 200;

class Result
{
    public function print(): void
    {
        thor.lang.print("GC result ---------------------\n");
        thor.lang.print("c0_exist: \{c0_exist}\n");
        thor.lang.print("c1_exist: \{c1_exist}\n");
        thor.lang.print("--------------------- GC result\n");
    }

    public var c0_exist: bool = false;
    public var c1_exist: bool = false;
}

function check_result() : Result
{
    return check_result(__getActiveObjects());
}

function check_result(objs: Vector<Object>) : Result
{
    var r = new Result();

    for (var i = 0; i < objs.size(); ++i)
    {
        var o = cast<Bytes>(objs.get(i));
        if (o != null)
        {
            //c0
            if (o.size() == gTotalBytes)
            {   
                var match = true;
                for (var j = 0; j < gTotalBytes; ++j)
                {
                    if ((j % 128) != o[j]) match = false;
                }
                r.c0_exist = match;
            }
            //c1
            if (o.size() == gTotalBytes1)
            {
                var match = true;
                for (var j = 0; j < gTotalBytes1; ++j)
                {
                    if ((j % 128) != o[j]) match = false;
                }
                r.c1_exist = match;
            }
        }
    }

    r.print();

    return r;
}

function create_object(): bool
{
    var c1 = new Bytes(gTotalBytes1);

    for (var i = 0; i < gTotalBytes1; ++i)
    {
        c1[i] = i % 128;
    }

    var r = check_result();
    return (r.c0_exist && r.c1_exist);
}

var c0: Bytes = null;

@entry
task test1() : void
{
    c0 = new Bytes(gTotalBytes);

    for (var i = 0; i < gTotalBytes; ++i)
    {
        c0[i] = i % 128;
    }

    if (!create_object()) exit(1);

    __waitGC(
        lambda(objs: Vector<Object>): void
        {
            var r = check_result(objs);
            if (r.c0_exist && !r.c1_exist)
                exit(0);
            else
                exit(2);
        }
    );
}
