/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import . = thor.container;
import . = thor.lang;
import . = thor.gc;

var c0_base : int32 = 0;
var c1_base : int32 = 10;
var c2_base : int32 = 30;
var upper_bound : int32 = 60;

class A 
{
    public function new(id : int32): void
    {
        magic = id;
    }

	public function isLessThan(a : A) : bool 
	{
		return this.magic < a.magic;
	}

    public var magic: int32;
}

class Result
{
    public function print(): void
    {
        thor.lang.print("GC result ---------------------\n");
        thor.lang.print("   c0_map_exist: \{   c0_map_exist}\n");
        thor.lang.print("   c1_map_exist: \{   c1_map_exist}\n");
        thor.lang.print("   c2_map_exist: \{   c2_map_exist}\n");
        thor.lang.print("obj_in_c0_exist: \{obj_in_c0_exist}\n");
        thor.lang.print("obj_in_c1_exist: \{obj_in_c1_exist}\n");
        thor.lang.print("obj_in_c2_exist: \{obj_in_c2_exist}\n");
        thor.lang.print("--------------------- GC result\n");
    }

    public var    c0_map_exist: bool = false;
    public var    c1_map_exist: bool = false;
    public var    c2_map_exist: bool = false;
    public var obj_in_c0_exist: bool = false;
    public var obj_in_c1_exist: bool = false;
    public var obj_in_c2_exist: bool = false;
}

function check_result(): Result
{
    return check_result(__getActiveObjects());
}

function check_result(objs: Vector<Object>): Result
{
    var r = new Result();

    var obj_in_c0_count = 0;
    var obj_in_c1_count = 0;
    var obj_in_c2_count = 0;
    var unexpect_A_appear = false;
    for (var i = 0; i < objs.size(); ++i)
    {
        // c0
        if (isa<Map<A, int32> >(objs.get(i)))
        {
            var o = cast<Map<A, int32> >(objs.get(i));
            if (o.size() == c1_base - c0_base)
            {
                var match = true;
                var iter = o.iterator();
                do
                {
                    var entry = iter.next();
                    if (entry.key.magic != entry.value) match = false;

                } while (iter.hasNext());

                if (match) r.c0_map_exist = true;
            }
        }
        // c1
        else if (isa<Map<int32, A> >(objs.get(i)) )
        {
            var o = cast<Map<int32, A> >(objs.get(i));
            if (o.size() == c2_base - c1_base)
            {
                var match = true;
                var iter = o.iterator();
                do
                {
                    var entry = iter.next();
                    if (entry.key != entry.value.magic) match = false;

                } while (iter.hasNext());

                if (match) r.c1_map_exist = true;
            }
        }
        // c2
        else if(isa<Map<A, A> >(objs.get(i)))
        {
            var o = cast<Map<A, A> >(objs.get(i));
            if (o.size() == upper_bound - c2_base)
            {
                var match = true;
                var iter = o.iterator();
                do
                {
                    var entry = iter.next();
                    if (entry.key.magic != entry.value.magic) match = false;

                } while (iter.hasNext());

                if (match) r.c2_map_exist = true;
            }
        }
        // A
        else if (isa<A>(objs.get(i)))
        {
            var o = cast<A>(objs.get(i));
            if (o.magic >= c0_base && o.magic < c1_base)
                ++obj_in_c0_count;
            else if(o.magic >= c1_base && o.magic < c2_base)
                ++obj_in_c1_count;
            else if(o.magic >= c2_base && o.magic < upper_bound)
                ++obj_in_c2_count;
            else
                unexpect_A_appear = true;
        }
    }

    if (unexpect_A_appear == false)
    {
        if (obj_in_c0_count == c1_base - c0_base) r.obj_in_c0_exist = true;       
        if (obj_in_c1_count == c2_base - c1_base) r.obj_in_c1_exist = true;
        if (obj_in_c2_count == (upper_bound - c2_base)*2) r.obj_in_c2_exist = true;
    }

    r.print();

    return r;
}

function create_objects(): bool
{
    var c1 = new Map<int32, A>();
    var c2 = new Map<A, A>();

    for (var i = c1_base; i < c2_base; ++i)
    {
        c1.set(i, new A(i));
    }

    for (var i = c2_base; i < upper_bound; ++i)
    {
        c2.set(new A(i), new A(i));
    }

    var r = check_result();
    return (r.c0_map_exist && r.c1_map_exist && r.c2_map_exist &&
            r.obj_in_c0_exist && r.obj_in_c1_exist && r.obj_in_c2_exist);
}

var c0: Map<A, int32> = null;

@entry
task test1() : void
{
	c0 = new Map<A, int32>();

	for (var i = c0_base; i < c1_base; ++i)
	{
		c0.set(new A(i), i);
	}

    if (!create_objects()) exit(1);

    __waitGC(
        lambda(objs: Vector<Object>): void
        {
            var r = check_result(objs);

            if (r.c0_map_exist && r.obj_in_c0_exist && 
                !r.c1_map_exist && !r.obj_in_c1_exist &&
                !r.c2_map_exist && !r.obj_in_c2_exist) 
                exit(0);
            else
                exit(2);
        }
    );
}
