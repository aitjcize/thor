/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import import_test;
import import_test.ns;

function test_bools(): int32 {
    var ori_class  = new import_test.ns.ori_class ();
    var ori_struct = new import_test   .ori_struct();

    ori_class .set_b(true );
    ori_struct.set_b(false);

    if(!ori_class .get_b()) return -1;
    if( ori_struct.get_b()) return -2;

    return 0;
}

function test_integers(): int32 {
    var ori_class  = new import_test.ns.ori_class ();
    var ori_struct = new import_test   .ori_struct();

    ori_class .set_i8 (0);
    ori_class .set_i16(1);
    ori_class .set_i32(2);
    ori_class .set_i64(3);
    ori_struct.set_i8 (4);
    ori_struct.set_i16(5);
    ori_struct.set_i32(6);
    ori_struct.set_i64(7);

    if(ori_class .get_i8 () != 0) return -11;
    if(ori_class .get_i16() != 1) return -12;
    if(ori_class .get_i32() != 2) return -13;
    if(ori_class .get_i64() != 3) return -14;
    if(ori_struct.get_i8 () != 4) return -15;
    if(ori_struct.get_i16() != 5) return -16;
    if(ori_struct.get_i32() != 6) return -17;
    if(ori_struct.get_i64() != 7) return -18;

    return 0;
}

function test_floats(): int32 {
    var ori_class  = new import_test.ns.ori_class ();
    var ori_struct = new import_test   .ori_struct();

    ori_class .set_f32(1.0);
    ori_class .set_f64(2.0);
    ori_struct.set_f32(4.0);
    ori_struct.set_f64(8.0);

    if(ori_class .get_f32() != 1.0) return -20;
    if(ori_class .get_f64() != 2.0) return -21;
    if(ori_struct.get_f32() != 4.0) return -22;
    if(ori_struct.get_f64() != 8.0) return -23;

    return 0;
}

@entry
function test_main(): int32 {
    var result: int32 = 0;

    result = test_bools   (); if(result != 0) return result;
    result = test_integers(); if(result != 0) return result;
    result = test_floats  (); if(result != 0) return result;

    return 0;
}

