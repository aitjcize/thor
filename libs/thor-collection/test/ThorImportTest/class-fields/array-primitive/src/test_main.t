/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import import_test;
import import_test.ns;

function test_bools(error_base: int32): int32 {
    var ori_class  = new import_test.ns.ori_class ();
    var ori_struct = new import_test   .ori_struct();

    if(ori_class .get_b_size() != 1) return error_base - 1;
    if(ori_struct.get_b_size() != 1) return error_base - 2;
    
    ori_class .set_b(true , 0);
    ori_struct.set_b(false, 0);

    if(!ori_class .get_b(0)) return error_base - 3;
    if( ori_struct.get_b(0)) return error_base - 4;

    return 0;
}

function test_integers(error_base: int32): int32 {
    var ori_class  = new import_test.ns.ori_class ();
    var ori_struct = new import_test   .ori_struct();

    if(ori_class .get_i8_size()  != 2) return error_base - 1;
    if(ori_class .get_i16_size() != 3) return error_base - 2;
    if(ori_class .get_i32_size() != 4) return error_base - 3;
    if(ori_class .get_i64_size() != 5) return error_base - 4;
    if(ori_struct.get_i8_size()  != 2) return error_base - 5;
    if(ori_struct.get_i16_size() != 3) return error_base - 6;
    if(ori_struct.get_i32_size() != 4) return error_base - 7;
    if(ori_struct.get_i64_size() != 5) return error_base - 8;

    for(var i = 0; i < ori_class .get_i8_size() ; ++i) { ori_class .set_i8 (cast<int8>(i), i); if(ori_class .get_i8 (i) != i) return error_base -  9;}
    for(var i = 0; i < ori_class .get_i16_size(); ++i) { ori_class .set_i16(cast<int8>(i), i); if(ori_class .get_i16(i) != i) return error_base - 10;}
    for(var i = 0; i < ori_class .get_i32_size(); ++i) { ori_class .set_i32(cast<int8>(i), i); if(ori_class .get_i32(i) != i) return error_base - 11;}
    for(var i = 0; i < ori_class .get_i64_size(); ++i) { ori_class .set_i64(cast<int8>(i), i); if(ori_class .get_i64(i) != i) return error_base - 12;}
    for(var i = 0; i < ori_struct.get_i8_size() ; ++i) { ori_struct.set_i8 (cast<int8>(i), i); if(ori_struct.get_i8 (i) != i) return error_base - 13;}
    for(var i = 0; i < ori_struct.get_i16_size(); ++i) { ori_struct.set_i16(cast<int8>(i), i); if(ori_struct.get_i16(i) != i) return error_base - 14;}
    for(var i = 0; i < ori_struct.get_i32_size(); ++i) { ori_struct.set_i32(cast<int8>(i), i); if(ori_struct.get_i32(i) != i) return error_base - 15;}
    for(var i = 0; i < ori_struct.get_i64_size(); ++i) { ori_struct.set_i64(cast<int8>(i), i); if(ori_struct.get_i64(i) != i) return error_base - 16;}

    return 0;
}

function test_floats(error_base: int32): int32 {
    var ori_class  = new import_test.ns.ori_class ();
    var ori_struct = new import_test   .ori_struct();

    if(ori_class .get_f32_size() != 6) return error_base - 1;
    if(ori_class .get_f64_size() != 7) return error_base - 2;
    if(ori_struct.get_f32_size() != 6) return error_base - 3;
    if(ori_struct.get_f64_size() != 7) return error_base - 4;

    var test_f32: float32 = 1.0;
    var test_f64: float64 = 1.0;

    for(var i = 0; i < ori_class .get_f32_size(); ++i) { ori_class .set_f32(test_f32, i); if(ori_class .get_f32(i) != test_f32) return error_base - 5; test_f32 *= 2.0;}
    for(var i = 0; i < ori_class .get_f64_size(); ++i) { ori_class .set_f64(test_f64, i); if(ori_class .get_f64(i) != test_f64) return error_base - 6; test_f64 *= 2.0;}
    for(var i = 0; i < ori_struct.get_f32_size(); ++i) { ori_struct.set_f32(test_f32, i); if(ori_struct.get_f32(i) != test_f32) return error_base - 7; test_f32 *= 2.0;}
    for(var i = 0; i < ori_struct.get_f64_size(); ++i) { ori_struct.set_f64(test_f64, i); if(ori_struct.get_f64(i) != test_f64) return error_base - 8; test_f64 *= 2.0;}

    return 0;
}

@entry
function test_main(): int32 {
    var result: int32 = 0;

    result = test_bools   (-20); if(result != 0) return result;
    result = test_integers(-40); if(result != 0) return result;
    result = test_floats  (-60); if(result != 0) return result;

    return 0;
}

