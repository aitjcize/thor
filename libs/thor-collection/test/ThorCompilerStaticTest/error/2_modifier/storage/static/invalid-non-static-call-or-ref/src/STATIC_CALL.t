/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import pl;
import pl.pl2;

@native
function global_call(): pl.dummy;

class cls_call extends parent {
	@native public static function static_call    (): pl.dummy;
	@native public        function non_static_call(): pl.dummy;

	public static function for_static_call(x: pl.dummy, y: pl.pl2.dummy): void {
		pl.dummy.static_call();
		pl.dummy.static_call().static_call();
		pl.dummy.static_call().non_static_call();
		pl.dummy.static_call().static_var;
		pl.dummy.static_call().non_static_var;

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_CALL", parameters = { func_id = "non_static_call" } } }
		pl.dummy.non_static_call(); // should fail

		pl.pl2.dummy.static_call();
		pl.pl2.dummy.static_call().static_call();
		pl.pl2.dummy.static_call().non_static_call();
		pl.pl2.dummy.static_call().static_var;
		pl.pl2.dummy.static_call().non_static_var;

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_CALL", parameters = { func_id = "non_static_call" } } }
		pl.pl2.dummy.non_static_call(); // should fail

		static_call();
		static_call().static_call();
		static_call().non_static_call();
		static_call().static_var;;
		static_call().non_static_var;

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_CALL", parameters = { func_id = "non_static_call" } } }
		non_static_call(); // should fail

		       parent_static_call();
		parent.parent_static_call();

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_CALL", parameters = { func_id = "parent_non_static_call" } } }
		       parent_non_static_call(); // should fail
		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_CALL", parameters = { func_id = "parent_non_static_call" } } }
		parent.parent_non_static_call(); // should fail

		child_call.child_static_call();

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_CALL", parameters = { func_id = "child_non_static_call" } } }
		child_call.child_non_static_call(); //shoudl fail

		x.static_call();
		x.static_call().static_call();
		x.static_call().non_static_call();
		x.static_call().static_var;
		x.static_call().non_static_var;
		x.non_static_call();
		x.non_static_call().static_call();
		x.non_static_call().non_static_call();
		x.non_static_call().static_var;
		x.non_static_call().non_static_var;

		y.static_call();
		y.static_call().static_call();
		y.static_call().non_static_call();
		y.static_call().static_var;
		y.static_call().non_static_var;
		y.non_static_call();
		y.non_static_call().static_call();
		y.non_static_call().non_static_call();
		y.non_static_call().static_var;
		y.non_static_call().non_static_var;

		(new pl.dummy()).static_call();
		(new pl.dummy()).static_call().static_call();
		(new pl.dummy()).static_call().non_static_call();
		(new pl.dummy()).static_call().static_var;
		(new pl.dummy()).static_call().non_static_var;
		(new pl.dummy()).non_static_call();
		(new pl.dummy()).non_static_call().static_call();
		(new pl.dummy()).non_static_call().non_static_call();
		(new pl.dummy()).non_static_call().static_var;
		(new pl.dummy()).non_static_call().non_static_var;

		(new pl.pl2.dummy()).static_call();
		(new pl.pl2.dummy()).static_call().static_call();
		(new pl.pl2.dummy()).static_call().non_static_call();
		(new pl.pl2.dummy()).static_call().static_var;
		(new pl.pl2.dummy()).static_call().non_static_var;
		(new pl.pl2.dummy()).non_static_call();
		(new pl.pl2.dummy()).non_static_call().static_call();
		(new pl.pl2.dummy()).non_static_call().non_static_call();
		(new pl.pl2.dummy()).non_static_call().static_var;
		(new pl.pl2.dummy()).non_static_call().non_static_var;

		global_call();
		global_call().static_call();
		global_call().non_static_call();
		global_call().static_var;
		global_call().non_static_var;

		pl.global_call();
		pl.global_call().static_call();
		pl.global_call().non_static_call();
		pl.global_call().static_var;
		pl.global_call().non_static_var;

		pl.pl2.global_call();
		pl.pl2.global_call().static_call();
		pl.pl2.global_call().non_static_call();
		pl.pl2.global_call().static_var;
		pl.pl2.global_call().non_static_var;
	}

	public function for_non_static_call(x: pl.dummy, y: pl.pl2.dummy): void {
		pl.dummy.static_call();
		pl.dummy.static_call().static_call();
		pl.dummy.static_call().non_static_call();
		pl.dummy.static_call().static_var;
		pl.dummy.static_call().non_static_var;

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_CALL", parameters = { func_id = "non_static_call" } } }
		pl.dummy.non_static_call(); // should fail

		pl.pl2.dummy.static_call();
		pl.pl2.dummy.static_call().static_call();
		pl.pl2.dummy.static_call().non_static_call();
		pl.pl2.dummy.static_call().static_var;
		pl.pl2.dummy.static_call().non_static_var;

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_CALL", parameters = { func_id = "non_static_call" } } }
		pl.pl2.dummy.non_static_call(); // should fail

		static_call();
		static_call().static_call();
		static_call().non_static_call();
		static_call().static_var;
		static_call().non_static_var;
		non_static_call();
		non_static_call().static_call();
		non_static_call().non_static_call();
		non_static_call().static_var;
		non_static_call().non_static_var;

		       parent_static_call();
		parent.parent_static_call();
		       parent_non_static_call();
		parent.parent_non_static_call();

		child_call.child_static_call();

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_CALL", parameters = { func_id = "child_non_static_call" } } }
		child_call.child_non_static_call(); //shoudl fail

		x.static_call();
		x.static_call().static_call();
		x.static_call().non_static_call();
		x.static_call().static_var;
		x.static_call().non_static_var;
		x.non_static_call();
		x.non_static_call().static_call();
		x.non_static_call().non_static_call();
		x.non_static_call().static_var;
		x.non_static_call().non_static_var;

		y.static_call();
		y.static_call().static_call();
		y.static_call().non_static_call();
		y.static_call().static_var;
		y.static_call().non_static_var;
		y.non_static_call();
		y.non_static_call().static_call();
		y.non_static_call().non_static_call();
		y.non_static_call().static_var;
		y.non_static_call().non_static_var;

		(new pl.dummy()).static_call();
		(new pl.dummy()).static_call().static_call();
		(new pl.dummy()).static_call().non_static_call();
		(new pl.dummy()).static_call().static_var;
		(new pl.dummy()).static_call().non_static_var;
		(new pl.dummy()).non_static_call();
		(new pl.dummy()).non_static_call().static_call();
		(new pl.dummy()).non_static_call().non_static_call();
		(new pl.dummy()).non_static_call().static_var;
		(new pl.dummy()).non_static_call().non_static_var;

		(new pl.pl2.dummy()).static_call();
		(new pl.pl2.dummy()).static_call().static_call();
		(new pl.pl2.dummy()).static_call().non_static_call();
		(new pl.pl2.dummy()).static_call().static_var;
		(new pl.pl2.dummy()).static_call().non_static_var;
		(new pl.pl2.dummy()).non_static_call();
		(new pl.pl2.dummy()).non_static_call().static_call();
		(new pl.pl2.dummy()).non_static_call().non_static_call();
		(new pl.pl2.dummy()).non_static_call().static_var;
		(new pl.pl2.dummy()).non_static_call().non_static_var;

		global_call();
		global_call().static_call();
		global_call().non_static_call();
		global_call().static_var;
		global_call().non_static_var;

		pl.global_call();
		pl.global_call().static_call();
		pl.global_call().non_static_call();
		pl.global_call().static_var;
		pl.global_call().non_static_var;

		pl.pl2.global_call();
		pl.pl2.global_call().static_call();
		pl.pl2.global_call().non_static_call();
		pl.pl2.global_call().static_var;
		pl.pl2.global_call().non_static_var;
	}
}

class child_call extends cls_call {
	public static function child_static_call    (): void {}
	public        function child_non_static_call(): void {}
}

function for_call_test(x: pl.dummy, y: pl.pl2.dummy): void {
	pl.dummy.static_call();
	pl.dummy.static_call().static_call();
	pl.dummy.static_call().non_static_call();
	pl.dummy.static_call().static_var;
	pl.dummy.static_call().non_static_var;

	@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_CALL", parameters = { func_id = "non_static_call" } } }
	pl.dummy.non_static_call(); // should fail

	pl.pl2.dummy.static_call();
	pl.pl2.dummy.static_call().static_call();
	pl.pl2.dummy.static_call().non_static_call();
	pl.pl2.dummy.static_call().static_var;
	pl.pl2.dummy.static_call().non_static_var;

	@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_CALL", parameters = { func_id = "non_static_call" } } }
	pl.pl2.dummy.non_static_call(); // should fail

	x.static_call();
	x.static_call().static_call();
	x.static_call().non_static_call();
	x.static_call().static_var;
	x.static_call().non_static_var;
	x.non_static_call();
	x.non_static_call().static_call();
	x.non_static_call().non_static_call();
	x.non_static_call().static_var;
	x.non_static_call().non_static_var;

	y.static_call();
	y.static_call().static_call();
	y.static_call().non_static_call();
	y.static_call().static_var;
	y.static_call().non_static_var;
	y.non_static_call();
	y.non_static_call().static_call();
	y.non_static_call().non_static_call();
	y.non_static_call().static_var;
	y.non_static_call().non_static_var;

	(new pl.dummy()).static_call();
	(new pl.dummy()).static_call().static_call();
	(new pl.dummy()).static_call().non_static_call();
	(new pl.dummy()).static_call().static_var;
	(new pl.dummy()).static_call().non_static_var;
	(new pl.dummy()).non_static_call();
	(new pl.dummy()).non_static_call().static_call();
	(new pl.dummy()).non_static_call().non_static_call();
	(new pl.dummy()).non_static_call().static_var;
	(new pl.dummy()).non_static_call().non_static_var;

	(new pl.pl2.dummy()).static_call();
	(new pl.pl2.dummy()).static_call().static_call();
	(new pl.pl2.dummy()).static_call().non_static_call();
	(new pl.pl2.dummy()).static_call().static_var;
	(new pl.pl2.dummy()).static_call().non_static_var;
	(new pl.pl2.dummy()).non_static_call();
	(new pl.pl2.dummy()).non_static_call().static_call();
	(new pl.pl2.dummy()).non_static_call().non_static_call();
	(new pl.pl2.dummy()).non_static_call().static_var;
	(new pl.pl2.dummy()).non_static_call().non_static_var;

	global_call();
	global_call().static_call();
	global_call().non_static_call();
	global_call().static_var;
	global_call().non_static_var;

	pl.global_call();
	pl.global_call().static_call();
	pl.global_call().non_static_call();
	pl.global_call().static_var;
	pl.global_call().non_static_var;

	pl.pl2.global_call();
	pl.pl2.global_call().static_call();
	pl.pl2.global_call().non_static_call();
	pl.pl2.global_call().static_var;
	pl.pl2.global_call().non_static_var;
}

