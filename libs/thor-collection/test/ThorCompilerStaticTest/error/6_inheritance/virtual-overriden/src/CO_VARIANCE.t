/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
interface I1 {}

class C1 implements I1 {}
class C2               {}
class C3 extends C1 {}
class C4 extends C2 {}

interface II1
{
	public function get_primitive() : int32;
	public function get_iface() : I1;
	public function get_class() : C1;
}

interface II2 extends II1
{
	public function get_primitive() : int32;
	public function get_iface() : C1;
	public function get_class() : C3;
}

interface II3 extends II1
{
	@static_test
	{
		expect_message =
		{
			level = "LEVEL_ERROR", id = "INVALID_COVARIANCE", parameters =
			{
				origin_class       = "II1",
				origin_return_type = "int32"
			}
		}
	}
	public function get_primitive() : void;

	@static_test
	{
		expect_message =
		{
			level = "LEVEL_ERROR", id = "INVALID_COVARIANCE", parameters =
			{
				origin_class       = "II1",
				origin_return_type = "I1"
			}
		}
	}
	public function get_iface() : C2;

	@static_test
	{
		expect_message =
		{
			level = "LEVEL_ERROR", id = "INVALID_COVARIANCE", parameters =
			{
				origin_class       = "II1",
				origin_return_type = "C1"
			}
		}
	}
	public function get_class() : C4;
}

class CC0 implements II1
{
    public virtual function get_primitive() : int32 { return 0   ; }
    public virtual function get_iface    () : I1    { return null; }
    public virtual function get_class    () : C1    { return null; }
}

class CC1 implements II1
{
	public virtual function get_primitive() : int32
	{
		return 0;
	}

	public virtual function get_iface() : C1
	{
		return null;
	}

	public virtual function get_class() : C3
	{
		return null;
	}
}

class CC2 implements II1
{
	@static_test
	{
		expect_message =
		{
			level = "LEVEL_ERROR", id = "INVALID_COVARIANCE", parameters =
			{
				origin_class       = "II1",
				origin_return_type = "int32"
			}
		}
	}
	public virtual function get_primitive() : int64
	{
		return 0;
	}

	@static_test
	{
		expect_message =
		{
			level = "LEVEL_ERROR", id = "INVALID_COVARIANCE", parameters =
			{
				origin_class       = "II1",
				origin_return_type = "I1"
			}
		}
	}
	public virtual function get_iface() : C2
	{
		return null;
	}

	@static_test
	{
		expect_message =
		{
			level = "LEVEL_ERROR", id = "INVALID_COVARIANCE", parameters =
			{
				origin_class       = "II1",
				origin_return_type = "C1"
			}
		}
	}
	public virtual function get_class() : C4
	{
		return null;
	}
}

class CC3 implements II2
{
	@static_test
	{
		expect_message =
		{
			level = "LEVEL_ERROR", id = "INVALID_COVARIANCE", parameters =
			{
				origin_class       = "II2",
				origin_return_type = "int32"
			}
		}
	}
	public virtual function get_primitive() : int8
	{
		return 0;
	}

	@static_test
	{
		expect_message =
		{
			level = "LEVEL_ERROR", id = "INVALID_COVARIANCE", parameters =
			{
				origin_class       = "II2",
				origin_return_type = "C1"
			}
		}
	}
	public virtual function get_iface() : I1
	{
		return null;
	}

	@static_test
	{
		expect_message =
		{
			level = "LEVEL_ERROR", id = "INVALID_COVARIANCE", parameters =
			{
				origin_class       = "II2",
				origin_return_type = "C3"
			}
		}
	}
	public virtual function get_class() : C1
	{
		return null;
	}
}

class C5
{
	public virtual function get_virtual_primitive() : int32
	{
		return 0;
	}

	public virtual function get_virtual_iface() : I1
	{
		return null;
	}

	public virtual function get_virtual_class() : C1
	{
		return null;
	}
}

class C6 extends C5 {}

class C7 extends C5
{
	public virtual function get_virtual_primitive() : int32
	{
		return 0;
	}

	public virtual function get_virtual_iface() : C1
	{
		return null;
	}

	public virtual function get_virtual_class() : C3
	{
		return null;
	}
}

class C8 extends C6
{
	@static_test
	{
		expect_message =
		{
			level = "LEVEL_ERROR", id = "INVALID_COVARIANCE", parameters =
			{
				origin_class       = "C5",
				origin_return_type = "int32"
			}
		}
	}
	public virtual function get_virtual_primitive() : int64
	{
		return 0;
	}

	@static_test
	{
		expect_message =
		{
			level = "LEVEL_ERROR", id = "INVALID_COVARIANCE", parameters =
			{
				origin_class       = "C5",
				origin_return_type = "I1"
			}
		}
	}
	public virtual function get_virtual_iface() : C2
	{
		return null;
	}

	@static_test
	{
		expect_message =
		{
			level = "LEVEL_ERROR", id = "INVALID_COVARIANCE", parameters =
			{
				origin_class       = "C5",
				origin_return_type = "C1"
			}
		}
	}
	public virtual function get_virtual_class() : C4
	{
		return null;
	}
}
