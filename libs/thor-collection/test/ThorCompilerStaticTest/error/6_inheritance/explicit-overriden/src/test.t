/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
interface iface               {         function iface_func_void(        ): void ;             }
class     base1               {         function  base_func_void(        ): void {           }
                                virtual function  base_func_int8(        ): int8 { return 0; } }
class     base2 extends base1 { virtual function  base_func_void(i8: int8): void {           } }

class d1
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "iface_func_void" } } }
    override function iface_func_void(        ): void {           }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "iface_func_void" } } }
    override function iface_func_void(i8: int8): void {           }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "iface_func_int8" } } }
    override function iface_func_int8(        ): int8 { return 0; }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "iface_func_int8" } } }
    override function iface_func_int8(i8: int8): int8 { return 0; }

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "base_func_void" } } }
    override function  base_func_void(        ): void {           }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "base_func_void" } } }
    override function  base_func_void(i8: int8): void {           }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "base_func_int8" } } }
    override function  base_func_int8(        ): int8 { return 0; }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "base_func_int8" } } }
    override function  base_func_int8(i8: int8): int8 { return 0; }
}

class d2 extends base1 implements iface
{
    function iface_func_void(        ): void {           }
    function iface_func_void(i8: int8): void {           }
    function iface_func_int8(        ): int8 { return 0; }
    function iface_func_int8(i8: int8): int8 { return 0; }

    function  base_func_void(        ): void {           }
    function  base_func_void(i8: int8): void {           }
    function  base_func_int8(        ): int8 { return 0; }
    function  base_func_int8(i8: int8): int8 { return 0; }
}

class d3 extends base1 implements iface
{
    override function iface_func_void(        ): void {           }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "iface_func_void" } } }
    override function iface_func_void(i8: int8): void {           }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "iface_func_int8" } } }
    override function iface_func_int8(        ): int8 { return 0; }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "iface_func_int8" } } }
    override function iface_func_int8(i8: int8): int8 { return 0; }

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "base_func_void" } } }
    override function  base_func_void(        ): void {           }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "base_func_void" } } }
    override function  base_func_void(i8: int8): void {           }
    override function  base_func_int8(        ): int8 { return 0; }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "base_func_int8" } } }
    override function  base_func_int8(i8: int8): int8 { return 0; }
}

class d4 extends base2 implements iface
{
    function iface_func_void(        ): void {           }
    function iface_func_void(i8: int8): void {           }
    function iface_func_int8(        ): int8 { return 0; }
    function iface_func_int8(i8: int8): int8 { return 0; }

    function  base_func_void(        ): void {           }
    function  base_func_void(i8: int8): void {           }
    function  base_func_int8(        ): int8 { return 0; }
    function  base_func_int8(i8: int8): int8 { return 0; }
}

class d5 extends base2 implements iface
{
    override function iface_func_void(        ): void {           }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "iface_func_void" } } }
    override function iface_func_void(i8: int8): void {           }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "iface_func_int8" } } }
    override function iface_func_int8(        ): int8 { return 0; }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "iface_func_int8" } } }
    override function iface_func_int8(i8: int8): int8 { return 0; }

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "base_func_void" } } }
    override function  base_func_void(        ): void {           }
    override function  base_func_void(i8: int8): void {           }
    override function  base_func_int8(        ): int8 { return 0; }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN", parameters = { name = "base_func_int8" } } }
    override function  base_func_int8(i8: int8): int8 { return 0; }
}
