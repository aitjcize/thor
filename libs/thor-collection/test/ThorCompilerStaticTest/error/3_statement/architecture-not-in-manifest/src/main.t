/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
// any @gpu decls in this file are not allowed
@gpu
@static_test { expect_message = { level = "LEVEL_ERROR", id = "DECL_WITH_ARCH_NOT_APPEAR_IN_PROJECT_SETTING" } }
var global_gpu_var: int32 = 0;

@cpu
var global_cpu_var: int32 = 0;

@static_test { expect_message = { level = "LEVEL_ERROR", id = "DECL_WITH_ARCH_NOT_APPEAR_IN_PROJECT_SETTING" } }
@gpu
class gpu_class { }

@cpu
class cpu_calss
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "DECL_WITH_ARCH_NOT_APPEAR_IN_PARENT" } }
    @gpu
    function gpu_method(): void { }

    @cpu
    function cpu_method(): void { }

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "DECL_WITH_ARCH_NOT_APPEAR_IN_PARENT" } }
    @gpu
    var gpu_attribute: int32;

    @cpu
    var cpu_attribute: int32;
}

@static_test { expect_message = { level = "LEVEL_ERROR", id = "DECL_WITH_ARCH_NOT_APPEAR_IN_PROJECT_SETTING" } }
@gpu
typedef int32 gpu_typedef;

@cpu
typedef int32 cpu_typedef;

@static_test { expect_message = { level = "LEVEL_ERROR", id = "DECL_WITH_ARCH_NOT_APPEAR_IN_PROJECT_SETTING" } }
@gpu
function gpu_function(): void { }

@cpu
function cpu_function(): void { }

@entry
task main() : void
{
    exit(0);
}
