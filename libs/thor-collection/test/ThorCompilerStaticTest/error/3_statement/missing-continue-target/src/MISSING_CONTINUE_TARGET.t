/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function stand_alone(): void
{
    @static_test { expect_message={ level="LEVEL_ERROR", id="MISSING_CONTINUE_TARGET" } }
    continue;

    if (true)
    {
        @static_test { expect_message={ level="LEVEL_ERROR", id="MISSING_CONTINUE_TARGET" } }
        continue;
    }
}

function in_switch(): void
{
    switch (0)
    {
    case 0:
        @static_test { expect_message={ level="LEVEL_ERROR", id="MISSING_CONTINUE_TARGET" } }
        continue;
    default:
        @static_test { expect_message={ level="LEVEL_ERROR", id="MISSING_CONTINUE_TARGET" } }
        continue;
    }
}

function in_loop(): void
{
    for (;;)
    {
        continue;
    }

    while (true)
    {
        continue;
    }
}

function in_lambda(): void
{
    lambda(): int32
    {
        @static_test { expect_message={ level="LEVEL_ERROR", id="MISSING_CONTINUE_TARGET" } }
        continue;

        return 0;
    };
}

function in_lambda_under_loop(): void
{
    for (;;)
    {
        lambda(): int32
        {
            @static_test { expect_message={ level="LEVEL_ERROR", id="MISSING_CONTINUE_TARGET" } }
            continue;

            return 0;
        };

        continue;
    }

    while (true)
    {
        lambda(): int32
        {
            @static_test { expect_message={ level="LEVEL_ERROR", id="MISSING_CONTINUE_TARGET" } }
            continue;

            return 0;
        };

        continue;
    }
}
