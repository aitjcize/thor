/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function test_constant(): void
{
    var value: int32 = 0;
    // should success
    switch(value)
    {
    case 0:
    case 1:
    case 2:
    }

    // one duplicate case value
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPL_CASE_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPL_CASE_VALUE_PREV" } }
    switch(value)
    {
    case 0:
    case 1:
    case 2:
    case 1:
    }

    // two duplicate case values
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPL_CASE_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPL_CASE_VALUE_PREV" } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPL_CASE_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPL_CASE_VALUE_PREV" } }
    switch(value)
    {
    case 0:
    case 1:
    case 2:
    case 1:
    case 0:
    }
}

enum Enum
{
    A, B, C
}

function test_enum(): void
{
    var value: Enum = Enum.A;
    // should success
    switch(value)
    {
    case Enum.A:
    case Enum.B:
    case Enum.C:
    }

    // one duplicate case value
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPL_CASE_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPL_CASE_VALUE_PREV" } }
    switch(value)
    {
    case Enum.A:
    case Enum.B:
    case Enum.C:
    case Enum.B:
    }

    // two duplicate case values
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPL_CASE_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPL_CASE_VALUE_PREV" } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPL_CASE_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPL_CASE_VALUE_PREV" } }
    switch(value)
    {
    case Enum.A:
    case Enum.B:
    case Enum.C:
    case Enum.B:
    case Enum.A:
    }
}

function test_mixed(): void
{
    /// TODO: add test code here(issue #981)
}
