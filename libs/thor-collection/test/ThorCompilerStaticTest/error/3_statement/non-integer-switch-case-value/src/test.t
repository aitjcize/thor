/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function test_int_literal()
{
    // should success
    switch(0)
    {
    case 1:
    case 2:
    case 3:
    }
}

function test_bool_literal()
{
    // should success
    switch(0)
    {
    case true:
    case false:
    }
}

function test_float_literal()
{
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "NON_INTEGER_CASE_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "NON_INTEGER_CASE_VALUE" } }
    switch(0)
    {
    case 0.5:
    case 0.5+1.5:
    }
}

function test_mixed()
{
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "NON_INTEGER_CASE_VALUE" } }
    switch(0)
    {
    case false:
    case 0.5:
    case 1:
    }
}
