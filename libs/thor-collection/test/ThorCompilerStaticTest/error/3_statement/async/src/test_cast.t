/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
task test_cast_i08(): void
{
    async ->
        global_i08 = get_i08();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i16 = get_i08();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i32 = get_i08();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i64 = get_i08();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_f32 = get_i08();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_f64 = get_i08();
}

task test_cast_i16(): void
{
    async ->
        @static_test{expect_message={level = "LEVEL_WARNING", id = "IMPLICIT_CAST_PRECISION_LOSS"     }}
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i08 = get_i16();
    async ->
        global_i16 = get_i16();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i32 = get_i16();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i64 = get_i16();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_f32 = get_i16();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_f64 = get_i16();
}

task test_cast_i32(): void
{
    async ->
        @static_test{expect_message={level = "LEVEL_WARNING", id = "IMPLICIT_CAST_PRECISION_LOSS"     }}
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i08 = get_i32();
    async ->
        @static_test{expect_message={level = "LEVEL_WARNING", id = "IMPLICIT_CAST_PRECISION_LOSS"     }}
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i16 = get_i32();
    async ->
        global_i32 = get_i32();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i64 = get_i32();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_f32 = get_i32();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_f64 = get_i32();
}

task test_cast_i64(): void
{
    async ->
        @static_test{expect_message={level = "LEVEL_WARNING", id = "IMPLICIT_CAST_PRECISION_LOSS"     }}
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i08 = get_i64();
    async ->
        @static_test{expect_message={level = "LEVEL_WARNING", id = "IMPLICIT_CAST_PRECISION_LOSS"     }}
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i16 = get_i64();
    async ->
        @static_test{expect_message={level = "LEVEL_WARNING", id = "IMPLICIT_CAST_PRECISION_LOSS"     }}
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i32 = get_i64();
    async ->
        global_i64 = get_i64();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_f32 = get_i64();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_f64 = get_i64();
}

task test_cast_f32(): void
{
    async ->
        @static_test{expect_message={level = "LEVEL_WARNING", id = "IMPLICIT_CAST_PRECISION_LOSS"     }}
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i08 = get_f32();
    async ->
        @static_test{expect_message={level = "LEVEL_WARNING", id = "IMPLICIT_CAST_PRECISION_LOSS"     }}
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i16 = get_f32();
    async ->
        @static_test{expect_message={level = "LEVEL_WARNING", id = "IMPLICIT_CAST_PRECISION_LOSS"     }}
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i32 = get_f32();
    async ->
        @static_test{expect_message={level = "LEVEL_WARNING", id = "IMPLICIT_CAST_PRECISION_LOSS"     }}
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i64 = get_f32();
    async ->
        global_f32 = get_f32();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_f64 = get_f32();
}

task test_cast_f64(): void
{
    async ->
        @static_test{expect_message={level = "LEVEL_WARNING", id = "IMPLICIT_CAST_PRECISION_LOSS"     }}
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i08 = get_f64();
    async ->
        @static_test{expect_message={level = "LEVEL_WARNING", id = "IMPLICIT_CAST_PRECISION_LOSS"     }}
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i16 = get_f64();
    async ->
        @static_test{expect_message={level = "LEVEL_WARNING", id = "IMPLICIT_CAST_PRECISION_LOSS"     }}
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i32 = get_f64();
    async ->
        @static_test{expect_message={level = "LEVEL_WARNING", id = "IMPLICIT_CAST_PRECISION_LOSS"     }}
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i64 = get_f64();
    async ->
        @static_test{expect_message={level = "LEVEL_WARNING", id = "IMPLICIT_CAST_PRECISION_LOSS"     }}
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_f32 = get_f64();
    async ->
        global_f64 = get_f64();
}

task test_cast_obj(): void
{
    async ->
        global_dum = get_dum();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_obj = get_dum();
    async ->
        global_obj = get_obj();
}

task test_cast_explicit(): void
{
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i08 = cast<int8 >(            get_i08() );
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i32 = cast<int8 >(cast<int32>(get_i08()));
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_i32 =             cast<int32>(get_i08()) ;

    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_dum = cast<Dummy >(get_dum());
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_dum = cast<Dummy >(get_obj());
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_obj = cast<Object>(get_dum());
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR"  , id = "INVALID_IMPLCIT_ASYNC_FOUND_CAST" }}
        global_obj = cast<Object>(get_obj());
}
