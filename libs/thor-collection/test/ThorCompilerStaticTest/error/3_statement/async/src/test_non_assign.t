/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
task test_non_assign(): int32
{
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 + test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 - test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 * test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 / test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 % test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 & test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 | test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 ^ test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 >> test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 << test_non_assign();

    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 == test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 != test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 >  test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 >= test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 <= test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 <  test_non_assign();

    // logical operator will insert implicit cast, this cause the error message is not same as the others
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_FOUND_CAST"             }}
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_INVALID_LHS"            }}
        global_i32 && test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_FOUND_CAST"             }}
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_INVALID_LHS"            }}
        global_i32 || test_non_assign();

    async ->
        global_i32 = test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 += test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 -= test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 *= test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 /= test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 %= test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 &= test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 |= test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 ^= test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 >>= test_non_assign();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id="INVALID_IMPLCIT_ASYNC_BINARY_OP_NOT_A_ASSIGN" }}
        global_i32 <<= test_non_assign();

    return 0;
}
