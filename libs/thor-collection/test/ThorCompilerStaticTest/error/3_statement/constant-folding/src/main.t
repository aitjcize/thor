/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class dummy {}
enum  e     { V}

function test_arithmetic()
{
    @static_test { expect_constant = 6    }  1   + 5  ;
    @static_test { expect_constant = 6    } 10   - 4  ;
    @static_test { expect_constant = 6    }  2   * 3  ;
    @static_test { expect_constant = 6    } 18   / 3  ;
    @static_test { expect_constant = 6    } 13   % 7  ;
    @static_test { expect_constant = 5.5  }  3.2 + 2.3;
    @static_test { expect_constant = 5.5  }  7.1 - 1.6;
    @static_test { expect_constant = 5.5  }  1.1 * 5  ;
    @static_test { expect_constant = 5.5  } 11.0 / 2  ;

    @static_test { expect_constant = 6    } 5 + true ;
    @static_test { expect_constant = 6    } 7 - true ;
    @static_test { expect_constant = 6    } 6 * true ;
    @static_test { expect_constant = 6    } 6 / true ;
    @static_test { expect_constant = 0    } 6 % true ;
}

function test_logical()
{
    @static_test { expect_constant = true  } true  && true ;
    @static_test { expect_constant = false } true  && false;
    @static_test { expect_constant = false } false && true ;
    @static_test { expect_constant = false } false && false;
    @static_test { expect_constant = true  }   1   &&   1  ;
    @static_test { expect_constant = false }   1   &&   0  ;
    @static_test { expect_constant = false }   0   &&   1  ;
    @static_test { expect_constant = false }   0   &&   0  ;
    @static_test { expect_constant = true  }   1.0 &&   1.0;
    @static_test { expect_constant = false }   1.0 &&   0.0;
    @static_test { expect_constant = false }   0.0 &&   1.0;
    @static_test { expect_constant = false }   0.0 &&   0.0;

    @static_test { expect_constant = true  } true  || true ;
    @static_test { expect_constant = true  } true  || false;
    @static_test { expect_constant = true  } false || true ;
    @static_test { expect_constant = false } false || false;
    @static_test { expect_constant = true  }   1   ||   1  ;
    @static_test { expect_constant = true  }   1   ||   0  ;
    @static_test { expect_constant = true  }   0   ||   1  ;
    @static_test { expect_constant = false }   0   ||   0  ;
    @static_test { expect_constant = true  }   1.0 ||   1.0;
    @static_test { expect_constant = true  }   1.0 ||   0.0;
    @static_test { expect_constant = true  }   0.0 ||   1.0;
    @static_test { expect_constant = false }   0.0 ||   0.0;

    @static_test { expect_constant = false } !true ;
    @static_test { expect_constant = true  } !false;
    @static_test { expect_constant = true  } !0    ;
    @static_test { expect_constant = false } !1    ;
    @static_test { expect_constant = true  } !0.0  ;
    @static_test { expect_constant = false } !1.0  ;
}

function test_binary()
{
    @static_test { expect_constant =                   2  }      1  <<      1;
    @static_test { expect_constant =          1073741824  }      1  <<     30;
    @static_test { expect_message  = { level = "LEVEL_WARNING", id = "NUMERIC_LITERAL_OVERFLOW" } }
    @static_test { expect_constant =                   0  }      1  <<     62;
    @static_test { expect_constant = 4611686018427387904L }      1L <<     62;

    @static_test { expect_message  = { level = "LEVEL_WARNING", id = "NUMERIC_LITERAL_UNDERFLOW" } }
    @static_test { expect_constant =           0x3FFFFFFF } 0x7FFFFFFF            >>  1;
    @static_test { expect_message  = { level = "LEVEL_WARNING", id = "NUMERIC_LITERAL_UNDERFLOW" } }
    @static_test { expect_constant =                    1 } 0x7FFFFFFF            >> 30;
    @static_test { expect_message  = { level = "LEVEL_WARNING", id = "NUMERIC_LITERAL_UNDERFLOW" } }
    @static_test { expect_constant =                    0 } 0x7FFFFFFF            >> 62;
    @static_test { expect_constant =                    1 } 4611686018427387904L  >> 62;
    @static_test { expect_message  = { level = "LEVEL_WARNING", id = "NUMERIC_LITERAL_UNDERFLOW" } }
    @static_test { expect_constant =                    1 } 9223372036854775807L  >> 62;

    @static_test { expect_constant =              0xFFFF  } 0xFEFF  |  0xFF7F;
    @static_test { expect_constant =              0xFE7F  } 0xFEFF  &  0xFF7F;
    @static_test { expect_constant =              0x0180  } 0xFEFF  ^  0xFF7F;

    @static_test { expect_constant =          2 } ~0xFFFFFFFD; // FIXME fix the incorrect overflow warning
    @static_test { expect_constant = 0xFFFFFFFE } ~true      ; // FIXME fix the incorrect overflow warning
    @static_test { expect_constant =         -1 } ~false     ;
}

function test_cast()
{
    @static_test { expect_constant =  1 } cast<int32>(  1L)    ;
    @static_test { expect_constant =  2 } cast<int8 >(258 )    ;
    @static_test { expect_constant = 16 } cast<int16>( 32 ) / 2;
    @static_test { expect_constant =  0 } cast<int32>(e.V )    ;
    @static_test { expect_constant =  1 } cast<int32>(e.V ) + 1;

    @static_test { expect_constnat =  0 } cast<int32>(cast<e>(0));
    @static_test { expect_constnat =  1 } cast<int32>(cast<e>(0)) + 1;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_VALUE_FOR_ENUM" } } cast<e>(1    );
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_VALUE_FOR_ENUM" } } cast<e>(0 - 1);
}

function test_isa()
{
    @static_test { expect_constant = true  } isa<int32>(  1 );
    @static_test { expect_constant = false } isa<int32>(  1L);
    @static_test { expect_constant = false } isa<int32>(e.V );
    @static_test { expect_constant = false } isa<dummy>(  1 );
    @static_test { expect_constant = false } isa<dummy>(  1L);
    @static_test { expect_constant = false } isa<dummy>(e.V );
    @static_test { expect_constant = false } isa<e    >(  1 );
    @static_test { expect_constant = false } isa<e    >(  1L);
    @static_test { expect_constant = true  } isa<e    >(e.V );
}

function test_mixed()
{
    @static_test { expect_constant = 2 }         true +       true ;
    @static_test { expect_constant = 0 } ~(0xF00FF00F + 0x0FF00FF0); // FIXME fix the incorrect overflow warning
}

function test_ternary()
{
    // only one constant expression in condition
    @static_test { expect_constant = 1 } true  ? 1 : 2;
    @static_test { expect_constant = 2 } false ? 1 : 2;

    // convertible to boolean
    @static_test { expect_constant = 1 }   1   ? 1 : 2;
    @static_test { expect_constant = 2 }   0   ? 1 : 2;

    // complex condtion
    @static_test { expect_constant = 1 } false || true  ? 1 : 2;
    @static_test { expect_constant = 2 } true  && false ? 1 : 2;

    // complex result
    @static_test { expect_constant = 110 } true  ? (50 + 60) : (70 + 80);
    @static_test { expect_constant = 150 } false ? (50 + 60) : (70 + 80);
}
