/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class X {}

var x   : X       = null;
var b   : bool    = false;
var i8  : int8    = 0;
var i16 : int16   = 0;
var i32 : int32   = 0;
var i64 : int64   = 0;
var f32 : float32 = 0;
var f64 : float64 = 0;

function main() : int32 {
    atomic -> {
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNSUPPORTED_ATOMIC_TYPE", parameters={ name="x" } } }
        x    = null;
        b    = true; 
        i8   = 1;
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNSUPPORTED_ATOMIC_TYPE", parameters={ name="i16" } } }
        i16  = 1;
        i32  = 1;
        i64  = 1;
        f32  = 1;
        f64  = 1;
    }
    return 0;
}
