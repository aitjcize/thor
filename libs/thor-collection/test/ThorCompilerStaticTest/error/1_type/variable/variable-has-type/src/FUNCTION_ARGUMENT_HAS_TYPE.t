/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function function_1_1(arg1: int32) {}

function function_1_2(arg1: dummy_2) {}

function function_1_3(
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} } arg1
) {}

function function_2_1(arg1: int32,arg2: dummy_1) {}

function function_2_2(
                                                                                       arg1: int32,
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} } arg2
) {}

function function_2_3(
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} } arg1         ,
                                                                                       arg2: dummy_1
) {}

function function_2_4(
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} } arg1,
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} } arg2
) {}

