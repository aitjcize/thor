/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class the_class_1 {
           var member_1: int32 = 1234;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
           var member_2        = 4321;
    static var member_3: int32 = 1234;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
    static var member_4        = 4321;
}

class the_class_2 {
           const member_1: int32 = 1234;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
           const member_2        = 4321;
    static const member_3: int32 = 1234;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
    static const member_4        = 4321;
}

class the_class_3<T> {
           var member_1: int32 = 1234;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
           var member_2        = 4321;
    static var member_3: int32 = 1234;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
    static var member_4        = 4321;
}

class the_class_4<T> {
           const member_1: int32 = 1234;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
           const member_2        = 4321;
    static const member_3: int32 = 1234;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
    static const member_4        = 4321;
}

class the_class_1 {
           var member_1: dummy_1 = new dummy_1;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
           var member_2          = new dummy_2;
    static var member_3: dummy_2 = new dummy_1;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
    static var member_4          = new dummy_2;
}

class the_class_2 {
           const member_1: dummy_1 = new dummy_1;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
           const member_2          = new dummy_2;
    static const member_3: dummy_2 = new dummy_1;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
    static const member_4          = new dummy_2;
}

class the_class_3<T> {
           var member_1: dummy_1 = new dummy_1;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
           var member_2          = new dummy_2;
    static var member_3: dummy_2 = new dummy_1;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
    static var member_4          = new dummy_2;
}

class the_class_4<T> {
           const member_1: dummy_1 = new dummy_1;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
           const member_2          = new dummy_2;
    static const member_3: dummy_2 = new dummy_1;
    @static_test { expect_message={ level="LEVEL_ERROR", id="VARIABLE_MISSING_TYPE"} }
    static const member_4          = new dummy_2;
}

