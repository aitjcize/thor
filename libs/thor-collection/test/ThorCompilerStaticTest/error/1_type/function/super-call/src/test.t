/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class B {
    public        function non_static_() : void {}
    public static function     static_() : void {}
}

class D extends B {
    public        function non_static_a() : void { super.    static_(); }
    public        function non_static_b() : void { super.non_static_(); }
    public static function     static_a() : void { super.    static_(); }
    public static function     static_b() : void { @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_CALL", parameters = { func_id = "non_static_"} } } super.non_static_(); }
}

function test_main(): int32 {
    var d: D;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_CALL", parameters = { func_id = "non_static_a"} } } D.non_static_a();
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_CALL", parameters = { func_id = "non_static_b"} } } D.non_static_b();
                                                                                                                                         D.    static_a();
                                                                                                                                         D.    static_b();

                                                                                                                                         d.non_static_a();
                                                                                                                                         d.non_static_b();
                                                                                                                                         d.    static_a();
                                                                                                                                         d.    static_b();

    return 0;
}
