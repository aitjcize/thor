/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@entry task entry_task_ret_void (): void  {}
@entry task entry_task_ret_int32(): int32 {}

@entry
@static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_A_TASK" } }
function entry_non_task_ret_void (): void  {}

@entry
@static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_A_TASK" } }
function entry_non_task_ret_int32(): int32 {}

@entry
@static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_ACCEPT_NO_PARAMETER" } }
task entry_1_arg_task_ret_void (i32:   int32): void  {}

@entry
@static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_ACCEPT_NO_PARAMETER" } }
task entry_1_arg_task_ret_int32(f64: float64): int32 {}

@entry
@static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_ACCEPT_NO_PARAMETER" } }
task entry_2_arg_task_ret_void (f64: float64, i32:   int32): void  {}

@entry
@static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_ACCEPT_NO_PARAMETER" } }
task entry_2_arg_task_ret_int32(i32:   int32, f64: float64): int32 {}
