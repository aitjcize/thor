/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class QWE {}

function test_assignment(qwe: QWE, x: int32): void
{
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ rhs_type="int32"    , lhs_type="class QWE"       } } }
    qwe=x;

    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ rhs_type="class QWE", lhs_type="int32"           } } }
    x=qwe;
}

function test_ternary(qwe: QWE, x: int32): void
{
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ rhs_type="class QWE", lhs_type="bool"            } } }
    qwe         ? true : false;
    qwe == null ? true : false;

    x           ? true : false;
    x   == 0    ? true : false;
}

function test_if_else(qwe: QWE, x: int32): void
{
    // LogInfo is attached on IfElseStmt in current implementation
    // So here are two LogInfo from condition of 'if' and 'elif'
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ rhs_type="class QWE", lhs_type="bool"            } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ rhs_type="class QWE", lhs_type="bool"            } } }
    if(qwe);
    elif(qwe);

    if(qwe == null);
    elif(qwe == null);

    if(x);
    elif(x);

    if(x == 0);
    elif(x == 0);
}

function test_for(qwe: QWE, x: int32): void
{
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ rhs_type="class QWE", lhs_type="bool"            } } }
    for(; qwe        ; );
    for(; qwe == null; );
    for(; x          ; );
    for(; x   == 0   ; );
}

function test_while(qwe: QWE, x: int32): void
{
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ rhs_type="class QWE", lhs_type="bool"            } } }
    while(qwe        );
    while(qwe == null);
    while(x          );
    while(x   == 0   );
}

function test_init(): void
{
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ rhs_type="int32"    , lhs_type="class QWE"       } } }
    var qwe: QWE              = 1234;
    var x  : int32            = 1234;
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ rhs_type="int32"    , lhs_type="function():void" } } }
    var f  : function(): void = 1234;
}

function test_default(
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ rhs_type="int32"       , lhs_type="class QWE"       } } }
    qwe_1: QWE   = 1234,
    qwe_2: QWE   = null,
    i32_1: int32 = 1234,
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ rhs_type="class Object", lhs_type="int32"           } } }
    i32_2: int32 = null
): void {}

@native function multi_value() : (int32, QWE, function():void);

function test_multi(): void
{
    var i32 : int32;
    var qwe : QWE;
    var func: function(): void;

    i32, qwe, func =  i32,  qwe, func;

    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="function():void"  , rhs_type="class QWE"        } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="class QWE"        , rhs_type="function():void"  } } }
    i32, qwe, func =  i32, func,  qwe;

    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="int32"            , rhs_type="class QWE"        } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="class QWE"        , rhs_type="int32"            } } }
    i32, qwe, func =  qwe,  i32, func;

    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="int32"            , rhs_type="class QWE"        } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="class QWE"        , rhs_type="function():void"  } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="function():void"  , rhs_type="int32"            } } }
    i32, qwe, func =  qwe, func,  i32;

    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="int32"            , rhs_type="function():void"  } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="class QWE"        , rhs_type="int32"            } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="function():void"  , rhs_type="class QWE"        } } }
    i32, qwe, func = func,  i32,  qwe;

    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="int32"            , rhs_type="function():void"  } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="function():void"  , rhs_type="int32"            } } }
    i32, qwe, func = func,  qwe,  i32;

    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="(int32,class QWE)", rhs_type="int32"            } } }
    i32, qwe = i32;

    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="int32"            , rhs_type="(int32,class QWE)"} } }
    i32 = i32, qwe;

    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="(int32,class QWE)", rhs_type="(int32,class QWE,function():void)"} } }
    i32, qwe = multi_value();

    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="(int32,class QWE)", rhs_type="(int32,class QWE,function():void)"} } }
    (i32, qwe = multi_value()) = i32, qwe;
}

enum Enum32_A { x, y }
enum Enum32_B { x, y }
enum Enum64_C { x = 1L << 33L, y }

function test_enum(): void
{
    var ea : Enum32_A = Enum32_A.x;
    var eb : Enum32_B = Enum32_B.x;
    var ec : Enum64_C = Enum64_C.x;

    var i16 : int16;
    var i32 : int32;
    var i64 : int64;

    var f32 : float32;
    var f64 : float64;

    // int to enum
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="enum Enum32_A", rhs_type="int32"} } }
    ea = i32;
    ea = cast<Enum32_A>(i32);
    @static_test { expect_message={ level="LEVEL_WARNING", id="EXPLICIT_CAST_PRECISION_LOSS"} }
    ea = cast<Enum32_A>(i64);
    ea = cast<Enum32_A>(cast<int32>(i64));

    // enum to int
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="int32", rhs_type="enum Enum32_A"} } }
    i32 = Enum32_A.x;
    i32 = cast<int32>(Enum32_A.x);
    @static_test { expect_message={ level="LEVEL_WARNING", id="EXPLICIT_CAST_PRECISION_LOSS"} } 
    i16 = cast<int16>(Enum32_A.x);
    i16 = cast<int16>(cast<int32>(Enum32_A.x));

    // enum to enum implicitly
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="enum Enum32_A", rhs_type="enum Enum32_B"} } }
    ea = eb;
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="enum Enum32_A", rhs_type="enum Enum32_B"} } }
    ea = Enum32_B.x;
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="enum Enum32_A", rhs_type="enum Enum64_C"} } }
    ea = Enum64_C.x;
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="enum Enum64_C", rhs_type="enum Enum32_A"} } }
    ec = Enum32_A.x;

    // enum to enum explicitly
    ea = cast<Enum32_A>(eb);
    ea = cast<Enum32_A>(Enum32_B.x);
    @static_test { expect_message={ level="LEVEL_WARNING", id="EXPLICIT_CAST_PRECISION_LOSS"} }
    ea = cast<Enum32_A>(Enum64_C.x);
    ec = cast<Enum64_C>(Enum32_A.x);
    ea = cast<Enum32_A>(cast<int32>(cast<int64>(Enum64_C.x)));
    ec = cast<Enum64_C>(cast<int64>(cast<int32>(Enum32_A.x)));

    // enum to float
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="float32", rhs_type="enum Enum32_A"} } }
    f32 = ea;
    @static_test { expect_message={ level="LEVEL_WARNING", id="EXPLICIT_CAST_PRECISION_LOSS"} }
    f32 = cast<float32>(ea);
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_CONV", parameters={ lhs_type="enum Enum32_A", rhs_type="float32"} } }
    ea = f32;
    @static_test { expect_message={ level="LEVEL_WARNING", id="EXPLICIT_CAST_PRECISION_LOSS"} }
    ea = cast<Enum32_A>(f32);
}
