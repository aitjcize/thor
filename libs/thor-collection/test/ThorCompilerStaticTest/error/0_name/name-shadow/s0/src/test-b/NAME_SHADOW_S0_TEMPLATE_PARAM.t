/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
// test shadowing on template parameter

// 1.class template parameter + member function template parameter
class test_value_1<
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_1
>
{
    function test_value_1_a<
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1"} } }
        value_1
    >(): void {}
}

// 2.class template parameter + function parameter
class test_value_2<
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_2
>
{
    function test_value_1_a(
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_2"} } }
        value_2: int32
    ): void {}
}

// 3.class template parameter + member variables
class test_value_3<
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_3
>
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3"} } }
    var value_3: int32 = 0;
}

// 4.class template parameter + local variables
class test_value_4<
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_4_a_a,
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_4_a_b,
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_4_a_c,
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_4_b_a,
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_4_b_b,
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_4_b_c
>
{
    function test_value_4_a<T>(): void
    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_4_a_a"} } }
        var value_4_a_a: int32 = 0;

        {
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_4_a_b"} } }
            var value_4_a_b: int32 = 0;
        }

        for(
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_4_a_c"} } }
            var value_4_a_c: int32 = 0; ;
        );
    }

    function test_value_4_b(): void
    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_4_b_a"} } }
        var value_4_b_a: int32 = 0;

        {
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_4_b_b"} } }
            var value_4_b_b: int32 = 0;
        }

        for(
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_4_b_c"} } }
            var value_4_b_c: int32 = 0; ;
        );
    }
}

// 5.function template parameter + function parameter
function test_value_5<
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_5
>(
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_5"} } }
    value_5: int32
): void {}

// 6.function template parameter + local variables
function test_value_6<
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_6_a,
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_6_b,
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_6_c
>(): void
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_6_a"} } }
    var value_6_a: int32 = 0;

    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_6_b"} } }
        var value_6_b: int32 = 0;
    }

    for(
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_6_c"} } }
        var value_6_c: int32 = 0; ;
    );
}
