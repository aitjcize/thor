/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
interface Interface
{
    // should report errors #1: new function
    @static_test { expect_message={ level="LEVEL_ERROR", id="CTOR_MUST_NOT_BE_VIRTUAL", parameters={ name="constructor" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_FUNCTION_IN_INTERFACE" } }
    public function new(): void;
    // should report errors #2: delete function
    @static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_FUNCTION_IN_INTERFACE" } }
    public function delete(): void;

    // should success
    public function other_method(): void;
}
