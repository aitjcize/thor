/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function eliminate_unused_warning<T>( t:T ): void { }

function main(): int32
{
    var int_result : int64 = 0;

    // integral overflow
    @static_test { expect_message={ level="LEVEL_WARNING", id="NUMERIC_LITERAL_OVERFLOW" } }
    int_result = 1L << 64;

    int_result = 1L << 63;

    @static_test { expect_message={ level="LEVEL_WARNING", id="NUMERIC_LITERAL_OVERFLOW" } }
    int_result = 2L << 63;

    int_result = 2L << 62;

    // integral underflow
    int_result = 1L >> 0;

    @static_test { expect_message={ level="LEVEL_WARNING", id="NUMERIC_LITERAL_UNDERFLOW" } }
    int_result = 1L >> 1;

    int_result = 2L >> 1;

    @static_test { expect_message={ level="LEVEL_WARNING", id="NUMERIC_LITERAL_UNDERFLOW" } }
    int_result = 2L >> 2;

    eliminate_unused_warning( int_result );

    return 0;
}
