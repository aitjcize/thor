#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#
thor_test_add_bundle(
    TARGET flow-bundle-functions-jit-lib
    PROJECT_DIR lib
    BUNDLE_TYPE jit
)

thor_test_add_bundle(
    TARGET flow-bundle-functions-native-lib
    PROJECT_DIR lib
    BUNDLE_TYPE native
)

thor_test_add_bundle_test_target(
    TARGET flow-bundle-functions-jit-jit
    PROJECT_DIR app
    ENTRY test
    DEPENDS flow-bundle-functions-jit-lib
    BUNDLE_TYPE jit
)

thor_test_add_bundle_test_target(
    TARGET flow-bundle-functions-native-jit
    PROJECT_DIR app
    ENTRY test
    DEPENDS flow-bundle-functions-native-lib
    BUNDLE_TYPE jit
)

thor_test_add_bundle_test_target(
    TARGET flow-bundle-functions-native-native
    PROJECT_DIR app
    ENTRY test
    DEPENDS flow-bundle-functions-native-lib
    BUNDLE_TYPE native
)

