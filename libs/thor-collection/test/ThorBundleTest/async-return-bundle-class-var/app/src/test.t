/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import lib;

task get_bar()
{
    return new lib.Bar(123, true);
}

var counter : int32 = 10;
task wait(check : lambda() : bool)
{
    if(check())
        exit(0);
    else
    {
        if(counter == 0)
        {
            print(lib.Foo.foo);
            exit(-1);
        }

        --counter;
        async->wait(check);
    }
}


@entry
task test_member()
{
    async->lib.foobar.x = get_bar();

    async->wait(lambda() : bool{
            return lib.foobar.x.i == 123 && lib.foobar.x.b;
        }
    );
}

@entry
task test_static()
{
    var x = lambda() : String
    {
        return "static_foo_string";
    };

    async->lib.Foo.foo = x();

    async->wait(lambda() : bool{
        return lib.Foo.foo.isEqual(x());
    });
}

// yentien (2013/01/21)
// It seemed that if the following code exist, test_foobar & test_static_foo will fail.
// If you comment the test_inst_here task out, both will pass.
@entry
task test_inst()
{
    var ff : lib.FooTemplate<int32> = new lib.FooTemplate<int32>;

    var x = lambda() : int32
    {
        return 999;
    };

    async->ff.x = x();

    async->wait(lambda() : bool{
        return ff.x == x();
    });
}

