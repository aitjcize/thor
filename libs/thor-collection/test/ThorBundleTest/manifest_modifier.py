#!/usr/bin/env python
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import sys
import errno
import re
import xml.etree.ElementTree as ElementTree

def mod_manifest(node, modmap):
    for attrname in node.attrib:
        if attrname in modmap:
            node.attrib[attrname] = modmap[attrname]

    for child in node:
        mod_manifest(child, modmap)

def main(args):
    if len(args) < 3:
        print "Usage: ", args[0], "<manifest input filepath> <manifest output filepath> [attribute name=value]"
        print "Note that we doesn't take into account attributes with same name but attached on different tags in current implementation. BEWARE.\n"
        return 0

    infile = args[1]
    outfile = args[2]
    args = args[3:]
    try:
        with open(infile, 'r') as f:
            tree = ElementTree.parse(f)

        modmap = {}
        for modpair in args:
            match = re.match("(\w+)=([\w\\\/_\-\s]+)", modpair)
            if match:
                modmap[match.group(1)] = match.group(2)

        mod_manifest(tree.getroot(), modmap)

        with open(outfile, 'w') as f:
            tree.write(f)
        
        return 0;

    except IOError, e:
        print errno.errorcode[e.errno], e
        quit(e.errno)

if __name__  == "__main__":
    main(sys.argv)
