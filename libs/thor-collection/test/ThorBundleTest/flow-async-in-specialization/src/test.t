/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class Async{}
class Flow{}
class Seq{}

class Runner<ExecType>{}

class Runner<ExecType : Seq>
{
    public function run<FuncType>(func : FuncType) : void
    {
        func();
    }
}

class Runner<ExecType : Async>
{
    public function run<FuncType>(func : FuncType) : void
    {
        async-> ok = func();
    }

    public function is_ok() : void
    {
        if(ok == 1)
            exit(0);
        else
        {
            if(count >= 10)
                exit(-3);

            ++count;
            async->is_ok();
        }
    }

    public static var ok : int32 = 0;

    private var count : int32 = 0;
}

class Runner<ExecType : Flow>
{
    public function run<FuncType>(func : FuncType) : void
    {
        flow ->
        {
            func();
        }
    }
}

var result : int32 = 0;

@entry
task test()
{
    var run_seq   : Runner<Seq>   = new Runner<Seq>();
    var run_flow  : Runner<Flow>  = new Runner<Flow>();
    var run_async : Runner<Async> = new Runner<Async>();

    var foo = lambda() : int32 {
        result = 1;
        return result;
    };

    run_seq.run(foo);
    if(result != 1)
        exit(-1);
    result = 0;

    run_flow.run(foo);
    if(result != 1)
        exit(-2);
    result = 0;

    run_async.run(foo);
    run_async.is_ok();
}
