#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

#!/usr/bin/python
import sys
sys.path.insert(0, "../../")
sys.path.insert(0, "./script/")

import lib 
from lib.baselib import *

#
# Run and check
#
gdb.Breakpoint("test.t:test2")
set_global_config(white_list = ['test.t'])
#lazy_generate_mode('i', 'j', 'x', threshold=[0, 10], max_step=50)

try:
        execute_check("run", 16)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 17)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 18)
        execute_check_ex("s", {"i":0, "j":0, "x":0, "EXPECT_FILE":"test.t"}, 21)
        execute_check_ex("s", {"i":0, "j":0, "x":0, "EXPECT_FILE":"test.t"}, 22)
        execute_check_ex("s", {"i":0, "j":0, "x":0, "EXPECT_FILE":"test.t"}, 26)
        execute_check_ex("s", {"i":0, "j":0, "x":0, "EXPECT_FILE":"test.t"}, 27)
        execute_check_ex("s", {"i":0, "j":0, "x":1, "EXPECT_FILE":"test.t"}, 31)
        execute_check_ex("s", {"i":0, "j":0, "x":1, "EXPECT_FILE":"test.t"}, 28)
        execute_check_ex("s", {"i":0, "j":1, "x":1, "EXPECT_FILE":"test.t"}, 27)
        execute_check_ex("s", {"i":0, "j":1, "x":2, "EXPECT_FILE":"test.t"}, 31)
        execute_check_ex("s", {"i":0, "j":1, "x":2, "EXPECT_FILE":"test.t"}, 28)
        execute_check_ex("s", {"i":0, "j":2, "x":2, "EXPECT_FILE":"test.t"}, 27)
        execute_check_ex("s", {"i":0, "j":2, "x":2, "EXPECT_FILE":"test.t"}, 23)
        execute_check_ex("s", {"i":1, "j":2, "x":2, "EXPECT_FILE":"test.t"}, 22)
        execute_check_ex("s", {"i":1, "j":2, "x":2, "EXPECT_FILE":"test.t"}, 26)
        execute_check_ex("s", {"i":1, "j":0, "x":2, "EXPECT_FILE":"test.t"}, 27)
        execute_check_ex("s", {"i":1, "j":0, "x":3, "EXPECT_FILE":"test.t"}, 31)
        execute_check_ex("s", {"i":1, "j":0, "x":3, "EXPECT_FILE":"test.t"}, 28)
        execute_check_ex("s", {"i":1, "j":1, "x":3, "EXPECT_FILE":"test.t"}, 27)
        execute_check_ex("s", {"i":1, "j":1, "x":4, "EXPECT_FILE":"test.t"}, 31)
        execute_check_ex("s", {"i":1, "j":1, "x":4, "EXPECT_FILE":"test.t"}, 28)
        execute_check_ex("s", {"i":1, "j":2, "x":4, "EXPECT_FILE":"test.t"}, 27)
        execute_check_ex("s", {"i":1, "j":2, "x":4, "EXPECT_FILE":"test.t"}, 23)
        execute_check_ex("s", {"i":2, "j":2, "x":4, "EXPECT_FILE":"test.t"}, 22)
        execute_check_ex("s", {"i":2, "j":2, "x":4, "EXPECT_FILE":"test.t"}, 34)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 35)
except MismatchException, e:
        gdb.execute('quit 1') # quit with error code
