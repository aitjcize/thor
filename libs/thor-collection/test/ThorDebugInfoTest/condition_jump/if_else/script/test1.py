#!/usr/bin/python
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import sys
sys.path.insert(0, "../../")
sys.path.insert(0, "./script/")

import lib
from lib.baselib import *

#
# Run and check
#
gdb.Breakpoint("test.t:test1")

set_global_config(white_list = ['test.t'])
#lazy_generate_mode('x', 'y', 'a', 'b', 'c', threshold=[0, 50], max_step=50)

try:
        execute_check("run", 3)
        execute_check_ex("s", {"x":32, "EXPECT_FILE":"test.t"}, 4)
        execute_check_ex("s", {"x":32, "y":42, "EXPECT_FILE":"test.t"}, 5)
        execute_check_ex("s", {"x":32, "y":42, "a":10, "EXPECT_FILE":"test.t"}, 6)
        execute_check_ex("s", {"x":32, "y":42, "a":10, "b":9, "EXPECT_FILE":"test.t"}, 7)
        execute_check_ex("s", {"x":32, "y":42, "a":10, "b":9, "c":8, "EXPECT_FILE":"test.t"}, 9)
        execute_check_ex("s", {"x":32, "y":42, "a":10, "b":9, "c":8, "EXPECT_FILE":"test.t"}, 17)
        execute_check_ex("s", {"x":32, "y":42, "a":32, "b":9, "c":8, "EXPECT_FILE":"test.t"}, 18)
        execute_check_ex("s", {"x":32, "y":42, "a":30, "b":9, "c":8, "EXPECT_FILE":"test.t"}, 22)
        execute_check_ex("s", {"x":32, "y":42, "a":30, "b":9, "c":8, "EXPECT_FILE":"test.t"}, 23)
        execute_check_ex("s", {"x":32, "y":42, "a":42, "b":9, "c":8, "EXPECT_FILE":"test.t"}, 29)
        execute_check_ex("s", {"x":32, "y":42, "a":42, "b":9, "c":8, "EXPECT_FILE":"test.t"}, 34)
        execute_check_ex("s", {"x":32, "y":42, "a":42, "b":9, "c":8, "EXPECT_FILE":"test.t"}, 36)
        execute_check_ex("s", {"x":32, "y":3, "a":42, "b":9, "c":8, "EXPECT_FILE":"test.t"}, 37)
        execute_check_ex("s", {"x":32, "y":3, "a":42, "b":9, "c":8, "EXPECT_FILE":"test.t"}, 43)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 44)
except MismatchException, e:
        gdb.execute('quit 1') # quit with error code
