/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
interface I
{
	public function interesting() : int32;
}

class C
{
	public function new() : void
	{
		vc = 0;
	}
	public virtual function f(a : int32) : void
	{
		vc = a;
	}
	public var vc : int32;
}

class B extends C
{	
	public function new() : void
	{
		vb = 0;
	}

	public virtual function f(a : int16) : void
	{
		vb = a;
	}
	public var vb : int16;
}

class A extends B implements I
{
	public function new() : void
	{
	}
	public virtual function interesting() : int32
	{
		return 1234;
	}
}

@native
function getObject() : A;

@entry task test1() : int32
{
	var obj : A = getObject();
	var x : int32 = obj.interesting();
	
	obj.f(10000);

	var y : int32 = obj.vb;

	y = y + 1;
	exit(0);
	return -1;
}
