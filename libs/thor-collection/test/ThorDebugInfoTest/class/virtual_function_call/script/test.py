#!/usr/bin/python
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import sys
sys.path.insert(0, "../../")
sys.path.insert(0, "./script/")

import lib
from lib.baselib import *

#
# Run and check
#
gdb.Breakpoint("test.t:test1")

'''
set_global_config(white_list = ['test.t', 'native.cpp'])
lazy_generate_mode('x', 'obj.vb', 'obj.vc', 'y', 'this.vb', 'this.vc', 'vb', 'vc', 'a', threshold=[0, 20000], max_step=30)
'''

set_global_config(white_list = ['test.t', 'native.cpp'])
try:
        execute_check("run", 47)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 49)
        execute_check_ex("s", {"EXPECT_FILE":"native.cpp"}, 52)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 35)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 21)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 8)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 10)
        execute_check_ex("s", {"this.vc":0, "vc":0, "EXPECT_FILE":"test.t"}, 11)
        execute_check_ex("s", {"this.vc":0, "vc":0, "EXPECT_FILE":"test.t"}, 23)
        execute_check_ex("s", {"this.vb":0, "this.vc":0, "vb":0, "vc":0, "EXPECT_FILE":"test.t"}, 24)
        execute_check_ex("s", {"this.vb":0, "this.vc":0, "vb":0, "vc":0, "EXPECT_FILE":"test.t"}, 37)
        execute_check_ex("s", {"EXPECT_FILE":"native.cpp"}, 53)
        execute_check_ex("s", {"obj.vb":0, "obj.vc":0, "EXPECT_FILE":"test.t"}, 50)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 38)
        execute_check_ex("s", {"this.vb":0, "this.vc":0, "vb":0, "vc":0, "EXPECT_FILE":"test.t"}, 40)
        execute_check_ex("s", {"this.vb":0, "this.vc":0, "vb":0, "vc":0, "EXPECT_FILE":"test.t"}, 41)
        execute_check_ex("s", {"x":1234, "obj.vb":0, "obj.vc":0, "EXPECT_FILE":"test.t"}, 52)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 26)
        execute_check_ex("s", {"this.vb":0, "this.vc":0, "vb":0, "vc":0, "a":10000, "EXPECT_FILE":"test.t"}, 28)
        execute_check_ex("s", {"this.vb":10000, "this.vc":0, "vb":10000, "vc":0, "a":10000, "EXPECT_FILE":"test.t"}, 29)
        execute_check_ex("s", {"x":1234, "obj.vb":10000, "obj.vc":0, "EXPECT_FILE":"test.t"}, 54)
        execute_check_ex("s", {"x":1234, "obj.vb":10000, "obj.vc":0, "y":10000, "EXPECT_FILE":"test.t"}, 56)
        execute_check_ex("s", {"x":1234, "obj.vb":10000, "obj.vc":0, "y":10001, "EXPECT_FILE":"test.t"}, 57)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 58)
except MismatchException, e:
        gdb.execute('quit 1') # quit with error code
