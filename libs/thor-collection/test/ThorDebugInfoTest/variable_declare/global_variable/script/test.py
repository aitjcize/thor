#!/usr/bin/python
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import sys
sys.path.insert(0, "../../")
sys.path.insert(0, "./script/")

import lib
from lib.baselib import *

#
# Run and check
#
gdb.Breakpoint("test.t:test1")

set_global_config(white_list = ['test.t'])
#lazy_generate_mode('pl::global_value', 'pl::pl2::global_value', 'gV', 'gA.x', 'gA->x', threshold=[0, 100000], max_step=50)

try:
        execute_check("run", 21)
        execute_check_ex("s", {"pl::global_value":10, "pl::pl2::global_value":8, "gV":0, "EXPECT_FILE":"test.t"}, 14)
        execute_check_ex("s", {"pl::global_value":10, "pl::pl2::global_value":8, "gV":0, "EXPECT_FILE":"test.t"}, 16)
        execute_check_ex("s", {"pl::global_value":10, "pl::pl2::global_value":8, "gV":99999, "EXPECT_FILE":"test.t"}, 17)
        execute_check_ex("s", {"pl::global_value":10, "pl::pl2::global_value":8, "gV":99999, "EXPECT_FILE":"test.t"}, 17)
        execute_check_ex("s", {"pl::global_value":10, "pl::pl2::global_value":8, "gV":99999, "EXPECT_FILE":"test.t"}, 2)
        execute_check_ex("s", {"pl::global_value":10, "pl::pl2::global_value":8, "gV":99999, "EXPECT_FILE":"test.t"}, 18)
        execute_check_ex("s", {"pl::global_value":10, "pl::pl2::global_value":8, "gV":99999, "EXPECT_FILE":"test.t"}, 24)
        execute_check_ex("s", {"pl::global_value":10, "pl::pl2::global_value":8, "gV":99999, "EXPECT_FILE":"test.t"}, 4)
        execute_check_ex("s", {"pl::global_value":10, "pl::pl2::global_value":8, "gV":99999, "EXPECT_FILE":"test.t"}, 6)
        execute_check_ex("s", {"pl::global_value":10, "pl::pl2::global_value":8, "gV":99999, "gA.x":1234, "gA->x":1234, "EXPECT_FILE":"test.t"}, 7)
        execute_check_ex("s", {"pl::global_value":10, "pl::pl2::global_value":8, "gV":99999, "gA.x":1234, "gA->x":1234, "EXPECT_FILE":"test.t"}, 25)
        execute_check_ex("s", {"pl::global_value":10, "pl::pl2::global_value":8, "gV":99999, "gA.x":1234, "gA->x":1234, "EXPECT_FILE":"test.t"}, 26)
except MismatchException, e:
        gdb.execute('quit 1') # quit with error code

