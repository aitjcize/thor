#!/usr/bin/python
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import sys
sys.path.insert(0, "../../")
sys.path.insert(0, "./script/")

import lib
from lib.baselib import execute_check, MismatchException

#
# Run and check
#
gdb.Breakpoint("test.t:test1")

try:
    set_global_config(white_list = ['test.t'])
	execute_check("run",  8 )
	execute_check("s", 10)
	execute_check("s", 1)
	execute_check("s", 3, a = 100, b = 200)
	execute_check("s", 4, x = 0, a = 100, b = 200)
	execute_check("s", 5, x = 300, a = 100, b = 200)
	execute_check("s", 6, a = 100, b = 200)
	execute_check("s", 10)
	execute_check("s", 11, x = 300)
	execute_check("s", 12)
	gdb.execute('quit')
except MismatchException, e:
	gdb.execute('quit 1') # quit with error code

