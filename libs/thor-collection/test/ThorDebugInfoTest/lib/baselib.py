#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import gdb
import os 


#########################################################
#
# Global configuration
#
#########################################################
WHITE_LIST = []


#########################################################
#
# Customized Exception
#
#########################################################
class MismatchException(Exception):
    def __init__(self):
        pass

#########################################################
#
# Utility Function
#
#########################################################
def get_current_line():
    try:
        frame = gdb.selected_frame()
        sym = frame.find_sal()
        return sym.line
    except:
        return None

def get_current_file():
    try:
        frame = gdb.selected_frame()
        sym = frame.find_sal()
        return os.path.basename(sym.symtab.filename)
    except:
        return None

#########################################################
#
# Expose API
#
#########################################################
def set_global_config(**args):
    global WHITE_LIST

    if args.has_key('white_list'):
        WHITE_LIST = args['white_list']

def lazy_generate_mode(*vars, **flags):
    """
        When to use it:
            Sometimes, we are too lazy to examine each step and filled out the variables we want to examine
            when generating the test script. An alternative way is to first attach the target program, and 
            make sure the tracing is correct. Then use this function to generate the script.

        example:
            gdb.Breakpoint("test.t:test1")
            set_global_config(white_list = ['test.t', 'native.cpp'])
            lazy_generate_mode('a.x', 'a.member.y', 'x', 'y', 'a->x', threshold=[0, 100], max_step=50)
    """
    global WHITE_LIST

    output_buffer = """
#!/usr/bin/python
import sys
sys.path.insert(0, "../../")
sys.path.insert(0, "./script/")

import lib
from lib.baselib import *

"""
    if WHITE_LIST:
        output_buffer += 'set_global_config(white_list = ' + str(WHITE_LIST) + ')\n'

    output_buffer += "try:\n"

    gdb.execute("run")
    output_buffer += '\texecute_check("run", ' + str(get_current_line()) + ')\n'
    
    max_steps = 30
    if flags.has_key('max_step'):
        max_steps = flags['max_step']

    threshold = []
    if flags.has_key('threshold'):
        threshold = flags['threshold']

    step = 0
    while step < max_steps:
        try:
            gdb.execute("s")
    
            # check if the current file is in the white list
            if WHITE_LIST:
                if get_current_file() not in WHITE_LIST:
                    gdb.execute("fin")
                    continue
        except: # we finish running
            break

        output_buffer += '\texecute_check_ex("s", {'
        for var in vars:
            try:
                value = gdb.parse_and_eval(var)

                if threshold and not (threshold[1] >= value >= threshold[0]):
                    continue

                output_buffer += '"' + str(var) + '":' + str(value) + ', '
            except:
                continue
        output_buffer += '"EXPECT_FILE":"' + str(get_current_file()) + '"}, ' + str(get_current_line()) + ')\n'
        step += 1

    output_buffer += "except MismatchException, e:\n"
    output_buffer += "\tgdb.execute('quit 1') # quit with error code\n"

    print output_buffer


def __check_value(value, expected):
    result = True

    if type(value) is float or type(expected) is float:
        if abs(value - expected) > 0.01:
            result = False
    elif value != expected:
        result = False

    return result

def execute_check_ex(command, eval_expr, *args):
    execute_check(command, *args, **eval_expr)

def execute_check(command, *args, **eval_expr):
    """
        FORMAT: eval_expr = {'expr': expect value}
    
        execute and check whether the operation meets the expected value
    """
    gdb.execute(command)

    if WHITE_LIST:
        while get_current_file() not in WHITE_LIST:
            # TOFIX: uh, the command should be something to trigger program counter
            gdb.execute('fin')
            gdb.execute(command)
            continue

    if not args and not eval_expr:
        return

    expect_line = None
    if args:
        expect_line = args[0]

    # Check expected line
    current_line = get_current_line()
    result = True 
    if expect_line and current_line != expect_line:
        result = False
        print 'Line mismatched : ', current_line, ' expected: ', expect_line

    # Check evaluation of expression
    if eval_expr and result:
        for expr, expected in eval_expr.items():
            if expr == 'EXPECT_FILE':
                value = get_current_file()
            else:
                value = gdb.parse_and_eval(expr)

            if not __check_value(value, expected):
                result = False
                print current_line, ': Mismatched: ', expr, ' = ', value, ' expected: ', expected
                break

    if not result:
        raise MismatchException()

