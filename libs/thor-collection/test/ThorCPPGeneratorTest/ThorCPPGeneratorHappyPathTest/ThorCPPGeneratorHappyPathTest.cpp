/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/filesystem.hpp>
#include "language/stage/generator/visitor/CPPGeneratorVisitor.h"
#include "../../ThorTreeTest/ASTNodeSamples.h"

#define BOOST_TEST_MODULE ThorCPPGeneratorTest_ThorCPPGeneratorHappyPathTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE( ThorCPPGeneratorTest_ThorCPPGeneratorHappyPathTestSuite )

BOOST_AUTO_TEST_CASE( ThorCPPGeneratorTest_ThorCPPGeneratorHappyPathTestCase1 )
{
    using namespace zillians::language::tree;
    // class some_class {
    //     public function some_member_function() {
    //         var a : some_class;
    //         var b : some_class;
    //         a = b;
    //     }
    //     private static function static_member_function() {
    //     }
    //     public var some_member_variable1 : some_class;
    //     public var some_member_variable2 : some_class;
    //     public static var static_variable3 : some_class;
    // }
    Tangle* tangle = cast<Tangle>(createSample6());
    Source* source = tangle->sources.begin()->second;

    std::ostringstream oss;
    zillians::language::stage::visitor::ThorCPPGeneratorStageVisitor cppGenerator(oss);

    cppGenerator.visit(*tangle);

    std::cerr << oss.str() << std::endl;
    BOOST_ASSERT_EQUAL(oss.str().empty(), false);
}

BOOST_AUTO_TEST_SUITE_END()
