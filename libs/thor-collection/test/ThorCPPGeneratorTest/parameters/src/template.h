/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#ifndef TEMPLATE_H
#define TEMPLATE_H

#include <type_traits>

template<typename T>
void callee(T) {
    constexpr bool is_void     = std::is_same <                             T       ,                 void >::value;
    constexpr bool is_integer  = std::is_same <                             T       , ::thor::lang::  int8 >::value ||
                                 std::is_same <                             T       , ::thor::lang::  int16>::value ||
                                 std::is_same <                             T       , ::thor::lang::  int32>::value ||
                                 std::is_same <                             T       , ::thor::lang::  int64>::value;
    constexpr bool is_floating = std::is_same <                             T       , ::thor::lang::float32>::value ||
                                 std::is_same <                             T       , ::thor::lang::float64>::value;
    constexpr bool is_enum     = std::is_enum <                             T                              >::value;
    constexpr bool is_class    = std::is_class<typename std::remove_pointer<T>::type                       >::value;

    static_assert(
        is_void     ||
        is_integer  ||
        is_floating ||
        is_enum     ||
        is_class,
        "unexpected explicit instantiation from cppgen"
    );
}

#endif /* TEMPLATE_H */
