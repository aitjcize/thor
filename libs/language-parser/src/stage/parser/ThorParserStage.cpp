/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/stage/parser/ThorParserStage.h"
#include "language/context/ConfigurationContext.h"
#include "language/context/ParserContext.h"
#include "language/grammar/Thor.h"
#include "language/action/SemanticActions.h"
#include "language/tree/visitor/PrettyPrintVisitor.h"
#include "language/ThorCompiler.h"
#include "utility/Foreach.h"
#include "utility/UnicodeUtil.h"
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

#include <cwchar>

namespace classic = boost::spirit::classic;
namespace qi = boost::spirit::qi;

namespace zillians { namespace language { namespace stage {

namespace {

// since '\t' may be printed in spaces and I don't know a way to change std::wcout, we simply replace the tab with desired number of spaces
// so that we can have correct error pointing cursor
// (note that because '\t' equals to 4 spaces reported by spirit, we have to make sure it's printed in the exact same way)
static void expand_tabs(const std::wstring& input, std::wstring& output, int number_of_space_for_tab = 4)
{
    for(std::wstring::const_iterator it = input.begin(); it != input.end(); ++it)
    {
        if(*it == '\t')
            for(int i=0;i<number_of_space_for_tab;++i) output.push_back(L' ');
        else
            output.push_back(*it);
    }
}

// see: http://stackoverflow.com/questions/1746136/how-do-i-normalize-a-pathname-using-boostfilesystem/1750710#1750710
static boost::filesystem::path resolve(const boost::filesystem::path& p)
{
    //p = boost::filesystem::absolute(p);
    boost::filesystem::path result;
    for(boost::filesystem::path::iterator it=p.begin();
        it!=p.end();
        ++it)
    {
        if(*it == "..")
        {
            // /a/b/.. is not necessarily /a if b is a symbolic link
            if(boost::filesystem::is_symlink(result) )
                result /= *it;
            // /a/b/../.. is not /a/b/.. under most circumstances
            // We can end up with ..s in our result because of symbolic links
            else if(result.filename() == "..")
                result /= *it;
            // Otherwise it should be safe to resolve the parent
            else
                result = result.parent_path();
        }
        else if(*it == ".")
        {
            // Ignore
        }
        else
        {
            // Just cat other path entries
            result /= *it;
        }
    }
    return result;
}

static bool enumerate_package(const boost::filesystem::path& root, const boost::filesystem::path& p, std::deque<std::wstring>& sequence)
{
    boost::filesystem::path t = p.parent_path();

    while (true)
    {
        if (t.empty())
            return false;

        if (t == root)
            break;
        else
            sequence.push_front(t.stem().wstring());

        t = t.parent_path();
    }

    return true;
}

static boost::filesystem::path normalize_path(boost::filesystem::path p)
{
    if(p.is_absolute())
        return resolve(p);
    else
        return resolve(boost::filesystem::absolute(p));
}

}

ThorParserStage::ThorParserStage() :
        debug_parser(false),
        debug_ast(false),
        debug_ast_with_loc(false),
        use_relative_path(false),
        dump_ts(false),
        dump_graphviz(false),
        no_system(false),
        keep_going(false),
        emit_debug_info(false),
        opt_level(0)
{ }

ThorParserStage::~ThorParserStage()
{ }

const char* ThorParserStage::name()
{
    return "Parser Stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> ThorParserStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options()
        ("root-dir", po::value<std::string>(), "root source directory");

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options()
    	("arch-flags", po::value<Architecture::flag_t>(), "default architecture flags")
        ("debug-parser", "dump parsing tree for debugging purpose")
        ("debug-parser-ast", "dump parsed abstract syntax tree for debugging purpose")
        ("debug-parser-ast-with-loc", "dump parsed abstract syntax tree with location for debugging purpose")
        ("use-relative-path", "use relative file path instead of absolute path (for debugging info generation)")
        ("dump-ts", "dump intermediate AST in Thor format")
        ("dump-ts-dir", po::value<std::string>(), "directory to dump intermediate AST in Thor format")
        ("dump-graphviz", po::bool_switch(), "dump intermediate AST in graphviz format")
        ("dump-graphviz-dir", po::value<std::string>(), "directory to dump intermediate AST in graphviz format")
        ("no-system", "no system bundle")
        ("prepend-package", po::value<std::string>(), "prepend package to all sources")
        ("emit-debug-info", po::bool_switch(), "generate debug info for debugging if on")
        ("optimization-level,O", po::value<unsigned>()->default_value(0u), "Set the optimization level (0~3), and the default opt level for release build is 2 while for debug build is 0. However, in debug build, you could explicitly set optimizeation level which would overwritten the default value.")
    ;

    return std::make_pair(option_desc_public, option_desc_private);
}

bool ThorParserStage::parseOptions(po::variables_map& vm)
{
    debug_parser = (vm.count("debug-parser") > 0);
    debug_ast = (vm.count("debug-parser-ast") > 0);
    debug_ast_with_loc = (vm.count("debug-parser-ast-with-loc") > 0);
    use_relative_path = (vm.count("use-relative-path") > 0);
    dump_ts = (vm.count("dump-ts") > 0);
    dump_graphviz = vm["dump-graphviz"].as<bool>();
    emit_debug_info = vm["emit-debug-info"].as<bool>();
    opt_level       = vm["optimization-level"].as<unsigned>();

    if(vm.count("arch-flags") > 0)
    {
    	Architecture::flag_t archs = vm["arch-flags"].as<Architecture::flag_t>();

    	Architecture::default_ = Architecture(archs);
    }

    if(vm.count("dump-ts-dir") > 0)
    {
        dump_ts_dir = vm["dump-ts-dir"].as<std::string>();
    }

    if(vm.count("dump-graphviz-dir") > 0)
    {
        dump_graphviz_dir = vm["dump-graphviz-dir"].as<std::string>();
    }
    if(vm.count("prepend-package"))
    {
        prepend_package = vm["prepend-package"].as<std::string>();
    }

    if(vm.count("root-dir") == 0)
        root_dir = boost::filesystem::current_path() / "src";
    else
    {
        root_dir = vm["root-dir"].as<std::string>();
        root_dir = normalize_path(root_dir);
    }

    if(vm.count("input") == 0)
    {
        LOG4CXX_ERROR( LoggerWrapper::ParserStage, "error: does not provide any input sources" );
        return false;
    }

    if(vm.count("no-system") > 0)
    {
        no_system = true;
    }

    if(vm.count("mode-parse") > 0)
    {
        keep_going = true;
    }


    inputs = vm["input"].as<std::vector<std::string>>();
    return true;
}

bool ThorParserStage::execute(bool& continue_execution)
{
    UNUSED_ARGUMENT(continue_execution);

    if(keep_going)
        continue_execution = true;

    // prepare the global parser context
    if(!hasParserContext())
        setParserContext(new ParserContext());

    for(boost::filesystem::path p : inputs)
    {
#if defined(_WIN32)
        if(wcscmp(p.extension().c_str(), L".t") == 0)
#else
        if(strcmp(p.extension().c_str(), ".t") == 0)
#endif
            if(!parse(p))
                return false;
    }

    if(getParserContext().tangle && (debug_ast || debug_ast_with_loc))
    {
        tree::visitor::PrettyPrintVisitor printer(debug_ast_with_loc);
        printer.visit(*getParserContext().tangle);
    }

    auto& tree_actions = getParserContext().tree_actions;
    for(auto& action : tree_actions)
    {
        action();
    }
    tree_actions.clear();

    return !(LoggerWrapper::instance()->hasError() || LoggerWrapper::instance()->hasFatal());
}

static void addImportSystem(tree::Tangle* tangle)
{
    // You must not import system in "thor.lang"

    bool  should_import_thor_lang = true;
    auto& active_source = *getParserContext().active_source;

    for(auto* i : active_source.imports)
    {
        Import& import = *i;

        // only 'import .= thor.lang' is ignored
        if(import.alias && import.alias->toString().empty() && import.ns->toString() == L"thor.lang")
        {
            should_import_thor_lang = false;

            break;
        }
    }

    if(should_import_thor_lang)
    {
        NestedIdentifier* import_id = new NestedIdentifier();

        import_id->appendIdentifier(new SimpleIdentifier(L"thor"));
        import_id->appendIdentifier(new SimpleIdentifier(L"lang"));

        Import* import = new Import(new SimpleIdentifier(L""), import_id);

        ASTNodeHelper::foreachApply<ASTNode>(*import, [](ASTNode& node){
            SourceInfoContext::set(&node, new SourceInfoContext(0, 1));
        });

        active_source.addImport(import);
    }
}

struct IsDot {
    bool operator()(const wchar_t c) const {
        return c == L'.';
    }
};

bool ThorParserStage::parse(const boost::filesystem::path& p)
{
    std::deque<std::wstring> parent_sequence;
    if (!enumerate_package(root_dir, normalize_path(p), parent_sequence))
    {
        LOG4CXX_ERROR(LoggerWrapper::ParserStage, "failed to enumerate package for file: " << p.string());
        return false;
    }

    if(!prepend_package.empty())
    {
        std::vector<std::wstring> v;
        std::wstring ws = s_to_ws(prepend_package);
        // boost::split take string as an l-value
        // can't boost::split(v, s_to_ws(...), ...);
        boost::split(v, ws, IsDot());
        parent_sequence.insert(parent_sequence.end(), v.begin(), v.end());
    }

    // map the created program by the nested identifier as its key
    if(!getParserContext().tangle)
    {
        const auto&      project_id = hasConfigurationContext() ? getConfigurationContext().global_config.manifest.project_id : UUID::nil();
              auto*const tangle     = project_id.is_nil() ? new Tangle(UUID::random()) : new Tangle(project_id);

        BOOST_ASSERT(tangle->config != nullptr && "null pointer exception");
        tangle->config->emit_debug_info = emit_debug_info;
        tangle->config->opt_level       = opt_level;

        getParserContext().tangle = tangle;
    }

    getParserContext().active_source = new Source(p.string());
    SourceInfoContext::set(getParserContext().active_source, new SourceInfoContext(0, 0)); // for logger, just in case
    getParserContext().active_package = getParserContext().tangle->root;

    if(parent_sequence.size() > 0)
    {
        for(auto& i : parent_sequence)
        {
            Package* package = getParserContext().active_package->findPackage(i);

            if(package == NULL)
            {
                package = new Package(new SimpleIdentifier(i));
                getParserContext().active_package->addPackage(package);
            }

            getParserContext().active_package = package;
        }
    }

    getParserContext().active_package->addSource(getParserContext().active_source);

    // save the greatest last modification time-stamp of all sources
    // the serialized AST file will use that time-stamp for thor-make to check for dirty tangles
    std::time_t t = boost::filesystem::last_write_time(p);
    if(getParserContext().last_modification_time < t)
        getParserContext().last_modification_time = t;

    std::ifstream in(p.string(), std::ios_base::in);

    // try to speed up the ifstream object (as suggested in http://stackoverflow.com/questions/9371238/why-is-reading-lines-from-stdin-much-slower-in-c-than-python)
    in.sync_with_stdio(false);

    // get the file size in advance to avoid unnecessary re-allocation of string buffer
    std::ifstream::pos_type file_size = in.tellg();
    in.seekg(0, std::ios::end);
    file_size = in.tellg() - file_size;
    in.seekg(0, std::ios::beg);

    if(!in.good())
    {
        LOG4CXX_ERROR(LoggerWrapper::ParserStage, "failed to open file: " << p.string());
        return false;
    }

    // ignore the BOM marking the beginning of a UTF-8 file in Windows
    if(in.peek() == '\xef')
    {
        char s[4];
        in >> s[0] >> s[1] >> s[2];
        s[3] = '\0';
        if (s != std::string("\xef\xbb\xbf"))
        {
            std::cerr << "parser error: unexpected characters from input file: " << p.string() << std::endl;
            return false;
        }
    }

    // TODO avoid this copy but use iterator adopter to covert UTF8 input to UCS4 directly
    std::string source_code_raw; source_code_raw.reserve((std::string::size_type)file_size + 1);
    in.unsetf(std::ios::skipws); // disable white space skipping
    std::copy(std::istream_iterator<char>(in), std::istream_iterator<char>(), std::back_inserter(source_code_raw));

    // convert it from UTF8 into UCS4 as a string by using u8_to_u32_iterator
    std::wstring source_code; source_code.reserve((std::string::size_type)file_size + 1);
    utf8_to_ucs4(source_code_raw, source_code);

	try
	{
    	// enable correct locale so that we can print UCS4 characters
	    enable_default_locale(std::wcout);
	}
	catch (std::runtime_error& e)
	{
		// the default locale does not accpeted by the system, maybe user had define wrong locale setting
		// in this case, we use the safer one
		enable_c_locale(std::wcout);
	}

    getParserContext().dump_rule_debug = debug_parser;
    getParserContext().enable_semantic_action = !debug_parser;
    getParserContext().debug.line = 1;
    getParserContext().debug.column = 1;

    // try to parse
    typedef classic::position_iterator2<std::wstring::iterator> pos_iterator_type;
    try
    {
        pos_iterator_type begin(source_code.begin(), source_code.end(), p.wstring());
        pos_iterator_type end;

        grammar::Thor<pos_iterator_type, action::ThorTreeAction> parser;
        grammar::detail::WhiteSpace<pos_iterator_type> skipper;

        if(!qi::phrase_parse(
                begin, end,
                parser,
                skipper))
        {
            return false;
        }
        // add import system;
        if(!no_system)
            addImportSystem(getParserContext().tangle);
    }
    catch (const qi::expectation_failure<pos_iterator_type>& e)
    {
        const classic::file_position_base<std::wstring>& pos = e.first.get_position();

        // TODO output error using Logger
        std::wstring current_line;
        expand_tabs(e.first.get_currentline(), current_line);
        std::wcerr << L"parse error at file " << pos.file << L" line " << pos.line
                << L" column " << pos.column << std::endl
                << L"'" << current_line << L"'" << std::endl
                << std::setw(pos.column) << L" " << L"^- here" << std::endl;

        return false;
    }

    if(dump_ts)
    {
        boost::filesystem::path p(dump_ts_dir);
        getParserContext().tangle->toSource(p / "post-parse.t");
    }

    if(dump_graphviz)
    {
        boost::filesystem::path p(dump_graphviz_dir);
        ASTNodeHelper::visualize(getParserContext().tangle, p / "post-parse.dot");
    }

    return true;
}

} } }

