/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <ctime>

#include <iostream>
#include <algorithm>
#include <vector>
#include <unordered_set>

#include <boost/assert.hpp>

#include "utility/Filesystem.h"
#include "utility/archive/Archive.h"
#include "utility/UnicodeUtil.h"
#include "utility/sha1.h"

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/stage/bundle/ThorBundleStage.h"
#include "language/stage/tree_refactor/detail/TreeContextRefactorer.h"
#include "language/stage/serialization/detail/ASTSerializationHelper.h"

namespace zillians { namespace language { namespace stage {

namespace {

bool addArchiveItemImpl(/*INOUT*/ ArchiveItem& item, const std::time_t &t, std::istream& input)
{
    struct std::tm* timeinfo = std::localtime(&t);

    item.zip_info.tmz_date.tm_year = timeinfo->tm_year;
    item.zip_info.tmz_date.tm_mon = timeinfo->tm_mon;
    item.zip_info.tmz_date.tm_mday = timeinfo->tm_mday;
    item.zip_info.tmz_date.tm_hour = timeinfo->tm_hour;
    item.zip_info.tmz_date.tm_min = timeinfo->tm_min;
    item.zip_info.tmz_date.tm_sec = timeinfo->tm_sec;

    input.seekg(0, std::ios::end);
    item.buffer.resize(input.tellg());
    input.seekg(0, std::ios::beg);
    input.read((char*)&item.buffer[0], item.buffer.size());

    return true;
}

bool addArchiveItem(std::istream& input, /*INOUT*/ ArchiveItem& item)
{
    const std::time_t& t = std::time(nullptr);

    if(!input)
        return false;

    return addArchiveItemImpl(item, t, input);
}

bool addArchiveItem(const boost::filesystem::path& p, /*INOUT*/ ArchiveItem& item)
{
    // update time stamp
    if(!Filesystem::is_regular_file(p)) return false;

    const std::time_t& t = boost::filesystem::last_write_time(p);

    // get the raw buffer from file
    std::ifstream fin(p.string(), std::ios::in | std::ios::binary | std::ios::ate);
    if(!fin)
        return false;

    return addArchiveItemImpl(item, t, fin);
}

}

ThorBundleStage::ThorBundleStage()
{
    config.loadDefault(true, false, false);
}

ThorBundleStage::~ThorBundleStage()
{ }

const char* ThorBundleStage::name()
{
    return "bundle_stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> ThorBundleStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options()
        ("compress,c", "compress and create bundle to build directory")
        ("extract,x", "extract bundle to build directory")
        ("root-dir", po::value<std::string>(), "project root directory")
        ("build-path", po::value<std::string>(), "build directory")
        ("extract-bundle-path", po::value<std::string>(), "directory to extract bundle")
    ;

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options();

    return std::make_pair(option_desc_public, option_desc_private);
}

bool ThorBundleStage::parseOptions(po::variables_map& vm)
{
    if(!vm.count("extract") && !vm.count("compress"))
    {
        std::cerr << "Error: either 'compress' or 'extract' option should be specified" << std::endl;
        return false;
    }

    if(vm.count("extract"))
        mode = EXTRACT;

    if(vm.count("compress"))
        mode = COMPRESS;

    if(vm.count("root-dir"))
        root_dir = vm["root-dir"].as<std::string>();
    else
        root_dir = boost::filesystem::current_path();

    if(vm.count("build-path"))
        config.build_path_override = vm["build-path"].as<std::string>();

    if(vm.count("extract-bundle-path"))
        config.extract_bundle_path_override = vm["extract-bundle-path"].as<std::string>();

    if(vm.count("input"))
        bundles_to_extract = vm["input"].as<std::vector<std::string>>();

    return true;
}

bool ThorBundleStage::execute(bool& continue_execution)
{
    if(mode == EXTRACT)
        return extract(continue_execution);
    else if(mode == COMPRESS)
        return compress(continue_execution);
    else
    {
        UNREACHABLE_CODE();
        return false;
    }
}

bool ThorBundleStage::extractBundle(const boost::filesystem::path& bundle_path, std::vector<boost::filesystem::path>& all_extracted_bundle_path, bool continue_extract)
{
    const auto extract_to_path = config.extract_bundle_path / bundle_path.filename();

    if(!boost::filesystem::exists(bundle_path))
        return false;

    std::time_t timestamp_of_bundle = boost::filesystem::last_write_time(bundle_path);

    // check if the bundle is up-to-date if the extract folder already exists
    if(Filesystem::is_directory(extract_to_path))
    {
        std::time_t timestamp_of_extract_to_folder = boost::filesystem::last_write_time(extract_to_path);

        if(timestamp_of_extract_to_folder == timestamp_of_bundle)
        {
            all_extracted_bundle_path.push_back(extract_to_path);
            return true;
        }
    }

    Archive ar(bundle_path.string(), ArchiveMode::ARCHIVE_FILE_DECOMPRESS);
    if(!ar.open())
    {
        std::cerr << "Error: failed to decompress bundle file '" << bundle_path.string() << "'" << std::endl;
        return false;
    }

    // extract the bundle to build-path/bundle_file_name/
    std::vector<ArchiveItem> archive_items;
    ar.extractAllToFolder(archive_items, extract_to_path.string());
    ar.close();

    // update the time stamp
    boost::filesystem::last_write_time(extract_to_path, timestamp_of_bundle);

    /**
     * 2. load the config and continually extract depend bundle
     */
    if (continue_extract)
    {
        ThorBuildConfiguration dep_config;
        dep_config.load(extract_to_path.string());

        for(auto& bundle : dep_config.manifest.deps.bundles)
        {
            boost::filesystem::path p;
            if(bundle.lpath.empty())
            {
                // this can be either a verified bundle under home directory or under the current root dependency directory
                if(boost::filesystem::exists(config.sdk_bundle_path / bundle.name))
                {
                    p = config.sdk_bundle_path / bundle.name;
                }
                else if(boost::filesystem::exists(config.extract_bundle_path / bundle.name))
                {
                    p = config.extract_bundle_path / bundle.name;
                }
                else
                {
                    std::cerr << "Error: missing bundle file '" << bundle.name << "'" << std::endl;
                    return false;
                }

            }
            else
            {
            	p = bundle.lpath;
                p /= bundle.name;
            }

            // Well, since the manifest in the bundle has a collection of the native bundles which were a super of the depended
            // native bundles, we don't need to do further extraction (actually, we don't have such bundles)
            if (!extractBundle(p, all_extracted_bundle_path, true))
                return false;
        }
    }

    all_extracted_bundle_path.push_back(extract_to_path);
    return true;
}

bool ThorBundleStage::extract(bool& continue_execution)
{
    UNUSED_ARGUMENT(continue_execution);

    if(!config.loadDefault(false, true, true))
    {
        std::cerr << "Error: failed to load project configuration" << std::endl;
        return false;
    }

    if(bundles_to_extract.size() == 0)
    {
    	// if the bundle to extract is not specified, we look for the configuration to try to extract required bundles
		bool result = true;
		std::vector<boost::filesystem::path> all_extracted_bundle_path;

		for(auto& bundle : config.manifest.deps.bundles)
		{
			// try to locate the dependent bundle
			boost::filesystem::path p;
			if(bundle.lpath.empty())
			{
				// this can be either a verified bundle under home directory or under the current root dependency directory
				if(boost::filesystem::exists(config.sdk_bundle_path / bundle.name))
				{
					p = config.sdk_bundle_path / bundle.name;
				}
				else if(boost::filesystem::exists(config.extract_bundle_path / bundle.name))
				{
					p = config.extract_bundle_path / bundle.name;
				}
				else
				{
					std::cerr << "Error: missing bundle file '" << bundle.name << "'" << std::endl;
					return false;
				}
			}
			else
			{
				p = bundle.lpath;
                p /= bundle.name;
			}

			if (!extractBundle(p, all_extracted_bundle_path, true))
				result = false;
		}

		// remove stale extracted bundle
		std::vector<boost::filesystem::path> all_files_under_dep = Filesystem::collect_files(config.extract_bundle_path, "*", false);
		for(auto& name : all_files_under_dep)
		{
			// only clean up regular files
			// avoid deleting all hidden files (.xxxxx like .cache)
			if(Filesystem::is_directory(name))
			{
				// if we can't find it in the all outputs list, that should be a stale file, then we remove it
				if(std::find_if(
						all_extracted_bundle_path.begin(),
						all_extracted_bundle_path.end(),
						[&](boost::filesystem::path& x) { return boost::filesystem::equivalent(x, name); }) == all_extracted_bundle_path.end())
				{
					std::cout << "removing stale directory: '" << name.string() << "'" << std::endl;
					boost::filesystem::remove_all(name);
				}
			}
		}

	    return result;
    }
    else
    {
		bool result = true;
		std::vector<boost::filesystem::path> all_extracted_bundle_path;

		for(boost::filesystem::path p : bundles_to_extract)
		{
			if (!extractBundle(p, all_extracted_bundle_path, true))
				result = false;
		}

		return result;
    }

}

bool ThorBundleStage::compress(bool& continue_execution)
{
    UNUSED_ARGUMENT(continue_execution);

    if(!config.loadDefault(false, true, true))
    {
        std::cerr << "Error: failed to load project configuration" << std::endl;
        return false;
    }

    boost::filesystem::path output_file = config.binary_output_path / (config.manifest.name + THOR_EXTENSION_BUNDLE);

    // remove the output bundle first if it exists
    if(Filesystem::is_regular_file(output_file))
        boost::filesystem::remove(output_file);

    // create bundle archive
    Archive ar(output_file.string(), ArchiveMode::ARCHIVE_FILE_COMPRESS);

    // enumerate extracted bundles
    std::vector<boost::filesystem::path> all_files_under_dep = Filesystem::collect_files(config.extract_bundle_path, "*", false);
    std::vector<boost::filesystem::path> all_directories_under_dep;
    for(auto& name : all_files_under_dep)
    {
        if(Filesystem::is_directory(name))
        {
            all_directories_under_dep.push_back(name);
        }
    }

    // add merged AST to archive
    // note that the AST was merged by thor-make
    {
        auto       merged_ast_file = (root_dir / "bin" / (config.manifest.name + THOR_EXTENSION_AST)).string();
        auto*const tangle          = tree::cast_or_null<tree::Tangle>(ASTSerializationHelper::deserialize(merged_ast_file));

        if(tangle == nullptr || tangle->root == nullptr)
            return false;

        const auto& bundle_ast = stage::visitor::bundled_ast::create(*tangle);

        std::stringstream reduced_ast_stream;
        ASTSerializationHelper::serialize_bundle_ast(reduced_ast_stream, bundle_ast);

        ArchiveItem item;
        item.filename = "bin/" + (config.manifest.name + THOR_EXTENSION_AST);
        if(!addArchiveItem(reduced_ast_stream, item))
        {
            std::cerr << "Error: failed to compress AST" << std::endl;
            return false;
        }
        ar.add(item);

        // make sure the AST file's integrity
    }

    // add manifest to archive
    {
        // we have to create a temporary manifest file which has unnecessary dependency cleaned up
        // (the static linked object or library should be removed from the dependencies)
        // (the verified native bundles of dependent bundle should be also merged to the dependencies)
        boost::filesystem::path temporary_manifest;
        {
            ThorManifest manifest = config.manifest;
            manifest.deps.native_objects.clear();
            manifest.deps.native_libraries.clear();

            // Collect the existed bundle dependencies
            std::unordered_set<std::string> existed_bundle;
            std::for_each(manifest.deps.bundles.begin(), manifest.deps.bundles.end(),
                    [&existed_bundle] (ThorManifest::DependentBundle& bundle) {
                        if (bundle.lpath.length() == 0)
                        {
                            // it is verified bundle
                            existed_bundle.insert(bundle.name);
                        }
            });

            // merged verified bundle dependencies into current bundle depenedency
            for(auto& name : all_directories_under_dep)
            {
                ThorBuildConfiguration dep_config;
                dep_config.load(name);

                std::for_each(dep_config.manifest.deps.bundles.begin(), dep_config.manifest.deps.bundles.end(),
                        [&manifest, &existed_bundle](ThorManifest::DependentBundle& bundle) {
                            if (bundle.lpath.length() == 0 && existed_bundle.count(bundle.name) == 0)
                            {
                                manifest.deps.bundles.push_back(bundle);
                                existed_bundle.insert(bundle.name);
                            }
                });
            }

            temporary_manifest = boost::filesystem::unique_path(config.build_path / "%%%%%%%%%%%%%%%%%");
            const auto manifest_dtd = config.sdk_executable_path / THOR_DEFAULT_MANIFEST_DTD_PATH;
            XMLValidator validator(manifest_dtd);

            if(!manifest.save(temporary_manifest, validator))
            {
                std::cerr << "Error: failed to create temporary manifest for compression" << std::endl;
                return false;
            }
        }

        ArchiveItem item;
        item.filename = THOR_DEFAULT_MANIFEST_PATH;
        if(!addArchiveItem(temporary_manifest.string(), item))
        {
            std::cerr << "Error: failed to compress manifest" << std::endl;
            return false;
        }
        ar.add(item);

        // remove the temporary manifest
        boost::filesystem::remove(temporary_manifest);
    }

    // compress all dependent shared objects
    if(config.manifest.bundle_type == ThorManifest::BundleType::NATIVE)
    {
        if(config.manifest.arch.is_cuda())
        {
            std::string cuda_lib_filename = THOR_PREFIX_LIBRARY + config.manifest.name + "_cuda" + THOR_EXTENSION_LIB;
            boost::filesystem::path cuda_lib_path = config.binary_output_path / cuda_lib_filename;

            ArchiveItem item;
            item.filename = "bin/" + cuda_lib_filename;
            if(!addArchiveItem(cuda_lib_path.string(), item))
            {
                std::cerr << "Error: failed to compress the generated CUDA library" << std::endl;
                return false;
            }
            ar.add(item);
        }

        // for native project, add the generated shared library to archive
        ArchiveItem item;
        std::string filename = THOR_PREFIX_LIBRARY + config.manifest.name + THOR_EXTENSION_SO;
        item.filename = "bin/" + filename;
        if(!addArchiveItem((config.binary_output_path / filename).string(), item))
        {
            std::cerr << "Error: failed to compress the generated shared object" << std::endl;
            return false;
        }
        ar.add(item);

        // collect share objects from extracted none-verified native bundles
        for(auto& directory : all_directories_under_dep)
        {
            std::vector<boost::filesystem::path> all_shared_libraries = Filesystem::collect_files(directory, THOR_EXTENSION_SO, true);
            for(auto& shared_library : all_shared_libraries)
            {
                ThorBuildConfiguration dep_config;
                dep_config.load(shared_library);

                // skip verified bundle
                if (dep_config.manifest.signature.length() != 0) continue;

                ArchiveItem item;
                item.filename = "bin/" + shared_library.filename().generic_string();
                if(!addArchiveItem(shared_library.generic_string(), item))
                {
                    std::cerr << "Error: failed to compress shared object from extracted none-verified native bundles" << std::endl;
                    return false;
                }
                ar.add(item);
            }
        }

        // collect depended share objects
        for(auto& library : config.manifest.deps.native_shared_libraries)
        {
            ArchiveItem item;
            std::string filename = THOR_PREFIX_LIBRARY + library.name + THOR_EXTENSION_SO;

            boost::filesystem::path source = config.binary_output_path / filename;
            item.filename = "bin/" + filename;
            if(!addArchiveItem(source, item))
            {
                std::cerr << "Error: failed to copy dependent shared object to binary directory" << std::endl;
                return false;
            }
            ar.add(item);
        }
    }

    // compress all cpp templates anyway
    std::vector<boost::filesystem::path> cpp_headers = Filesystem::collect_files(config.source_path, THOR_EXTENSION_H, true);
    std::vector<boost::filesystem::path> cu_headers = Filesystem::collect_files(config.source_path, THOR_EXTENSION_CUH, true);
    std::vector<boost::filesystem::path> headers;

    headers.insert(headers.end(), cpp_headers.begin(), cpp_headers.end());
    headers.insert(headers.end(), cu_headers.begin(), cu_headers.end());

    for(auto& header : headers)
    {
        if(Filesystem::is_regular_file(header))
        {
            boost::filesystem::path relative_path = Filesystem::path_difference(config.project_path, header);
            ArchiveItem item;
            item.filename = relative_path.string();
            if(!addArchiveItem(header.string(), item))
            {
                std::cerr << "Error: failed to copy dependent headers '" << relative_path.string() << "'" << std::endl;
                return false;
            }
            ar.add(item);
        }
    }

    return ar.close();
}

} } }
