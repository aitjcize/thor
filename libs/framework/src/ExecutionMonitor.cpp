/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/ExecutionMonitor.h"
#include "utility/Foreach.h"

namespace zillians { namespace framework {

ExecutionMonitor::ExecutionMonitor()
{
    mLock.v_ = 0;
    mIncompletedCount = 0;
    mCompletionFlags = new bool[buffer::buffer_constants::max_targets];
    mSignalForCompletion = new std::vector<shared_ptr<ConditionVariable<int32>>>();

	for(int i=0;i<buffer::buffer_constants::max_targets;++i)
	{
	    mCompletionFlags[i] = true;
	}
}

ExecutionMonitor::~ExecutionMonitor()
{
    boost::detail::spinlock::scoped_lock lock(mLock);

	SAFE_DELETE_ARRAY(mCompletionFlags);

	if(mSignalForCompletion->size() > 0)
	{
	    for(auto& to_signal : *mSignalForCompletion)
            to_signal->signal(-1);
	}

	SAFE_DELETE(mSignalForCompletion);
}

//////////////////////////////////////////////////////////////////////////
bool ExecutionMonitor::isCompleted()
{
    boost::detail::spinlock::scoped_lock lock(mLock);

	if(mIncompletedCount == 0)
		return true;

	return false;
}

bool ExecutionMonitor::isCompleted(uint32 processor_id)
{
    BOOST_ASSERT(processor_id < buffer::buffer_constants::max_targets);

    boost::detail::spinlock::scoped_lock lock(mLock);

	return mCompletionFlags[processor_id];
}

//////////////////////////////////////////////////////////////////////////
void ExecutionMonitor::markCompleted(uint32 processor_id)
{
	BOOST_ASSERT(processor_id < buffer::buffer_constants::max_targets);

    boost::detail::spinlock::scoped_lock lock(mLock);

	if(!mCompletionFlags[processor_id])
	{
	    mCompletionFlags[processor_id] = true;
	    --mIncompletedCount;
	}

	if(mIncompletedCount == 0)
	{
        for(auto& to_signal : *mSignalForCompletion)
            to_signal->signal(0);

        mSignalForCompletion->clear();
	}
}

void ExecutionMonitor::markIncompleted(uint32 processor_id)
{
	BOOST_ASSERT(processor_id < buffer::buffer_constants::max_targets);

    boost::detail::spinlock::scoped_lock lock(mLock);

    if(mCompletionFlags[processor_id])
    {
        mCompletionFlags[processor_id] = false;
        ++mIncompletedCount;
    }
}

void ExecutionMonitor::signalForCompletion(shared_ptr<ConditionVariable<int32>> v)
{
    boost::detail::spinlock::scoped_lock lock(mLock);

    if(mIncompletedCount == 0)
        v->signal(0);
    else
        mSignalForCompletion->push_back(v);
}

} }
