/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <utility>

#include "utility/Foreach.h"

#include "core/SharedPtr.h"

#include "framework/ExecutionEngine.h"
#include "framework/ProcessorId.h"

namespace zillians { namespace framework {

log4cxx::LoggerPtr ExecutionEngine::mLogger(log4cxx::Logger::getLogger("ExecutionEngine"));

//////////////////////////////////////////////////////////////////////////
ExecutionEngine::ExecutionEngine() :
		mMonitor(make_shared<ExecutionMonitor>()),
		mDispatcher(make_shared<Dispatcher<KernelITC>>(
                    DispatcherOptions()
                        .setMaxDispatcherThreads(buffer_constants::max_targets)))
{
	for(uint32 i=0;i<ksMaxProcessorCount;++i) mProcessors[i] = NULL;
}

ExecutionEngine::~ExecutionEngine()
{
	// stop & detach all processors
	detachAllProcessors();

	mDispatcher.reset();
	mMonitor.reset();
}

//////////////////////////////////////////////////////////////////////////
uint32 ExecutionEngine::countProcessors()
{
	uint32 count = 0;
	for(uint32 i=0;i<ksMaxProcessorCount;++i) if(mProcessors[i]) ++count;
	return count;
}

Processor* ExecutionEngine::operator [] (uint32 id)
{
	return mProcessors[id];
}

//////////////////////////////////////////////////////////////////////////
void ExecutionEngine::attachProcessor(Processor* processor)
{
	uint32 id = processor->getId();
	BOOST_ASSERT(mProcessors[id] == NULL);

	mProcessors[id] = processor;
}

//////////////////////////////////////////////////////////////////////////
void ExecutionEngine::detachAllProcessors()
{
	for(uint32 i=0;i<ksMaxProcessorCount;++i)
	{
		if(mProcessors[i])
		{
			detachProcessor(i);
		}
	}
}

void ExecutionEngine::detachProcessor(uint32 id)
{
	if(mProcessors[id])
	{
		stopProcessor(id);
		SAFE_DELETE(mProcessors[id]);
	}
}

//////////////////////////////////////////////////////////////////////////
bool ExecutionEngine::initializeProcessor(uint32 id)
{
    Processor* processor = mProcessors[id];

    BOOST_ASSERT(processor != nullptr && "null pointer exception");

    // create a new dispatcher thread context
    auto dispatcher_thread_context = mDispatcher->createThreadContext(id);

    // create execution itc for it
    auto execution_itc = make_shared<ExecutionITC>(id, std::move(dispatcher_thread_context));

    processor->setExecutionITC(std::move(execution_itc));
    processor->initialize();

    return true;
}

void ExecutionEngine::initializeAllProcessors()
{
    for (const auto& id : zero_to(ksMaxProcessorCount))
        if (mProcessors[id])
            initializeProcessor(id);
}

bool ExecutionEngine::finalizeProcessor(uint32 id)
{
    // find the processor & associated processor context
    Processor* processor = mProcessors[id];

    BOOST_ASSERT(processor != nullptr && "null pointer exception");

    processor->finalize();

    // mark the processor completed
    mMonitor->markCompleted(id);

    return true;
}

void ExecutionEngine::finalizeAllProcessors()
{
    for (const auto& id : zero_to(ksMaxProcessorCount))
        if (mProcessors[id])
            finalizeProcessor(id);
}

bool ExecutionEngine::startProcessor(uint32 id)
{
	Processor* processor = mProcessors[id];

    BOOST_ASSERT(processor != nullptr && "null pointer exception");

	if (processor->isRunning())
	    return false;

//	LOG4CXX_DEBUG(mLogger, "[ExecutionEngine::startProcessor] starting processor: " << ProcessorId::id_to_name(id));

	return processor->start(mMonitor);
}

void ExecutionEngine::startAllProcessors()
{
    for (const auto& id : zero_to(ksMaxProcessorCount))
		if(mProcessors[id])
			startProcessor(id);
}

bool ExecutionEngine::stopProcessor(uint32 id)
{
	// find the processor & associated processor context
	Processor* processor = mProcessors[id];

    BOOST_ASSERT(processor != nullptr && "null pointer exception");

	if(!processor->isRunning())
	    return false;

	// notify the processor to stop
	return processor->stop();
}

void ExecutionEngine::stopAllProcessors()
{
    for (const auto& id : zero_to(ksMaxProcessorCount))
		if(mProcessors[id])
			stopProcessor(id);
}

//////////////////////////////////////////////////////////////////////////
bool ExecutionEngine::waitForCompletion()
{
    shared_ptr<ConditionVariable<int32>> cond(new ConditionVariable<int32>);
    mMonitor->signalForCompletion(cond);

    int result = 0;
    cond->wait(result);

    if(!result)
        return true;

    return false;
}

} }
