/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <dlfcn.h>

#include <limits>
#include <thread>

#include <boost/range/adaptor/map.hpp>

#include "utility/Foreach.h"

#include "framework/Processor.h"

namespace zillians { namespace framework {

//////////////////////////////////////////////////////////////////////////
Processor::Processor(uint32 local_id)
    : mProcessorId(local_id)
    , mExecutionITC()
    , mMonitor()
    , mStopRequest(false)
    , mWorker()
{
//	mProcessorId = std::numeric_limits<uint32>::max();
}

Processor::~Processor()
{
}

//////////////////////////////////////////////////////////////////////////
uint32 Processor::getId()
{
	return mProcessorId;
}

//////////////////////////////////////////////////////////////////////////
void Processor::setExecutionITC(shared_ptr<ExecutionITC> execution_itc)
{
	mExecutionITC = execution_itc;
}

shared_ptr<ExecutionITC> Processor::getExecutionITC()
{
	return mExecutionITC;
}

void Processor::setMonitor(shared_ptr<ExecutionMonitor> monitor)
{
	mMonitor = monitor;
}

shared_ptr<ExecutionMonitor> Processor::getMonitor()
{
	return mMonitor;
}

//////////////////////////////////////////////////////////////////////////
void Processor::processITC(bool blocking, bool one_or_all)
{
	mExecutionITC->process(blocking, one_or_all);
}

void Processor::sendITC(uint32 processor_id, KernelITC& itc)
{
	mExecutionITC->dispatch(processor_id, itc);
}

void Processor::sendITC(uint32 processor_id, KernelITC& itc, ExecutionITC::kernel_itc_handler_t continuation_handler)
{
	mExecutionITC->dispatchWithContiuation(processor_id, itc, continuation_handler);
}

//////////////////////////////////////////////////////////////////////////
void Processor::initialize()
{
	BufferManager::instance()->commit();
	BufferNetwork::instance()->commit();
}

void Processor::finalize()
{
}

bool Processor::start(const std::shared_ptr<ExecutionMonitor>& monitor)
{
    mStopRequest = false;
    mWorker = boost::thread(
        &Processor::run,
        this,
        monitor
    );

    return true;
}

bool Processor::stop()
{
    mStopRequest = true;

    if (!mWorker.joinable())
        return false;

    boost::thread dummy;

    swap(mWorker, dummy);

    dummy.join();

    return true;
}

//////////////////////////////////////////////////////////////////////////
bool Processor::waitForCompletion()
{
	try
	{
		while(!isCompleted())
		{
			boost::this_thread::yield();
		}
	}
	catch(...)
	{
		return false;
	}

	return true;
}

bool Processor::waitForCompletion(const boost::system_time& abs)
{
	try
	{
		while(true)
		{
			if(boost::get_system_time() >= abs)
			{
				return false;
			}
			else
			{
				if(!isCompleted())
				{
				    boost::this_thread::yield();
				}
				else
				{
					return true;
				}
			}
		}
	}
	catch(...)
	{
		return false;
	}

	return true;
}

bool Processor::waitForCompletion(const boost::posix_time::time_duration& duration)
{
	return waitForCompletion(boost::get_system_time() + duration);
}

ProcessorRT::ProcessorRT(uint32 local_id)
    : Processor(local_id)
    , mEnableServerFunction(false)
    , mEnableClientFunction(false)
    , mKernel()
    , mServices()
{
}

bool ProcessorRT::isServerFunctionEnabled() const noexcept { return mEnableServerFunction; }
bool ProcessorRT::isClientFunctionEnabled() const noexcept { return mEnableClientFunction; }
void ProcessorRT::enabledServerFunction(bool enable) noexcept { mEnableServerFunction = enable; }
void ProcessorRT::enabledClientFunction(bool enable) noexcept { mEnableClientFunction = enable; }

const std::shared_ptr<KernelBase>& ProcessorRT::getKernel() const noexcept
{
    return mKernel;
}

const std::shared_ptr<Service>& ProcessorRT::getService(uint32 service_id) const
{
    return mServices.at(service_id);
}

void ProcessorRT::initialize()
{
    using boost::adaptors::map_values;

    Processor::initialize();

    for (const auto& service : mServices | map_values)
        service->beforeInitializeOnKernel(mKernel);

    mKernel->initialize();

    for (const auto& service : mServices | map_values)
        service->afterInitializeOnKernel(mKernel);

    for (const auto& service : mServices | map_values)
        service->initialize();
}

void ProcessorRT::finalize()
{
    using boost::adaptors::map_values;

    for (const auto& service : mServices | map_values)
        service->finalize();

    for (const auto& service : mServices | map_values)
        service->beforeFinalizeOnKernel(mKernel);

    mKernel->finalize();

    for (const auto& service : mServices | map_values)
        service->afterFinalizeOnKernel(mKernel);

    Processor::finalize();
}

bool ProcessorRT::load(const boost::filesystem::path& main_ast_path, 
                       const boost::filesystem::path& main_runtime_path,
                       const std::vector<boost::filesystem::path>& dep_paths)
{
    BOOST_ASSERT(mKernel != nullptr && "kernel is not ready");

    return mKernel->load(main_ast_path, main_runtime_path, dep_paths);
}

int64 ProcessorRT::queryFunctionId(const std::string& name) const
{
    BOOST_ASSERT(mKernel != nullptr && "no kernel");

    return mKernel->queryFunctionId(name);
}

void ProcessorRT::attachService(uint32 service_id, shared_ptr<Service>&& service)
{
    BOOST_ASSERT((mKernel == nullptr || !mKernel->isInitialized()) && "you should not attach service after kernel initialization");

    mServices.emplace(std::move(service_id), std::move(service));
}

void ProcessorRT::detachService(uint32 service_id)
{
    BOOST_ASSERT((mKernel == nullptr || !mKernel->isInitialized()) && "you should not detach service after kernel initialization");

    mServices.erase(service_id);
}

} }
