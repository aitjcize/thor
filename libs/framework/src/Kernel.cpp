/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>
#include <set>
#include <tuple>

#include <boost/assert.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/numeric.hpp>

#include "utility/UnicodeUtil.h"

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/RecursiveASTDiscover.h"
#include "language/stage/serialization/detail/ASTSerializationHelper.h"

#include "framework/Kernel.h"

namespace zillians { namespace framework {

using namespace zillians::language;
using namespace zillians::language::tree;
using namespace zillians::language::stage;

KernelBase::KernelBase(bool enable_server_function, bool enable_client_function) :
		isInit(false),
		mEnableServerFunction(enable_server_function), mEnableClientFunction(enable_client_function),
		mVerboseMode(false)
{
}

KernelBase::~KernelBase()
{ }

void KernelBase::initialize()
{
	isInit = true;
}

void KernelBase::finalize()
{ }

bool KernelBase::isInitialized()
{
	return isInit;
}

bool KernelBase::setKernel(const boost::filesystem::path& ast_file)
{
    // load the AST file
    ASTNode* node = ASTSerializationHelper::deserialize(ast_file.string());
    Tangle* tangle = (node) ? cast<Tangle>(node) : NULL;
    if(!tangle)
    {
        std::cerr << "Error: failed to load AST file: " << ast_file.string() << std::endl;
        return false;
    }
    mTangle = tangle;

    bool success = true;

    // compute global offsets
    if (!computeGlobalOffsets(*tangle))
        return false;

    // create late binding id for symbols/string literals and build up the string literal map
    if (!addKernelStrings(*tangle))
        return false;

    // create late binding id for types so that other service can look up the actual type id from ASTNode
    if (!addKernelTypes(*tangle))
        return false;

    // create late binding id for functions so that other service can look up the actual function id from ASTNode
    if (!addKernelFunctions(*tangle))
        return false;

    return true;
}

int64 KernelBase::queryFunctionId(const std::string& function_name)
{
    for (const auto& entry : mFunctionIdMap.right)
    {
        const FunctionDecl*const func_decl = entry.first;

        const auto*const name_mangling_ctx = NameManglingContext::get(func_decl);
        BOOST_ASSERT(name_mangling_ctx != nullptr && "every function in function id map should have mangled name");

        if (name_mangling_ctx->mangled_name == function_name)
            return entry.second;
    }

    return -1;
}

const language::tree::FunctionDecl* KernelBase::queryFunctionDecl(int64 function_id)
{
	auto it = mFunctionIdMap.left.find(function_id);
	if(UNLIKELY(it == mFunctionIdMap.left.end()))
	{
		return nullptr;
	}
	else
	{
		return it->second;
	}
}

const language::tree::ClassDecl* KernelBase::queryClassDecl(int64 type_id)
{
	auto it = mTypeIdMap.left.find(type_id);
	if(UNLIKELY(it == mTypeIdMap.left.end()))
	{
		return nullptr;
	}
	else
	{
		return it->second;
	}
}

const std::wstring* KernelBase::queryStringLiteral(int64 symbol_id)
{
    auto it = mStringLiteralMap.left.find(symbol_id);
    if(UNLIKELY(it == mStringLiteralMap.left.end()))
    {
        return nullptr;
    }
    else
    {
        return &it->second;
    }
}

void KernelBase::setVerbose(bool verbose)
{
    mVerboseMode = verbose;
}

bool KernelBase::getVerbose()
{
    return mVerboseMode;
}

bool KernelBase::computeGlobalOffsets(language::tree::Tangle& tangle)
{
    using boost::adaptors::map_values;

    enum MappingType
    {
        MAPPING_FUNC,
        MAPPING_SYMB,
        MAPPING_TYPE,
    };

    mGlobalOffsets.clear();

    const auto& func_mapping = tangle.getMappingOfFunction();
    const auto& symb_mapping = tangle.getMappingOfSymbol();
    const auto& type_mapping = tangle.getMappingOfType();

    const auto& register_offset = [this](const std::set<language::tree::Tangle::OffsetedId>& offseted_ids, int type)
    {
        for (const auto& entry : offseted_ids)
        {
            const auto& tangle_id = entry.first;
            const auto& offset    = entry.second;

            const auto& insertion_result = mGlobalOffsets.emplace(tangle_id, std::make_tuple(-1, -1, -1));
                  auto& offsets          = insertion_result.first->second;
                  auto& origin_offset    = type == MAPPING_FUNC ? std::get<0>(offsets) :
                                           type == MAPPING_SYMB ? std::get<1>(offsets) :
                                                                  std::get<2>(offsets) ;

            if (origin_offset < offset)
                origin_offset = offset;
        }
    };

    for (const auto& entry : func_mapping | map_values)
        register_offset(entry.first, MAPPING_FUNC);

    for (const auto& entry : symb_mapping | map_values)
        register_offset(entry.first, MAPPING_SYMB);

    for (const auto& entry : type_mapping | map_values)
        register_offset(entry.first, MAPPING_TYPE);

    int64 last_func_offset = -1;
    int64 last_symb_offset = -1;
    int64 last_type_offset = -1;

    // data stored in mGlobalOffsets are the max offset for each tangle ID
    // transform it to runtime offset
    for (auto& entry : mGlobalOffsets)
    {
        const auto& next_last_func_offset = last_func_offset + 1 + std::get<0>(entry.second);
        const auto& next_last_symb_offset = last_symb_offset + 1 + std::get<1>(entry.second);
        const auto& next_last_type_offset = last_type_offset + 1 + std::get<2>(entry.second);

        std::get<0>(entry.second) = last_func_offset + 1;
        std::get<1>(entry.second) = last_symb_offset + 1;
        std::get<2>(entry.second) = last_type_offset + 1;

        last_func_offset = next_last_func_offset;
        last_symb_offset = next_last_symb_offset;
        last_type_offset = next_last_type_offset;
    }

    return true;
}

bool KernelBase::addKernelStrings(Tangle& tangle)
{
    const auto& symb_mapping = tangle.getMappingOfSymbol();

    for (const auto& entry : symb_mapping)
    {
        const auto& string       = entry.first;
        const auto& offseted_ids = entry.second.first;

        for (const auto& offseted_id : offseted_ids)
        {
            const auto& tangle_id = offseted_id.first;
            const auto& offset    = offseted_id.second;

            BOOST_ASSERT(mGlobalOffsets.count(tangle_id) > 0 && "global offset is not computed!?");

            const auto& global_offset = std::get<GlobalOffsetsIndexSymbol>(mGlobalOffsets[tangle_id]);
            const auto&   real_offset = global_offset + offset;

            const auto& insertion_result = mStringLiteralMap.insert({real_offset, string});
            const auto& is_inserted      = insertion_result.second;

            if (!is_inserted)
                return false;
        }
    }

    return true;
}

bool KernelBase::addKernelTypes(Tangle& tangle)
{
    const auto& type_mapping = tangle.getMappingOfType();

    for (const auto& entry : type_mapping)
    {
        const auto& class_decl   = entry.first;
        const auto& offseted_ids = entry.second.first;

        for (const auto& offseted_id : offseted_ids)
        {
            const auto& tangle_id = offseted_id.first;
            const auto& offset    = offseted_id.second;

            BOOST_ASSERT(mGlobalOffsets.count(tangle_id) > 0 && "global offset is not computed!?");

            const auto& global_offset = std::get<GlobalOffsetsIndexType>(mGlobalOffsets[tangle_id]);
            const auto&   real_offset = global_offset + offset;

            const auto& insertion_result = mTypeIdMap.insert({real_offset, class_decl});
            const auto& is_inserted      = insertion_result.second;

            if (!is_inserted)
                return false;
        }
    }

    return true;
}

bool KernelBase::addKernelFunctions(Tangle& tangle)
{
    const auto& func_mapping = tangle.getMappingOfFunction();

    for (const auto& entry : func_mapping)
    {
        const auto& func_decl    = entry.first;
        const auto& offseted_ids = entry.second.first;
        const auto& kind         = static_cast<ASTNodeHelper::ExportKind>(entry.second.second);

        const auto& is_current_export = (kind == ASTNodeHelper::ExportKind::GeneralExport                         ) ||
                                        (kind == ASTNodeHelper::ExportKind:: ClientExport && mEnableClientFunction) ||
                                        (kind == ASTNodeHelper::ExportKind:: ServerExport && mEnableServerFunction)
                                        ;

        if (!is_current_export)
            continue;

        for (const auto& offseted_id : offseted_ids)
        {
            const auto& tangle_id = offseted_id.first;
            const auto& offset    = offseted_id.second;

            BOOST_ASSERT(mGlobalOffsets.count(tangle_id) > 0 && "global offset is not computed!?");

            const auto& global_offset = std::get<GlobalOffsetsIndexFunction>(mGlobalOffsets[tangle_id]);
            const auto&   real_offset = global_offset + offset;

            const auto& insertion_result = mFunctionIdMap.insert({real_offset, func_decl});
            const auto& is_inserted      = insertion_result.second;

            if (mVerboseMode)
            {
                if (func_decl->is_member)
                    std::cerr << "exported fid[" << real_offset << "] => " << ws_to_s(cast<ClassDecl>(func_decl->parent)->name->toString()) << "::" << ws_to_s(func_decl->name->toString()) << std::endl;
                else
                    std::cerr << "exported fid[" << real_offset << "] => " << ws_to_s(func_decl->name->toString()) << std::endl;
            }

            if (!is_inserted)
                return false;
        }
    }

    return true;
}

} }
