/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/processor/ProcessorExternal.h"
#include "framework/service/runtime/RuntimeServiceBuffer.h"
#include "framework/buffer/BufferNetwork.h"
#include "threading/AdaptiveWait.h"
#include "framework/Configuration.h"

namespace zillians { namespace framework { namespace processor {

//////////////////////////////////////////////////////////////////////////
ProcessorExternal::ProcessorExternal(uint32 local_id) : Processor(local_id)
{
	mImplicitPendingCompletion = 0;

	BufferNetwork::instance()->declareReadableLocation<service::InvocationRequestBuffer>(local_id, buffer_location_t::host_unpinned, 0);
	BufferNetwork::instance()->declareWritableLocation<service::InvocationRequestBuffer>(local_id, buffer_location_t::host_unpinned, 0);
#ifdef BUILD_WITH_RDMA
	if(Configuration::instance()->enable_rdma)
	{
		BufferNetwork::instance()->declareReadableLocation<service::InvocationRequestBuffer>(local_id, buffer_location_t::host_ib_pinned, 1);
		BufferNetwork::instance()->declareWritableLocation<service::InvocationRequestBuffer>(local_id, buffer_location_t::host_ib_pinned, 1);
	}
#endif
#ifdef BUILD_WITH_CUDA
	if(Configuration::instance()->enable_cuda)
	{
		BufferNetwork::instance()->declareReadableLocation<service::InvocationRequestBuffer>(local_id, buffer_location_t::host_ptx_pinned, 2);
		BufferNetwork::instance()->declareWritableLocation<service::InvocationRequestBuffer>(local_id, buffer_location_t::host_ptx_pinned, 2);
	}
#endif
#ifdef BUILD_WITH_OPENCL
	if(Configuration::instance()->enable_opencl)
	{
		BufferNetwork::instance()->declareReadableLocation<service::InvocationRequestBuffer>(local_id, buffer_location_t::host_ocl_pinned, 3);
		BufferNetwork::instance()->declareWritableLocation<service::InvocationRequestBuffer>(local_id, buffer_location_t::host_ocl_pinned, 3);
	}
#endif
}

ProcessorExternal::~ProcessorExternal()
{ }

//////////////////////////////////////////////////////////////////////////
void ProcessorExternal::run(const shared_ptr<ExecutionMonitor>& monitor)
{
    mMonitor = monitor;

    while (!mStopRequest)
    {
        if(!isCompleted())
            mMonitor->markIncompleted(getId());

        processITC(false, false);
    }
}

bool ProcessorExternal::isRunning()
{
	return mWorker.joinable();
}

bool ProcessorExternal::isCompleted()
{
	return mITCQueue.empty();
}

//////////////////////////////////////////////////////////////////////////
void ProcessorExternal::processITC(bool blocking, bool one_or_all)
{
	boost::tuple<uint32, KernelITC, ExecutionITC::kernel_itc_handler_t > t;

	static threading::AdaptiveWait<
		1000 /*NumberOfSignalsBeforeEnteringWaitState*/, 0 /*NumberOfSignalsBeforeLeavingWaitState*/,
		10000 /*MinWait*/, 50000 /*MaxWait*/,
//		0 /*MinWait*/, 0 /*MaxWait*/,
		500 /*SlowDownStep*/, 5000 /*SpeedUpStep*/> adaptive_wait;

	// flush all items in mITCQueue
	while(true)
	{
		if(mITCQueue.try_pop(t))
		{
			adaptive_wait.speedup();
			if(!t.get<2>())
			{
				mExecutionITC->dispatch(t.get<0>(), t.get<1>());
			}
			else
			{
				mExecutionITC->dispatchWithContiuation(t.get<0>(), t.get<1>(), t.get<2>());
			}
		}
		else
		{
			if(mImplicitPendingCompletion == 0)
			{
				mMonitor->markCompleted(getId());
			}
			adaptive_wait.slowdown();
			adaptive_wait.wait();
			break;
		}
	}

	mExecutionITC->process(blocking, one_or_all);
}


void ProcessorExternal::pushImplicitPendingCompletion(int count)
{
    if(mImplicitPendingCompletion == 0)
        mMonitor->markIncompleted(getId());

    mImplicitPendingCompletion += count;
}

void ProcessorExternal::popImplicitPendingCompletion(int count)
{
    mImplicitPendingCompletion -= count;
}

//////////////////////////////////////////////////////////////////////////
void ProcessorExternal::enqueueITC(uint32 processor_id, KernelITC& itc)
{
    mITCQueue.push(
        {
            processor_id,
            itc
        }
    );
}

void ProcessorExternal::enqueueITC(uint32 processor_id, KernelITC& itc, ExecutionITC::kernel_itc_handler_t continuation_handler)
{
    mITCQueue.push(
        {
            processor_id,
            itc,
            continuation_handler
        }
    );
}

} } }
