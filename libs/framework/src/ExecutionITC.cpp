/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/buffer/BufferNetwork.h"
#include "framework/ExecutionITC.h"

namespace zillians { namespace framework {

void KernelITC::destroy()
{
    if (isInvocationRequest())
    {
        auto*const buffer = getInvocationRequest().buffer;

        BOOST_ASSERT(buffer != nullptr && "null pointer exception");

        BufferNetwork::instance()->destroy(*buffer);
        delete buffer;
    }
}

int64 KernelITC::get_session_id() const
{
    if (const auto*const p = boost::get<InvocationRequest              >(&itc_variant)) return p->session_id;
    if (const auto*const p = boost::get<DomainSignalConnectedRequest   >(&itc_variant)) return p->session_id;
    if (const auto*const p = boost::get<DomainSignalDisconnectedRequest>(&itc_variant)) return p->session_id;

    UNREACHABLE_CODE();

    return 0;
}

log4cxx::LoggerPtr ExecutionITC::mLogger(log4cxx::Logger::getLogger("ExecutionITC"));

//////////////////////////////////////////////////////////////////////////
ExecutionITC::ExecutionITC(uint32 local_target_id, shared_ptr<DispatcherThreadContext<KernelITC> > ctx) :
	mLocalTargetId(local_target_id), mDispatcherContext(ctx), mCurrentContinuationId(1)
{
}

ExecutionITC::~ExecutionITC()
{
//	LOG4CXX_DEBUG(mLogger, "processor " << mLocalTargetId << " destroyed its execution itc");
}

//////////////////////////////////////////////////////////////////////////
bool ExecutionITC::process(bool blocking, bool one_or_all)
{
	bool any_result = false;

	uint32    source_id;
	KernelITC itc;

	while(true)
	{
		if(mDispatcherContext->read(source_id, itc, blocking))
		{
			any_result = true;

			dispatchKernelITC(source_id, itc);

			if(one_or_all) break;
		}
		else
		{
			if(!blocking)
				break;
		}
	}

	return any_result;
}

//////////////////////////////////////////////////////////////////////////
void ExecutionITC::registerHandler(KernelITC::type type, kernel_itc_handler_t request_handler)
{
	BOOST_ASSERT(KernelITC::is_request(type));

	auto it = mHandlers.find((uint32)type);
	if(UNLIKELY(it != mHandlers.end()))
	{
		LOG4CXX_ERROR(mLogger, "processor id = " << mLocalTargetId << " has duplicate handler registration, type = " << type);
	}
	else
	{
		mHandlers[(uint32)type] = request_handler;
	}
}

void ExecutionITC::unregisterHandler(KernelITC::type type)
{
	BOOST_ASSERT(KernelITC::is_request(type));

	auto it = mHandlers.find((uint32)type);
	if(LIKELY(it != mHandlers.end()))
	{
		mHandlers.erase(it);
	}
	else
	{
		LOG4CXX_ERROR(mLogger, "processor id = " << mLocalTargetId << " was asked to unregister handler that does not exist, type = " << type);
	}
}

//////////////////////////////////////////////////////////////////////////
void ExecutionITC::dispatchWithContiuation(uint32 target_id, KernelITC& itc, kernel_itc_handler_t response_handler)
{
	itc.continuation_id = mCurrentContinuationId++;
	mContinuations[itc.continuation_id] = response_handler;

	dispatch(target_id, itc);
}

void ExecutionITC::dispatch(uint32 target_id, KernelITC& itc)
{
	if(target_id == mLocalTargetId)
	{
		dispatchKernelITC(target_id, itc);
	}
	else
	{
		// find target object from cache
		shared_ptr<DispatcherDestination<KernelITC> > target;
		auto it = mTargets.find(target_id);
		if(LIKELY(it != mTargets.end()))
		{
			target = it->second;
		}
		else
		{
			target = mDispatcherContext->createDestination(target_id);
			mTargets[target_id] = target;
		}

		target->write(itc);
	}
}

//////////////////////////////////////////////////////////////////////////
void ExecutionITC::dispatchKernelITC(uint32 source_id, KernelITC& itc)
{
	if(itc.is_response())
	{
	    auto it = mContinuations.find(itc.continuation_id);
		if(LIKELY(it != mContinuations.end()))
		{
			it->second(source_id, itc);
		}
		else
		{
			LOG4CXX_ERROR(mLogger, "processor id = " << mLocalTargetId << ", unregistered kernel itc response received, continuation id = " << itc.continuation_id << ", source id = " << source_id);

			itc.destroy();
		}
	}
	else
	{
	    auto it = mHandlers.find(itc.get_type());
		if(LIKELY(it != mHandlers.end()))
		{
			it->second(source_id, itc);
		}
		else
		{
			LOG4CXX_ERROR(mLogger, "processor id = " << mLocalTargetId << ", unregistered kernel itc request received, type id = " << itc.get_type() << ", source id = " << source_id);

			itc.destroy();
		}
	}
}

} }
