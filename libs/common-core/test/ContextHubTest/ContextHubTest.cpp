/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "core/Prerequisite.h"
#include "core/ContextHub.h"
#include <iostream>
#include <string>
#include <limits>

#define BOOST_TEST_MODULE ContextHubTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

using namespace zillians;
using namespace std;

BOOST_AUTO_TEST_SUITE( ContextHubTest )

class A { };
class B { };
class C { };
class D { };
class E { };
class F { };

BOOST_AUTO_TEST_CASE( ContextHubTestCase1 )
{
    ContextHub<ContextOwnership::transfer> hub;

    BOOST_CHECK_NO_THROW(hub.set<A>(new A));
    BOOST_CHECK_NO_THROW(hub.set<B>(new B));
    BOOST_CHECK_NO_THROW(hub.set<C>(new C));
    BOOST_CHECK_NO_THROW(hub.set<D>(new D));
    BOOST_CHECK_NO_THROW(hub.set<E>(new E));
    BOOST_CHECK_NO_THROW(hub.set<F>(new F));

    A* a = NULL; BOOST_CHECK_NO_THROW(a = hub.get<A>()); BOOST_CHECK(a != NULL);
    B* b = NULL; BOOST_CHECK_NO_THROW(b = hub.get<B>()); BOOST_CHECK(b != NULL);
    C* c = NULL; BOOST_CHECK_NO_THROW(c = hub.get<C>()); BOOST_CHECK(c != NULL);
    D* d = NULL; BOOST_CHECK_NO_THROW(d = hub.get<D>()); BOOST_CHECK(d != NULL);
    E* e = NULL; BOOST_CHECK_NO_THROW(e = hub.get<E>()); BOOST_CHECK(e != NULL);
    F* f = NULL; BOOST_CHECK_NO_THROW(f = hub.get<F>()); BOOST_CHECK(f != NULL);
}

template<typename T>
class Counter
{
public:
    Counter(int& ctor_counter, int& dtor_counter) : ctor_counter_(ctor_counter), dtor_counter_(dtor_counter)
    { ++ctor_counter_; }

    ~Counter()
    { ++dtor_counter_; }

public:
    int& ctor_counter_;
    int& dtor_counter_;
};

typedef Counter<A> CA;
typedef Counter<B> CB;
typedef Counter<C> CC;
typedef Counter<D> CD;
typedef Counter<E> CE;
typedef Counter<F> CF;

BOOST_AUTO_TEST_CASE( ContextHubTestCase2 )
{
    ContextHub<ContextOwnership::transfer> hub;

    int ctor_counter = 0;
    int dtor_counter = 0;

    BOOST_CHECK_NO_THROW(hub.set<CA>(new CA(ctor_counter, dtor_counter)));
    BOOST_CHECK_NO_THROW(hub.set<CB>(new CB(ctor_counter, dtor_counter)));
    BOOST_CHECK_NO_THROW(hub.set<CC>(new CC(ctor_counter, dtor_counter)));
    BOOST_CHECK_NO_THROW(hub.set<CD>(new CD(ctor_counter, dtor_counter)));
    BOOST_CHECK_NO_THROW(hub.set<CE>(new CE(ctor_counter, dtor_counter)));
    BOOST_CHECK_NO_THROW(hub.set<CF>(new CF(ctor_counter, dtor_counter)));

    BOOST_CHECK(ctor_counter == 6);
    BOOST_CHECK(dtor_counter == 0);

    CA* a = NULL; BOOST_CHECK_NO_THROW(a = hub.get<CA>()); BOOST_CHECK(a != NULL);
    CB* b = NULL; BOOST_CHECK_NO_THROW(b = hub.get<CB>()); BOOST_CHECK(b != NULL);
    CC* c = NULL; BOOST_CHECK_NO_THROW(c = hub.get<CC>()); BOOST_CHECK(c != NULL);
    CD* d = NULL; BOOST_CHECK_NO_THROW(d = hub.get<CD>()); BOOST_CHECK(d != NULL);
    CE* e = NULL; BOOST_CHECK_NO_THROW(e = hub.get<CE>()); BOOST_CHECK(e != NULL);
    CF* f = NULL; BOOST_CHECK_NO_THROW(f = hub.get<CF>()); BOOST_CHECK(f != NULL);

    BOOST_CHECK(ctor_counter == 6);
    BOOST_CHECK(dtor_counter == 0);

    BOOST_CHECK_NO_THROW(hub.set<CA>(new CA(ctor_counter, dtor_counter)));
    BOOST_CHECK_NO_THROW(hub.set<CB>(new CB(ctor_counter, dtor_counter)));
    BOOST_CHECK_NO_THROW(hub.set<CC>(new CC(ctor_counter, dtor_counter)));
    BOOST_CHECK_NO_THROW(hub.set<CD>(new CD(ctor_counter, dtor_counter)));
    BOOST_CHECK_NO_THROW(hub.set<CE>(new CE(ctor_counter, dtor_counter)));
    BOOST_CHECK_NO_THROW(hub.set<CF>(new CF(ctor_counter, dtor_counter)));

    BOOST_CHECK(ctor_counter == 12);
    BOOST_CHECK(dtor_counter == 6);

    BOOST_CHECK_NO_THROW(hub.reset<CA>());
    BOOST_CHECK_NO_THROW(hub.reset<CB>());
    BOOST_CHECK_NO_THROW(hub.reset<CC>());
    BOOST_CHECK_NO_THROW(hub.reset<CD>());
    BOOST_CHECK_NO_THROW(hub.reset<CE>());
    BOOST_CHECK_NO_THROW(hub.reset<CF>());

    BOOST_CHECK(ctor_counter == 12);
    BOOST_CHECK(dtor_counter == 12);
}

BOOST_AUTO_TEST_CASE( ContextHubTestCase3 )
{
    NamedContextHub<ContextOwnership::transfer> hub;

    int ctor_counter = 0;
    int dtor_counter = 0;

    CA* a = new CA(ctor_counter, dtor_counter);
    CB* b = new CB(ctor_counter, dtor_counter);
    CC* c = new CC(ctor_counter, dtor_counter);
    CD* d = new CD(ctor_counter, dtor_counter);
    CE* e = new CE(ctor_counter, dtor_counter);
    CF* f = new CF(ctor_counter, dtor_counter);

    BOOST_CHECK(ctor_counter == 6);
    BOOST_CHECK(dtor_counter == 0);

    hub.set(a, "CA");
    hub.set(b, "CB");
    hub.set(c, "CC");
    hub.set(d, "CD");
    hub.set(e, "CE");
    hub.set(f, "CF");

    CA* ta = NULL; BOOST_CHECK_NO_THROW(ta = hub.get<CA>("CA")); BOOST_CHECK(a == ta);
    CB* tb = NULL; BOOST_CHECK_NO_THROW(tb = hub.get<CB>("CB")); BOOST_CHECK(b == tb);
    CC* tc = NULL; BOOST_CHECK_NO_THROW(tc = hub.get<CC>("CC")); BOOST_CHECK(c == tc);
    CD* td = NULL; BOOST_CHECK_NO_THROW(td = hub.get<CD>("CD")); BOOST_CHECK(d == td);
    CE* te = NULL; BOOST_CHECK_NO_THROW(te = hub.get<CE>("CE")); BOOST_CHECK(e == te);
    CF* tf = NULL; BOOST_CHECK_NO_THROW(tf = hub.get<CF>("CF")); BOOST_CHECK(f == tf);

    BOOST_CHECK_NO_THROW(hub.set<CA>(new CA(ctor_counter, dtor_counter), "CA"));
    BOOST_CHECK_NO_THROW(hub.set<CB>(new CB(ctor_counter, dtor_counter), "CB"));
    BOOST_CHECK_NO_THROW(hub.set<CC>(new CC(ctor_counter, dtor_counter), "CC"));
    BOOST_CHECK_NO_THROW(hub.set<CD>(new CD(ctor_counter, dtor_counter), "CD"));
    BOOST_CHECK_NO_THROW(hub.set<CE>(new CE(ctor_counter, dtor_counter), "CE"));
    BOOST_CHECK_NO_THROW(hub.set<CF>(new CF(ctor_counter, dtor_counter), "CF"));

    BOOST_CHECK(ctor_counter == 12);
    BOOST_CHECK(dtor_counter == 6);

    BOOST_CHECK_NO_THROW(hub.reset<CA>("CA"));
    BOOST_CHECK_NO_THROW(hub.reset<CB>("CB"));
    BOOST_CHECK_NO_THROW(hub.reset<CC>("CC"));
    BOOST_CHECK_NO_THROW(hub.reset<CD>("CD"));
    BOOST_CHECK_NO_THROW(hub.reset<CE>("CE"));
    BOOST_CHECK_NO_THROW(hub.reset<CF>("CF"));

    BOOST_CHECK(ctor_counter == 12);
    BOOST_CHECK(dtor_counter == 12);
}

BOOST_AUTO_TEST_CASE( ContextHubTestCase4 )
{
    NamedContextHub<ContextOwnership::transfer> hub;

    int ctor_counter = 0;
    int dtor_counter = 0;

    CA* a = new CA(ctor_counter, dtor_counter);
    CB* b = new CB(ctor_counter, dtor_counter);
    CC* c = new CC(ctor_counter, dtor_counter);
    CD* d = new CD(ctor_counter, dtor_counter);
    CE* e = new CE(ctor_counter, dtor_counter);
    CF* f = new CF(ctor_counter, dtor_counter);

    BOOST_CHECK(ctor_counter == 6);
    BOOST_CHECK(dtor_counter == 0);

    hub.set(a);
    hub.set(b);
    hub.set(c);
    hub.set(d);
    hub.set(e);
    hub.set(f);

    CA* ta = NULL; BOOST_CHECK_NO_THROW(ta = hub.get<CA>()); BOOST_CHECK(a == ta);
    CB* tb = NULL; BOOST_CHECK_NO_THROW(tb = hub.get<CB>()); BOOST_CHECK(b == tb);
    CC* tc = NULL; BOOST_CHECK_NO_THROW(tc = hub.get<CC>()); BOOST_CHECK(c == tc);
    CD* td = NULL; BOOST_CHECK_NO_THROW(td = hub.get<CD>()); BOOST_CHECK(d == td);
    CE* te = NULL; BOOST_CHECK_NO_THROW(te = hub.get<CE>()); BOOST_CHECK(e == te);
    CF* tf = NULL; BOOST_CHECK_NO_THROW(tf = hub.get<CF>()); BOOST_CHECK(f == tf);

    BOOST_CHECK_NO_THROW(hub.set<CA>(new CA(ctor_counter, dtor_counter)));
    BOOST_CHECK_NO_THROW(hub.set<CB>(new CB(ctor_counter, dtor_counter)));
    BOOST_CHECK_NO_THROW(hub.set<CC>(new CC(ctor_counter, dtor_counter)));
    BOOST_CHECK_NO_THROW(hub.set<CD>(new CD(ctor_counter, dtor_counter)));
    BOOST_CHECK_NO_THROW(hub.set<CE>(new CE(ctor_counter, dtor_counter)));
    BOOST_CHECK_NO_THROW(hub.set<CF>(new CF(ctor_counter, dtor_counter)));

    BOOST_CHECK(ctor_counter == 12);
    BOOST_CHECK(dtor_counter == 6);

    BOOST_CHECK_NO_THROW(hub.reset<CA>());
    BOOST_CHECK_NO_THROW(hub.reset<CB>());
    BOOST_CHECK_NO_THROW(hub.reset<CC>());
    BOOST_CHECK_NO_THROW(hub.reset<CD>());
    BOOST_CHECK_NO_THROW(hub.reset<CE>());
    BOOST_CHECK_NO_THROW(hub.reset<CF>());

    BOOST_CHECK(ctor_counter == 12);
    BOOST_CHECK(dtor_counter == 12);
}

typedef SimpleContextTrait<struct traits1, bool, true> context_traits1;
typedef SimpleContextTrait<struct traits2, bool, true> context_traits2;
typedef SimpleContextTrait<struct traits3, bool, true> context_traits3;
typedef SimpleContextTrait<struct traits4, bool, true> context_traits4;

BOOST_AUTO_TEST_CASE( ContextHubTestCase5 )
{
    ContextHub<ContextOwnership::transfer> hub;
    hub.set<context_traits1>(new context_traits1(true));
    hub.set<context_traits2>(new context_traits2(false));
    hub.set<context_traits3>(new context_traits3(true));
    hub.set<context_traits4>(new context_traits4(false));

    BOOST_CHECK(hub.get<context_traits1>()->value == true);
    BOOST_CHECK(hub.get<context_traits2>()->value == false);
    BOOST_CHECK(hub.get<context_traits3>()->value == true);
    BOOST_CHECK(hub.get<context_traits4>()->value == false);
}

BOOST_AUTO_TEST_SUITE_END()
