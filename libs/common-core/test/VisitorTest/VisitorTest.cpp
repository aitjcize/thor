/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "core/Prerequisite.h"
#include "core/Visitor.h"
#include <iostream>
#include <string>
#include <limits>
#include <tbb/tick_count.h>

#define BOOST_TEST_MODULE VisitorTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

using namespace zillians;
using namespace std;

class Shape : public VisitableBase<Shape> {
public:
	Shape() { }
	virtual ~Shape() { }

    DEFINE_VISITABLE();
};

class Circle : public Shape {
public:
    DEFINE_VISITABLE();
};

class CircleX : public Circle {
public:
    DEFINE_VISITABLE();
};

class CompoundShape : public Shape {
public:
    DEFINE_VISITABLE();

    explicit CompoundShape(Shape& _a, Shape& _b) : a(_a), b(_b) { }

    Shape& a;
    Shape& b;
};

class Renderer: public Visitor<const Shape, void>
{
public:
    Renderer()
    {
        REGISTER_VISITABLE(DrawInvoker, Shape, Circle, CircleX, CompoundShape);
    }

    void draw(const Shape&)
    {
        printf("draw Shape\n");
    }

    void draw(const Circle&)
    {
        printf("draw Circle\n");
    }

    void draw(const CompoundShape& shape)
    {
        printf("draw CompoundShape\n");
        visit(shape.a);
        visit(shape.b);
    }

    CREATE_INVOKER(DrawInvoker, draw);
};

class Cloner : public Visitor<const Shape, Shape*>
{
public:
    Cloner()
    {
        REGISTER_VISITABLE(CloneInvoker, Shape, CircleX);
    }

    Shape* clone(const Shape& s)
    {
        printf("clone Shape\n");
        return new Shape();
    }

    Shape* clone(const Circle& c)
    {
        printf("clone Circle\n");
        return new Circle(c);
    }

    Shape* clone(const CircleX& c)
    {
        printf("clone CircleX\n");
        return new CircleX(c);
    }

    CREATE_INVOKER(CloneInvoker, clone);
};

BOOST_AUTO_TEST_SUITE( VisitorTestSuite )

BOOST_AUTO_TEST_CASE( VisitorTestCase1 )
{
    Circle s;
    Shape& ref_s = s;

    Renderer renderer;
    renderer.visit(ref_s);

    Cloner cloner;
    Shape* cloned_shape = cloner.visit(ref_s);
}

BOOST_AUTO_TEST_CASE( VisitorTestCase2 )
{
    Circle a;
    Circle b;
    CompoundShape c(a, b);

    Renderer renderer;
    renderer.visit(c);
}

BOOST_AUTO_TEST_SUITE_END()
