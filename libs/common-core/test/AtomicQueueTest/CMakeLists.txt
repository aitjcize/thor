#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

ADD_EXECUTABLE(AtomicQueueTest AtomicQueueTest.cpp)
ADD_EXECUTABLE(SpscQueueTest SpscQueueTest.cpp)

TARGET_LINK_LIBRARIES(AtomicQueueTest 
    thor-common-core
    thor-common-utility
    )

TARGET_LINK_LIBRARIES(SpscQueueTest 
    thor-common-core
    thor-common-utility
    )

zillians_add_simple_test(TARGET AtomicQueueTest)
zillians_add_test_to_subject(SUBJECT common-core-misc TARGET AtomicQueueTest)

zillians_add_simple_test(TARGET SpscQueueTest)
zillians_add_test_to_subject(SUBJECT common-core-misc TARGET SpscQueueTest)
