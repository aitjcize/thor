/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <tbb/tick_count.h>
#include <tbb/tbb_thread.h>
//#include <boost/thread.hpp>
//#include <boost/timer.hpp>
#include <boost/assert.hpp>
#include "core/Prerequisite.h"

// load with 'consume' (data-dependent) memory ordering
    template<typename T>
T load_consume(T const* addr)
{
    // hardware fence is implicit on x86
    T v = *const_cast<T const volatile*>(addr);
    //__memory_barrier(); // compiler fence
    __sync_synchronize();
    return v;
}

// store with 'release' memory ordering
    template<typename T>
void store_release(T* addr, T v)
{
    // hardware fence is implicit on x86
    //__memory_barrier(); // compiler fence
    __sync_synchronize();
    *const_cast<T volatile*>(addr) = v;
}

// cache line size on modern x86 processors (in bytes)
const std::size_t cache_line_size = 64;

// single-producer/single-consumer queue
template<typename T>
class spsc_queue
{
    public:
        spsc_queue()
        {
            node* n = new node;
            n->next_ = 0;
            tail_ = head_ = first_= tail_copy_ = n;
        }

        ~spsc_queue()
        {
            node* n = first_;
            do
            {
                node* next = n->next_;
                delete n;
                n = next;
            }
            while (n);
        }

        void enqueue(T v)
        {
            node* n = alloc_node();
            n->next_ = 0;
            n->value_ = v;
            store_release(&head_->next_, n);
            head_ = n;
        }

        // returns 'false' if queue is empty
        bool dequeue(T& v)
        {
            if (load_consume(&tail_->next_))
            {
                v = tail_->next_->value_;
                store_release(&tail_, tail_->next_);
                return true;
            }
            else
            {
                return false;
            }
        }

    private:
        // internal node structure
        struct node
        {
            node* next_;
            T value_;
        };

        // consumer part
        // accessed mainly by consumer, infrequently be producer
        node* tail_; // tail of the queue

        // delimiter between consumer part and producer part,
        // so that they situated on different cache lines
        char cache_line_pad_ [cache_line_size];

        // producer part
        // accessed only by producer
        node* head_; // head of the queue
        node* first_; // last unused node (tail of node cache)
        node* tail_copy_; // helper (points somewhere between first_ and tail_)

        node* alloc_node()
        {
            // first tries to allocate node from internal node cache,
            // if attempt fails, allocates node via ::operator new()

            if (first_ != tail_copy_)
            {
                node* n = first_;
                first_ = first_->next_;
                return n;
            }
            tail_copy_ = load_consume(&tail_);
            if (first_ != tail_copy_)
            {
                node* n = first_;
                first_ = first_->next_;
                return n;
            }
            node* n = new node;
            return n;
        }

        spsc_queue(spsc_queue const&);
        spsc_queue& operator = (spsc_queue const&);
};

void writer(spsc_queue<int>* q, int n)
{
    for(int i=0;i<n;++i)
    {
        q->enqueue(i);
    }
}

void reader(spsc_queue<int>* q, int n)
{
    int v;
    for(int i=0;i<n;++i)
    {
        while(!q->dequeue(v));
        BOOST_ASSERT(v == i);
    }
}

// usage example
int main(int argc, char** argv)
{
    int n = 10000000;
    if(argc >= 2)
        n = atoi(argv[1]);

    spsc_queue<int> q;

    tbb::tick_count start, end;
    start = tbb::tick_count::now();
//  boost::timer timer;
//  timer.restart();
//  boost::thread tr(boost::bind(reader, &q, n));
//  boost::thread tw(boost::bind(, &q, n));
    tbb::tbb_thread tw(boost::bind(&writer, &q, n));
    tbb::tbb_thread tr(boost::bind(&reader, &q, n));

    tr.join();
    tw.join();

    end = tbb::tick_count::now();
    float total = (end - start).seconds()*1000.0;

//  double elapsed = timer.elapsed();
    printf("enqueue/dequeue %d elements in %f ms\n", n, total);

    return 0;
}
