/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @file ScalablePoolAllocator.cpp
 * @todo Put file description here.
 *
 * @date Mar 1, 2009 nothing - Initial version created.
 */

/**
 * Arguments
 * @li -perf    Performance test
 * @li -intg    Integrity test
 */

#include <iostream>
#include <cstring>
#include "core/ScalablePoolAllocator.h"
#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>

using namespace std;

enum TestMode
{
    Unknown = 0,
    Performance = 1,
    Integrity
};

TestMode testMode = Unknown;
log4cxx::LoggerPtr logg(log4cxx::Logger::getLogger("zillians.common.core.ScalablePoolAllocatorTest"));

int testPerformance();
int testIntegrity();

int main(int argc, char** argv)
{
    log4cxx::BasicConfigurator::configure();

    // Parse argv
    for(int i = 1; i < argc; ++i)
    {
        if(strcmp(argv[i], "-perf") == 0)
        {
            testMode = Performance;
        }
        else if(strcmp(argv[i], "-intg") == 0)
        {
            testMode = Integrity;
        }
    }

    // No test mode selected, error
    if(testMode == Unknown)
    {
        LOG4CXX_ERROR(logg, "No test selected. Use -perf for performance test, -intg for integrity test.");
        return -1;
    }

    int ret = 0;
    if(testMode == Performance)
    {
        ret = testPerformance();
    }
    else if(testMode == Integrity)
    {
        ret = testIntegrity();
    }

    // Finish
    return ret;
}


struct Allocation
{
    size_t sz;
    byte* data;
};


/**
 * Test allocation and deallocation speed by allocate/deallocate
 * 1000000 times.
 *
 * Default settings:
 * @li 4 threads
 * @li Random allocation between 4 and 16384 bytes. Making half of them in bins, half of them in large allocation.
 *
 * @return 0 for success, fail otherwise
 */
int testPerformance()
{
    const size_t sz = 100 * 1048576;
    byte* buf = new byte[sz];// 100MB
    zillians::ScalablePoolAllocator allocator(buf, sz);



    delete[] buf;
    return -1;
}


int testIntegrity()
{
    return -1;
}
