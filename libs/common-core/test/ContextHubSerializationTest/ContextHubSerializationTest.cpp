/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "core/Prerequisite.h"
#include "core/ContextHub.h"
#include "core/ContextHubSerialization.h"
#include <iostream>
#include <string>
#include <limits>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#define BOOST_TEST_MODULE ContextHubSerializationTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

using namespace zillians;
using namespace std;

BOOST_AUTO_TEST_SUITE( ContextHubSerializationTest )

struct Context1
{
    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar & data;
    }

    std::string data;
};

struct Context2
{
    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar & a;
        ar & b;
    }

    int a;
    int b;
};

BOOST_AUTO_TEST_CASE( ContextHubSerializationTestCase1 )
{
    const char * testfile = "hello.txt";

    // serialize
    {
        ContextHub<ContextOwnership::transfer> hub;

        // prepare data
        Context1* ctx1 = new Context1;
        ctx1->data = "orz";
        hub.set<Context1>(ctx1);

        Context2* ctx2 = new Context2;
        ctx2->a = 0xFFFFABAB; ctx2->b = 0xAAAABBBB;
        hub.set<Context2>(ctx2);

        std::ofstream ofs(testfile);
        boost::archive::text_oarchive oa(ofs);

        ContextHubSerialization<boost::mpl::vector<Context1, Context2> > serializer(hub);
        oa << serializer;
    }

    // deserialize
    {
        ContextHub<ContextOwnership::transfer> hub;

        std::ifstream ifs(testfile);
        boost::archive::text_iarchive ia(ifs);

        ContextHubSerialization<boost::mpl::vector<Context1, Context2> > serializer(hub);
        ia >> serializer;

        Context1* ctx1 = hub.get<Context1>();
        BOOST_CHECK(ctx1 != NULL);
        if(ctx1)
        {
            BOOST_CHECK(ctx1->data == "orz");
        }

        Context2* ctx2 = hub.get<Context2>();
        BOOST_CHECK(ctx2 != NULL);
        if(ctx2)
        {
            BOOST_CHECK(ctx2->a == 0xFFFFABAB);
            BOOST_CHECK(ctx2->b == 0xAAAABBBB);
        }

    }
}

BOOST_AUTO_TEST_SUITE_END()
