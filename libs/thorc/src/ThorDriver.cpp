/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <iterator>

#include <boost/range/begin.hpp>
#include <boost/range/end.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/format.hpp>

#include "utility/StringUtil.h"

#include "language/logging/LoggerWrapper.h"
#include "language/stage/serialization/detail/ASTSerializationHelper.h"
#include "language/ThorDriver.h"
#include "language/ThorToolBase.h"
#include "language/ToolExitCode.h"


namespace zillians { namespace language {

namespace po = boost::program_options;
namespace fs = boost::filesystem;

namespace
{

bool is_abbreviate_of(const std::string& a, const std::string& b)
{
    return b.compare(0, a.size(), a) == 0;
}

}


int ThorDriver::main(int argc, const char** argv)
{
    LoggerWrapper::instance();

    return run(argc, argv);
}

ThorDriver::CmdOptions::iterator ThorDriver::findCmdEnd(ThorDriver::CmdOptions& options, const ThorDriver::CmdOptions& cmds)
{
    CmdOptions::iterator end = options.end();
    for(CmdOptions::iterator it = options.begin(); it != end; ++it)
    {
        if(find(cmds.begin(),cmds.end(),*it) != cmds.end())
        {
            return ++it;
        }
    }
    return end;
}

fs::path ThorDriver::findSharedLibrary(const std::string& name, const std::string& lpath)
{
    std::vector<fs::path> search_path;

    if(!lpath.empty())
    {
        std::vector<std::string> tokens = StringUtil::tokenize(lpath, ";", false);
        for(auto& token : tokens)
        {
            search_path.push_back(fs::path(token));
        }
    }

    search_path.push_back(fs::path("/usr/local/lib"));
    search_path.push_back(fs::path("/usr/lib"));

    for(auto& search_from : search_path)
    {
        std::string filename = THOR_PREFIX_LIBRARY + name + THOR_EXTENSION_SO;
        fs::path p = search_from / filename;
        if(fs::exists(p))
            return p;
    }

    return fs::path();
}

fs::path ThorDriver::findLibrary(const std::string& name, const std::string& lpath)
{
    std::vector<fs::path> search_path;

    if(!lpath.empty())
    {
        auto tokens = StringUtil::tokenize(lpath, ";", false);
        for(auto& token : tokens)
        {
            search_path.push_back(fs::path(token));
        }
    }

    search_path.push_back(fs::path("/usr/local/lib"));
    search_path.push_back(fs::path("/usr/lib"));

    for(auto& search_from : search_path)
    {
        std::string filename = THOR_PREFIX_LIBRARY + name + THOR_EXTENSION_LIB;
        fs::path p = search_from / filename;
        if(fs::exists(p))
            return p;
    }

    return fs::path();
}

int ThorDriver::run(int argc, const char** argv)
{
    auto help = [&]() {
        std::cerr <<
                "Usage: thorc <command> [command options]\n"
                "\n"
                "Allowed commands: \n"
                "   project(p)    Project creation and management.\n"
                "   build(b)      Compile and build project.\n"
                "   generate(g)   Generate stubs needed for client libraries.\n"
                "   extract(e)    Extract dependant bundles.\n"
                "   run(r)        Run on virtual machine.\n"
                "   help(h)       Show this help.\n"
                << std::endl;
    };

    CmdOptions args = { std::next(argv), std::next(argv, argc) };
    if(args.empty())
    {
        help();
        return ToolExitCode::Success;
    }

    std::string& cmd = *boost::begin(args);
    CmdOptions remnant = { std::next(boost::begin(args)), boost::end(args) };
    if(is_abbreviate_of(cmd, "project"))
    {
        return runCommandProject(remnant);
    }
    else if(is_abbreviate_of(cmd, "build"))
    {
        return runCommandBuild(remnant);
    }
    else if(is_abbreviate_of(cmd, "generate"))
    {
        return runCommandGenerate(remnant);
    }
    else if(is_abbreviate_of(cmd, "extract"))
    {
        return runCommandExtract();
    }
    else if(is_abbreviate_of(cmd, "run"))
    {
        return runCommandRun(remnant);
    }
    else if(is_abbreviate_of(cmd, "help") || cmd == "--help" || cmd == "-h")
    {
        help();
        return ToolExitCode::Success;
    }

    std::cerr << "thorc error: no recognized command is given." <<std::endl;
    help();
    return ToolExitCode::Failed;
}

int ThorDriver::runCommandProject(CmdOptions args)
{
    auto help = [&]() {
        std::cerr <<
                "Usage: thorc project [options] <command> [command options]\n"
                "\n"
                "Allowed commands: \n"
                "   create     Create project.\n"
                "   push       Push project to remote repository.\n"
                "   help       Show this help.\n"
                << std::endl;
    };

    if(args.empty())
    {
        help();
        return ToolExitCode::Success;
    }

    std::string& cmd = args[0];
    if(is_abbreviate_of(cmd, "create"))
        return runCommandProjectCreate({args.begin()+1,args.end()});

    else if(is_abbreviate_of(cmd, "push"))
    {
        std::cout<< "thorc project push is not yet implemented!" << std::endl;
        return ToolExitCode::Success;
    }
    else if(is_abbreviate_of(cmd, "help") || cmd == "--help" || cmd == "-h")
    {
        help();
        return ToolExitCode::Success;
    }
    else
    {
        std::cerr << "thorc project error: no recognized command is given." << std::endl;
        help();
        return ToolExitCode::Failed;
    }
}

int ThorDriver::runCommandProjectCreate(CmdOptions args)
{
    // avoid putting a project within another project
    fs::path dummy;
    if(ThorBuildConfiguration::findProjectRoot(dummy, fs::current_path()))
    {
        std::cerr << "Error: nested project creation is not allowed" << std::endl;
        return ToolExitCode::Failed;
    }

    po::options_description options_desc;
    options_desc.add_options()
            ("help,h", "Show the help")
            ("interactive,i", "Interactively create project")
            ("author", po::value<std::string>(), "Set project author name")
            ("version", po::value<std::string>(), "Set project version")
            ("no-system", po::bool_switch(), "Disable automatic system bundle import")
            ;

    auto help = [&]() {
        std::cerr <<
                "Usage: thorc project create [project_name] [options]\n"
                "\n"
                "Allowed options: \n";

        std::cerr << options_desc << std::endl;
    };

    auto interactive = [&]() {
        // interactive ask for project definition
        std::cout << "Project Name: "; std::cin >> config.manifest.name;
        std::cout << "Author Name: "; std::cin >> config.manifest.author;

        while(true)
        {
            std::cout << "Allow native linkage? [yes/no]";

            std::string s; std::cin >> s;

            if(s == "y" || s == "yes")      config.manifest.bundle_type = ThorManifest::BundleType::NATIVE;
            else if(s == "n" || s == "no")  config.manifest.bundle_type = ThorManifest::BundleType::JIT;
            else continue;

            std::cout << std::endl;
            break;
        }
    };

    if(args.empty())
        interactive();

    else
    {
        try
        {
            //TBD: any more natural way to deal with this case with program_options?
            config.manifest.name = args[0];
            po::variables_map vm;
            store(po::command_line_parser({args.begin()+1,args.end()}).options(options_desc).run(),vm);
            notify(vm);

            if(vm.count("help"))
            {
                help();
                return ToolExitCode::Success;
            }

            if(vm.count("author"))
                config.manifest.author = vm["author"].as<std::string>();

            if(vm.count("version"))
                config.manifest.author = vm["version"].as<std::string>();

            config.manifest.no_system = vm["no-system"].as<bool>();

            if(vm.count("interactive"))
                interactive();
        }
        catch(const po::error& e)
        {
            std::cerr << "thorc project create error: no recognized command line options is available. " << std::endl;
            help();
            return ToolExitCode::Failed;
        }
    }

    // check if there's existing directory as project name
    fs::path project_path = fs::current_path() / config.manifest.name;
    if(fs::exists(project_path) && fs::is_directory(project_path))
    {
        std::cerr << "Error: directory '" << config.manifest.name << "' already exists" << std::endl;
        return ToolExitCode::Failed;
    }

    // try to create the project directories and manifest files
    config.loadDefault(true, false, false);
    config.configureDefaultPaths(project_path);

    if(!config.createDirectoriesIfNecessary())
    {
        std::cerr << "Error: failed to create project directories" << std::endl;
    }

    const auto manifest_dtd = config.sdk_executable_path / THOR_DEFAULT_MANIFEST_DTD_PATH;
    XMLValidator validator(manifest_dtd);

    if(!config.manifest.save(config.manifest_path, validator))
    {
        std::cerr << "Error: failed to create manifest" << std::endl;
        return ToolExitCode::Failed;
    }

    std::cout << "Project '" << config.manifest.name << "' is created under " << config.project_path.string() << std::endl;

    return ToolExitCode::Success;
}

int ThorDriver::runCommandBuild(CmdOptions args)
{
    config.loadDefault(true, false, false);

    po::options_description options_desc;
    options_desc.add_options()
        ("project-path", po::value<std::string>(), "Override the default build path")
        ("build-path", po::value<std::string>(), "Override the default build path")
        ("output-path", po::value<std::string>(), "Override the default output path")
        ("verbose,v", po::bool_switch(), "Verbose mode, which dumps all commands")
        ("dump-ts", po::bool_switch(), "Dump intermediate Thor")
        ("dump-llvm,l", po::bool_switch(), "Dump LLVM IR")
        ("dump-graphviz", po::bool_switch(), "Dump tree structure in graphviz")
        ("jobs,j", po::value<int>()->default_value(1), "Allow N jobs at once; automatically decided if not specified")
        ("static-test-parse", "Run into static test for script parsing")
        ("static-test-constant-folding", "Run into static test for constant folding")
        ("static-test-s0", "Run into static test for semantic checking (s0)")
        ("static-test-xform", "Run into static test for AST transforming")
        ("static-test-cast", "Run into static test for implicit casting")
        ("static-test-s1", "Run into static test for semantic checking (s1)")
        ("optimization-level,O", po::value<unsigned>(), "Set the optimization level (0~3), and the default opt level for release build is 2 while for debug build is 0. However, in debug build, you could explicitly set optimizeation level which would overwritten the default value.")
        ("debug-tool,g", po::value<std::string>()->implicit_value("thor-compile"), "Debug specific Thor tool. e.g: thorc b --debug-tool=thor-compile")
        ("debug-tool-cmd", po::value<std::string>(), "Custom debugger command to debug specific tool from --debug-tool")
        ("timing,t", po::bool_switch(), "Timing elapsed time of each stage")
        ("codegen-type", po::value<ThorBuildConfiguration::CodeGenType>()->default_value(ThorBuildConfiguration::CodeGenType::NATIVE), "Set the code generation type of the build. Native = 0, JIT tangle AST = 1, JIT bundle AST = 2. Default to 0.")
        ("extract-bundle-path", po::value<std::string>(), "Explicitly designate the path to find dependent bundle AST. (For JIT bundle handling only)")
    ;

    auto help = [&]() {
        std::cerr <<
                "Usage: thorc build <configuration/command> [options]\n"
                "\n"
                "Allowed configuration/command: \n"
                "   debug,d          Build with debug symbol and disable all optimizations.(default)\n"
                "   release,rel,r    Build without debug symbol and turn on all optimizations.\n"
                "   help             Show this help.\n"
                "\n"
                "Allowed options: \n";

        std::cerr << options_desc << std::endl;
    };

    bool loaded_from_cache = false;
    auto load_default_configuration = [&]() {
        // TODO load the last build configuration
        if(!config.loadDefault(false, true, true))
            return false;

        if(config.loadFromCache(true))
            loaded_from_cache = true;

        return true;
    };

    unsigned opt_level                 = 0;
    bool     emit_debug_info           = false;
    bool     project_path_was_provided = false;

    if(args.empty())
    {
        if(!load_default_configuration())
        {
            return ToolExitCode::Failed;
        }
    }
    else
    {
        std::string& cmd = *boost::begin(args);
        std::vector<std::string> build_args;

        if(is_abbreviate_of(cmd, "debug"))
        {
            emit_debug_info = true;
            opt_level       = 0;
            build_args.assign(std::next(boost::begin(args)), boost::end(args));
        }
        else if(is_abbreviate_of(cmd, "release"))
        {
            emit_debug_info = false;
            opt_level       = 2;
            build_args.assign(std::next(boost::begin(args)), boost::end(args));
        }
        else if(is_abbreviate_of(cmd, "help") || cmd == "--help" || cmd == "-h")
        {
            help();
            return ToolExitCode::Success;
        }
        else
        {
            build_args = args;
            if(!load_default_configuration())
                return ToolExitCode::Failed;
        }

        try
        {
            po::variables_map vm;
            store(po::command_line_parser(build_args).options(options_desc).run(), vm);
            notify(vm);

            if(project_path_was_provided = vm.count("project-path"))
            {
                fs::path project_path = vm["project-path"].as<std::string>();

                config.configureProjectPaths(project_path);
            }

            config.max_parallel_jobs = vm["jobs"].as<int>();

            if(vm.count("build-path"))
                config.build_path_override = vm["build-path"].as<std::string>();

            if(vm.count("output-path"))
                config.binary_output_path_override = vm["output-path"].as<std::string>();

            config.verbose       = vm["verbose"      ].as<bool>();
            config.dump_ts       = vm["dump-ts"      ].as<bool>();
            config.dump_llvm     = vm["dump-llvm"    ].as<bool>();
            config.dump_graphviz = vm["dump-graphviz"].as<bool>();
            config.timing        = vm["timing"       ].as<bool>();
            config.codegen_type  = vm["codegen-type" ].as<ThorBuildConfiguration::CodeGenType>();

            if(vm.count("optimization-level"))
                opt_level = vm["optimization-level"].as<unsigned>();

            if(vm.count("extract-bundle-path"))
                config.extract_bundle_path_override = vm["extract-bundle-path"].as<std::string>();

            if(vm.count("static-test-parse"))
                config.static_test_case = "static-test-parse";

            if(vm.count("static-test-constant-folding"))
                config.static_test_case = "static-test-constant-folding";

            if(vm.count("static-test-s0"))
                config.static_test_case = "static-test-s0";

            if(vm.count("static-test-xform"))
                config.static_test_case = "static-test-xform";

            if(vm.count("static-test-cast"))
                config.static_test_case = "static-test-cast";

            if(vm.count("static-test-s1"))
                config.static_test_case = "static-test-s1";

            if(vm.count("debug-tool"))
                config.debug_tool = vm["debug-tool"].as<std::string>();

            if(vm.count("debug-tool-cmd"))
                config.debug_tool_cmd = vm["debug-tool-cmd"].as<std::string>();
        }
        catch(const po::error& e)
        {
            std::cerr << "thorc build error: failed to parse command line: " << e.what() << std::endl;
            help();
            return ToolExitCode::Failed;
        }
    }

    if(!loaded_from_cache)
    {
        if(!config.loadDefault(false, !project_path_was_provided, true))
            return ToolExitCode::Failed;
    }

    if(!config.createDirectoriesIfNecessary())
        return ToolExitCode::Failed;

    if(config.codegen_type == ThorBuildConfiguration::CodeGenType::NATIVE)
        return build(emit_debug_info, opt_level);
    else
        return jitCodeGen();
}

int ThorDriver::runCommandGenerate(CmdOptions args)
{
    auto help = [&]() {
        std::cerr <<
                "Usage: thorc generate <command> [command options]\n"
                "\n"
                "Allowed commands: \n"
                "   bundle      Create or update project bundle\n"
                "   doc         Generate source documents\n"
                "   stub        Generate stubs for client libraries\n"
                "   help        Show this help\n"
                << std::endl;
    };

    if(args.empty())
    {
        help();
        return ToolExitCode::Success;
    }
    
    std::string& cmd = args[0];
    CmdOptions remnant = {args.begin()+1, args.end()};
    if(cmd == "bundle" || cmd == "b")
        return runCommandGenerateBundle(remnant);

    else if(cmd == "doc" || cmd == "d")
        return runCommandGenerateDoc(remnant);

    else if(cmd == "stub" || cmd == "s")
        return runCommandGenerateStub(remnant);

    else if(cmd == "help" || cmd == "h" || cmd == "--help" || cmd == "-h")
    {
        help();
        return ToolExitCode::Success;
    }

    else
    {
        std::cerr << "thorc generate error: no recognized command is given" << std::endl;
        help();
        return ToolExitCode::Failed;
    }
}

int ThorDriver::runCommandGenerateBundle(CmdOptions args)
{
    config.loadDefault(true, false, false);

    po::options_description options_desc;
    options_desc.add_options()
            ("help,h", "Show this help")
            ("verbose,v", "Verbose mode, which dumps all commands");

    auto help = [&]() {
        std::cerr <<
                "Usage: thorc project generate bundle [options]\n"
                "\n"
                "Allowed options: \n";

        std::cerr << options_desc << std::endl;
    };

    try
    {
        po::variables_map vm;
        store(po::command_line_parser(args).options(options_desc).run(),vm);
        notify(vm);

        if(vm.count("help"))
        {
            help();
            return ToolExitCode::Success;
        }

        if(vm.count("verbose"))
            config.verbose = true;
        
        else
            config.verbose = false;
    }
    catch(const po::error& e)
    {
        std::cerr << "thorc generate bundle error: "<<e.what()<<std::endl;
        return ToolExitCode::Failed;
    }

    if(!config.loadDefault(false, true, true))
        return ToolExitCode::Failed;

    if(!config.createDirectoriesIfNecessary())
        return ToolExitCode::Failed;

    std::cout << "Generating project bundle to '" << (config.binary_output_path / (config.manifest.name + THOR_EXTENSION_BUNDLE)).string() << std::endl;

    return bundle();
}

int ThorDriver::runCommandGenerateDoc(CmdOptions args)
{
    config.loadDefault(true, false, false);

    po::options_description options_desc;
    options_desc.add_options()
            ("help,h", "Show this help")
            ("verbose,v", "Verbose mode, which dumps all commands");

    auto help = [&]() {
        std::cerr <<
                "Usage: thorc project generate doc [options]\n"
                "\n"
                "Allowed options: \n";

        std::cerr << options_desc << std::endl;
    };

    if(args.empty())
    {
        help();
        return ToolExitCode::Success;
    }

    try
    {
        po::variables_map vm;
        store(po::command_line_parser(args).options(options_desc).run(),vm);
        notify(vm);

        if(vm.count("help"))
        {
            help();
            return ToolExitCode::Success;
        }

        if(vm.count("verbose"))
            config.verbose = true;
        else
            config.verbose = false;
    }
    catch(const po::error& e)
    {
        std::cerr << "thorc generate doc error: "<<e.what()<<std::endl;
        return ToolExitCode::Failed;
    }

    if(!config.loadDefault(false, true, true))
        return ToolExitCode::Failed;

    if(!config.createDirectoriesIfNecessary())
        return ToolExitCode::Failed;

    std::cout << "Generating project document under '" << (config.binary_output_path / "doc").string() << std::endl;

    return doc();
}

int ThorDriver::runCommandGenerateStub(CmdOptions args)
{
    config.loadDefault(true, false, false);

    po::options_description options_desc;
    options_desc.add_options()
            ("help,h", "Show this help")
            ("verbose,v", po::bool_switch(), "Verbose mode, which dumps all commands")
            ("cpp", po::bool_switch(), "Generate client stub in C++")
            ("java", po::bool_switch(), "Generate client stub in Java")
            ("js", po::bool_switch(), "Generate client stub in JavaScript");

    auto help = [&]() {
        std::cerr <<
                "Usage: thorc project generate stub [options]\n"
                "\n"
                "Allowed options: \n";

        std::cerr << options_desc << std::endl;
    };

    if(args.empty())
    {
        help();
        return ToolExitCode::Success;
    }

    bool generate_cpp = false;
    bool generate_java = false;
    bool generate_js = false;
    try
    {
        po::variables_map vm;
        store(po::command_line_parser(args).options(options_desc).run(),vm);
        notify(vm);
        if(vm.count("help"))
        {
            help();
            return ToolExitCode::Success;
        }

        config.verbose = vm["verbose"].as<bool>();
        generate_cpp = vm["cpp"].as<bool>();
        generate_java = vm["java"].as<bool>();
        generate_js = vm["js"].as<bool>();
    }
    catch(const po::error& e)
    {
        std::cerr << "thorc generate stub error: " << e.what() << std::endl;
        return ToolExitCode::Failed;
    }

    if(!config.loadDefault(false, true, true))
        return ToolExitCode::Failed;

    if(!config.createDirectoriesIfNecessary())
        return ToolExitCode::Failed;

    if(generate_cpp)
    {
        std::cout << "Generating C++ client stub under '" 
                  << (config.binary_output_path / "stub").string() << std::endl;

        return generateCppClientStub();
    }

    if(generate_js)
    {
        std::cout << "Generating JavaScript client stub under '" 
                  << (config.binary_output_path / "stub").string() << std::endl;

        return generateJsClientStub();
    }

    return ToolExitCode::Success;
}

int ThorDriver::runCommandExtract()
{
    if(!config.loadFromCache(false/* track change */))
    {
        if(!config.loadDefault(false, true/* configure_path */, true/* configure_manifest */))
        {
            return ToolExitCode::Failed;
        }
    }

    return unbundle();
}

int ThorDriver::runCommandRun(CmdOptions args)
{
    if(!config.loadFromCache(true))
    {
        config.loadDefault(true, false, false);
    }

    po::options_description options_desc;
    options_desc.add_options()
        ("help,h", "Show this help")
        ("server,s", po::bool_switch(), "Create a server VM instance")
        ("client,c", po::bool_switch(), "Create a client VM instance")
            //yentien: --args option is here to show some nice text only, it will never be processed by po
            //         due to its non-standard behavior.
        ("args,a",  "Specify the arguments to pass to the entry function") 
        ("dev", po::value<int>(), "Specify the device id for accelerator domain")
        ("verbose,v", po::bool_switch(), "Verbose mode, which dumps all commands")
        ("domain,D", po::value<std::string>(), "Specify the type of domain, options are:\n\n"
                                               "    st: single thread\n"
                                               "    mt: multithread\n"
                                               "    kepler: TBD\n"
                                               "    tahiti: TBD\n"
                                               "    opengl: TBD\n"
                                               "    cuda: TBD\n"
                                               "    gpu: TBD\n"
                                               "    k10m: TBD\n")
        ("debug-tool,g", po::value<std::string>()->implicit_value("thor-vm"), "Debug specific Thor tool. e.g: thorc run func --debug-tool=thor-vm")
        ("debug-tool-cmd", po::value<std::string>(), "Custom debugger command to debug specific tool from --debug-tool")
    ;

    auto help = [&]() {
        std::cerr <<
                "Usage: thorc run <entry> [options]\n"
                "\n"
                "Allowed options: \n";

        std::cerr << options_desc << std::endl;
    };

    if(args.empty())
    {
        std::cerr << "thorc run error: no entry function name is given."<<std::endl;
        help();
        return ToolExitCode::Failed;
    }

    const std::string& entry = args[0];
    bool run_as_server = false;
    bool run_as_client = false;
    std::string domain;
    int dev_id = 0;

    //when only --help or -h is given.
    if(entry == "--help" || entry == "-h")
    {
        help();
        return ToolExitCode::Success;
    }

    try
    {
        CmdOptions::iterator ts_arg_begin = findCmdEnd(args,{"--args","-a"});
        po::variables_map vm;

        store(po::command_line_parser({args.begin()+1,ts_arg_begin}).options(options_desc).run(),vm);
        notify(vm);
        CmdOptions ts_args({ts_arg_begin,args.end()});

        if(vm.count("help"))
        {
            help();
            return ToolExitCode::Success;
        }

        run_as_server = vm["server"].as<bool>();
        run_as_client = vm["client"].as<bool>();

        if(vm.count("domain"))
            domain = vm["domain"].as<std::string>();

        if(vm.count("dev"))
            domain = vm["dev"].as<int>();

        config.verbose = vm["verbose"].as<bool>();

        if(vm.count("debug-tool"))
            config.debug_tool = vm["debug-tool"].as<std::string>();

        if(vm.count("debug-tool-cmd"))
            config.debug_tool_cmd = vm["debug-tool-cmd"].as<std::string>();

        if(!config.loadDefault(false, true, true))
            return ToolExitCode::Failed;

        if(!config.createDirectoriesIfNecessary())
            return ToolExitCode::Failed;

        return run(run_as_server, run_as_client, entry, ts_args, domain, dev_id);
    }
    catch(const po::error& e)
    {
        std::cerr << "thorc run error: " << e.what() << std::endl;
        help();
        return ToolExitCode::Failed;
    }
}

int ThorDriver::prepareEnvironment()
{
    using namespace boost::filesystem;

    // copy all extracted library to the bin folder
    std::vector<boost::filesystem::path> all_libraries_under_dep = Filesystem::collect_files(config.extract_bundle_path, THOR_EXTENSION_LIB, true);
    for(auto& library : all_libraries_under_dep)
    {
        path dest = config.binary_output_path / library.filename();
        if(!Filesystem::copy_file_if_differnt(library, dest))
        {
            std::cerr << "Error: failed to copy extracted shared object to binary directory" << std::endl;
            return ToolExitCode::Failed;
        }
    }

    // copy all extracted shared objects to the bin folder
    std::vector<boost::filesystem::path> all_shared_libraries_under_dep = Filesystem::collect_files(config.extract_bundle_path, THOR_EXTENSION_SO, true);
    for(auto& library : all_shared_libraries_under_dep)
    {
        path dest = config.binary_output_path / library.filename();
        if(!Filesystem::copy_file_if_differnt(library, dest))
        {
            std::cerr << "Error: failed to copy extracted shared object to binary directory" << std::endl;
            return ToolExitCode::Failed;
        }
    }

    // copy all dependent share objects to the bin folder
    for(auto& library : config.manifest.deps.native_shared_libraries)
    {
        fs::path p = findSharedLibrary(library.name, library.lpath);
        if(p.empty())
        {
            std::cerr << "Warn: failed to find dependent shared object \"" << library.name << "\", ignored" << std::endl;
            continue;
        }

        std::string filename = THOR_PREFIX_LIBRARY + library.name + THOR_EXTENSION_SO;
        path dest = config.binary_output_path / filename;
        if(!Filesystem::copy_file_if_differnt(p, dest))
        {
            std::cerr << "Error: failed to copy dependent shared object \"" << p << "\" to binary directory" << std::endl;
            return ToolExitCode::Failed;
        }
    }
    return ToolExitCode::Success;
}

tree::Tangle* ThorDriver::mergeAllTangles()
{
    using namespace tree;

    std::vector<fs::path> all_ast_files = Filesystem::collect_files(config.build_path, THOR_EXTENSION_AST);
    tree::Tangle* tangle = NULL;

    for(auto& ast : all_ast_files)
    {
        ASTNode* deserialized = stage::ASTSerializationHelper::deserialize(ast.string());

        if(!deserialized || !isa<Tangle>(deserialized))
        {
            std::cerr << "Error: failed to deserialize AST file: " << ast.string() << std::endl;
        }
        else
        {
            Tangle* t = cast<Tangle>(deserialized);
            if(!tangle) tangle = t;
            else        tangle->merge(*t);  // merge with existing tangle
        }
    }

    return tangle;
}

int ThorDriver::generateStub(const std::vector<std::string>& stubTypes)
{
    tree::Tangle* tangle = mergeAllTangles();
    fs::path stubAstPath = config.build_path / ("stub-" + config.manifest.name + THOR_EXTENSION_AST);
    stage::ASTSerializationHelper::serialize(stubAstPath.string(), tangle);

    for(auto& type : stubTypes)
    {
        std::string parameters = " " + stubAstPath.string() +
                                 " --output-path='" + config.stub_output_path.string() + "'" +
                                 " --stub-type=" + type +
                                 " --game-name=" + config.manifest.name;

        int ec = ThorToolBase::shell(config, THOR_STUB, parameters);
        if(ec != ToolExitCode::Success)
        {
            return ec;
        }
    }
    return ToolExitCode::Success;
}

int ThorDriver::generateCppClientStub()
{
    return generateStub({"CLIENT_CLIENTSTUB_H",
                         "CLIENT_GAMESERVICE_CPP",
                         "CLIENT_GAMESERVICE_H"});
}

int ThorDriver::generateJsClientStub()
{
    return generateStub({"CLIENT_RPC_JS"});
}

int ThorDriver::generateServerStub()
{
    return generateStub({"GATEWAY_GAMECOMMAND_CLIENTCOMMANDOBJECT_H",
                         "GATEWAY_GAMECOMMAND_CLOUDCOMMANDOBJECT_H",
                         "GATEWAY_GAMECOMMAND_GAMECOMMANDTRANSLATOR_CPP",
                         "GATEWAY_GAMECOMMAND_GAMEMODULE_MODULE"});
}

int ThorDriver::doc()
{
    fs::path doxyfile = config.build_path / "Doxyfile";

    if(!fs::exists(doxyfile))
    {
        if(ThorToolBase::shell(config, THOR_DOC, " -g " + doxyfile.string()) != 0)
        {
            std::cerr << "Error: cannot generate Doxyfile for source document generation" << std::endl;
            return ToolExitCode::Failed;
        }
        else
        {
            // read configuration into string lines
            std::ifstream fin(doxyfile.string());
            if(!fin.good()) 
                return ToolExitCode::Failed;

            std::vector<std::string> lines;
            for(std::string line; std::getline(fin, line); )
                lines.push_back(line);
            fin.close();

            // change a few options here
            std::map<std::string, std::string> configs = {
                {"OUTPUT_DIRECTORY", "doc"},
                {"RECURSIVE", "YES"},
                {"EXTRACT_ALL", "YES"},
                {"THOR_ROOT_DIR", (fs::current_path() / "src").string()}
            };

            // update the Doxyfile
            std::ofstream fout(doxyfile.string());
            if(!fout.good()) 
                return ToolExitCode::Failed;

            for(auto& line : lines)
            {
                for(auto& config : configs)
                {
                    std::string key, value;
                    std::tie(key, value) = config;
                    if(boost::algorithm::starts_with(line, key))
                    {
                        line = key + " = " + value;
                        break;
                    }
                }
                fout << line << std::endl;
            }
        }
    }

    int ec = ThorToolBase::shell(config, THOR_DOC, " " + doxyfile.string());
    if(ec != 0)
        std::cerr << "Error: cannot generate document" << std::endl;

    return ec;
}

//////////////////////////////////////////////////////////////////////////////
int ThorDriver::build(bool emit_debug_info, unsigned opt_level)
{
    if(config.verbose)
        std::cout << "Building project '" << config.manifest.name << "'" << std::endl;

    int ec = ToolExitCode::Failed;
    do
    {
        ec = unbundle();
        if(ec != ToolExitCode::Success)
            break;

        ec = prepareEnvironment();
        if(ec != ToolExitCode::Success)
            return ToolExitCode::Failed;

        ec = make(emit_debug_info, opt_level);
        if(ec != ToolExitCode::Success)
            break;

        if(config.static_test_case.empty())
        {
            ec = link();
            if(ec != ToolExitCode::Success)
                break;
        }
    }while(0);

    // save the build configuration as cache so that we can re-use that next time
    config.saveToCache(true);

    return ec;
}

int ThorDriver::bundle()
{
    int ec = ThorToolBase::shell(config, THOR_BUNDLER,
                                       " -c" \
                                       " --root-dir='" + config.project_path.string() + "'" \
                                       " --build-path='" + config.build_path.string() + "'");
    if(ec != ToolExitCode::Success)
        std::cerr << "Error: failed to create bundle, error code = " << ec << std::endl;

    return ec;
}

int ThorDriver::unbundle()
{
    // TODO run thor-bundle to unbundle only if the file is not yet unbundle or the source bundle has been changed
    int ec = ToolExitCode::Success;
    if(config.manifest.deps.bundles.size() > 0)
    {
        ec = ThorToolBase::shell(config, THOR_BUNDLER, " -x");
        if(ec != ToolExitCode::Success)
            std::cerr << "Error: failed to un-bundle dependent bundle files, error code = " << ec << std::endl;
    }
    return ec;
}

int ThorDriver::make(bool emit_debug_info, unsigned opt_level)
{
    std::stringstream ss;
    ss << " '--build-path=" << config.build_path.string() << "' -w";

    ss << " --jobs=" << config.max_parallel_jobs;

    if(config.verbose)
        ss << " --verbose";

    if(config.dump_ts)
        ss << " --dump-ts";

    if(config.dump_graphviz)
        ss << " --dump-graphviz";

    if(config.timing)
        ss << " --timing";

    if(!config.static_test_case.empty())
        ss << " --" << config.static_test_case;

    if(!config.prepend_package.empty())
        ss << " --prepend-package=" << config.prepend_package;

    if(emit_debug_info)
        ss << " --emit-debug-info";

    if(opt_level > 0)
        ss << " --optimization-level=" << opt_level;

    if(config.changed)
        ss << " --force-rebuild"; // force rebuild when there's configuration change

    if(!config.debug_tool.empty())
    {
        ss << " --debug-tool=" << config.debug_tool;

        if(!config.debug_tool_cmd.empty())
        {
            ss << " \"--debug-tool-cmd=" << config.debug_tool_cmd << '"';
        }
    }

    // launch the shell command
    int ec = ThorToolBase::shell(config, THOR_MAKE, ss.str());

    if(ec != ToolExitCode::Success)
        std::cerr << "Error: failed to make project, error code = " << ec << std::endl;

    return ec;
}

namespace
{
    int perform_codegen(const ThorBuildConfiguration& config, const boost::filesystem::path& input_ast_path, const boost::filesystem::path& output_ast_path)
    {
        // thor-make would create a merged ast file in the binary path
        std::stringstream ss;
        ss << " --project-path='" << config.project_path.string() << "'"
           << " --build-path='" << config.build_path.string() << "'"
           << " --emit-ast='" << output_ast_path.string() << "'";
        std::cout << "manifest path: " << config.manifest_path.string() << std::endl;

        if(config.codegen_type == ThorBuildConfiguration::CodeGenType::JIT_BUNDLE_AST)
        {
            ss << " --mode-bundle-ast";
            
            if(!config.extract_bundle_path.empty())
            {
                std::vector<boost::filesystem::path> dep_bundle_paths = {input_ast_path};
                for(auto& bundle : config.manifest.deps.bundles)
                {
                    boost::filesystem::path extract_to_path = config.extract_bundle_path / bundle.name;

                    std::vector<boost::filesystem::path> ast_files = Filesystem::collect_files(extract_to_path, THOR_EXTENSION_AST, true);
                    boost::push_back(dep_bundle_paths, ast_files);
                }

                for(auto& dep_path : dep_bundle_paths)
                {
                    ss << " --dep-bundle-asts='" + dep_path.string() + "'";
                }

                ss << " --extract-bundle-path='" << config.extract_bundle_path.string() << "'";
            }
        }
        else //NATIVE or JIT_TANGLE_AST is treated the same way.
        {
            ss << " --load-ast='" << input_ast_path.string() << "'";

            if(!config.extract_bundle_path.empty())
                ss << " --extract-bundle-path='" << config.extract_bundle_path.string() << "'";
        }

        if(config.verbose)
            ss << " --verbose";

        if(config.dump_llvm)
            ss << " --dump-llvm";

        if(config.timing)
            ss << " --timing";

        if(config.dump_graphviz)
        {
            ss << " --dump-graphviz";

            if(!config.dump_graphviz_dir.empty())
                ss << " --dump-graphviz-dir='" << config.dump_graphviz_dir.string() << "'";
        }

        int ec = ThorToolBase::shell(config, THOR_LINKER, ss.str());
        if(ec != ToolExitCode::Success)
            std::cerr << "Error: failed to link and to create shared object, error code = " << ec << std::endl;

        return ec;
    }
}

int ThorDriver::jitCodeGen()
{
    //Don't do anything if not jit bundle. A native bundle should be already been built.
    if(config.manifest.bundle_type != ThorManifest::BundleType::JIT)
        return ToolExitCode::JitIgnore;

    std::cout << "JIT compile project '" << config.manifest.name 
              << "' as "  << ((config.codegen_type == ThorBuildConfiguration::CodeGenType::JIT_BUNDLE_AST) ? "Bundle AST" : "Tangle AST" ) 
              << " mode." << std::endl;

    boost::filesystem::path  input_ast_file = config.binary_output_path / (config.manifest.name + THOR_EXTENSION_AST);
    boost::filesystem::path output_ast_file =         config.build_path / (config.manifest.name + THOR_JIT_AST_POSTFIX + THOR_EXTENSION_AST);
    return perform_codegen(config, input_ast_file, output_ast_file);
}

int ThorDriver::link()
{
    boost::filesystem::path merged_ast_file = config.build_path / (config.manifest.name + THOR_EXTENSION_AST);
    boost::filesystem::path output_ast_file = config.binary_output_path / (config.manifest.name + THOR_EXTENSION_AST);

    // only native bundle needs linking,
    // jit mode has only .ast in the bundle file
    // which will be generate by thor-bundle.
    if(config.manifest.bundle_type == ThorManifest::BundleType::JIT) // just copy the merged ast under build path to binary output path
    {
        if(Filesystem::copy_file_if_differnt(merged_ast_file, output_ast_file))
            return ToolExitCode::Success;
        else
            return ToolExitCode::Failed;
    }

    else
        return perform_codegen(config, merged_ast_file, output_ast_file);
}

int ThorDriver::run(bool run_as_server, bool run_as_client, const std::string& entry, const std::vector<std::string>& args, const std::string& domain, int dev_id)
{
    std::stringstream ss;
    ss << " --project-path=" << config.project_path.string();

    if(run_as_client && run_as_server)
    {
        std::cerr << "Error: virtual machine type can be only set as either server or client" << std::endl;
        return ToolExitCode::Failed;
    }

    if (!config.debug_tool.empty())
    {
        ss << " --debug-tool=" << config.debug_tool;

        if (!config.debug_tool_cmd.empty())
            ss << " --debug-tool-cmd \"" << config.debug_tool_cmd << '"';
    }

    // if not specify any type, default is client
    if (!run_as_client && !run_as_server) 
        run_as_client = true;

    if(run_as_client)
        ss << " --client";

    if(run_as_server)
        ss << " --server";

    if(!domain.empty())
        ss << " --domain=" << domain;

    if(config.verbose)
        ss << " --verbose";

    ss << " --dev=" << dev_id;

    ss << " " + entry;

    if(!args.empty())
    {
        ss << boost::format(" -a %1%") % boost::algorithm::join(
            args, " " 
        );
    }

    int ec = ThorToolBase::shell(config, THOR_VM, ss.str());

    if(ec != 0)
        std::cerr << "Error: virtual machine failed to run on entry '" << entry << "'" << std::endl;

    return ec;
}

} }
