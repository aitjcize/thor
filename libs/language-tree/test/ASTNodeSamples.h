/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ASTNODESAMPLES_H_
#define ASTNODESAMPLES_H_

#include "core/Prerequisite.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFactory.h"

using namespace zillians;
using namespace zillians::language::tree;

ASTNode* createSample1()
{
    Tangle* tangle = new Tangle();
    tangle->root->addSource(new Source("test-program-1", false));
    return tangle;
}

ASTNode* createSample2()
{
    Tangle* tangle = new Tangle();
    {
        Package* com_package = new Package(new SimpleIdentifier(L"com"));
        tangle->root->addPackage(com_package);
        {
            Package* supercell_package = new Package(new SimpleIdentifier(L"supercell"));
            com_package->addPackage(supercell_package);

            Package* zillians_package = new Package(new SimpleIdentifier(L"zillians"));
            com_package->addPackage(zillians_package);
            {
                zillians_package->addSource(new Source("test-program-2", false));
            }
        }
    }
    return tangle;
}

ASTNode* createSample3()
{
    Tangle* tangle = new Tangle();
    {
        Package* com_package = new Package(new SimpleIdentifier(L"com"));
        tangle->root->addPackage(com_package);
        {
            Package* zillians_package = new Package(new SimpleIdentifier(L"zillians"));
            com_package->addPackage(zillians_package);
            {
                Source* source = new Source("test-program-3", false);
                zillians_package->addSource(source);
                {
                    ClassDecl* class_decl = new ClassDecl(new SimpleIdentifier(L"some_class"));
                    source->addDeclare(class_decl);
                    {
                        FunctionDecl* some_member_function = new FunctionDecl(
                                new SimpleIdentifier(L"some_member_function"),
                                NULL,
                                true, false, false, false,
                                Declaration::VisibilitySpecifier::PUBLIC,
                                new NormalBlock());
                        class_decl->addFunction(some_member_function);
                        {
                            Block* block = some_member_function->block;

                            {
                                DeclarativeStmt* stmt = new DeclarativeStmt(new VariableDecl(new SimpleIdentifier(L"a"), new TypeSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT));
                                block->appendObject(stmt);
                            }

                            {
                                DeclarativeStmt* stmt = new DeclarativeStmt(new VariableDecl(new SimpleIdentifier(L"b"), new TypeSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT));
                                block->appendObject(stmt);
                            }

                            {
                                ExpressionStmt* stmt = new ExpressionStmt(new BinaryExpr(BinaryExpr::OpCode::ASSIGN, new PrimaryExpr(new SimpleIdentifier(L"a")), new PrimaryExpr(new SimpleIdentifier(L"b"))));
                                block->appendObject(stmt);
                            }
                        }

                        VariableDecl* some_member_variable1 = new VariableDecl(
                                new SimpleIdentifier(L"some_member_variable1"),
                                new TypeSpecifier(PrimitiveKind::FLOAT64_TYPE),
                                true, false, false,
                                Declaration::VisibilitySpecifier::PUBLIC);
                        class_decl->addVariable(some_member_variable1);

                        VariableDecl* some_member_variable2 = new VariableDecl(
                                new SimpleIdentifier(L"some_member_variable2"),
                                new TypeSpecifier(PrimitiveKind::FLOAT32_TYPE),
                                true, false, false,
                                Declaration::VisibilitySpecifier::PUBLIC);
                        class_decl->addVariable(some_member_variable2);
                    }
                }
            }
        }
    }

    return tangle;
}

/*
com/zillians/test-program-4.t

class some_class {
    public : function some_member_function() {
        var a : some_class;
        var b : some_class;
        a = b;
    }
    public : var some_member_variable1 : some_class;
    public : var some_member_variable2 : some_class;
}
 */
ASTNode* createSample4()
{
    Tangle* tangle = new Tangle();
    {
        Package* com_package = new Package(new SimpleIdentifier(L"com"));
        tangle->root->addPackage(com_package);
        {
            Package* zillians_package = new Package(new SimpleIdentifier(L"zillians"));
            com_package->addPackage(zillians_package);
            {
                Source* source = new Source("test-program-4", false);
                zillians_package->addSource(source);
                {
                    ClassDecl* class_decl = new ClassDecl(new SimpleIdentifier(L"some_class"));
                    source->addDeclare(class_decl);
                    {
                        typedef bool IsMember;
                        typedef bool IsStatic;
                        typedef bool IsConst;

                        FunctionDecl* some_member_function = new FunctionDecl(
                                new SimpleIdentifier(L"some_member_function"),
                                NULL,
                                true, false, false, false,
                                Declaration::VisibilitySpecifier::PUBLIC,
                                new NormalBlock());
                        class_decl->addFunction(some_member_function);
                        {
                            Block* block = some_member_function->block;

                            {
                                DeclarativeStmt* stmt = new DeclarativeStmt(new VariableDecl(new SimpleIdentifier(L"a"), new TypeSpecifier(new SimpleIdentifier(L"some_class")), IsMember(false), IsStatic(false), IsConst(false), Declaration::VisibilitySpecifier::DEFAULT));
                                block->appendObject(stmt);
                            }

                            {
                                DeclarativeStmt* stmt = new DeclarativeStmt(new VariableDecl(new SimpleIdentifier(L"b"), new TypeSpecifier(new SimpleIdentifier(L"some_class")), IsMember(false), IsStatic(false), IsConst(false), Declaration::VisibilitySpecifier::DEFAULT));
                                block->appendObject(stmt);
                            }

                            {
                                ExpressionStmt* stmt = new ExpressionStmt(new BinaryExpr(BinaryExpr::OpCode::ASSIGN, new PrimaryExpr(new SimpleIdentifier(L"a")), new PrimaryExpr(new SimpleIdentifier(L"b"))));
                                block->appendObject(stmt);
                            }
                        }

                        VariableDecl* some_member_variable1 = new VariableDecl(
                                new SimpleIdentifier(L"some_member_variable1"),
                                new TypeSpecifier(new SimpleIdentifier(L"some_class")),
                                IsMember(true), IsStatic(false), IsConst(false),
                                Declaration::VisibilitySpecifier::PUBLIC);
                        class_decl->addVariable(some_member_variable1);

                        VariableDecl* some_member_variable2 = new VariableDecl(
                                new SimpleIdentifier(L"some_member_variable2"),
                                new TypeSpecifier(new SimpleIdentifier(L"some_class")),
                                IsMember(true), IsStatic(false), IsConst(false),
                                Declaration::VisibilitySpecifier::PUBLIC);
                        class_decl->addVariable(some_member_variable2);
                    }
                }
            }
        }
    }

    return tangle;
}

// class with member function template
ASTNode* createSample5()
{
    Tangle* tangle = new Tangle();
    {
        Package* com_package = new Package(new SimpleIdentifier(L"com"));
        tangle->root->addPackage(com_package);
        {
            Package* zillians_package = new Package(new SimpleIdentifier(L"zillians"));
            com_package->addPackage(zillians_package);
            {
                Source* source = new Source("test-program-5", false);
                zillians_package->addSource(source);
                {
                    ClassDecl* class_decl = new ClassDecl(new SimpleIdentifier(L"some_class"));
                    source->addDeclare(class_decl);
                    {
                        // create annotations
                        Annotations* annotations = new Annotations();
                        annotations->appendAnnotation(new Annotation(new SimpleIdentifier(L"server")));
                        // normal function
                        FunctionDecl* some_member_function = new FunctionDecl(
                                new SimpleIdentifier(L"some_member_function"),
                                NULL,
                                true, false, false, false,
                                Declaration::VisibilitySpecifier::PUBLIC,
                                new NormalBlock());
                        class_decl->addFunction(some_member_function);
                        some_member_function->setAnnotations(annotations);

                        // formal function template
                        FunctionDecl* template_function = new FunctionDecl(
                                new TemplatedIdentifier(TemplatedIdentifier::Usage::FORMAL_PARAMETER, new SimpleIdentifier(L"some_member_function")),
                                NULL,
                                true, false, false, false,
                                Declaration::VisibilitySpecifier::PUBLIC,
                                new NormalBlock());
                        class_decl->addFunction(template_function);

                        // specialized function template
                        FunctionDecl* specialized_function = new FunctionDecl(
                                new TemplatedIdentifier(TemplatedIdentifier::Usage::ACTUAL_ARGUMENT, new SimpleIdentifier(L"some_member_function")),
                                NULL,
                                true, false, false, false,
                                Declaration::VisibilitySpecifier::PUBLIC,
                                new NormalBlock());
                        class_decl->addFunction(specialized_function);
                    }
                }
            }
        }
    }

    return tangle;
}

/*
com/zillians/test-program-6.t

class some_class {
    public function some_member_function() {
        var a : some_class;
        var b : some_class;
        a = b;
    }
    private static function static_member_function() {
    }
    public var some_member_variable1 : some_class;
    public var some_member_variable2 : some_class;
    public static var static_variable3 : some_class;
}
 */
ASTNode* createSample6()
{
    Tangle* tangle = new Tangle();
    {
        Package* com_package = new Package(new SimpleIdentifier(L"com"));
        tangle->root->addPackage(com_package);
        {
            Package* zillians_package = new Package(new SimpleIdentifier(L"zillians"));
            com_package->addPackage(zillians_package);
            {
                Source* source = new Source("test-program-4", false);
                zillians_package->addSource(source);
                {
                    ClassDecl* class_decl = new ClassDecl(new SimpleIdentifier(L"some_class"));
                    source->addDeclare(class_decl);
                    {
                        typedef bool IsMember;
                        typedef bool IsStatic;
                        typedef bool IsConst;
                        typedef bool IsVirtual;
                        typedef bool IsOverride;

                        FunctionDecl* some_member_function = new FunctionDecl(
                                new SimpleIdentifier(L"some_member_function"),
                                NULL,
                                IsMember(true), IsMember(true), IsVirtual(false), IsOverride(false),
                                Declaration::VisibilitySpecifier::PUBLIC,
                                new NormalBlock());
                        class_decl->addFunction(some_member_function);
                        {
                            Block* block = some_member_function->block;

                            {
                                DeclarativeStmt* stmt = new DeclarativeStmt(new VariableDecl(new SimpleIdentifier(L"a"), new TypeSpecifier(new SimpleIdentifier(L"some_class")), IsMember(false), IsStatic(false), IsConst(false), Declaration::VisibilitySpecifier::DEFAULT));
                                block->appendObject(stmt);
                            }

                            {
                                DeclarativeStmt* stmt = new DeclarativeStmt(new VariableDecl(new SimpleIdentifier(L"b"), new TypeSpecifier(new SimpleIdentifier(L"some_class")), IsMember(false), IsStatic(false), IsConst(false), Declaration::VisibilitySpecifier::DEFAULT));
                                block->appendObject(stmt);
                            }

                            {
                                ExpressionStmt* stmt = new ExpressionStmt(new BinaryExpr(BinaryExpr::OpCode::ASSIGN, new PrimaryExpr(new SimpleIdentifier(L"a")), new PrimaryExpr(new SimpleIdentifier(L"b"))));
                                block->appendObject(stmt);
                            }
                        }

                        FunctionDecl* static_member_function = new FunctionDecl(
                                new SimpleIdentifier(L"static_member_function"),
                                NULL,
                                IsMember(true), IsStatic(true), IsVirtual(false), IsOverride(false),
                                Declaration::VisibilitySpecifier::PRIVATE,
                                new NormalBlock());
                        class_decl->addFunction(static_member_function);
                        {
                            Block* block = static_member_function->block;
                        }

                        VariableDecl* some_member_variable1 = new VariableDecl(
                                new SimpleIdentifier(L"some_member_variable1"),
                                new TypeSpecifier(new SimpleIdentifier(L"some_class")),
                                IsMember(true), IsStatic(false), IsConst(false),
                                Declaration::VisibilitySpecifier::PUBLIC);
                        class_decl->addVariable(some_member_variable1);

                        VariableDecl* some_member_variable2 = new VariableDecl(
                                new SimpleIdentifier(L"some_member_variable2"),
                                new TypeSpecifier(new SimpleIdentifier(L"some_class")),
                                IsMember(true), IsStatic(false), IsConst(false),
                                Declaration::VisibilitySpecifier::PUBLIC);
                        class_decl->addVariable(some_member_variable2);

                        VariableDecl* some_member_variable3 = new VariableDecl(
                                new SimpleIdentifier(L"some_member_variable3"),
                                new TypeSpecifier(new SimpleIdentifier(L"some_class")),
                                IsMember(true), IsStatic(true), IsConst(false),
                                Declaration::VisibilitySpecifier::PRIVATE);
                        class_decl->addVariable(some_member_variable3);
                    }
                }
            }
        }
    }

    return tangle;
}

#endif /* ASTNODESAMPLES_H_ */
