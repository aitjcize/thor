/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */


#include "core/Prerequisite.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/visitor/GarbageCollectionVisitor.h"
#include "language/tree/visitor/ObjectCountVisitor.h"
#include "../ASTNodeSamples.h"
#include <iostream>
#include <string>
#include <limits>

#define BOOST_TEST_MODULE ThorTreeTest_GarbageCollectionVisitor
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

using namespace zillians;
using namespace zillians::language::tree;
using namespace zillians::language::tree::visitor;

BOOST_AUTO_TEST_SUITE( ThorTreeTest_GarbageCollectionVisitorSuite )

BOOST_AUTO_TEST_CASE( ThorTreeTest_GarbageCollectionVisitorTestCase1 )
{
    Source* node = new Source("test-source");

    std::size_t count_before_gc = 0;
    {
        ObjectCountVisitor<> counter;
        counter.visit(*node);
        count_before_gc = counter.get_count();
    }

    {
        GarbageCollectionVisitor<> marker;
        marker.visit(*node);
        BOOST_CHECK(marker.get_sweep_count() == 0);
    }

    std::size_t count_after_gc = 0;
    {
        ObjectCountVisitor<> counter;
        counter.visit(*node);
        count_after_gc = counter.get_count();
    }

    BOOST_CHECK(count_before_gc == count_after_gc);

    BOOST_CHECK(count_after_gc == ASTNodeGC::instance()->cleanup());
}

BOOST_AUTO_TEST_CASE( ThorTreeTest_GarbageCollectionVisitorTestCase2 )
{
    Source* node = new Source("test-source");

    std::size_t count_before_gc = 0;
    {
        ObjectCountVisitor<> counter;
        counter.visit(*node);
        count_before_gc = counter.get_count();
    }

    {
        GarbageCollectionVisitor<> marker;
        marker.visit(*node);
        BOOST_CHECK(marker.get_sweep_count() == 0);
    }

    std::size_t count_after_gc = 0;
    {
        ObjectCountVisitor<> counter;
        counter.visit(*node);
        count_after_gc = counter.get_count();
    }

    BOOST_CHECK(count_before_gc == count_after_gc);

    BOOST_CHECK(count_after_gc == ASTNodeGC::instance()->cleanup());
}

BOOST_AUTO_TEST_CASE( ThorTreeTest_GarbageCollectionVisitorTestCase3 )
{
    ASTNode* node = createSample1();
    std::size_t count_before_gc = 0;
    {
        ObjectCountVisitor<> counter;
        counter.visit(*node);
        count_before_gc = counter.get_count();
    }

    {
        GarbageCollectionVisitor<> marker;
        marker.visit(*node);
        BOOST_CHECK(marker.get_sweep_count() == 0);
    }

    std::size_t count_after_gc = 0;
    {
        ObjectCountVisitor<> counter;
        counter.visit(*node);
        count_after_gc = counter.get_count();
    }

    BOOST_CHECK(count_before_gc == count_after_gc);

    BOOST_CHECK(count_after_gc == ASTNodeGC::instance()->cleanup());
}

BOOST_AUTO_TEST_SUITE_END()
