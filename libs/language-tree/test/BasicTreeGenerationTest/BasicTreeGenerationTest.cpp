/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */


#include <iostream>
#include <string>
#include "core/Prerequisite.h"
#include "utility/UnicodeUtil.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/visitor/GarbageCollectionVisitor.h"
#include "../ASTNodeSamples.h"
#include <iostream>
#include <string>
#include <limits>

// boost test don't support wstring
// see https://svn.boost.org/trac/boost/ticket/1136
namespace std {
    inline std::ostream& operator<<(std::ostream& os, const std::wstring& s) {
        return os << zillians::ws_to_s(s) ;
    }
}

#define BOOST_TEST_MODULE ThorTreeTest_BasicTreeGenerationTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

using namespace zillians;
using namespace zillians::language::tree;
using namespace zillians::language::tree::visitor;

BOOST_AUTO_TEST_SUITE( ThorTreeTest_BasicTreeGenerationTestSuite )

BOOST_AUTO_TEST_CASE( ThorTreeTest_BasicTreeGenerationTestCase1_IsA )
{
    {
        ASTNode *node = new Source("test-source.t", new Package(new SimpleIdentifier(L"")));
        BOOST_CHECK(isa<Source>(node));
        BOOST_CHECK(!isa<Literal>(node));
    }

    {
        ASTNode *node = new BinaryExpr(BinaryExpr::OpCode::ASSIGN, new PrimaryExpr(new SimpleIdentifier(L"abc")), new PrimaryExpr(new NumericLiteral((int64)123L)));
        BOOST_CHECK(isa<Expression>(node));
        BOOST_CHECK(isa<BinaryExpr>(node));
    }

    {
        NestedIdentifier* node = new NestedIdentifier();
        BOOST_CHECK(isa<Identifier>(node));
    }

    {
        VariableDecl* node = new VariableDecl(new SimpleIdentifier(L"test"), new TypeSpecifier(PrimitiveKind::FLOAT32_TYPE), false, false, true, Declaration::VisibilitySpecifier::DEFAULT);
        BOOST_CHECK(isa<VariableDecl>(node));
        BOOST_CHECK(isa<Declaration>(node));
    }
}

BOOST_AUTO_TEST_CASE( ThorTreeTest_BasicTreeGenerationTestCase2 )
{
    {
        Tangle* tangle = new Tangle();
        BOOST_CHECK_EQUAL(tangle->root->id->toString(), L""); // the default package name is ""
    }
}

BOOST_AUTO_TEST_CASE( ThorTreeTest_BasicTreeGenerationTestCase3 )
{
    // test arithmetic promotion
    {
        PrimitiveKind::kind promoted = PrimitiveType::promote(PrimitiveKind::INT8_TYPE, PrimitiveKind::INT8_TYPE);
        BOOST_CHECK(promoted == PrimitiveKind::INT32_TYPE);
    }

    {
        PrimitiveKind::kind promoted = PrimitiveType::promote(PrimitiveKind::INT16_TYPE, PrimitiveKind::INT16_TYPE);
        BOOST_CHECK(promoted == PrimitiveKind::INT32_TYPE);
    }

    {
        PrimitiveKind::kind promoted = PrimitiveType::promote(PrimitiveKind::INT32_TYPE, PrimitiveKind::INT32_TYPE);
        BOOST_CHECK(promoted == PrimitiveKind::INT32_TYPE);
    }

    {
        PrimitiveKind::kind promoted = PrimitiveType::promote(PrimitiveKind::INT64_TYPE, PrimitiveKind::INT64_TYPE);
        BOOST_CHECK(promoted == PrimitiveKind::INT64_TYPE);
    }

    {
        PrimitiveKind::kind promoted = PrimitiveType::promote(PrimitiveKind::INT8_TYPE, PrimitiveKind::INT16_TYPE);
        BOOST_CHECK(promoted == PrimitiveKind::INT32_TYPE);
    }
    {
        PrimitiveKind::kind promoted = PrimitiveType::promote(PrimitiveKind::INT8_TYPE, PrimitiveKind::INT32_TYPE);
        BOOST_CHECK(promoted == PrimitiveKind::INT32_TYPE);
    }
    {
        bool precision_loss = false;
        PrimitiveKind::kind promoted = PrimitiveType::promote(PrimitiveKind::INT8_TYPE, PrimitiveKind::INT64_TYPE);
        BOOST_CHECK(promoted == PrimitiveKind::INT64_TYPE);
    }
}

BOOST_AUTO_TEST_CASE( ThorTreeTest_TreeEqualityTest )
{
    {
        ASTNode* p1_1 = createSample1() ;
        ASTNode* p1_2 = createSample1() ;
        BOOST_CHECK(p1_1->isEqual(*p1_2));
    }

    {
        ASTNode* p2_1 = createSample2() ;
        ASTNode* p2_2 = createSample2() ;
        BOOST_CHECK(p2_1->isEqual(*p2_2));
    }

    {
        ASTNode* p3_1 = createSample3() ;
        ASTNode* p3_2 = createSample3() ;
        BOOST_CHECK(p3_1->isEqual(*p3_2));
    }

    {
        ASTNode* p4_1 = createSample4() ;
        ASTNode* p4_2 = createSample4() ;
        BOOST_CHECK(p4_1->isEqual(*p4_2));
    }
}

BOOST_AUTO_TEST_SUITE_END()
