/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "core/Prerequisite.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/CustomizedSerialization.h"
#include "../ASTNodeSamples.h"
#include <iostream>
#include <string>
#include <limits>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#define BOOST_TEST_MODULE ThorTreeTest_SerializationTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

using namespace zillians;
using namespace zillians::language::tree;
using namespace zillians::language::tree::visitor;

BOOST_AUTO_TEST_SUITE( ThorTreeTest_SerializationTestSuite )

template <typename TreeCreateFunction>
void CreateSaveRestoreComare(TreeCreateFunction f)
{
    // create and save program ast to file
    ASTNode* origTangle = f();
    {
        std::ofstream ofs("p1.txt");
        boost::archive::text_oarchive oa(ofs);
        oa << origTangle;
    }

    // restore ast from saved file
    ASTNode* restoredTangle = NULL;
    {
        std::ifstream ifs("p1.txt");
        boost::archive::text_iarchive ia(ifs);
        ia >> restoredTangle;
    }

    // compare
    BOOST_CHECK(origTangle->isEqual(*restoredTangle));
}

BOOST_AUTO_TEST_CASE( ThorTreeTest_SerializationTestCase1 )
{
    CreateSaveRestoreComare(createSample1);
    CreateSaveRestoreComare(createSample2);
    CreateSaveRestoreComare(createSample3);
    CreateSaveRestoreComare(createSample4);
}

BOOST_AUTO_TEST_SUITE_END()
