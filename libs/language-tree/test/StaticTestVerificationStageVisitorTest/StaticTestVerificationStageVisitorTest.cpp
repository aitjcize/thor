/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "core/Prerequisite.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/stage/verifier/visitor/StaticTestVerificationStageVisitor.h"
#include "language/context/LogInfoContext.h"
#include "../ASTNodeSamples.h"
#include <iostream>
#include <string>
#include <limits>

#define BOOST_TEST_MODULE ThorTreeTest_StaticTestVerificationStageVisitorTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

using namespace zillians;
using namespace zillians::language::tree;
using namespace zillians::language::tree::visitor;

BOOST_AUTO_TEST_SUITE( ThorTreeTest_StaticTestVerificationStageVisitorTestTestSuite )

Tangle* createPassSample()
{
    Tangle* tangle = new Tangle();
    {
        Package* com_package = new Package(new SimpleIdentifier(L"com"));
        tangle->root->addPackage(com_package);
        {
            Package* zillians_package = new Package(new SimpleIdentifier(L"zillians"));
            com_package->addPackage(zillians_package);
            {
                Source* source = new Source("pass-source");
                zillians_package->addSource(source);
                {
                    ClassDecl* class_decl = new ClassDecl(new SimpleIdentifier(L"some_class"));
                    source->addDeclare(class_decl);
                    {
                        FunctionDecl* some_member_function = new FunctionDecl(
                                new SimpleIdentifier(L"some_member_function"),
                                NULL,
                                true, false, false, false,
                                Declaration::VisibilitySpecifier::PUBLIC,
                                new NormalBlock());
                        class_decl->addFunction(some_member_function);
                        {
                            Block* block = some_member_function->block;

                            {
                                DeclarativeStmt* stmt = new DeclarativeStmt(new VariableDecl(new SimpleIdentifier(L"a"), new TypeSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT));
                                block->appendObject(stmt);
                            }

                            {
                                DeclarativeStmt* stmt = new DeclarativeStmt(new VariableDecl(new SimpleIdentifier(L"b"), new TypeSpecifier(PrimitiveType::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT));
                                block->appendObject(stmt);
                            }

                            {
                                // set error check annotation data
                                Annotation* msgParams = new Annotation(NULL);
                                msgParams->appendKeyValue(new SimpleIdentifier(L"id"), new PrimaryExpr(new StringLiteral(L"mCount")));
                                msgParams->appendKeyValue(new SimpleIdentifier(L"type"), new PrimaryExpr(new StringLiteral(L"int")));
                                Annotation* levelIdParams = new Annotation(NULL);
                                levelIdParams->appendKeyValue(new SimpleIdentifier(L"level"), new PrimaryExpr(new StringLiteral(L"LEVEL_WARNING")));
                                levelIdParams->appendKeyValue(new SimpleIdentifier(L"id"), new PrimaryExpr(new StringLiteral(L"EXAMPLE_UNDECLARED_VARIABLE")));
                                levelIdParams->appendKeyValue(new SimpleIdentifier(L"parameters"), msgParams);

                                Annotation* anno = new Annotation(new SimpleIdentifier(L"static_test"));
                                anno->appendKeyValue(new SimpleIdentifier(L"expect_message"), levelIdParams);
                                Annotations* annos = new Annotations();
                                annos->appendAnnotation(anno);

                                // set error message context data
                                std::map<std::wstring, std::wstring> m;
                                m[L"id"] = L"mCount";
                                m[L"type"] = L"int";
                                zillians::language::LogInfo errorContext(L"LEVEL_WARNING", L"EXAMPLE_UNDECLARED_VARIABLE", m);

                                ExpressionStmt* stmt = new ExpressionStmt(new BinaryExpr(BinaryExpr::OpCode::ASSIGN, new PrimaryExpr(new SimpleIdentifier(L"EXAMPLE_UNDECLARED_VARIABLE")), new PrimaryExpr(new SimpleIdentifier(L"b"))));
                                stmt->setAnnotations(annos);
                                zillians::language::LogInfoContext::push_back(stmt, errorContext);

                                block->appendObject(stmt);
                            }
                        }
                    }
                }
            }
        }
    }
    return tangle;
}

Tangle* createFailSample()
{
    Tangle* tangle = new Tangle();
    {
        Package* com_package = new Package(new SimpleIdentifier(L"com"));
        tangle->root->addPackage(com_package);
        {
            Package* zillians_package = new Package(new SimpleIdentifier(L"zillians"));
            com_package->addPackage(zillians_package);
            {
                Source* source = new Source("fail-source");
                zillians_package->addSource(source);
                {
                    ClassDecl* class_decl = new ClassDecl(new SimpleIdentifier(L"some_class"));
                    source->addDeclare(class_decl);
                    {
                        FunctionDecl* some_member_function = new FunctionDecl(
                                new SimpleIdentifier(L"some_member_function"),
                                NULL,
                                true, false, false, false,
                                Declaration::VisibilitySpecifier::PUBLIC,
                                new Block);
                        class_decl->addFunction(some_member_function);
                        {
                            Block* block = some_member_function->block;

                            {
                                DeclarativeStmt* stmt = new DeclarativeStmt(new VariableDecl(new SimpleIdentifier(L"a"), new TypeSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT));
                                block->appendObject(stmt);
                            }

                            {
                                DeclarativeStmt* stmt = new DeclarativeStmt(new VariableDecl(new SimpleIdentifier(L"b"), new TypeSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT));
                                block->appendObject(stmt);
                            }

                            {
                                // set error check annotation data
                                Annotation* msgParams = new Annotation(NULL);
                                msgParams->appendKeyValue(new SimpleIdentifier(L"id"), new PrimaryExpr(new StringLiteral(L"mCount")));
                                msgParams->appendKeyValue(new SimpleIdentifier(L"type"), new PrimaryExpr(new StringLiteral(L"int")));
                                msgParams->appendKeyValue(new SimpleIdentifier(L"extra_fail_key"), new PrimaryExpr(new StringLiteral(L"extra_fail_value")));
                                Annotation* levelIdParams = new Annotation(NULL);
                                levelIdParams->appendKeyValue(new SimpleIdentifier(L"level"), new PrimaryExpr(new StringLiteral(L"LEVEL_WARNING")));
                                levelIdParams->appendKeyValue(new SimpleIdentifier(L"id"), new PrimaryExpr(new StringLiteral(L"EXAMPLE_UNDECLARED_VARIABLE")));
                                levelIdParams->appendKeyValue(new SimpleIdentifier(L"parameters"), msgParams);

                                Annotation* anno = new Annotation(new SimpleIdentifier(L"static_test"));
                                anno->appendKeyValue(new SimpleIdentifier(L"expect_message"), levelIdParams);
                                Annotations* annos = new Annotations();
                                annos->appendAnnotation(anno);

                                // set error message context data
                                std::map<std::wstring, std::wstring> m;
                                m[L"id"] = L"mCount";
                                m[L"type"] = L"int";
                                zillians::language::LogInfo errorContext(L"LEVEL_WARNING", L"EXAMPLE_UNDECLARED_VARIABLE", m);

                                ExpressionStmt* stmt = new ExpressionStmt(new BinaryExpr(BinaryExpr::OpCode::ASSIGN, new PrimaryExpr(new SimpleIdentifier(L"EXAMPLE_UNDECLARED_VARIABLE")), new PrimaryExpr(new SimpleIdentifier(L"b"))));
                                stmt->setAnnotations(annos);
                                zillians::language::LogInfoContext::push_back(stmt, errorContext);

                                // set source info context
                                zillians::language::stage::SourceInfoContext::set(stmt, new zillians::language::stage::SourceInfoContext(32, 10) );

                                block->appendObject(stmt);
                            }
                        }
                    }
                }
            }
        }
    }
    return tangle;
}

BOOST_AUTO_TEST_CASE( ThorTreeTest_StaticTestVerificationStageVisitorTestCase1 )
{
    // prepare module info for debug purpose
    using namespace zillians::language;
    setParserContext(new ParserContext());

    zillians::language::stage::visitor::StaticTestVerificationStageVisitor checker;

    Tangle* okTangle = createPassSample();
    //checker.programNode = okProgram;
    getParserContext().active_source = okTangle->root->children[0]->children[0]->sources[0];
    checker.visit(*okTangle);
    BOOST_CHECK(checker.isAllMatch());

    Tangle* failTangle = createFailSample();
    //checker.programNode = failTangle;
    getParserContext().active_source = okTangle->root->children[0]->children[0]->sources[0];
    checker.visit(*failTangle);
    BOOST_CHECK(!checker.isAllMatch());
}

BOOST_AUTO_TEST_SUITE_END()
