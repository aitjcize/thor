/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/tree/statement/DeclarativeStmt.h"
#include "language/tree/statement/IterativeStmt.h"

namespace zillians { namespace language { namespace tree {

DeclarativeStmt::DeclarativeStmt() : declaration(NULL)
{ }

DeclarativeStmt::DeclarativeStmt(Declaration* declaration) : declaration(declaration)
{
    BOOST_ASSERT(declaration && "null declaration for declarative statement is not allowed");

    declaration->parent = this;
}

bool DeclarativeStmt::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (declaration)
    );
}

bool DeclarativeStmt::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (declaration)
    );
}

DeclarativeStmt* DeclarativeStmt::clone() const
{
    auto cloned = new DeclarativeStmt(declaration->clone());
    if(annotations) cloned->setAnnotations(annotations->clone());
    return cloned;
}

std::wostream& DeclarativeStmt::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(declaration && "null pointer exception");

    bool is_normal_stmt = true;

    if (const ForStmt* for_stmt = cast<ForStmt>(parent->parent))
    {
        if (parent == for_stmt->init ||
            parent == for_stmt->cond ||
            parent == for_stmt->step)
            is_normal_stmt = false;
    }

    if (is_normal_stmt)
        output << out_indent(indent);

    output << out_source(*declaration, indent);

    if (is_normal_stmt)
        output << L';';

    return output;
}

} } }
