/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/tree/declaration/TypedefDecl.h"

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Annotations.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/basic/DeclType.h"
#include "language/tree/declaration/Declaration.h"

namespace zillians { namespace language { namespace tree {

TypedefDecl::TypedefDecl(TypeSpecifier* f,
                         Identifier* t)
    : Declaration(t)
    , type(f)
    , typedef_type_(new TypedefType(this))
{
    BOOST_ASSERT(f && t && "null \"from node\" or \"to node\" for typedef is not allowed");

    type->parent = this;
}

bool TypedefDecl::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (type)
    );
}

bool TypedefDecl::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (type)
    );
}

TypedefType* TypedefDecl::getType() const
{
    return typedef_type_;
}

std::wstring TypedefDecl::toString() const
{
    return L"typedef " + Declaration::toString();
}

TypedefDecl* TypedefDecl::clone() const
{
    TypedefDecl* cloned = new TypedefDecl(
            (type) ? type->clone() : NULL,
            (name) ? name->clone() : NULL);

    if(annotations != NULL)
    {
        Annotations* anno = annotations->clone();
        cloned->setAnnotations(anno);
    }

    cloned->arch         = arch;
    cloned->may_conflict = may_conflict;

    return cloned;
}

Type* TypedefDecl::getCanonicalType() const
{
    return this->type->getCanonicalType();
}

std::wostream& TypedefDecl::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(type && "null pointer exception");
    BOOST_ASSERT(name && "null pointer exception");

    BaseNode::toSource(output, indent);

    output << out_indent(indent) << L"typedef " << out_source(*type, indent) << L' ' << out_source(*name, indent) << L';';

    return output;
}

} } }
