/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cstdint>

#include <algorithm>
#include <set>
#include <utility>
#include <vector>

#include <boost/algorithm/cxx11/all_of.hpp>
#include <boost/assert.hpp>
#include <boost/next_prior.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/join.hpp>

#include "utility/Functional.h"

#include "language/resolver/VirtualTable.h"

#include "language/tree/basic/Annotations.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/basic/DeclType.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/declaration/TypedefDecl.h"

namespace zillians { namespace language { namespace tree {

namespace {

ClassDecl* getClassDeclByNamedSpecifier(const NamedSpecifier*const ts)
{
    BOOST_ASSERT(ts != nullptr && "null pointer exception");

    const Type*const type = ts->getCanonicalType();

    if (type == nullptr)
        return nullptr;

    auto*const base_cls_decl = type->getAsClassDecl();

    return base_cls_decl;
}

}

ClassDecl::ClassDecl(Identifier* name,
                     bool is_interface)
    : Declaration(name)
    , base(NULL)
    , is_interface(is_interface)
    , is_base_vtables_ready(false)
    , vtable()
    , record_type_(new RecordType(this))
{
    BOOST_ASSERT(name && "null class name identifier is not allowed");
}

void ClassDecl::addFunction(FunctionDecl* func)
{
    BOOST_ASSERT(name && "null member function declaration is not allowed");
    if(is_interface)
    {
        BOOST_ASSERT(!func->block && "interface method should not have function body");
    }

    func->parent = this;
    func->is_member = true;
    member_functions.push_back(func);

    if(symbol_table.is_clean())
        symbol_table.insert(func);
}

void ClassDecl::insertFunction(std::vector<FunctionDecl*>::iterator it, FunctionDecl* func)
{
    BOOST_ASSERT(name && "null member function declaration is not allowed");
    if(is_interface)
    {
        BOOST_ASSERT(!func->block && "interface method should not have function body");
    }

    func->parent = this;
    func->is_member = true;
    member_functions.insert(it, func);

    if(symbol_table.is_clean())
        symbol_table.insert(func);
}

void ClassDecl::addVariable(VariableDecl* var)
{
    BOOST_ASSERT(name && "null member variable declaration is not allowed");
    BOOST_ASSERT(!is_interface && "interface should not have member variable");

    var->parent = this;
    var->is_member = true;
    member_variables.push_back(var);

    if(symbol_table.is_clean())
    {
        symbol_table.insert(var);
    }
}

RecordType* ClassDecl::getType() const
{
    return record_type_;
}

RecordType* ClassDecl::getCanonicalType() const
{
    return record_type_;
}

void ClassDecl::setBase(NamedSpecifier* extends_from)
{
    BOOST_ASSERT(extends_from && "null base class is not allowed");
    BOOST_ASSERT(!symbol_table.is_clean() && "you cannot modify class hierarchy after symbol table generation!");

    extends_from->parent = this;

    if(base)
        base->parent = NULL;

    base = extends_from;
}

void ClassDecl::addInterface(NamedSpecifier* interface_)
{
    BOOST_ASSERT(interface_ && "null interface is not allowed");
    BOOST_ASSERT(!symbol_table.is_clean() && "you cannot modify class hierarchy after symbol table generation!");

    interface_->parent = this;
    implements.push_back(interface_);
}

auto ClassDecl::getBaseClassDecls() const -> BasesRange
{
    using boost::adaptors::transformed;

    if (base != nullptr)
        return    boost::range::join(
                      boost::make_iterator_range(&base, boost::next(&base)),
                      implements
                  )
                | transformed(&getClassDeclByNamedSpecifier);
    else
        return    implements
                | transformed(&getClassDeclByNamedSpecifier);
}

bool ClassDecl::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (base            )
        (implements      )
        (member_functions)
        (member_variables)
        (is_interface    )
    );
}

bool ClassDecl::isCompleted() const
{
    if(auto template_name = cast<TemplatedIdentifier>(name))
        return template_name->isFullySpecialized();
    else
        return true;
}

bool ClassDecl::isTemplated() const
{
    return isa<TemplatedIdentifier>(name);
}

bool ClassDecl::isReplicable() const
{
    return  !is_interface &&
            hasAnnotation(L"replicable");
}

bool ClassDecl::hasDefaultConstructor() const
{
    return std::any_of(
        member_functions.begin(),
        member_functions.end(),
        [](FunctionDecl* func_decl)
        {
            BOOST_ASSERT(func_decl && "null pointer exception");

            return func_decl->isDefaultConstructor();
        }
    );
}

bool ClassDecl::hasVirtual() const
{
    return vtable.has_virtual();
}

std::pair<std::uint32_t, bool> ClassDecl::getVirtualIndex(const tree::FunctionDecl& member_function) const
{
    return vtable.get_virtual_index(member_function);
}

bool ClassDecl::hasPureVirtual() const
{
    return vtable.has_pure_virtual();
}

std::set<const FunctionDecl*> ClassDecl::getPureVirtuals() const
{
    return vtable.get_pure_virtuals();
}

std::vector<virtual_table::segment_info_type> ClassDecl::generateVirtualTableInfo() const
{
    return vtable.generate_info();
}

bool ClassDecl::isVirtualTableReady() const noexcept
{
    return is_base_vtables_ready && vtable.is_ready();
}

bool ClassDecl::buildVirtualTable()
{
    if (isVirtualTableReady())
        return true;

    std::set<const ClassDecl*> visited;

    return buildVirtualTableImpl(visited);
}

bool ClassDecl::buildVirtualTableImpl(std::set<const ClassDecl*>& visited)
{
    using boost::adaptors::transformed;

    const auto& base_cls_decls = getBaseClassDecls();

    if (!is_base_vtables_ready)
    {
        if (!visited.insert(this).second)
            return false;

        for (auto*const base_cls_decl : base_cls_decls)
        {
            if (base_cls_decl == nullptr)
                return false;

            if (!base_cls_decl->buildVirtualTableImpl(visited))
                return false;
        }

        is_base_vtables_ready = true;
    }

    BOOST_ASSERT(boost::algorithm::all_of(base_cls_decls, not_null()) && "null pointer exception");

    std::vector<const virtual_table*> base_vtables;

    for (const auto*const base_cls_decl : base_cls_decls)
        base_vtables.emplace_back(&base_cls_decl->vtable);

    return vtable.build(*this, base_vtables, member_functions);
}

bool ClassDecl::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (base            )
        (implements      )
        (member_functions)
        (member_variables)
    );
}

std::wstring ClassDecl::toString() const
{
    return L"class " + Declaration::toString();
}

ClassDecl* ClassDecl::clone() const
{
    ClassDecl* cloned = new ClassDecl((name ? name->clone() : NULL), is_interface);

    if(annotations != NULL)
    {
        Annotations* anno = annotations->clone();
        cloned->setAnnotations(anno);
    }

    cloned->arch         = arch;
    cloned->may_conflict = may_conflict;

    if(base) cloned->setBase(base->clone());

    for(auto* impl : implements)
        cloned->addInterface(clone_or_null(impl));

    for(auto* method : member_functions)
        cloned->addFunction(clone_or_null(method));

    for(auto* attribute : member_variables)
        cloned->addVariable(clone_or_null(attribute));

    return cloned;
}

std::wostream& ClassDecl::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BaseNode::toSource(output, indent);

    BOOST_ASSERT(name && "null pointer exception");

    output << out_indent(indent) << (is_interface ? L"interface " : L"class ") << out_source(*name, indent);

    if (base != nullptr)
        output << std::endl << out_indent(indent + 1) << L"extends " << out_source(*base, indent);

    if (!implements.empty())
    {
              auto i    = implements.cbegin();
        const auto iend = implements.cend();

        output << std::endl << out_indent(indent + 1) << L"implements " << out_source(**i, indent);

        while (++i != iend)
            output << L", " << out_source(**i, indent);
    }

    output << std::endl;
    output << out_indent(indent) << '{';

    for (FunctionDecl* member_function : member_functions)
        output << std::endl << out_indent(indent + 1) << out_source(*member_function, indent + 1) << std::endl;

    for (VariableDecl* member_variable : member_variables)
        output << std::endl << out_indent(indent + 1) << out_source(*member_variable, indent + 1) << L';' << std::endl;

    output << out_indent(indent) << L'}';

    return output;
}

} } }
