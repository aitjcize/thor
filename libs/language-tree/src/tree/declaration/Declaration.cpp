/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Block.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/module/Source.h"

namespace zillians { namespace language { namespace tree {

Declaration::Declaration()
    : may_conflict(false)
    , name(nullptr)
    , resolved_type(nullptr)
    , arch()
{
}

Declaration::Declaration(Identifier* name)
    : may_conflict(false)
    , name(name)
    , resolved_type(nullptr)
    , arch()
{
    if (name != nullptr)
        name->parent = this;
}

bool Declaration::isGlobal() const
{
    return isa<Source>(parent);
}

bool Declaration::mayConflict() const
{
    return may_conflict;
}

bool Declaration::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (name       )
    );
}

bool Declaration::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (name       )
    );
}
const Identifier* Declaration::getName() const noexcept
{
    return name;
}

const unique_name_t& Declaration::getUniqueName() const noexcept
{
    return unique_name;
}

Architecture Declaration::getActualArch() const noexcept
{
    if (arch.is_not_zero())
        return arch;

    for (const ASTNode* current = parent; current != nullptr; current = current->parent)
    {
        if (const auto* block = cast<Block      >(current)) return block->getActualArch();
        if (const auto* decl  = cast<Declaration>(current)) return decl ->getActualArch();
    }

    return Architecture::default_;
}

std::wstring Declaration::toString() const
{
    return name->toString();
}

std::wostream& Declaration::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BaseNode::toSource(output, indent);

    if (!arch.is_zero())
        output << out_arch(0, arch) << out_indent(indent);

    return output;
}

} } }
