/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/tree/module/Source.h"

#include "language/tree/ASTNode.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/module/Import.h"
#include "language/tree/module/Package.h"
#include "utility/UnicodeUtil.h"

namespace zillians { namespace language { namespace tree {

Source::Source() : is_imported(false) {}

Source::Source(const std::string& filename, bool is_imported/* = false*/) : is_imported(is_imported), filename(filename) {}

bool Source::isImported() const
{
    return is_imported;
}

void Source::addImport(Import* import)
{
    BOOST_ASSERT(import && "null import is not allowed");

    import->parent = this;
    imports.push_back(import);
}

void Source::addDeclare(Declaration* declare)
{
    BOOST_ASSERT(declare && "null declaration is not allowed");

    declare->parent = this;
    declares.push_back(declare);

    // Update symbol table
    if(Package* package = getOwner<Package>())
    {
        if(package->symbol_table.is_clean())
        {
            package->symbol_table.insert(declare);
        }
    }
}

bool Source::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (filename)
        (imports )
        (declares)
    );
}

bool Source::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (imports )
        (declares)
    );
}

std::wostream& Source::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << out_indent(indent) << L"// Source : " << s_to_ws(filename);

    if (!imports.empty())
    {
        for (const Import* import : imports)
            output << std::endl << out_indent(indent) << out_source(*import, indent);

        output << std::endl;
    }

    for (const Declaration* decl : declares)
        output << std::endl  << out_indent(indent) << out_source(*decl, indent) << std::endl;

    return output;
}

Source* Source::clone() const
{
    Source* cloned = new Source(filename, is_imported);

    for(auto* to_import : imports)
        cloned->addImport(to_import->clone());

    for(auto* decl : declares)
        cloned->addDeclare(decl->clone());

    return cloned;
}

} } }
