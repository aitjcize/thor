/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <ostream>

#include <boost/assert.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/module/Import.h"

namespace zillians { namespace language { namespace tree {

Import::Import() {}

Import::Import(Identifier* ns) : alias(nullptr), ns(ns)
{
    BOOST_ASSERT(ns && "null identifier for import node is not allowed");

    ns->parent = this;
}

Import::Import(Identifier* alias, Identifier* ns) : alias(alias), ns(ns)
{
    BOOST_ASSERT(alias && "null identifier for import node is not allowed");
    BOOST_ASSERT(ns && "null identifier for import node is not allowed");

    alias->parent = this;
    ns->parent = this;
}

bool Import::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (alias)
        (ns   )
    );
}

bool Import::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (alias)
        (ns   )
    );
}

Import* Import::clone() const
{
    if(alias)
        return new Import(alias->clone(), ns->clone());
    else
        return new Import(ns->clone());
}

std::wostream& Import::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << out_indent(indent) << L"import ";

    if (alias != nullptr)
    {
        if (alias->toString().empty())
            output << L'.';
        else
            output << out_source(*alias, indent);

        output << L" = ";
    }

    output << out_source(*ns, indent) << L';';

    return output;
}

} } }
