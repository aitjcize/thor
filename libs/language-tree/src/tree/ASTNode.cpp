/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <fstream>
#include <sstream>
#include <string>

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

#include "language/Architecture.h"
#include "language/tree/ASTNode.h"

namespace zillians { namespace language { namespace tree {

bool ASTNode::toSource(const boost::filesystem::path& file_path) const
{
    if (file_path.has_parent_path())
        boost::filesystem::create_directories(file_path.parent_path());

    std::wofstream output(file_path.string());

    if (!output.is_open())
    {
        std::cerr << "Can not open file" << file_path.string() << std::endl;

        return false;
    }

    toSource(output);

    return output.good();
}

std::wstring ASTNode::toSource(unsigned indent/* = 0*/) const
{
    std::wostringstream stream;

    toSource(stream, indent);

    return stream.str();
}

ASTNode::~ASTNode() = default;

output_indent_t out_indent(const unsigned indent)
{
    return output_indent_t{indent};
}

output_arch_t out_arch(const unsigned indent, const Architecture arch)
{
    return output_arch_t
    {
        output_indent_t{indent},
        arch
    };
}

output_source_t out_source(const ASTNode& node, const unsigned indent)
{
    return output_source_t{&node, indent};
}

std::wostream& operator<<(std::wostream& output, output_indent_t indent)
{
    for (unsigned i = 0; i < indent.value; ++i)
        output << L"    ";

    return output;
}

std::wostream& operator<<(std::wostream& output, output_arch_t arch)
{
    if (!arch.arch.is_zero())
        output << L"@arch=\"" << arch.arch.toString(L",") << L'"' << std::endl;

    return output;
}

std::wostream& operator<<(std::wostream& output, output_source_t source)
{
    BOOST_ASSERT(source.node && "null pointer exception");

    return source.node->toSource(output, source.indent);
}

} } }
