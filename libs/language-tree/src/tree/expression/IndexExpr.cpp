/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <sstream>
#include <string>

#include <boost/assert.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/expression/IndexExpr.h"

namespace zillians { namespace language { namespace tree {

IndexExpr::IndexExpr()
    : array(nullptr)
    , index(nullptr)
{
}

IndexExpr::IndexExpr(Expression* array, Expression* index)
    : array(array)
    , index(index)
{
    BOOST_ASSERT(array && "null pointer exception");
    BOOST_ASSERT(index && "null pointer exception");

    array->parent = this;
    index->parent = this;
}

bool IndexExpr::hasValue() const
{
    return true;
}

bool IndexExpr::isRValue() const
{
    return false;
}

bool IndexExpr::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (array)
        (index)
    );
}

bool IndexExpr::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (array)
        (index)
    );
}

IndexExpr* IndexExpr::clone() const
{
    BOOST_ASSERT(array && "null pointer exception");
    BOOST_ASSERT(index && "null pointer exception");

    return new IndexExpr(
        array->clone(),
        index->clone()
    );
}

std::wostream& IndexExpr::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(array && "null pointer exception");
    BOOST_ASSERT(index && "null pointer exception");

    output << L'(' << out_source(*array, indent) << L")[" << out_source(*index, indent) << L']';

    return output;
}

} } }
