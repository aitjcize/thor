/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <initializer_list>
#include <ostream>

#include <boost/assert.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/expression/CallExpr.h"

namespace zillians { namespace language { namespace tree {

CallExpr::CallExpr() : node(nullptr) { }

CallExpr::CallExpr(Expression* node, std::initializer_list<Expression*> parameters/* = {}*/) : node(node)
{
    BOOST_ASSERT(node && "null callee for call expression is not allowed");

    node->parent = this;

    for (Expression* parameter : parameters)
        appendParameter(parameter);
}

void CallExpr::appendParameter(Expression* parameter)
{
    BOOST_ASSERT(parameter && "null parameter for call expression is not allowed");

    parameter->parent = this;
    parameters.push_back(parameter);
}

void CallExpr::prependParameter(Expression* parameter)
{
    BOOST_ASSERT(parameter && "null parameter for call expression is not allowed");

    parameter->parent = this;
    parameters.insert(parameters.begin(), parameter);
}

bool CallExpr::hasValue() const
{
    return true;
}

bool CallExpr::isRValue() const
{
    return true;
}

bool CallExpr::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (node      )
        (parameters)
    );
}

bool CallExpr::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (node      )
        (parameters)
    );
}

CallExpr* CallExpr::clone() const
{
    CallExpr* cloned = new CallExpr((node) ? node->clone() : NULL);

    for(auto const* param : parameters)
        cloned->appendParameter(clone_or_null(param));

    return cloned;
}

std::wostream& CallExpr::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(node && "null pointer exception");

    output << out_source(*node, indent) << L'(';

    if (!parameters.empty())
    {
              auto i    = parameters.cbegin();
        const auto iend = parameters.cend();

        output << out_source(**i, indent);

        while (++i != iend)
            output << L", " << out_source(**i, indent);
    }

    output << ')';

    return output;
}

} } }
