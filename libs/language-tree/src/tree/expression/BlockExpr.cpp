/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <ostream>

#include <boost/assert.hpp>

#include "utility/UnicodeUtil.h"

#include "language/tree/basic/Block.h"
#include "language/tree/expression/BlockExpr.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace tree {

BlockExpr::BlockExpr() : block(NULL) { }

BlockExpr::BlockExpr(Block* b, const std::string& tag/* = ""*/) : block(b), tag(tag)
{
    if(block)
        block->parent = this;
}

bool BlockExpr::hasValue() const
{
    return true;
}

bool BlockExpr::isRValue() const
{
    return true;
}

bool BlockExpr::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (block)
    );
}

bool BlockExpr::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (block)
    );
}

BlockExpr* BlockExpr::clone() const
{
    BlockExpr* cloned = new BlockExpr(clone_or_null(block), tag);
    return cloned;
}

std::wostream& BlockExpr::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(block && "null pointer exception");

    output << L"// block-tag : " << s_to_ws(tag) << std::endl << out_source(*block, indent + 1) << std::endl << out_indent(indent);

    return output;
}

} } }
