/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <ostream>

#include <boost/assert.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/expression/StringizeExpr.h"

namespace zillians { namespace language { namespace tree {

StringizeExpr::StringizeExpr() : node(nullptr) { }

StringizeExpr::StringizeExpr(Expression* node) : node(node)
{
    BOOST_ASSERT(node && "null node for stringize expression is not allowed");

    node->parent = this;
}

bool StringizeExpr::hasValue() const
{
    BOOST_ASSERT(node && "null node for stringize expression is not allowed");

    return node->hasValue();
}

bool StringizeExpr::isRValue() const
{
    BOOST_ASSERT(node && "null node for stringize expression is not allowed");

    return node->isRValue();
}

bool StringizeExpr::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (node      )
    );
}

bool StringizeExpr::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (node      )
    );
}

StringizeExpr* StringizeExpr::clone() const
{
    return new StringizeExpr(
        clone_or_null(node)
    );
}

std::wostream& StringizeExpr::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(node && "null node for stringize expression is not allowed");

    output << L"<stringize-expr>(" << out_source(*node, indent) << L')';

    return output;
}

} } }

