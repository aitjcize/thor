/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <initializer_list>

#include "language/tree/basic/Block.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/statement/IterativeStmt.h"

namespace zillians { namespace language { namespace tree {

Block::Block(Architecture arch /*= Architecture::zero()*/)
    : arch(arch)
    , is_implicit(false)
    , objects()
{
}

Block::Block(std::initializer_list<Statement*> objects, Architecture arch/* = Architecture::zero()*/)
    : Block(arch)
{
    appendObjects(objects);
}

bool Block::isImplicit() const noexcept
{
    return is_implicit;
}

void Block::setImplicit(bool new_is_implicit) noexcept
{
    is_implicit = new_is_implicit;
}

void Block::prependObject(Statement* object)
{
    object->parent = this;
    objects.push_front(object);
}

void Block::appendObject(Statement* object)
{
    object->parent = this;
    objects.push_back(object);
}

Statement* Block::insertObjectBefore(Statement* before, Statement* object, bool replace_before)
{
    BOOST_ASSERT(before != nullptr && "no statement specifies the position");

    auto it = std::find(objects.begin(), objects.end(), before);
    if(it != objects.end())
    {
        object->parent = this;
        objects.insert(it, object);

        if(replace_before)
            objects.erase(it);

        return object;
    }
    else
    {
        return nullptr;
    }
}

Statement* Block::insertObjectAfter(Statement* after, Statement* object, bool replace_after)
{
    BOOST_ASSERT(after != nullptr && "no statement specifies the position");

    auto it = std::find(objects.begin(), objects.end(), after);
    if(it != objects.end())
    {
        ++it;
        object->parent = this;
        objects.insert(it, object);

        if(replace_after)
            objects.erase(it);

        return object;
    }
    else
    {
        return nullptr;
    }
}

void Block::prependObjects(std::initializer_list<Statement*> object_list)
{
    prependObjects<decltype(object_list)>(object_list);
}

void Block::appendObjects(std::initializer_list<Statement*> object_list)
{
    appendObjects<decltype(object_list)>(object_list);
}

bool Block::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (objects)
    );
}

bool Block::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (objects)
    );
}

Architecture Block::getActualArch() const noexcept
{
    if (arch.is_not_zero())
        return arch;

    for (const ASTNode* current = parent; current != nullptr; current = current->parent)
    {
        if (const auto* block = cast<Block      >(current)) return block->getActualArch();
        if (const auto* decl  = cast<Declaration>(current)) return decl ->getActualArch();
    }

    return Architecture::default_;
}

std::wostream& Block::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << L'{' << std::endl;

    for (Statement* object : objects)
    {
        BOOST_ASSERT(object && "null pointer exception");

        output << out_source(*object, indent + 1) << std::endl;
    }

    output << out_indent(indent) << L'}';

    return output;
}

bool Block::merge(Block& rhs)
{
    for(auto& object : rhs.objects)
    {
        objects.push_back( object );
    }

    return true;
}

NormalBlock::NormalBlock(Architecture arch/* = Architecture::zero()*/)
    : Block(arch)
{
}

NormalBlock::NormalBlock(std::initializer_list<Statement*> objects, Architecture arch/* = Architecture::zero()*/)
    : Block(objects, arch)
{
}

std::wostream& NormalBlock::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << out_arch(indent, arch) << out_indent(indent);

    return BaseNode::toSource(output, indent);
}

NormalBlock* NormalBlock::clone() const
{
    NormalBlock* cloned = new NormalBlock(arch);

    cloned->is_implicit = is_implicit;

    for (Statement* object: objects)
    {
        BOOST_ASSERT(object && "null pointer exception");

        cloned->appendObject(object->clone());
    }

    return cloned;
}

AsyncBlock::AsyncBlock(Architecture arch /*= Architecture::zero()*/, Expression* target /*= nullptr*/, Expression* block /*= nullptr*/, Expression* grid /*= nullptr*/)
    : AsyncBlock({}, arch, target, block, grid)
{
}

AsyncBlock::AsyncBlock(std::initializer_list<Statement*> objects, Architecture arch /*= Architecture::zero()*/, Expression* target /*= nullptr*/, Expression* block /*= nullptr*/, Expression* grid /*= nullptr*/)
    : Block(objects, arch)
    , target(target)
    , block(block)
    , grid(grid)
{
    if (target != nullptr) target->parent = this;
    if (block  != nullptr) block ->parent = this;
    if (grid   != nullptr) grid  ->parent = this;
}

bool AsyncBlock::hasTarget() const noexcept
{
    return target != nullptr;
}

bool AsyncBlock::hasBlock() const noexcept
{
    return block != nullptr;
}

bool AsyncBlock::hasGrid() const noexcept
{
    return grid != nullptr;
}

Expression* AsyncBlock::getTarget() const noexcept
{
    return target;
}

Expression* AsyncBlock::getBlock() const noexcept
{
    return block;
}

Expression* AsyncBlock::getGrid() const noexcept
{
    return grid;
}

bool AsyncBlock::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (target)
        (block )
        (grid  )
    );
}

bool AsyncBlock::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (target)
        (block )
        (grid  )
    );
}

AsyncBlock* AsyncBlock::clone() const
{
    AsyncBlock* cloned = new AsyncBlock(
        arch,
        clone_or_null(target),
        clone_or_null(block ),
        clone_or_null(grid  )
    );

    cloned->is_implicit = is_implicit;

    for (Statement* object: objects)
    {
        BOOST_ASSERT(object && "null pointer exception");

        cloned->appendObject(object->clone());
    }

    return cloned;
}

std::wostream& AsyncBlock::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << out_arch(indent, arch) << out_indent(indent);

    BOOST_ASSERT(
        (
            (target != nullptr && block != nullptr && grid != nullptr) ||
            (target != nullptr && block == nullptr && grid == nullptr) ||
            (target == nullptr && block != nullptr && grid != nullptr) ||
            (target == nullptr && block == nullptr && grid == nullptr)
        ) && "ill-formed AsyncBlock!"
    );

    if (target != nullptr && block != nullptr && grid != nullptr)
    {
        output
            << L"async["
            << out_source(*target, indent) << L", "
            << out_source(*block , indent) << L", "
            << out_source(*grid  , indent) << L']';
    }
    else if (target != nullptr && block == nullptr && grid == nullptr)
    {
        output << L"async[" << out_source(*target, indent) << L']';
    }
    else if (target == nullptr && block != nullptr && grid != nullptr)
    {
        output
            << L"async["
            << out_source(*block, indent) << L", "
            << out_source(*grid , indent) << L']';
    }
    else
    {
        output << L"async";
    }

    output << L" -> ";

    return BaseNode::toSource(output, indent);
}

AtomicBlock::AtomicBlock(Architecture arch /*= Architecture::zero()*/, int stm_policy /*= 0*/)
    : AtomicBlock({}, arch, stm_policy)
{
}

AtomicBlock::AtomicBlock(std::initializer_list<Statement*> objects, Architecture arch /*= Architecture::zero()*/, int stm_policy /*= 0*/)
    : Block(objects, arch)
    , stm_policy(stm_policy)
{
}

bool AtomicBlock::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (stm_policy)
    );
}

bool AtomicBlock::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (stm_policy)
    );
}

AtomicBlock* AtomicBlock::clone() const
{
    AtomicBlock* cloned = new AtomicBlock(
        arch,
        stm_policy
    );

    cloned->is_implicit = is_implicit;

    for (Statement* object: objects)
    {
        BOOST_ASSERT(object && "null pointer exception");

        cloned->appendObject(object->clone());
    }

    return cloned;
}

std::wostream& AtomicBlock::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << out_indent(indent) << L"atomic(" << stm_policy << L") -> ";

    return BaseNode::toSource(output, indent);
}

FlowBlock::FlowBlock(Architecture arch /*= Architecture::zero()*/)
    : FlowBlock({}, arch)
{
}

FlowBlock::FlowBlock(std::initializer_list<Statement*> objects, Architecture arch /*= Architecture::zero()*/)
    : Block(objects, arch)
{
}

bool FlowBlock::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
    );
}

bool FlowBlock::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
    );
}

FlowBlock* FlowBlock::clone() const
{
    FlowBlock* cloned = new FlowBlock(
        arch
    );

    cloned->is_implicit = is_implicit;

    for (Statement* object: objects)
    {
        BOOST_ASSERT(object && "null pointer exception");

        cloned->appendObject(object->clone());
    }

    return cloned;
}

std::wostream& FlowBlock::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << out_indent(indent) << L"flow -> ";

    return BaseNode::toSource(output, indent);
}

LockBlock::LockBlock(Architecture arch /*= Architecture::zero()*/, bool rw_lock_or_simple_mutex /*= true*/)
    : LockBlock({}, arch, rw_lock_or_simple_mutex)
{
}

LockBlock::LockBlock(std::initializer_list<Statement*> objects, Architecture arch /*= Architecture::zero()*/, bool rw_lock_or_simple_mutex /*= true*/)
    : Block(objects, arch)
    , rw_lock_or_simple_mutex(rw_lock_or_simple_mutex)
{
}

bool LockBlock::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (rw_lock_or_simple_mutex)
    );
}

bool LockBlock::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (rw_lock_or_simple_mutex)
    );
}

LockBlock* LockBlock::clone() const
{
    LockBlock* cloned = new LockBlock(
        arch,
        rw_lock_or_simple_mutex
    );

    cloned->is_implicit = is_implicit;

    for (Statement* object: objects)
    {
        BOOST_ASSERT(object && "null pointer exception");

        cloned->appendObject(object->clone());
    }

    return cloned;
}

std::wostream& LockBlock::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << out_indent(indent) << L"lock(" << std::boolalpha << rw_lock_or_simple_mutex << L") -> ";

    return BaseNode::toSource(output, indent);
}

PipelineBlock::PipelineBlock(Architecture arch /*= Architecture::zero()*/, bool async_or_sync /*= true*/)
    : PipelineBlock({}, arch, async_or_sync)
{
}

PipelineBlock::PipelineBlock(std::initializer_list<Statement*> objects, Architecture arch /*= Architecture::zero()*/, bool async_or_sync /*= true*/)
    : Block(objects, arch)
    , async_or_sync(true)
{
}

bool PipelineBlock::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (async_or_sync)
    );
}

bool PipelineBlock::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (async_or_sync)
    );
}

PipelineBlock* PipelineBlock::clone() const
{
    PipelineBlock* cloned = new PipelineBlock(
        arch,
        async_or_sync
    );

    cloned->is_implicit = is_implicit;

    for (Statement* object: objects)
    {
        BOOST_ASSERT(object && "null pointer exception");

        cloned->appendObject(object->clone());
    }

    return cloned;
}

std::wostream& PipelineBlock::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << out_indent(indent) << L"pipeline(" << std::boolalpha << async_or_sync << L") -> ";

    return BaseNode::toSource(output, indent);
}

} } }
