/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <ostream>
#include <sstream>
#include <string>

#include <boost/assert.hpp>
#include <boost/regex.hpp>

#include "core/Types.h"

#include "language/context/ResolverContext.h"
#include "language/tree/ASTNode.h"
#include "language/tree/basic/Literal.h"
#include "language/tree/basic/PrimitiveType.h"
#include "language/tree/module/Tangle.h"

namespace zillians { namespace language { namespace tree {

bool Literal::isRValue() const
{
    return true;
}

const wchar_t* ObjectLiteral::LiteralType::toString(type t)
{
    switch(t)
    {
    case NULL_OBJECT: return L"null";
//    case SELF_OBJECT: return L"self";
    case THIS_OBJECT: return L"this";
    case SUPER_OBJECT: return L"super";
    default : UNREACHABLE_CODE(); return L"<unknown>";
    }
}

ObjectLiteral::ObjectLiteral() {}

ObjectLiteral::ObjectLiteral(LiteralType::type t) : type(t) {}

bool ObjectLiteral::isNullLiteral() const
{
    return type == LiteralType::NULL_OBJECT;
}

bool ObjectLiteral::isThisLiteral() const
{
    return type == LiteralType::THIS_OBJECT;
}

bool ObjectLiteral::isSuperLiteral() const
{
    return type == LiteralType::SUPER_OBJECT;
}

bool ObjectLiteral::hasValue() const
{
    switch(type)
    {
    case LiteralType::THIS_OBJECT:
    case LiteralType::NULL_OBJECT:
        return true;
    default:
        return false;
    }
}

Type* ObjectLiteral::getCanonicalType() const
{
    Type* resolved = ResolvedType::get(this);
    if(resolved == NULL) return NULL;

    if(Declaration* decl = resolved->getAsDecl())
    {
        return decl->getCanonicalType();
    }
    else
    {
        return resolved->getAsPrimitiveType();
    }
}

bool ObjectLiteral::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (type)
    );
}

bool ObjectLiteral::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    UNUSED_ARGUMENT(from);
    UNUSED_ARGUMENT(to);
    UNUSED_ARGUMENT(update_parent);

    return false;
}

ObjectLiteral* ObjectLiteral::clone() const
{
    return new ObjectLiteral(type);
}

std::wostream& ObjectLiteral::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    return output << LiteralType::toString(type);
}

std::wstring ObjectLiteral::toString() const
{
    return LiteralType::toString(type);
}

NumericLiteral::NumericLiteral() { }
NumericLiteral::NumericLiteral(bool   v) { primitive_kind.setKindValue(PrimitiveKind::BOOL_TYPE   ); value.b   = v;  }
NumericLiteral::NumericLiteral(int8   v) { primitive_kind.setKindValue(PrimitiveKind::INT8_TYPE   ); value.i8  = v;  }
NumericLiteral::NumericLiteral(int16  v) { primitive_kind.setKindValue(PrimitiveKind::INT16_TYPE  ); value.i16 = v; }
NumericLiteral::NumericLiteral(int32  v) { primitive_kind.setKindValue(PrimitiveKind::INT32_TYPE  ); value.i32 = v; }
NumericLiteral::NumericLiteral(int64  v) { primitive_kind.setKindValue(PrimitiveKind::INT64_TYPE  ); value.i64 = v; }
NumericLiteral::NumericLiteral(float  v) { primitive_kind.setKindValue(PrimitiveKind::FLOAT32_TYPE); value.f32 = v; }
NumericLiteral::NumericLiteral(double v) { primitive_kind.setKindValue(PrimitiveKind::FLOAT64_TYPE); value.f64 = v; }

size_t NumericLiteral::byteSize() const
{
    return primitive_kind.byteSize();
}

bool NumericLiteral::hasValue() const
{
    return true;
}

Type* NumericLiteral::getCanonicalType() const
{
    Tangle* tangle = this->getOwner<Tangle>();
    BOOST_ASSERT(tangle && "no tangle node found");

    return tangle->internal->getPrimitiveType(this->primitive_kind);
}

bool NumericLiteral::isEqualImpl(const ASTNode& rhs) const
{
    if (!BaseNode::isEqualImpl(rhs))
        return false;

    const auto* rhs_node = cast<const NumericLiteral>(&rhs);

    BOOST_ASSERT(rhs_node != nullptr && "mismatched type should be handled in other place");

    if (primitive_kind != rhs_node->primitive_kind)
        return false;

    switch(primitive_kind)
    {
    case PrimitiveKind::BOOL_TYPE    : if (!isEqualEval(value.b  , rhs_node->value.b  )) return false; break;
    case PrimitiveKind::INT8_TYPE    : if (!isEqualEval(value.i8 , rhs_node->value.i8 )) return false; break;
    case PrimitiveKind::INT16_TYPE   : if (!isEqualEval(value.i16, rhs_node->value.i16)) return false; break;
    case PrimitiveKind::INT32_TYPE   : if (!isEqualEval(value.i32, rhs_node->value.i32)) return false; break;
    case PrimitiveKind::INT64_TYPE   : if (!isEqualEval(value.i64, rhs_node->value.i64)) return false; break;
    case PrimitiveKind::FLOAT32_TYPE : if (!isEqualEval(value.f32, rhs_node->value.f32)) return false; break;
    case PrimitiveKind::FLOAT64_TYPE : if (!isEqualEval(value.f64, rhs_node->value.f64)) return false; break;
    default: break;
    }

    return true;
}

bool NumericLiteral::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    UNUSED_ARGUMENT(from);
    UNUSED_ARGUMENT(to);
    UNUSED_ARGUMENT(update_parent);

    return false;
}

NumericLiteral* NumericLiteral::clone() const
{
    switch(primitive_kind)
    {
    case PrimitiveKind::BOOL_TYPE    : return new NumericLiteral(value.b  );
    case PrimitiveKind::INT8_TYPE    : return new NumericLiteral(value.i8 );
    case PrimitiveKind::INT16_TYPE   : return new NumericLiteral(value.i16);
    case PrimitiveKind::INT32_TYPE   : return new NumericLiteral(value.i32);
    case PrimitiveKind::INT64_TYPE   : return new NumericLiteral(value.i64);
    case PrimitiveKind::FLOAT32_TYPE : return new NumericLiteral(value.f32);
    case PrimitiveKind::FLOAT64_TYPE : return new NumericLiteral(value.f64);
    default: break;
    }
    return NULL;
}

std::wostream& NumericLiteral::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    switch (primitive_kind)
    {
    case PrimitiveKind::BOOL_TYPE   : output << std::boolalpha << get<bool  >(); break;
    case PrimitiveKind::INT8_TYPE   : output <<                   get<int32 >(); break;
    case PrimitiveKind::INT16_TYPE  : output <<                   get<int32 >(); break;
    case PrimitiveKind::INT32_TYPE  : output <<                   get<int32 >(); break;
    case PrimitiveKind::INT64_TYPE  : output <<                   get<int64 >(); break;
    case PrimitiveKind::FLOAT32_TYPE: output <<                   get<float >(); break;
    case PrimitiveKind::FLOAT64_TYPE: output <<                   get<double>(); break;
    default                         : output << L"<unknown-primitive-type>"    ; break;
    }

    return output;
}

std::wstring NumericLiteral::toString() const
{
    std::wostringstream oss;
    switch(primitive_kind)
    {
    case PrimitiveKind::BOOL_TYPE   : oss <<        get<bool  >(); break;
    case PrimitiveKind::INT8_TYPE   : oss << (int32)get<int8  >(); break;
    case PrimitiveKind::INT16_TYPE  : oss << (int32)get<int16 >(); break;
    case PrimitiveKind::INT32_TYPE  : oss <<        get<int32 >(); break;
    case PrimitiveKind::INT64_TYPE  : oss <<        get<int64 >(); break;
    case PrimitiveKind::FLOAT32_TYPE: oss <<        get<float >(); break;
    case PrimitiveKind::FLOAT64_TYPE: oss <<        get<double>(); break;
    default: break;
    }
    return oss.str();
}

StringLiteral::StringLiteral() { }
StringLiteral::StringLiteral(const std::wstring& s) : value(s) {}

bool StringLiteral::hasValue() const
{
    return true;
}

bool StringLiteral::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (value)
    );
}

bool StringLiteral::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    UNUSED_ARGUMENT(from);
    UNUSED_ARGUMENT(to);
    UNUSED_ARGUMENT(update_parent);

    return false;
}

StringLiteral* StringLiteral::clone() const
{
    return new StringLiteral(value);
}

std::wostream& StringLiteral::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    std::wstring s;

    for (const auto c : value)
    {
        switch (c)
        {
        case L'\n': s += L"\\n"; break;
        case L'\r': s += L"\\r"; break;
        case L'\t': s += L"\\t"; break;
        default   : s += c     ; break;
        }
    }

    return output << s;
}

Type* StringLiteral::getCanonicalType() const
{
    Tangle* tangle = this->getOwner<Tangle>();
    BOOST_ASSERT(tangle && "no tangle node found");

    return tangle->internal->getPrimitiveType(PrimitiveKind::INT64_TYPE);
}

std::wstring StringLiteral::toString() const
{
    std::wostringstream oss;
    oss << L'"' << value << L'"';
    return oss.str();
}

bool StringLiteral::searchPlaceholder( string_type::const_iterator begin,
                               string_type::const_iterator end,
                               std::pair<
                                         string_type::const_iterator,
                                         string_type::const_iterator
                                        >& place )
{
    auto escape = std::find( begin, end, getInternalPlaceholderBegin() );
    if( escape == end )
        return false;

    // match rest part of the placeholder
    boost::basic_regex<string_type::value_type> rest_placeholder_rule( LR"__((\s*[a-zA-Z_][a-zA-Z0-9_]*\s*)(\.(\s*[a-zA-Z_][a-zA-Z0-9_]*\s*))*\})__" );
    boost::match_results<string_type::const_iterator> results;

    auto rest_begin = escape + 1;
    if( !boost::regex_search(rest_begin, end, results, rest_placeholder_rule) )
        return false;

    // format error
    if( results[ 0 ].first != rest_begin )
        return false;

    place.first = escape;
    place.second = results[ 0 ].second;

    return true;
}

IdLiteral::IdLiteral(const std::wstring& tag, int64 local_id, const UUID& tangle_uuid) : tag(tag), local_id(local_id), tangle_uuid(tangle_uuid)
{ }

bool IdLiteral::hasValue() const
{
    return true;
}

bool IdLiteral::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (tag     )
        (local_id)
        (tangle_uuid)
    );
}

bool IdLiteral::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    UNUSED_ARGUMENT(from);
    UNUSED_ARGUMENT(to);
    UNUSED_ARGUMENT(update_parent);

    return false;
}

Type* IdLiteral::getCanonicalType() const
{
    Tangle* tangle = this->getOwner<Tangle>();
    BOOST_ASSERT(tangle && "no tangle node found");

    return tangle->internal->getPrimitiveType(PrimitiveKind::INT64_TYPE);
}

std::wostream& IdLiteral::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    return output << L"id_literal(\"" << tag << L"\":" << local_id << L":\"" << tangle_uuid << L"\")";
}

std::wstring IdLiteral::toString() const
{
    std::wostringstream oss;
    oss << L"<id_literal>(\"" << tag << "\":" << local_id << L"\":" << tangle_uuid << L")";
    return oss.str();
}

SymbolIdLiteral* SymbolIdLiteral::clone() const
{
    return new SymbolIdLiteral(tag, local_id, tangle_uuid);
}

TypeIdLiteral* TypeIdLiteral::clone() const
{
    return new TypeIdLiteral(tag, local_id, tangle_uuid);
}

FunctionIdLiteral* FunctionIdLiteral::clone() const
{
    return new FunctionIdLiteral(tag, local_id, tangle_uuid);
}

} } }
