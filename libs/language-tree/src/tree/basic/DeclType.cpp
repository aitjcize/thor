/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/context/ParserContext.h"
#include "language/tree/basic/Type.h"
#include "language/tree/basic/DeclType.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/PrimitiveKind.h"
#include "language/tree/basic/PrimitiveType.h"
#include "language/tree/basic/FunctionType.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/EnumDecl.h"
#include "language/tree/declaration/TypenameDecl.h"
#include "language/tree/declaration/TypedefDecl.h"
#include "language/tree/ASTNodeHelper.h"

//////////////////////////////////////////////////////////////////////////////
// DeclType
//////////////////////////////////////////////////////////////////////////////

DeclType* DeclType::create(Declaration* declaration)
{
    UNIMPLEMENTED_CODE();
    return nullptr;
}

//////////////////////////////////////////////////////////////////////////////
// RecordType
//////////////////////////////////////////////////////////////////////////////

RecordType::RecordType() : decl_(nullptr)
{
}

RecordType::RecordType(ClassDecl* classDecl) : decl_(classDecl)
{
    this->parent = classDecl;
}

bool RecordType::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (decl_)
    );
}

bool RecordType::replaceUseWith(const ASTNode& from,
                                const ASTNode& to,
                                bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (decl_)
    );
}

RecordType* RecordType::clone() const
{
    // Only one RecordType of one pointee type is allowed
    // if you need multiple RecordType of the same pointee type
    // please check your design
    UNREACHABLE_CODE();
    return nullptr;
}

std::wostream& RecordType::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(decl_ && "null pointer exception");
    BOOST_ASSERT(decl_->name && "null pointer exception");

    output << L"class " << out_source(*decl_->name, indent);

    return output;
}

bool RecordType::isSame(const Type& rhs) const
{
    if(!rhs.isPointerType())
        return false;

    const RecordType* rhs_pt = cast<RecordType>(&rhs);
    return getDecl() == rhs_pt->getDecl();
}

bool RecordType::isLessThan(const Type& rhs) const
{
    if(getTypeClassSerialNumber() < rhs.getTypeClassSerialNumber())
        return true;
    if(rhs.getTypeClassSerialNumber() < getTypeClassSerialNumber())
        return false;

    BOOST_ASSERT(isa<RecordType>(&rhs));
    const RecordType* pt_rhs = cast<RecordType>(&rhs);

    return getDecl() < pt_rhs->getDecl();
}

std::wstring RecordType::toString() const
{
    return getDecl()->toString();
}

Declaration* RecordType::getDecl() const
{
    return decl_;
}

RecordType* RecordType::getCanonicalType() const
{
    return const_cast<RecordType*>(this);
}

bool RecordType::isInterface() const
{
    return decl_->is_interface;
}

size_t RecordType::getTypeClassSerialNumber() const
{
    return ASTNodeType::RecordType;
}

Type::ConversionRank RecordType::getConversionRank(const Type& target, const Type::ConversionPolicy policy) const
{
    const RecordType* target_record_type = cast<RecordType>(&target);

    if (target_record_type == nullptr)
        return ConversionRank::NotMatch;

    if (this == target_record_type)
        return ConversionRank::ExactMatch;

    auto is_inherited_from = ASTNodeHelper::isInheritedFromEx(getAsClassDecl(), target_record_type->getAsClassDecl());

    static_assert(std::is_same<decltype(is_inherited_from), boost::tribool>::value, "we expect the return value of ASTNodeHelper::isInheritedFromEx is boost::tribool!");

    if(is_inherited_from)
        return ConversionRank::StandardConversion;
    else if(boost::indeterminate(is_inherited_from))
        return ConversionRank::UnknownYet;
    else
        return ConversionRank::NotMatch;
}

//////////////////////////////////////////////////////////////////////////////
// EnumType
//////////////////////////////////////////////////////////////////////////////

EnumType::EnumType() : decl_(nullptr)
{
}

EnumType::EnumType(EnumDecl* enumDecl) : decl_(enumDecl)
{
    this->parent = enumDecl;
}

bool EnumType::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (decl_)
    );
}

bool EnumType::replaceUseWith(const ASTNode& from,
                              const ASTNode& to,
                              bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (decl_)
    );
}

EnumType* EnumType::clone() const
{
    // Only one EnumType of one pointee type is allowed
    // if you need multiple EnumType of the same pointee type
    // please check your design
    UNREACHABLE_CODE();
    return nullptr;
}

std::wostream& EnumType::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(decl_ && "null pointer exception");
    BOOST_ASSERT(decl_->name && "null pointer exception");

    output << L"enum " << out_source(*decl_->name, indent);

    return output;
}

bool EnumType::isSame(const Type& rhs) const
{
    if(!rhs.isPointerType())
        return false;

    const EnumType* rhs_pt = cast<EnumType>(&rhs);
    return getDecl() == rhs_pt->getDecl();
}

bool EnumType::isLessThan(const Type& rhs) const
{
    if(getTypeClassSerialNumber() < rhs.getTypeClassSerialNumber())
        return true;
    if(rhs.getTypeClassSerialNumber() < getTypeClassSerialNumber())
        return false;

    BOOST_ASSERT(isa<EnumType>(&rhs));
    const EnumType* pt_rhs = cast<EnumType>(&rhs);

    return getDecl() < pt_rhs->getDecl();
}

std::wstring EnumType::toString() const
{
    return getDecl()->toString();
}

Declaration* EnumType::getDecl() const
{
    return decl_;
}

EnumType* EnumType::getCanonicalType() const
{
    return const_cast<EnumType*>(this);
}

PrimitiveType* EnumType::getArithmeticCompatibleType() const
{
    const EnumDecl*const decl = getAsEnumDecl();

    BOOST_ASSERT(decl && "EnumType does not refer to EnumDecl");

    const auto& underlying_kind = decl->getUnderlyingType();

    BOOST_ASSERT(underlying_kind.isArithmeticCapable() && "underlying type of EnumDecl is not arithmetic compatible");

    const auto*const tangle = getOwner<Tangle>();

    BOOST_ASSERT(tangle && "no tangle");
    BOOST_ASSERT(tangle->internal && "no internal node under tangle");

    return tangle->internal->getPrimitiveType(underlying_kind);
}

size_t EnumType::getTypeClassSerialNumber() const
{
    return ASTNodeType::EnumType;
}

Type::ConversionRank EnumType::getConversionRank(const Type& target, const Type::ConversionPolicy policy) const
{
    PrimitiveKind source_kind = PrimitiveKind::VOID_TYPE;
    PrimitiveKind target_kind = PrimitiveKind::VOID_TYPE;

    const EnumDecl* source_decl = getAsEnumDecl();
    BOOST_ASSERT(source_decl && "null pointer exception");
    source_kind = source_decl->getUnderlyingType();
    BOOST_ASSERT((source_kind == PrimitiveKind::INT32_TYPE || source_kind == PrimitiveKind::INT64_TYPE) && "unexpected underlying type");

    // enum to enum
    if (const EnumType* target_enum_type = cast<EnumType>(&target))
    {
        // exactly same enum
        if (this == target_enum_type)
        {
            return ConversionRank::ExactMatch;
        }
        // different enum need explicit cast
        else if(policy == Type::ConversionPolicy::Explicitly)
        {
            const EnumDecl* target_decl = target_enum_type->getAsEnumDecl();
            BOOST_ASSERT(target_decl && "null pointer exception");
            target_kind = target_decl->getUnderlyingType();
            BOOST_ASSERT((target_kind == PrimitiveKind::INT32_TYPE || target_kind == PrimitiveKind::INT64_TYPE) && "unexpected underlying type");
            Type::ConversionRank r = source_kind.getConversionRank(target_kind);
            return r;
        }
    }
    // enum to int
    else if (const PrimitiveType* target_primitive_type = cast<PrimitiveType>(&target))
    {
        if(policy == Type::ConversionPolicy::Explicitly)
        {
            target_kind = target_primitive_type->getKind();
            Type::ConversionRank r = source_kind.getConversionRank(target_kind);
            return r;
        }
    }

    return source_kind.getConversionRank(target_kind);
}

//////////////////////////////////////////////////////////////////////////////
// TypedefeType
//////////////////////////////////////////////////////////////////////////////

TypedefType::TypedefType() : decl_(nullptr)
{
}

TypedefType::TypedefType(TypedefDecl* typedefDecl) : decl_(typedefDecl)
{
    this->parent = typedefDecl;
}

bool TypedefType::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (decl_)
    );
}

bool TypedefType::replaceUseWith(const ASTNode& from,
                              const ASTNode& to,
                              bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (decl_)
    );
}

TypedefType* TypedefType::clone() const
{
    // Only one TypedefType of one pointee type is allowed
    // if you need multiple TypedefType of the same pointee type
    // please check your design
    UNREACHABLE_CODE();
    return nullptr;
}

std::wostream& TypedefType::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(decl_ && "null pointer exception");
    BOOST_ASSERT(decl_->name && "null pointer exception");

    output << L"typedef " << out_source(*decl_->name, indent);

    return output;
}

bool TypedefType::isSame(const Type& rhs) const
{
    if(!rhs.isPointerType())
        return false;

    const TypedefType* rhs_pt = cast<TypedefType>(&rhs);
    return getDecl() == rhs_pt->getDecl();
}

bool TypedefType::isLessThan(const Type& rhs) const
{
    if(getTypeClassSerialNumber() < rhs.getTypeClassSerialNumber())
        return true;
    if(rhs.getTypeClassSerialNumber() < getTypeClassSerialNumber())
        return false;

    BOOST_ASSERT(isa<TypedefType>(&rhs));
    const TypedefType* pt_rhs = cast<TypedefType>(&rhs);

    return getDecl() < pt_rhs->getDecl();
}

std::wstring TypedefType::toString() const
{
    return getDecl()->toString();
}

Declaration* TypedefType::getDecl() const
{
    return decl_;
}

Type* TypedefType::getCanonicalType() const
{
    return this->getDecl()->getCanonicalType();
}

size_t TypedefType::getTypeClassSerialNumber() const
{
    return ASTNodeType::TypedefType;
}

Type::ConversionRank TypedefType::getConversionRank(const Type& target, const Type::ConversionPolicy policy) const
{
    return ConversionRank::NotMatch;
}

//////////////////////////////////////////////////////////////////////////////
// TypenameType
//////////////////////////////////////////////////////////////////////////////

TypenameType::TypenameType() : decl_(nullptr)
{
}

TypenameType::TypenameType(TypenameDecl* typenameDecl) : decl_(typenameDecl)
{
    this->parent = typenameDecl;
}

bool TypenameType::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (decl_)
    );
}

bool TypenameType::replaceUseWith(const ASTNode& from,
                              const ASTNode& to,
                              bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (decl_)
    );
}

TypenameType* TypenameType::clone() const
{
    // Only one TypenameType of one pointee type is allowed
    // if you need multiple TypenameType of the same pointee type
    // please check your design
    //UNREACHABLE_CODE();
    TypenameType* result = new TypenameType(this->decl_);
    return result;
}

std::wostream& TypenameType::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(decl_ && "null pointer exception");
    BOOST_ASSERT(decl_->name && "null pointer exception");

    output << L"typename " << out_source(*decl_->name, indent);

    return output;
}

bool TypenameType::isSame(const Type& rhs) const
{
    if(!rhs.isPointerType())
        return false;

    const TypenameType* rhs_pt = cast<TypenameType>(&rhs);
    return getDecl() == rhs_pt->getDecl();
}

bool TypenameType::isLessThan(const Type& rhs) const
{
    if(getTypeClassSerialNumber() < rhs.getTypeClassSerialNumber())
        return true;
    if(rhs.getTypeClassSerialNumber() < getTypeClassSerialNumber())
        return false;

    BOOST_ASSERT(isa<TypenameType>(&rhs));
    const TypenameType* pt_rhs = cast<TypenameType>(&rhs);

    return getDecl() < pt_rhs->getDecl();
}

std::wstring TypenameType::toString() const
{
    return getDecl()->toString();
}

Declaration* TypenameType::getDecl() const
{
    return decl_;
}

Type* TypenameType::getCanonicalType() const
{
    return getDecl()->getCanonicalType();
}

size_t TypenameType::getTypeClassSerialNumber() const
{
    return ASTNodeType::TypenameType;
}

Type::ConversionRank TypenameType::getConversionRank(const Type& target, const Type::ConversionPolicy policy) const
{
    return ConversionRank::NotMatch;
}

