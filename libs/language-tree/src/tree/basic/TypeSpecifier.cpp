/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <functional>
#include <ostream>
#include <string>
#include <utility>
#include <vector>

#include <boost/algorithm/cxx11/all_of.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/begin.hpp>
#include <boost/range/end.hpp>

#include "utility/Foreach.h"
#include "utility/Functional.h"

#include "core/Types.h"

#include "language/tree/ASTNode.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/PrimitiveKind.h"
#include "language/tree/basic/Type.h"
#include "language/tree/basic/TypeSpecifier.h"

namespace zillians { namespace language { namespace tree {

Type* TypeSpecifier::getCanonicalType() const
{
    if (resolved_type == nullptr)
        return nullptr;

    return resolved_type->getCanonicalType();
}

bool TypeSpecifier::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
    );
}

bool TypeSpecifier::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
    );
}

PrimitiveSpecifier::PrimitiveSpecifier()
    : kind(PrimitiveKind::VOID_TYPE)
{
}

PrimitiveSpecifier::PrimitiveSpecifier(PrimitiveKind kind)
    : kind(kind)
{
}

PrimitiveKind PrimitiveSpecifier::getKind() const
{
    return kind;
}

std::wstring PrimitiveSpecifier::toString() const
{
    return kind.toString();
}

bool PrimitiveSpecifier::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (kind)
    );
}

bool PrimitiveSpecifier::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (kind)
    );
}

PrimitiveSpecifier* PrimitiveSpecifier::clone() const
{
    return new PrimitiveSpecifier(kind);
}

std::wostream& PrimitiveSpecifier::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << kind.toString();

    return output;
}

NamedSpecifier::NamedSpecifier()
    : name(nullptr)
{
}

NamedSpecifier::NamedSpecifier(Identifier* name)
    : name(name)
{
    NOT_NULL(name);

    name->parent = this;
}

Identifier* NamedSpecifier::getName() const noexcept
{
    return name;
}

std::wstring NamedSpecifier::toString() const
{
    NOT_NULL(name);

    return name->toString();
}

bool NamedSpecifier::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (name)
    );
}

bool NamedSpecifier::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (name)
    );
}

NamedSpecifier* NamedSpecifier::clone() const
{
    NOT_NULL(name);

    return new NamedSpecifier(name->clone());
}

std::wostream& NamedSpecifier::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    NOT_NULL(name);

    output << out_source(*name, indent);

    return output;
}

MultiSpecifier::MultiSpecifier()
    : types()
{
}

MultiSpecifier::MultiSpecifier(const std::vector<TypeSpecifier*>& types)
    : types(types)
{
    for (auto*const type : this->types)
    {
        NOT_NULL(type);

        type->parent = this;
    }
}

MultiSpecifier::MultiSpecifier(std::vector<TypeSpecifier*>&& types)
    : types(std::move(types))
{
    for (auto*const type : this->types)
    {
        NOT_NULL(type);

        type->parent = this;
    }
}

const std::vector<TypeSpecifier*>& MultiSpecifier::getTypes() const noexcept
{
    return types;
}

std::wstring MultiSpecifier::toString() const
{
    using boost::adaptors::transformed;

    return L"(" + boost::join(types | transformed(std::mem_fn(&TypeSpecifier::toString)), L", ") + L")";
}

bool MultiSpecifier::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (types)
    );
}

bool MultiSpecifier::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (types)
    );
}

MultiSpecifier* MultiSpecifier::clone() const
{
    BOOST_ASSERT(boost::algorithm::all_of(types, not_null()) && "null pointer exception");

    using boost::adaptors::transformed;

    const auto& cloned_types = types | transformed(std::mem_fn(&TypeSpecifier::clone));

    return new MultiSpecifier({boost::begin(cloned_types), boost::end(cloned_types)});
}

std::wostream& MultiSpecifier::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(boost::algorithm::all_of(types, not_null()) && "null pointer exception");

    output << L'(';

    for_each_and_pitch(
        types,
        [&output, indent](TypeSpecifier* type)
        {
            output << out_source(*type, indent);
        },
        [&output]
        {
            output << L", ";
        }
    );

    output << L')';

    return output;
}

FunctionSpecifier::FunctionSpecifier()
    : param_types()
    , return_type(nullptr)
    , is_lambda(false)
{
}

FunctionSpecifier::FunctionSpecifier(const std::vector<TypeSpecifier*>& param_types, TypeSpecifier* return_type, bool is_lambda/* = false*/)
    : param_types(param_types)
    , return_type(return_type)
    , is_lambda(is_lambda)
{
    for (auto*const param_type : this->param_types)
    {
        NOT_NULL(param_type);

        param_type->parent = this;
    }

    if (return_type != nullptr)
        return_type->parent = this;
}

FunctionSpecifier::FunctionSpecifier(std::vector<TypeSpecifier*>&& param_types, TypeSpecifier* return_type, bool is_lambda/* = false*/)
    : param_types(std::move(param_types))
    , return_type(return_type)
    , is_lambda(is_lambda)
{
    for (auto*const param_type : this->param_types)
    {
        NOT_NULL(param_type);

        param_type->parent = this;
    }

    if (return_type != nullptr)
        return_type->parent = this;
}

const std::vector<TypeSpecifier*>& FunctionSpecifier::getParameterTypes() const noexcept
{
    return param_types;
}

TypeSpecifier* FunctionSpecifier::getReturnType() const noexcept
{
    return return_type;
}

bool FunctionSpecifier::isLambda() const noexcept
{
    return is_lambda;
}

std::wstring FunctionSpecifier::toString() const
{
    using boost::adaptors::transformed;

    std::wstring str = is_lambda ? L"lambda (" : L"function (";

    str += boost::algorithm::join(
        param_types | transformed(std::mem_fn(&TypeSpecifier::toString)),
        L", "
    );

    str += L"):";

    if (return_type == nullptr)
        str += L"<not-ready>";
    else
        str += return_type->toString();

    return std::move(str);
}

bool FunctionSpecifier::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (param_types)
        (return_type)
        (is_lambda  )
    );
}

bool FunctionSpecifier::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (param_types)
        (return_type)
        (is_lambda  )
    );
}

FunctionSpecifier* FunctionSpecifier::clone() const
{
    BOOST_ASSERT(boost::algorithm::all_of(param_types, not_null()) && "null pointer exception");

    using boost::adaptors::transformed;

    const auto&      cloned_param_types = param_types | transformed(std::mem_fn(&TypeSpecifier::clone));
          auto*const cloned_return_type = clone_or_null(return_type);

    return new FunctionSpecifier({boost::begin(cloned_param_types), boost::end(cloned_param_types)}, cloned_return_type, is_lambda);
}

std::wostream& FunctionSpecifier::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << (is_lambda ? L"lambda (" : L"function (");

    for_each_and_pitch(
        param_types,
        [&output, indent](TypeSpecifier* param_type)
        {
            output << out_source(*param_type, indent);
        },
        [&output]
        {
            output << L", ";
        }
    );

    output << L"):";

    if (return_type == nullptr)
        output << L"<not-ready>";
    else
        output << out_source(*return_type, indent);

    return output;
}

} } }
