/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <limits>
#include "runtime/reactor/BasicReactor.h"

#include "utility/UnicodeUtil.h"

#include "language/SystemFunction.h"

#include "framework/Processor.h"
#include "framework/Service.h"
#include "framework/ProcessorId.h"
#include "framework/processor/KernelMT.h"
#include "framework/processor/ProcessorCPU.h"
#include "framework/processor/ProcessorGateway.h"
#include "framework/service/runtime/RuntimeServiceMT.h"
#include "framework/service/object/ObjectServiceMT.h"
#include "framework/service/domain/DomainServiceMT.h"
#include "framework/buffer/init/HostBufferKernelInit.h"

#ifdef BUILD_WITH_CUDA
#include "framework/processor/KernelCUDA.h"
#include "framework/processor/ProcessorCUDA.h"
#include "framework/service/runtime/RuntimeServiceCUDA.h"
#include "framework/service/object/ObjectServiceCUDA.h"
#include "framework/buffer/init/CudaBufferKernelInit.h"
#endif

namespace zillians { namespace runtime {

using namespace zillians::framework;
using namespace zillians::framework::processor;
using namespace zillians::framework::service;


BasicReactor::BasicReactor(DomainType domain_type, VMMode vm_mode, int dev_id, const std::vector<std::string>& args, bool verbose) :
		mExecutionEngineThread(NULL), mProcessorExternal(NULL), mLogger(log4cxx::Logger::getLogger("ClientReactor"))
{
	config.domain_type = domain_type;
	config.vm_mode     = vm_mode;
	config.dev_id      = dev_id;
	config.args        = args;
	config.verbose     = verbose;
}

BasicReactor::~BasicReactor()
{
	SAFE_NULL(mProcessorExternal); // we don't need to delete since the ownership is delegated to execution engine
    SAFE_DELETE(mExecutionEngineThread);
}

void BasicReactor::initialize()
{
	buffer::initHostBufferKernels();

    std::vector<std::wstring> wargs;
    for(auto arg : config.args)
    {
        wargs.push_back(s_to_ws(arg));
    }

    switch(config.domain_type)
    {
    case DomainType::SingleThreaded:
    case DomainType::MultiThreaded:
    {
    	auto principle = new ProcessorCPU(ProcessorId::PROCESSOR_PRINCIPLE);

    	auto runtime_service = make_shared<service::mt::RuntimeService>(principle, wargs);
    	auto  object_service = make_shared<service::mt::ObjectService >(principle);
    	auto  domain_service = make_shared<service::mt::DomainService >(principle);

    	principle->setMultiThread(config.domain_type == DomainType::MultiThreaded);

    	mExecutionEngine.attachProcessor(principle);
    	principle->attachService(ServiceId::SERVICE_RUNTIME, std::move(runtime_service));
    	principle->attachService(ServiceId::SERVICE_OBJECT , std::move( object_service));
    	principle->attachService(ServiceId::SERVICE_DOMAIN , std::move( domain_service));

    	break;
    }
    case DomainType::CUDA:
    {
#ifdef BUILD_WITH_CUDA
    	buffer::initCudaBufferKernels();
    	Configuration::instance()->enable_cuda = true;

    	auto principle = new ProcessorCUDA(ProcessorId::PROCESSOR_PRINCIPLE, config.dev_id);

    	auto runtime_service = make_shared<service::cuda::RuntimeService>(principle);
    	auto  object_service = make_shared<service::cuda::ObjectService >(principle);

    	mExecutionEngine.attachProcessor(principle);
    	principle->attachService(ServiceId::SERVICE_RUNTIME, std::move(runtime_service));
    	principle->attachService(ServiceId::SERVICE_OBJECT , std::move( object_service));
#else
    	std::cerr << "Error: CUDA domain is not supported in this build" << std::endl;
#endif
    	break;
    }
    case DomainType::GPU:
    case DomainType::Kepler:
    case DomainType::Tahiti:
    case DomainType::OpenCL:
    {
    	//buffer::initOpenCLBufferKernels();
    	Configuration::instance()->enable_opencl = true;
    	UNIMPLEMENTED_CODE();
    	break;
    }
    case DomainType::K10M:
    {
    	UNIMPLEMENTED_CODE();
    	break;
    }
    }

    mProcessorExternal = new ProcessorExternal(ProcessorId::PROCESSOR_EXTERNAL);
    mExecutionEngine.attachProcessor(mProcessorExternal);

    ProcessorGateway* gateway = new ProcessorGateway(ProcessorId::PROCESSOR_GATEWAY_START);
    mExecutionEngine.attachProcessor(gateway);

    mExecutionEngine.initializeAllProcessors();

    ProcessorRT* rt_principle = reinterpret_cast<ProcessorRT*>(mExecutionEngine[ProcessorId::PROCESSOR_PRINCIPLE]);
    const auto&  rt_kernel    = rt_principle->getKernel();

    if (config.verbose)
        rt_kernel->setVerbose(true);

    gateway->setKernel(rt_principle->getKernel());
}

void BasicReactor::finalize()
{
    mExecutionEngine.finalizeAllProcessors();
}

void BasicReactor::start()
{
    mExecutionEngine.startAllProcessors();
}

void BasicReactor::stop()
{
    mExecutionEngine.stopAllProcessors();
}

void BasicReactor::kill()
{
}

int32 BasicReactor::waitForCompletion()
{
    mExecutionEngine.waitForCompletion();

    // return the exit code from runtime service
    Processor*const   processor    = mExecutionEngine[ProcessorId::PROCESSOR_PRINCIPLE];
    ProcessorRT*const processor_rt = static_cast<ProcessorRT*>(processor);

    BOOST_ASSERT(processor    != nullptr && "no processor!");
    BOOST_ASSERT(processor_rt != nullptr && "no processor!");

    return processor_rt->getExitCode();
}

void BasicReactor::configureHardwareArchitecture(const std::string& arch)
{
	if(config.domain_type == DomainType::CUDA)
	{
#ifdef BUILD_WITH_CUDA
		shared_ptr<KernelBase> kernel = reinterpret_cast<ProcessorRT*>(mExecutionEngine[ProcessorId::PROCESSOR_PRINCIPLE])->getKernel();
		shared_ptr<processor::cuda::Kernel> kernel_cuda = static_pointer_cast<processor::cuda::Kernel>(kernel);
		kernel_cuda->configureHardwareArchitecture(arch.c_str());
#else
		std::cerr << "Error: unsupported architecture \"" << arch << "\"" << std::endl;
#endif
	}
	else
	{
		UNREACHABLE_CODE();
	}
}

bool BasicReactor::loadKernel(const boost::filesystem::path& main_ast_path,
                              const boost::filesystem::path& main_runtime_path,
                              const std::vector<boost::filesystem::path>& dep_paths)
{
    ProcessorRT*const rt_principle = reinterpret_cast<ProcessorRT*>(mExecutionEngine[ProcessorId::PROCESSOR_PRINCIPLE]);
    const auto&       rt_kernel    = rt_principle->getKernel();

    if(!rt_principle->load(main_ast_path, main_runtime_path, dep_paths))
    {
        LOG4CXX_ERROR(mLogger, "[BasicReactor::loadKernel] cannot load kernel");
        return false;
    }
    return true;
}

bool BasicReactor::runInit()
{
    return runFunction(language::system_function::global_initialization_id);
}

bool BasicReactor::runSystemInit()
{
    return runFunction(language::system_function::system_initialization_id);
}

bool BasicReactor::runEntry(const std::string& entry_name)
{
    ProcessorRT* processor = reinterpret_cast<ProcessorRT*>(mExecutionEngine[ProcessorId::PROCESSOR_PRINCIPLE]);

    int64 function_id = processor->queryFunctionId(entry_name);
    if(function_id == -1)
    {
        std::cerr << "Error: failed to find entry function \"" << entry_name << "\"" << std::endl;
        return false;
    }

    if(!runFunction(function_id, false))
    	return false;

    return true;
}

bool BasicReactor::runFunction(int64 function_id, const bool block/* = true*/)
{
    // create invocation request buffer
    InvocationRequestBuffer* buffer = new InvocationRequestBuffer(ProcessorId::PROCESSOR_EXTERNAL);
    if(!BufferNetwork::instance()->create(ProcessorId::PROCESSOR_EXTERNAL, *buffer, InvocationRequestBuffer::Dimension(1)))
    {
        // FIXME memory leak: buffer

    	return false;
    }

    if(config.verbose)
        std::cerr << "running function id = " << function_id << std::endl;

    buffer->setExecutionMode(false);
    buffer->setInvocationCount(1);
    buffer->setFunctionId(0, function_id);
    buffer->setSessionId(0, -1);

    auto  itc                = KernelITC::make<KernelITC::InvocationRequest>(0, 0, new InvocationRequestBuffer(ProcessorId::PROCESSOR_PRINCIPLE));
    auto& invocation_request = itc.getInvocationRequest();

#ifdef BUILD_WITH_CUDA
    // FIXME, currently the device location is hard coded.
    // need to adaptively choose.
    // and, not always need construct the buffer. figure out the condition
    if(dynamic_cast<ProcessorCUDA*>(mExecutionEngine[0])) {
        auto target_buffer_location = buffer::buffer_location_t::get_ptx_device_location(0);
        invocation_request.buffer->construct(target_buffer_location, buffer->declared_dim);
    }
#endif

    if(BufferNetwork::instance()->moveOrCopy(ProcessorId::PROCESSOR_EXTERNAL, ProcessorId::PROCESSOR_PRINCIPLE, *buffer, *invocation_request.buffer))
    {
    	if(!buffer->is_same(*invocation_request.buffer))
    	{
    		// if the buffer is copied, the source buffer can be safely deleted
    		// otherwise, the ownership of the buffer is moved to the principle processor who will be in charge of clean up the buffer allocation
    		BufferNetwork::instance()->destroy(*buffer);
    		SAFE_DELETE(buffer);
    	}
    }

    if(block)
    {
        ConditionVariable<int> cond;
        mProcessorExternal->enqueueITC(ProcessorId::PROCESSOR_PRINCIPLE, itc, [&](uint32 source, KernelITC& itc) -> void {
            BOOST_ASSERT(itc.isInvocationResponse());
            cond.signal(0);
        });

        int dummy_result = 0;
        cond.wait(dummy_result);
    }
    else
    {
        mProcessorExternal->enqueueITC(ProcessorId::PROCESSOR_PRINCIPLE, itc);
    }

    return true;
}

} }
