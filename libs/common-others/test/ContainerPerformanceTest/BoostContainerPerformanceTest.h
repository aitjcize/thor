/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#ifndef BOOSTCONTAINERPERFORMANCETEST_H_
#define BOOSTCONTAINERPERFORMANCETEST_H_

#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>
#include <tbb/tick_count.h>

// test boost::unordered_map insertion and deletion performance (in same order)
void test_boost_unordered_map_insert_search_delete_in_same_order(int iterations)
{
    boost::unordered_map<int,int> m;
    tbb::tick_count start, end;
    
    start = tbb::tick_count::now();
    {
        for(int i=0;i<iterations;++i)
        {
            m[i] = i;
        }
    }
    end = tbb::tick_count::now();
    printf("\tinsertion takes %lf ms\n", (end - start).seconds()*1000.0);
    
    start = tbb::tick_count::now();
    {
        for(int i=0;i<iterations;++i)
        {
            volatile boost::unordered_map<int,int>::iterator it = m.find(i);
            it;
        }
    }
    end = tbb::tick_count::now();
    printf("\tsearch takes %lf ms\n", (end - start).seconds()*1000.0);
    
    start = tbb::tick_count::now();
    {
        for(int i=0;i<iterations;++i)
        {
            m.erase(m.find(i));
        }
    }
    end = tbb::tick_count::now();
    printf("\tdeletion takes %lf ms\n", (end - start).seconds()*1000.0);
}

// test boost::unordered_map insertion and deletion performance (in reversed order)
void test_boost_unordered_map_insert_search_delete_in_reverse_order(int iterations)
{
    boost::unordered_map<int,int> m;
    tbb::tick_count start, end;
    
    start = tbb::tick_count::now();
    {
        for(int i=0;i<iterations;++i)
        {
            m[i] = i;
        }
    }
    end = tbb::tick_count::now();
    printf("\tinsertion takes %lf ms\n", (end - start).seconds()*1000.0);
    
    start = tbb::tick_count::now();
    {
        for(int i=iterations-1;i>=0;--i)
        {
            volatile boost::unordered_map<int,int>::iterator it = m.find(i);
            it;
        }
    }
    end = tbb::tick_count::now();
    printf("\tsearch takes %lf ms\n", (end - start).seconds()*1000.0);
    
    start = tbb::tick_count::now();
    {
        for(int i=iterations-1;i>=0;--i)
        {
            m.erase(m.find(i));
        }
    }
    end = tbb::tick_count::now();
    printf("\tdeletion takes %lf ms\n", (end - start).seconds()*1000.0);
}

// test boost::unordered_set insertion and deletion performance (in same order)
void test_boost_unordered_set_insert_search_delete_in_same_order(int iterations)
{
    boost::unordered_set<int> m;
    tbb::tick_count start, end;
    
    start = tbb::tick_count::now();
    {
        for(int i=0;i<iterations;++i)
        {
            m.insert(i);
        }
    }
    end = tbb::tick_count::now();
    printf("\tinsertion takes %lf ms\n", (end - start).seconds()*1000.0);
    
    start = tbb::tick_count::now();
    {
        for(int i=0;i<iterations;++i)
        {
            volatile boost::unordered_set<int>::iterator it = m.find(i);
            it;
        }
    }
    end = tbb::tick_count::now();
    printf("\tsearch takes %lf ms\n", (end - start).seconds()*1000.0);
    
    start = tbb::tick_count::now();
    {
        for(int i=0;i<iterations;++i)
        {
            m.erase(m.find(i));
        }
    }
    end = tbb::tick_count::now();
    printf("\tdeletion takes %lf ms\n", (end - start).seconds()*1000.0);
}

// test boost::unordered_set insertion and deletion performance (in reversed order)
void test_boost_unordered_set_insert_search_delete_in_reverse_order(int iterations)
{
    boost::unordered_set<int> m;
    tbb::tick_count start, end;
    
    start = tbb::tick_count::now();
    {
        for(int i=0;i<iterations;++i)
        {
            m.insert(i);
        }
    }
    end = tbb::tick_count::now();
    printf("\tinsertion takes %lf ms\n", (end - start).seconds()*1000.0);
    
    start = tbb::tick_count::now();
    {
        for(int i=iterations-1;i>=0;--i)
        {
            volatile boost::unordered_set<int>::iterator it = m.find(i);
            it;
        }
    }
    end = tbb::tick_count::now();
    printf("\tsearch takes %lf ms\n", (end - start).seconds()*1000.0);
    
    start = tbb::tick_count::now();
    {
        for(int i=iterations-1;i>=0;--i)
        {
            m.erase(m.find(i));
        }
    }
    end = tbb::tick_count::now();
    printf("\tdeletion takes %lf ms\n", (end - start).seconds()*1000.0);
}

#endif /*BOOSTCONTAINERPERFORMANCETEST_H_*/
