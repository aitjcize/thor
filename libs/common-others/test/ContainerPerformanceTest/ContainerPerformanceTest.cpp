/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#define TEST_STLPORT 0

#include "core/Types.h"

#if TEST_STLPORT
    #include "STLPortContainerPerformance.h"
#else
    #include "STDContainerPerformanceTest.h"
    #include "BoostContainerPerformanceTest.h"
    #include "TBBContainerPerformanceTest.h"
#endif

#define ITERATION_COUNT 1
#define ELEMENT_COUNT 20000
int main(int argc, char** argv)
{
    printf("ITERATION_COUNT = %d, ELEMENT_COUNT = %d\n", ITERATION_COUNT, ELEMENT_COUNT);

#if TEST_STLPORT
    printf("[test_stlport_hash_map_insert_search_delete_in_same_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_stlport_hash_map_insert_search_delete_in_same_order(ELEMENT_COUNT);
    }

    printf("[test_stlport_hash_map_insert_search_delete_in_reverse_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_stlport_hash_map_insert_search_delete_in_reverse_order(ELEMENT_COUNT);
    }

#else
    printf("[test_std_map_insert_delete_in_same_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_std_map_insert_search_delete_in_same_order(ELEMENT_COUNT);
    }

    printf("[test_std_map_insert_delete_in_reverse_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_std_map_insert_search_delete_in_reverse_order(ELEMENT_COUNT);
    }

    printf("[test_gnucxx_hash_map_insert_search_delete_in_same_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_gnucxx_hash_map_insert_search_delete_in_same_order(ELEMENT_COUNT);
    }

    printf("[test_gnucxx_hash_map_insert_search_delete_in_reverse_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_gnucxx_hash_map_insert_search_delete_in_reverse_order(ELEMENT_COUNT);
    }

    printf("[test_gnucxx_hash_set_insert_search_delete_in_same_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_gnucxx_hash_set_insert_search_delete_in_same_order(ELEMENT_COUNT);
    }

    printf("[test_gnucxx_hash_set_insert_search_delete_in_reverse_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_gnucxx_hash_set_insert_search_delete_in_reverse_order(ELEMENT_COUNT);
    }

    printf("[test_std_unordered_map_insert_search_delete_in_same_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_std_unordered_map_insert_search_delete_in_same_order(ELEMENT_COUNT);
    }

    printf("[test_std_unordered_map_insert_search_delete_in_reverse_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_std_unordered_map_insert_search_delete_in_reverse_order(ELEMENT_COUNT);
    }

    printf("[test_boost_unordered_map_insert_delete_in_same_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_boost_unordered_map_insert_search_delete_in_same_order(ELEMENT_COUNT);
    }

    printf("[test_boost_unordered_map_insert_delete_in_reverse_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_boost_unordered_map_insert_search_delete_in_reverse_order(ELEMENT_COUNT);
    }

    printf("[test_boost_unordered_set_insert_delete_in_same_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_boost_unordered_set_insert_search_delete_in_same_order(ELEMENT_COUNT);
    }

    printf("[test_boost_unordered_set_insert_delete_in_reverse_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_boost_unordered_set_insert_search_delete_in_reverse_order(ELEMENT_COUNT);
    }

    printf("[test_tbb_concurrent_hash_map_insert_search_delete_in_same_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_tbb_concurrent_hash_map_insert_search_delete_in_same_order(ELEMENT_COUNT);
    }

    printf("[test_tbb_concurrent_hash_map_insert_search_delete_in_reverse_order]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_tbb_concurrent_hash_map_insert_search_delete_in_reverse_order(ELEMENT_COUNT);
    }

    printf("[test_concurrent_queue_push_pop]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_concurrent_queue_push_pop(ELEMENT_COUNT);
    }

    printf("[test_std_priority_queue_push_pop]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_std_priority_queue_push_pop(ELEMENT_COUNT);
    }


#endif
    /*
    printf("[test_my_concurrent_queue_push_pop]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_my_concurrent_queue_push_pop(ELEMENT_COUNT);
    }
    */
    /*
    printf("[test_std_queue_push_pop_with_boost_mutex]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_std_queue_push_pop_with_boost_mutex(ELEMENT_COUNT);
    }

    printf("[test_std_queue_push_pop_with_mutex]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_std_queue_push_pop_with_mutex(ELEMENT_COUNT);
    }

    printf("[test_std_queue_push_pop_with_spin_rw_mutex]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_std_queue_push_pop_with_spin_rw_mutex(ELEMENT_COUNT);
    }

    printf("[test_std_queue_push_pop_with_spin_mutex]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_std_queue_push_pop_with_spin_mutex(ELEMENT_COUNT);
    }



    printf("[test_std_queue_push_pop_with_recursive_mutex]\n");
    for(int i=0;i<ITERATION_COUNT;++i)
    {
        test_std_queue_push_pop_with_recursive_mutex(ELEMENT_COUNT);
    }
    */

    return 0;
}
