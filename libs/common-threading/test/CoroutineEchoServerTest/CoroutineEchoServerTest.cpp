/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "core/Prerequisite.h"
#include "threading/Coroutine.h"

using namespace zillians;

//int main()
//{
//  try
//  {
//      using boost::asio::ip::tcp;
//      using namespace boost::lambda;
//
//      boost::asio::io_service io_service;
//      tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), 54321));
//
//      const int max_clients = 100;
//      coroutine coro[max_clients];
//      std::auto_ptr<tcp::socket> socket[max_clients];
//      boost::system::error_code ec[max_clients];
//      std::size_t length[max_clients];
//      boost::array<char, 1024> data[max_clients];
//
//      // Kick off all the coroutines.
//      int n = -1;
//      for (int i = 0; i < max_clients; ++i)
//      {
//          socket[i].reset(new tcp::socket(io_service));
//          io_service.post(unlambda((var(n) = i)));
//      }
//
//      for (; io_service.run_one() > 0; n = -1)
//      {
//          if (n != -1)
//          {
//              CoroutineReenter (coro[n])
//              {
//                  CoroutineEntry:
//                  for (;;)
//                  {
//                      // Wait for a client to connect.
//                      CoroutineYield acceptor.async_accept(
//                              *socket[n],
//                              unlambda((
//                                              var(n) = n,
//                                              var(ec[n]) = boost::lambda::_1
//                                      )));
//
//                      // Echo at will.
//                      while (!ec[n])
//                      {
//                          CoroutineYield socket[n]->async_read_some(
//                                  boost::asio::buffer(data[n]),
//                                  unlambda((
//                                                  var(n) = n,
//                                                  var(ec[n]) = boost::lambda::_1,
//                                                  var(length[n]) = boost::lambda::_2
//                                          )));
//
//                          if (!ec[n])
//                          {
//                              CoroutineYield boost::asio::async_write(
//                                      *socket[n],
//                                      boost::asio::buffer(data[n], length[n]),
//                                      unlambda((
//                                                      var(n) = n,
//                                                      var(ec[n]) = boost::lambda::_1
//                                              )));
//                          }
//                      }
//
//                      // Clean up before accepting next client.
//                      socket[n]->close();
//                  }
//              }
//          }
//      }
//  }
//  catch (std::exception& e)
//  {
//      std::cerr << "Exception: " << e.what() << "\n";
//  }
//}

using boost::asio::async_write;
using boost::asio::buffer;
using boost::asio::ip::tcp;
using boost::system::error_code;
using std::size_t;

struct Session : public Coroutine
{
    boost::shared_ptr<tcp::socket> socket_;
    boost::shared_ptr<std::vector<char> > buffer_;

    Session(boost::shared_ptr<tcp::socket> socket) :
        socket_(socket), buffer_(new std::vector<char>(1024))
    { }


    void operator()(error_code ec = error_code(), size_t n = 0)
    {
        if (!ec)
        {
            CoroutineReenter (this)
            {
                CoroutineEntry:
                for (;;)
                {
                    CoroutineYield socket_->async_read_some(buffer(*buffer_), *this);
                    CoroutineYield boost::asio::async_write(*socket_, buffer(*buffer_, n), *this);
                }
            }
        }
    }

//private:
//  Session(const Session& session) //: socket_(session.socket_), buffer_(session.buffer_)
//  {
//      printf("this = %p, session = %p, copyed!\n", this, &session);
//  }
};

struct Server : public Coroutine
{
    boost::asio::io_service& io_service_;
    boost::shared_ptr<tcp::acceptor> acceptor_;
    boost::shared_ptr<tcp::socket> socket_;

    Server(boost::asio::io_service& io_service) :
        io_service_(io_service), acceptor_(new tcp::acceptor(io_service,
                tcp::endpoint(tcp::v4(), 54321)))
    { }

    void operator()(error_code ec = error_code())
    {
        CoroutineReenter (this)
        {
            CoroutineEntry:
            for (;;)
            {
                socket_.reset(new tcp::socket(io_service_));
                CoroutineYield acceptor_->async_accept(*socket_, *this);
                io_service_.post(Session(socket_));
            }
        }
    }
};

int main()
{
    boost::asio::io_service io_service;
    io_service.post(Server(io_service));
    io_service.run();
}

