/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @date Jun 6, 2010 sdk - Initial version created.
 */

#include "network/sys/LocalInterface.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <netinet/if_ether.h>
#include <arpa/inet.h>
#include <netdb.h>
#ifdef __PLATFORM_LINUX__
#include <netinet/ether.h>
#endif
namespace zillians { namespace network { namespace sys {

//////////////////////////////////////////////////////////////////////////
LocalInterface::LocalInterface()
{
	mCurrentIndex = 0;
}

LocalInterface::~LocalInterface()
{
	cleanup();
}

#define IOCTL_ERROR_MESSAGE		"error"

//////////////////////////////////////////////////////////////////////////
bool LocalInterface::enumerate()
{
	int s = socket(AF_INET, SOCK_DGRAM, 0);
	if(s < 0) return false;

	ifconf ifc;
	ifc.ifc_req = NULL;
	ifc.ifc_len = 0;

	if(ioctl(s, SIOCGIFCONF, &ifc) < 0)
		return false;

	int count = ifc.ifc_len / sizeof(ifreq);
	ifc.ifc_buf = (char*)malloc(ifc.ifc_len);

	if(ioctl(s, SIOCGIFCONF, &ifc) < 0)
		return false;

	cleanup();

	for(int i=0;i<count;++i)
	{
		ifreq* req = &ifc.ifc_req[i];
		if(req)
		{
			InterfaceInfo* info = new InterfaceInfo;
			info->name = req->ifr_name;
			info->ip_address = inet_ntoa(((sockaddr_in*)&req->ifr_addr)->sin_addr);

			if(!ioctl(s, SIOCGIFBRDADDR, req))
				info->broadcast_address = inet_ntoa(((sockaddr_in*)&req->ifr_broadaddr)->sin_addr);
			else
				info->broadcast_address = IOCTL_ERROR_MESSAGE;

#ifdef __PLATFORM_MAC__
			if(!ioctl(s, SIOCGIFADDR, req))
				info->hw_address = ether_ntoa((ether_addr*)req->ifr_addr.sa_data);
#endif
#ifdef __PLATFORM_LINUX__
			if(!ioctl(s, SIOCGIFHWADDR, req))
				info->hw_address = ether_ntoa((ether_addr*)req->ifr_hwaddr.sa_data);
#endif
			else
				info->hw_address = IOCTL_ERROR_MESSAGE;

			if(!ioctl(s, SIOCGIFNETMASK, req))
				info->netmask = inet_ntoa(((sockaddr_in*)&req->ifr_broadaddr)->sin_addr);
			else
				info->netmask = IOCTL_ERROR_MESSAGE;

			if(!ioctl(s, SIOCGIFFLAGS, req))
				info->flags = req->ifr_flags;
			else
				info->flags = 0;

			if(!ioctl(s, SIOCGIFMETRIC, req))
				info->metric = req->ifr_metric;
			else
				info->metric = 0;

			if(!ioctl(s, SIOCGIFMTU, req))
				info->mtu = req->ifr_mtu;
			else
				info->mtu = 0;

			mInterfaces.push_back(info);
		}
	}

	free(ifc.ifc_buf);

	close(s);

	mCurrentIndex = -1;

	return true;
}

bool LocalInterface::next()
{
	if(mCurrentIndex >= count() - 1)
		return false;

	++mCurrentIndex;
	mCurrentInterface = mInterfaces[mCurrentIndex];

	return true;
}

int LocalInterface::count() const
{
	return mInterfaces.size();
}

//////////////////////////////////////////////////////////////////////////
std::string LocalInterface::name()
{
	if(!mCurrentInterface)
		throw std::runtime_error("null interface");

	return mCurrentInterface->name;
}

std::string LocalInterface::hw_address()
{
	if(!mCurrentInterface)
		throw std::runtime_error("null interface");

	return mCurrentInterface->hw_address;
}

std::string LocalInterface::ip_address()
{
	if(!mCurrentInterface)
		throw std::runtime_error("null interface");

	return mCurrentInterface->ip_address;
}

std::string LocalInterface::broadcast_address()
{
	if(!mCurrentInterface)
		throw std::runtime_error("null interface");

	return mCurrentInterface->broadcast_address;
}

std::string LocalInterface::netmask()
{
	if(!mCurrentInterface)
		throw std::runtime_error("null interface");

	return mCurrentInterface->netmask;
}

short LocalInterface::flags()
{
	if(!mCurrentInterface)
		throw std::runtime_error("null interface");

	return mCurrentInterface->flags;
}

int LocalInterface::metric()
{
	if(!mCurrentInterface)
		throw std::runtime_error("null interface");

	return mCurrentInterface->metric;
}

int LocalInterface::mtu()
{
	if(!mCurrentInterface)
		throw std::runtime_error("null interface");

	return mCurrentInterface->mtu;
}

//////////////////////////////////////////////////////////////////////////
bool LocalInterface::has_flag(short flag, flags::type f)
{
#ifdef __PLATFORM_MAC__
	switch(f)
	{
	case flags::up:			return flag & IFF_UP;
	case flags::broadcast:	return flag & IFF_BROADCAST;
	case flags::debug:		return flag & IFF_DEBUG;
	case flags::loopback:	return flag & IFF_LOOPBACK;
	case flags::point_to_point:	return flag & IFF_POINTOPOINT;
	case flags::notrailers:	return flag & IFF_NOTRAILERS;
	case flags::running:	return flag & IFF_RUNNING;
	case flags::noarp:		return flag & IFF_NOARP;
	case flags::promisc:	return flag & IFF_PROMISC;
	case flags::allmulti:	return flag & IFF_ALLMULTI;
	case flags::oactive:	return flag & IFF_OACTIVE;
	case flags::simplex:	return flag & IFF_SIMPLEX;
	case flags::link1:	return flag & IFF_LINK1;
	case flags::link2:	return flag & IFF_LINK2;
	case flags::altphys:	return flag & IFF_ALTPHYS;
	case flags::multicast:	return flag & IFF_MULTICAST;
	}

#else
	switch(f)
	{
	case flags::up:			return flag & IFF_UP;
	case flags::broadcast:	return flag & IFF_BROADCAST;
	case flags::debug:		return flag & IFF_DEBUG;
	case flags::loopback:	return flag & IFF_LOOPBACK;
	case flags::point_to_point:	return flag & IFF_POINTOPOINT;
	case flags::notrailers:	return flag & IFF_NOTRAILERS;
	case flags::running:	return flag & IFF_RUNNING;
	case flags::noarp:		return flag & IFF_NOARP;
	case flags::promisc:	return flag & IFF_PROMISC;
	case flags::allmulti:	return flag & IFF_ALLMULTI;
	case flags::slave:	return flag & IFF_SLAVE;
	case flags::master:	return flag & IFF_MASTER;
	case flags::multicast:	return flag & IFF_MULTICAST;
	}
#endif
	return false;
}

char* LocalInterface::flags_to_string(short flag)
{
	static char buffer[256];
	buffer[0] = '\0';

	if(flag == 0)
		strcat(buffer, "no flags");
	else
	{
		if(has_flag(flag, flags::up))				strcat(buffer, "up ");
		if(has_flag(flag, flags::broadcast))		strcat(buffer, "broadcast ");
		if(has_flag(flag, flags::debug))			strcat(buffer, "debug ");
		if(has_flag(flag, flags::loopback))			strcat(buffer, "loopback ");
		if(has_flag(flag, flags::point_to_point))	strcat(buffer, "point_to_point ");
		if(has_flag(flag, flags::notrailers))		strcat(buffer, "notrailers ");
		if(has_flag(flag, flags::running))			strcat(buffer, "running ");
		if(has_flag(flag, flags::noarp))			strcat(buffer, "noarp ");
		if(has_flag(flag, flags::promisc))			strcat(buffer, "promisc ");
		if(has_flag(flag, flags::allmulti))			strcat(buffer, "allmulti ");
#ifdef __PLATFORM_MAC__

		if(has_flag(flag, flags::oactive))			strcat(buffer, "oactive ");
		if(has_flag(flag, flags::simplex))			strcat(buffer, "simpex ");
		if(has_flag(flag, flags::link1))			strcat(buffer, "link1 ");
		if(has_flag(flag, flags::link2))			strcat(buffer, "link2 ");
		if(has_flag(flag, flags::altphys))			strcat(buffer, "altphys ");
#else
		if(has_flag(flag, flags::slave))			strcat(buffer, "slave ");
		if(has_flag(flag, flags::master))			strcat(buffer, "master ");
#endif
		if(has_flag(flag, flags::multicast))		strcat(buffer, "multicast ");
	}

	return buffer;
}

//////////////////////////////////////////////////////////////////////////
void LocalInterface::cleanup()
{
	mCurrentInterface = NULL;
	for(std::vector<InterfaceInfo*>::iterator it = mInterfaces.begin(); it != mInterfaces.end(); ++it)
	{
		SAFE_DELETE(*it);
	}
	mInterfaces.clear();
}

} } }
