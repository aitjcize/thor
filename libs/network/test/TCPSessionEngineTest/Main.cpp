/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "core/Prerequisite.h"
#include "utility/FixPointCombinator.h"
#include "network/session/TCPSessionEngine.h"
#include "network/session/TCPSession.h"
#include "network/session/stream/SegmentedStream.h"
#include <boost/preprocessor.hpp>
#include <atomic>
#include <functional>

using namespace zillians;
using namespace zillians::network::session;

#define ENABLE_PERFORMANCE_REPORT 1
#define PERIODIC_WRITE 1

namespace po = boost::program_options;

struct HelloMessage {
	enum { TYPE = 0x123 };

	template<typename Archive>
	void serialize(Archive& ar, unsigned int version) {
		ar & m;
	}

	std::string m;
};

struct ClientContext
{
	explicit ClientContext(int id, TCPSession* session) : id(id), session(session), stream(new SegmentedStream<TCPSession>(*session)), completed_count(0), write_timer(NULL), summary_timer(NULL)
	{ }

	~ClientContext()
	{
		std::cout << "ClientContext dtor, this = " << this << std::endl;
		if(write_timer)
		{
			write_timer->cancel();
			SAFE_DELETE(write_timer);
		}
		if(summary_timer)
		{
			summary_timer->cancel();
			SAFE_DELETE(summary_timer);
		}
		session->markForDelete();
	}

	void start(bool send_or_recv)
	{
		if(send_or_recv)
		{
			message.m = "hello!!hello!!";
			write();
#if PERIODIC_WRITE
			write_timer = new boost::asio::deadline_timer(session->get_io_service(), boost::posix_time::milliseconds(write_every_n_ms));
			write_timer->async_wait(boost::bind(&ClientContext::timeout, this, _1));
#endif
#if ENABLE_PERFORMANCE_REPORT
			summary_timer = new boost::asio::deadline_timer(session->get_io_service(), boost::posix_time::milliseconds(summary_every_n_ms));
			summary_timer->async_wait(boost::bind(&ClientContext::summary, this, _1));
#endif
		}
		else
		{
			read();
#if ENABLE_PERFORMANCE_REPORT
			summary_timer = new boost::asio::deadline_timer(session->get_io_service(), boost::posix_time::milliseconds(summary_every_n_ms));
			summary_timer->async_wait(boost::bind(&ClientContext::summary, this, _1));
#endif
		}
	}

	void read()
	{
		stream->readAsync(message, [=](const boost::system::error_code& ec, const std::size_t& bytes_transferred) {
			if(ec || bytes_transferred == 0) { std::cout << "#" << id << " client read failed, ec = " << ec.message() << std::endl; delete this; }
			else
			{
				//std::cout << "client received: " << message.m << std::endl;
				++completed_count;
				read();
			}
		});
	}

	void write()
	{
		stream->writeAsync(message, [=](const boost::system::error_code& ec, const std::size_t& bytes_transferred) {
			if(ec) { std::cout << "#" << id << " client write failed, ec = " << ec.message() << std::endl; delete this; }
			else
			{
				++completed_count;
#if !PERIODIC_WRITE
				write();
#endif
			}
		});
	}

	void timeout(const boost::system::error_code& ec)
	{
		if(ec != boost::asio::error::operation_aborted)
		{
			write();
			write_timer->expires_from_now(boost::posix_time::milliseconds(write_every_n_ms));
			write_timer->async_wait(boost::bind(&ClientContext::timeout, this, _1));
		}
	}

	void summary(const boost::system::error_code& ec)
	{
		if(ec != boost::asio::error::operation_aborted)
		{
			std::cout << "#" << id << " client: " << (double)completed_count / ((double)summary_every_n_ms/1000.0) << " message/s" << std::endl;
			completed_count = 0;
			summary_timer->expires_from_now(boost::posix_time::milliseconds(summary_every_n_ms));
			summary_timer->async_wait(boost::bind(&ClientContext::summary, this, _1));
		}
	}

	static const int write_every_n_ms = 100;
	static const int summary_every_n_ms = 1000;
	int id;
	HelloMessage message;
	TCPSession* session;
	SegmentedStream<TCPSession>* stream;
	int completed_count;
	boost::asio::deadline_timer* write_timer;
	boost::asio::deadline_timer* summary_timer;
};

//////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
    po::options_description option_desc;
    po::positional_options_description positional_desc;

    option_desc.add_options()
        ("address", po::value<std::string>(), "ip address")
        ("port,p", po::value<int>(), "port")
        ("connections,c", po::value<int>(), "total connections")
        ("client", "client mode")
        ("server", "server");

    positional_desc.add("address", -1);


    po::variables_map vm;
    try
    {
        po::store(po::command_line_parser(argc, argv).options(option_desc).positional(positional_desc).run(), vm);
        po::notify(vm);
    }
    catch(const boost::program_options::error& e)
    {
        std::cerr << "failed to parse command line: " << e.what() << std::endl;
        std::cerr << option_desc << std::endl;
        return -1;
    }

    bool run_as_server_or_client = false;
    std::string address;
    int port;
    int connections = -1;

    if(vm.count("server") > 0 && vm.count("port") > 0)
    {
    	port = vm["port"].as<int>();
    	if(vm.count("connections") > 0) connections = vm["connections"].as<int>();
    	run_as_server_or_client = true;
    }
    else if(vm.count("client") > 0 && vm.count("port") > 0 && vm.count("address") > 0)
    {
    	port = vm["port"].as<int>();
    	address = vm["address"].as<std::string>();
    	if(vm.count("connections") > 0) connections = vm["connections"].as<int>();
    	run_as_server_or_client = false;
    }
    else
    {
        std::cerr << "invalid options" << std::endl;
        std::cerr << option_desc << std::endl;
        return -1;
    }


	TCPSessionEngine* engine = new TCPSessionEngine();
	std::vector<shared_ptr<Scheduler>> schedulers;
	for(int i=0;i<1;++i)
	{
		shared_ptr<Scheduler> scheduler(new Scheduler());
		engine->addScheduler(scheduler);
		schedulers.push_back(scheduler);
	}

    if(run_as_server_or_client) // as server
    {
    	engine->listen(TCPSessionEngine::protocol_t::v4(), port);

    	typedef boost::function<void(TCPSession*, const boost::system::error_code&)> function_t;
    	auto accept_handler = ycombine([=](function_t f) -> function_t {
    		return [=](TCPSession* session, const boost::system::error_code& ec) {
    			if(session)
				{
					if(ec)
					{
						std::cout << "error while accepting: " << ec.message() << std::endl;
						sleep(1000);
						session->close();
					}
					else
					{
						static int counter = 0;
						std::cout << "#" << counter << " session accepted" << std::endl;

						ClientContext* ctx = new ClientContext(counter, session);
						ctx->start(true);

						++counter;

						if(connections != -1 && counter >= connections)
							return;
					}
				}

				TCPSession* another_session = engine->createSession();
				engine->acceptAsync(another_session, boost::bind(f, another_session, _1));
    		};
    	});
//
    	accept_handler(NULL, boost::asio::error::make_error_code(boost::asio::error::in_progress));
    }
    else // as client
    {
    	typedef boost::function<void(TCPSession*, const boost::system::error_code&)> function_t;
    	auto connect_handler = ycombine([=](function_t f) -> function_t {
    		return [=](TCPSession* session, const boost::system::error_code& ec) {
				if(session)
				{
					if(ec)
					{
						std::cout << "error: " << ec.message() << std::endl;
						sleep(1001);
						session->close();
					}
					else
					{
						static int counter = 0;
						std::cout << "#" << counter << " session connected" << std::endl;

						ClientContext* ctx = new ClientContext(counter, session);
						ctx->start(false);

						++counter;

						if(connections != -1 && counter >= connections)
							return;
					}
				}

				TCPSession* another_session = engine->createSession();
				std::stringstream ss; ss << address << ":" << port;
				engine->connectAsync(another_session, TCPSessionEngine::protocol_t::v4(), ss.str(), boost::bind(f, another_session, _1));
			};
    	});

    	connect_handler(NULL, boost::asio::error::make_error_code(boost::asio::error::in_progress));
    }

    for(auto i : schedulers)
    {
    	i->wait();
    }

	return 0;
}
