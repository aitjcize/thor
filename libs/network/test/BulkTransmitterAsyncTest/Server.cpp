/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @date Jul 22, 2009 sdk - Initial version created.
 */

#include "core/Prerequisite.h"
#include "network/sys/Session.h"
#include "network/sys/SessionEngine.h"
#include "network/sys/BulkTransmitter.h"
#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <tbb/tbb_thread.h>
#include "Common.h"

using namespace zillians;
using namespace zillians::network::sys;

using boost::asio::ip::tcp;

log4cxx::LoggerPtr gLogger(log4cxx::Logger::getLogger("BulkTransmitterAsyncServer"));

TcpSessionEngine* gEngine;
BulkTransmitter<TcpSessionEngine, TcpSession>* gTransfer;

// forward declaration
void handle_listen(TcpSessionEngine* engine, const boost::system::error_code& ec);
void handle_accept(TcpSessionEngine* engine, TcpSession* session, const boost::system::error_code& ec);
void handle_session_close(TcpSessionEngine* engine, TcpSession* session);
void handle_session_error(TcpSession* session, const boost::system::error_code& ec);

void handle_listen(TcpSessionEngine* engine, const boost::system::error_code& ec)
{
	// create new session object
	TcpSession* new_session = engine->createSession();

	// start accepting connections
	engine->acceptAsync(new_session, boost::bind(handle_accept, engine, new_session, placeholders::error));
}


void handle_accept(TcpSessionEngine* engine, TcpSession* session, const boost::system::error_code& ec)
{
	if (!ec)
	{
		// let the session engine dispatch message automatically
		engine->startDispatch(
				session,
				boost::bind(handle_session_close, engine, session),
				boost::bind(handle_session_error, session, _1));

		// create new session object
		TcpSession* new_session = engine->createSession();

		// accept the next connection
		engine->acceptAsync(
				new_session,
				boost::bind(handle_accept, engine, new_session, placeholders::error));
	}
	else
	{
		LOG4CXX_INFO(gLogger, "failed to accept, error = " << ec.message());
		session->markForDeletion();
	}
}

void handle_session_close(TcpSessionEngine* engine, TcpSession* session)
{
	engine->stop();
}

void handle_session_error(TcpSession* session, const boost::system::error_code& ec)
{
	LOG4CXX_DEBUG(gLogger, "error encountered = " << ec.message());
}

void handle_dummy_message(TcpSession& session, DummyMessage& message)
{
	LOG4CXX_INFO(gLogger, "received dummy message: length = " << message.length << ", buffer size = " << message.buffer.dataSize());
	bool passed = true;
	for(uint32 i=0;i<message.length;++i)
	{
		uint32 j;
		if(message.buffer.dataSize() > 0)
		{
			message.buffer >> j;
			if(i != j)
			{
				passed = false;
				LOG4CXX_ERROR(gLogger, "error! message is corrupted at position #" << i);
				break;
			}
		}
		else
		{
			passed = false;
			LOG4CXX_ERROR(gLogger, "error! message is corrupted, insufficient size");
			break;
		}
	}

	if(passed)
	{
		LOG4CXX_INFO(gLogger, "verification passed");
	}

	//gEngine->stop();
}

//////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
	log4cxx::BasicConfigurator::configure();

	try
	{
		if (argc != 2)
		{
			std::cerr << "Usage: " << argv[0] << " <port>\n";
			return 1;
		}

		int port = atoi(argv[1]);

		boost::asio::io_service ios;

		gEngine = new TcpSessionEngine(&ios);
		gTransfer = new BulkTransmitter<TcpSessionEngine, TcpSession>(*gEngine);

		gEngine->getDispatcher().bind<DummyMessage>(
				boost::bind(handle_dummy_message,
						placeholders::dispatch::source_ref,
						placeholders::dispatch::message_ref));

		gEngine->listenAsync(tcp::v4(), port, boost::bind(handle_listen, gEngine, placeholders::error));

		gEngine->run();

		SAFE_DELETE(gTransfer);
		SAFE_DELETE(gEngine);
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}
