/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class DeductionWrapper<T> { }

@static_test { resolution="CheckDeductionRule<X,Y,Z>" }
class CheckDeductionRule<X, Y, Z>
{
}

@static_test { resolution="CheckDeductionRule<X,Y:DeductionWrapper<X>,Z>" }
class CheckDeductionRule<X, Y:DeductionWrapper<X>, Z>
{
}

@static_test { resolution="CheckDeductionRule<X,Y:DeductionWrapper<X>,Z:DeductionWrapper<X>>" }
class CheckDeductionRule<X, Y:DeductionWrapper<X>, Z:DeductionWrapper<X> >
{
}

@static_test { resolution="CheckDeductionRule<X,Y:DeductionWrapper<X>,Z:DeductionWrapper<Y>>" }
class CheckDeductionRule<X, Y:DeductionWrapper<X>, Z:DeductionWrapper<Y> >
{
}

function ForTest() : void
{
	@static_test { expect_resolution="CheckDeductionRule<X,Y,Z>" }
	var a:CheckDeductionRule<int32,int32,int32>;
	
	@static_test { expect_resolution="CheckDeductionRule<X,Y:DeductionWrapper<X>,Z>" }
	var b:CheckDeductionRule<int32,DeductionWrapper<int32>,int32>;

	@static_test { expect_resolution="CheckDeductionRule<X,Y:DeductionWrapper<X>,Z:DeductionWrapper<X>>" }
	var c:CheckDeductionRule<int32,DeductionWrapper<int32>,DeductionWrapper<int32> >;

	@static_test { expect_resolution="CheckDeductionRule<X,Y:DeductionWrapper<X>,Z:DeductionWrapper<Y>>" }
	var d:CheckDeductionRule<int32,DeductionWrapper<int32>,DeductionWrapper<DeductionWrapper<int32> > >;
}
