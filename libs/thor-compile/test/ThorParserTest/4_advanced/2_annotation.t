/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@tag { key = 13, key2 = "aaa" }
function f():void
{
}

@tag { key = 17, key2 = "bbb" }
class my_class
{
    @tag { key = 19, key2 = "ccc" }
    var x:int32;

    @tag { key = 23, key2 = "ddd" }
    function f():void
    {
    }
}

@tag { key = 29, key2 = "eee" }
@tag2 { key = 31, key2 = "fff" }
function g():void
{
}

@tag { key = 37, key2 = { key = 41, key2 = 43 } }
function h():void
{
}

@tag { key = 47, key2 = "ggg" }
interface iface
{
    function f():void;
}
