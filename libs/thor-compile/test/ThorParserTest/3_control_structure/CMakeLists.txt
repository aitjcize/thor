#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#
zillians_add_complex_test(
    TARGET thor-parser-test-auto-control-structure
    SHELL ${CMAKE_CURRENT_SOURCE_DIR}/../test.sh ${THOR_COMPILER} 3
            ${CMAKE_CURRENT_SOURCE_DIR}/0_if_elif_else.t
            ${CMAKE_CURRENT_SOURCE_DIR}/1_switch_case_default.t
            ${CMAKE_CURRENT_SOURCE_DIR}/2_while_dowhile.t
            ${CMAKE_CURRENT_SOURCE_DIR}/3_foreach.t
            ${CMAKE_CURRENT_SOURCE_DIR}/4_for.t
    DEPENDS ${THOR_COMPILER}
    )

zillians_add_complex_test(
    TARGET thor-parser-test-control-structure
    SHELL ${CMAKE_CURRENT_SOURCE_DIR}/../test.sh ${THOR_COMPILER} 0
            ${CMAKE_CURRENT_SOURCE_DIR}/0_if_elif_else.t
            ${CMAKE_CURRENT_SOURCE_DIR}/1_switch_case_default.t
            ${CMAKE_CURRENT_SOURCE_DIR}/2_while_dowhile.t
            ${CMAKE_CURRENT_SOURCE_DIR}/3_foreach.t
            ${CMAKE_CURRENT_SOURCE_DIR}/4_for.t
    DEPENDS ${THOR_COMPILER}
    )

zillians_add_complex_test(
    TARGET thor-parser-test-ast-control-structure
    SHELL ${CMAKE_CURRENT_SOURCE_DIR}/../test.sh ${THOR_COMPILER} 1
            ${CMAKE_CURRENT_SOURCE_DIR}/0_if_elif_else.t
            ${CMAKE_CURRENT_SOURCE_DIR}/1_switch_case_default.t
            ${CMAKE_CURRENT_SOURCE_DIR}/2_while_dowhile.t
            ${CMAKE_CURRENT_SOURCE_DIR}/3_foreach.t
            ${CMAKE_CURRENT_SOURCE_DIR}/4_for.t
    DEPENDS ${THOR_COMPILER}
    )

zillians_add_complex_test(
    TARGET thor-parser-test-xform-control-structure
    SHELL ${CMAKE_CURRENT_SOURCE_DIR}/../test.sh ${THOR_COMPILER} 2
            ${CMAKE_CURRENT_SOURCE_DIR}/0_if_elif_else.t
            ${CMAKE_CURRENT_SOURCE_DIR}/1_switch_case_default.t
            ${CMAKE_CURRENT_SOURCE_DIR}/2_while_dowhile.t
            ${CMAKE_CURRENT_SOURCE_DIR}/3_foreach.t
            ${CMAKE_CURRENT_SOURCE_DIR}/4_for.t
    DEPENDS ${THOR_COMPILER}
    )

zillians_add_test_to_subject(SUBJECT thor-parser-test-auto-all TARGET thor-parser-test-auto-control-structure)
zillians_add_test_to_subject(SUBJECT thor-parser-test-all TARGET thor-parser-test-control-structure)
zillians_add_test_to_subject(SUBJECT thor-parser-test-ast-all TARGET thor-parser-test-ast-control-structure)
