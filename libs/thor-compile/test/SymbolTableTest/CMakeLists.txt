#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

ADD_EXECUTABLE(ThorResolutionTest_SymbolTableTest SymbolTableTest.cpp)

TARGET_LINK_LIBRARIES(ThorResolutionTest_SymbolTableTest
    thor-common-core
    thor-language-tree
    )

zillians_add_simple_test(TARGET ThorResolutionTest_SymbolTableTest)

zillians_add_complex_test(
    TARGET thor-symbol-resolution-test-dupe-name
    DEPENDS thor-compile
    SHELL ${THOR_COMPILER} --mode-resolution-test --enable-static-test --no-system --root-dir=${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/dupe-name.t
)

zillians_add_complex_test(
    TARGET thor-symbol-resolution-test-shadow-package
    DEPENDS thor-compile
    SHELL ${THOR_COMPILER} --mode-resolution-test --enable-static-test --no-system --root-dir=${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/shadow-package.t
                                                                                                                               ${CMAKE_CURRENT_SOURCE_DIR}/xxx/b.t
)

zillians_add_test_to_subject(SUBJECT thor-resolution-test TARGET ThorResolutionTest_SymbolTableTest)
zillians_add_test_to_subject(SUBJECT thor-resolution-test TARGET thor-symbol-resolution-test-dupe-name)
zillians_add_test_to_subject(SUBJECT thor-resolution-test TARGET thor-symbol-resolution-test-shadow-package)
