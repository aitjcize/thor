/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@static_test { specialization_of = "" } function func         (                  ): void {}
@static_test { specialization_of = "" } function func         (a: int32          ): void {}
@static_test { specialization_of = "" } function func         (b: int32, c: int8 ): void {}
@static_test { specialization_of = "" } function func<T      >(                  ): void {}
@static_test { specialization_of = "" } function func<T      >(a: T              ): void {}
@static_test { specialization_of = "" } function func<T      >(b: T    , c: int8 ): void {}
@static_test { specialization_of = "" } function func<T, U   >(                  ): void {}
@static_test { specialization_of = "" } function func<T, U   >(a: T              ): void {}
@static_test { specialization_of = "" } function func<T, U   >(b: T    , C: U    ): void {}
@static_test { specialization_of = "" } function func<T, U, V>(                  ): void {}
@static_test { specialization_of = "" } function func<T, U, V>(a: T              ): void {}
@static_test { specialization_of = "" } function func<T, U, V>(b: U    , C: V    ): void {}

@static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_NAME_PREV"                                                                          } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_NAME_PREV"                                                                          } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function func_d():void" } } }
function func_d(): void {}

@static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_NAME"                       , parameters = { id = "func_d"                        } } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function func_d():void" } } }
function func_d(): void {}

@static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_NAME"                       , parameters = { id = "func_d"                        } } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function func_d():int8" } } }
function func_d(): int8 {}

function call_redefined_function(): void
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE"       , parameters = { id = "func_d()" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO"       , parameters = { id = "func_d"   } } }
    func_d();
}
