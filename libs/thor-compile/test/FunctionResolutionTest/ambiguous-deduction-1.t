/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class cls<T> {}

@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function do_call<T,U>(T,U):void" } } }function do_call<T, U>(t:             T     , u:         U   ): void {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function do_call<T,U>(cls<_:T>,cls<_:cls<_:U>>):void" } } }function do_call<T, U>(t:         cls<T>    , u: cls<cls<U> >): void {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function do_call<T,U>(cls<_:cls<_:cls<_:T>>>,cls<_:U>):void" } } }function do_call<T, U>(t: cls<cls<cls<T> > >, u:     cls<U>  ): void {}

function main(): void {
    var vClsClsClsI32 : cls<cls<cls<int32> > >;

    @static_test { expect_resolution = "" }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "do_call(class cls<T:cls<T:cls<T:int32>>>, class cls<T:cls<T:cls<T:int32>>>)" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "do_call" } } }
    do_call(vClsClsClsI32, vClsClsClsI32);
}
