/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
//////////////////////////////////////////////////////////////////////////////
// some classes
//////////////////////////////////////////////////////////////////////////////

    class Foo {}
    class Bar {}
    class Base<T> {}
    class Base<T:int32> {}
    class Derived<T> extends Base<T> {}
    class Extended<T> extends Derived<T> {}

//////////////////////////////////////////////////////////////////////////////
// 1 template functions
//////////////////////////////////////////////////////////////////////////////

    // 1 nested
    @static_test { resolution="f<T>(T)"                  } function f<T>(a:T) : void {}
    @static_test { resolution="f<T>(Base<T>)"            } function f<T>(a:Base<T>) : void {}
    @static_test { resolution="f(Base<int32>)"           } function f(a:Base<int32>) : void {}
    @static_test { resolution="f<T>(Base<Base<T>>)"      } function f<T>(a:Base<Base<T> >) : void {}
    @static_test { resolution="f(Base<Base<int32>>)"     } function f(a:Base<Base<int32> >) : void {}
    @static_test { resolution="f(Base<Base<Foo>>)"       } function f(a:Base<Base<Foo> >) : void {}
    @static_test { resolution="f<T>(Derived<Base<Foo>>)" } function f<T>(a:Derived<Base<Bar> >) : void {}

    // 1 nested
    //@static_test { resolution="f<T>(T)"                  } function f<T>(a:T) : void {}
    @static_test { resolution="g<T>(Base<T>)"            } function g<T>(a:Base<T>) : void {}
    @static_test { resolution="g(Base<int32>)"           } function g(a:Base<int32>) : void {}
    @static_test { resolution="g<T>(Base<Base<T>>)"      } function g<T>(a:Base<Base<T> >) : void {}
    @static_test { resolution="g(Base<Base<int32>>)"     } function g(a:Base<Base<int32> >) : void {}
    @static_test { resolution="g(Base<Base<Foo>>)"       } function g(a:Base<Base<Foo> >) : void {}
    @static_test { resolution="g(Derived<Base<Bar>>)"    } function g(a:Derived<Base<Bar> >) : void {}

//////////////////////////////////////////////////////////////////////////////
// main
//////////////////////////////////////////////////////////////////////////////

function main() : void
{
    var vint8         : int8              ;
    
    var vBint8        : Base<int8>        ;
    var vBint32       : Base<int32>       ;
    var vBFoo         : Base<Foo>         ;
    
    var vBBint8       : Base<Base<int8> > ;
    var vBBint32      : Base<Base<int32> >;
    var vBBFoo        : Base<Base<Foo> >  ;
    var vBBBar        : Base<Base<Bar> >  ;
    
    var vDint8        : Derived<int8>     ;
    var vDint32       : Derived<int32>    ;
    var vDFoo         : Derived<Foo>      ;

    var vDBint8       : Derived<Base<int8> > ;
    var vDBint32      : Derived<Base<int32> >;
    var vDBFoo        : Derived<Base<Foo> >  ;
    var vDBBar        : Derived<Base<Bar> >  ;

    var vEBint8       : Extended<Base<int8> > ;
    var vEBint32      : Extended<Base<int32> >;
    var vEBFoo        : Extended<Base<Foo> >  ;
    var vEBBar        : Extended<Base<Bar> >  ;

    // 1 nested
    @static_test { expect_resolution="f<T>(T)" } f(vint8);
    
    @static_test { expect_resolution="f<T>(Base<T>)" }       f(vBint8);
    @static_test { expect_resolution="f(Base<int32>)" }      f(vBint32);
    @static_test { expect_resolution="f<T>(Base<T>)" }       f(vBFoo);
    
    @static_test { expect_resolution="f<T>(Base<Base<T>>)" }       f(vBBint8);
    @static_test { expect_resolution="f(Base<Base<int32>>)" }      f(vBBint32);
    @static_test { expect_resolution="f(Base<Base<Foo>>)" }        f(vBBFoo);
    
    @static_test { expect_resolution="g<T>(Base<T>)" }       g(vDint8);
    @static_test { expect_resolution="g(Base<int32>)" }      g(vDint32);
    @static_test { expect_resolution="g<T>(Base<T>)" }       g(vDFoo);

    @static_test { expect_resolution="g<T>(Base<Base<T>>)" }       g(vDBint8);
    @static_test { expect_resolution="g(Base<Base<int32>>)" }      g(vDBint32);
    @static_test { expect_resolution="g(Base<Base<Foo>>)" }        g(vDBFoo);

    @static_test { expect_resolution="g<T>(Base<Base<T>>)" }       g(vEBint8);
    @static_test { expect_resolution="g(Base<Base<int32>>)" }      g(vEBint32);
    @static_test { expect_resolution="g(Base<Base<Foo>>)" }        g(vEBFoo);

    @static_test { expect_resolution="g(Derived<Base<Bar>>)" }     g(vEBBar); 
}


