/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
//////////////////////////////////////////////////////////////////////////////
// some classes
//////////////////////////////////////////////////////////////////////////////

    class Foo {}
    class Bar {}
    class Base {}
    class Derived extends Base {}
    class Extended extends Derived {}
    class Complex<T> {}

//////////////////////////////////////////////////////////////////////////////
// 1 template functions
//////////////////////////////////////////////////////////////////////////////

    // full generalized
    @static_test { resolution="f<T>(T)"                               } function f<T>(a:T) : void {}
    @static_test { resolution="f<T>(T,T)"                             } function f<T>(a:T, b:T) : void {}
    @static_test { resolution="f<T>(T,T,T)"                           } function f<T>(a:T, b:T, c:T) : void {}
    @static_test { resolution="f2<T>(T,int8)"                         } function f2<T>(a:T, b:int8) : void {}
    @static_test { resolution="f2<T>(T,int32)"                        } function f2<T>(a:T, b:int32) : void {}

    // specilaized
    @static_test { resolution="f<int8>(T)"                            } function f<T:int8>(a:T) : void {}
    @static_test { resolution="f<Foo>(T)"                             } function f<T:Foo>(a:T) : void {}
    @static_test { resolution="f<Base>(T)"                            } function f<T:Base>(a:T) : void {}
    @static_test { resolution="f<Complex<int32>>(T)"                  } function f<T:Complex<int32> >(a:T) : void {}
    @static_test { resolution="f<Complex<int64>>(T)"                  } function f<T:Complex<int64> >(a:T) : void {}
    @static_test { resolution="f2<int32>(T,int8)"                     } function f2<T:int32>(a:T, b:int8) : void {}
    @static_test { resolution="f2<int32>(T,int32)"                    } function f2<T:int32>(a:T, b:int32) : void {}

    // non-template
    @static_test { resolution="f(int32)"                              } function f(a:int32) : void {}

    // g for and not deduceded
    @static_test { resolution="g<T>(T,T)" }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function g<T>(T,T):void" } } }
    function g<T>(a:T, b:T) : void {}
    @static_test { resolution="g<int32>(T,T)" }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function g<T:int32>(T,T):void [with T=int32,T=int32,T=int32]" } } }
    function g<T:int32>(a:T, b:T) : void {}
    @static_test { resolution="g<Foo>(T,T)" }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function g<T:Foo>(T,T):void [with T=Foo,T=Foo,T=Foo]" } } }
    function g<T:Foo>(a:T, b:T) : void {}

    // g for and not deduceded
    @static_test { resolution="g2<T>(T,T,int8)" }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function g2<T>(T,T,int8):void" } } }
    function g2<T>(a:T, b:T, c:int8) : void {}
    @static_test { resolution="g2<int32>(T,T,int8)"                   }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function g2<T:int32>(T,T,int8):void [with T=int32,T=int32,T=int32]" } } }
    function g2<T:int32>(a:T, b:T, c:int8) : void {}
    @static_test { resolution="g2<Foo>(T,T,int8)" }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function g2<T:Foo>(T,T,int8):void [with T=Foo,T=Foo,T=Foo]" } } }
    function g2<T:Foo>(a:T, b:T, c:int8) : void {}

    // no-parameter template function
    @static_test { resolution="h<T>()"                                } function h<T>() : void {}
    @static_test { resolution="h<int8>()"                             } function h<T:int8>() : void {}
    @static_test { resolution="h<Foo>()"                              } function h<T:Foo>() : void {}

    // one type explicit specified, one deduced by param
    @static_test { resolution="c<T,U>(U)"                             } function i<T,U>(v:U) : T {var result : T; return result;}

    // z for function argument mismatch
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z<T>(T):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z<T>(T):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z<T>(T):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z<T>(T):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z<T>(T):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z<T>(T):void" } } }
    function z<T>(a:T) : void {}

//////////////////////////////////////////////////////////////////////////////
// main
//////////////////////////////////////////////////////////////////////////////

    function main() : void
    {
        var vint8      : int8              ;
        var vint16     : int16             ;
        var vint32     : int32             ;
        var vint64     : int64             ;
        var vfloat32   : float32           ;
        var vfloat64   : float64           ;
        var vFoo       : Foo               ;
        var vBar       : Bar               ;
        var vBase      : Base              ;
        var vDerived   : Derived           ;
        var vExtended  : Extended          ;
        var vComplex   : Complex<int32>    ;

        // full match after deduction
        @static_test { expect_resolution="f<T>(T)" } f(vint16);
        @static_test { expect_resolution="f<T>(T)" } f(vfloat64);
        @static_test { expect_resolution="f<T>(T)" } f(vBar);

        // specialization is better than generalization
        @static_test { expect_resolution="f<int8>(T)" } f(vint8);
        @static_test { expect_resolution="f<Foo>(T)" } f(vFoo);
        @static_test { expect_resolution="f<Complex<int32>>(T)" } f(vComplex);

        // specialization is better than generalization & exact is better thean promotion
        @static_test { expect_resolution="f2<int32>(T,int8)" } f2(vint32, vint8);
        // specialization is better than generalization & promotion is better than standard conversion
        @static_test { expect_resolution="f2<int32>(T,int32)" } f2(vint32, vint16);

        // non-template is better than template
        @static_test { expect_resolution="f(int32)" } f(vint32);

        // can not deduced
        @static_test { expect_resolution="" }
        @static_test { expect_message={level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="g(int32, int64)"} } }
        @static_test { expect_message={level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="g"} } }
        g(vint32, vint64);

        // fully qualified template instantiation
        @static_test { expect_resolution="g<T>(T,T)" } g<float64>(vfloat64, vfloat64);
        @static_test { expect_resolution="g<T>(T,T)" } g<Bar>(vBar, vBar);
        @static_test { expect_resolution="g<int32>(T,T)" } g<int32>(vint32, vint64);
        @static_test { expect_resolution="g<Foo>(T,T)" } g<Foo>(vFoo, vFoo);

        @static_test { expect_resolution="g2<int32>(T,T,int8)" } g2<int32>(vint32, vint64, vint16);
        @static_test { expect_resolution="g2<Foo>(T,T,int8)" } g2<Foo>(vFoo, vFoo, vint16);
        @static_test { expect_resolution="" }
        @static_test { expect_message={level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="g2<_:Foo>(class Foo, int8, int8)"} } }
        @static_test { expect_message={level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="g2<_:Foo>"} } }
        g2<Foo>(vFoo, vint8, vint8);

        // no-parameter template function
        @static_test { expect_resolution="h<T>()" } h<float64>();
        @static_test { expect_resolution="h<T>()" } h<int16>();
        @static_test { expect_resolution="h<int8>()" } h<int8>();
        @static_test { expect_resolution="h<Foo>()" } h<Foo>();

        // one type explicit specified, one deduced by param
        @static_test { expect_resolution="c<T,U>(U)" } i<int64>(vint32);

        // argument mismatch
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z<_:int32>()"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z<_:int32>"} } } z<int32>();
                                                                                                                          z<int32>(vint32);
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z<_:int32>(int32, int32)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z<_:int32>"} } } z<int32>(vint32, vint32);
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z<_:int32>(int32, int32, int32)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z<_:int32>"} } } z<int32>(vint32, vint32, vint32);
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z<_:int32>(class Foo)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z<_:int32>"} } } z<int32>(vFoo);
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z<_:int32>(class Foo, class Foo)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z<_:int32>"} } } z<int32>(vFoo, vFoo);
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z<_:int32>(class Foo, class Foo, class Foo)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z<_:int32>"} } } z<int32>(vFoo, vFoo, vFoo);

    }
