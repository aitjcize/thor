/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import pl;

class cls {}
enum enu {e1, e2}
typedef cls type_def;
class class_type {}
typedef class_type typedef_type;
enum enum_type { dummy }

@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function test_call<T>(T):void" } } }
function test_call<T>(t: T    ): void {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function test_call(int32):void" } } }
function test_call   (t: int32): void {}

function test_package_not_expression(): void
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } test_call<int32>(pl);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } test_call       (pl);

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } cast<int32>(pl);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } isa<int32>(pl);

                                                                                                                        1 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } pl;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } 1  + pl;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } pl + 2 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } 1  - pl;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } pl - 2 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } 1  * pl;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } pl * 2 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } 1  / pl;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } pl / 2 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } 1  % pl;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } pl % 2 ;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } pl += 1 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } pl -= 1 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } pl *= 1 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } pl /= 1 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } pl %= 1 ;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } 1  + 2  + pl;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } 1  + pl + 3 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } pl + 2  + 3 ;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } 1  + pl + pl;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } pl + 2  + pl;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } pl + pl + 3 ;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } (pl += 1) += 2 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } (pl -= 1) += 2 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } (pl *= 1) += 2 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } (pl /= 1) += 2 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "pl"} } } (pl %= 1) += 2 ;
}

function test_class_not_expression(): void
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "test_call<_:int32>(class class_type)" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "test_call<_:int32>"      } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } test_call<int32>(class_type);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } test_call       (class_type);

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } cast<int32>(class_type);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } isa<int32>(class_type);

                                                                                                                        1 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } class_type;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } 1   + class_type;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } class_type + 2  ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } 1   - class_type;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } class_type - 2  ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } 1   * class_type;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } class_type * 2  ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } 1   / class_type;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } class_type / 2  ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } 1   % class_type;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } class_type % 2  ;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } class_type += 1 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } class_type -= 1 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } class_type *= 1 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } class_type /= 1 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } class_type %= 1 ;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } 1   + 2   + class_type;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } 1   + class_type + 3  ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } class_type + 2   + 3  ;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } 1   + class_type + class_type;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } class_type + 2   + class_type;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } class_type + class_type + 3  ;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } (class_type += 1) += 2 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } (class_type -= 1) += 2 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } (class_type *= 1) += 2 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } (class_type /= 1) += 2 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } } (class_type %= 1) += 2 ;
}

function template_func<typename_type>(): void
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } }
    class_type + 1;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "class_type"} } }
    1 + class_type;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "enum_type"} } }
    enum_type + 2;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "enum_type"} } }
    2 + enum_type;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "typedef_type"} } }
    typedef_type + 3;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "typedef_type"} } }
    3 + typedef_type;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "typename_type"} } }
    typename_type + 4;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "typename_type"} } }
    4 + typename_type;
} 

// a caller force template to be instantiated
function caller(): void
{
    template_func<int32>();
}

function test_enum_not_expression(): void
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "enu"} } } enu;
}

function test_typedef_not_expression(): void
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "type_def"} } } type_def;
}

function test_typename_not_expression<T>(): void
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION", parameters = { name = "T"} } } T;
}

function typename_function_instanciater(): void
{
    test_typename_not_expression<int32>();
}
