/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@static_test { resolution="Foo" }
class Foo
{
    @static_test { resolution="Foo.vint32" } public var vint32 : int32;
    @static_test { resolution="Foo.vFoo"   } public var vFoo   : Foo  ;
}

@static_test { resolution="Bar<T>" }
class Bar<T>
{
    @static_test { resolution="Bar<T>.vint16" } public var vint16 : int16 ;
    @static_test { resolution="Bar<T>.vBar"   } public var vBar   : Bar<T>;
}

function func_1   () : void {}
function func_2<T>() : void {}

function main() : void
{
    var vint32 : int32     ;
    var vFoo   : Foo       ;
    var vBar   : Bar<int32>;

    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vint32" } } } func_1.vint32;
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vFoo"   } } } func_1.vFoo;
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vBar"   } } } func_1.vBar;

    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vint32" } } } func_2<int32>.vint32;
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vFoo"   } } } func_2<int32>.vFoo;
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vBar"   } } } func_2<int32>.vBar;

    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vint32" } } } vint32.vint32;
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vFoo"   } } } vint32.vFoo;
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vBar"   } } } vint32.vBar;

    @static_test { expect_resolution="Foo.vint32"    } @static_test { expect_type="int32"        }                  vFoo.vint32;
    @static_test { expect_resolution="Foo.vFoo"      } @static_test { expect_type="Foo"          }                  vFoo.vFoo;
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vBar"   } } } vFoo.vBar;

    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vint32" } } } vFoo.vint32.vint32;
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vFoo"   } } } vFoo.vint32.vFoo;
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vBar"   } } } vFoo.vint32.vBar;

    @static_test { expect_resolution="Foo.vint32"    } @static_test { expect_type="int32"        }                  vFoo.vFoo.vint32;
    @static_test { expect_resolution="Foo.vFoo"      } @static_test { expect_type="Foo"          }                  vFoo.vFoo.vFoo;
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vBar"   } } } vFoo.vFoo.vBar;

    @static_test { expect_resolution="Bar<T>.vint16" } @static_test { expect_type="int32"        }                  vBar.vint16;
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vFoo"   } } } vBar.vFoo;
    @static_test { expect_resolution="Bar<T>.vBar"   } @static_test { expect_type="Bar<int32>"   }                  vBar.vBar;

    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vint16" } } } vBar.vint16.vint16;
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vFoo"   } } } vBar.vint16.vFoo;
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vBar"   } } } vBar.vint16.vBar;

    @static_test { expect_resolution="Bar<T>.vint16" } @static_test { expect_type="int32"        }                  vBar.vBar.vint16;
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="vFoo"   } } } vBar.vBar.vFoo;
    @static_test { expect_resolution="Bar<T>.vBar"   } @static_test { expect_type="Foo"          }                  vBar.vBar.vBar;
}
