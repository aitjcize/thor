/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <type_traits>
#include <boost/mpl/at.hpp>
#include <boost/mpl/insert.hpp>
#include <boost/mpl/is_sequence.hpp>
#include <boost/mpl/map.hpp>
#include <boost/mpl/pair.hpp>
#include <boost/mpl/push_back.hpp>
#include <boost/mpl/size.hpp>
#include <boost/utility/enable_if.hpp>
#include "language/ThorCompiler.h"
#include "language/stage/transformer/ConstantFoldingStage.h"
#include "language/stage/parser/ThorParserStage.h"
#include "language/stage/transformer/FlowSplitStage.h"
#include "language/stage/transformer/RestructureStage0.h"
#include "language/stage/transformer/RestructureStage1.h"
#include "language/stage/transformer/ResolutionStage.h"
#include "language/stage/transformer/IdManglingStage.h"
#include "language/stage/transformer/ImplicitConversionStage.h"
#include "language/stage/transformer/NameManglingStage.h"
#include "language/stage/CompoundStage.h"
#include "language/stage/serialization/ASTSerializationStage.h"
#include "language/stage/serialization/BundleASTMergeStage.h"
#include "language/stage/verifier/SemanticVerificationStage0.h"
#include "language/stage/verifier/SemanticVerificationStage1.h"
#include "language/stage/verifier/SemanticVerificationStage2.h"
#include "language/stage/verifier/StaticTestVerificationStage.h"
#include "language/stage/compiler/InitConfigurationStage.h"

using namespace zillians::language::stage;

namespace zillians { namespace language {

namespace {

template<typename Entry, typename Enable = void>
struct StageCollectorEntryHelper
{
    typedef Entry             StageType;
    typedef boost::mpl::void_ TagType;
};

template<typename Entry>
struct StageCollectorEntryHelper<Entry, typename boost::disable_if_c<std::is_base_of<Stage, Entry>::value>::type>
{
    typedef typename Entry::first  StageType;
    typedef typename Entry::second TagType;
};

template<typename Begin, typename End>
struct StageCollectorEntry : StageCollectorEntry<Begin, typename boost::mpl::prior<End>::type>
{
    typedef StageCollectorEntry<Begin, typename boost::mpl::prior<End>::type>                ParentEntry;
    typedef typename boost::mpl::deref<typename boost::mpl::prior<End>::type>::type          CurrentEntry;

    typedef typename StageCollectorEntryHelper<CurrentEntry>::StageType                      StageType;
    typedef typename StageCollectorEntryHelper<CurrentEntry>::TagType                        TagType;

    typedef typename boost::mpl::push_back<typename ParentEntry::AllStages, StageType>::type AllStages;

    typedef typename boost::mpl::if_<
        typename boost::mpl::is_not_void_<TagType>::type,
        typename boost::mpl::insert<
            typename ParentEntry::TaggedStages,
            typename boost::mpl::pair<
                TagType,
                typename boost::mpl::push_back<
                    AllStages,
                    StaticTestVerificationStage
                >::type
            >::type
        >::type,
        typename ParentEntry::TaggedStages
    >::type                                                                                  TaggedStages;
};

template<typename End>
struct StageCollectorEntry<End, End>
{
    typedef boost::mpl::vector<> AllStages;
    typedef boost::mpl::map<>    TaggedStages;
};

template<typename Stages>
struct StageCollector : StageCollectorEntry<typename boost::mpl::begin<Stages>::type, typename boost::mpl::end<Stages>::type>
{
};


}

ThorCompiler::ThorCompiler() : stage::StageBuilder(true)
{
    typedef StageCollector<
        boost::mpl::vector<
                              InitConfigurationStage    ,
            boost::mpl::pair< ThorParserStage     , struct TagParse     >,
                              BundleASTMergeStage       ,
            boost::mpl::pair< SemanticVerificationStage0, struct TagS0        >,
            boost::mpl::pair< RestructureStage0         , struct TagXform     >,
            boost::mpl::pair< ResolutionStage           , struct TagResolution>,
            boost::mpl::pair< ImplicitConversionStage   , struct TagCast      >,
                              CompoundStage< RestructureStage1, ResolutionStage, ImplicitConversionStage >,
            boost::mpl::pair< ConstantFoldingStage      , struct TagConstFold >,
            boost::mpl::pair< SemanticVerificationStage1, struct TagS1        >,
                              CompoundStage<
                                  FlowSplitStage,
                                  RestructureStage0,
                                  CompoundStage< ResolutionStage, ImplicitConversionStage, RestructureStage1 >
                              >,
                              SemanticVerificationStage2,
                              ConstantFoldingStage      ,
            boost::mpl::pair< NameManglingStage         , struct TagMangling  >,
                              IdManglingStage           ,
                              ASTSerializationStage
        >
    > CompilerStages;

    addDefaultMode<CompilerStages::AllStages>();

    addMode<boost::mpl::at<CompilerStages::TaggedStages, struct TagParse     >::type>("mode-parse"            , "for syntax check stage");
    addMode<boost::mpl::at<CompilerStages::TaggedStages, struct TagConstFold >::type>("mode-constant-folding" , "for constant folding stage");
    addMode<boost::mpl::at<CompilerStages::TaggedStages, struct TagS0        >::type>("mode-semantic-verify-0", "for semantic verification stage 0");
    addMode<boost::mpl::at<CompilerStages::TaggedStages, struct TagXform     >::type>("mode-xform"            , "for debugging transform stage");
    addMode<boost::mpl::at<CompilerStages::TaggedStages, struct TagResolution>::type>("mode-resolution-test"  , "for resolution test");
    addMode<boost::mpl::at<CompilerStages::TaggedStages, struct TagCast      >::type>("mode-implicit-cast"    , "for implicit cast");
    addMode<boost::mpl::at<CompilerStages::TaggedStages, struct TagS1        >::type>("mode-semantic-verify-1", "for semantic verification stage 1");
    addMode<boost::mpl::at<CompilerStages::TaggedStages, struct TagMangling  >::type>("mode-mangling"         , "for debugging resolution stage");
}

ThorCompiler::~ThorCompiler()
{ }

} }
