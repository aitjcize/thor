/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/range/adaptor/filtered.hpp>
#include <boost/algorithm/string/join.hpp>

#include <llvm/Config/config.h>
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 3
    #include <llvm/IR/Module.h>
    #include <llvm/IR/LLVMContext.h>
    #include <llvm/IRReader/IRReader.h>
#else
    #include <llvm/Module.h>
    #include <llvm/LLVMContext.h>
    #include <llvm/Support/IRReader.h>
#endif
#include <llvm/PassManager.h>
#include <llvm/Linker.h>
#include <llvm/Target/TargetMachine.h>
#include <llvm/Bitcode/BitstreamWriter.h>
#include <llvm/Bitcode/ReaderWriter.h>
#include <llvm/ADT/Triple.h>
#include <llvm/Support/Path.h>
#include <llvm/Support/ToolOutputFile.h>
#include <llvm/Support/SourceMgr.h>
#include <llvm/Support/TargetRegistry.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Support/FormattedStream.h>
#include <llvm/Support/Host.h>
#include <llvm/Support/Program.h>

#include "utility/Filesystem.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/stage/linker/ThorLinkerStage.h"
#include "language/stage/serialization/detail/ASTSerializationHelper.h"
#include "language/stage/linker/visitor/CudaObjectSkeletonGeneratorVisitor.h"
#include "language/stage/generator/detail/CppSourceGenerator.h"
#include "language/ThorConfiguration.h"


static std::string generate_dependence_include(const zillians::language::ThorBuildConfiguration& config)
{
    std::string result;
    for(auto& bundle : config.manifest.deps.bundles)
    {
        result += " -I" + (config.extract_bundle_path / bundle.name / "src").string();
    }
    return result;
}

namespace zillians { namespace language { namespace stage {

ThorLinkerStage::ThorLinkerStage() : verbose(false)
{
}

ThorLinkerStage::~ThorLinkerStage()
{ }

const char* ThorLinkerStage::name()
{
    return "linker_stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> ThorLinkerStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options()
        ("verbose,v", "verbose mode");

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options();

    return std::make_pair(option_desc_public, option_desc_private);
}

bool ThorLinkerStage::parseOptions(po::variables_map& vm)
{
    verbose = (vm.count("verbose") > 0);
    return true;
}

bool ThorLinkerStage::execute(bool& continue_execution)
{
    UNUSED_ARGUMENT(continue_execution);

    if(!buildNativeCodeX86()) return false;
    if(!buildNativeCodeCUDA()) return false;
    if(!linkObjects()) return false;

    if(true)
    {
    	ASTNodeHelper::visualize(getParserContext().tangle, getGeneratorContext().global_config.dump_graphviz_dir / "before_bundle_save.dot");
    }

    return true;
}


bool ThorLinkerStage::buildNativeCodeX86()
{
	GeneratorContext& generator_context = getGeneratorContext();

    // TODO: How about windows!?
  std::string native_compiler = llvm::sys::FindProgramByName(THOR_CPP_NATIVE_COMPILER);
    if (native_compiler.empty())
    {
        std::cerr << "Error: failed to locate " << THOR_CPP_NATIVE_COMPILER << std::endl;
        return false;
    }

	boost::filesystem::path current_working_dir = boost::filesystem::current_path();
	boost::filesystem::current_path(generator_context.global_config.project_path);

    for(auto* config : generator_context.target_config)
    {
        if(config->arch.is_not_x86())
    		continue;

    	std::string asm_file = config->asm_file;
    	std::string output_file;
    	{
    		boost::filesystem::path p = asm_file;
    		p.replace_extension(THOR_EXTENSION_OBJ);
    		output_file = p.string();
    	}

#ifdef __PLATFORM_WINDOWS__
		UNIMPLEMENTED_CODE();
#else
	    std::stringstream ss;

        // TODO: tempoarily disable assembler warning (xxx.s: Warning: stand-alone `data16' prefix)
        // From binutils: gas/config/tc-i386.c, it looks like the warning stands for the opcode is useless
        ss << " -Wa,-q";
		ss << " -c -shared -fPIC -o " + output_file;
		ss << " " << asm_file;

#endif
		if(shell(native_compiler, ss.str()) != 0)
		{
			return false;
		}

		config->object_file = output_file;
    }

    boost::filesystem::current_path(current_working_dir);

    return true;
}

bool ThorLinkerStage::buildNativeCodeCUDA()
{
	GeneratorContext& generator_context = getGeneratorContext();

	bool has_any_cuda_build_target = false;
	for(auto* config : generator_context.target_config)
	{
	    if(config->arch.is_cuda())
		{
			has_any_cuda_build_target = true; break;
		}
	}

	if(!has_any_cuda_build_target)
		return true;

	// generate the CUDA skeleton
	if(true)
	{
		Tangle* tangle = getParserContext().tangle;

		std::wostringstream ss;
		if(true)
		{
            std::wostringstream ss_hdr, ss_fwd;
            visitor::CudaObjectSkeletonGeneratorVisitor skeleton_generator(ss_hdr, ss_fwd);
            skeleton_generator.setForwardDeclarationOnly(true);
            skeleton_generator.visit(*tangle);

            static constexpr auto primitive_type_definitions =
                #include "thor/PrimitiveTypes.inc"
            ;

            ss << primitive_type_definitions << std::endl;
            ss << L"using thor::int8;"       << std::endl;
            ss << L"using thor::int16;"      << std::endl;
            ss << L"using thor::int32;"      << std::endl;
            ss << L"using thor::int64;"      << std::endl;
            ss << L"using thor::float32;"    << std::endl;
            ss << L"using thor::float64;"    << std::endl;

            ss << L"/***** begin included header *****/" << std::endl;
            ss << ss_hdr.str() << std::endl;
            ss << L"/***** end included header *****/" << std::endl;

			if(true)
			{
			    ss << L"/***** begin forward declaration *****/" << std::endl;
				ss << ss_fwd.str();
				ss << L"/***** end forward declaration *****/" << std::endl;
				ss << std::endl;
			}

			if(true)
			{
				std::wostringstream ss_dummy, ss_actual;
				visitor::CudaObjectSkeletonGeneratorVisitor skeleton_generator(ss_dummy, ss_actual);
				skeleton_generator.setForwardDeclarationOnly(false);
				skeleton_generator.visit(*tangle);

				ss << L"/***** begin dummy implementation *****/" << std::endl;
				ss << ss_actual.str();
				ss << L"/***** end dummy implementation *****/" << std::endl;
				ss << std::endl;
			}
		}

		// write the skeleton cu into file
		std::string cu_file;
		if(true)
		{
			cu_file = (generator_context.global_config.build_path / (generator_context.global_config.manifest.name + THOR_EXTENSION_CU)).string();

			std::wofstream cu_file_handle(cu_file);
			if(!cu_file_handle.is_open())
			{
				std::cerr << "Error: failed to open " << cu_file << "for writing" << std::endl;
				return false;
			}

			cu_file_handle << ss.str();
			cu_file_handle.close();
		}
	}

	// perform nvcc-compatible linking steps to generate final object file
	if(true)
	{
		std::string base_isa;
		for(auto* config : generator_context.target_config)
		{
		    if(config->arch.is_cuda())
			{
				base_isa = config->arch.toString("");
				break;
			}
		}

		boost::filesystem::path output_object_path = generator_context.global_config.build_path / (generator_context.global_config.manifest.name + "_cuda" + THOR_EXTENSION_OBJ);

		// use cudafe, gcc, filehash, ptxas to generate nvcc-compatible GPU object file
		std::string	gcc       = llvm::sys::FindProgramByName("gcc");
		std::string	nvcc      = llvm::sys::FindProgramByName("nvcc");
		std::string	cudafe    = llvm::sys::FindProgramByName("cudafe");
		std::string	cudafepp  = llvm::sys::FindProgramByName("cudafe++");
		std::string	filehash  = llvm::sys::FindProgramByName("filehash");
		std::string	ptxas     = llvm::sys::FindProgramByName("ptxas");
		std::string	fatbinary = llvm::sys::FindProgramByName("fatbinary");

		if(gcc.empty())
		{
			std::cerr << "Error: failed to find gcc, please check" << std::endl;
			return false;
		}
		if(nvcc.empty())
		{
			std::cerr << "Error: failed to find nvcc, please check your CUDA installation" << std::endl;
			return false;
		}
		if(cudafe.empty())
		{
			std::cerr << "Error: failed to find cudafe, please check your CUDA installation" << std::endl;
			return false;
		}
		if(filehash.empty())
		{
			std::cerr << "Error: failed to find filehash, please check your CUDA installation" << std::endl;
			return false;
		}
		if(ptxas.empty())
		{
			std::cerr << "Error: failed to find ptxas, please check your CUDA installation" << std::endl;
			return false;
		}
		if(fatbinary.empty())
		{
			std::cerr << "Error: failed to find fatbinary, please check your CUDA installation" << std::endl;
			return false;
		}

		// prepare cuda include path
		boost::filesystem::path include_path = nvcc;
		include_path = include_path.parent_path().parent_path() / "include";

		auto translate_isa_to_cuda_arch = [](const std::string& isa) -> std::string {
			if(isa == "sm_10") return "100";
			if(isa == "sm_12") return "120";
			if(isa == "sm_13") return "130";
			if(isa == "sm_20") return "200";
			if(isa == "sm_21") return "210";
			if(isa == "sm_22") return "220";
			if(isa == "sm_23") return "230";
			if(isa == "sm_30") return "300";
			if(isa == "sm_35") return "300";

			UNIMPLEMENTED_CODE();
			return "";
		};

		auto translate_sm_to_compute_version = [](const std::string& sm) -> std::string {
			if(sm == "sm_10") return "compute_10";
			if(sm == "sm_12") return "compute_12";
			if(sm == "sm_13") return "compute_13";
			if(sm == "sm_20") return "compute_20";
			if(sm == "sm_21") return "compute_21";
			if(sm == "sm_22") return "compute_22";
			if(sm == "sm_23") return "compute_23";
			if(sm == "sm_30") return "compute_30";
			if(sm == "sm_35") return "compute_35";

			std::cerr << "unsupported sm = " << sm << std::endl;
			UNIMPLEMENTED_CODE();
			return "";
		};

		bool using_64bit = generator_context.global_config.manifest.using_64bit;
		std::string project_name = generator_context.global_config.manifest.name;

		std::string base_isa_translated = translate_sm_to_compute_version(base_isa);

		boost::filesystem::path current_working_dir = boost::filesystem::current_path();
		boost::filesystem::current_path(generator_context.global_config.build_path);

#ifdef __PLATFORM_WINDOWS__
		UNIMPLEMENTED_CODE();
#else
		// step #1:
		// gcc
		// -D__CUDA_ARCH__=350 // any version ranges from 200 to 350; in nvcc it just uses the first gencode isa
		// -E -x c++
		// -DCUDA_DOUBLE_MATH_FUNCTIONS
		// -D__CUDACC__
		// -D__NVCC__
		// -fPIC
		// -I"/usr/local/cuda-5.0/include" -I"." -I".." -I"../../common/inc" -I"../../../shared/inc" "-I/usr/local/cuda-5.0/bin/../include"
		// -include "cuda_runtime.h"
		// -m64 // depends on either 32bit or 64 bit arch
		// -o "simpleDeviceLibrary.compute_20.cpp1.ii" // {1}
		// "simpleDeviceLibrary.cu" {0}

		if(true)
		{
			std::stringstream ss;
			ss  << " -D__CUDA_ARCH__=" << translate_isa_to_cuda_arch(base_isa)
				<< " -E -x c++"
				<< " -DCUDA_DOUBLE_MATH_FUNCTIONS"
				<< " -D__CUDACC__"
				<< " -D__NVCC__"
				<< " -fPIC"
				<< " -I\"" << include_path.string() << "\""
                << " -I\"" << generator_context.global_config.source_path.string() << "\""
                << generate_dependence_include(generator_context.global_config)
				<< " -include \"cuda_runtime.h\""
				<< " " << (using_64bit ? " -m64" : " -m32")
				<< " -o \"" << project_name << "." << base_isa_translated << ".cpp1.ii\""
				<< " \"" << project_name << ".cu\"";

			if(shell(gcc, ss.str()) != 0)
				goto error_while_building_gpu_code;
		}

		// step #2:
		// cudafe
		// --m64
		// --gnu_version=40701
		// -tused
		// --no_remove_unneeded_entities
		// --device-c
		// --gen_c_file_name "simpleDeviceLibrary.compute_20.cudafe1.c" {2_0}
		// --stub_file_name "simpleDeviceLibrary.compute_20.cudafe1.stub.c" {2_1}
		// --gen_device_file_name "simpleDeviceLibrary.compute_20.cudafe1.gpu"
		// --nv_arch "compute_20"
		// --gen_module_id_file
		// --module_id_file_name "simpleDeviceLibrary.module_id" {2_2}
		// --include_file_name "simpleDeviceLibrary.fatbin.c"
		// "simpleDeviceLibrary.compute_20.cpp1.ii" {1}
		if(true)
		{
			std::stringstream ss;
			ss  << " " << (using_64bit ? " --m64" : " --m32")
				<< " --gnu_version=40701"
				<< " -tused"
				<< " --no_remove_unneeded_entities"
				<< " --device-c"
				<< " --gen_c_file_name \"" << project_name << "." << base_isa_translated << ".cudafe1.c\""
				<< " --stub_file_name \"" << project_name << "." << base_isa_translated << ".cudafe1.stub.c\""
				<< " --gen_device_file_name \"" << project_name << "." << base_isa_translated << ".cudafe1.stub.gpu\""
				<< " --nv_arch \"" << base_isa_translated << "\""
				<< " --gen_module_id_file"
				<< " --module_id_file_name \"" << project_name << ".module_id\""
				<< " --include_file_name \"" << project_name << ".fatbin.c\""
				<< " \"" << project_name << "." << base_isa_translated << ".cpp1.ii\"";

			if(shell(cudafe, ss.str()) != 0)
				goto error_while_building_gpu_code;
		}

		// step #3:
		// gcc
		// -E -x c++
		// -D__CUDACC__
		// -D__NVCC__
		// -fPIC
		// -I"/usr/local/cuda-5.0/include" -I"." -I".." -I"../../common/inc" -I"../../../shared/inc" "-I/usr/local/cuda-5.0/bin/../include"
		// -include "cuda_runtime.h"
		// -m64
		// -o "simpleDeviceLibrary.cpp4.ii" {3}
		// "simpleDeviceLibrary.cu" {0}
		if(true)
		{
			std::stringstream ss;
			ss  << " -E -x c++"
				<< " -D__CUDACC__"
				<< " -D__NVCC__"
				<< " -fPIC"
				<< " -I\"" << include_path.string() << "\""
                << " -I\"" << generator_context.global_config.source_path.string() << "\""
                << generate_dependence_include(generator_context.global_config)
				<< " -include \"cuda_runtime.h\""
				<< " " << (using_64bit ? " -m64" : " -m32")
				<< " -o \"" << project_name << ".cpp4.ii\""
				<< " \"" << project_name << ".cu\"";

			if(shell(gcc, ss.str()) != 0)
				goto error_while_building_gpu_code;
		}

		// step #4:

		// cudafe++
		// --m64
		// --gnu_version=40701
		// --parse_templates
		// --device-c
		// --gen_c_file_name "simpleDeviceLibrary.compute_20.cudafe1.cpp" {4_1} (output)
		// --stub_file_name "simpleDeviceLibrary.compute_20.cudafe1.stub.c" {2_1} (input)
		// --module_id_file_name "simpleDeviceLibrary.module_id" {2_2} (input)
		// "simpleDeviceLibrary.cpp4.ii" {3}
		if(true)
		{
			std::stringstream ss;
			ss  << " " << (using_64bit ? " --m64" : " --m32")
				<< " --gnu_version=40701"
				<< " --parse_templates"
				<< " --device-c"
				<< " --gen_c_file_name \"" << project_name << "." << base_isa_translated << ".cudafe1.cpp\""
				<< " --stub_file_name \"" << project_name << "." << base_isa_translated << ".cudafe1.stub.c\""
				<< " --module_id_file_name \"" << project_name << ".module_id\""
				<< " \"" << project_name << ".cpp4.ii\"";

			if(shell(cudafepp, ss.str()) != 0)
				goto error_while_building_gpu_code;
		}

		// step #5: assembly all PTX file into cubin

		// ptxas
		// -arch=sm_20
		// -m64
		// --compile-only  "simpleDeviceLibrary.compute_20.ptx"
		// -o "simpleDeviceLibrary.compute_20.cubin" {5_0}

		// ptxas
		// -arch=sm_30
		// -m64
		// --compile-only
		// "simpleDeviceLibrary.compute_30.ptx"
		// -o "simpleDeviceLibrary.compute_30.cubin" {5_1}

		// ptxas
		// -arch=sm_35
		// -m64
		// --compile-only
		// "simpleDeviceLibrary.compute_35.ptx"
		// -o "simpleDeviceLibrary.compute_35.cubin" {5_2}
		if(true)
		{
			for(auto* config : generator_context.target_config)
			{
			    if(config->arch.is_cuda())
				{
					std::string isa = config->arch.toString("");
					std::stringstream ss;
					ss  << " -arch=" << isa
						<< (using_64bit ? " -m64" : " -m32")
						<< " --compile-only"
						<< " \"" << config->asm_file << "\""
						<< " -o \"" << project_name << "." << translate_sm_to_compute_version(isa) << ".cubin\"";

					if(shell(ptxas, ss.str()) != 0)
						goto error_while_building_gpu_code;
				}
			}
		}

		// step #6: combine all cubins into one fatbin

		// fatbinary
		// --create="simpleDeviceLibrary.fatbin"
		// -64
		// --key="xxxxxxxxxx"
		// --ident="simpleDeviceLibrary.cu"
		// --cmdline="--compile-only  "
		// "--image=profile=sm_20@compute_20,file=simpleDeviceLibrary.compute_20.cubin" {5_0}
		// "--image=profile=sm_30@compute_30,file=simpleDeviceLibrary.compute_30.cubin" {5_1}
		// "--image=profile=sm_35@compute_35,file=simpleDeviceLibrary.compute_35.cubin" {5_2}
		// --embedded-fatbin="simpleDeviceLibrary.fatbin.c"
		// --cuda --device-c
		if(true)
		{
			std::stringstream ss;
			ss  << " --create=\"" << project_name << ".fatbin\""
				<< " " << (using_64bit ? " --64" : " --32")
				<< " --key=\"xxxxxxxxxx\""
				<< " --ident=\"" << project_name << ".cu\""
				<< " --cmdline=\"--compile-only  \"";
			for(auto* config : generator_context.target_config)
			{
			    if(config->arch.is_cuda())
				{
					std::string isa = config->arch.toString("");
					std::string isa_translated = translate_sm_to_compute_version(isa);
					ss << " \"--image=profile=" << isa << "@" << isa_translated << ",file=" << project_name << "." << isa_translated << ".cubin\"";
				}
			}
			ss  << " --embedded-fatbin=\"" << project_name << ".fatbin.c\""
				<< " --cuda"
				<< " --device-c";

			if(shell(fatbinary, ss.str()) != 0)
				goto error_while_building_gpu_code;
		}

		// step #7:

		// gcc
		// -D__CUDA_ARCH__=350
		// -E -x c++
		// -DCUDA_DOUBLE_MATH_FUNCTIONS
		// -D__CUDA_PREC_DIV
		// -D__CUDA_PREC_SQRT
		// -fPIC
		// -I"/usr/local/cuda-5.0/include" -I"." -I".." -I"../../common/inc" -I"../../../shared/inc" "-I/usr/local/cuda-5.0/bin/../include"
		// -m64
		// -o "simpleDeviceLibrary.cu.cpp.ii" {7}
		// "simpleDeviceLibrary.compute_20.cudafe1.cpp" {4_1}
		if(true)
		{
			std::stringstream ss;
			ss  << " -D__CUDA_ARCH__=" << translate_isa_to_cuda_arch(base_isa)
				<< " -E -x c++"
				<< " -DCUDA_DOUBLE_MATH_FUNCTIONS"
				<< " -D__CUDA_PREC_DIV"
				<< " -D__CUDA_PREC_SQRT"
				<< " -fPIC"
				<< " -I\"" << include_path.string() << "\""
				<< " " << (using_64bit ? " -m64" : " -m32")
				<< " -o \"" << project_name << ".cu.cpp.ii\""
				<< " \"" << project_name << "." << base_isa_translated << ".cudafe1.cpp\"";

			if(shell(gcc, ss.str()) != 0)
				goto error_while_building_gpu_code;
		}

		// step #8:

		// gcc
		// -c -x c++
		// -fPIC
		// -I"/usr/local/cuda-5.0/include" -I"." -I".." -I"../../common/inc" -I"../../../shared/inc" "-I/usr/local/cuda-5.0/bin/../include"
		// -fpreprocessed
		// -m64
		// -o "simpleDeviceLibrary.o" {8}
		// "simpleDeviceLibrary.cu.cpp.ii" {7}
		if(true)
		{
			std::stringstream ss;
			ss  << " -c -x c++"
				<< " -fPIC"
				<< " -I\"" << include_path.string() << "\""
				<< " -fpreprocessed"
				<< " " << (using_64bit ? " -m64" : " -m32")
				<< " -o \"" << output_object_path.string() << "\""
				<< " \"" << project_name << ".cu.cpp.ii\"";

			if(shell(gcc, ss.str()) != 0)
				goto error_while_building_gpu_code;
		}

		// save the output object file path
		for(auto* config : generator_context.target_config)
		{
		    if(config->arch.is_cuda())
			{
				config->object_file = output_object_path.string();
				break;
			}
		}
#endif

		// restore the current working directory
		boost::filesystem::current_path(current_working_dir);
		return true;

error_while_building_gpu_code:
		std::cerr << "Error: error while building nvcc GPU object code" << std::endl;
		boost::filesystem::current_path(current_working_dir);
		return false;
	}

	return true;
}

bool ThorLinkerStage::linkObjects()
{
    // TODO do the actual link only
    GeneratorContext& generator_context = getGeneratorContext();
    generator_context.target_config;

    // TODO: How about windows!?
    std::string cpp = llvm::sys::FindProgramByName(THOR_CPP_NATIVE_COMPILER);
    std::string ar = llvm::sys::FindProgramByName(THOR_NATIVE_ARCHIVER);

    if(cpp.empty())
    {
        std::cerr << "Error: failed to locate " << THOR_CPP_NATIVE_COMPILER << std::endl;
        return false;
    }

    if(ar.empty())
    {
        std::cerr << "Error: failed to locate " << THOR_NATIVE_ARCHIVER << std::endl;
        return false;
    }

	boost::filesystem::path current_working_dir = boost::filesystem::current_path();
	boost::filesystem::current_path(generator_context.global_config.project_path);

	// build x86/ARM shared object
    if(true)
    {
    	boost::filesystem::path output_file_path = generator_context.global_config.binary_output_path / (THOR_PREFIX_LIBRARY + generator_context.global_config.manifest.name + THOR_EXTENSION_SO);
    	std::string output_file = output_file_path.string();

    	std::stringstream ss;
#if defined (__PLATFORM_LINUX__) || defined (__PLATFORM_MAC__)
  	    // Prepare parameters to invoke gcc/cl. Use std::string to easily concate string
#ifdef __PLATFORM_MAC__
		boost::filesystem::path p1(output_file);
		std::string install_name = p1.filename().string();
		ss << " -undefined dynamic_lookup -dynamiclib -install_name ./bin/" << install_name;
#endif

		ss << " -shared -fPIC -o " << output_file;

#ifdef __PLATFORM_MAC__
		ss << " -L" << generator_context.global_config.sdk_lib_path.string();

		// by default, we will look for the libraries under the same folder
		ss << " -L" << generator_context.global_config.binary_output_path.string();
#else
		ss << " -Wl,-rpath=" << generator_context.global_config.sdk_lib_path.string();
		ss << " -L" << generator_context.global_config.sdk_lib_path.string();

		// by default, we will look for the libraries under the same folder
		ss << " -Wl,-rpath=./bin";
		ss << " -L" << generator_context.global_config.binary_output_path.string();
#endif

//    	    for (size_t i = 0; i < link_libraries.size(); i++)
//    	        args += " -l" + link_libraries[i];
//
//    	    // search path
//    	    for (size_t i = 0; i < link_search_paths.size(); i++)
//    	        args += " -L" + link_search_paths[i];

#ifdef __PLATFORM_MAC__
		// args += " -Wl,-rpath " + runtime_search_paths[i];
#else
//    	    for (size_t i = 0; i < runtime_search_paths.size(); i++)
//    	        args += " -Wl,-rpath=" + runtime_search_paths[i];
#endif
    	    // locate all object file under bin directory
#ifdef __PLATFORM_MAC__
		ss << " -Wl,-all_load";
#else
		ss << " -Wl,--whole-archive";
#endif

		for(auto* config : generator_context.target_config)
		{
		    if(config->arch.is_not_cuda())
			{
				if(!config->object_file.empty())
				{
					ss << " " << config->object_file;
				}
			}
		}

		std::string cpp_gen_temporary_file_name = "temporary_template_instantiation";
		boost::filesystem::path cpp_gen_obj_path = generator_context.global_config.build_path / (cpp_gen_temporary_file_name + THOR_EXTENSION_OBJ);
		if(boost::filesystem::exists(cpp_gen_obj_path))
		{
			ss << " " << cpp_gen_obj_path.string();
		}

//		std::vector<boost::filesystem::path> object_under_build = Filesystem::collect_files(generator_context.global_config.build_path, THOR_EXTENSION_OBJ);
//		foreach (i, object_under_build)
//		{
//			ss << " " << i->string();
//		}

		// input files
		for(auto& object : generator_context.global_config.manifest.deps.native_objects)
		{
		    if(object.arch.is_cpu())
			{
				boost::filesystem::path object_path(object.lpath);
				ss << " " << (object_path / (object.name)).string();
			}
		}

#ifdef __PLATFORM_MAC__
    	   // args += " -Wl,-noall_load";
#else
		ss << " -Wl,--no-whole-archive";
#endif

		// libraries from configuration
#ifdef __PLATFORM_MAC__
		ss << " -Wl,-all_load";
#else
		ss << " -Wl,--whole-archive";
#endif

		for (auto& library : generator_context.global_config.manifest.deps.native_libraries)
		{
		    if(library.arch.is_cpu())
			{
				ss << " -l" << library.name << " -L" << filterValidLinkerPath(library.lpath);
			}
		}

#ifdef __PLATFORM_MAC__
		//args += " -Wl,-noall_load";
#else
		ss << " -Wl,--no-whole-archive";
#endif

		for(auto& library : generator_context.global_config.manifest.deps.native_shared_libraries)
		{
		    if(library.arch.is_cpu())
			{
				if (library.rpath.length())
				{
#ifdef __PLATFORM_MAC__
					ss << " -l" << library.name << " -L" << filterValidLinkerPath(library.lpath);
#else
					ss << " -l" << library.name << " -L" << filterValidLinkerPath(library.lpath) << " -Wl,-rpath=" << library.rpath;
#endif
				}
				else
				{
					ss << " -l" << library.name << " -L" << filterValidLinkerPath(library.lpath);
				}
			}
		}

		// locate all the shared object under the project binary output path
		std::vector<boost::filesystem::path> so_under_bin = Filesystem::collect_files(generator_context.global_config.binary_output_path, THOR_EXTENSION_SO);
		for (auto& shared_library : so_under_bin)
		{
			auto stem = shared_library.stem();  // e.g. libfoo
			std::string symbol_name(stem.string(), 3);  // symbol_name = foo

			// skip myself XD
			if (symbol_name == generator_context.global_config.manifest.name) continue;

			BOOST_ASSERT(symbol_name.length() != 0 && "Well, please examine the share object file name, should start with 'lib'");
			ss << " -l" << symbol_name;
	//      args += i->string();
		}


#endif

#ifdef __PLATFORM_WINDOWS__
		UNIMPLEMENTED_CODE();
#endif

		if(shell(cpp, ss.str()) != 0)
		{
			std::cerr << "Error: error while building shared object" << std::endl;
			return false;
		}
    }

    // create the CUDA linkable archive
    if(true)
    {
    	std::stringstream ss;
    	ss << " cr";

    	boost::filesystem::path output_archive_path = generator_context.global_config.binary_output_path / (THOR_PREFIX_LIBRARY + generator_context.global_config.manifest.name + "_cuda" + THOR_EXTENSION_LIB);
    	ss << " " << output_archive_path.string();

    	bool require_archive_for_cuda_linking = false;
		for(auto* config : generator_context.target_config)
		{
		    if(config->arch.is_cuda())
			{
				if(!config->object_file.empty())
				{
					ss << " " << config->object_file;
					require_archive_for_cuda_linking = true;
				}
			}
		}

		for (auto& native_object : generator_context.global_config.manifest.deps.native_objects)
		{
            if(native_object.arch.is_cuda())
			{
				boost::filesystem::path object_path(native_object.lpath);
				ss << " " << (object_path / (native_object.name)).string();
			}
		}

		for (auto& native_library : generator_context.global_config.manifest.deps.native_libraries)
		{
            if(native_library.arch.is_cuda())
			{
			    // TODO the i->lpath could be of semicolumn-delimited format, in which we should iterate each of them to locate the actual library
				// extract all enclosing objects
				boost::filesystem::path path_to_extract_object;
				if(true)
				{
					boost::filesystem::path lpath(native_library.lpath);
					boost::filesystem::path path_to_lib;
					if(lpath.empty())
					{
						// TODO we should search for default link path (LIBRARY_PATH)
						UNIMPLEMENTED_CODE();
					}
					else
					{
						if(lpath.is_absolute())
							path_to_lib = lpath / (THOR_PREFIX_LIBRARY + (native_library.name) + THOR_EXTENSION_LIB);
						else
							path_to_lib = boost::filesystem::current_path() / lpath / (THOR_PREFIX_LIBRARY + (native_library.name) + THOR_EXTENSION_LIB);
					}

					path_to_extract_object = generator_context.global_config.build_path / native_library.name;
					if(!extractArchive(path_to_lib, path_to_extract_object))
					{
						std::cerr << "Error: failed to extract library '" << (native_library.name) << "'" << std::endl;
						return false;
					}
				}

				ss << " " << (path_to_extract_object / (std::string("*") + THOR_EXTENSION_OBJ)).string();
			}
		}

		if(require_archive_for_cuda_linking)
		{
			if(shell(ar, ss.str()) != 0)
			{
				std::cerr << "Error: error while building archive for CUDA linking" << std::endl;
				return false;
			}

		}
    }

    boost::filesystem::current_path(current_working_dir);

    return true;
}

std::string ThorLinkerStage::getThorDefaultPathLinkOption()
{
	GeneratorContext& generator_context = getGeneratorContext();

    std::string args;

    // we will check ts home directory
    // TODO: where is the path of library in ts home directory?
#ifdef __PLATFORM_MAC__
//    args += " -Wl,-rpath " + config.sdk_lib_path.string();
    args += " -L" + generator_context.global_config.sdk_lib_path.string();

    // by default, we will look for the libraries under the same folder
//    args += " -Wl,-rpath ./bin";
    args += " -L" + generator_context.global_config.binary_output_path.string();
#else
    args += " -Wl,-rpath=" + generator_context.global_config.sdk_lib_path.string();
    args += " -L" + generator_context.global_config.sdk_lib_path.string();

    // by default, we will look for the libraries under the same folder
    args += " -Wl,-rpath=./bin";
    args += " -L" + generator_context.global_config.binary_output_path.string();
#endif

    return args;

}

bool ThorLinkerStage::extractArchive(const boost::filesystem::path& absolute_library_path, const boost::filesystem::path& extract_path)
{
    std::string ar = llvm::sys::FindProgramByName(THOR_NATIVE_ARCHIVER);
    if(ar.empty())
    {
        std::cerr << "Error: failed to locate " << THOR_NATIVE_ARCHIVER << std::endl;
        return false;
    }

	if(!boost::filesystem::exists(extract_path))
	{
		if(!boost::filesystem::create_directories(extract_path))
			return false;
	}

	// save the current path
	boost::filesystem::path current_path_backup = boost::filesystem::current_path();

	// switch to the path to extract objects (because ar can only extract to current directory
	boost::filesystem::current_path(extract_path);

	std::stringstream ss;
	ss << " x " << absolute_library_path.string();

	if(shell(ar, ss.str()) != 0)
	{
		boost::filesystem::current_path(current_path_backup);
		return false;
	}

	// restore current path
	boost::filesystem::current_path(current_path_backup);

	return true;
}

std::string ThorLinkerStage::filterValidLinkerPath(const std::string& path)
{
    return boost::algorithm::join(
        StringUtil::tokenize(path, ";", false) | boost::adaptors::filtered([](const boost::filesystem::path& p){ // implicit conversion
            return boost::filesystem::exists(p) && boost::filesystem::is_directory(p);
        }),
        ";"
    );
}

int ThorLinkerStage::shell(const std::string& tool, const std::string& parameters)
{
	GeneratorContext& generator_context = getGeneratorContext();

	if(generator_context.global_config.verbose)
	{
		std::cerr << "[Link] invokes '" << tool << " " << parameters << "'" << std::endl;
	}

	std::string cmd = (tool + " " + parameters);
	return std::system(cmd.c_str());
}


} } }
