/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>
#include <deque>
#include <utility>

#include <boost/filesystem.hpp>

#include "utility/Functional.h"

#include "core/SharedPtr.h"

#include "language/context/ParserContext.h"

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/module/Package.h"

#include "language/stage/Stage.h"
#include "language/stage/serialization/ASTDeserializationStage.h"
#include "language/stage/serialization/detail/ASTSerializationHelper.h"

namespace zillians { namespace language { namespace stage {

const char* ASTDeserializationStage::name()
{
    return "AST Deserialization Stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> ASTDeserializationStage::getOptions()
{
    auto option_desc_public  = make_shared<po::options_description>();
    auto option_desc_private = make_shared<po::options_description>();

    option_desc_public->add_options()
        ("load-ast"         , po::value<std::string>()->required(), "load serialized AST file as root")
        ("dump-graphviz"    , po::bool_switch()                   , "dump AST in graphviz format")
        ("dump-graphviz-dir", po::value<std::string>()            , "dump AST in graphviz format");

    for (const auto& option : option_desc_public->options())
        option_desc_private->add(option);

    return std::make_pair(option_desc_public, option_desc_private);
}

bool ASTDeserializationStage::parseOptions(po::variables_map& vm)
{
    ast_file      = vm["load-ast"         ].as<std::string>();
    dump_graphviz = vm["dump-graphviz"    ].as<bool       >();

    if (vm.count("dump-graphviz-dir"))
        dump_graphviz_dir = vm["dump-graphviz-dir"].as<std::string>();

    return true;
}

bool ASTDeserializationStage::execute(bool&)
{
    BOOST_ASSERT(
        (
            !hasParserContext() ||
            getParserContext().tangle == nullptr
        ) && "overriding origin tangle!?"
    );

    auto*const node   = ASTSerializationHelper::deserialize(ast_file);
    auto*const tangle = tree::cast_or_null<tree::Tangle>(node);

    if (tangle == nullptr)
    {
        std::cerr << "fail to deserialize tangle" << std::endl;

        return false;
    }

    if (!hasParserContext())
        setParserContext(new ParserContext);

    getParserContext().tangle = tangle;

    if(dump_graphviz)
    {
        boost::filesystem::path p(dump_graphviz_dir);
        ASTNodeHelper::visualize(getParserContext().tangle, p / "post-deserialized.dot");
    }

    return true;
}

} } }
