/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <deque>
#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include <boost/range/adaptor/map.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>

#include "utility/Functional.h"

#include "core/SharedPtr.h"

#include "language/context/ParserContext.h"
#include "language/tree/ASTNodeHelper.h"

#include "language/stage/Stage.h"
#include "language/stage/serialization/detail/ASTSerializationHelper.h"
#include "language/stage/serialization/BundleASTMergeStage.h"
#include "language/stage/tree_refactor/detail/TreeContextRefactorer.h"

namespace zillians { namespace language { namespace tree {

struct Declaration;
struct Tangle;

} } }

namespace zillians { namespace language { namespace stage {

const char* BundleASTMergeStage::name()
{
    return "Bundle AST Merge Stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> BundleASTMergeStage::getOptions()
{
    auto option_desc_public  = make_shared<po::options_description>();
    auto option_desc_private = make_shared<po::options_description>();

    option_desc_public->add_options()
        ("dep-bundle-asts"  , po::value<std::vector<std::string>>()->default_value(std::vector<std::string>(), "no depended bundle AST by default"), "Bundle ASTs to be merged"                             )
        ("dump-graphviz"    , po::bool_switch()                                                                                                    , "dump AST in graphviz format"                          )
        ("dump-graphviz-dir", po::value<std::string>()                                                                                             , "directory to dump intermediate AST in graphviz format")
    ;

    for (const auto& option : option_desc_public->options())
        option_desc_private->add(option);

    return {std::move(option_desc_public), std::move(option_desc_private)};
}

bool BundleASTMergeStage::parseOptions(po::variables_map& vm)
{
    bundle_ast_files = vm["dep-bundle-asts"  ].as<std::vector<std::string>>();
    dump_graphviz    = vm["dump-graphviz"    ].as<bool>();

    if (vm.count("dump-graphviz-dir"))
        dump_graphviz_dir = vm["dump-graphviz-dir"].as<std::string>();

    return true;
}

bool BundleASTMergeStage::execute(bool&)
{
    using boost::adaptors::transformed;

    if(!hasParserContext() && bundle_ast_files.empty())
    {
        std::cerr << "Neither an parsed tangle nor bundle ast files are given." << std::endl;
        return false;
    }

    if (bundle_ast_files.empty())
        return true;

    std::deque<visitor::bundled_ast> bundle_asts;

    if (hasParserContext())
    {
        bundle_asts.emplace_back(visitor::bundled_ast::create_dummy(*getParserContext().tangle));
    }
    else
    {
        setParserContext(new ParserContext());
    }

    boost::push_back(
        bundle_asts,
          bundle_ast_files
        | transformed(&ASTSerializationHelper::deserialize_bundle_ast)
    );

    const auto& all_success = boost::algorithm::all_of(
        boost::make_iterator_range(bundle_asts, 1, 0) | transformed(std::mem_fn(&visitor::bundled_ast::get_tangle)),
        not_null()
    );

    if (!all_success)
    {
        std::cerr << "fail to deserialize bundle ASTs" << std::endl;

        return false;
    }

    auto*const merged_tangle = visitor::bundled_ast::merge(bundle_asts);

    if (merged_tangle == nullptr)
    {
        std::cerr << "fail to merge tangles" << std::endl;

        return false;
    }

    getParserContext().tangle = merged_tangle;

    if(dump_graphviz)
    {
        boost::filesystem::path p(dump_graphviz_dir);

        getParserContext().tangle->toSource(p / "post-ast-merge.t");
        ASTNodeHelper::visualize(getParserContext().tangle, p / "post-ast-merge.dot");
    }

    return true;
}

} } }
