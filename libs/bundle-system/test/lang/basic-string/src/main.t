/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@entry
task main() : void
{
    var exit_success: int32 = 0;

    var x : String = "Hello";
    if( !(x.length() == 5) ) exit(1);

    x = x.concate( " World");
    if( !(x.length() == 11) ) exit(2);

    if( !(x.isEqual("Hello World")) ) exit(3);
    if( !(x.isEqual("Hello World", false)) ) exit(4);
    if( !(x.isEqual("hELLO wORLD", true )) ) exit(4);

    if( !(x.find("llo") == 2) ) exit(5);
    if( !(x.find("hello") == -1) ) exit(6);

    if( !(x.compareTo("ApPlE") > 0) ) exit(7);
    if( !(x.compareTo("Hello World") == 0) ) exit(8);
    if( !(x.compareTo("OrAnge") < 0) ) exit(9);

    if( !(x.compareTo("ApPlE", true) > 0) ) exit(10);
    if( !(x.compareTo("HELLO WORLD", true) == 0) ) exit(11);
    if( !(x.compareTo("HELLO WORLD", false) != 0) ) exit(11);
    if( !(x.compareTo("OrAnGe", true) < 0) ) exit(12);

    if( !(x.startsWith("Hello")) ) exit(13);
    if( !(x.startsWith("World", 6)) ) exit(14);
    if( !(x.startsWith("qq") == false) ) exit(15);

    if( !(x.endsWith("World")) ) exit(16);
    if( !(x.endsWith("Hello") == false) ) exit(17);

    if( !(x.indexOf("llo Worl") == 2) ) exit(18);
    if( !(x.indexOf("Hello", 1) == -1) ) exit(19);
    if( !(x.indexOf("qq") == -1) ) exit(20);

    if( !(x.lastIndexOf("Wor") == 6) ) exit(21);
    if( !(x.lastIndexOf("Hello", 1) == -1) ) exit(22);
    if( !(x.lastIndexOf("qq") == -1) ) exit(23);

    if( !(x.substring(2).isEqual("llo World")) ) exit(24);
    if( !(x.substring(1, 5).isEqual("ello")) ) exit(25);

    if( !(x.toUpperCase().isEqual("HELLO WORLD")) ) exit(26);
    if( !(x.toLowerCase().isEqual("hello world")) ) exit(27);

    x = " \t \r  Bingo \r \t \n";
    if( !(x.trim().isEqual("Bingo")) ) exit(28);

    var first = false; var second = true; var third = 2;
    x = "\{first}, \{second}, \{third}";
    if(!x.isEqual("false, true, 2")) exit(29);


    exit(exit_success);
}
