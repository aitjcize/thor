/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * Constructed lambda
 */
class my_lambda
{
	public function new(in_a: int32) : void
	{
		a = in_a;
	}

	public function invoke(x : int32) : int32
	{
		return a + x;
	}

	var a : int32;
}

class my_lambda2
{
	public function new(in_a: int32) : void
	{
		a = in_a;
	}

	public function invoke(x: int32, y: int32) : int32
	{
		return a - x - y;
	}

	var a : int32;
}

@entry
task test1() : void
{
	var a = 9;
	/* One variable
	 *
	 * var f = lambda(x: int32) : int32 {
	 *            return a + x;
	 *         }
	 * var z = f(10);
	 */

	var _f = new my_lambda(a);
	var f = new Lambda1<int32, int32>(_f);
	var z = f.invoke(10);

	if (z != 19) exit(-1);

	/*
	 * Two variable
	 *
	 * var g = lambda(x: int32, y:int32) : int32 {
	 return a - x - y;
	 }
	 var z2 = g(1, 2);
	 */
	var _g = new my_lambda2(a);
	var g = new Lambda2<int32, int32, int32>(_g);
	var z2 = g.invoke(1, 2);

	if (z2 != 6) exit(-2);

	/* 
	 * try assignment
	 *
	 * var p = g;
	 * var z3 = p(3, 4);
	 *
	 */ 
	var p = g;
	var z3 = p.invoke(3, 4);

	if (z3 != 2) exit(-3);

	// try function call
	if (testing(f) != 0) exit(-4);

	exit(0);
}

function testing(x: Lambda1<int32, int32>) : int32
{
	if (x.invoke(8) != 17) return -1;
	return 0;
}
