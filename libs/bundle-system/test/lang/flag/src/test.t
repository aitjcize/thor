/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= thor.lang;
import .= thor.container;

function float_equal(a:float64,b:float64)
{
    return (a < b + 0.000001f && a > b - 0.000001f);
}

@entry
task test_integer()
{
    const f = new Flag();
    f.createInteger("tint","Test integer");
    if(f.parse())
    {
        const v : int64 = f.getInteger("tint");
        print("Integer: \{v}\n");
        exit(cast<int32>(v - 12345L));
    }
    exit(-1);
}

@entry
task test_float()
{
    const f = new Flag();
    f.createFloat("tfloat","Test float");
    if(f.parse())
    {
       const v : float64 = f.getFloat("tfloat");
       print("Float: \{v}\n");
       if(float_equal(v,1.0))
       {
           exit(0);
       }
    }
    exit(-1);
}

@entry
task test_string()
{
    const f = new Flag();
    f.createString("tstring","Test string");

    if(f.parse())
    {
        const s : String = f.getString("tstring");
        print("String: \{s}\n");
        if(s.isEqual("HelloThor"))
        {
            exit(0);
        }
    }
    exit(-1);
}

@entry
task test_has()
{
    const f = new Flag();
    f.createInteger("t1","Integer");
    f.createFloat("t2","Float");
    f.createString("t3","String");

    if(f.parse())
    {
        if(!f.has("t1"))
        {
            exit(-1);
        }
        if(!f.has("t2"))
        {
            exit(-2);
        }
        if(!f.has("t3"))
        {
            exit(-4);
        }
        if(f.has("XDD"))
        {
            exit(-5);
        }
        exit(0);
    }
    else
    {
        exit(-6);
    }
}

@entry
task test_help()
{
    const f = new Flag();
    f.createInteger("t1","Integer");
    f.createFloat("t2","Float");
    f.createString("t3","String");

    var help_str : String = f.help();
    if(help_str.find("--t1") != -1 && help_str.find("--t2") != -1 && help_str.find("--t3") != -1 &&
        help_str.find("Integer") != -1 && help_str.find("Float") != -1 && help_str.find("String") != -1)
    {
        print(f.help());
        exit(0);
    }
    exit(-1);
}

@entry
task test_required_integer_failed()
{
    const f = new Flag();
    f.createInteger("req","give it to me",true);
    if(!f.parse())
    {
        exit(0);
    }
    exit(-1);
}


@entry
task test_required_integer_success()
{
    const f = new Flag();
    f.createInteger("req","I'll give this time.");

    if(f.parse())
    {
        exit(0);
    }
    exit(-1);
}

@entry
task test_unknown_options()
{
    const f = new Flag();
    f.createInteger("only-thing-I-know","thorc-will-pass-xd-for-me-in-this-test");

    if(!f.parse())
    {
        exit(0);
    }
    exit(-1);
}

@entry
task test_empty_option()
{
    const f = new Flag();
    if(f.parse())
    {
        exit(0);
    }
    exit(-1);
}

@entry
task test_positional_integer()
{
    const f = new Flag();
    f.createPositionalInteger("pos-int","XXXXX");
    if(f.parse())
    {
       var pr:Vector<int64> = f.getPositionalInteger("pos-int");
       if(pr.size() != 5)
       {
           exit(-1);
       }
       for(var i = 0; i < pr.size(); ++i)
       {
           if(pr.get(i) != i)
           {
                exit(-2);
           }
       }
       exit(0);
    }
    else
    {
        exit(-3);
    }
}

@entry
task test_positional_float()
{
    const f = new Flag();
    f.createPositionalFloat("pos-float","XXXXX");
    if(f.parse())
    {
       var pr:Vector<float64> = f.getPositionalFloat("pos-float");
       if(pr.size() != 5)
       {
           exit(-1);
       }
       for(var i:int32 = 0; i < pr.size(); ++i)
       {
           if(!float_equal(pr.get(i),cast<float64>(i)+0.1))
           {
                exit(-1);
           }
       }
       exit(0);
    }
    else
    {
        exit(-2);
    }
}

@entry
task test_positional_string()
{
    const f = new Flag();
    f.createPositionalString("pos-string","XXXXX");
    if(f.parse())
    {
       var pr:Vector<String> = f.getPositionalString("pos-string");
       if(pr.size() != 5)
       {
           exit(-1);
       }
       if(!pr.get(0).isEqual("AAA") || !pr.get(1).isEqual("BBB") || !pr.get(2).isEqual("CCC") ||
          !pr.get(3).isEqual("DDD") || !pr.get(4).isEqual("EEE"))
       {
           exit(-2);
       }
       exit(0);
    }
    else
    {
        exit(-3);
    }
}

@entry
task test_multi_positional()
{
    const f = new Flag();
    f.createPositionalInteger("ints","XXXXX",2);
    f.createPositionalFloat("floats","XXXXX",3);
    f.createPositionalString("ss","XXXXX",4);
    if(f.parse())
    {
       var is:Vector<int64> = f.getPositionalInteger("ints");
       var fs:Vector<float64> = f.getPositionalFloat("floats");
       var ss:Vector<String> = f.getPositionalString("ss");
       if(is.size() != 2 || 
            is.get(0) != 12121 || 
            is.get(1) != 33333)
       {
           exit(-1);
       }
       if(fs.size() != 3 || 
            !float_equal(fs.get(0),3.14159) || 
            !float_equal(fs.get(1),0.333) ||
            !float_equal(fs.get(2),123.456))
       {
           exit(-2);
       }
       if(ss.size() != 4 || 
            !ss.get(0).isEqual("abc") ||
            !ss.get(1).isEqual("EFG") ||
            !ss.get(2).isEqual("hij") ||
            !ss.get(3).isEqual("AAA"))
       {
           exit(-3);
       }
       exit(0);
    }
    else
    {
        exit(-4);
    }
}

@entry
task test_positional_out_of_bound()
{
    const f = new Flag();
    f.createPositionalInteger("ints","XXXXX",2);
    if(!f.parse())
    {
       exit(0);
    }
    exit(-1);
}

@entry
task test_create_duplicate()
{
    const f = new Flag();
    f.createInteger("AAA","You shall not pass!");
    if(f.createInteger("AAA","XD"))
    {
        exit(-1);
    }
    if(f.createFloat("AAA","XD"))
    {
        exit(-2);
    }
    if(f.createString("AAA","XD"))
    {
        exit(-3);
    }
    if(f.createPositionalInteger("AAA","XD"))
    {
        exit(-4);
    }
    if(f.createPositionalFloat("AAA","XD"))
    {
        exit(-5);
    }
    if(f.createPositionalString("AAA","XD"))
    {
        exit(-6);
    }
    exit(0);
}

@entry
task test_get_failed()
{
    const f = new Flag();
    f.createInteger("CCCC","WTF");
    if(f.parse())
    {
        if(f.getInteger("CCCC")) //defined but is not given
        {
            exit(-1);
        }
        if(f.getInteger("CC")) //undefined
        {
            exit(-2);
        }
        if(f.getFloat("CCCC")) //wrong type
        {
            exit(-3);
        }
        exit(0);
    }
    else
    {
        exit(-4);
    }
}

@entry
task test_get_raw()
{
    const f = new Flag();
    var raw:Vector<String> = f.getRaw();
    if(raw.get(0).isEqual("--this") && raw.get(1).isEqual("-is") &&
        raw.get(2).isEqual("so") && raw.get(3).isEqual("^weird.") &&
        raw.get(4).isEqual("Please") && raw.get(5).isEqual("---do--it--yourself."))
    {
        exit(0);
    }
    exit(-1);
}

