/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <memory>

#include "thor/lang/Language.h"
#include "thor/lang/Domain.h"
#include "thor/lang/Process.h"
#include "framework/service/runtime/RuntimeServiceBuffer.h"
#include "framework/service/object/ObjectServiceBuffer.h"

namespace thor { namespace lang {

namespace mt_proxies = zillians::framework::service::mt::proxies;

Object::Object() {
}

Object::~Object() {
}

int64 Object::hash()
{
    return (int64)this;
}

Object* __createObject(int64 size, int64 type_id)
{
    return (Object*)mt_proxies::object::create(size, type_id);
}

void __destroyObjectInternal(int32 count, byte** objects )
{
    // Note that the objects are dynamic allocated in runtime service
    std::unique_ptr<Object*[]> p(reinterpret_cast<Object**>(objects));

    for (uint32 i = 0; i < count; i++)
    {
        Object* object = p[i];
        object->~Object();
    }

    mt_proxies::object::destroy(count, objects);
}

void __addToRootSet(int8* ptr)
{
    using zillians::framework::service::mt::ObjectHeader;

    if (ptr == nullptr)
        return;

    mt_proxies::object::addToObjectRootSet(ptr);
}

void __removeFromRootSet(int8* ptr)
{
    using zillians::framework::service::mt::ObjectHeader;

    if (ptr == nullptr)
        return;

    mt_proxies::object::removeFromObjectRootSet(ptr);
}

void __addToGlobalRootSet(int8* reference)
{
    return mt_proxies::object::addToGlobalObjectRootSet(reference);
}

void __removeFromGlobalRootSet(int8* reference)
{
    return mt_proxies::object::removeFromGlobalObjectRootSet(reference);
}

// dynamic cast
byte* __dynCastImpl(byte* object, int64 target_type_id)
{
    return mt_proxies::object::dynCast(object, target_type_id);
}

} }
