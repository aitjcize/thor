/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstddef>

#include <limits>
#include <map>
#include <tuple>
#include <utility>

#include <boost/mpl/map.hpp>
#include <boost/mpl/integral_c.hpp>

#include "thor/Thor.h"
#include "thor/lang/Replication.h"

#include "framework/service/runtime/RuntimeServiceBuffer.h"
#include "framework/service/object/ObjectServiceBuffer.h"

namespace thor { namespace lang {

namespace mt_proxies = ::zillians::framework::service::mt::proxies;

namespace {

    using namespace thor::lang;

    struct ReplicationBaseMemberTypes
    {
        using Target = ReplicationBase;
        using Location = Target::Buffer::buffer_location_t;

        template < Target::PrimitiveTypeHeader Value >
        using constant = boost::mpl::integral_c< zillians::byte, (zillians::byte)Value >;

        using primitive_type_header_constants = boost::mpl::map<
            boost::mpl::pair< bool,    constant<Target::PrimitiveTypeHeader::Bool>    >,
            boost::mpl::pair< int8,    constant<Target::PrimitiveTypeHeader::Int8>    >,
            boost::mpl::pair< int16,   constant<Target::PrimitiveTypeHeader::Int16>   >,
            boost::mpl::pair< int32,   constant<Target::PrimitiveTypeHeader::Int32>   >,
            boost::mpl::pair< int64,   constant<Target::PrimitiveTypeHeader::Int64>   >,
            boost::mpl::pair< float32, constant<Target::PrimitiveTypeHeader::Float32> >,
            boost::mpl::pair< float64, constant<Target::PrimitiveTypeHeader::Float64> >
        >::type;
    };

    template < typename T >
    bool put_impl( ReplicationEncoder::Buffer& output, const T& object )
    {
        return sizeof(object) == output.write( reinterpret_cast<const zillians::byte*>(&object),
                                               sizeof(object) );
    }

    // ReplicationData related manipulators
    template < typename Primitive >
    bool put_with_header( ReplicationEncoder::Buffer& output, const Primitive& value )
    {
        using namespace boost;

        using headers_type = ReplicationBaseMemberTypes::primitive_type_header_constants;
        static_assert( mpl::has_key< headers_type, Primitive >::value, "unsupported type" );

        // get asscociated header value
        auto header = (ReplicationBase::PrimitiveTypeHeader)mpl::at< headers_type, Primitive >::type::value;

        // write header & entity
        return put_impl( output, header ) && put_impl( output, value );
    }

    template < typename T >
    T peek( ReplicationDecoder::Buffer& input )
    {
        T object;
        input.peek( reinterpret_cast<zillians::byte*>(&object), sizeof(object) );

        return object;
    }

    template < typename T >
    T get( ReplicationDecoder::Buffer& input )
    {
        T object;
        input.read( reinterpret_cast<zillians::byte*>(&object), sizeof(object) );

        return object;
    }

    ReplicationBase::TypeIdentifier get_type_id( Object* object )
    {
        const auto * tracker_header = GET_RAW_BUFFER(object)->tracker_ptr;

        return tracker_header->type_id;
    }
}

ReplicationBase::ReplicationBase()
: content( new ReplicationBase::Buffer(
               ReplicationBaseMemberTypes::Location::host_unpinned
           ) )
{ }

ReplicationBase::ReplicationBase( Buffer* buffer )
: content( buffer )
{ } ReplicationBase::~ReplicationBase()
{ delete content; }

ReplicationEncoder::ReplicationEncoder()
: serialization_identifiers( new ObjectToSerializationIdentifier )
{ }

ReplicationEncoder::~ReplicationEncoder()
{
    delete serialization_identifiers;
}

bool ReplicationEncoder::put(std::size_t count, const void* buffer)
{
    if (std::numeric_limits<int64>::max() < count)
        return false;

    if (!put_impl(*content, PrimitiveTypeHeader::Raw))
        return false;

    if (!put_with_header(*content, static_cast<int64>(count)))
        return false;

    const auto write_count = content->write(reinterpret_cast<const byte*>(buffer), count);

    if (write_count != count)
        return false;

    return true;
}

auto ReplicationEncoder::stolenContent() noexcept -> Buffer*
{
    auto give_out = content;
    content = nullptr;
    return give_out;
}

bool ReplicationEncoder::put(bool b)
{
    return put_with_header( *content, b );
}

bool ReplicationEncoder::put(int8 i8)
{
    return put_with_header( *content, i8 );
}

bool ReplicationEncoder::put(int16 i16)
{
    return put_with_header( *content, i16 );
}

bool ReplicationEncoder::put(int32 i32)
{
    return put_with_header( *content, i32 );
}

bool ReplicationEncoder::put(int64 i64)
{
    return put_with_header( *content, i64 );
}

bool ReplicationEncoder::put(float32 f32)
{
    return put_with_header( *content, f32 );
}

bool ReplicationEncoder::put(float64 f64)
{
    return put_with_header( *content, f64 );
}

bool ReplicationEncoder::putReference( SerializationIdentifier id )
{
    return put_impl( *content, ObjectMarker::Reference ) &&
           put_impl( *content, id                      );
}

bool ReplicationEncoder::putNull()
{
    return put_impl( *content, ObjectMarker::Null );
}

bool ReplicationEncoder::put( Object* object )
{
    using namespace zillians::framework;
    using namespace zillians::framework::service::mt;

    if( object == nullptr )
        return putNull();

    // (1) determine if this object was serialized
    bool serialized;
    SerializationIdentifier serialization_identifier;
    std::tie( serialized, serialization_identifier ) = get_serialization_id( object );
    if( serialized )
        return putReference( serialization_identifier );

    // (2) this object have not been serialized, prepare to call it's method 'serialize'
    if( !put_impl( *content, ObjectMarker::Begin        ) ||
        !put_with_header( *content, get_type_id(object) ) )
    {
        return false;
    }

    auto serializer = mt_proxies::runtime::getSerializer( get_type_id(object) );
    BOOST_ASSERT( serializer && "can not file serializer for this type" );

    return serializer( this, object ) &&
           put_impl( *content, ObjectMarker(ObjectMarker::End) );
}

std::pair< bool, ReplicationEncoder::SerializationIdentifier >
ReplicationEncoder::get_serialization_id( Object* object )
{
    auto previous_serialized_count = serialization_identifiers->size();
    SerializationIdentifier identifier = previous_serialized_count;

    bool successfully_insert;
    ObjectToSerializationIdentifier::iterator object_and_identifier;

    std::tie( object_and_identifier, successfully_insert ) = serialization_identifiers->insert(
        std::make_pair( object, identifier )
    );

    return std::make_pair(
        !successfully_insert, // already serialized
        std::get<1>( *object_and_identifier )
    );
}

ReplicationDecoder::ReplicationDecoder()
: deserialized_objects( new SerializationIdentifierToObject )
{ }

ReplicationDecoder::ReplicationDecoder( ReplicationEncoder* encoder )
: ReplicationBase( encoder->stolenContent() ),
  deserialized_objects( new SerializationIdentifierToObject )
{
    content->resetRead();
}

ReplicationDecoder::~ReplicationDecoder()
{ delete deserialized_objects; }

bool ReplicationDecoder::isEnded()
{
    return content->reach_end_of_data();
}

void ReplicationDecoder::setContent(Buffer& new_content)
{
    delete content;
    content = &new_content;
    content->resetRead();
}

#define DEFINE_DECODER_IS_AND_GET(type, name)                                    \
    template<>                                                                   \
    bool ReplicationDecoder::isNext<type>(type)                                  \
    {                                                                            \
        return isNext ## name();                                                 \
    }                                                                            \
    bool ReplicationDecoder::isNext ## name()                                    \
    {                                                                            \
        return peek<PrimitiveTypeHeader>(*content) == PrimitiveTypeHeader::name; \
    }                                                                            \
    template<>                                                                   \
    std::pair<bool, type> ReplicationDecoder::nativeGet<type>(type)              \
    {                                                                            \
        return nativeGet ## name();                                              \
    }                                                                            \
    std::pair<bool, type> ReplicationDecoder::nativeGet ## name()                \
    {                                                                            \
        if (!isNext ## name())                                                   \
            return {false, type()};                                              \
                                                                                 \
        type result = nativeGet ## name ## Impl();                               \
                                                                                 \
        return {true, result};                                                   \
    }                                                                            \
    type ReplicationDecoder::nativeGet ## name ## Impl()                         \
    {                                                                            \
        PrimitiveTypeHeader header = get<PrimitiveTypeHeader>(*content);         \
        assert(header == PrimitiveTypeHeader::name && "invalid header");         \
                                                                                 \
        type result = get<type>(*content);                                       \
                                                                                 \
        return result;                                                           \
    }

DEFINE_DECODER_IS_AND_GET(bool   , Bool   )
DEFINE_DECODER_IS_AND_GET(int8   , Int8   )
DEFINE_DECODER_IS_AND_GET(int16  , Int16  )
DEFINE_DECODER_IS_AND_GET(int32  , Int32  )
DEFINE_DECODER_IS_AND_GET(int64  , Int64  )
DEFINE_DECODER_IS_AND_GET(float32, Float32)
DEFINE_DECODER_IS_AND_GET(float64, Float64)

#undef DEFINE_DECODER_IS_AND_GET

bool ReplicationDecoder::isNextNull()
{
    const auto& marker = peek<ObjectMarker>(*content);

    return marker == ObjectMarker::Null;
}

template<>
bool ReplicationDecoder::isNext<Object*>(Object*)
{
    return isNextObject();
}

bool ReplicationDecoder::isNextObject()
{
    const auto& marker = peek<ObjectMarker>(*content);

    return  marker == ObjectMarker::Reference ||
            marker == ObjectMarker::Begin;
}

template<>
std::pair<bool, Object*> ReplicationDecoder::nativeGet<Object*>(Object*)
{
    return nativeGetObject();
}

std::pair<bool, Object*> ReplicationDecoder::nativeGetObject()
{
    const auto& marker = peek<ObjectMarker>(*content);

    switch (marker)
    {
    case ObjectMarker::Reference:
    case ObjectMarker::Begin    :
        break;

    case ObjectMarker::Null:
        get<ObjectMarker>(*content);
        return { true, nullptr};

    default:
        return {false, nullptr};
    }

    Object*const object = nativeGetObjectImpl();

    return {object != nullptr, object};
}

void ReplicationDecoder::eatNullMark()
{
    const auto& marker = get<ObjectMarker>(*content);

    BOOST_ASSERT(marker == ObjectMarker::Null && "we expect marker is null here");
}

Object* ReplicationDecoder::nativeGetObjectImpl()
{
    const auto& marker = get<ObjectMarker>(*content);

    switch (marker)
    {
    case ObjectMarker::Reference:
    {
        const auto& id    = get<SerializationIdentifier>(*content);
        const auto& found = deserialized_objects->find(id);

        BOOST_ASSERT(found != deserialized_objects->end() && "can not find previous deserialized object");
        if (found == deserialized_objects->end())
            return nullptr;

        return found->second;
    }

    case ObjectMarker::Begin:
        break;

    default:
        return nullptr;
    }

    const auto& type_id_result = nativeGetInt64();
    const auto& type_id_found  = type_id_result.first;
    const auto& type_id        = type_id_result.second;

    if (!type_id_found)
        return nullptr;

    // (1) prepare to call object's creator
    // (2) prepare to call object's deserializer
    auto*const creator      = mt_proxies::runtime::getCreator(type_id);
    auto*const deserializer = mt_proxies::runtime::getDeserializer(type_id);

    if (creator == nullptr)
        return nullptr;

    if (deserializer == nullptr)
        return nullptr;

    Object*const object = creator();

    // pre register to prevent self reference
    registerDeserialized(object);

    const auto& is_deserialized_successfully = deserializer(this, object);

    if (!is_deserialized_successfully)
        return nullptr;

    const auto& end_marker = get<ObjectMarker>(*content);

    if (end_marker != ObjectMarker::End)
        return nullptr;

    return object;
}

std::pair<std::size_t, bool> ReplicationDecoder::eatRawBegin()
{
    if (get<PrimitiveTypeHeader>( *content ) != PrimitiveTypeHeader::Raw )
        return {0, false};

    if (!isNextInt64())
        return {0, false};

    const auto& raw_size_result = nativeGetInt64();
    const auto& raw_size_found  = raw_size_result.first;
    const auto& raw_size        = raw_size_result.second;

    assert(raw_size_found && "raw buffer size missing");
    if (!raw_size_found)
        return {0, false};

    assert(raw_size >= 0 && "buffer size should not be negative integer!");
    if (raw_size < 0)
        return {0, false};

    if (std::numeric_limits<std::size_t>::max() < raw_size)
        return {0, false};

    return {static_cast<std::size_t>(raw_size), true};
}

std::size_t ReplicationDecoder::getRawData(std::size_t count, void* buffer)
{
    return content->read(reinterpret_cast<byte*>(buffer), count);
}

void ReplicationDecoder::registerDeserialized( Object* object )
{
    SerializationIdentifier identifier = deserialized_objects->size();
    deserialized_objects->insert(
        std::make_pair( identifier, object )
    );
}

} }
