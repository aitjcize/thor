/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstdlib>
#include <ctime>

#include <boost/preprocessor/seq/for_each.hpp>

#include "thor/lang/Process.h"
#include "thor/lang/Domain.h"
#include "thor/lang/Lambda.h"
#include "thor/lang/Replication.h"
#include "thor/container/Vector.h"
#include "framework/service/runtime/RuntimeServiceBuffer.h"
#include "rstm/api/library.hpp"

namespace thor { namespace lang {

namespace mt_proxies = ::zillians::framework::service::mt::proxies;

namespace {

int32 getTargetId(int64 session_id)
{
    return session_id == -1 ? 0 : session_id >> 32;
}

}

void exit(int32 exit_code)
{
    mt_proxies::runtime::exit(exit_code);
}

int32 __getCurrentInvocationId()
{
	int32 id = mt_proxies::runtime::getCurrentInvocationId();
//	std::cerr << "get current invocation id = " << id << std::endl;
	return id;
}

int64 __getCurrentFunctionId(int32 invocation_id)
{
    int64 id = mt_proxies::runtime::getCurrentInvocations()->getFunctionId(invocation_id);
//    std::cerr << "#" << invocation_id << " function-id = " << id << std::endl;
    return id;
}

int64 __getCurrentSessionId(int32 invocation_id)
{
    return mt_proxies::runtime::getCurrentInvocations()->getSessionId(invocation_id);
}

char* __getCurrentParameterPtr(int32 invocation_id)
{
    return mt_proxies::runtime::getCurrentInvocations()->getInvocationPtr(invocation_id);
}

void __postponeInvocationDependencyTo(int32 postpone_to_id)
{
    const auto*const local_domain = Domain::local();

    const int64 session_id = local_domain->session_id;
    const int32  target_id = getTargetId(session_id);

    return mt_proxies::runtime::postponeInvocationDependencyTo(target_id, postpone_to_id);
}

void __setInvocationDependOnCount(int32 invocation_id, int32 count)
{
    const auto*const local_domain = Domain::local();

    const int64 session_id = local_domain->session_id;
    const int32  target_id = getTargetId(session_id);

    return mt_proxies::runtime::setInvocationDependOnCount(target_id, invocation_id, count);
}

void __setInvocationDependOn(int32 invocation_id, int32 depend_on_id)
{
    const auto*const local_domain = Domain::local();

    const int64 session_id = local_domain->session_id;
    const int32  target_id = getTargetId(session_id);

    return mt_proxies::runtime::setInvocationDependOn(target_id, invocation_id, depend_on_id);
}

int32 __addInvocation(int64 function_id, int8* ret_ptr)
{
    const auto*const local_domain = Domain::local();

    const int64 session_id = local_domain->session_id;
    const int32  target_id = getTargetId(session_id);

    return mt_proxies::runtime::addInvocation(target_id, session_id, function_id, ret_ptr);
}

int32 __reserveInvocation(int64 function_id)
{
    const Domain*const local_domain = Domain::local();

    const int64 session_id = local_domain->session_id;
    const int32  target_id = getTargetId(session_id);

    return mt_proxies::runtime::reserveInvocation(target_id, session_id, function_id);
}

bool __commitInvocation(int32 id)
{
    const Domain*const local_domain = Domain::local();

    const int64 session_id = local_domain->session_id;
    const int32  target_id = getTargetId(session_id);

    return mt_proxies::runtime::commitInvocation(target_id, id);
}

bool __abortInvocation(int32 id)
{
    const Domain*const local_domain = Domain::local();

    const int64 session_id = local_domain->session_id;
    const int32  target_id = getTargetId(session_id);

    return mt_proxies::runtime::abortInvocation(target_id, id);
}

void __setInvocationReturnPtr(int32 id, void* ptr)
{
    const Domain*const local_domain = Domain::local();

    const int64 session_id = local_domain->session_id;
    const int32  target_id = getTargetId(session_id);

    return mt_proxies::runtime::setInvocationReturnPtr(target_id, id, ptr);
}

#define PARAMETER_VALUE_TYPES          \
    (bool   )                          \
    (int8   )                          \
    (int16  )                          \
    (int32  )                          \
    (int64  )                          \
    (float32)                          \
    (float64)                          \
    (int8*  ) /* use 'int8*' in order to work with interfaces */

#define APPEND_INVOCATION_PARAM_DEF(r, _, type)                  \
    int64 __appendInvocationParameter(                           \
        int32 id    ,                                            \
        int64 offset,                                            \
        type  value                                              \
    )                                                            \
    {                                                            \
        const Domain*const local_domain = Domain::local();       \
                                                                 \
        const int64 session_id = local_domain->session_id;       \
        const int32  target_id = getTargetId(session_id);        \
                                                                 \
        return mt_proxies::runtime::appendInvocationParameter(   \
            target_id,                                           \
            id,                                                  \
            offset,                                              \
            value                                                \
        );                                                       \
    }

BOOST_PP_SEQ_FOR_EACH(APPEND_INVOCATION_PARAM_DEF, _, PARAMETER_VALUE_TYPES)

#undef APPEND_INVOCATION_PARAM_DEF
#undef PARAMETER_VALUE_TYPES

bool __invokeRemotely(Domain* domain, int64 adaptor_id, ReplicationEncoder* encoder)
{
    assert(encoder != nullptr && "parameter encoding failure!");
    if (encoder == nullptr)
        return false;

    const int64 session_id = domain->session_id;
    const int32  target_id = getTargetId(session_id);
    auto*const     data    = encoder->stolenContent();

    mt_proxies::runtime::addInvocation(target_id, session_id, adaptor_id, data);

    return true;
}

void __remoteInvoker(byte* parameter)
{
    using zillians::framework::service::ReplicationData;

    auto*const param_buffer = reinterpret_cast<decltype(zillians::framework::service::Invocation::buffer)*>(parameter);
    auto&      remote_info  = param_buffer->remote_info;

    ReplicationData*& data       = remote_info.data;
    int64             adaptor_id = remote_info.adaptor_id;

    auto adaptor = mt_proxies::runtime::getRemoteAdaptor(adaptor_id);

    if (adaptor == nullptr)
        return;

    ReplicationDecoder*const decoder = ReplicationDecoder::__create();

    decoder->setContent(*data);
    data = nullptr;

    adaptor(decoder);
}

void __invokeKernel(Object* domain, int64 function_id, int32 dim_x, int32 dim_y, int32 dim_z, Lambda0<void>* callback)
{

}

void __RSTMSystemInitialize()
{
    TM_SYS_INIT();
}

void __resetRandomSeed()
{
    srand((unsigned)time(nullptr));
}

} }

namespace stm {
    template bool         stm_read<bool        >(bool        *, TxThread*);
    template std::int8_t  stm_read<std::int8_t >(std::int8_t *, TxThread*);
    template std::int32_t stm_read<std::int32_t>(std::int32_t*, TxThread*);
    template std::int64_t stm_read<std::int64_t>(std::int64_t*, TxThread*);
    template float        stm_read<float       >(float       *, TxThread*);
    template double       stm_read<double      >(double      *, TxThread*);
    template std::int8_t* stm_read<std::int8_t*>(std::int8_t**, TxThread*);

    template void stm_write<bool        >(bool        *, bool        , TxThread*);
    template void stm_write<std::int8_t >(std::int8_t *, std::int8_t , TxThread*);
    template void stm_write<std::int32_t>(std::int32_t*, std::int32_t, TxThread*);
    template void stm_write<std::int64_t>(std::int64_t*, std::int64_t, TxThread*);
    template void stm_write<float       >(float       *, float       , TxThread*);
    template void stm_write<double      >(double      *, double      , TxThread*);
    template void stm_write<std::int8_t*>(std::int8_t**, std::int8_t*, TxThread*);
}

