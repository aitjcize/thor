/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/assert.hpp>

#include "thor/lang/Domain.h"
#include "thor/lang/Process.h"

#include "framework/service/detail/RootSetPtr.h"
#include "framework/service/domain/DomainServiceBuffer.h"

namespace thor {

namespace lang {

namespace mt_proxies = ::zillians::framework::service::mt::proxies;

using ::zillians::framework::service::mt::root_set_ptr;
using ::zillians::framework::service::mt::root_set_ptr_base;
using ::zillians::framework::service::mt::make_root_set_ptr;

// TODO implementation note
// the domain can be created through ManagementEngine
// and can be queried through DomainRepository
//
// DomainRepository provides API to add/remove remote Domain configuration
// DomainRepository may use ClusterEngine to monitor memberships of remote Domain (if ClusterEngine is present)
//
// we need to create ProcessorForwarder which sees remote domain id (client id?)
// for every cross-domain call, there's target id and domain id
// by using target id we can look-up a processor to process the request,
// by using domain we know how the request should be forwarded to (if the processor is ProcessorForwarder)
//
// Object can be pushed or pulled from one domain to another in flat or recursive manner
// or send as parameters by using @byval annotation on declared parameters
//
// we can have non-fixed-size invocation buffer?
// slow on GPU but may not be slow on CPU
// (use vote and data shuffle to reduce the overhead and minimize memory transaction?)
//
// TODO think about conversion rank for two templated identifiers
//

namespace {

root_set_ptr<Domain> getDomainObject(int64 session_id)
{
    root_set_ptr<Domain> d(mt_proxies::domain::find(session_id));

    if (d == nullptr)
    {
        d.reset(Domain::create());
        d->setSessionId(session_id);

        d.reset(mt_proxies::domain::insert(session_id, d.get()));
    }

    return d;
}

}

Domain::Domain() : session_id(-1)
{
}

Domain::~Domain()
{
}

Domain* Domain::local()
{
    const auto& d = getDomainObject(-1);

    return d.get();
}

Domain* Domain::caller()
{
    const auto& invocation_id = __getCurrentInvocationId();
    const auto&    session_id = __getCurrentSessionId(invocation_id);
    const auto&             d = getDomainObject(session_id);

    return d.get();
}

int64 Domain::getSessionId()
{
    return session_id;
}

void Domain::setSessionId(int64 id)
{
    session_id = id;
}

::thor::util::UUID* Domain::listen(String* endpoint, ConnCallback* conn_cb, ConnCallback* disconn_cb, ErrCallback* err_cb)
{
    if (endpoint == nullptr)
        return ::thor::util::UUID::nil();

    BOOST_ASSERT(endpoint->data != nullptr && "null pointer exception");

    ::thor::util::UUID*const thor_id = ::thor::util::UUID::__create();
    boost::uuids::uuid            id = mt_proxies::domain::listen(*endpoint->data, conn_cb, disconn_cb, err_cb);

    thor_id->setId(std::move(id));

    return thor_id;
}

::thor::util::UUID* Domain::connect(String* endpoint, ConnCallback* conn_cb, ConnCallback* disconn_cb, ErrCallback* err_cb)
{
    if (endpoint == nullptr)
        return ::thor::util::UUID::nil();

    BOOST_ASSERT(endpoint->data != nullptr && "null pointer exception");

    ::thor::util::UUID*const thor_id = ::thor::util::UUID::__create();
    boost::uuids::uuid            id = mt_proxies::domain::connect(*endpoint->data, conn_cb, disconn_cb, err_cb);

    thor_id->setId(std::move(id));

    return thor_id;
}

bool Domain::cancel(::thor::util::UUID* id)
{
    if (id == nullptr)
        return false;

    return mt_proxies::domain::cancel(id->getId());
}

void Domain::__onConn() noexcept
{
    const auto       invocation_id = __getCurrentInvocationId();
          char*const parameter_ptr = __getCurrentParameterPtr(invocation_id);

    const auto& callback   = make_root_set_ptr(*reinterpret_cast<ConnCallback** >(parameter_ptr                                  ));
    const auto& uuid       =                   *reinterpret_cast<zillians::UUID*>(parameter_ptr + sizeof(callback)               ) ;
    const auto& session_id =                   *reinterpret_cast<int64*         >(parameter_ptr + sizeof(callback) + sizeof(uuid)) ;

    BOOST_ASSERT(callback != nullptr && "null pointer exception");

    const auto& thor_uuid   = make_root_set_ptr(::thor::util::UUID::fromId(uuid));
    const auto& thor_domain = getDomainObject(session_id);

    callback->invoke(thor_uuid.get(), thor_domain.get());

    root_set_ptr_base::rootSetRemove(callback.get());
}

void Domain::__onErr() noexcept
{
    const auto       invocation_id = __getCurrentInvocationId();
          char*const parameter_ptr = __getCurrentParameterPtr(invocation_id);

    const auto& callback = make_root_set_ptr(*reinterpret_cast<ErrCallback**  >(parameter_ptr                                  ));
    const auto& uuid     =                   *reinterpret_cast<zillians::UUID*>(parameter_ptr + sizeof(callback)               ) ;
    const auto& error    =                   *reinterpret_cast<DomainError*   >(parameter_ptr + sizeof(callback) + sizeof(uuid)) ;

    BOOST_ASSERT(callback != nullptr && "null pointer exception");

    const auto& thor_uuid = make_root_set_ptr(::thor::util::UUID::fromId(uuid));

    callback->invoke(thor_uuid.get(), error);

    root_set_ptr_base::rootSetRemove(callback.get());
}

} }
