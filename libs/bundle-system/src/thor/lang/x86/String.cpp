/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>

#include <functional>
#include <algorithm>
#include <sstream>
#include <new>
#include <iomanip>
#include <string>
#include <tuple>
#include <utility>

#include <boost/range/iterator_range.hpp>

#include "thor/lang/Language.h"
#include "thor/lang/Replication.h"
#include "thor/lang/String.h"
#include "thor/util/Macro.h"

#include "framework/processor/KernelMT.h"
#include "framework/service/runtime/RuntimeServiceBuffer.h"

#define PP_MAKE_MUTABLESTRING_CONCATE(type)                \
    MutableString* MutableString::concate(type value) {    \
        *(this->data) += thor::lang::lexical_cast(value);  \
        return this;                                       \
    }

#define PP_MAKE_LEXICAL_CAST(type)                         \
    std::wstring lexical_cast(type value) {                \
        std::wostringstream stream;                        \
        stream << value;                                   \
        return stream.str();                               \
    }

namespace thor { namespace lang {

std::wstring lexical_cast(bool value) {
    return (value ? L"true" : L"false");
}

PP_FOREACH_APPLY(
    PP_MAKE_LEXICAL_CAST,
    int8, int16, int32, int64, float32, float64
)

String::String() : data(new string_type())
{ }

String::~String()
{
    if (data)
    {
        delete data;
        data = nullptr;
    }
}

int64 String::hash()
{
    return std::hash<std::wstring>()(*data);
}

String* String::clone()
{
    return substring(0);
}

bool String::isEqual(String* rhs)
{
    return compareTo(rhs) == 0;
}

bool String::isEqual(String* rhs, bool ignore_case)
{
    return compareTo(rhs, ignore_case) == 0;
}

bool String::isLessThan(String* rhs)
{
    return compareTo(rhs) < 0;
}

int32 String::length()
{
    return (int32)data->length();
}

String* String::concate(String* other)
{
    String* obj = String::__create();
    *(obj->data) += *data;
    *(obj->data) += *(other->data);
    return obj;
}

int32 String::find(String* candidate)
{
    size_type t = data->find(*(candidate->data));
    if(t == string_type::npos)
        return -1;
    else
        return (int32)t;
}

int32 String::compareTo(String* other) 
{
    return data->compare( *(other->data) );
}

int32 String::compareTo(String* other, bool ignore_case)
{
    if (!ignore_case)
        return compareTo(other);

    String *lower_this = toLowerCase(),
           *lower_other = other->toLowerCase();

    return lower_this->compareTo( lower_other );
}

bool String::endsWith(String* suffix) 
{
    if( data->length() < suffix->data->length() )
        return false;

    size_type offset = data->length() - suffix->data->length();

    return traits_type::compare( suffix->data->c_str(), data->c_str() + offset,
                                 suffix->data->length() ) == 0;
}

int32 String::indexOf(String* str)
{
    return indexOf(str, 0);
}

int32 String::indexOf(String* str, int32 fromIndex)
{
    if( fromIndex < 0 || data->length() < fromIndex )
        return false;

    size_type first = data->find( *(str->data), (size_type)fromIndex );

    return (first == string_type::npos) ? -1 : (int32)first;
}

int32 String::lastIndexOf(String* str)
{
    return lastIndexOf(str, 0);
}

int32 String::lastIndexOf(String* str, int32 fromIndex)
{
    if( fromIndex < 0 || data->length() < fromIndex )
        return false;

    String * sub = substring( fromIndex );
    assert( sub );
    size_type last = sub->data->rfind( *(str->data) );

    return (last == string_type::npos) ? -1 : (int32)last;
}

bool String::startsWith(String* prefix)
{
    return startsWith(prefix, 0);
}

bool String::startsWith(String* prefix, int32 toffset)
{
    if (toffset < 0)
        return false;

    const auto this_len = length();

    if (this_len < toffset)
        return false;

    const auto this_remain_len = this_len - toffset;
    const auto prefix_len      = prefix->length();

    if (prefix_len > this_remain_len)
        return false;

    return traits_type::compare( prefix->data->c_str(), data->c_str() + toffset,
                                 prefix_len ) == 0;
}

String* String::substring(int32 beginIndex)
{
    return substring( beginIndex, data->length() );
}

String* String::substring(int32 beginIndex, int32 endIndex)
{
    if( beginIndex < 0 || endIndex < 0 || endIndex < beginIndex )
        return NULL;

    String* obj = String::__create();
    *(obj->data) = data->substr( (size_type)beginIndex, size_type(endIndex-beginIndex) );

    return obj;
}

namespace detail {

    template <typename CharT>
    inline CharT get_space();

    template <>
    inline wchar_t get_space()
    {
        return L' ';
    }

    template <>
    inline char get_space()
    {
        return ' ';
    }

}

String* String::trim()
{
    using namespace detail;

    String* obj = String::__create();

    typedef string_type::value_type value_type;

    string_type::const_iterator begin = data->begin();
    for(; begin != data->end(); ++begin) {
        if(traits_type::lt(get_space<value_type>(), *begin))
            break;
    }

    if( begin == data->end() )
        return obj;

    string_type::const_reverse_iterator rbegin = data->rbegin();
    for( ;rbegin != data->rend(); ++rbegin) {
        if(traits_type::lt(get_space<value_type>(), *rbegin))
            break;
    }

    obj->data->assign( begin, rbegin.base() );

    return obj;
}

String* String::toString()
{
    return substring(0);
}

bool String::regionMatches(int32 toffset, String* other, int32 ooffset, int32 len)
{
    return regionMatches(false, toffset, other, ooffset, len);
}

bool String::regionMatches(bool ignoreCase, int32 toffset, String* other, int32 ooffset, int32 len)
{
    if(toffset < 0 || ooffset < 0 || len < 0)
        return false;

    if(data->length() < toffset + len || other->data->length() < ooffset + len)
        return false;

    string_type::const_iterator source = data->begin() + (size_type)toffset,
                                target = other->data->begin() + (size_type)ooffset;

    if(ignoreCase) {

        std::locale const default_locale;
        for( ; len; --len ) {
            if( std::toupper(*source++, default_locale) != std::toupper(*target++, default_locale) )
                return false;
        }

        return true;

    } else {
        return std::equal( source, source + (size_type)len, 
                           target, traits_type::eq );
    }
}

String* String::toLowerCase()
{
    String* obj = String::__create();

    obj->data->resize(data->length());

    std::locale const default_locale;
    std::transform(data->begin(), data->end(), obj->data->begin(), 
                   std::bind(std::tolower<value_type>, std::placeholders::_1, default_locale));

    return obj;
}

String* String::toUpperCase()
{
    String* obj = String::__create();

    obj->data->resize(data->length());

    std::locale const default_locale;
    std::transform(data->begin(), data->end(), obj->data->begin(),
                   std::bind(std::toupper<value_type>, std::placeholders::_1, default_locale));

    return obj;
}

String* String::fromString(const std::wstring& str)
{
    String* instance = __create();
    (*instance->data) = str;
    return instance;
}

String* String::fromString(std::wstring&& str)
{
    String* instance = __create();
    (*instance->data) = std::move(str);
    return instance;
}

bool String::serialize(ReplicationEncoder* encoder, String* instance)
{
    if (!Object::serialize(encoder, instance))
        return false;

    const auto*const str = instance->data;

    assert(str != nullptr && "null pointer exception");

    constexpr auto       str_element_size = sizeof(value_type);
    const     auto       str_buffer_size  = str->size() * str_element_size;
    const     auto*const str_data         = str->c_str();

    return encoder->put(str_buffer_size, str_data);
}

bool String::deserialize(ReplicationDecoder* decoder, String* instance)
{
    if (!Object::deserialize(decoder, instance))
        return false;

    bool        is_raw   = false;
    std::size_t raw_size = 0;

    std::tie(raw_size, is_raw) = decoder->eatRawBegin();

    if (!is_raw)
        return false;

    auto*const str = instance->data;

    assert(str != nullptr && "null pointer exception");

    constexpr auto str_element_size = sizeof(value_type);

    if (raw_size % str_element_size != 0)
        return false;

    const auto str_buffer_size = raw_size / str_element_size;

    try
    {
        str->resize(str_buffer_size);
    }
    catch (const std::bad_alloc&)
    {
        return false;
    }

    const auto str_result_size = decoder->getRawData(raw_size, &str->front());
    assert(0 <= str_result_size && str_result_size <= raw_size && "unexpected result, should be null (failure) or value equal to str_bufer_size");

    if (str_result_size == raw_size)
        return true;

    // failed, clear buffer
    str->clear();

    return false;
}

MutableString::MutableString(String* s)
{
    *(this->data) = *(s->data);
}

MutableString::~MutableString()
{ }

MutableString* MutableString::clone()
{
    auto       new_data     = boost::copy_range<string_type>(*data);
    auto*const new_instance = __create();

    *new_instance->data = std::move(new_data);

    return new_instance;
}

MutableString* MutableString::concate(String* other)
{
    *(this->data) += *(other->data);
    return this;
}

String* __getStringLiteral(int64 symbol_id)
{
    String* obj = String::__create();

    const std::wstring*const data = zillians::framework::service::mt::proxies::runtime::queryStringLiteral(symbol_id);

    if (data != nullptr)
        *obj->data = *data;

    return obj;
}

PP_FOREACH_APPLY(
    PP_MAKE_MUTABLESTRING_CONCATE,
    bool, int8, int16, int32, int64, float32, float64
)

} } // namespace thor::lang
