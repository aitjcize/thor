/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_LANG_DOMAIN_CUH_
#define THOR_LANG_DOMAIN_CUH_

#include "thor/Thor.cuh"
#include "thor/lang/Language.cuh"

namespace thor { namespace lang {

enum DomainEvent {
	Connected,
	Disconnected,
	Unreachable,
	Resumed,
	Crashed,
};

class Domain : public Object
{
	__device__ Domain();
public:
	__device__ virtual ~Domain();

public:
	__device__ static Domain* create();
	__device__ static Domain* local();
	__device__ static Domain* caller();

public:
	__device__ void setSessionId(int64 id);

public:
//	typedef void (*DomainEventCallback)(Domain*);
//	static void watch(DomainEvent e, DomainEventCallback callback);
//	__device__ static void watch(int32 e, Lambda1<void, Domain>* callback);

public:
	int64 session_id;
};

} }

#endif /* THOR_LANG_DOMAIN_CUH_ */
