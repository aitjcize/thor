/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_LANG_STRING_H_
#define THOR_LANG_STRING_H_

#include "thor/Thor.h"
#include "thor/lang/Language.h"
#include "thor/lang/Replication.h"
#include "thor/util/Macro.h"

#define PP_MAKE_LEXICAL_CAST_DECL(type)     \
    std::wstring lexical_cast(type value);


namespace thor { namespace lang {

PP_FOREACH_APPLY(
    PP_MAKE_LEXICAL_CAST_DECL,
    bool, int8, int16, int32, int64, float32, float64
)

class MutableString;

class String : public Object, public Cloneable
{
public:
    String();
    virtual ~String();

public:
    static String* __create();

public:
    int64 hash();
    bool isEqual(String* rhs);
    bool isEqual(String* rhs, bool ignore_case);
    bool isLessThan(String* rhs);

public:
    int32 length();
    String* clone();
    String* concate(String* other);
    int32 find(String* candidate);
    int32 compareTo(String* other);
    int32 compareTo(String* other, bool ignore_case);
    bool endsWith(String* suffix);
    int32 indexOf(String* str);
    int32 indexOf(String* str, int32 fromIndex);
    int32 lastIndexOf(String* str);
    int32 lastIndexOf(String* str, int32 fromIndex);
    bool regionMatches(bool ignoreCase, int32 toffset, String* other, int32 ooffset, int32 len);
    bool regionMatches(int32 toffset, String* other, int32 ooffset, int32 len);
    bool startsWith(String* prefix);
    bool startsWith(String* prefix, int32 toffset);
    String* substring(int32 beginIndex);
    String* substring(int32 beginIndex, int32 endIndex);
    String* toLowerCase();
    String* toUpperCase();
    String* trim();
    String* toString();

    MutableString* __add__(bool    other);
    MutableString* __add__(int8    other);
    MutableString* __add__(int16   other);
    MutableString* __add__(int32   other);
    MutableString* __add__(int64   other);
    MutableString* __add__(float32 other);
    MutableString* __add__(float64 other);
    MutableString* __add__(String* other);

protected:
    static bool serialize  (ReplicationEncoder* encoder, String* instance);
    static bool deserialize(ReplicationDecoder* decoder, String* instance);

public:
    //For C++ only
    static String* fromString(const std::wstring& str);
    static String* fromString(std::wstring&& str);

public:
    typedef std::wstring string_type;
    typedef string_type::value_type value_type;
    typedef string_type::size_type size_type;
    typedef std::char_traits<value_type> traits_type;

    string_type* data;
};

class MutableString : public String
{
public:
    MutableString(String*);
    virtual ~MutableString();

    virtual MutableString* clone() override;

protected:
    static MutableString* __create();

public:
    MutableString* concate(bool    other);
    MutableString* concate(int8    other);
    MutableString* concate(int16   other);
    MutableString* concate(int32   other);
    MutableString* concate(int64   other);
    MutableString* concate(float32 other);
    MutableString* concate(float64 other);
    MutableString* concate(String* other);

    MutableString* __add__(bool    other);
    MutableString* __add__(int8    other);
    MutableString* __add__(int16   other);
    MutableString* __add__(int32   other);
    MutableString* __add__(int64   other);
    MutableString* __add__(float32 other);
    MutableString* __add__(float64 other);
    MutableString* __add__(String* other);
};

String* __getStringLiteral(int64 symbol_id);

} }

#undef PP_MAKE_LEXICAL_CAST_DECL

#endif /* THOR_LANG_STRING_H_ */
