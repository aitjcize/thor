/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

import . = thor.container;
import . = thor.util;

/**
 * @brief Define possible errors by Domain APIs.
 */
@cpu @system enum DomainError {
    OK                     , /**< No error. */
    INVALID_ENDPOINT_SPEC  , /**< Incorrect end-point specified by string. */
    UNSUPPORTED_TRANSPORT  , /**< Unsupported protocol specified by end-point string. */
    EXCEED_CONCURRENT_LIMIT, /**< Run out of concurrent limit (hard limit) of domain connections. */
    NO_SUCH_REQUEST        , /**< ID specified by user is not recognized as any Domain session. */
    UNKNOWN_ERROR            /**< Other errors. */
}

/**
 * @brief Connection callback for Domain APIs.
 *
 * @remark This callback is used in both connect and disconnect.
 */
@cpu typedef lambda(thor.util.UUID, Domain     ): void DomainConnCallback;

/**
 * @brief Error callback for Domain APIs.
 */
@cpu typedef lambda(thor.util.UUID, DomainError): void DomainErrCallback;

/**
 * @brief Class represents the virtual target for program execution.
 *
 * In Thor, program execution could be dispatched to any possible
 * targets. That is, a function call could be decided to run on CPU or GPU at
 * runtime. @n
 *
 * This class collects the required functionality for that:
 * - local() and caller() to retrieve Domain object represents the target of
 *   local and invoker.
 * - type() to retrieve the type of Domain, for example, single-threaded or
 *   multi-threaded.
 * - listen, connect, and cancel for inter-domain communication.
 * .
 */
@cpu
@native { include="thor/lang/Domain.h" }
class Domain extends Object
{
    /**
     * @brief Function to retrieve instance represents local target.
     *
     * @return The instance represents local target.
     *
     * @remark This function will not return @c null.
     *
     * @see caller()
     */
    @native
    public static function local():Domain;

    /**
     * @brief Function to retrieve instance represents target of asynchronous caller.
     *
     * In Thor, the asynchronous caller means the caller who requests
     * to execute a asynchronous function. @n
     *
     * For example, in the following code:
     * @code
     * function callee_inner()
     * {
     *     const caller_domain: Domain = Domain.caller();
     * }
     * function callee()
     * {
     *     const caller_domain: Domain = Domain.caller();
     *     callee_inner();
     * }
     * function caller()
     * {
     *     const requester: Domain = Domain.local();
     *     const executor : Domain = ...; // get the execution target
     *     async[executor] -> callee();
     * }
     * @endcode
     *
     * @c caller_domain in both @c callee() and @c callee_inner() should be
     * logically equivalent to @c requester in @c caller().
     *
     * @return The instance represents target of asynchronous caller.
     *
     * @remark This function will not return @c null.
     *
     * @see local()
     */
    @native
    public static function caller():Domain;

    /**
     * @brief Function to listen for connection from other domains.
     *
     * By design, user needs to specifies callbacks with string which specifies
     * end point to listen on. Then Thor VM could notify user through
     * callbacks on events, in order to avoid long-run functions. @n
     *
     * To stop listening, pass the returned identifier to Domain.cancel. @n
     *
     * @arg endpoint   URI string specifies the end point to listen on.
     * @arg conn_cb    Callback to be called on connected.
     * @arg disconn_cb Callback to be called on disconnected.
     * @arg err_cb     Callback to be called on errors.
     *
     * @return Identifier for this listen request.
     * @retval nil    Fail to listen.
     * @retval others Listen is started.
     *
     * @remark This function will not return @c null.
     *
     * @see connect
     * @see cancel
     */
    @native public static function listen(endpoint: String, conn_cb: DomainConnCallback, disconn_cb: DomainConnCallback, err_cb: DomainErrCallback): thor.util.UUID;

    /**
     * @brief Function to connect to remote domains.
     *
     * By design, user needs to specifies callbacks with string which specifies
     * end point to connect to. Then Thor VM could notify user through
     * callbacks on events, in order to avoid long-run functions. @n
     *
     * To disconnect, pass the returned identifier to Domain.cancel. @n
     *
     * @arg endpoint   URI string specifies the end point to connect to.
     * @arg conn_cb    Callback to be called on connected.
     * @arg disconn_cb Callback to be called on disconnected.
     * @arg err_cb     Callback to be called on errors.
     *
     * @return Identifier for this connection request.
     * @retval nil    Fail to connect to domain.
     * @retval others Connected to remote domain.
     *
     * @remark This function will not return @c null.
     *
     * @see listen
     * @see cancel
     */
    @native public static function connect(endpoint: String, conn_cb: DomainConnCallback, disconn_cb: DomainConnCallback, err_cb: DomainErrCallback): thor.util.UUID;

    /**
     * @brief Function to cancel a connected or listening session.
     *
     * For each connected or listening sessions ( @a Domain.listen or
     * @a Domain.connect), user could abort it by this function.
     *
     * @arg id The identifier represents connected or listening session.
     *
     * @return Boolean value indicates the session is successfully canceled or
     *         not.
     * @retval true  Successfully canceled.
     * @retval false Corresponding session not found.
     *
     * @see listen
     * @see connect
     */
    @native public static function cancel(id: thor.util.UUID): bool;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    @native private function new():void;
    @native private function delete():void;

    private static function create():Domain {
        var obj:Domain = new Domain();
        return obj;
    }

    @native private function getSessionId():int64;
    @native private function setSessionId(id: int64):void;

    @export @native private static function __onConn(): void;
    @export @native private static function __onErr(): void;

    private var session_id:int64;
    /**
     * @endcond
     */
}
