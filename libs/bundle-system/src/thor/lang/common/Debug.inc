/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "thor/lang/Debug.h"

#include <stdio.h>

namespace thor { namespace lang {

CUDA_DEVICE void print(bool v)
{
    if(v)
        printf("true");
    else
        printf("false");
}

CUDA_DEVICE void print(int8 v)
{
    printf("%d", (int32)v);
}

CUDA_DEVICE void print(int16 v)
{
    printf("%d", (int32)v);
}

CUDA_DEVICE void print(int32 v)
{
    printf("%d", (int32)v);
}

CUDA_DEVICE void print(int64 v)
{
    printf("%lld", v);
}

CUDA_DEVICE void print(float32 v)
{
    printf("%f", v);
}

CUDA_DEVICE void print(float64 v)
{
    printf("%f", v);
}

#ifndef __CUDACC__

CUDA_DEVICE void print(String* v)
{
    if(!v->data)
    {
        std::cerr << "Error: invalid string data used in print, string object ptr = " << v << std::endl;
    }
    else
    {
        const auto*const v_data = v->data;

        printf("%ls", v_data->c_str());
    }
}

#endif // __CUDACC__

CUDA_DEVICE void println(bool v)
{
    if(v)
        printf("true\n");
    else
        printf("false\n");
}

CUDA_DEVICE void println(int8 v)
{
    printf("%d\n", (int32)v);
}

CUDA_DEVICE void println(int16 v)
{
    printf("%d\n", (int32)v);
}

CUDA_DEVICE void println(int32 v)
{
    printf("%d\n", (int32)v);
}

CUDA_DEVICE void println(int64 v)
{
    printf("%lld\n", v);
}

CUDA_DEVICE void println(float32 v)
{
    printf("%f\n", v);
}

CUDA_DEVICE void println(float64 v)
{
    printf("%f\n", v);
}

#ifndef __CUDACC__

CUDA_DEVICE void println(String* v)
{
    if(!v->data)
    {
        std::cerr << "Error: invalid string data used in print, string object ptr = " << v << std::endl;
    }
    else
    {
        const auto*const v_data = v->data;

        printf("%ls\n", v_data->c_str());
    }
}

#endif // __CUDACC__

} }
