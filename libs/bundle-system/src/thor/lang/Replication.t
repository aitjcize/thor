/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

import thor.unmanaged;

/**
 * @brief Base class carries common data in both replication encoding and decoding.
 *
 * This class is for @b ReplicationEncoder and @b ReplicationDecoder only.
 * Users should not use this class directly.
 *
 * @see ReplicationEncoder
 * @see ReplicationDecoder
 */
@cpu
@system
class ReplicationBase extends Object
{
    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    protected var content : thor.unmanaged.ptr_<int64>;
    /**
     * @endcond
     */
}

/**
 * @brief Class provides actions which dump value into raw data.
 *
 * For the following types:
 * - Primitive values
 * - Objects
 *
 * This class defines how they are dumped, then provide methods (overloaded
 * with name @c put) to dump them into raw data. @n
 *
 * If encoding is failure, @c put will return @c false. For example, @c false
 * will be returned if there is no more memory to store raw data.
 *
 * @see ReplicationDecoder
 */
@cpu
@system
class ReplicationEncoder extends ReplicationBase
{
    /**
     * @brief Default constructor initializes instance to empty state.
     */
    @native public function new():void;

    /**
     * @brief Destructor releases resources.
     */
    @native public function delete():void;

    /**
     * @brief Dump boolean value into raw data.
     *
     * @arg b Boolean value to be dumped.
     *
     * @return Boolean value indicates that value is successfully dumped or not.
     * @retval true  Dumped successfully.
     * @retval false Failed to dump value of @c b.
     *
     * @see put(int8)
     * @see put(int16)
     * @see put(int32)
     * @see put(int64)
     * @see put(float32)
     * @see put(float64)
     */
    @native public function put(b: bool): bool;

    /**
     * @brief Dump 8-bit integer value into raw data.
     *
     * @arg i8 8-bit integer value to be dumped.
     *
     * @return Boolean value indicates that value is successfully dumped or not.
     * @retval true  Dumped successfully.
     * @retval false Failed to dump value of @c i8.
     *
     * @see put(bool)
     * @see put(int16)
     * @see put(int32)
     * @see put(int64)
     * @see put(float32)
     * @see put(float64)
     */
    @native public function put(i8 : int8 ): bool;

    /**
     * @brief Dump 16-bit integer value into raw data.
     *
     * @arg i16 16-bit integer value to be dumped.
     *
     * @return Boolean value indicates that value is successfully dumped or not.
     * @retval true  Dumped successfully.
     * @retval false Failed to dump value of @c i16.
     *
     * @see put(bool)
     * @see put(int8)
     * @see put(int32)
     * @see put(int64)
     * @see put(float32)
     * @see put(float64)
     */
    @native public function put(i16: int16): bool;

    /**
     * @brief Dump 32-bit integer value into raw data.
     *
     * @arg i32 32-bit integer value to be dumped.
     *
     * @return Boolean value indicates that value is successfully dumped or not.
     * @retval true  Dumped successfully.
     * @retval false Failed to dump value of @c i32.
     *
     * @see put(bool)
     * @see put(int8)
     * @see put(int16)
     * @see put(int64)
     * @see put(float32)
     * @see put(float64)
     */
    @native public function put(i32: int32): bool;

    /**
     * @brief Dump 64-bit integer value into raw data.
     *
     * @arg i64 64-bit integer value to be dumped.
     *
     * @return Boolean value indicates that value is successfully dumped or not.
     * @retval true  Dumped successfully.
     * @retval false Failed to dump value of @c i64.
     *
     * @see put(bool)
     * @see put(int8)
     * @see put(int16)
     * @see put(int32)
     * @see put(float32)
     * @see put(float64)
     */
    @native public function put(i64: int64): bool;

    /**
     * @brief Dump 32-bit floating value into raw data.
     *
     * @arg f32 32-bit floating value to be dumped.
     *
     * @return Boolean value indicates that value is successfully dumped or not.
     * @retval true  Dumped successfully.
     * @retval false Failed to dump value of @c f32.
     *
     * @see put(bool)
     * @see put(int8)
     * @see put(int16)
     * @see put(int32)
     * @see put(int64)
     * @see put(float64)
     */
    @native public function put(f32: float32): bool;

    /**
     * @brief Dump 64-bit floating value into raw data.
     *
     * @arg f64 64-bit floating value to be dumped.
     *
     * @return Boolean value indicates that value is successfully dumped or not.
     * @retval true  Dumped successfully.
     * @retval false Failed to dump value of @c f64.
     *
     * @see put(bool)
     * @see put(int8)
     * @see put(int16)
     * @see put(int32)
     * @see put(int64)
     * @see put(float32)
     */
    @native public function put(f64: float64): bool;

    /**
     * @brief Dump objects into raw data.
     *
     * @arg o Instance to be dumped. @b null is acceptable.
     *
     * @return Boolean value indicates that value is successfully dumped or not.
     * @retval true  Dumped successfully.
     * @retval false Failed to dump value of @c o.
     *
     * @see put<T>(T)
     */
    @native public function put(o: Object): bool;

    /**
     * @brief Dump objects into raw data.
     *
     * This function is provided as template, in order to dump instance through
     * interfaces which not inherits from thor.lang.Object.
     *
     * @arg t Instance to be dumped. @b null is acceptable.
     *
     * @return Boolean value indicates that value is successfully dumped or not.
     * @retval true  Dumped successfully.
     * @retval false Failed to dump value of @c t.
     *
     * @see put(thor.lang.Object)
     */
    public function put<T>(t:T) : bool
    {
        var object = cast<Object>(t);

        return put(object);
    }

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var serialization_id_of : thor.unmanaged.ptr_<int64>;
    /**
     * @endcond
     */
}

/**
 * @brief Class provides actions which restore value from raw data.
 *
 * For the following types:
 * - Primitive values
 * - Objects
 *
 * This class defines how they are restored, then provide methods (overloaded
 * with name @c get) to restore them from raw data. Such methods use multiple
 * return value to get value and with additional boolean value which indicates
 * value is successfully decoded or not. @n
 *
 * For more details, decoder will keep track of references which refer to the
 * same instance. So if there are more than one references refer to instance
 * @c obj. Decoder will return the same reference as expected.
 *
 * @remark Note @c get is overloaded by single dummy argument since overloading
 *         is not allowed on return type only.
 *
 * @see ReplicationEncoder
 */
@cpu
@system
class ReplicationDecoder extends ReplicationBase
{
    /**
     * @brief Default constructor initializes instance to empty state
     */
    @native public function new() : void;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    @native public function new(encoder:ReplicationEncoder):void;
    /**
     * @endcond
     */

    /**
     * @brief Destructor releases resources.
     */
    @native public function delete():void;

    /**
     * @brief Restore boolean value from raw data.
     *
     * @arg unused Unused value for function overloading only.
     *
     * @return Value pair where first boolean value indicates second value is
     *         successfully restored or not.
     * @retval (true, any) Restored value successfully, second value
     *         is the restored boolean value.
     * @retval (false, any) Failed to restore value, second value is
     *         meaningless.
     */
    public function get(unused:bool): (bool, bool)
    {
        if (isNextBool())
            return true, nativeGetBoolImpl();
        else
            return false, false;
    }

    /**
     * @brief Restore 8-bit integer value from raw data.
     *
     * @arg unused Unused value for function overloading only.
     *
     * @return Value pair where first boolean value indicates second value is
     *         successfully restored or not.
     * @retval (true, any) Restored value successfully, second value
     *         is the restored 8-bit integer value.
     * @retval (false, any) Failed to restore value, second value is
     *         meaningless.
     */
    public function get(unused:int8): (bool, int8)
    {
        if (isNextInt8())
            return true, nativeGetInt8Impl();
        else
            return false, 0;
    }

    /**
     * @brief Restore 16-bit integer value from raw data.
     *
     * @arg unused Unused value for function overloading only.
     *
     * @return Value pair where first boolean value indicates second value is
     *         successfully restored or not.
     * @retval (true, any) Restored value successfully, second value
     *         is the restored 16-bit integer value.
     * @retval (false, any) Failed to restore value, second value is
     *         meaningless.
     */
    public function get(unused:int16): (bool, int16)
    {
        if (isNextInt16())
            return true, nativeGetInt16Impl();
        else
            return false, 0;
    }

    /**
     * @brief Restore 32-bit integer value from raw data.
     *
     * @arg unused Unused value for function overloading only.
     *
     * @return Value pair where first boolean value indicates second value is
     *         successfully restored or not.
     * @retval (true, any) Restored value successfully, second value
     *         is the restored 32-bit integer value.
     * @retval (false, any) Failed to restore value, second value is
     *         meaningless.
     */
    public function get(unused:int32): (bool, int32)
    {
        if (isNextInt32())
            return true, nativeGetInt32Impl();
        else
            return false, 0;
    }

    /**
     * @brief Restore 64-bit integer value from raw data.
     *
     * @arg unused Unused value for function overloading only.
     *
     * @return Value pair where first boolean value indicates second value is
     *         successfully restored or not.
     * @retval (true, any) Restored value successfully, second value
     *         is the restored 64-bit integer value.
     * @retval (false, any) Failed to restore value, second value is
     *         meaningless.
     */
    public function get(unused:int64): (bool, int64)
    {
        if (isNextInt64())
            return true, nativeGetInt64Impl();
        else
            return false, 0;
    }

    /**
     * @brief Restore 32-bit floating value from raw data.
     *
     * @arg unused Unused value for function overloading only.
     *
     * @return Value pair where first boolean value indicates second value is
     *         successfully restored or not.
     * @retval (true, any) Restored value successfully, second value
     *         is the restored 32-bit floating value.
     * @retval (false, any) Failed to restore value, second value is
     *         meaningless.
     */
    public function get(unused:float32): (bool, float32)
    {
        if (isNextFloat32())
            return true, nativeGetFloat32Impl();
        else
            return false, 0.0f;
    }

    /**
     * @brief Restore 64-bit floating value from raw data.
     *
     * @arg unused Unused value for function overloading only.
     *
     * @return Value pair where first boolean value indicates second value is
     *         successfully restored or not.
     * @retval (true, any) Restored value successfully, second value
     *         is the restored 64-bit floating value.
     * @retval (false, any) Failed to restore value, second value is
     *         meaningless.
     */
    public function get(unused:float64): (bool, float64)
    {
        if (isNextFloat64())
            return true, nativeGetFloat64Impl();
        else
            return false, 0.0;
    }

    /**
     * @brief Restore object instance from raw data.
     *
     * This function is provided as template, in order to restore instance to
     * interfaces which not inherits from thor.lang.Object.
     *
     * @arg unused Unused value for function overloading only.
     *
     * @return Value pair where first boolean value indicates second value is
     *         successfully restored or not.
     * @retval (true, any) Restored value successfully, second value
     *         is the restored object instance (@c null is a valid value).
     * @retval (false, any) Failed to restore value, second value is
     *         meaningless.
     */
    public function get<T>(unused:T): (bool, T)
    {
        if (isNextNull())
        {
            eatNullMark();

            return true, null;
        }

        const object: Object = nativeGetObjectImpl();

        if (object == null)
            return false, null;

        const t: T = cast<T>(object);

        return t != null, t;
    }

    /**
     * @brief Test if there is no more data for deserialization.
     *
     * @return Boolean value indicates there is no more data or not.
     * @retval true  No more data.
     * @retval false There is more data.
     */
    @native public function isEnded(): bool;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    @native private function isNextBool   (): bool;
    @native private function isNextInt8   (): bool;
    @native private function isNextInt16  (): bool;
    @native private function isNextInt32  (): bool;
    @native private function isNextInt64  (): bool;
    @native private function isNextFloat32(): bool;
    @native private function isNextFloat64(): bool;
    @native private function isNextNull   (): bool;
    @native private function isNextObject (): bool;

    @native private function nativeGetBoolImpl   (): bool   ;
    @native private function nativeGetInt8Impl   (): int8   ;
    @native private function nativeGetInt16Impl  (): int16  ;
    @native private function nativeGetInt32Impl  (): int32  ;
    @native private function nativeGetInt64Impl  (): int64  ;
    @native private function nativeGetFloat32Impl(): float32;
    @native private function nativeGetFloat64Impl(): float64;
    @native private function nativeGetObjectImpl (): Object ;

    @native private function eatNullMark         (): void   ;

    private var deserialized_objects : thor.unmanaged.ptr_<int64>;
    /**
     * @endcond
     */
}
