/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "thor/lang/Domain.cuh"
#include "thor/lang/Process.cuh"

namespace thor { namespace lang {

__device__ Domain::Domain() : session_id(-1)
{
}

__device__ Domain::~Domain()
{
}

__device__ Domain* Domain::local()
{
//	return (Domain*)gRuntimeServiceBuffer->getLocalDomainObject();
	return NULL;
}

__device__ Domain* Domain::caller()
{
//	int64 session_id = __getCurrentSessionId(__getCurrentInvocationId());
//	if(session_id == -1)
//		return (Domain*)gRuntimeServiceBuffer->getLocalDomainObject();
//	else
//		return (Domain*)gRuntimeServiceBuffer->findDomainObject(session_id);
	return NULL;
}

__device__ void Domain::setSessionId(int64 id)
{
    session_id = id;
}

//void Domain::watch(DomainEvent e, DomainEventCallback callback)
//{
//	std::cerr << "Domain::watch (static function callback)" << std::endl;
//}

//__device__ void Domain::watch(int32 e, Lambda1<void, Domain>* callback)
//{
//	__addToRootSet(callback);
////	std::cerr << "Domain::watch (lambda function callback)" << std::endl;
//	gRuntimeServiceBuffer->addDomainEventCallback((int32)e, callback);
//}

} }
