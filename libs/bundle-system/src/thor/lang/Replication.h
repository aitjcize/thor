/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_LANG_REPLICATION_H_
#define THOR_LANG_REPLICATION_H_

#include <cstddef>
#include <cstdint>

#include <utility>
#include <map>

#include "thor/lang/Language.h"

// forward declaration
namespace zillians { namespace framework {

namespace service {
    class ReplicationData;
}

} }

namespace thor { namespace lang {

class ReplicationBase : public Object
{
public:

    using Buffer = zillians::framework::service::ReplicationData;
    using TypeIdentifier = int64;
    using SerializationIdentifier = std::uint32_t;

    // helper types for primitive type headers
    enum class PrimitiveTypeHeader : byte
    {
        Bool, Int8, Int16, Int32, Int64,
        Float32, Float64, Raw,
        PrimitiveTypeHeaderEnd,
    };

    // helper types for object related markers
    enum class ObjectMarker : byte
    {
        Null = static_cast<byte>(PrimitiveTypeHeader::PrimitiveTypeHeaderEnd),
        Reference, Begin, End,
        ObjectMarkerEnd,
    };

    ReplicationBase();
    explicit ReplicationBase(Buffer* buffer);
    ~ReplicationBase();

protected:
    Buffer * content;
};

class ReplicationEncoder : public ReplicationBase
{
    using ObjectToSerializationIdentifier = std::map< Object*, SerializationIdentifier >;
    ObjectToSerializationIdentifier * serialization_identifiers;

    std::pair< bool, SerializationIdentifier > get_serialization_id(Object* object);
    bool putReference(SerializationIdentifier id);
    bool putNull();

public:
    ReplicationEncoder();
    ~ReplicationEncoder() override;

    bool put(bool b);

    bool put(int8  i8 );
    bool put(int16 i16);
    bool put(int32 i32);
    bool put(int64 i64);

    bool put(float32 f32);
    bool put(float64 f64);

    bool put(Object* o);

    // C++ only interface to put raw data
    bool put(std::size_t count, const void* buffer);

    // C++ only interface to exchange internal buffer
    Buffer* stolenContent() noexcept;
};

class ReplicationDecoder : public ReplicationBase
{
    using SerializationIdentifierToObject = std::map< SerializationIdentifier, Object* >;
    SerializationIdentifierToObject * deserialized_objects;

public:
    ReplicationDecoder();
    ReplicationDecoder(ReplicationEncoder* encoder);
    ~ReplicationDecoder() override;

    static ReplicationDecoder* __create();

    bool isEnded();

    void setContent(Buffer& new_content);

    template<typename T> bool               isNext(T unused);
    template<typename T> std::pair<bool, T> nativeGet(T);

#define DECALRE_DECODER_IS_AND_GET(type, name)                     \
public:                                                            \
    bool                             isNext ## name();             \
    std::pair<bool, type>            nativeGet ## name();          \
private:                                                           \
    type                             nativeGet ## name ##Impl()

    DECALRE_DECODER_IS_AND_GET(bool   , Bool   );
    DECALRE_DECODER_IS_AND_GET(int8   , Int8   );
    DECALRE_DECODER_IS_AND_GET(int16  , Int16  );
    DECALRE_DECODER_IS_AND_GET(int32  , Int32  );
    DECALRE_DECODER_IS_AND_GET(int64  , Int64  );
    DECALRE_DECODER_IS_AND_GET(float32, Float32);
    DECALRE_DECODER_IS_AND_GET(float64, Float64);

#undef DECALRE_DECODER_IS_AND_GET

public:
    bool                     isNextNull();
    bool                     isNextObject();
    std::pair<bool, Object*> nativeGetObject();
private:
    Object*                  nativeGetObjectImpl();
    void                     eatNullMark();

public:
    // C++ only interface to get raw data
    std::pair<std::size_t, bool> eatRawBegin();
    std::size_t                  getRawData(std::size_t count, void* buffer);

private:
    void registerDeserialized(Object* object);
};


} }

#endif /* THOR_LANG_REPLICATION_H_ */
