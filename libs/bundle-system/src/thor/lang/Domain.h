/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_LANG_DOMAIN_H_
#define THOR_LANG_DOMAIN_H_

#include "thor/Thor.h"
#include "thor/lang/Language.h"
#include "thor/lang/Lambda.h"
#include "thor/container/Vector.h"
#include "thor/util/UUID.h"

namespace thor { namespace lang {

enum class DomainError : thor::int32 {
    OK                     ,
    OPERATION_CANCELED     ,
    INVALID_ENDPOINT_SPEC  ,
    UNSUPPORTED_TRANSPORT  ,
    EXCEED_CONCURRENT_LIMIT,
    NO_SUCH_REQUEST        ,
    UNKNOWN_ERROR          ,
};

class Domain : public Object
{
public:
    using ConnCallback = Lambda2<void, ::thor::util::UUID*, Domain*    >;
    using ErrCallback  = Lambda2<void, ::thor::util::UUID*, DomainError>;

private:
	Domain();

public:
    virtual ~Domain();

public:
    static Domain* create();
    static Domain* local();
    static Domain* caller();

public:
	int64 getSessionId();
    void setSessionId(int64 id);

    static ::thor::util::UUID* listen (String* endpoint, ConnCallback* conn_cb, ConnCallback* disconn_cb, ErrCallback* err_cb);
    static ::thor::util::UUID* connect(String* endpoint, ConnCallback* conn_cb, ConnCallback* disconn_cb, ErrCallback* err_cb);
    static bool                cancel (::thor::util::UUID* id);

private:
    static void __onConn() noexcept;
    static void __onErr() noexcept;

public:
	int64 session_id;
};

} }

#endif /* THOR_LANG_DOMAIN_H_ */
