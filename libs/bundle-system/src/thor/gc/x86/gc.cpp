/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "thor/gc/gc.h"
#include "framework/service/object/ObjectServiceBuffer.h"

namespace thor { namespace gc {

namespace mt_proxies = zillians::framework::service::mt::proxies;

using ::thor::container::Vector;
using ::thor::lang::Object;

Vector<Object*>* __getActiveObjects()
{
    auto*const objects = Vector<Object*>::create();

    mt_proxies::object::getActiveObjectsInternal(objects);

    return objects;
}

int64 __getGenerationId()
{
    return mt_proxies::object::getGCGenerationId();
}

} }
