/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_UTIL_MACRO_H_
#define THOR_UTIL_MACRO_H_

#define PP_CAT(x, y) PP_CAT_(x, y)
#define PP_CAT_(x, y) x##y

#define PP_APPLY(macro, ...) PP_APPLY_(macro, __VA_ARGS__)
#define PP_APPLY_(macro, ...) macro(__VA_ARGS__)

#define PP_ARG_COUNT(...) \
    PP_ARG_COUNT_(__VA_ARGS__, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
#define PP_ARG_COUNT_(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, count, ...) count 

#define PP_LIST_SIZE(list) PP_ARG_COUNT list

#define PP_ARG_GET(    n, ...) PP_ARG_GET_(n, __VA_ARGS__)
#define PP_ARG_GET_(   n, ...) PP_CAT(PP_ARG_GET_, n)(__VA_ARGS__)
#define PP_ARG_GET_1( _1, ...) _1
#define PP_ARG_GET_2( _1, ...) PP_CAT(PP_ARG_GET_, 1)(__VA_ARGS__)
#define PP_ARG_GET_3( _1, ...) PP_CAT(PP_ARG_GET_, 2)(__VA_ARGS__)
#define PP_ARG_GET_4( _1, ...) PP_CAT(PP_ARG_GET_, 3)(__VA_ARGS__)
#define PP_ARG_GET_5( _1, ...) PP_CAT(PP_ARG_GET_, 4)(__VA_ARGS__)
#define PP_ARG_GET_6( _1, ...) PP_CAT(PP_ARG_GET_, 5)(__VA_ARGS__)
#define PP_ARG_GET_7( _1, ...) PP_CAT(PP_ARG_GET_, 6)(__VA_ARGS__)
#define PP_ARG_GET_8( _1, ...) PP_CAT(PP_ARG_GET_, 7)(__VA_ARGS__)
#define PP_ARG_GET_9( _1, ...) PP_CAT(PP_ARG_GET_, 8)(__VA_ARGS__)
#define PP_ARG_GET_10(_1, ...) PP_CAT(PP_ARG_GET_, 9)(__VA_ARGS__)

#define PP_LIST_GET(n, list) PP_CAT(PP_ARG_GET_, n) list

#define PP_MAKE_LIST(n, text, macro) PP_CAT(PP_MAKE_LIST_, n)(text, macro)
#define PP_MAKE_LIST_1( text, macro)                          PP_APPLY(macro, text, 0)
#define PP_MAKE_LIST_2( text, macro) PP_CAT(PP_MAKE_LIST_, 1) ( text, macro), PP_APPLY(macro, text,  1)
#define PP_MAKE_LIST_3( text, macro) PP_CAT(PP_MAKE_LIST_, 2) ( text, macro), PP_APPLY(macro, text,  2)
#define PP_MAKE_LIST_4( text, macro) PP_CAT(PP_MAKE_LIST_, 3) ( text, macro), PP_APPLY(macro, text,  3)
#define PP_MAKE_LIST_5( text, macro) PP_CAT(PP_MAKE_LIST_, 4) ( text, macro), PP_APPLY(macro, text,  4)
#define PP_MAKE_LIST_6( text, macro) PP_CAT(PP_MAKE_LIST_, 5) ( text, macro), PP_APPLY(macro, text,  5)
#define PP_MAKE_LIST_7( text, macro) PP_CAT(PP_MAKE_LIST_, 6) ( text, macro), PP_APPLY(macro, text,  6)
#define PP_MAKE_LIST_8( text, macro) PP_CAT(PP_MAKE_LIST_, 7) ( text, macro), PP_APPLY(macro, text,  7)
#define PP_MAKE_LIST_9( text, macro) PP_CAT(PP_MAKE_LIST_, 8) ( text, macro), PP_APPLY(macro, text,  8)
#define PP_MAKE_LIST_10(text, macro) PP_CAT(PP_MAKE_LIST_, 9) ( text, macro), PP_APPLY(macro, text,  9)
#define PP_MAKE_LIST_11(text, macro) PP_CAT(PP_MAKE_LIST_, 10)( text, macro), PP_APPLY(macro, text, 10)

#define PP_TRANSFORM(   list1, list2, macro) PP_CAT(PP_TRANSFORM_, PP_LIST_SIZE(list1))(list1, list2, macro)
#define PP_TRANSFORM_1( list1, list2, macro)                                                PP_APPLY(macro, PP_LIST_GET( 1, list1), PP_LIST_GET( 1, list2))
#define PP_TRANSFORM_2( list1, list2, macro) PP_CAT(PP_TRANSFORM_, 1)(list1, list2, macro), PP_APPLY(macro, PP_LIST_GET( 2, list1), PP_LIST_GET( 2, list2))
#define PP_TRANSFORM_3( list1, list2, macro) PP_CAT(PP_TRANSFORM_, 2)(list1, list2, macro), PP_APPLY(macro, PP_LIST_GET( 3, list1), PP_LIST_GET( 3, list2))
#define PP_TRANSFORM_4( list1, list2, macro) PP_CAT(PP_TRANSFORM_, 3)(list1, list2, macro), PP_APPLY(macro, PP_LIST_GET( 4, list1), PP_LIST_GET( 4, list2))
#define PP_TRANSFORM_5( list1, list2, macro) PP_CAT(PP_TRANSFORM_, 4)(list1, list2, macro), PP_APPLY(macro, PP_LIST_GET( 5, list1), PP_LIST_GET( 5, list2))
#define PP_TRANSFORM_6( list1, list2, macro) PP_CAT(PP_TRANSFORM_, 5)(list1, list2, macro), PP_APPLY(macro, PP_LIST_GET( 6, list1), PP_LIST_GET( 6, list2))
#define PP_TRANSFORM_7( list1, list2, macro) PP_CAT(PP_TRANSFORM_, 6)(list1, list2, macro), PP_APPLY(macro, PP_LIST_GET( 7, list1), PP_LIST_GET( 7, list2))
#define PP_TRANSFORM_8( list1, list2, macro) PP_CAT(PP_TRANSFORM_, 7)(list1, list2, macro), PP_APPLY(macro, PP_LIST_GET( 8, list1), PP_LIST_GET( 8, list2))
#define PP_TRANSFORM_9( list1, list2, macro) PP_CAT(PP_TRANSFORM_, 8)(list1, list2, macro), PP_APPLY(macro, PP_LIST_GET( 9, list1), PP_LIST_GET( 9, list2))
#define PP_TRANSFORM_10(list1, list2, macro) PP_CAT(PP_TRANSFORM_, 9)(list1, list2, macro), PP_APPLY(macro, PP_LIST_GET(10, list1), PP_LIST_GET(10, list2))

#define PP_FOREACH_APPLY(   macro, ...) PP_CAT(PP_FOREACH_APPLY_, PP_ARG_COUNT(__VA_ARGS__))(macro, __VA_ARGS__)
#define PP_FOREACH_APPLY_1( macro, ...) PP_APPLY(macro, PP_ARG_GET(1, __VA_ARGS__))
#define PP_FOREACH_APPLY_2( macro, ...) PP_CAT(PP_FOREACH_APPLY_, 1)(macro, __VA_ARGS__) PP_APPLY(macro, PP_ARG_GET( 2, __VA_ARGS__))
#define PP_FOREACH_APPLY_3( macro, ...) PP_CAT(PP_FOREACH_APPLY_, 2)(macro, __VA_ARGS__) PP_APPLY(macro, PP_ARG_GET( 3, __VA_ARGS__))
#define PP_FOREACH_APPLY_4( macro, ...) PP_CAT(PP_FOREACH_APPLY_, 3)(macro, __VA_ARGS__) PP_APPLY(macro, PP_ARG_GET( 4, __VA_ARGS__))
#define PP_FOREACH_APPLY_5( macro, ...) PP_CAT(PP_FOREACH_APPLY_, 4)(macro, __VA_ARGS__) PP_APPLY(macro, PP_ARG_GET( 5, __VA_ARGS__))
#define PP_FOREACH_APPLY_6( macro, ...) PP_CAT(PP_FOREACH_APPLY_, 5)(macro, __VA_ARGS__) PP_APPLY(macro, PP_ARG_GET( 6, __VA_ARGS__))
#define PP_FOREACH_APPLY_7( macro, ...) PP_CAT(PP_FOREACH_APPLY_, 6)(macro, __VA_ARGS__) PP_APPLY(macro, PP_ARG_GET( 7, __VA_ARGS__))
#define PP_FOREACH_APPLY_8( macro, ...) PP_CAT(PP_FOREACH_APPLY_, 7)(macro, __VA_ARGS__) PP_APPLY(macro, PP_ARG_GET( 8, __VA_ARGS__))
#define PP_FOREACH_APPLY_9( macro, ...) PP_CAT(PP_FOREACH_APPLY_, 8)(macro, __VA_ARGS__) PP_APPLY(macro, PP_ARG_GET( 9, __VA_ARGS__))
#define PP_FOREACH_APPLY_10(macro, ...) PP_CAT(PP_FOREACH_APPLY_, 9)(macro, __VA_ARGS__) PP_APPLY(macro, PP_ARG_GET(10, __VA_ARGS__))

#endif
