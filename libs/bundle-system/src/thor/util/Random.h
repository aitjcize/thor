/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_UTIL_RANDOM_H_
#define THOR_UTIL_RANDOM_H_

#include <cmath>

#include <random>
#include <limits>
#include <type_traits>

#include "thor/Thor.h"
#include "thor/lang/Language.h"

namespace thor { namespace util { 

struct Uniform : public thor::lang::Object
{
    Uniform();
    virtual ~Uniform();
};

struct Normal : public thor::lang::Object
{
    Normal();
    virtual ~Normal();
};

template < typename T, typename D >
class Random
{
    static_assert(
        std::is_same<D,Uniform*>::value || std::is_same<D,Normal*>::value
        , "pass types other than \'Uniform\' nor \'Normal\' as distribution are not allowed"
    );
};

template < typename T >
class Random< T, Uniform* > : public thor::lang::Object
{
    static_assert(thor::lang::is_builtin<T>::value && !std::is_same<T,bool>::value, "we only support primitive types(except bool)");

    // member types
    using engine_t       = std::mt19937;
    using distribution_t = typename std::conditional<
        std::is_integral< T >::value
        , std::uniform_int_distribution< T >
        , std::uniform_real_distribution< T >
    >::type;
    // date members
    engine_t * engine;
    distribution_t * distribution;
public:

    Random()
    : Random(std::random_device()())
    { }

    Random(int64 seed)
    : engine(new engine_t(seed)),
      distribution(new distribution_t)
    { }

    Random(T min, T max)
    : Random(std::random_device()(), min, max)
    { }

    Random(int64 seed, T min, T max)
    : engine(new engine_t(seed)),
      distribution(new distribution_t(min, max))
    { }

    virtual ~Random() {
        delete engine, delete distribution;
    }

    void seed(int64 seed) {
        engine->seed( seed );
    }

    T next() {
        return (*distribution)(*engine);
    }
};

template < typename T >
class Random< T, Normal* > : public thor::lang::Object
{
    static_assert(thor::lang::is_builtin<T>::value && !std::is_same<T,bool>::value, "we only support primitive types(except bool)");

    // member types
    using engine_t       = std::mt19937;
    using distribution_t = typename std::conditional<
        std::is_integral< T >::value
        , std::normal_distribution< float64 > // always use floating point internally
        , std::normal_distribution< T >
    >::type;
    // tags for dispatching
    struct need_round { };
    struct return_directly { };

    using case_tag = typename std::conditional<
        std::is_integral< T >::value
        , need_round
        , return_directly
    >::type;
    // date members
    engine_t * engine;
    distribution_t * distribution;
    // helper functions, dispatch on different types
    T next_impl(need_round) {
        auto result = (*distribution)(*engine);
        return static_cast<T>(std::round(result));
    }

    T next_impl(return_directly) {
        return (*distribution)(*engine);
    }
public:

    Random()
    : Random(std::random_device()())
    { }

    Random(int64 seed)
    : engine(new engine_t(seed)),
      distribution(new distribution_t)
    { }

    Random(T mean, T stddev)
    : Random(std::random_device()(), mean, stddev)
    { }

    Random(int64 seed, T mean, T stddev)
    : engine(new engine_t(seed)),
      distribution(new distribution_t(mean, stddev))
    { }

    virtual ~Random() {
        delete engine, delete distribution;
    }

    void seed(int64 seed) {
        engine->seed( seed );
    }

    T next() {
        return next_impl( case_tag() );
    }
};

} }

#endif
