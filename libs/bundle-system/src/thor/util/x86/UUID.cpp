/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <type_traits>
#include <utility>

#include <boost/lexical_cast.hpp>
#include <boost/uuid/nil_generator.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "thor/lang/String.h"
#include "thor/util/UUID.h"

namespace thor { namespace util {

namespace {

template<typename DataType>
using asUUIDType = typename std::conditional<
    std::is_const<DataType>::value,
    std::add_const<boost::uuids::uuid>::type,
                   boost::uuids::uuid
>::type;

template<typename DataType>
typename std::add_lvalue_reference<asUUIDType<DataType>>::type asUUIDUnsafe(DataType& data) noexcept
{
    static_assert(
        sizeof(data) == sizeof(asUUIDType<DataType>),
        "size of underlying type is not same"
    );

    return reinterpret_cast<typename std::add_lvalue_reference<asUUIDType<DataType>>::type>(data);
}

template<typename GeneratorType, typename... ArgTypes>
boost::uuids::uuid generate(GeneratorType generator, ArgTypes&&... args) noexcept(noexcept(generator(std::forward<ArgTypes>(args)...)))
{
    return generator(std::forward<ArgTypes>(args)...);
}

}

UUID::UUID() noexcept
    : data()
{
    setNil();
}

UUID::~UUID() noexcept = default;

UUID* UUID::clone() noexcept
{
    UUID*const cloned_uuid = __create();

    static_assert(noexcept(cloned_uuid->data = data), "break exception specification");
                           cloned_uuid->data = data                                   ;

    return cloned_uuid;
}

int64 UUID::hash() noexcept
{
    const auto& boost_uuid = getId();
    const auto  value      = hash_value(boost_uuid);

    static_assert(std::is_integral<decltype(value)>::value, "unexpected hashed value");

    return static_cast<int64>(value);
}

bool UUID::isNil() noexcept
{
    const auto& boost_uuid = getId();

    return boost_uuid.is_nil();
}

void UUID::setNil() noexcept
{
    auto& boost_uuid = getId();

    boost_uuid = boost::uuids::nil_uuid();
}

bool UUID::isEqual(UUID* rhs) noexcept
{
    const auto& boost_uuid_lhs =      getId();
    const auto& boost_uuid_rhs = rhs->getId();

    return boost_uuid_lhs == boost_uuid_rhs;
}

thor::lang::String* UUID::toString() noexcept
{
    try
    {
        std::wstring uuid_string      = to_wstring(getId());
        auto*const   thor_lang_string = thor::lang::String::__create();

        *thor_lang_string->data = std::move(uuid_string);

        return thor_lang_string;
    }
    catch (...)
    {
    }

    return nullptr;
}

UUID* UUID::nil() noexcept
{
    return __create();
}

UUID* UUID::random() noexcept
{
    try
    {
        boost::uuids::uuid randomed_uuid = generate(boost::uuids::random_generator());

        return fromId(std::move(randomed_uuid));
    }
    catch (...)
    {
    }

    return nullptr;
}

UUID* UUID::parse(thor::lang::String* text) noexcept
{
    try
    {
        auto parsed_uuid = boost::lexical_cast<boost::uuids::uuid>(*text->data);

        return fromId(std::move(parsed_uuid));
    }
    catch (...)
    {
    }

    return nullptr;
}

UUID* UUID::fromId(const boost::uuids::uuid& id) noexcept
{
    UUID*const instance = __create();

    static_assert(noexcept(instance->setId(id)), "break exception specification");
                           instance->setId(id);                                  ;

    return instance;
}

UUID* UUID::fromId(boost::uuids::uuid&& id) noexcept
{
    UUID*const instance = __create();

    static_assert(noexcept(instance->setId(std::move(id))), "break exception specification");
                           instance->setId(std::move(id));                                  ;

    return instance;
}

const boost::uuids::uuid& UUID::getId() const noexcept
{
    return asUUIDUnsafe(data);
}

boost::uuids::uuid& UUID::getId() noexcept
{
    return asUUIDUnsafe(data);
}

void UUID::setId(const boost::uuids::uuid& id) noexcept
{
    auto& boost_uuid = getId();

    boost_uuid = id;
}

void UUID::setId(boost::uuids::uuid&& id) noexcept
{
    auto& boost_uuid = getId();

    boost_uuid = std::move(id);
}

} }
