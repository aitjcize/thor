/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "thor/lang/String.h"
#include "thor/util/Macro.h"
#include "thor/util/StringBuilder.h"

#include <cstdint>
#include <cassert>

#include <stack>
#include <queue>
#include <iostream>
#include <sstream>

#define PP_MAKE_STRINGBUILDER_APPEND(type)                  \
    void StringBuilder::append(type value) {                \
        stream->rdbuf()->pubseekoff(0, ios_base::end);      \
        *stream << thor::lang::lexical_cast(value);         \
    }

#define PP_MAKE_STRINGBUILDER_APPENDASCHAR(type)            \
    void StringBuilder::appendAsCharacter(type value) {     \
        stream->rdbuf()->pubseekoff(0, ios_base::end);      \
        stream->rdbuf()->sputc(                             \
            traits_type::to_char_type(value)                \
        );                                                  \
    }

#define PP_MAKE_STRINGBUILDER_INSERT(type)                  \
    void StringBuilder::insert(int32 offset, type value) {  \
        if (offset < 0)                                     \
            return;                                         \
                                                            \
        if (stream->tellp() < offset)                       \
            return;                                         \
                                                            \
        buffer_type* buffer = stream->rdbuf();              \
        buffer->pubseekoff(offset, ios_base::beg);          \
                                                            \
        std::queue<traits_type::int_type> chars;            \
        pullChars(chars);                                   \
        *stream << thor::lang::lexical_cast(value);         \
                                                            \
        while(!chars.empty()) {                             \
            buffer->sputc(chars.front());                   \
            chars.pop();                                    \
        }                                                   \
    }

using namespace thor::lang;
using namespace std;

namespace thor { namespace util {

StringBuilder::StringBuilder()
: stream( new stream_type )
{ }

StringBuilder::StringBuilder( String * s )
: stream( new stream_type( *(s->data) ) )
{ }

StringBuilder::~StringBuilder()
{
    delete stream;
}

int32 StringBuilder::length()
{
    stream->rdbuf()->pubseekoff( 0, ios_base::end );
    return stream->tellp();
}

void StringBuilder::reverse()
{
    buffer_type * old_buffer = stream->rdbuf();

    stack<traits_type::int_type> chars;
    old_buffer->pubseekoff( 0, ios_base::beg );
    pullChars( chars ); 

    stream_type * new_stream = new stream_type;
    buffer_type * new_buffer = new_stream->rdbuf();
    while( !chars.empty() )
    {
        new_buffer->sputc( chars.top() );
        chars.pop();
    }

    delete stream;
    stream = new_stream;
}

String *
StringBuilder::toString()
{
    String * result = String::__create();
    *(result->data) = stream->str();
    return result;
}

String *
StringBuilder::toString( int32 start, int32 end )
{
    if (start < 0)
        return nullptr;

    if (end < start)
        return nullptr;

    int32 length = end - start;

    buffer_type * buffer = stream->rdbuf();
    thor::lang::String * result = thor::lang::String::__create();

    stream->rdbuf()->pubseekoff( start, ios_base::beg );
    for( ; 0 < length; --length )
        result->data->append( 1, traits_type::to_char_type(buffer->sbumpc()) );

    return result;
}

void
StringBuilder::append(thor::lang::String* v)
{
    *stream << *(v->data);
}

void
StringBuilder::insert(int32 offset, String* s)
{
    if (offset < 0)
        return;

    if (stream->tellp() < offset)
        return;

    buffer_type * buffer = stream->rdbuf();

    buffer->pubseekoff(offset, ios_base::beg);
    
    queue<traits_type::int_type> chars;
    pullChars(chars);

    *stream << *(s->data);
  
    while(!chars.empty())
    {
        buffer->sputc(chars.front());
        chars.pop();
    }
}

PP_FOREACH_APPLY(
    PP_MAKE_STRINGBUILDER_INSERT,
    bool, int8, int16, int32, int64, float32, float64
)

PP_FOREACH_APPLY(
    PP_MAKE_STRINGBUILDER_APPEND,
    bool, int8, int16, int32, int64, float32, float64
)

PP_FOREACH_APPLY(
    PP_MAKE_STRINGBUILDER_APPENDASCHAR,
    int8, int16, int32
)

} } // namespace thor::util

