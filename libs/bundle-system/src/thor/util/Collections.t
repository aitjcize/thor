/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import . = thor.unmanaged;
import . = thor.lang;

/**
 * @brief Sort a sequential container with @b less comparator.
 *
 * Example:
 * @code
 *
 * import .= thor.container;
 * import .= thor.util;
 *
 * @entry
 * task test_sort()
 * {
 *    var vec = new Vector<int32>;
 *    vec.pushBack(4);
 *    vec.pushBack(2);
 *    vec.pushBack(1);
 *    vec.pushBack(3);
 *
 *    sort(vec);
 *
 *    for(var v in vec)
 *    {
 *        print("\{v} ");
 *    }
 *    print("\n");
 *
 *    var arr = [2,3,1,4];
 *    sort(arr);
 *
 *    for(var v in arr)
 *    {
 *        print("\{v} ");
 *    }
 *    print("\n");
 *
 *    exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * 1 2 3 4
 * 1 2 3 4
 * @endverbatim
 *
 * @param collection The sequential container which is going to be sorted.
 *
 * @note It's an unstable sort.
 * @see stableSort<CollectionType>
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function sort<CollectionType>( collection: CollectionType ) : void;

/**
 * @brief Sort a sequential container with @b user-defined comparator.
 *
 * A user-defined comparator is a class with a method @c compare().
 * It should be a function accept two arguments as consistent type with your
 * target sequential container. It should return @b negative value if the
 * first argument is less than the second, a @b positive value if the first
 * argument is @b greater then the second, and @b zero if they are equal.
 *
 * Example:
 * @code
 *
 * import .= thor.container;
 * import .= thor.util;
 *
 * class Greater
 * {
 *     public function compare(a : int32, b : int32) : int32
 *     {
 *         return b - a;
 *     }
 * }
 *
 * task test_sort()
 * {
 *    var vec = new Vector<int32>;
 *    vec.pushBack(4);
 *    vec.pushBack(2);
 *    vec.pushBack(1);
 *    vec.pushBack(3);
 *
 *    sort(vec, new Greater());
 *
 *    for(var v in vec)
 *    {
 *        print("\{v} ");
 *    }
 *    print("\n");
 *
 *    exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * 4 3 2 1
 * @endverbatim
 *
 * @param collection The sequential container which is going to be sorted.
 *
 * @note It's an unstable sort.
 * @see stableSort<CollectionType, Compare>
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function sort<CollectionType, Compare>( collection: CollectionType, compare: Compare ) : void;

/**
 * @brief Stably sort a sequential container with less comparator.
 *
 * Sort a sequential container in a stable way. The usage is the same as sort().
 *
 * @see sort<CollectionType>
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function stableSort<CollectionType>( collection: CollectionType ) : void;

/**
 * @brief Stably sort a sequential container with a user-defined comparator.
 *
 * Sort a sequential container in a stable way with a user-defined comparator.
 * The usage is the same as sort() with a user-defined comparator.
 *
 * @see sort<CollectionType, Compare>
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function stableSort<CollectionType, Compare>( collection: CollectionType, compare: Compare ) : void;

/**
 * @brief Rearrange a sequential container such that elements in the given range is identical to the sorted one.
 *
 * Given a mid-point, m, partialSort() will rearrange elements of the sequential container such that those in [begin, m)
 * is identical to the fully sorted one.
 *
 * Example:
 * @code
 *
 * import .= thor.util;
 *
 * task test_partial()
 * {
 *    var arr = [10,9,8,7,6,5,4,3,2,1];
 *
 *    partialSort(arr, 5);
 *    for(var v in arr)
 *    {
 *        print("\{v} ");
 *    }
 *    print("\n");
 *
 *    exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * 1 2 3 4 5 10 9 8 7 6
 * @endverbatim
 *
 *
 * @param collection The sequential container which is going to be partially sorted.
 * @param middle The mid-point which separtes the sorted range and the un-sorted range.
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function partialSort<CollectionType,Size>( collection: CollectionType, middle: Size ) : void;

/**
 * @brief Rearrange a sequential container such that the element in the given range is identical to the sorted one using user-defined comparator.
 *
 * Despite the user-defined comparator, the logic is the same as @c partialSort<CollectionType,Size>.
 *
 * A user-defined comparator is a class with a method @c compare().
 * It should be a function accept two arguments as consistent type with your
 * target sequential container. It should return @b negative value if the
 * first argument is less than the second, a @b positive value if the first
 * argument is @b greater then the second, and @b zero if they are equal.
 *
 * Example:
 * @code
 *
 * class Greater
 * {
 *     public function compare(a : int32, b : int32) : int32
 *     {
 *         return b - a;
 *     }
 * }
 *
 * task test_partial()
 * {
 *    var arr = [1,2,3,4,5,6,7,8,9,10];
 *
 *    partialSort(arr, 5, new Greater());
 *    for(var v in arr)
 *    {
 *        print("\{v} ");
 *    }
 *    print("\n");
 *
 *    exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * 10 9 8 6 5 1 2 3 4 5
 * @endverbatim
 *
 * @param collection The sequential container which is going to be partially sorted.
 * @param middle The mid-point which separtes the sorted range and the un-sorted range.
 * @param compare The user-defined comparator.
 *
 * @see sort<CollectionType>
 * @see partialSort<CollectionType,Size>
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function partialSort<CollectionType,Size, Compare>( collection: CollectionType, middle: Size, compare: Compare ) : void;

/**
 * @brief Check if elements of the given sequential container is in ascending order.
 *
 * Example:
 * @code
 *
 * import .= thor.util;
 *
 * task test_ascend()
 * {
 *    var arr  = [1,2,3,4,5];
 *    var arr2 = [5,4,3,2,1];
 *
 *    var r  = isAscending(arr);
 *    var r2 = isAscending(arr2);
 *    println("\{r} : \{r2}");
 *
 *    exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * true : false
 * @endverbatim
 *
 * @param collection The input sequential container.
 *
 * @return If @c collection is in ascending order.
 * @retval true @c collection is in ascending order.
 * @retval false @c collection is not in ascending order.
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function isAscending<CollectionType>( collection: CollectionType ) : bool;

/**
 * @brief Use user-defined comparator to check if elements of the given sequential container is in ascending order.
 *
 * Example:
 * @code
 *
 * class Greater
 * {
 *     public function compare(a : int32, b : int32) : int32
 *     {
 *         return b - a;
 *     }
 * }
 *
 * import .= thor.util;
 *
 * task test_ascend()
 * {
 *    var arr  = [1,2,3,4,5];
 *    var arr2 = [5,4,3,2,1];
 *
 *    var r  = isAscending(arr , new Greater());
 *    var r2 = isAscending(arr2, new Greater());
 *    println("\{r} : \{r2}");
 *
 *    exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * false : true
 * @endverbatim
 *
 * @param collection The input sequential container.
 * @param compare A user-defined comparator.
 *
 * @return If @c collection is in ascending order in respect of @c compare.
 * @retval true @c collection is in ascending order in respect of @c compare.
 * @retval false @c collection is not in ascending order in respect of @c compare.
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function isAscending<CollectionType, Compare>( collection: CollectionType, compare: Compare ) : bool;

/**
 * @brief Reverse the order of the input sequential container.
 *
 * Example:
 * @code
 *
 * import .= thor.util;
 *
 * task test_reverse()
 * {
 *     var arr = [5,4,3,2,1];
 *     reverse(arr);
 *
 *     for(var v in arr)
 *     {
 *         print("\{v} ");
 *     }
 *     print("\n");
 *
 *     exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * 1 2 3 4 5
 * @endverbatim
 *
 * @param collection The input sequential container.
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function reverse<CollectionType>( collection: CollectionType ) : void;

/**
 * @brief Randomly reorder the input sequential container.
 *
 * Example:
 * @code
 *
 * import .= thor.util;
 *
 * task test_shuffle()
 * {
 *     var arr = [1,2,3,4,5];
 *     shuffle(arr);
 *
 *     for(var v in arr)
 *     {
 *         print("\{v} ");
 *     }
 *     print("\n");
 *
 *     exit(0);
 * }
 *
 * Output: (the result varies)
 * @verbatim
 * 2 1 5 3 4
 * @endverbatim
 *
 * @param collection The input sequential container.
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function shuffle<CollectionType>( collection: CollectionType ) : void;

/**
 * @brief Copy content from one sequential container to the other.
 *
 * Copy all elements of a source sequential container to a destination sequential container.
 * Example:
 * @code
 *
 * import .= thor.util;
 * import .= thor.container;
 *
 * task test_copy()
 * {
 *     var arr = [1,2,3,4,5];
 *
 *     var dest = new Vector<int32>(5);
 *     copy(dest,arr);
 *
 *     for(var v in dest)
 *     {
 *         print("\{v} ");
 *     }
 *     print("\n");
 *
 *     exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * 1 2 3 4 5
 * @endverbatim
 *
 * @param destination The destination sequential container.
 * @param source The source squential container.
 *
 * @note The size of @c destination must be at least equal to @c source.
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function copy<CollectionType1, CollectionType2>( destination: CollectionType1, source: CollectionType2 ) : void;

/**
 * @brief Fill in a container with a given value.
 *
 * Example:
 * @code
 *
 * import .= thor.util;
 *
 * task test_fill()
 * {
 *     var arr = [1,2,3,4,5];
 *
 *     fill(arr,0);
 *     for(var v in arr)
 *     {
 *         print("\{v} ");
 *     }
 *     print("\n");
 *
 *     exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * 0 0 0 0 0
 * @endverbatim
 *
 * @param collection The input container.
 * @param element The value that is going to fill @c collection.
 *
 */
@cpu
function fill<CollectionType,Element>( collection: CollectionType, element: Element ) : void
{
    var idx : int64;
    for( idx = 0; idx < collection.size(); ++idx )
        collection.set( idx, element );
}

/**
 * @brief Return a sequential container with n-copies of a given element.
 *
 * Example:
 * @code
 *
 * import .= thor.util;
 * import .= thor.container;
 *
 * task test_ncopies()
 * {
 *     var vec = nCopies<Vector<int32> >(5,3);
 *
 *     for(var v in vec)
 *     {
 *         print("\{v} ");
 *     }
 *     print("\n");
 *
 *     exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * 3 3 3 3 3
 * @endverbatim
 *
 * @param n Number of copies.
 * @param element Element to be copied.
 *
 * @return The @c CollectionType container with @c n copies of @c element.
 *
 */
@cpu
function nCopies<CollectionType,Element>( n: int64, element: Element ) : CollectionType
{
    var collection: CollectionType = new CollectionType;
    for( ; 0 < n; --n )
        collection.pushBack( element );
    return collection;
}

/**
 * @brief Clockwise rotate a sequential container.
 *
 * Clockwise rotate a sequential container for a given step.
 *
 * Example:
 * @code
 *
 * import .= thor.util;
 *
 * task test_rotate()
 * {
 *     var arr = [1,2,3,4,5];
 *
 *     rotate(arr, 3);
 *
 *     for(var v in arr)
 *     {
 *         print("\{v} ");
 *     }
 *     print("\n");
 *
 *     exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * 3 4 5 1 2
 * @endverbatim
 *
 * @param collection The input sequential container.
 * @param step The given step of rotating.
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function rotate<CollectionType,Size>( collection: CollectionType, step: Size ) : void;

/**
 * @brief Replace all elements with value @c oldVal to @c newVal.
 *
 * Example:
 * @code
 *
 * import .= thor.util;
 *
 * task test_replace()
 * {
 *     var arr = [1,2,1,2,1];
 *
 *     replaceAll(arr, 1, 2);
 *
 *     for(var v in arr)
 *     {
 *         print("\{v} ");
 *     }
 *     print("\n");
 *
 *     exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * 2 2 2 2 2
 * @endverbatim
 *
 * @param collection The input sequential container.
 * @param oldVal The target value to be replaced.
 * @param newVal The value which is going to replace @c oldVal.
 *
 * @retval true At least one element is replaced by @c newVal.
 * @retval false No element is replaced.
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function replaceAll<CollectionType,OldVal,NewVal>( collection: CollectionType, oldVal: OldVal, newVal: NewVal ) : bool;

/**
 * @brief Binary search for a element.
 *
 * Example:
 * @code
 *
 * task test_binary_search()
 * {
 *     var arr = [1,3,5];
 *
 *     println(binarySearch(arr,3));
 *     println(binarySearch(arr,1));
 *     println(binarySearch(arr,6));
 *     println(binarySearch(arr,0));
 *     println(binarySearch(arr,2));
 *
 *     exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * 1
 * 0
 * 3
 * -1
 * 1
 * @endverbatim
 *
 * @param collection The input sequential container.
 * @param key The element to be found.
 *
 * @return
 * If @c key is found, its index is returned. If @c key is less than the smallest
 * of @c collection, return -1. If @c key is greater than the largest of @c colletion,
 * return the size of @c collection. If @c key is not in @collection and it's between
 * the smallest and the largest, the index of the largest of those elements smaller
 * than @c key is returned.
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function binarySearch<CollectionType,Element>( collection: CollectionType, key: Element ) : int64;

/**
 * @brief Binary search for a element using a user-defined comparator.
 *
 * Perform binary search on a sequential container to find a given element.
 * It's the same as binarySearch<CollectionType,Element> except you can define your
 * own comparator with this version.
 *
 * A user-defined comparator is a class with a method @c compare().
 * It should be a function accept two arguments as consistent type with your
 * target sequential container. It should return @b negative value if the
 * first argument is less than the second, a @b positive value if the first
 * argument is @b greater then the second, and @b zero if they are equal.
 *
 * @param collection The input sequential container.
 * @param key The element to be found.
 * @param compare A user-defined comparator.
 *
 * @see binarySearch<CollectionType,Element>
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function binarySearch<CollectionType,Element,Compare>( collection: CollectionType, key: Element, compare: Compare ) : int64;

/**
 * @brief Find the begin index of the first consecutive subsequence matching the given one.
 *
 * Example:
 *
 * @code
 *
 * task test_index_sub()
 * {
 *     var arr = [3,4,5,3,4,5];
 *
 *     println(indexOfSubList(arr, [3,4]));
 *     println(indexOfSubList(arr, [3,5]));
 *     println(indexOfSubList(arr, [9,10]));
 *
 *     exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * 0
 * -1
 * -1
 * @endverbatim
 *
 * @param source The target sequential container.
 * @param target The consecutive subsequence which is going to be found.
 * @return The begin index of the first consecutive subsequence matching @c target in @c source.
 * -1 if it's not found.
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function indexOfSubList<CollectionType1,CollectionType2>( source: CollectionType1, target: CollectionType2 ) : int64;

/**
 * @brief Find the begin index of the last consecutive subsequence matching the given one.
 *
 * Example:
 *
 * @code
 *
 * task test_index_sub()
 * {
 *     var arr = [3,4,5,3,4,5];
 *
 *     println(lastIndexOfSubList(arr, [3,4]));
 *     println(lastIndexOfSubList(arr, [3,5]));
 *     println(lastIndexOfSubList(arr, [9,10]));
 *
 *     exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * 3
 * -1
 * -1
 * @endverbatim
 *
 * @param source The target sequential container.
 * @param target The consecutive subsequence which is going to be found.
 * @return The begin index of the last consecutive subsequence matching @c target in @c source.
 * -1 if it's not found.
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function lastIndexOfSubList<CollectionType1,CollectionType2>( source: CollectionType1, target: CollectionType2 ) : int64;

/**
 * @brief Find index of the first element matching the given value.
 *
 * Example:
 * @code
 *
 * task test_indexof()
 * {
 *     var arr = [3,1,4,5,9,8,5];
 *     println(indexOf(arr,5));
 *     println(indexOf(arr,7));
 *
 *     exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * 3
 * -1
 * @endverbatim
 *
 * @param collection The input sequential container.
 * @param element The target value.
 * @return Index of the first element matching the given value. -1 if it's not found.
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function indexOf<CollectionType,Element>( collection: CollectionType, element: Element ) : int64;

/**
 * @brief Find index of the last element matching the given value.
 *
 * Example:
 * @code
 *
 * task test_lastindexof()
 * {
 *     var arr = [3,1,4,5,9,8,5];
 *     println(lastIndexOf(arr,5));
 *     println(lastIndexOf(arr,7));
 *
 *     exit(0);
 * }
 *
 * @endcode
 *
 * Output:
 * @verbatim
 * 6
 * -1
 * @endverbatim
 *
 * @param collection The input sequential container.
 * @param element The target value.
 * @return Index of the last element matching the given value. -1 if it's not found.
 *
 */
@cpu
@native { include="thor/util/Collections.h" }
function lastIndexOf<CollectionType,Element>( collection: CollectionType, element: Element ) : int64;
