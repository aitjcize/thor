/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

import .= thor.lang;

/**
 * @brief Provides utilities to generate string representation from
 *        variables or vice versa.
 * @remark Users should use static member functions by @c Convert
 *         class name and not trying create instances of this class.
 */
@cpu
class Convert extends Object
{
    /**
     * @brief Constructor. This function is just an interface for C++
     *        compatibility.
     */
    private function new():void { }

    /**
     * @brief Destructor. This function is just an interface for C++
     *        compatibility.
     */
    private function delete():void { }

    /**
     * @brief Convert string content into int8 value.
     * @param @c s The source string.
     * @return The result value from string. For example, string "123"
     *         will be converted to 123.
     * @remark there is no way to know if string content is valid or not,
     *         for example: "@" or "~".
     */
    @native
    public static function toInt8(s: String): int8;

    /**
     * @brief Convert string content into int16 value.
     * @param @c s The source string.
     * @return The result value from string. For example, string "1234"
     *         will be converted to 1234.
     * @remark there is no way to know if string content is valid or not,
     *         for example: "@" or "~".
     */
    @native
    public static function toInt16(s: String): int16;

    /**
     * @brief Convert string content into int32 value.
     * @param @c s The source string.
     * @return The result value from string. For example, string "12345"
     *         will be converted to 12345.
     * @remark there is no way to know if string content is valid or not,
     *         for example: "@" or "~".
     */
    @native
    public static function toInt32(s: String): int32;

    /**
     * @brief Convert string content into int64 value.
     * @param @c s The source string.
     * @return The result value from string. For example, string "123456"
     *         will be converted to 123456L.
     * @remark there is no way to know if string content is valid or not,
     *         for example: "@" or "~".
     */
    @native
    public static function toInt64(s: String): int64;

    /**
     * @brief Convert string content into float32 value.
     * @param @c s The source string.
     * @return The result value from string. For example, string "543.21"
     *         will be converted to 543.21f.
     * @remark there is no way to know if string content is valid or not,
     *         for example: "@" or "~".
     */
    @native
    public static function toFloat32(s: String): float32;

    /**
     * @brief Convert string content into float64 value.
     * @param @c s The source string.
     * @return The result value from string. For example, string "54.321"
     *         will be converted to 54.321.
     * @remark there is no way to know if string content is valid or not,
     *         For example: "@" or "~".
     */
    @native
    public static function toFloat64(s: String): float64;

    /**
     * @brief Create a @c String which represents builtin types.
     * @param @c value The source value which has builtin type.
     * @return A string representation of the passed value. For example:
     *         float32 parameter 54.321f will produces a string "54.321".
     * @remark there is no way to know if string content is valid or not,
     *         for example: "@" or "~".
     */
    private static function buildString<Value>(value: Value): String
    {
        var builder : StringBuilder = new StringBuilder();
        builder.append(value);

        return builder.toString();
    }

    /**
     * @brief Create a @c String which represents an int8 value.
     * @param @c v The source int8 value.
     * @return A String representation of the passed value. For example:
     *         parameter 123 will produces a string "123"
     */
    public static function toString(v: int8): String
    {
       return buildString(v);
    }

    /**
     * @brief Create a @c String which represents an int16 value.
     * @param @c v The source int16 value.
     * @return A String representation of the passed value. For example:
     *         parameter 1234 will produces a string "1234"
     */
    public static function toString(v: int16): String
    {
       return buildString(v);
    }

    /**
     * @brief Create a @c String which represents an int32 value.
     * @param @c v The source int32 value.
     * @return A String representation of the passed value. For example:
     *         parameter 12345 will produces a string "12345"
     */
    public static function toString(v: int32): String
    {
       return buildString(v);
    }

    /**
     * @brief Create a @c String which represents an int64 value.
     * @param @c v The source int64 value.
     * @return A String representation of the passed value. For example:
     *         parameter 123456L will produces a string "123456"
     */
    public static function toString(v: int64): String
    {
       return buildString(v);
    }

    /**
     * @brief Create a @c String which represents an float32 value.
     * @param @c v The source float32 value.
     * @return A String representation of the passed value. For example:
     *         parameter 54.321f will produces a string "54.321"
     */
    public static function toString(v: float32): String
    {
       return buildString(v);
    }

    /**
     * @brief Create a @c String which represents an float64 value.
     * @param @c v The source float64 value.
     * @return A String representation of the passed value. For example:
     *         parameter 543.21 will produces a string "543.21"
     */
    public static function toString(v: float64): String
    {
       return buildString(v);
    }

    /**
     * @brief Create a @c String which represents an Object which has
     *        user-defined type.
     *
     * The is a function template which can take any user-defined type
     * objects. Finally it returns the result from calling member
     * function @c toString() on parameter @c value. The @c toString()
     * return type should be convertible to class @c thor.lang.String.
     *
     * @param @c value The user-defined type object.
     * @return The result string returned by value.toString().
     */
    @native { include="thor/util/Convert.h" }
    public static function toString<Value>(value: Value): String;
}
