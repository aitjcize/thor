/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include "language/stage/import/Constants.h"
#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/CxxPimplGrammar.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/Helpers.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
cxx_pimpl_getter_decl_grammar<iterator>::cxx_pimpl_getter_decl_grammar() : cxx_pimpl_getter_decl_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::string;

    using boost::phoenix::at_c;

    start = pimpl.alias();

    pimpl =
           string[_1 = helpers::ts_decl_wrapped_name(at_c<0>(_val))]
        << "* "
        << optional_scope[_1 = _val]
        << CXX_FUNCTION_GET_PIMPL_NAME
        << "()"
    ;
}

template<typename iterator>
cxx_pimpl_getter_def_grammar<iterator>::cxx_pimpl_getter_def_grammar() : cxx_pimpl_getter_def_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;

    using boost::phoenix::construct;

    start = pimpl.alias();

    pimpl =
           pimpl_decl[_1 = construct<boost::fusion::vector<ts_class*, bool>>(_val, true)] << eol
        << pimpl_body
    ;

    pimpl_body =
           '{' << eol
        << "    return " << CXX_VARIABLE_IMPL_NAME << ';' << eol
        << '}' << eol
        << eol
    ;
}

template<typename iterator>
cxx_pimpl_setter_decl_grammar<iterator>::cxx_pimpl_setter_decl_grammar() : cxx_pimpl_setter_decl_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eps;
    using boost::spirit::karma::lit;
    using boost::spirit::karma::string;

    using boost::phoenix::at_c;

    start = pimpl.alias();

    pimpl =
           lit(CXX_VOID_NAME)
        << ' '
        << optional_scope[_1 = _val]
        << CXX_FUNCTION_SET_PIMPL_NAME
        << '('
        << string[_1 = helpers::ts_decl_wrapped_name(at_c<0>(_val))]
        << "* "
        << CXX_VARIABLE_LOCAL_IMPL_NAME
        << ", bool "
        << CXX_VARIABLE_LOCAL_OWN_IMPL_NAME
        << ')'
    ;
}

template<typename iterator>
cxx_pimpl_setter_def_grammar<iterator>::cxx_pimpl_setter_def_grammar() : cxx_pimpl_setter_def_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;

    using boost::phoenix::construct;

    start = pimpl.alias();

    pimpl =
           pimpl_decl[_1 = construct<boost::fusion::vector<ts_class*, bool>>(_val, true)] << eol
        << pimpl_body
    ;

    pimpl_body =
           '{' << eol
        << "    if (" << CXX_VARIABLE_OWN_IMPL_NAME << ')' << eol
        << "    {" << eol
        << "        delete " << CXX_VARIABLE_IMPL_NAME << ';' << eol
        << "    }" << eol
        << eol
        << "    " << CXX_VARIABLE_OWN_IMPL_NAME << " = " << CXX_VARIABLE_LOCAL_OWN_IMPL_NAME << ';' << eol
        << "    " << CXX_VARIABLE_IMPL_NAME     << " = " << CXX_VARIABLE_LOCAL_IMPL_NAME     << ';' << eol
        << '}' << eol
        << eol
    ;
}

template<typename iterator>
cxx_pimpl_creator_decl_grammar<iterator>::cxx_pimpl_creator_decl_grammar() : cxx_pimpl_creator_decl_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;

    start = pimpl.alias();

    pimpl =
           "static "
        << qualified_identifier
        << "* "
        << CXX_FUNCTION_CREATOR_PIMPL_NAME
        << "()"
    ;
}

TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_pimpl_getter_decl_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_pimpl_getter_def_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_pimpl_setter_decl_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_pimpl_setter_def_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_pimpl_creator_decl_grammar)

} } } } }
