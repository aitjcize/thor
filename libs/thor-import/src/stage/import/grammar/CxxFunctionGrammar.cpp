/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/fusion/container/vector.hpp>
#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include "language/stage/import/Constants.h"
#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/CxxFunctionGrammar.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/Helpers.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
cxx_function_decl_grammar<iterator>::cxx_function_decl_grammar() : cxx_function_decl_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eps;

    using boost::phoenix::at_c;
    using boost::phoenix::construct;
    using boost::phoenix::empty;

    start = function.alias();

    function =
           optional_return_type[_1 = helpers::ts_function_result_type(at_c<0>(_val))]
        << optional_scope      [_1 = construct<boost::fusion::vector<ts_class*, bool>>(helpers::ts_decl_member_of(at_c<0>(_val)), at_c<1>(_val))]
        << function_name       [_1 = at_c<0>(_val)]
        << '('
        << parameters          [_1 = helpers::ts_function_parameters(at_c<0>(_val))]
        << ')'
    ;

    function_name =
          eps(helpers::ts_function_is_constructor(_val)) <<     constructor_name_impl
        | eps(helpers::ts_function_is_destructor (_val)) <<      destructor_name_impl
        |                                                   normal_function_name_impl
    ;

    constructor_name_impl =
        identifier[_1 = helpers::ts_decl_id(helpers::ts_decl_member_of(_val))]
    ;

    destructor_name_impl =
        '~' << identifier[_1 = helpers::ts_decl_id(helpers::ts_decl_member_of(_val))]
    ;

    normal_function_name_impl =
        identifier[_1 = helpers::ts_decl_id(_val)]
    ;

    optional_return_type =
          eps(helpers::ts_type_is_none(_val))
        | (
               type[_1 = _val]
            << ' '
          )
    ;

    parameters =
          eps(empty(_val))
        | parameter % ", "
    ;
}

template<typename iterator>
cxx_function_def_grammar<iterator>::cxx_function_def_grammar() : cxx_function_def_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::eps;
    using boost::spirit::karma::lit;
    using boost::spirit::karma::string;

    using boost::phoenix::construct;
    using boost::phoenix::empty;

    start = function.alias();

    function =
           function_decl          [_1 = construct<boost::fusion::vector<ts_function*, bool>>(_val, true)] << eol
        << (
               eps(helpers::ts_function_is_constructor(_val)) << constructor_body
             | eps(helpers::ts_function_is_destructor (_val)) << destructor_body
             |                                                     function_body
           )                      [_1 = _val]
        << eol
        << eol
    ;

    constructor_body =
           "    : " << lit(CXX_THOR_LANG_OBJECT_NAME ) << "()"        << eol
        << "    , " << lit(CXX_VARIABLE_OWN_IMPL_NAME) << "(false)"   << eol
        << "    , " << lit(CXX_VARIABLE_IMPL_NAME    ) << "(nullptr)" << eol
        << '{' << eol
        << parameter_pre_processings    [_1 = helpers::ts_function_parameters(_val)]
        << constructor_pimpl_initializer[_1 = _val]
        << '}'
    ;

    constructor_pimpl_initializer =
           "    // initializing Pimpl" << eol
        << "    " << CXX_VARIABLE_IMPL_NAME << " = new " << string[_1 = helpers::ts_decl_wrapped_name(helpers::ts_decl_member_of(_val))] << eol
        << "    (" << eol
        << parameter_passings[_1 = helpers::ts_function_parameters(_val)]
        << "    );" << eol
        << "    " << CXX_VARIABLE_OWN_IMPL_NAME << " = true;" << eol
    ;

    destructor_body =
           '{' << eol
        << "    // uninitializing Pimpl" << eol
        << "    " << CXX_FUNCTION_SET_PIMPL_NAME << "(nullptr, false);" << eol
        << '}'
    ;

    function_body =
           '{' << eol
        << parameter_pre_processings    [_1 = helpers::ts_function_parameters(_val)]
        << function_return              [_1 = _val]
        << '}'
    ;

    parameter_pre_processings =
           "    // parameter conversions" << eol
        << (*parameter_conversion)[_1 = _val] << eol
        << "    // parameter aliasings (post actions)" << eol
        << (*parameter_aliasing  )[_1 = _val] << eol
    ;

    parameter_passings =
          eps(empty(_val))
        | (("                " << parameter_passing) % ( ',' << eol)) << eol
    ;

#define CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_DISPATCH(r, base_rule, method)                                             \
    eps(helpers::BOOST_PP_CAT(ts_parameter_is_wrapping_, method)(_val)) << BOOST_PP_CAT(BOOST_PP_CAT(base_rule, _), method) |

#define CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_IMPL(base_rule)                                                                                \
    base_rule =                                                                                                                                     \
        BOOST_PP_SEQ_FOR_EACH(CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_DISPATCH, base_rule, CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_WRAP_METHODS) \
        eps(false)

    CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_IMPL(parameter_conversion);
    CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_IMPL(parameter_aliasing);
    CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_IMPL(parameter_passing);

#undef CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_DISPATCH
#undef CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_IMPL

    // hwo to unwrap parameters
    //           | conversion |    aliasing
    // ----------------------------------------
    // by-pass   |            |
    // ----------------------------------------
    // pointer   |  un-pimpl  |
    // ----------------------------------------
    // value     |   deref    |
    // ----------------------------------------
    // reference |   deref    |
    // ----------------------------------------
    // c_string  |   convert  |          c_str
    // ----------------------------------------
    // c_wstring |            |  deref + c_str

    parameter_conversion_by_pass = eps;
    parameter_aliasing_by_pass   = eps;
    parameter_passing_by_pass    = parameter_plain_name.alias();

    parameter_conversion_pointer =
           "    auto* "
        << parameter_conversion_name[_1 = _val]
        << " = "
        << parameter_plain_name     [_1 = _val]
        << " == nullptr ? nullptr : "
        << parameter_plain_name     [_1 = _val]
        << "->"
        << CXX_FUNCTION_GET_PIMPL_NAME
        << "();"
        << eol
    ;

    parameter_aliasing_pointer = eps;
    parameter_passing_pointer  = parameter_conversion_name.alias();

    parameter_conversion_value =
           "    auto& "
        << parameter_conversion_name[_1 = _val]
        << " = *"
        << parameter_plain_name     [_1 = _val]
        << "->"
        << CXX_FUNCTION_GET_PIMPL_NAME
        << "();"
        << eol
    ;

    parameter_aliasing_value = eps;
    parameter_passing_value  = parameter_conversion_name.alias();

    parameter_conversion_reference = parameter_conversion_value.alias();
    parameter_aliasing_reference   = parameter_aliasing_value.alias();
    parameter_passing_reference    = parameter_passing_value.alias();

    parameter_conversion_c_string =
           "    const auto& "
        << parameter_conversion_name[_1 = _val]
        << " = "
        << parameter_plain_name     [_1 = _val]
        << " == nullptr ? std::string() : "
        << CXX_BUILTIN_FUNCTION_TO_STRING_NAME
        << "(*"
        << parameter_plain_name[_1 = _val]
        << ");"
        << eol
    ;

    parameter_aliasing_c_string =
           "    const auto* "
        << parameter_aliasing_name  [_1 = _val]
        << " = "
        << parameter_plain_name     [_1 = _val]
        << " == nullptr ? nullptr : "
        << parameter_conversion_name[_1 = _val]
        << ".c_str();"
        << eol
    ;

    parameter_passing_c_string = parameter_aliasing_name.alias();

    parameter_conversion_c_wstring = eps;

    parameter_aliasing_c_wstring =
           "    const auto* "
        << parameter_aliasing_name  [_1 = _val]
        << " = "
        << parameter_plain_name     [_1 = _val]
        << " == nullptr ? nullptr : "
        << parameter_plain_name     [_1 = _val]
        << "->data->c_str();"
        << eol
    ;

    parameter_passing_c_wstring = parameter_aliasing_name.alias();

    parameter_conversion_name =
        parameter_plain_name << "_conversion_" << CXX_LOCAL_VARIABLE_SUFFIX
    ;

    parameter_aliasing_name =
        parameter_plain_name << "_alias_" << CXX_LOCAL_VARIABLE_SUFFIX
    ;

    parameter_plain_name =
        identifier[_1 = helpers::ts_decl_id(_val)]
    ;

#define CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_PRE_DISPATCH(r, base_rule, method)                                             \
         eps(helpers::BOOST_PP_CAT(ts_function_is_result_wrapping_, method)(_val))                                           \
      << BOOST_PP_CAT(BOOST_PP_CAT(BOOST_PP_CAT(base_rule, _), method), _pre )[_1 = helpers::ts_function_result_type(_val)]  \
    |

#define CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_POST_DISPATCH(r, base_rule, method)                                            \
         eps(helpers::BOOST_PP_CAT(ts_function_is_result_wrapping_, method)(_val))                                           \
      << BOOST_PP_CAT(BOOST_PP_CAT(BOOST_PP_CAT(base_rule, _), method), _post)[_1 = helpers::ts_function_result_type(_val)]  \
    |

#define CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_IMPL(base_rule)                                                                                        \
    base_rule =                                                                                                                                        \
           (                                                                                                                                           \
             BOOST_PP_SEQ_FOR_EACH(CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_PRE_DISPATCH , base_rule, CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_METHODS)  \
             eps(false)                                                                                                                                \
           )                                                                                                                                           \
        << string[_1 = helpers::ts_decl_wrapped_name(_val)] << eol                                                                                   \
        << "            (" << eol                                                                                                                          \
        << parameter_passings[_1 = helpers::ts_function_parameters(_val)]                                                                            \
        << "            )"                                                                                                                                 \
        << (                                                                                                                                           \
             BOOST_PP_SEQ_FOR_EACH(CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_POST_DISPATCH, base_rule, CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_METHODS)  \
             eps(false)                                                                                                                                \
           )

    CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_IMPL(function_return);

    function_return_by_pass_pre =
        eps << "    return "
    ;

    function_return_by_pass_post =
        eps << ';' << eol
    ;

    function_return_copy_create_pre =
           "    auto* result_" << lit(CXX_LOCAL_VARIABLE_SUFFIX) << " = " << qualified_identifier[_1 = helpers::ts_type_get_class(_val)] << "::" << CXX_FUNCTION_CREATOR_PIMPL_NAME << "();" << eol
        << "    result_" << CXX_LOCAL_VARIABLE_SUFFIX << "->" << CXX_FUNCTION_SET_PIMPL_NAME << '(' << eol
        << "        new " << string[_1 = helpers::ts_decl_wrapped_name(helpers::ts_type_get_class(_val))] << '(' << eol
        << "            "
    ;

    function_return_copy_create_post =
           eol
        << "        )," << eol
        << "        true" << eol
        << "    );" << eol
        << "    return result_" << CXX_LOCAL_VARIABLE_SUFFIX << ';' << eol
    ;

#undef CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_IMPL
#undef CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_POST_DISPATCH
#undef CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_PRE_DISPATCH
}

TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_function_decl_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_function_def_grammar)

} } } } }
