/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/fusion/container/vector.hpp>
#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/TsTypeGrammar.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/Helpers.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
ts_type_grammar<iterator>::ts_type_grammar() : ts_type_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eps;
    using boost::spirit::karma::string;

    using boost::phoenix::at_c;
    using boost::phoenix::construct;

    start = type.alias();

    type =
          eps(helpers::ts_type_is_primitive(at_c<0>(_val))) << primitive[_1 = helpers::ts_type_get_primitive(at_c<0>(_val))]
        | eps(helpers::ts_type_is_builtin  (at_c<0>(_val))) << builtin  [_1 = helpers::ts_type_get_builtin  (at_c<0>(_val))]
        | eps(helpers::ts_type_is_class    (at_c<0>(_val))) << cls
                                                               [
                                                                   _1 = construct<boost::fusion::vector<ts_class*, ts_package*>>
                                                                        (
                                                                            helpers::ts_type_get_class(at_c<0>(_val)),
                                                                            at_c<1>(_val)
                                                                        )
                                                               ]
    ;

    primitive =
        string[_1 = helpers::ts_name(_val)]
    ;

    builtin =
        string[_1 = helpers::ts_name(_val)]
    ;

    cls =
        qualified_identifier
    ;
}

TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(ts_type_grammar)

} } } } }
