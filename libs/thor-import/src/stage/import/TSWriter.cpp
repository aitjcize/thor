/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <iterator>
#include <string>
#include <vector>

#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/spirit/include/karma_generate.hpp>

#include "language/stage/import/Constants.h"
#include "language/stage/import/TSInfo.h"
#include "language/stage/import/TSWriter.h"
#include "language/stage/import/grammar/CxxPackageGrammar.h"
#include "language/stage/import/grammar/TsPackageGrammar.h"

namespace zillians { namespace language { namespace stage { namespace import {

namespace {

bool dump_package(const boost::filesystem::path& output_dir, const ts_package& package)
{
    bool success =
        (boost::filesystem::is_directory(output_dir) || boost::filesystem::create_directories(output_dir)) &&
        std::all_of(
            package.children.begin(),
            package.children.end(),
            [&output_dir](const ts_package* child) -> bool
            {
                return dump_package(output_dir / child->id->name, *child);
            }
        )
    ;

    if(success)
    {
        boost::filesystem::ofstream cxx_fwd_output   (output_dir / CXX_HEADER_FWD_FILENAME);
        boost::filesystem::ofstream cxx_header_output(output_dir / CXX_HEADER_FILENAME);
        boost::filesystem::ofstream cxx_source_output(output_dir / CXX_SOURCE_FILENAME);
        boost::filesystem::ofstream ts_source_output (output_dir / TS_SOURCE_FILENAME);

#define TS_WRITER_OUTPUT_IS_OK()        \
    (                                   \
        cxx_fwd_output    &&            \
        cxx_header_output &&            \
        cxx_source_output &&            \
        ts_source_output                \
    )

        grammar::cxx_package_fwd_grammar<std::ostream_iterator<char>>  cxx_fwd_grammar;
        grammar::cxx_package_decl_grammar<std::ostream_iterator<char>> cxx_header_grammar;
        grammar::cxx_package_def_grammar<std::ostream_iterator<char>>  cxx_source_grammar;
        grammar::ts_package_grammar<std::ostream_iterator<char>>       ts_source_grammar;

        using boost::spirit::karma::generate;

        success =
            TS_WRITER_OUTPUT_IS_OK()                                                                                        &&
            generate(std::ostream_iterator<char>(cxx_fwd_output   ), cxx_fwd_grammar   , const_cast<ts_package*>(&package)) &&
            generate(std::ostream_iterator<char>(cxx_header_output), cxx_header_grammar, const_cast<ts_package*>(&package)) &&
            generate(std::ostream_iterator<char>(cxx_source_output), cxx_source_grammar, const_cast<ts_package*>(&package)) &&
            generate(std::ostream_iterator<char>(ts_source_output ), ts_source_grammar , const_cast<ts_package*>(&package)) &&
            TS_WRITER_OUTPUT_IS_OK()
        ;

#undef  TS_WRITER_OUTPUT_IS_OK
    }

    return success;
}

}

TSWriter::TSWriter(const boost::filesystem::path& root)
    : root(root)
{
}

bool TSWriter::write(const ts_package& package_root)
{
    bool success = dump_package(
        root,
        package_root
    );

    return success;
}

} } } }
