/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#include <string>
#include <utility>

#include <boost/assert.hpp>
#include <boost/range/adaptor/reversed.hpp>
#include <boost/range/algorithm/replace.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/variant.hpp>

#include "language/stage/import/TSInfo.h"
#include "language/stage/import/TSInfoPostProcessings.h"

namespace zillians { namespace language { namespace stage { namespace import {

namespace {

std::string random_underscored_uuid()
{
    std::string result = boost::uuids::to_string(boost::uuids::random_generator()());

    boost::replace(result, '-', '_');

    return result;
}

void rename_class_impl(ts_class& cls)
{
    BOOST_ASSERT(cls.id && "null pointer exception");

    const bool need_scope_name = cls.origin_scope != nullptr;
    const bool need_reset_name = cls.id->name.empty();
    const bool need_rename     = need_scope_name || need_reset_name;

    if(need_rename)
    {
        std::string new_name;

        if(need_scope_name)
        {
            new_name = cls.origin_scope->id->name + '_';
        }

        if(need_reset_name)
        {
            new_name += "unnamed";
        }
        else
        {
            new_name += cls.id->name;
        }

        new_name += '_';
        new_name += random_underscored_uuid();

        cls.id->name = std::move(new_name);
    }
}

}

void recursively_rename_classes(ts_package& package)
{
    struct declaration_visitor : public boost::static_visitor<>
    {
        result_type operator()(ts_class* cls) const
        {
            BOOST_ASSERT(cls && "null pointer exception");

            rename_class_impl(*cls);
        }

        result_type operator()(ts_function* func) const
        {
        }
    } visitor;

    for(const auto& variant: package.declarations | boost::adaptors::reversed)
    {
        boost::apply_visitor(visitor, variant);
    }

    for(ts_package* child: package.children)
    {
        BOOST_ASSERT(child && "null pointer exception");

        recursively_rename_classes(*child);
    }
}

} } } }
