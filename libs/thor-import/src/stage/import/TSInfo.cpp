/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <string>
#include <unordered_set>
#include <vector>

#include <boost/assert.hpp>

#include "language/stage/import/TSInfo.h"

namespace zillians { namespace language { namespace stage { namespace import {

namespace {

std::unordered_set<ts_node*> ts_all_nodes;

}

ts_node::ts_node()
{
    ts_all_nodes.insert(this);
}

ts_node::~ts_node()
{
    ts_all_nodes.erase(this);
}

ts_package* ts_decl::get_package() const noexcept
{
    return scope;
}

ts_package* ts_parameter::get_package() const noexcept
{
    BOOST_ASSERT(parameter_of != nullptr && "null pointer exception");

    return parameter_of->get_package();
}

bool ts_function::is_constructor() const noexcept
{
    return type == CONSTRUCTOR;
}

bool ts_function::is_destructor() const noexcept
{
    return type == DESTRUCTOR;
}

bool ts_getset::has_getter() const noexcept
{
    return getter_wrapping != GETTER_NO_WRAP;
}

bool ts_getset::has_getter_by_pass() const noexcept
{
    return (getter_wrapping & GETTER_BY_PASS) != 0;
}

bool ts_getset::has_getter_address_of() const noexcept
{
    return (getter_wrapping & GETTER_ADDRESS_OF) != 0;
}

bool ts_getset::has_getter_array_element() const noexcept
{
    return (getter_wrapping & GETTER_ARRAY_ELEM) != 0;
}

bool ts_getset::has_getter_array_size() const noexcept
{
    return (getter_wrapping & GETTER_ARRAY_SIZE) != 0;
}

bool ts_getset::has_setter() const noexcept
{
    return setter_wrapping != SETTER_NO_WRAP;
}

bool ts_getset::has_setter_by_pass() const noexcept
{
    return (setter_wrapping & SETTER_BY_PASS) != 0;
}

bool ts_getset::has_setter_copy_assign() const noexcept
{
    return (setter_wrapping & SETTER_COPY_ASSIGN) != 0;
}

bool ts_getset::has_setter_array_element() const noexcept
{
    return (setter_wrapping & SETTER_ARRAY_ELEM) != 0;
}

bool ts_package::comparer::operator()(const ts_package* lhs, const ts_package* rhs) const
{
    BOOST_ASSERT(lhs && "null pointer exception");
    BOOST_ASSERT(rhs && "null pointer exception");
    BOOST_ASSERT(lhs->id && "null pointer exception");
    BOOST_ASSERT(rhs->id && "null pointer exception");

    return lhs->id->name < rhs->id->name;
}

ts_package& ts_package::get_common_parent(const ts_package& another) const
{
    ts_package* lhs = const_cast<ts_package*>(this);
    ts_package* rhs = const_cast<ts_package*>(&another);

    unsigned lhs_level =         depth;
    unsigned rhs_level = another.depth;

    for(; lhs_level > rhs_level; --lhs_level, lhs = lhs->scope);
    for(; rhs_level > lhs_level; --rhs_level, rhs = rhs->scope);

    for(; lhs != rhs; lhs = lhs->scope, rhs = rhs->scope);

    BOOST_ASSERT(lhs && rhs && "null pointer exception");
    BOOST_ASSERT(lhs == rhs && "common parent is not found!");
    BOOST_ASSERT(        depth >= lhs->depth && "depth of current package is smaller than the common parent!?");
    BOOST_ASSERT(another.depth >= lhs->depth && "depth of another package is smaller than the common parent!?");

    return *lhs;
}

const std::vector<std::string>& ts_package::get_wrapped_includes() const
{
    if(scope != nullptr)
    {
        return scope->get_wrapped_includes();
    }
    else
    {
        BOOST_ASSERT(wrapped_includes && "null pointer exception");

        return *wrapped_includes;
    }
}

void ts_node::clean()
{
    using std::swap;

    decltype(ts_all_nodes) temp_ts_all_nodes;

    swap(temp_ts_all_nodes, ts_all_nodes);

    for(ts_node* node: temp_ts_all_nodes)
    {
        delete node;
    }
}

} } } }
