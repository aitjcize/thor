/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>
#include <vector>
#include <map>

#include "utility/UnicodeUtil.h"

#include "language/context/LogInfoContext.h"
#include "language/logging/LoggerWrapper.h"
#include "language/logging/StringTable.h"
#include "language/tree/ASTNodeFactory.h"

#include "language/stage/verifier/visitor/StaticTestVerificationStageVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

//////////////////////////////////////////////////////////////////////////////
// Static function implementation

static void printLogInfoVec(const std::vector<LogInfo>& logInfoVec)
{
    for(std::vector<LogInfo>::size_type i = 0; i != logInfoVec.size(); ++i)
    {
        std::wcout << L"  [" << i + 1 << L'/' << logInfoVec.size() << L']' << std::endl ;
        std::wcout << L"    log_level : " << logInfoVec[i].log_level << std::endl ;
        std::wcout << L"    log_id    : " << logInfoVec[i].log_id << std::endl ;
        std::wcout << L"    parameters: " ;
        if(logInfoVec[i].parameters.empty())
        {
            std::wcout << L"(NONE)" << std::endl ;
        }
        else {
            zillians::for_each_and_pitch(
                logInfoVec[i].parameters,
                [](decltype(logInfoVec[i].parameters)::const_reference param){
                    std::wcout << param.first << ": " << param.second << std::endl;
                },
                [](){ std::wcout << "                "; }
            );
        }
    }
}

static void dumpMultipleMismatchLogInfoError(ASTNode* errorNode, const std::vector<LogInfo>& annotatedLogInfo, const std::vector<LogInfo>& hookedLogInfo)
{
    std::wcout << L"Rest mismatched hooked LogInfo on ASTNode and annotation." << std::endl ;

    std::wcout << L"LogInfo on ASTNode: " << hookedLogInfo.size() << std::endl ;
    printLogInfoVec(hookedLogInfo);

    std::wcout << L"LogInfo on annotation: " << annotatedLogInfo.size() << std::endl ;
    printLogInfoVec(annotatedLogInfo);

}

static std::wstring getLiteralString(Literal* literal)
{
    BOOST_ASSERT(isa<StringLiteral>(literal) || isa<NumericLiteral>(literal));
    if(isa<StringLiteral>(literal))
    {
        return cast<StringLiteral>(literal)->value;
    }
    else
    {
        NumericLiteral* numericLiteral = cast<NumericLiteral>(literal);
        std::wostringstream oss;
        switch(numericLiteral->primitive_kind)
        {
        case PrimitiveKind::kind::BOOL_TYPE    : oss << numericLiteral->value.b  ; break;
        case PrimitiveKind::kind::INT8_TYPE    : oss << numericLiteral->value.i8 ; break;
        case PrimitiveKind::kind::INT16_TYPE   : oss << numericLiteral->value.i16; break;
        case PrimitiveKind::kind::INT32_TYPE   : oss << numericLiteral->value.i32; break;
        case PrimitiveKind::kind::INT64_TYPE   : oss << numericLiteral->value.i64; break;
        case PrimitiveKind::kind::FLOAT32_TYPE : oss << numericLiteral->value.f32; break;
        case PrimitiveKind::kind::FLOAT64_TYPE : oss << numericLiteral->value.f64; break;
        default: break;
        }
        return oss.str();
    }
}

static LogInfo constructExpectMessageFromAnnotation(zillians::language::tree::ASTNode& node)
{
    using namespace zillians::language::tree;
    using zillians::language::tree::cast;

    ASTNode* expectMessageAnno = &node;

    BOOST_ASSERT(cast<Annotation>(expectMessageAnno) != NULL);
    if (cast<Annotation>(expectMessageAnno) == NULL)
    {
        LOG_MESSAGE(WRONG_STATIC_TEST_ANNOTATION_FORMAT, &node, _detail(L"child of Annotations should be Annotation"));
    }

    Annotation* anno = cast<Annotation>(expectMessageAnno);
    BOOST_ASSERT(cast<Annotation>(expectMessageAnno)->attribute_list.size() == 3 ||
                 cast<Annotation>(expectMessageAnno)->attribute_list.size() == 2);
    if (cast<Annotation>(expectMessageAnno)->attribute_list.size() < 2 ||
        cast<Annotation>(expectMessageAnno)->attribute_list.size() > 3)
    {
        LOG_MESSAGE(WRONG_STATIC_TEST_ANNOTATION_FORMAT, &node, _detail(L"number of attribute list should be 3 or 2"));
    }

    // log level
    std::pair<SimpleIdentifier*, ASTNode*> logLevel = cast<Annotation>(expectMessageAnno)->attribute_list[0];
    BOOST_ASSERT(cast<SimpleIdentifier>(logLevel.first) != NULL);
    if (cast<SimpleIdentifier>(logLevel.first) == NULL)
    {
        LOG_MESSAGE(WRONG_STATIC_TEST_ANNOTATION_FORMAT, &node, _detail(L"key should be SimpleIdentifier"));
    }

    BOOST_ASSERT(cast<SimpleIdentifier>(logLevel.first)->name == L"level");
    if (cast<SimpleIdentifier>(logLevel.first)->name != L"level")
    {
        LOG_MESSAGE(WRONG_STATIC_TEST_ANNOTATION_FORMAT, &node, _detail(L"key should be \"level\""));
    }

    BOOST_ASSERT(isa<StringLiteral>(logLevel.second));
    if (!isa<StringLiteral>(logLevel.second))
    {
        LOG_MESSAGE(WRONG_STATIC_TEST_ANNOTATION_FORMAT, &node, _detail(L"value should be StringLiteral"));
    }

    std::wstring levelString = cast<StringLiteral>(logLevel.second)->value;

    // log id
    std::pair<SimpleIdentifier*, ASTNode*> logId = cast<Annotation>(expectMessageAnno)->attribute_list[1];
    BOOST_ASSERT(cast<SimpleIdentifier>(logId.first) != NULL);
    if (cast<SimpleIdentifier>(logId.first) == NULL)
    {
        LOG_MESSAGE(WRONG_STATIC_TEST_ANNOTATION_FORMAT, &node, _detail(L"key should be SimpleIdentifier"));
    }

    BOOST_ASSERT(cast<SimpleIdentifier>(logId.first)->name == L"id");
    if (cast<SimpleIdentifier>(logId.first)->name != L"id")
    {
        LOG_MESSAGE(WRONG_STATIC_TEST_ANNOTATION_FORMAT, &node, _detail(L"key shoule be \"id\""));
    }

    BOOST_ASSERT(cast<StringLiteral>(logId.second) != NULL);
    if (!isa<StringLiteral>(logId.second))
    {
        LOG_MESSAGE(WRONG_STATIC_TEST_ANNOTATION_FORMAT, &node, _detail(L"value should be StringLiteral"));
    }

    std::wstring idString = cast<StringLiteral>(logId.second)->value;

    if (anno->attribute_list.size() == 2)
    {
        return LogInfo{levelString, idString, std::map<std::wstring, std::wstring>()};
    }

    // parameter pairs
    std::pair<SimpleIdentifier*, ASTNode*> paramPairs = cast<Annotation>(expectMessageAnno)->attribute_list[2];
    BOOST_ASSERT(cast<SimpleIdentifier>(paramPairs.first) != NULL);
    if (cast<SimpleIdentifier>(paramPairs.first) == NULL)
    {
        LOG_MESSAGE(WRONG_STATIC_TEST_ANNOTATION_FORMAT, &node, _detail(L"key should be SimpleIdentifier"));
    }

    BOOST_ASSERT(cast<SimpleIdentifier>(paramPairs.first)->name == L"parameters");
    if (cast<SimpleIdentifier>(paramPairs.first)->name != L"parameters")
    {
        LOG_MESSAGE(WRONG_STATIC_TEST_ANNOTATION_FORMAT, &node, _detail(L"key should be \"parameters\""));
    }

    BOOST_ASSERT(cast<Annotation>(paramPairs.second) != NULL);
    if (cast<Annotation>(paramPairs.second) == NULL)
    {
        LOG_MESSAGE(WRONG_STATIC_TEST_ANNOTATION_FORMAT, &node, _detail(L"value should be Annotation"));
    }

    Annotation* params = cast<Annotation>(paramPairs.second);
    std::map<std::wstring, std::wstring> paramsResult;
    for(auto& attribute : params->attribute_list)
    {
        SimpleIdentifier *paramKey = cast<SimpleIdentifier>(attribute.first);
        BOOST_ASSERT(paramKey != NULL);
        if (paramKey == NULL)
        {
            LOG_MESSAGE(WRONG_STATIC_TEST_ANNOTATION_FORMAT, &node, _detail(L"key should be SimpleIdentifier"));
        }

        Literal*const literal = cast<Literal>(attribute.second);

        BOOST_ASSERT((isa<StringLiteral, NumericLiteral>(literal)));
        if (!isa<StringLiteral, NumericLiteral>(literal))
        {
            LOG_MESSAGE(WRONG_STATIC_TEST_ANNOTATION_FORMAT, &node, _detail(L"value should be a StringLiteral or a NumericLiteral"));
        }

        std::wstring key = paramKey->name;
        std::wstring value = getLiteralString(literal);
        std::pair<std::wstring, std::wstring> param(key, value);
        paramsResult.insert(std::make_pair(key, value));
    }

    return LogInfo{levelString, idString, paramsResult};
}

//////////////////////////////////////////////////////////////////////////////
// Member function implement

std::vector<LogInfo> StaticTestVerificationStageVisitor::constructLogInfoVecFromAnnotations(zillians::language::tree::Annotations* annos)
{
    std::vector<LogInfo> annotatedLogInfoVec;
    if(annos == NULL)
    {
        return annotatedLogInfoVec;
    }

    for(auto i : annos->annotation_list)
    {
        if(i->name->name == L"static_test")
        {
            Annotation& anno = *i;
            for(std::pair<SimpleIdentifier*, ASTNode*>& attr : anno.attribute_list)
            {
                if(attr.first->name == L"expect_message")
                {
                    LogInfo logInfo = constructExpectMessageFromAnnotation(*attr.second);
                    annotatedLogInfoVec.push_back(logInfo);
                }
            }
        }
    }
    return annotatedLogInfoVec;
}

bool StaticTestVerificationStageVisitor::compareLogInfoVec(ASTNode* errorNode, const std::vector<LogInfo>& annotatedLogInfoVec, const std::vector<LogInfo>& hookedLogInfoVec)
{
    if(annotatedLogInfoVec == hookedLogInfoVec)
    {
        return true;
    }
    else
    {
        LOG_MESSAGE(WRONG_STATIC_TEST_ANNOTATION_FORMAT, errorNode, _detail(L"log info mismatch between node and annotation"));

        dumpMultipleMismatchLogInfoError(errorNode, annotatedLogInfoVec, hookedLogInfoVec);

        return false;
    }
}

/**
 * @brief Get the expect_resolution id of the function call
 * @param node The ExpressionStmt node of the function call
 * @return @p __no_annotation__ if there is no expect_resolution annotation on this node.
 *         @p (EmptyString) if the function call is not resolved
 *         @p SomeId if the function is resolved
 */
static std::wstring getUseId(ASTNode& node, const std::wstring& name)
{
    const Annotatable*const annotatable = cast<Annotatable>(&node);
    const Annotation*const  useAnno     = annotatable != nullptr ? annotatable->findAnnotation(L"static_test") : nullptr;

    if(useAnno == nullptr)
    {
        return L"__no_annotation__";
    }

    for(auto& i : useAnno->attribute_list)
    {
        SimpleIdentifier* useAnnoKey = cast<SimpleIdentifier>(i.first);
        if(useAnnoKey->name == name)
        {
            const std::wstring& useId = cast<StringLiteral>(i.second)->value;
            return useId;
        }
    }
    return L"__no_annotation__";
}

static Declaration* getDecl(ASTNode& node)
{
    ASTNode*         stmtNode = &node;
    ExpressionStmt*  exprStmt = cast<ExpressionStmt>(stmtNode);
    DeclarativeStmt* declStmt = cast<DeclarativeStmt>(stmtNode);
    Declaration*     decl     = cast<Declaration>(stmtNode);
    // function call
    if(exprStmt != NULL)
    {
        if(CallExpr* callExpr = cast<CallExpr>(exprStmt->expr))
        {
            if(ResolvedSymbol::get(callExpr->node) == NULL) return NULL;
            Declaration* decl = cast<Declaration>(ResolvedSymbol::get(callExpr->node));
            return decl;
        }
        else if(MemberExpr* memberExpr = cast<MemberExpr>(exprStmt->expr))
        {
            if(ResolvedSymbol::get(memberExpr) == NULL) return NULL;
            Declaration* decl = cast<Declaration>(ResolvedSymbol::get(memberExpr));
            return decl;
        }
        else if(BinaryExpr* binaryExpr = cast<BinaryExpr>(exprStmt->expr))
        {
            if(ResolvedSymbol::get(binaryExpr) == NULL) return NULL;
            Declaration* decl = cast<Declaration>(ResolvedSymbol::get(binaryExpr));
            return decl;
        }
    }
    // variable declaration
    else if(declStmt != NULL)
    {
        VariableDecl*  varDecl = cast<VariableDecl>(declStmt->declaration);
        TypeSpecifier* type    = varDecl->type;
        if(ResolvedType::get(type) == NULL) return NULL;
        Declaration*   decl    = ResolvedType::get(type)->getAsDecl();
        return decl;
    }
    // data member
    else if(VariableDecl* varDecl = cast<VariableDecl>(decl))
    {
        TypeSpecifier* type    = varDecl->type;
        if(ResolvedType::get(type) == NULL) return NULL;
        Declaration* decl = ResolvedType::get(type)->getAsDecl();
        return decl;
    }
    else if(decl != NULL)
    {
        return decl;
    }
    UNREACHABLE_CODE();
    return NULL;
}

/**
 * @brief Get the resolution id of the function declaration
 * @param node The ExpressionStmt node of the function call
 * @return @p __no_ResolvedSymbol__ if the function call is not resolved
 *         @p _no_StaticTestAnnotation_ if the declaration has no static_test annotation
 *         @p SomeId The declaration id if the function is resolved
 */
static std::wstring getDeclId(ASTNode& node)
{
    Declaration* decl = getDecl(node);
    if(decl == NULL)
    {
        return L"__no_ResolvedSymbol__";
    }

    Annotation* declAnno = decl->findAnnotation(L"static_test");
    if(declAnno == NULL)
    {
        return L"__no_annotation__";
    }

    auto*               declAnnoValue = declAnno->attribute_list.begin()->second;
    const std::wstring& declId        = cast<StringLiteral>(declAnnoValue)->value;
    return declId;
}

static ASTNode* getType(ASTNode& node)
{
    ASTNode*           stmtNode = &node;
    ExpressionStmt*    exprStmt = cast<ExpressionStmt>(stmtNode);
    // function call
    if(exprStmt != NULL)
    {
        if(ResolvedType::get(exprStmt->expr) == NULL) return NULL;

        Type* type = exprStmt->expr->getCanonicalType();

        if(Declaration* decl = type->getAsDecl())
        {
            return decl->name;
        }
        else
        {
            return type;
        }
    }
    UNREACHABLE_CODE();
    return NULL;
}

static std::wstring getTypeId(Type* type)
{
    if(type == NULL)
        return L"__no_ResolvedType__";

    if(type->isDeclType())
        return type->getAsDecl()->Declaration::toString();
    else
        return type->toString();

    UNREACHABLE_CODE();
    return L"__no_ResolvedType__";
}

struct CodePoint
{
    CodePoint(ASTNode* node)
    {
        Source* sourceNode = node->getOwner<Source>();

        if(sourceNode)
        {
            filename = s_to_ws(sourceNode->filename);
            stage::SourceInfoContext* source_info = stage::SourceInfoContext::get(node);
            line = source_info->line;
            column = source_info->column;
        }
        else
        {
            filename.clear();
            line = 0;
            column = 0;
        }
    }

    std::wstring filename;
    uint32 line;
    uint32 column;
} ;

/**
 * @brief Check function resolution
 * @param node function call stmt to be resolved
 * @return @p true if all match, @p false is not
 *
 * There are 3 OK:
 *   1. No annotation
 *   2. Can not be resolved && no ResolvedSymbol
 *   3. Can be resolved && has match ResolvedSymbol
 *
 * and 2 Fail:
 *   1. Fales positive: should not resolved, but resolved.
 *   2. Resolved, but not match
 */
bool StaticTestVerificationStageVisitor::checkResolutionSymbol(zillians::language::tree::ASTNode& node)
{
    std::wstring useId = getUseId(node, L"expect_resolution");

    // There are three OK:
    // 1. no annotation
    // 2. Can not be resolved && no ResolvedSymbol
    // 3. Can be resolved && has match ResolvedSymbol
    if(useId == L"__no_annotation__")
    {
        return true;
    }

    std::wstring callOrDecl = isa<ExpressionStmt>(&node) ? L"Function call" : L"Variable declaration";
    std::wstring declId = getDeclId(node);

    if(useId == L"" && declId == L"__no_ResolvedSymbol__")
    {
        std::cout << "Resolution OK" << std::endl;
        return true;
    }
    else if(useId == declId)
    {
        std::cout << "Resolution OK" << std::endl;
        return true;
    }

    // And three FAIL:
    // 1. Fales positive: should not resolved, but resolved.
    // 2. Should be resolved, but not.
    // 3. Resolved, but not match
    else if(useId == L"" && declId != L"__no_ResolvedSymbol__")
    {
        CodePoint usePoint(&node);
        CodePoint declPoint(getDecl(node));
        std::wcerr <<  usePoint.filename << L":" <<  usePoint.line << L": Error: " << callOrDecl << " `" << ASTNodeHelper::getNodeName(&node, true) << L"' should not be resolved, but resolved to:" << std::endl
                   << declPoint.filename << L":" << declPoint.line << L": " << ASTNodeHelper::getNodeName(getDecl(node), true) << std::endl;
        return false;
    }
    else if(useId != L"" && declId == L"__no_ResolvedSymbol__")
    {
        CodePoint usePoint(&node);
        std::wcerr <<  usePoint.filename << L":" <<  usePoint.line << L": Error: " << callOrDecl << " `" << ASTNodeHelper::getNodeName(&node, true) << L"' should be resolved, but not" << std::endl;
        return false;
    }
    else if(useId != declId)
    {
        CodePoint usePoint(&node);
        CodePoint declPoint(getDecl(node));
        std::wcerr <<  usePoint.filename << L":" <<  usePoint.line << L": Error: " << callOrDecl << " `" << ASTNodeHelper::getNodeName(&node, true) << L"' resolved to wrong declaration:" << std::endl
                   << declPoint.filename << L":" << declPoint.line << L": " << ASTNodeHelper::getNodeName(getDecl(node), true) << std::endl;
        return false;
    }

    UNREACHABLE_CODE();
}

bool StaticTestVerificationStageVisitor::checkResolutionType(zillians::language::tree::ASTNode& node)
{
    std::wstring useId = getUseId(node, L"expect_type");

    // There are three OK:
    // 1. no annotation
    // 2. Can not be resolved && no ResolvedSymbol
    // 3. Can be resolved && has match ResolvedSymbol
    if(useId == L"__no_annotation__")
    {
        return true;
    }

    BOOST_ASSERT(isa<ExpressionStmt>(&node) && "only ExpressionStmt is supported currently");

    ExpressionStmt* expr_stmt = cast<ExpressionStmt>(&node);
    std::wstring stmtInfo = L"Expression";
    Type* resolved_type = expr_stmt->expr->getCanonicalType();
    std::wstring typeId = getTypeId(resolved_type);

    if(useId == L"" && typeId == L"__no_ResolvedType__")
    {
        //std::cout << "Type OK" << std::endl;
        return true;
    }
    else if(useId == typeId)
    {
        //std::cout << "Type OK" << std::endl;
        return true;
    }

    // And three FAIL:
    // 1. Fales positive: should not resolved, but resolved.
    // 2. Should be resolved, but not.
    // 3. Resolved, but not match
    else if(useId == L"" && typeId != L"__no_ResolvedType__")
    {
        CodePoint usePoint(&node);
        CodePoint declPoint(getType(node));
        std::wcerr <<  usePoint.filename << L":" <<  usePoint.line << L": Error: " << stmtInfo << " `" << ASTNodeHelper::getNodeName(&node, true) << L"' should not be resolved, but resolved to:" << std::endl
                   << declPoint.filename << L":" << declPoint.line << L": " << ASTNodeHelper::getNodeName(getType(node), true) << std::endl;
        return false;
    }
    else if(useId != L"" && typeId == L"__no_ResolvedType__")
    {
        CodePoint usePoint(&node);
        std::wcerr <<  usePoint.filename << L":" <<  usePoint.line << L": Error: " << stmtInfo << " `" << ASTNodeHelper::getNodeName(&node, true) << L"' should be resolved, but not" << std::endl;
        return false;
    }
    else if(useId != typeId)
    {
        CodePoint usePoint(&node);
        CodePoint declPoint(getType(node));
        std::wcerr <<  usePoint.filename << L":" <<  usePoint.line << L": Error: " << stmtInfo << " `" << ASTNodeHelper::getNodeName(&node, true) << L"' resolved to wrong declaration:" << std::endl
                   << declPoint.filename << L":" << declPoint.line << L": " << ASTNodeHelper::getNodeName(getType(node), true) << std::endl;
        return false;
    }

    UNREACHABLE_CODE();
}

bool StaticTestVerificationStageVisitor::checkInferredType(zillians::language::tree::ASTNode& node)
{
    const auto*const annotatable = cast<Annotatable>(&node);

    ASTNode* anno_value = annotatable != nullptr ? annotatable->findAnnotation({L"static_test", L"infer_type"}) : nullptr;

    if(anno_value == NULL)
        return true;

    TypeSpecifier* type_spec = NULL;
    if(DeclarativeStmt* decl_stmt = cast<DeclarativeStmt>(&node))
    {
        if(VariableDecl* var_decl = cast<VariableDecl>(decl_stmt->declaration))
            type_spec = var_decl->type;
    }
    else if(FunctionDecl* func_decl = cast<FunctionDecl>(&node))
    {
        type_spec = func_decl->type;
    }
    else
    {
        return true;
    }

    std::wstring ann_type_str = cast<StringLiteral>(anno_value)->toString();
    ann_type_str = ann_type_str.substr(1, ann_type_str.size() - 2);
    std::wstring type_spec_str = type_spec == NULL ? L"(NULL_TYPE_SPECIFIER)" : type_spec->toString();
    bool result = ann_type_str == type_spec_str;

    if(!result)
        LOG_MESSAGE(WRONG_INFER_TYPE, &node, _expect(ann_type_str), _actual(type_spec_str));

    return result;
}

bool StaticTestVerificationStageVisitor::checkSpecializationOf(zillians::language::tree::ASTNode& node)
{
    const auto*const annotatable = cast<Annotatable>(&node);

    ASTNode* anno_value = annotatable != nullptr ? annotatable->findAnnotation({L"static_test", L"specialization_of"}) : nullptr;

    if(anno_value == NULL)
    {
        return true;
    }

    ASTNode* specialization_of = SpecializationOf::get(&node);

    BOOST_ASSERT(isa<StringLiteral>(anno_value) && "unexpected static test annotation");
    BOOST_ASSERT((specialization_of == NULL || isa<Declaration>(specialization_of)) && "specialization must be declaration");

    const std::wstring& expect = cast<StringLiteral>(anno_value)->value;
    const std::wstring& actual = specialization_of == nullptr ? std::wstring() : cast<Declaration>(specialization_of)->name->toString();

    const bool is_same = actual == expect;

    if(!is_same)
    {
        LOG_MESSAGE(WRONG_SPECIALIZATION_OF, &node, _expect(expect), _actual(actual));
    }

    return is_same;
}

bool StaticTestVerificationStageVisitor::checkConstantFolding(zillians::language::tree::ASTNode& node)
{
    const auto*const annotatable = cast<Annotatable>(&node);

    ASTNode* anno_value = annotatable != nullptr ? annotatable->findAnnotation({L"static_test", L"expect_constant"}) : nullptr;
    if(anno_value == NULL)
        return true;
    
    NumericLiteral * expected_literal = cast<NumericLiteral>( anno_value );
    BOOST_ASSERT( expected_literal && "static_test for constant folding should has a expected literal" );

    NumericLiteral * actual_literal = NULL;
    bool result = false;
    if( ExpressionStmt * statement = cast<ExpressionStmt>(&node) )
    {
        actual_literal = cast<NumericLiteral>(statement->expr);
        result = (expected_literal->toString() == actual_literal->toString());
    }

    if(!result)
        LOG_MESSAGE(WRONG_EXPECTED_VALUE, &node, _expect(expected_literal->toString()), _actual(actual_literal->toString()));

    return result;
}

} } } } // namespace zillians::language::tree::visitor
