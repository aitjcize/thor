/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>
#include <vector>
#include <map>

#include <boost/range/adaptors.hpp>

#include "language/GlobalConstant.h"
#include "language/logging/LoggerWrapper.h"
#include "language/logging/StringTable.h"
#include "language/stage/transformer/detail/OperatorOverloadingConfig.h"
#include "language/stage/verifier/detail/ControlFlowVerifier.h"
#include "language/stage/verifier/visitor/SemanticVerificationStageVisitor0.h"
#include "language/tree/ASTNodeFactory.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

namespace {

void checkAtomicBlock(Block& block)
{
    // TBD: async block dont need check?
    if(!isa<AtomicBlock>(&block) && !isa<LockBlock>(&block))
        return;

    ASTNodeHelper::foreachApply<ASTNode>(block, [&block](ASTNode& node) {
        if(isa<CallExpr>(&node)) {
            LOG_MESSAGE(FUNCTION_CALL_IN_ATOMIC_BLOCK, &node);
            LOG_MESSAGE(WITHIN_ATOMIC_BLOCK, &block);
            return;
        }
    });
}

std::pair<Expression*, CallExpr*> get_left_and_call_expr(ExpressionStmt& node)
{
    if(CallExpr* call_expr = cast<CallExpr>(node.expr)) {
        return {nullptr, call_expr};
    } else if(BinaryExpr* binary_expr = cast<BinaryExpr>(node.expr)) {
        // TBD allow +=, -=, *=, /=, ... ?
        if(binary_expr->opcode != BinaryExpr::OpCode::ASSIGN) {
            LOG_MESSAGE(FLOW_BINARYEXPR_NOT_ASSIGN, &node);
            return {nullptr, nullptr};
        }
        if(CallExpr* call_expr = cast<CallExpr>(binary_expr->right)) {
            return {binary_expr->left, call_expr};
        }
    }
    return {nullptr, nullptr};
}

bool is_simple_member_expr(Expression& node)
{
    if(isa<IdExpr>(&node)) {
        return true;
    }

    MemberExpr* member_expr = cast<MemberExpr>(&node);
    if(member_expr == nullptr) {
        return false;
    } else {
        return is_simple_member_expr(*member_expr->node);
    }
}

bool is_call_contain_assignment(CallExpr& node)
{
    bool result = false;
    ASTNodeHelper::foreachApply<BinaryExpr>(node, [&result](BinaryExpr& n) {
        if(n.opcode >= BinaryExpr::OpCode::ASSIGN && n.opcode <= BinaryExpr::OpCode::XOR_ASSIGN) {
            result = true;
        }
    });
    return result;
}

/*
 * Only allowed three form:
 * 1. f();
 * 2. a = f();
 * 3. x.y = g();
 *
 * constraints:
 * - In form 3, right hand side can NOT has assignment operator
 *   like a = f(b = c)
 *   of   a = lambda(x, y){ x = y; }();
 */
void verifyFlowStatement(Statement& node)
{
    ExpressionStmt* expr_stmt = cast<ExpressionStmt>(&node);

    // can only be expression statement
    if(expr_stmt == nullptr) {
        LOG_MESSAGE(FLOW_STATEMENT_IS_NOT_EXPRESSION, &node);
        return;
    }

    auto left_call_pair = get_left_and_call_expr(*expr_stmt);
    // check left hand side can only be primary or member
    if(left_call_pair.first != nullptr) {
        if(!is_simple_member_expr(*left_call_pair.first)) {
            LOG_MESSAGE(FLOW_LHS_PRIM_MAMBER, &node);
        }
    }

    // check left hand side can only be primary or member
    if(left_call_pair.second != nullptr) {
        if(is_call_contain_assignment(*left_call_pair.second)) {
            LOG_MESSAGE(FLOW_ASSIGN_IN_CALL, &node);
            return;
        }
    } else {
        LOG_MESSAGE(FLOW_STATEMENT_HAS_NO_CALL, &node);
        return;
    }
}

void checkFlowBlock(Block& block)
{
    if(!isa<FlowBlock>(&block))
        return;

    if(block.objects.empty())
    {
        LOG_MESSAGE(EMPTY_BLOCK, &block, _type(L"flow"));
        return;
    }

    for(Statement* stmt : block.objects) {
        verifyFlowStatement(*stmt);
    }
}

void checkAsyncBlock(Block& block)
{
    if(!isa<AsyncBlock>(&block))
        return;

    if(block.objects.empty())
    {
        LOG_MESSAGE(EMPTY_BLOCK, &block, _type(L"async"));
    }
}

} // anonymous namespace 

SemanticVerificationStageVisitor0::SemanticVerificationStageVisitor0() : errorCount_(0), warnCount_(0)
{
    REGISTER_ALL_VISITABLE_ASTNODE(verifyInvoker);

    // insert alternative keywords
    alternative_keywords.insert(L"double");
    alternative_keywords.insert(L"long");
    alternative_keywords.insert(L"char");
    alternative_keywords.insert(L"short");
    alternative_keywords.insert(L"int");

    alternative_keywords.insert(L"unsigned");
    alternative_keywords.insert(L"signed");
    alternative_keywords.insert(L"template");
    alternative_keywords.insert(L"volatile");
    alternative_keywords.insert(L"struct");
    alternative_keywords.insert(L"union");
    alternative_keywords.insert(L"sizeof");
    alternative_keywords.insert(L"operator");

    alternative_keywords.insert(L"and");
    alternative_keywords.insert(L"and_eq");
    alternative_keywords.insert(L"not");
    alternative_keywords.insert(L"not_eq");
    alternative_keywords.insert(L"xor");
    alternative_keywords.insert(L"xor_eq");
    alternative_keywords.insert(L"bitand");
    alternative_keywords.insert(L"bitor");
    alternative_keywords.insert(L"compl");

    alternative_keywords.insert(L"const_cast");
    alternative_keywords.insert(L"dynamic_cast");
    alternative_keywords.insert(L"static_cast");
    alternative_keywords.insert(L"reinterpret_cast");
}

void SemanticVerificationStageVisitor0::verify(ASTNode& node)
{
    revisit(node);
}

void SemanticVerificationStageVisitor0::verify(Source& node)
{
    // only verify non-imported AST
    if(!node.is_imported)
    {
        revisit(node);
    }
}

void SemanticVerificationStageVisitor0::verify(Import& node)
{
    verifyImportedPackageIsValid(node);

    revisit(node);
}

void SemanticVerificationStageVisitor0::verify(Package& node)
{
	verifyPackageConfictWithAlternativeKeywords(node);

    // PACKAGE_NAME_COLLIDE_PARENT
    if(!node.isRoot() && node.id->toString() == cast<Package>(node.parent)->id->toString())
    {
        LOG_MESSAGE(PACKAGE_NAME_COLLIDE_PARENT, &node, _package_id(node.id->toString()));
    }

    nameShadowVerifier.enter(node);

    revisit(node);

    nameShadowVerifier.leave(node);
}

void SemanticVerificationStageVisitor0::verify(FunctionSpecifier& node)
{
    verifyFunctionTypeParamsValid(node);
    verifyFunctionTypeReturnValid(node);

    revisit(node);
}

void SemanticVerificationStageVisitor0::verify(Annotation& node)
{
    verifyHasValidAnnotation(node);

    revisit(node);
}

void SemanticVerificationStageVisitor0::verify(UnaryExpr& node)
{
    verifyValidNewExpression(node);

    revisit(node);
}

void SemanticVerificationStageVisitor0::verify(BranchStmt& node)
{
    verifyBreakOrContinueTarget(node);

    revisit(node);
}

void SemanticVerificationStageVisitor0::verify(ClassDecl& node)
{
    verifyDefaultTemplateArgumentOrder(node.name->getTemplateId());

    nameShadowVerifier.enter(node);

    for( auto * method : node.member_functions )
    {
        verifyValidOperatorOverloading( *method );
    }

    revisit(node);

    nameShadowVerifier.leave(node);
}

void SemanticVerificationStageVisitor0::verify(VariableDecl& node)
{
    verifyVariableHasInit(node);
    verifyVariableHasValidType(node);

    nameShadowVerifier.enter(node);

    revisit(node);

    nameShadowVerifier.leave(node);
}

void SemanticVerificationStageVisitor0::verify(SimpleIdentifier& node)
{
	verifyIsConfictWithAlternativeKeywords(node, node.name);
}

void SemanticVerificationStageVisitor0::verify(FunctionDecl& node)
{
    verifyDefaultTemplateArgumentOrder(node.name->getTemplateId());
    verifyTemplateMemberFunction(node);
    verifyFunctionLinkage(node);
    verifyParameter(node);
    verifyMissingReturnType(node);
    verifyNonMemberCtorDtor(node);
    verifyLambdaSuperCall(node);
    verifyValidCtorOrDtor(node);
    verifyCtorDtorMustNotBeStatic(node);
    verifyCtorMustNotBeVirtual(node);
    verifyDtorMustAcceptNoParameter(node);
    verifyIsValidEntryFunction(node);

    DeadCodeVerifier::verifyFuncDeclImpl(node);

    nameShadowVerifier.enter(node);

    revisit(node);

    nameShadowVerifier.leave(node);
}

void SemanticVerificationStageVisitor0::verify(Block& node)
{
    checkAtomicBlock(node);
    checkFlowBlock(node);
    checkAsyncBlock(node);

    nameShadowVerifier.enter(node);

    revisit(node);

    nameShadowVerifier.leave(node);
}

void SemanticVerificationStageVisitor0::verify(TypenameDecl& node)
{
    nameShadowVerifier.enter(node);

    revisit(node);

    nameShadowVerifier.leave(node);
}

void SemanticVerificationStageVisitor0::verify(EnumDecl& node)
{
    nameShadowVerifier.enter(node);

    revisit(node);

    nameShadowVerifier.leave(node);
}

void SemanticVerificationStageVisitor0::applyCleanup()
{
    for(auto& cleaner: cleanup)
        cleaner();
    cleanup.clear();
}

void SemanticVerificationStageVisitor0::verifyImportedPackageIsValid(Import& node)
{
    const auto package_id = node.ns->toString();
    Package*const package = node.getOwner<Tangle>()->findPackage(package_id);

    if(package == NULL)
    {
        LOG_MESSAGE(PACKAGE_NOT_EXIST, &node, _package_id(package_id));
    }
    else if(package == node.getOwner<Package>())
    {
        LOG_MESSAGE(SOURCE_IMPORT_SELF, &node, _package_id(package_id));
    }
}

void SemanticVerificationStageVisitor0::verifyHasValidAnnotation(Annotation& node)
{
    // only check name after '@'
    if(isa<Annotations>(node.parent))
    {
        BOOST_ASSERT(node.name != NULL && "annotation without name!");

        // TODO global_ctor annotation is deprecated
//        if(node.name->toString() == global_constant::GLOBAL_CTOR)
//        {
//            LOG_MESSAGE(USE_OF_RESERVED_ANNOTATION, &node, _name(node.name->toString()));
//        }
    }
}

void SemanticVerificationStageVisitor0::verifyVariableHasInit(VariableDecl& node)
{
    if(!isa<EnumDecl>(node.parent))
    {
        if(node.isGlobal() || node.is_static)
        {
            if(node.initializer == NULL)
            {
                LOG_MESSAGE(MISSING_VAR_INIT, &node);
            }
        }
    }
}

void SemanticVerificationStageVisitor0::verifyTypeSpecifierNotVoid(TypeSpecifier& node)
{
    const auto*const primitive = cast<PrimitiveSpecifier>(&node);

    if (primitive != nullptr && primitive->getKind() == PrimitiveKind::VOID_TYPE)
    {
        LOG_MESSAGE(INVALID_USE_OF_TYPE_VOID, &node);
    }
}

void SemanticVerificationStageVisitor0::verifyVariableHasValidType(VariableDecl& node)
{
    if(node.type == NULL)
    {
        // global/member variable and function parameter must have type
        if(node.is_member || node.isGlobal() || isa<FunctionDecl>(node.parent))
        {
            LOG_MESSAGE(VARIABLE_MISSING_TYPE, &node);
        }
    }
    else
    {
        verifyTypeSpecifierNotVoid(*node.type);
    }
}

void SemanticVerificationStageVisitor0::verifyIsValidEntryFunction(FunctionDecl& node)
{
    if (!node.hasAnnotation(L"entry"))
        return;

    const auto& is_task        = node.is_task;
    const auto& no_parameter   = node.parameters.empty();
    const auto& is_template    = node.isTemplated();
    const auto& is_valid_scope = !node.is_member ||
                                 (node.is_static && node.visibility == Declaration::VisibilitySpecifier::PUBLIC && isa<ClassDecl>(node.parent) && !cast<ClassDecl>(node.parent)->is_interface);

    if (!is_task)
        LOG_MESSAGE(ENTRY_MUST_BE_A_TASK, &node);

    if (!no_parameter)
        LOG_MESSAGE(ENTRY_MUST_ACCEPT_NO_PARAMETER, &node);

    if(!is_valid_scope || is_template)
        LOG_MESSAGE(ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER, &node);
}

void SemanticVerificationStageVisitor0::verifyFunctionTypeParamsValid(FunctionSpecifier& node)
{
    for(auto* type : node.getParameterTypes())
    {
        BOOST_ASSERT(type && "null pointer exception");

        verifyTypeSpecifierNotVoid(*type);
    }
}

void SemanticVerificationStageVisitor0::verifyFunctionTypeReturnValid(FunctionSpecifier& node)
{
    auto*const return_type = node.getReturnType();

    if(return_type == NULL)
    {
        LOG_MESSAGE(FUNCTION_TYPE_WITHOUT_RETURN_TYPE, &node);
    }
}

void SemanticVerificationStageVisitor0::verifyValidNewExpression(UnaryExpr& node)
{
    if( node.opcode != UnaryExpr::OpCode::NEW )
        return;
    
    std::function<bool(ASTNode&)> f = [&](ASTNode& node) -> bool {
        if(isa<MemberExpr>(&node))
        {
            return f(*cast<MemberExpr>(&node)->node);
        }
        else if(isa<IdExpr>(&node))
        {
            return true;
        }
        else if(isa<Literal>(&node))
        {
            return true;
        }
        else
        {
            return false;
        }
    };

    if( f(*node.node) )
    {
        return; 
    }
    else if( isa<CallExpr>(node.node) )
    {
        CallExpr* call_expr = cast<CallExpr>(node.node);

        if( f(*call_expr->node) )
        {
            return;
        }
    }

    // invalid expression
    switch( node.opcode )
    {
    case UnaryExpr::OpCode::NEW:
        LOG_MESSAGE(INVALID_NEW, &node);
        break;
    default:
        UNREACHABLE_CODE();
        break;
    }
}

void SemanticVerificationStageVisitor0::verifyIsConfictWithAlternativeKeywords(ASTNode& node, std::wstring name)
{
	if (alternative_keywords.count(name))
	{
		LOG_MESSAGE(ALTERNATIVE_KEYWORD_CONFLICT, &node, _name(name));
	}
}

void SemanticVerificationStageVisitor0::verifyPackageConfictWithAlternativeKeywords(Package& node)
{
	if (alternative_keywords.count(node.id->name))
	{
		LOG_MESSAGE(PACKAGE_NAME_CONFLICT_ALTERNATIVE_KEYWORD, &node, _name(node.id->name));
	}
}

void SemanticVerificationStageVisitor0::verifyValidOperatorOverloading(FunctionDecl& node)
{
    SimpleIdentifier * identifier = node.name->getSimpleId();

    BOOST_ASSERT( identifier != nullptr );

    auto method_name = identifier->name;

    using namespace OperatorOverloading;
    
    // template function is not allowed for unary operator
    if( Unary::getReservedNames().count(method_name) != 0 )
    {
        if( node.parameters.size() != 0 )
        {
            LOG_MESSAGE( OVERLOAD_FAILED, &node );
        }
        else if( isa<TemplatedIdentifier>(node.name) )
        {
            LOG_MESSAGE( OVERLOAD_FAILED, &node );
        }
    }
    else if( Binary::getReservedNames().count(method_name) != 0 )
    {
        switch( node.parameters.size() )
        {
        case 0:
            LOG_MESSAGE( OVERLOAD_FAILED, &node );
            break;
        case 1:
            // for binary operators, template functions should not have greater than one type arg,
            // if have exact one, should be same as function parameter's
            if( TemplatedIdentifier * identifier = cast<TemplatedIdentifier>(node.name) )
            {
                // can't deduct rest of type args
                if( identifier->templated_type_list.size() != 1 ) 
                {
                    LOG_MESSAGE( OVERLOAD_FAILED, &node );
                    break;
                }
                
                // can't deduct the only one type arg by parameter                    
                if( !isa<NamedSpecifier>(node.parameters.front()->type) )
                {
                    LOG_MESSAGE( OVERLOAD_FAILED, &node );
                    break; 
                }
            }
            break;
        // greater than one parameter
        default:
            LOG_MESSAGE( OVERLOAD_FAILED, &node );
            break; 
        }
    }
}

void SemanticVerificationStageVisitor0::verifyLambdaSuperCall(FunctionDecl& node)
{
    if(!node.is_lambda) return;

    ASTNodeHelper::foreachApply<ObjectLiteral>(node, [&](ObjectLiteral& literal) {
        if(literal.isSuperLiteral())
        {
            auto attach_node = literal.getOwner<ExpressionStmt>();
            LOG_MESSAGE( NO_SUPER_IN_LAMBDA, attach_node );
        }
    });
}

void SemanticVerificationStageVisitor0::verifyNonMemberCtorDtor(FunctionDecl& node)
{
    // non-member function can't name 'new'
    if(!node.is_member)
    {
        if(node.isConstructor())
        {
            LOG_MESSAGE(NON_MEMBER_NEW_FUNCTION, &node);
        }
        else if(node.isDestructor())
        {
            LOG_MESSAGE(NON_MEMBER_DELETE_FUNCTION, &node);
        }
    }
}

void SemanticVerificationStageVisitor0::verifyMissingReturnType(FunctionDecl& node)
{
    // missing return type
    if(node.type == NULL &&
       (isa<TemplatedIdentifier>(node.name) || (node.is_member && isa<TemplatedIdentifier>(cast<ClassDecl>(node.parent)->name))))
    {
        LOG_MESSAGE(FUNCTION_MISSING_TYPE, &node);
    }

}

void SemanticVerificationStageVisitor0::verifyParameter(FunctionDecl& node)
{
    std::wstring name = node.name->toString();
    bool has_visited_optional_param = false;
    size_t n = 0;
    for(auto* param : node.parameters)
    {
        // MISSING_PARAM_INIT
        if(cast<VariableDecl>(param)->initializer)
            has_visited_optional_param = true;
        else if(has_visited_optional_param)
        {
            LOG_MESSAGE(MISSING_PARAM_INIT, &node, _param_index(std::to_wstring(n + 1)), _func_id(name));
        }

        n++;
    }

    // EXCEED_PARAM_LIMIT
    if(n > ConfigurationContext::max_param_count)
    {
        LOG_MESSAGE(EXCEED_PARAM_LIMIT, &node);
    }
}

void SemanticVerificationStageVisitor0::verifyTemplateMemberFunction(FunctionDecl& node)
{
    // exit when function is not a method nor template
    if(!(node.is_member && isa<TemplatedIdentifier>(node.name)))
        return;

    // virtual template is not allowed
    if(node.is_virtual)
        LOG_MESSAGE(TEMPLATES_CANNOT_BE_VIRTUAL, &node);
}

void SemanticVerificationStageVisitor0::verifyFunctionLinkage(FunctionDecl& node)
{
    ClassDecl*const parent_cls = cast<ClassDecl>(node.parent);

    bool need_impl = false;

#define IS_NATIVE(node)                     \
    (                                       \
        (node)->hasAnnotation(L"native") || \
        (node)->hasAnnotation(L"system")    \
    )

         if (IS_NATIVE(&node     )   ) need_impl = false;
    else if (node.block != nullptr   ) need_impl = true;
    else if (parent_cls == nullptr   ) need_impl = true;
    else if (IS_NATIVE(parent_cls)   ) need_impl = false;
    else if (parent_cls->is_interface) need_impl = false;
    else                               need_impl = true;

#undef IS_NATIVE

    if (need_impl)
    {
        if (node.block == nullptr)
        {
            const std::wstring& name = node.name->toString();

            LOG_MESSAGE(INCOMPLETE_FUNC, &node, _func_id(name));
        }
    }
    else
    {
        if (node.block != nullptr)
        {
            const std::wstring& name = node.name->toString();

            LOG_MESSAGE(IMPLEMENT_FUNC_SHOULD_BE_EMPTY, &node, _func_id(name));
        }
    }
}

void SemanticVerificationStageVisitor0::verifyBreakOrContinueTarget(BranchStmt& node)
{
    if (node.opcode != BranchStmt::OpCode::BREAK && node.opcode != BranchStmt::OpCode::CONTINUE)
        return;

    for (const ASTNode* parent = node.parent; ; parent = parent->parent)
    {
        BOOST_ASSERT(parent && "null pointer exception");

        if (isa<IterativeStmt>(parent))
        {
            break;
        }
        else if (isa<FunctionDecl>(parent))
        {
            if (node.opcode == BranchStmt::OpCode::BREAK)
            {
                LOG_MESSAGE(MISSING_BREAK_TARGET, &node);
            }
            else
            {
                LOG_MESSAGE(MISSING_CONTINUE_TARGET, &node);
            }

            break;
        }
    }
}

void SemanticVerificationStageVisitor0::verifyValidCtorOrDtor(FunctionDecl& node)
{
    if (!node.isConstructor() && !node.isDestructor())
        return;

    if (node.is_task)
    {
        const auto*const name = node.isConstructor() ? L"constructor" : L"destructor";

        LOG_MESSAGE(SPECIAL_FUNCTION_MUST_NOT_BE_TASK, &node, _name(name));
    }

    if(const auto* class_decl = cast<ClassDecl>(node.parent))
    {
        if(class_decl->is_interface)
            LOG_MESSAGE(INVALID_FUNCTION_IN_INTERFACE, &node);
    }
}

void SemanticVerificationStageVisitor0::verifyDefaultTemplateArgumentOrder(const TemplatedIdentifier* identifier)
{
    if(identifier == nullptr)
        return;

    using boost::adaptors::indirected;
    using boost::adaptors::reversed;

    bool expect_default = true;
    bool found_default = false;
    bool found_specialized = false;
    for(TypenameDecl &type : identifier->templated_type_list | reversed | indirected)
    {
        if(type.specialized_type != nullptr)
        {
            found_specialized = true;
        }
        else
        {
            if(type.default_type != nullptr)
            {
                found_default = true;
            }
            if(expect_default && !type.default_type)
            {
                expect_default = false;
            }
            else if(!expect_default && type.default_type)
            {
                LOG_MESSAGE(INVALID_TEMPLATE_DEFAULT_ARGUMENT_ORDER, identifier->parent);
            }
        }
    }
    if(found_default && found_specialized)
    {
        LOG_MESSAGE(INVALID_TEMPLATE_DEFAULT_ARGUMENT_IN_SPECIALIZATION, identifier->parent);
    }
}

void SemanticVerificationStageVisitor0::verifyCtorDtorMustNotBeStatic(FunctionDecl& node)
{
    if (!node.isConstructor() && !node.isDestructor())
        return;

    if (node.is_static)
    {
        const auto*const name = node.isConstructor() ? L"constructor" : L"destructor";

        LOG_MESSAGE(CTOR_DTOR_MUST_NOT_BE_STATIC, &node, _name(name));
    }
}

void SemanticVerificationStageVisitor0::verifyCtorMustNotBeVirtual(FunctionDecl& node)
{
    if (node.isConstructor() && node.is_virtual)
    {
        const auto*const name =  L"constructor";

        LOG_MESSAGE(CTOR_MUST_NOT_BE_VIRTUAL, &node, _name(name));
    }
}

void SemanticVerificationStageVisitor0::verifyDtorMustAcceptNoParameter(FunctionDecl& node)
{
    if (node.isDestructor() && node.parameters.size() != 0)
    {
        const auto*const name =  L"destructor";

        LOG_MESSAGE(DTOR_MUST_ACCEPT_NO_PARAMETER, &node, _name(name));
    }
}

} } } } // namespace zillians::language::tree::visitor
