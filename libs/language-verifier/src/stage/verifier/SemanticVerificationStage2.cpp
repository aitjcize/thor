/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <memory>
#include <utility>

#include "language/context/ParserContext.h"
#include "language/logging/LoggerWrapper.h"
#include "language/stage/verifier/SemanticVerificationStage2.h"
#include "language/stage/verifier/visitor/SemanticVerificationStageVisitor2.h"

namespace zillians { namespace language { namespace stage {

SemanticVerificationStage2::SemanticVerificationStage2() :
        keep_going(false)
{ }

SemanticVerificationStage2::~SemanticVerificationStage2()
{ }

const char* SemanticVerificationStage2::name()
{
    return "Semantic Verification Stage 2";
}

std::pair<std::shared_ptr<po::options_description>, std::shared_ptr<po::options_description>> SemanticVerificationStage2::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options();

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    return std::make_pair(option_desc_public, option_desc_private);
}

bool SemanticVerificationStage2::parseOptions(po::variables_map& vm)
{
    // keep going to static test when testing s0
    if(vm.count("mode-semantic-verify-2") > 0)
    {
        keep_going = true;
    }

    UNUSED_ARGUMENT(vm);
    return true;
}

bool SemanticVerificationStage2::execute(bool& continue_execution)
{
    if(keep_going)
        continue_execution = true;

    if(!hasParserContext())
        return false;

    LoggerWrapper::instance()->resetCounter();

    ParserContext& parser_context = getParserContext();

    if(parser_context.tangle)
    {
        LoggerWrapper::instance()->resetCounter();

        visitor::SemanticVerificationStageVisitor2 semantic_verifier;
        semantic_verifier.visit(*parser_context.tangle);
        semantic_verifier.applyCleanup();

        return !(LoggerWrapper::instance()->hasError() || LoggerWrapper::instance()->hasFatal());
    }
    else
    {
        return false;
    }
}

} } }

