/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_BUFFER_MAPPEDVARIABLES_H_
#define ZILLIANS_FRAMEWORK_BUFFER_MAPPEDVARIABLES_H_

#include "framework/buffer/BufferBase.h"
#include "framework/processor/cuda/kernel/api/RuntimeApi.h"
#include "framework/processor/cuda/kernel/MappedVariable.h"
#include <boost/thread/tss.hpp>
#include <boost/type_traits/is_pointer.hpp>

namespace zillians { namespace framework { namespace buffer {

using namespace zillians::framework::processor::cuda;

struct MappedVariableContainer
{
	static boost::thread_specific_ptr<MappedVariable<RuntimeApi, uint8> > mapped_variable_uint8;
	static boost::thread_specific_ptr<MappedVariable<RuntimeApi, uint16> > mapped_variable_uint16;
	static boost::thread_specific_ptr<MappedVariable<RuntimeApi, uint32> > mapped_variable_uint32;
	static boost::thread_specific_ptr<MappedVariable<RuntimeApi, uint64> > mapped_variable_uint64;
	static boost::thread_specific_ptr<MappedVariable<RuntimeApi, uint8*> > mapped_variable_pointer;
};

template<bool IsPointer, int Size, typename T>
struct MappedVariablesImpl;

template<int Size, typename T>
struct MappedVariablesImpl<true, Size, T>
{
	static void ensure_valid()
	{
		if(!MappedVariableContainer::mapped_variable_pointer.get())
		{
			//printf("allocate mapped variable for type pointer type %s: ", typeid(T).name());
			MappedVariableContainer::mapped_variable_pointer.reset(new MappedVariable<RuntimeApi, uint8*>(GetRuntimeApi()));
		}
	}

	static T* ptr(const accessLocation::type location)
	{
		return (T*)MappedVariableContainer::mapped_variable_pointer->ptr(location);
	}

	static T get()
	{
		return (T)MappedVariableContainer::mapped_variable_pointer->get();
	}

	static void set(const T& v)
	{
		MappedVariableContainer::mapped_variable_pointer->set(v);
	}
};

template<typename T>
struct MappedVariablesImpl<false, 1, T>
{
	static void ensure_valid()
	{
		if(!MappedVariableContainer::mapped_variable_uint8.get())
		{
			//printf("allocate mapped variable for type uint8 type %s: ", typeid(T).name());
			MappedVariableContainer::mapped_variable_uint8.reset(new MappedVariable<RuntimeApi, uint8>(GetRuntimeApi()));
		}
	}

	static T* ptr(const accessLocation::type location)
	{
		return (T*)MappedVariableContainer::mapped_variable_uint8->ptr(location);
	}

	static T get()
	{
		return (T)MappedVariableContainer::mapped_variable_uint8->get();
	}

	static void set(const T& v)
	{
		MappedVariableContainer::mapped_variable_uint8->set(v);
	}
};

template<typename T>
struct MappedVariablesImpl<false, 2, T>
{
	static void ensure_valid()
	{
		if(!MappedVariableContainer::mapped_variable_uint16.get())
		{
			//printf("allocate mapped variable for type uint16 type %s: ", typeid(T).name());
			MappedVariableContainer::mapped_variable_uint16.reset(new MappedVariable<RuntimeApi, uint16>(GetRuntimeApi()));
		}
	}

	static T* ptr(const accessLocation::type location)
	{
		return (T*)MappedVariableContainer::mapped_variable_uint16->ptr(location);
	}

	static T get()
	{
		return (T)MappedVariableContainer::mapped_variable_uint16->get();
	}

	static void set(const T& v)
	{
		MappedVariableContainer::mapped_variable_uint16->set(v);
	}
};

template<typename T>
struct MappedVariablesImpl<false, 4, T>
{
	static void ensure_valid()
	{
		if(!MappedVariableContainer::mapped_variable_uint32.get())
		{
			//printf("allocate mapped variable for type uint32 type %s: ", typeid(T).name());
			MappedVariableContainer::mapped_variable_uint32.reset(new MappedVariable<RuntimeApi, uint32>(GetRuntimeApi()));
		}
	}

	static T* ptr(const accessLocation::type location)
	{
		return (T*)MappedVariableContainer::mapped_variable_uint32->ptr(location);
	}

	static T get()
	{
		return (T)MappedVariableContainer::mapped_variable_uint32->get();
	}

	static void set(const T& v)
	{
		MappedVariableContainer::mapped_variable_uint32->set(v);
	}
};

template<typename T>
struct MappedVariablesImpl<false, 8, T>
{
	static void ensure_valid()
	{
		if(!MappedVariableContainer::mapped_variable_uint64.get())
		{
			//printf("allocate mapped variable for type uint64 type %s: ", typeid(T).name());
			MappedVariableContainer::mapped_variable_uint64.reset(new MappedVariable<RuntimeApi, uint64>(GetRuntimeApi()));
		}
	}

	static T* ptr(const accessLocation::type location)
	{
		return (T*)MappedVariableContainer::mapped_variable_uint64->ptr(location);
	}

	static T get()
	{
		return (T)MappedVariableContainer::mapped_variable_uint64->get();
	}

	static void set(const T& v)
	{
		MappedVariableContainer::mapped_variable_uint64->set(v);
	}
};

template<typename T>
struct MappedVariables
{
	static void ensure_valid()
	{
		MappedVariablesImpl<boost::is_pointer<T>::value, sizeof(T), T>::ensure_valid();
	}

	static T* ptr(const accessLocation::type location)
	{
		return MappedVariablesImpl<boost::is_pointer<T>::value, sizeof(T), T>::ptr(location);
	}

	static T get()
	{
		return MappedVariablesImpl<boost::is_pointer<T>::value, sizeof(T), T>::get();
	}

	static void set(const T& v)
	{
		MappedVariablesImpl<boost::is_pointer<T>::value, sizeof(T), T>::set(v);
	}
};


} } }

#endif /* ZILLIANS_FRAMEWORK_BUFFER_MAPPEDVARIABLES_H_ */
