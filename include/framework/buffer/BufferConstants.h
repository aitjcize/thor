/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_BUFFERS_BUFFERCONSTANTS_H_
#define ZILLIANS_FRAMEWORK_BUFFERS_BUFFERCONSTANTS_H_

#include "core/Types.h"
#include <stddef.h>
#include <limits.h>
#ifndef __CUDACC__
#include <boost/assert.hpp>
#endif

namespace zillians { namespace framework { namespace buffer {

namespace buffer_constants {

static const std::size_t word_size       =     4; // the minimal addressable word size in bytes, which is 4 bytes in most architecture
static const std::size_t pointer_size    =     8; // we are targeting 64bit architecture, so the memory pointer is all 8 bytes
static const std::size_t cache_line_size =   128;
static const std::size_t max_threads     =     4;
static const std::size_t max_targets     =     8;
static const std::size_t max_sessions    = 65536; // TODO move these constant to service section
static const std::size_t max_invocations = 16384;

}

struct buffer_location_t
{
	enum type
	{
		host_unpinned       = 0,
#ifdef BUILD_WITH_RDMA
		host_ib_pinned,
#endif
#ifdef BUILD_WITH_CUDA
		host_ptx_pinned,
		device_ptx_pinned_0,
		device_ptx_pinned_1,
		device_ptx_pinned_2,
		device_ptx_pinned_3,
		device_ptx_pinned_4,
		device_ptx_pinned_5,
		device_ptx_pinned_6,
		device_ptx_pinned_7,
#endif
#ifdef BUILD_WITH_OPENCL
		host_ocl_pinned,
		device_ocl_pinned_0,
		device_ocl_pinned_1,
		device_ocl_pinned_2,
		device_ocl_pinned_3,
		device_ocl_pinned_4,
		device_ocl_pinned_5,
		device_ocl_pinned_6,
		device_ocl_pinned_7,
#endif
		unknown,
	};

	static const int number_of_locations =
			1
#ifdef BUILD_WITH_RDMA
			+1
#endif
#ifdef BUILD_WITH_CUDA
			+9
#endif
#ifdef BUILD_WITH_OPENCL
			+9
#endif
			;

	static inline const char* get_string(buffer_location_t::type t)
	{
		static const char* names[] = {
				"host_unpinned",
#ifdef BUILD_WITH_RDMA
				"host_ib_pinned",
#endif
#ifdef BUILD_WITH_CUDA
				"host_ptx_pinned",
				"device_ptx_pinned_0",
				"device_ptx_pinned_1",
				"device_ptx_pinned_2",
				"device_ptx_pinned_3",
				"device_ptx_pinned_4",
				"device_ptx_pinned_5",
				"device_ptx_pinned_6",
				"device_ptx_pinned_7",
#endif
#ifdef BUILD_WITH_OPENCL
				"host_ocl_pinned",
				"device_ocl_pinned_0",
				"device_ocl_pinned_1",
				"device_ocl_pinned_2",
				"device_ocl_pinned_3",
				"device_ocl_pinned_4",
				"device_ocl_pinned_5",
				"device_ocl_pinned_6",
				"device_ocl_pinned_7",
#endif
				"unknown"
		};
#ifndef __CUDACC__
		BOOST_ASSERT((int)t >= 0 && (int)t < number_of_locations && "invalid buffer location");
#endif
		return names[(int)t];
	}

	static inline bool is_accessible_from_host(buffer_location_t::type t)
	{
	    return t == host_unpinned
#ifdef BUILD_WITH_RDMA
	            || t == host_ib_pinned
#endif
#ifdef BUILD_WITH_CUDA
	            || t == host_ptx_pinned
#endif
#ifdef BUILD_WITH_OPENCL
	            || t == host_ocl_pinned
#endif
	            ;
	}

#ifdef BUILD_WITH_CUDA
	static inline bool is_accessible_from_cuda_device(buffer_location_t::type t)
	{
	    return (int)t >= (int)device_ptx_pinned_0 && (int)t <= (int)device_ptx_pinned_7;
	}
#endif

#ifdef BUILD_WITH_OPENCL
    static inline bool is_accessible_from_ocl_device(buffer_location_t::type t)
    {
        return (int)t >= (int)device_ocl_pinned_0 && (int)t <= (int)device_ocl_pinned_7;
    }
#endif

#ifdef BUILD_WITH_CUDA
	static inline uint32 get_ptx_device_id(buffer_location_t::type t)
	{
#ifndef __CUDACC__
		BOOST_ASSERT((int)t >= (int)device_ptx_pinned_0 && (int)t <= (int)device_ptx_pinned_7 && "invalid ptx buffer location");
#endif
		return (int)t - (int)device_ptx_pinned_0;
	}

	static inline buffer_location_t::type get_ptx_device_location(int device_id)
	{
#ifndef __CUDACC__
		BOOST_ASSERT(device_id >= 0 && device_id < 8 && "invalid device id, currently up to 8 devices");
#endif
		return (buffer_location_t::type)((int)device_ptx_pinned_0 + device_id);
	}
#endif

#ifdef BUILD_WITH_OPENCL
	static inline uint32 get_ocl_device_id(buffer_location_t::type t)
	{
#ifndef __CUDACC__
		BOOST_ASSERT((int)t >= (int)device_ocl_pinned_0 && (int)t <= (int)device_ocl_pinned_7 && "invalid opencl buffer location");
#endif
		return (int)t - (int)device_ocl_pinned_0;
	}

	static inline buffer_location_t::type get_ocl_device_location(int device_id)
	{
#ifndef __CUDACC__
		BOOST_ASSERT(device_id >= 0 && device_id < 8 && "invalid device id, currently up to 8 devices");
#endif
		return (buffer_location_t::type)((int)device_ocl_pinned_0 + device_id);
	}
#endif
};

} } }

#endif /* ZILLIANS_FRAMEWORK_BUFFERS_BUFFERCONSTANTS_H_ */
