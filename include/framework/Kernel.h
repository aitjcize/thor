/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_KERNEL_H_
#define ZILLIANS_FRAMEWORK_KERNEL_H_

#include <set>
#include <map>
#include <string>
#include <tuple>

#include <boost/bimap/bimap.hpp>
#include <boost/bimap/multiset_of.hpp>
#include <boost/bimap/set_of.hpp>

#include "utility/Filesystem.h"

#include "core/Prerequisite.h"

#include "language/Architecture.h"
#include "language/tree/ASTNode.h"

namespace zillians { namespace framework {

class Processor;

class KernelBase
{
public:
    explicit KernelBase(bool enable_server_function, bool enable_client_function);
    virtual ~KernelBase();

    struct configuration
    {
        enum type
        {
            processor_local_id,
            //next_invocation_buffer,
            processor_synchronized_invocation_buffer,
            processor_remote_invocation_buffer,
            processor_remote_synchronized_invocation_buffer,

            hw_architecture,
        };
    };

    using                GlobalOffsetsType          = std::map<UUID, std::tuple<int64 /* function */, int64 /* symbol */, int64 /* type */>>;
    static constexpr int GlobalOffsetsIndexFunction = 0;
    static constexpr int GlobalOffsetsIndexSymbol   = 1;
    static constexpr int GlobalOffsetsIndexType     = 2;

public:
    virtual void initialize();
    virtual void finalize();
    virtual bool isInitialized();

    virtual void launch(Processor* p) = 0;

public:
    virtual bool load(const boost::filesystem::path& main_ast_path,
                      const boost::filesystem::path& main_runtime_path,
                      const std::vector<boost::filesystem::path>& dep_paths
                      ) = 0;

    bool setKernel(const boost::filesystem::path& ast_file);

    int64                               queryFunctionId(const std::string& function_name);
    const language::tree::FunctionDecl* queryFunctionDecl(int64 function_id);
    const language::tree::ClassDecl*    queryClassDecl(int64 type_id);
    const std::wstring*                 queryStringLiteral(int64 symbol_id);

    void setVerbose(bool verbose);
    bool getVerbose();

private:
    bool computeGlobalOffsets(language::tree::Tangle& tangle);

    bool addKernelStrings(language::tree::Tangle& tangle);
    bool addKernelTypes(language::tree::Tangle& tangle);
    bool addKernelFunctions(language::tree::Tangle& tangle);

private:
    volatile bool isInit;

protected:
    language::tree::Tangle* mTangle;
    GlobalOffsetsType mGlobalOffsets;
    boost::bimaps::bimap<boost::bimaps::set_of<int64>, boost::bimaps::multiset_of<const language::tree::FunctionDecl*>> mFunctionIdMap;
    boost::bimaps::bimap<boost::bimaps::set_of<int64>, boost::bimaps::multiset_of<const language::tree::ClassDecl*>> mTypeIdMap;
    boost::bimaps::bimap<boost::bimaps::set_of<int64>, boost::bimaps::multiset_of<std::wstring>> mStringLiteralMap;

    bool mEnableServerFunction;
    bool mEnableClientFunction;

    bool mVerboseMode;
};

} }

#endif /* ZILLIANS_FRAMEWORK_KERNEL_H_ */
