/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_EXECUTIONMONITOR_H_
#define ZILLIANS_FRAMEWORK_EXECUTIONMONITOR_H_

#include "core/Prerequisite.h"
#include "core/ConditionVariable.h"
#include "framework/buffer/Common.h"
#include <tbb/atomic.h>
#include <ext/hash_map>

namespace zillians { namespace framework {

class ExecutionMonitor
{
public:
	ExecutionMonitor();
	virtual ~ExecutionMonitor();

public:
	bool isCompleted();
	bool isCompleted(uint32 processor_id);

public:
	void markCompleted(uint32 processor_id);
	void markIncompleted(uint32 processor_id);

public:
	void signalForCompletion(shared_ptr<ConditionVariable<int32>> v);

private:
	boost::detail::spinlock mLock;
    int mIncompletedCount;
	bool* mCompletionFlags;
    std::vector<shared_ptr<ConditionVariable<int32>>>* mSignalForCompletion;
};

} }

#endif /* ZILLIANS_FRAMEWORK_EXECUTIONMONITOR_H_ */
