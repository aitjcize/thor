/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_H_

#include <chrono>
#include <map>

#include <boost/tuple/tuple.hpp>
#include <boost/thread/thread.hpp>

#include "core/Prerequisite.h"
#include "core/Buffer.h"
#include "core/ContextHub.h"
#include "framework/Service.h"
#include "framework/ExecutionMonitor.h"
#include "framework/ExecutionITC.h"
#include "framework/buffer/BufferManager.h"
#include "framework/buffer/BufferNetwork.h"

#define ENABLE_PERFORMANCE_STATISTICS	1

using namespace zillians::framework::buffer;

namespace zillians { namespace framework {

/**
 * Processor is the game logic processor interface
 */
class Processor : public ContextHub<ContextOwnership::transfer>
{
	friend class ExecutionEngine;

public:
	explicit Processor(uint32 local_id);
	virtual ~Processor();

public:
	uint32 getId();

public:
	void setExecutionITC(shared_ptr<ExecutionITC> execution_itc);
	shared_ptr<ExecutionITC> getExecutionITC();

	void setMonitor(shared_ptr<ExecutionMonitor> monitor);
	shared_ptr<ExecutionMonitor> getMonitor();

public:
	void processITC(bool blocking = false, bool one_or_all = false);
	void sendITC(uint32 processor_id, KernelITC& itc);
	void sendITC(uint32 processor_id, KernelITC& itc, ExecutionITC::kernel_itc_handler_t continuation_handler);

public:
	virtual void initialize();
	virtual void finalize();

	virtual bool start(const std::shared_ptr<ExecutionMonitor>& monitor);
	virtual bool stop();

private:
	virtual void run(const shared_ptr<ExecutionMonitor>& monitor) = 0;

public:
    virtual void pushImplicitPendingCompletion(int count) = 0;
    virtual void popImplicitPendingCompletion(int count) = 0;

public:
	virtual bool isCompleted() = 0;
	virtual bool isRunning() = 0;

public:
	bool waitForCompletion();
	bool waitForCompletion(const boost::system_time& abs);
	bool waitForCompletion(const boost::posix_time::time_duration& duration);

protected:
	uint32 mProcessorId;
	shared_ptr<ExecutionITC> mExecutionITC;
	shared_ptr<ExecutionMonitor> mMonitor;
	std::atomic<bool> mStopRequest;
	boost::thread     mWorker;
};

class ProcessorRT : public Processor
{
public:
    explicit ProcessorRT(uint32 local_id);

    bool isServerFunctionEnabled() const noexcept;
    bool isClientFunctionEnabled() const noexcept;
    void enabledServerFunction(bool enable) noexcept;
    void enabledClientFunction(bool enable) noexcept;

    const std::shared_ptr<KernelBase>& getKernel() const noexcept;
    const std::shared_ptr<Service>&    getService(uint32 service_id) const;

    virtual void initialize() override;
    virtual void finalize() override;

    bool load(const boost::filesystem::path& main_ast_path,
              const boost::filesystem::path& main_runtime_path,
              const std::vector<boost::filesystem::path>& dep_paths);

    int64 queryFunctionId(const std::string& name) const;

    void attachService(uint32 service_id, shared_ptr<Service>&& service);
    void detachService(uint32 service_id);

    virtual void  exit(int32 exit_code) = 0;
    virtual int32 getExitCode() const = 0;

protected:
    bool                                  mEnableServerFunction;
    bool                                  mEnableClientFunction;
    shared_ptr<KernelBase>                mKernel;
    std::map<uint32, shared_ptr<Service>> mServices;
};

} }

#endif /* ZILLIANS_FRAMEWORK_PROCESSOR_H_ */
