/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_SERVICES_DETAIL_PROXY_HELPER_H_
#define ZILLIANS_FRAMEWORK_SERVICES_DETAIL_PROXY_HELPER_H_

#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/list/enum.hpp>
#include <boost/preprocessor/list/rest_n.hpp>
#include <boost/preprocessor/list/transform.hpp>
#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/seq/to_list.hpp>
#include <boost/preprocessor/tuple/elem.hpp>

#define ZILLIANS_FRAMEWORK_SERVICE_PROXY_OBJECT_NAME(proxied_class)         \
    BOOST_PP_CAT(g, proxied_class)

#define ZILLIANS_FRAMEWORK_SERVICE_PROXY_DEF_IMPL_PARAM_DECL(r, _, param)   \
    BOOST_PP_TUPLE_ELEM(2, 0, param) BOOST_PP_TUPLE_ELEM(2, 1, param)

#define ZILLIANS_FRAMEWORK_SERVICE_PROXY_DEF_IMPL_PARAM_REF(r, _, param)    \
    BOOST_PP_TUPLE_ELEM(2, 1, param)

#define ZILLIANS_FRAMEWORK_SERVICE_PROXY_DEF_IMPL_PARAM(type, signature)                             \
    BOOST_PP_LIST_ENUM(                                                                              \
        BOOST_PP_LIST_TRANSFORM(                                                                     \
            BOOST_PP_CAT(ZILLIANS_FRAMEWORK_SERVICE_PROXY_DEF_IMPL_PARAM_, type),                    \
            ,                                                                                        \
            BOOST_PP_LIST_REST_N(2, BOOST_PP_SEQ_TO_LIST(signature))                                 \
        )                                                                                            \
    )

#define ZILLIANS_FRAMEWORK_SERVICE_PROXY_DEF_IMPL(r, proxied_class, signature)                                     \
    BOOST_PP_SEQ_ELEM(0, signature) BOOST_PP_SEQ_ELEM(1, signature)                                                \
        (                                                                                                          \
            ZILLIANS_FRAMEWORK_SERVICE_PROXY_DEF_IMPL_PARAM(DECL, signature)                                       \
        )                                                                                                          \
    {                                                                                                              \
        return ZILLIANS_FRAMEWORK_SERVICE_PROXY_OBJECT_NAME(proxied_class)->BOOST_PP_SEQ_ELEM(1, signature)(       \
            ZILLIANS_FRAMEWORK_SERVICE_PROXY_DEF_IMPL_PARAM(REF, signature)                                        \
        );                                                                                                         \
    }

#define ZILLIANS_FRAMEWORK_SERVICE_PROXY_DEF(proxied_class, ns, signatures)                                  \
    namespace {                                                                                              \
    proxied_class* ZILLIANS_FRAMEWORK_SERVICE_PROXY_OBJECT_NAME(proxied_class) = nullptr;                    \
    }                                                                                                        \
    namespace proxies { namespace ns {                                                                       \
        BOOST_PP_SEQ_FOR_EACH(                                                                               \
            ZILLIANS_FRAMEWORK_SERVICE_PROXY_DEF_IMPL,                                                       \
            proxied_class,                                                                                   \
            signatures                                                                                       \
        )                                                                                                    \
    } }  /* namespace proxies::ns */

#endif /* ZILLIANS_FRAMEWORK_SERVICES_DETAIL_PROXY_HELPER_H_ */
