/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_SERVICES_OBJECTSERVICECUDA_H_
#define ZILLIANS_FRAMEWORK_SERVICES_OBJECTSERVICECUDA_H_

#include "thor/lang/Language.h"
#include "framework/Service.h"
#include "framework/Processor.h"
#include "framework/buffer/Common.h"
#include "framework/processor/cuda/kernel/KernelApi.h"
#include "framework/service/object/ObjectServiceBuffer.h"

namespace zillians { namespace framework { namespace service { namespace cuda {

class ObjectService : public Service
{
public:
	explicit ObjectService(ProcessorRT* processor);
	virtual ~ObjectService();

public:
	virtual bool isCompleted();
	virtual bool isInvocationPending();

public:
	virtual void initialize();
	virtual void finalize();
	virtual void beforeInitializeOnKernel(shared_ptr<KernelBase> kernel);
	virtual void afterInitializeOnKernel(shared_ptr<KernelBase> kernel);
	virtual void beforeFinalizeOnKernel(shared_ptr<KernelBase> kernel);
	virtual void afterFinalizeOnKernel(shared_ptr<KernelBase> kernel);
	virtual void* getInternalBufferDataPtr();
	virtual void* getInternalBufferPtr();

public:
	virtual void handleEvent(events::type event);

private:
	int32 mDeviceId;
	ProcessorRT* mProcessor;
	bool mKernelInitialized;
	processor::cuda::KernelApi& mKernelApi;

private:
	TrackerHeader* mHeaderBlocks;
	uint32* mHeaderFlags;

	Block64* mB64Blocks;
	uint32* mB64Flags;

	Block128* mB128Blocks;
	uint32* mB128Flags;

	char* mGlobalHeap;
	char* mGlobalHeapNext;
	uint32* mGlobalHeapBucket;
	uint32* mGlobalHeapBucketNext;
	uint32* mGlobalHeapOffset;
	uint32* mGlobalHeapOffsetNext;
	uint32* mGlobalHeapBucketScanned; // for global heap compaction to store temporary result
	uint32* mGlobalHeapComactionWorkSegments; // for global heap compaction to store temporary result
	uint32* mGlobalHeapComactionWorkIndices; // for global heap compaction to store temporary result

	void (*mConfigureGlobalHeapFunc)(char* heap, uint32 heap_size, uint32* heap_bucket, uint32 heap_bucket_size, uint32* heap_bucket_offset, unsigned long long int heap_next_offset);

private:
	static log4cxx::LoggerPtr mLogger;
};

} } } }

#endif /* ZILLIANS_FRAMEWORK_SERVICES_OBJECTSERVICECUDA_H_ */
