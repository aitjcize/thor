/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_SERVICE_OBJECTSERVICEBUFFER_H_
#define ZILLIANS_FRAMEWORK_SERVICE_OBJECTSERVICEBUFFER_H_

#include <stdint.h>

#include "core/IntTypes.h"

#ifndef __CUDACC__
#include "thor/container/Vector.h"
#include "thor/lang/Language.h"
#endif

#ifdef __CUDACC__
    #define GET_RAW_BUFFER(p) ( ((zillians::byte*)p) - sizeof(zillians::framework::service::cuda::ObjectHeader) )
#else
    #define GET_RAW_BUFFER(p)                                                                                    \
        reinterpret_cast< ::zillians::framework::service::mt::ObjectHeader*>(                                    \
            reinterpret_cast< ::zillians::byte*>(p) - sizeof(::zillians::framework::service::mt::ObjectHeader)   \
        )
#endif

namespace zillians { namespace framework {

namespace buffer {

struct cuda_object_svc_constants {
    static const std::size_t max_object_count = 32*32*32*32;         // 1M objects
    static const std::size_t total_flags      = 1+32+32*32+32*32*32; // 4 layers of allocation bit flags
    static const std::size_t global_heap_size = 256*1024*1024;       // 256MB
    static const std::size_t heap_compaction_chunk_size = 128;       // 128 bytes
};

} // namespace buffer

namespace service {

namespace cuda {

struct TrackerHeader
{
    uint64_t epoch;
    uint64_t type_id;
    uint64_t session_id;
    uint64_t database_id;
    uint32_t allocation_level;
    uint32_t allocation_index;
    char* ptr;
    bool is_valid;
};

struct ObjectHeader
{
    int64_t magic_number;
    TrackerHeader* tracker_ptr;
};

struct Block64 {
    uint64_t t0;
    uint64_t t1;
    uint64_t t2;
    uint64_t t3;
    uint64_t t4;
    uint64_t t5;
    uint64_t t6;
    uint64_t t7;
};

struct Block128 {
    uint64_t t0;
    uint64_t t1;
    uint64_t t2;
    uint64_t t3;
    uint64_t t4;
    uint64_t t5;
    uint64_t t6;
    uint64_t t7;
    uint64_t t8;
    uint64_t t9;
    uint64_t t10;
    uint64_t t11;
    uint64_t t12;
    uint64_t t13;
    uint64_t t14;
    uint64_t t15;
};

} // namespace cuda

#ifndef __CUDACC__

namespace mt {

const int64 gMagicNumber = 0xFFEEFFAABBEE9933;

struct TrackerHeader
{
    static const bool sInitialValid = false;
    static const uint64 sInitialEpoch = 0;
    static const uint64 sInitialTypeId = -1;

    TrackerHeader()
    {
        reset();
    }

    void reset()
    {
        is_valid = sInitialValid;
        epoch = sInitialEpoch;
        type_id = sInitialTypeId;
        object_ptr = nullptr;
    }

    bool is_valid;
    uint64 epoch;
    uint64 type_id;
    byte* object_ptr;
};

struct ObjectHeader
{
    int64          magic_number;
    TrackerHeader* tracker_ptr;
};

namespace proxies { namespace object {

using Object = ::thor::lang::Object;
template<typename ValueType>
using Vector = ::thor::container::Vector<ValueType>;

byte*   create(int64 size, int64 type_id);
Object*  clone(Object* source);
bool   destroy(int32 count, byte** p);

byte* dynCast(byte* object, int64 target_type_id);

int64 getGCGenerationId();
void  getActiveObjectsInternal(Vector<Object*>* objects);

void      addToObjectRootSet(int8* object);
void removeFromObjectRootSet(int8* object);
void      addToGlobalObjectRootSet(int8* object);
void removeFromGlobalObjectRootSet(int8* object);

} }  // namespace proxies::object

} // namespace mt

#endif

} } }

#endif /* ZILLIANS_FRAMEWORK_SERVICE_OBJECTSERVICEBUFFER_H_ */
