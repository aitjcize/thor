/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_SERVICES_OBJECT_DETAIL_GARBAGECOLLECTOR_H_
#define ZILLIANS_FRAMEWORK_SERVICES_OBJECT_DETAIL_GARBAGECOLLECTOR_H_

#include <memory>
#include <unordered_set>

#include "core/ConditionVariable.h"
#include "core/IntTypes.h"

#include "framework/Processor.h"
#include "framework/Kernel.h"
#include "framework/ProcessorId.h"
#include "framework/service/object/detail/Tracker.h"

#include "thor/lang/Language.h"

namespace zillians { namespace framework { namespace service { namespace mt { namespace detail {

// define the type of the epoch
using EPOCH_TYPE = uint64;

class Tracker;

// define mapping from epoch to a color
#define COLOR(epoch)         ((epoch) % 3)
#define MUTATOR_COLOR(epoch) COLOR(epoch)
#define MARKER_COLOR(epoch)  COLOR(epoch-1)
#define SWEEPER_COLOR(epoch) COLOR(epoch-2)

struct ObjectTrackTrait
{
    using ElemType      = TrackerHeader*;
    using ContainerType = tbb::concurrent_hash_map<ElemType, unsigned>;
};

struct GlobalObjectTrackTrait
{
    using ElemType      = byte**;
    using ContainerType = tbb::concurrent_hash_map<ElemType, unsigned>;
};

struct GCContext
{
    GCContext(
              ObjectTrackTrait::ContainerType& object_root_set,
        GlobalObjectTrackTrait::ContainerType& global_root_set
    )
        : tracker(nullptr)
        , kernel()
        , object_root_set(&object_root_set)
        , global_root_set(&global_root_set)
    {
    }

    Tracker*               tracker;
    shared_ptr<KernelBase> kernel;

          ObjectTrackTrait::ContainerType* object_root_set;
    GlobalObjectTrackTrait::ContainerType* global_root_set;
};

/*
 * Base class for garbage collection related classes.
 */
class GCBase
{
public:
    explicit GCBase(GCContext& context);
    virtual ~GCBase();

public:
    void run(EPOCH_TYPE epoch);
    void stop();
    bool isWorking() { return is_working; }

    // yentien: There is no call to this function in the codebase. It's only for profiling.
#ifdef ENABLE_GC_STATS
    void dumpStats(const std::string& title);
#endif

public:
    void setTracker(Tracker* tracker);

private:
    void asyncRun();
    virtual void runImpl() = 0;

protected:
    GCContext& gc_context;
    EPOCH_TYPE current_epoch;

#ifdef ENABLE_GC_STATS
    double total_time;
       int total_count;
#endif

private:
    std::unique_ptr<boost::thread> work_thread;
    ConditionVariable<uint32>      cond;

    volatile bool is_working;
};


/*
 * @brief Implement VCGC
 *
 * It provides async marking and sync marking (reMark).
 */
class GCMarker : public GCBase
{
public:
    explicit GCMarker(GCContext& context);
    virtual ~GCMarker();

public:
    // It uses the previous collected marked object as the starting root set
    void reMark();

private:
    virtual void runImpl();

    std::vector<ObjectTrackTrait::ElemType> getSharedRoots();

    void marking(std::vector<ObjectTrackTrait::ElemType>& roots);
    void markObject(ObjectTrackTrait::ElemType& object);

    void traceObjectMembers(ObjectTrackTrait::ElemType& object, const language::tree::ClassDecl& node);
    void traceContainedObjects(ObjectTrackTrait::ElemType& object, const language::tree::ClassDecl& node, const language::tree::ClassDecl& garbage_collectable_interface);

private:
    bool isContainer(const language::tree::ClassDecl& node, const language::tree::ClassDecl*& garbage_collectable_interface);

private:
    static log4cxx::LoggerPtr gc_logger;

    std::unordered_set<ObjectTrackTrait::ElemType> marked_objects;
};


/*
 * @brief Implement VCGC
 *
 *
 */
class GCSweeper : public GCBase
{
public:
    using RecycleContainer = std::vector<thor::lang::Object*>;

    explicit GCSweeper(GCContext& context);
    virtual ~GCSweeper();

public:
    RecycleContainer& getRecycleObjects();
    void              resetRecycleObjects();

    void presentSweepResult();

private:
    virtual void runImpl();
    void sweepObject();

private:
    static log4cxx::LoggerPtr gc_logger;

private:
    // A list of need-to-be-collected objects put here, waiting for 
    // runtime service invoking recycle function
    //
    // Note that the data structure should be carefully treated, it's
    // not thread-safe.
    //
    // Since GarbageCollector uses mWorking flag to busy-waiting GCSweeper,
    // a double buffer scheme can be applied here instead of a mutex lock.
    RecycleContainer garbage_objs;   // Access by runtime service thread 
                                     // for creating object clearance invocations

    RecycleContainer collected_objs; // Internal working container for sweeper 
};

class GarbageCollector
{
public:
    explicit GarbageCollector(log4cxx::Logger& logger);
    ~GarbageCollector();

    bool isInitialized() const noexcept;
    void initialize(const std::shared_ptr<KernelBase>& kernel);
    void finalize();

    EPOCH_TYPE getEpoch() const noexcept;

    EPOCH_TYPE trigger(Tracker* tracker);

    bool isWorking() const;

    GCSweeper::RecycleContainer& getRecycleObjects();
    void                         resetRecycleObjects();

    void      addToObjectRootSet(int8* ptr);
    void removeFromObjectRootSet(int8* ptr);

    void      addToGlobalRootSet(int8* ptr);
    void removeFromGlobalRootSet(int8* ptr);

private:
    template<typename Trait> typename Trait::ContainerType*        getRootSet();
    template<typename Trait> void                                addToRootSetImpl(const typename Trait::ElemType ptr);
    template<typename Trait> void                           removeFromRootSetImpl(const typename Trait::ElemType ptr);

private:
    EPOCH_TYPE       current_epoch;

    GCContext                  context;
    std::unique_ptr<GCMarker > marker;
    std::unique_ptr<GCSweeper> sweeper;
    tbb::tick_count            last_time_gc;

          ObjectTrackTrait::ContainerType object_root_set;
    GlobalObjectTrackTrait::ContainerType global_root_set;

    log4cxx::Logger* logger;
};

} } } } }

#endif /* ZILLIANS_FRAMEWORK_SERVICES_OBJECT_DETAIL_GARBAGECOLLECTOR_H_ */
