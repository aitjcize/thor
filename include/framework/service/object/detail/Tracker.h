/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_SERVICES_OBJECT_DETAIL_TRACKER_H_
#define ZILLIANS_FRAMEWORK_SERVICES_OBJECT_DETAIL_TRACKER_H_

#include "core/IntTypes.h"

#include "framework/service/object/ObjectServiceBuffer.h"

#include <boost/lockfree/queue.hpp>

namespace zillians { namespace framework { namespace service { namespace mt { namespace detail {

class Tracker
{
public:
    explicit Tracker(Tracker* parent = nullptr);
    ~Tracker();

    byte* allocate(int64 size, uint64 epoch, int64 type_id);
    bool deallocate(byte* p);

    TrackerHeader** begin() noexcept;
    TrackerHeader** end  () noexcept;

private:
    uint64                                  total_count;
    TrackerHeader**                         headers;
    boost::lockfree::queue<TrackerHeader*>* allocator;
};

inline TrackerHeader** begin(Tracker& tracker) noexcept { return tracker.begin(); }
inline TrackerHeader** end  (Tracker& tracker) noexcept { return tracker.end  (); }

} } } } }

#endif /* ZILLIANS_FRAMEWORK_SERVICES_OBJECT_DETAIL_TRACKER_H_ */
