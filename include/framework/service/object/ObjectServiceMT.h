/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_SERVICES_OBJECTSERVICEMT_H_
#define ZILLIANS_FRAMEWORK_SERVICES_OBJECTSERVICEMT_H_

#include <queue>
#include <memory>
#include <vector>

#include <boost/thread/shared_mutex.hpp>

#include "framework/Service.h"
#include "framework/Processor.h"
#include "framework/buffer/Common.h"
#include "framework/service/object/ObjectServiceBuffer.h"
#include "framework/service/object/detail/GarbageCollector.h"

#include "thor/lang/Language.h"
#include "thor/container/Vector.h"


// forward declarations
namespace zillians { namespace framework { namespace processor { namespace mt {

class Kernel;

} } } }

namespace zillians { namespace language { namespace stage {

class SynthesizedObjectLayoutContext;

} } }

namespace zillians { namespace language { namespace tree {

class ClassDecl;

} } }


namespace zillians { namespace framework { namespace service { namespace mt {

namespace detail {

class Tracker;

}

using TypeId = decltype(TrackerHeader::type_id);

class ObjectService : public Service
{
private:
    using Object = ::thor::lang::Object;

    template<typename ValueType>
    using Vector = ::thor::container::Vector<ValueType>;

    using ObjectLayout = zillians::language::stage::SynthesizedObjectLayoutContext;
    using ClassDecl    = zillians::language::tree::ClassDecl;

public:
    explicit ObjectService(ProcessorRT* processor);
    virtual ~ObjectService();

public:
    virtual bool isCompleted();
    virtual bool isInvocationPending();

public:
    virtual void initialize();
    virtual void finalize();
    virtual void beforeInitializeOnKernel(shared_ptr<KernelBase> kernel);
    virtual void afterInitializeOnKernel(shared_ptr<KernelBase> kernel);
    virtual void beforeFinalizeOnKernel(shared_ptr<KernelBase> kernel);
    virtual void afterFinalizeOnKernel(shared_ptr<KernelBase> kernel);

public:
    byte*  create(int64 size, int64 type_id);
    Object* clone(Object* source);
    bool  destroy(int32 count, byte** p);

    byte* dynCast(byte* object, int64 target_type_id);

    bool isGCInitialized();

    detail::GCSweeper::RecycleContainer& getRecycleObjects();
    void                                 resetRecycleObjects();

    void      addToObjectRootSet(int8* object);
    void removeFromObjectRootSet(int8* object);
    void      addToGlobalObjectRootSet(int8* object);
    void removeFromGlobalObjectRootSet(int8* object);

private:
    processor::mt::Kernel* getKernel() const;
    ObjectLayout&          getObjectLayout(void*  object ) const;
    ObjectLayout&          getObjectLayout(TypeId type_id) const;
    const ClassDecl*       tryGetClassDecl(void*  object ) const;
    const ClassDecl*       tryGetClassDecl(TypeId type_id) const;
    const ClassDecl&       getClassDecl(TypeId type_id   ) const;

public:
    virtual void handleEvent(events::type event);

public:
    int64 getGCGenerationId();
    void getActiveObjectsInternal(Vector<Object*>* objects);

private:
    ProcessorRT* mProcessor;

private:
    detail::Tracker*             tracker;
    std::queue<detail::Tracker*> stale_trackers;
    boost::shared_mutex          tracker_proctor;

    detail::GarbageCollector     gc;

private:
    static log4cxx::LoggerPtr mLogger;
};

} } } }

#endif /* ZILLIANS_FRAMEWORK_SERVICES_OBJECTSERVICEMT_H_ */
