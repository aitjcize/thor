/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_SERVICE_DOMAIN_DOMAIN_SERVICEMT_H_
#define ZILLIANS_FRAMEWORK_SERVICE_DOMAIN_DOMAIN_SERVICEMT_H_

#include <memory>
#include <string>

#include <tbb/concurrent_hash_map.h>

#include "core/IntTypes.h"

#include "utility/UUIDUtil.h"

#include "framework/ExecutionITC.h"
#include "framework/Service.h"
#include "framework/service/detail/RootSetPtr.h"

namespace zillians { namespace framework {

class KernelBase;
class Processor;

} }  // namespace zillians::framework

namespace zillians { namespace framework { namespace service { namespace mt {

class DomainService : public Service
{
public:
    using Domain             = ::thor::lang::Domain;
    using DomainConnCallback = ::thor::lang::Domain::ConnCallback;
    using DomainErrCallback  = ::thor::lang::Domain::ErrCallback;

private:
    struct DomainCallbacks;

public:
    explicit DomainService(ProcessorRT* processor);

public:
    virtual bool isCompleted() override;
    virtual bool isInvocationPending() override;

    virtual void initialize() override;
    virtual void finalize() override;

    virtual void beforeInitializeOnKernel(std::shared_ptr<KernelBase> kernel) override;
    virtual void afterInitializeOnKernel(std::shared_ptr<KernelBase> kernel) override;
    virtual void beforeFinalizeOnKernel(std::shared_ptr<KernelBase> kernel) override;
    virtual void afterFinalizeOnKernel(std::shared_ptr<KernelBase> kernel) override;

    virtual void handleEvent(events::type event);

    Domain* find(int64 session_id);
    Domain* insert(int64 session_id, Domain* d);

    UUID listen (const std::wstring& endpoint, DomainConnCallback* conn_cb, DomainConnCallback* disconn_cb, DomainErrCallback* err_cb);
    UUID connect(const std::wstring& endpoint, DomainConnCallback* conn_cb, DomainConnCallback* disconn_cb, DomainErrCallback* err_cb);
    bool cancel (const UUID& id);

private:
    template<int RequestType, typename Handler>
    UUID domainInit(const std::wstring& endpoint, DomainConnCallback* conn_cb, DomainConnCallback* disconn_cb, DomainErrCallback* err_cb, Handler&& handler);

    void handleListenResponse(uint32 source, KernelITC& response_itc);
    void handleConnectResponse(uint32 source, KernelITC& response_itc);
    void handleCancelResponse(uint32 source, KernelITC& response_itc);

    void onDomainConnected   (uint32 source, KernelITC& request_itc);
    void onDomainDisconnected(uint32 source, KernelITC& request_itc);

    template<typename LambdaType, typename... ArgTypes>
    void doDomainCallback(const std::string& function_name, LambdaType* lambda, ArgTypes&&... args);

private:
    bool         verbose;
    ProcessorRT* processor;

    struct DomainCallbacks
    {
        enum
        {
            BY_LISTEN ,
            BY_CONNECT,
        };

        root_set_ptr<DomainConnCallback>    conn_callback;
        root_set_ptr<DomainConnCallback> disconn_callback;
        root_set_ptr<DomainErrCallback >     err_callback;

        int                                            by;

        bool isByListen () const noexcept;
        bool isByConnect() const noexcept;
    };

    tbb::concurrent_hash_map<int64, root_set_ptr<Domain>> domains;
    tbb::concurrent_hash_map<UUID, DomainCallbacks, UUIDHasher> callback_mapping;
};

} } } }

#endif /* ZILLIANS_FRAMEWORK_SERVICE_DOMAIN_DOMAIN_SERVICEMT_H_ */
