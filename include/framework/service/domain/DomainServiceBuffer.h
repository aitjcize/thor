/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_SERVICE_DOMAIN_DOMAINSERVICEBUFFER_H_
#define ZILLIANS_FRAMEWORK_SERVICE_DOMAIN_DOMAINSERVICEBUFFER_H_

#ifndef __CUDACC__
#include <string>

#include "core/IntTypes.h"

#include "utility/UUIDUtil.h"

#include "thor/lang/Domain.h"

namespace zillians { namespace framework { namespace service { namespace mt { namespace proxies { namespace domain {

using Domain = ::thor::lang::Domain;

Domain* find(int64 session_id);
Domain* insert(int64 session_id, Domain* d);

UUID listen (const std::wstring& endpoint, Domain::ConnCallback* conn_cb, Domain::ConnCallback* disconn_cb, Domain::ErrCallback* err_cb);
UUID connect(const std::wstring& endpoint, Domain::ConnCallback* conn_cb, Domain::ConnCallback* disconn_cb, Domain::ErrCallback* err_cb);
bool cancel (const UUID& id);

} } } } } }  // namespace zillians::framework::service::mt::proxies::domain
#endif

#endif /* ZILLIANS_FRAMEWORK_SERVICE_DOMAIN_DOMAINSERVICEBUFFER_H_ */
