/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_SERVICE_RUNTIMESERVICEBUFFER_H_
#define ZILLIANS_FRAMEWORK_SERVICE_RUNTIMESERVICEBUFFER_H_

#ifndef __CUDACC__
#include <cstring>

#include <atomic>

#include <boost/context/all.hpp>

#include <tbb/concurrent_hash_map.h>

#include "framework/buffer/BufferManager.h"

#include "thor/lang/Replication.h"
#endif

#include <functional>
#include <memory>
#include <vector>

#include "framework/buffer/Common.h"

namespace zillians { namespace framework {

namespace service {

#ifndef __CUDACC__

class ReplicationData;

#endif

struct Invocation {

#ifdef __CUDACC__
    __device__
    __host__
#endif
    void reset()
    {
        function_id = -1;
        session_id = -2;        // well, -1 stands for myself, choose another value
#ifndef __CUDACC__
        std::memset(&buffer, 0, sizeof(buffer));
#else
        for(int i=0;i<PARAMETER/sizeof(int);++i)
            ((int*)&parameters)[i] = 0;
#endif
        activated = 0;
        valid = 1;
        remote = 0;
        depend_on_counter = NULL;
        be_depended_counter = NULL;
        store_to = NULL;
    }

#ifndef __CUDACC__
    void cleanup();
#endif

    static const std::size_t PAYLOAD = 128;

    // NOTE: well, to prevent from padding, the safetest way is to make all member the 
    // same size

    int64 function_id;
    int64 session_id;
    int64 activated; // if true, it means that it had run or is running

    // You may wonder why we have invalid invocation. In kernel spilt, we may split the invocation request buffer according 
    // to whether it is active or not, and those inactive ones will be moved to the next invocation request buffer. However,
    // we cannot remove the invocation buffer from the current invocation buffer request. Instead, we mark it as invalid;
    int64 valid;
    int64 remote;

#ifndef __CUDACC__
    std::atomic<int32>*   depend_on_counter; // points to depended invocation's counter
    std::atomic<int32>* be_depended_counter; // points to shared memory which is a counter recognize how many invocation depended on this one
#else
    int32*                depend_on_counter; // points to depended invocation's counter
    int32*              be_depended_counter; // points to shared memory which is a counter recognize how many invocation depended on this one
#endif
    void* store_to; // store result into

    static const std::size_t HEADER    = sizeof(int64) /* function_id */
                                       + sizeof(int64) /*  session_id */
                                       + sizeof(int64) /*   activated */
                                       + sizeof(int64) /*   valid     */
                                       + sizeof(int64) /*   remote    */
#ifndef __CUDACC__
                                       + sizeof(std::atomic<int32>*) /*    depended_counter */
                                       + sizeof(std::atomic<int32>*) /* be_depended_counter */
#else
                                       + sizeof(int32*             ) /*    depended_counter */
                                       + sizeof(int32*             ) /* be_depended_counter */
#endif
                                       + sizeof(void*              ) /* store_to            */;
    static const std::size_t PARAMETER = PAYLOAD - HEADER;

#ifndef __CUDACC__
    union {
        struct {
            ReplicationData* data;
            int64            adaptor_id;
        } remote_info; // in this form if 'remote' is non-zero
        byte parameters[PARAMETER];
    } buffer;

    static_assert(sizeof(buffer) == sizeof(buffer.parameters), "inconsistent size between actual and expected size");
#else
    byte parameters[PARAMETER];
#endif
};

#ifndef __CUDACC__

class ReplicationData
{
private:

    struct DataBlock
    {
        DataBlock* next;
        byte       buffer[];
    };

public:
    using buffer_location_t = buffer::buffer_location_t::type;

    ReplicationData(buffer_location_t location) noexcept;
    ReplicationData(ReplicationData&& ref) noexcept;
    ~ReplicationData();

    ReplicationData& operator=(ReplicationData&& ref) noexcept;

    std::size_t read(byte* buffer, std::size_t count);
    std::size_t readsome(byte* buffer, std::size_t count);

    std::size_t write(const byte* data, std::size_t count);
    std::size_t writesome(const byte* data, std::size_t count);

    std::size_t peek(byte* buffer, std::size_t count);
    std::size_t peeksome(byte* buffer, std::size_t count);

    void resetRead() noexcept;
    bool reach_end_of_data() const;

    std::size_t getSize() const noexcept;

private:
    ReplicationData(const ReplicationData&);
    ReplicationData& operator=(const ReplicationData&);

    void cleanup() noexcept;

private:
    buffer_location_t location;
    DataBlock*        first_block;

    struct {
        DataBlock*  block;
        std::size_t buffer_offset;
        std::size_t remain_size;
    } info_read;

    struct {
        DataBlock*  block;
        std::size_t buffer_offset;
        std::size_t written_size;
    } info_write;

    static const std::size_t size_per_block;
};

class InvocationRequestBuffer : public buffer::DynamicBufferBase<InvocationRequestBuffer>
{
public:
    struct Dimension {
        Dimension() : number_of_invocations(0) { }
        Dimension(uint32 size) : number_of_invocations(size) { }
        uint32 number_of_invocations;
    };

public:
    explicit InvocationRequestBuffer(uint32 processor_id);

    virtual ~InvocationRequestBuffer();

public:
    bool construct(buffer::buffer_location_t::type location, const Dimension& dim);
    bool destruct();
    bool copyTo(InvocationRequestBuffer& target_buffer);
    bool moveTo(InvocationRequestBuffer& target_buffer);

public:
    void setExecutionMode(bool sync_or_async);
    bool getExecutionMode();

    int64 getResponseSequence() const;
    void setResponseSequence(int64 seq);

    uint32 getInvocationCount() const;
    void setInvocationCount(uint32 count);

    int64 getFunctionId(int32 index) const;
    void setFunctionId(int32 index, int64 function_id);

    int64 getSessionId(int32 index) const;
    void setSessionId(int32 index, int64 session_id);

    byte* getInvocationRawPtr(int32 index);
    byte* getInvocationPtr(int32 index);

    bool isActivated(int32 index) const;
    void setActivated(int32 index);

    bool isValid(int32 index) const;
    void setInvalid(int32 index);

    bool isRemote(int32 index) const;
    void setRemote(int32 index);

    Invocation* getInvocation(int32 index);
    Invocation* getReservedInvocation(int32 index);
    Invocation* reserveInvocation(int64 session_id, int64 function_id);
    int32       reserveInvocationId(int64 session_id, int64 function_id);

public:
    void insertInvocationAtFront(const std::vector<Invocation*>& invocations);

public:
    template<typename T>
    T* getExecutionContext() {
        return static_cast<T*>(getExecutionContext());
    }

    void* getExecutionContext();
    void setExecutionContext(void* ctx);

//    TaskListType* getTasks();
//    void setTasks(TaskListType* tasks);

public:
    Dimension declared_dim;

private:
    bool execution_mode;
    int64 response_seq;
    uint32 current_size;
    uint32 allocated_size;

private:
    void* execution_context;
};

struct InvocationResponseBuffer
{
    int64 response_seq;
    int32 result;
};

#endif

namespace cuda {

struct __attribute__((aligned(4))) ControlFlags
{
    bool daemonized;

    bool exited;
    int32 exit_code;

    bool implicit_exited;
    int32 implicit_exit_code;

    bool explicit_exited;
    int32 explicit_exit_code;
};

} // namespace cuda

#ifndef __CUDACC__

namespace mt { namespace proxies { namespace runtime {

const std::vector<std::wstring>& getArguments();
void                setArguments(const std::vector<std::wstring>& args);

void exit(int32 exit_code);

int32 getCurrentInvocationId();

InvocationRequestBuffer* getCurrentInvocations();
InvocationRequestBuffer* getCurrentThreadNextInvocations(uint32 target_id);

void postponeInvocationDependencyTo(int32 target_id, int32 postpone_to_id);
void setInvocationDependOnCount(int32 target_id, int32 invocation_id, int32 count);
void setInvocationDependOn(int32 target_id, int32 invocation_id, int32 depend_on_id);

void  delayInvocation(Invocation& invocation);
int32 addInvocation(int32 target_id, int64 session_id, int64 function_id, int8*            store_to);
void  addInvocation(int32 target_id, int64 session_id, int64 adaptor_id , ReplicationData* data    );

int32 reserveInvocation(int32 target_id, int64 session_id, int64 function_id);
bool  commitInvocation(int32 target_id, int32 id);
bool  abortInvocation(int32 target_id, int32 id);

void setInvocationReturnPtr(int32 target_id, int32 id, void* ptr);

template<typename Type>
int64 appendInvocationParameter(int32 target_id, int32 id, int64 offset, const Type& value);

using ::thor::lang::Object;
using ::thor::lang::ReplicationDecoder;
using ::thor::lang::ReplicationEncoder;

using RemoteAdaptorSignature = void(ReplicationDecoder*);
using CreatorSignature       = Object*();
using SerializerSignature    = bool(ReplicationEncoder*,Object*);
using DeserializerSignature  = bool(ReplicationDecoder*,Object*);

RemoteAdaptorSignature* getRemoteAdaptor(int64 adaptor_id);
CreatorSignature*       getCreator      (int64 type_id);
SerializerSignature*    getSerializer   (int64 type_id);
DeserializerSignature*  getDeserializer (int64 type_id);

const std::wstring* queryStringLiteral(int64 symbol_id);

} } }  // namespace mt::proxies::runtime

#endif /* __CUDACC__ */

} // namespace service

namespace buffer {

struct cuda_runtime_svc_constants {
    static const std::size_t dma_threads_per_block = 32;
    static const std::size_t compute_threads_per_block = 32;
    static const std::size_t dispatcher_threads_per_block = dma_threads_per_block + compute_threads_per_block;
    static const std::size_t warp_size = 32;
    static const std::size_t warps_per_block = dispatcher_threads_per_block / warp_size;
    static const std::size_t parameter_load_element_size = 4;
    static const std::size_t parameter_load_total_size_per_invocation = service::Invocation::PAYLOAD;
    static const std::size_t parameter_load_total_size_per_block = dispatcher_threads_per_block * parameter_load_element_size;
    static const std::size_t parameter_load_total_size_per_warp = warp_size * parameter_load_element_size;
    static const std::size_t parameter_load_element_count = parameter_load_total_size_per_invocation / parameter_load_element_size;
    static const std::size_t parameter_load_total_warp_per_invocation = parameter_load_total_size_per_invocation / parameter_load_total_size_per_warp;
    static const std::size_t parameter_load_total_warp_count = parameter_load_total_size_per_block / parameter_load_total_size_per_warp;
    static const std::size_t parameter_load_working_items_per_warp = CEILING(parameter_load_total_warp_count, warps_per_block);
    static const std::size_t dma_transfer_element_size = 4;
    static const std::size_t dma_transfer_buffer_size = 128;
    static const std::size_t next_invocation_bucket_size = 8;

#ifndef __CUDACC__
private:
    static void check() {
        static_assert(dispatcher_threads_per_block % warp_size == 0, "invalid dispatcher thread count");
        static_assert(parameter_load_total_size_per_invocation % (warp_size * parameter_load_element_size) == 0, "parameter size must be multiple of warp load size");
//        static_assert(compute_threads_per_block * )
    }
#endif
};

} // namespace buffer

} } // namespace zillians::framework

#endif /* ZILLIANS_FRAMEWORK_SERVICE_RUNTIMESERVICEBUFFER_H_ */
