/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_SERVICE_RUNTIMESERVICECUDA_H_
#define ZILLIANS_FRAMEWORK_SERVICE_RUNTIMESERVICECUDA_H_

#include "framework/Service.h"
#include "framework/Processor.h"
#include "framework/buffer/Common.h"
#include "framework/service/runtime/RuntimeService.h"
#include "framework/processor/cuda/kernel/KernelApi.h"
#include "framework/service/runtime/RuntimeServiceBuffer.h"

namespace zillians { namespace framework { namespace service { namespace cuda {

class RuntimeService : public RuntimeServiceBase
{
public:
	RuntimeService(ProcessorRT* processor);
	virtual ~RuntimeService();

public:
	virtual bool isCompleted();
	virtual bool isInvocationPending();

public:
	virtual void initialize();
	virtual void finalize();
	virtual void afterInitializeOnKernel(shared_ptr<KernelBase> kernel);

public:
	void setupArguments(const std::wstring& args);
	int32 getExitCode();

public:
	virtual void handleEvent(events::type event);

private:
	void handleProcessorInvocation(uint32 source, KernelITC& itc);
	void handleSessionEvent(int32 event, uint32 source, KernelITC& itc);

private:
    void dispatchNextInvocations();
    void dispatchObjectClearInvocations();

private:
	ProcessorRT* mProcessor;

private:
	int32 mDeviceId;

	struct {
		int64*  function_ids;
		int32*  shuffle_indices;
		Invocation** next_invocation_buffer_container;
		int32*  next_invocation_size_buffer;
		int32*  next_invocation_index_buffer;
		InvocationRequestBuffer** next_invocation_buffer_objects;
		ControlFlags* control_flags;
	} mDevicePtr;

	struct {
	    ControlFlags* control_flags;
	} mHostPtr;

	int64* mFunctionIdsBuffer;
	int32* mShuffleIndicesBuffer;

	void (*mSetDebugCondFunc)(bool cond);
	void (*mSetCurrentInvocationBufferFunc)(Invocation* ptr, int32 size);
	void (*mSetNextInvocationBufferFunc)(int32 index, Invocation* ptr, int32 size);
	bool mKernelInitialized;
	bool mVerboseMode;

	std::vector<InvocationRequestBuffer*> mBuffers;

private:
	std::list<std::pair<uint32, KernelITC>>  mDispatchedInvocations;

private:
	processor::cuda::KernelApi& mKernelApi;

private:
	static log4cxx::LoggerPtr mLogger;
};

} } } }

#endif /* ZILLIANS_FRAMEWORK_SERVICE_RUNTIMESERVICECUDA_H_ */
