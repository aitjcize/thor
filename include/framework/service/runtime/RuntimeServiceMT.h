/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_SERVICE_RUNTIMESERVICEMT_H_
#define ZILLIANS_FRAMEWORK_SERVICE_RUNTIMESERVICEMT_H_

#include <list>
#include <memory>
#include <string>
#include <utility>

#include <log4cxx/logger.h>

#include "core/IntTypes.h"

#include "framework/buffer/BufferConstants.h"
#include "framework/ExecutionITC.h"
#include "framework/Service.h"
#include "framework/service/runtime/RuntimeServiceBuffer.h"

#include "thor/lang/Lambda.h"
#include "thor/lang/Language.h"
#include "thor/lang/Replication.h"

namespace zillians { namespace framework {

class KernelBase;
class ProcessorRT;

namespace processor { namespace mt {

class Kernel;

} }  // namespace processor::mt

} }  // namespace zillians::framework

namespace zillians { namespace framework { namespace service { namespace mt {

class RuntimeService : public Service
{
public:
    explicit RuntimeService(ProcessorRT* processor, std::vector<std::wstring>& args);
    virtual ~RuntimeService();

public:
    virtual bool isCompleted();
    virtual bool isInvocationPending();

public:
    virtual void initialize();
    virtual void finalize();

    virtual void beforeInitializeOnKernel(shared_ptr<KernelBase> kernel);
    virtual void afterInitializeOnKernel(shared_ptr<KernelBase> kernel);
    virtual void beforeFinalizeOnKernel(shared_ptr<KernelBase> kernel);
    virtual void afterFinalizeOnKernel(shared_ptr<KernelBase> kernel);

public:
    const std::vector<std::wstring>& getArguments() const;
    void                setArguments(const std::vector<std::wstring>& args);

    void exit(int32 exit_code);

    int32 getCurrentInvocationId() const;

    Invocation*              getCurrentInvocation() const;
    InvocationRequestBuffer* getCurrentInvocations() const;
    InvocationRequestBuffer* getCurrentThreadNextInvocations(uint32 target_id);
    void                     setCurrentInvocations(InvocationRequestBuffer* current_invocation_req);

    void postponeInvocationDependencyTo(int32 target_id, int32 postpone_to_id);
    void setInvocationDependOnCount(int32 target_id, int32 invocation_id, int32 count);
    void setInvocationDependOn(int32 target_id, int32 invocation_id, int32 depend_on_id);

    void  delayInvocation(Invocation& invocation);
    int32 addInvocation(int32 target_id, int64 session_id, int64 function_id, int8* store_to);
    void  addInvocation(int32 target_id, int64 session_id, int64 adaptor_id, ReplicationData* data);

    using Object             = ::thor::lang::Object;

    int32 reserveInvocation(int32 target_id, int64 session_id, int64 function_id);
    bool  commitInvocation(int32 target_id, int32 id);
    bool  abortInvocation(int32 target_id, int32 id);

    void setInvocationReturnPtr(int32 target_id, int32 id, void* ptr);

    template<typename Type>
    int64 appendInvocationParameter(int32 target_id, int32 id, int64 offset, const Type& value);

public:
    using ReplicationDecoder = ::thor::lang::ReplicationDecoder;
    using ReplicationEncoder = ::thor::lang::ReplicationEncoder;

    using RemoteAdaptorSignature = void(ReplicationDecoder*);
    using CreatorSignature       = Object*();
    using SerializerSignature    = bool(ReplicationEncoder*,Object*);
    using DeserializerSignature  = bool(ReplicationDecoder*,Object*);

    RemoteAdaptorSignature* getRemoteAdaptor(int64 adaptor_id);
    CreatorSignature*       getCreator      (int64 type_id);
    SerializerSignature*    getSerializer   (int64 type_id);
    DeserializerSignature*  getDeserializer (int64 type_id);

    const std::wstring* queryStringLiteral(int64 symbol_id) const;

private:
    processor::mt::Kernel* getKernel() const;

public:
    virtual void handleEvent(events::type event);

private:
    void handleProcessorInvocation(uint32 source, KernelITC& itc);

private:
    void dispatchNextInvocations();
    void dispatchObjectClearInvocations();

private:
    ProcessorRT*      mProcessor;

private:
    bool mVerboseMode;

    std::vector<std::wstring> args;
    InvocationRequestBuffer* current;
    InvocationRequestBuffer* next[buffer::buffer_constants::max_targets * buffer::buffer_constants::max_threads];
    processor::mt::Kernel* kernel;

    typedef std::pair<uint32, KernelITC> invocation_entry_t;
    std::list<invocation_entry_t>  mDispatchedInvocations;

private:
    static log4cxx::LoggerPtr mLogger;
};

} } } }

#endif /* ZILLIANS_FRAMEWORK_SERVICE_RUNTIMESERVICEMT_H_ */
