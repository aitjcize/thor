/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_SERVICES_COMMON_H_
#define ZILLIANS_FRAMEWORK_SERVICES_COMMON_H_

namespace zillians { namespace framework {

template<typename T>
inline T ceiling(T x, T y) {
    return ((x) + (y) - 1) / (y);
}

struct ServiceKernel
{
	struct data
	{
		enum type
		{
			runtime_api_invocation_ids,
			runtime_api_mm_chunks,
			gameobject_api_attributes,
		};
	};

	virtual void setServiceData(data::type, void* data) = 0;
};
//
//struct ServiceITC
//{
//	enum type
//	{
//		runtime_service,
//		gameobject_service,
//	} service_itc_type;
//
//	union command_t
//	{
//		struct
//		{
//
//		} runtime_api;
//
//		struct
//		{
//			struct itc { enum type { create_object, destroy_object, set_attributes, get_attributes }; } type;
//			union
//			{
//				struct
//				{
//
//				} create_object;
//
//				struct
//				{
//
//				} destroy_object;
//
//				struct
//				{
//
//				} update_object_full;
//
//				struct
//				{
//
//				} query_object_full;
//			};
//
//		} gameobject_api;
//	} command;
//};

} }

#endif /* ZILLIANS_FRAMEWORK_SERVICES_COMMON_H_ */
