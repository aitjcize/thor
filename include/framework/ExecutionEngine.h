/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_EXECUTIONENGINE_H_
#define ZILLIANS_FRAMEWORK_EXECUTIONENGINE_H_

#include "core/Prerequisite.h"
#include "core/ContextHub.h"
#include "core/ConditionVariable.h"
#include "core/Hash.h"

#include "threading/Dispatcher.h"
#include "threading/DispatcherThreadContext.h"

#include "framework/ExecutionMonitor.h"
#include "framework/buffer/Common.h"
#include "framework/buffer/BufferManager.h"
#include "framework/buffer/BufferNetwork.h"
#include "framework/Processor.h"

#include <tbb/recursive_mutex.h>

namespace zillians { namespace framework {

using namespace zillians::threading;
using namespace zillians::framework::buffer;

class ExecutionEngine : public ContextHub<ContextOwnership::transfer>
{
	static const uint32 ksMaxProcessorCount = 32;

public:
	ExecutionEngine();
	virtual ~ExecutionEngine();

public:
	uint32 countProcessors();
	Processor* operator [] (uint32 processor_id);

public:
	void attachProcessor(Processor* processor);
	void detachProcessor(uint32 processor_id);
	void detachAllProcessors();

public:
	bool initializeProcessor(uint32 id);
	void initializeAllProcessors();

	bool finalizeProcessor(uint32 id);
	void finalizeAllProcessors();

	bool startProcessor(uint32 id);
	void startAllProcessors();

	bool stopProcessor(uint32 id);
	void stopAllProcessors();

public:
	bool waitForCompletion();

public:
	void commitConfiguration();

private:
	shared_ptr<ExecutionMonitor> mMonitor;
	Processor* mProcessors[ksMaxProcessorCount];
	shared_ptr<Dispatcher<KernelITC> > mDispatcher;

private:
	static log4cxx::LoggerPtr mLogger;
};

} }

#endif /* ZILLIANS_FRAMEWORK_EXECUTIONENGINE_H_ */
