/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSORS_CUDA_KERNEL_H_
#define ZILLIANS_FRAMEWORK_PROCESSORS_CUDA_KERNEL_H_

#include "utility/Filesystem.h"
#include "framework/Kernel.h"
#include "framework/processor/cuda/kernel/KernelApi.h"
#include "framework/service/runtime/RuntimeServiceBuffer.h"

using namespace zillians::framework::buffer;

namespace zillians { namespace framework { namespace processor { namespace cuda {

class Kernel : public KernelBase
{
public:
	explicit Kernel(cuda::KernelApi& kernel_api, bool enable_server_function, bool enable_client_function);
	virtual ~Kernel();

public:
    virtual bool load(const boost::filesystem::path& main_ast_path,
                      const boost::filesystem::path& main_runtime_path,
                      const std::vector<boost::filesystem::path>& dep_paths
                      ) override;

public:
	virtual void initialize();
	virtual void finalize();

	virtual void launch(Processor* p) override;

public:
	void* getDispatcherHandle();

	void configureShuffleIndicesBuffer(int32* buffer, int32 size);
	void configureNextInvocationBufferContainer(service::Invocation** container);
	void configureNextInvocationSizeBuffer(int32* size_buffer);
	void configureNextInvocationIndexBuffer(int32* index_buffer);
	void configureControlFlags(service::cuda::ControlFlags* flags);
    void configureCurrentInvocationBuffer(service::Invocation* ptr, int32 size);
    void configureNextInvocationBuffer(int32 index, service::Invocation* ptr, int32 size);
	void configureLaunchMode(bool as_block_or_as_task);
	void configureLaunchShape(dim3 blocks, dim3 threads);
	void configureHardwareArchitecture(const char* architectures);

private:
	bool generateGlobalDispatcher();

private:
	std::wstring stringify(const language::tree::Identifier* id);
	std::wstring stringify(const language::tree::TypeSpecifier* specifier, bool for_type_or_var_decl);
	std::wstring stringify(const language::tree::FunctionSpecifier* specifier, bool for_type_or_var_decl);
	std::wstring stringify(const language::tree::MultiSpecifier* specifier, bool for_type_or_var_decl);
	std::wstring stringify(const language::tree::NamedSpecifier* specifier, bool for_type_or_var_decl);
	std::wstring stringify(const language::tree::PrimitiveSpecifier* specifier, bool for_type_or_var_decl);
	std::wstring stringify(const language::tree::FunctionDecl* decl, bool export_fwd, bool export_empty_impl, bool export_impl);
	std::wstring stringify(const language::tree::ClassDecl* decl, bool for_full_export_or_type_specifier, bool for_type_or_var_decl);
	std::wstring stringify(const language::tree::EnumDecl* decl);
	std::wstring stringify(const language::tree::Package* package, bool ns_opening, bool ns_closing, bool ns_using);

private:
	std::vector<boost::filesystem::path> object_libraries;
	std::vector<std::pair<std::string,std::string> > supported_architectures;

private:
	cuda::KernelApi& mKernelApi;

	void (*mGlobalDispatcherFunc)(dim3, dim3);

	void (*mConfigureShuffleIndicesBufferFunc)(int32*, int32);
	void (*mConfigureNextInvocationBufferContainerFunc)(service::Invocation**);
	void (*mConfigureNextInvocationSizeBufferFunc)(int32*);
	void (*mConfigureNextInvocationIndexBufferFunc)(int32*);
	void (*mConfigureControlFlagsFunc)(service::cuda::ControlFlags*);
	void (*mConfigureCurrentInvocationBufferFunc)(service::Invocation*, int32);
	void (*mConfigureNextInvocationBufferFunc)(int32, service::Invocation*, int32);

	void* mDispatcherHandle;
	dim3 mInvocationBlocks;
	dim3 mInvocationThreads;
	bool mLaunchMode;
};

} } } }

#endif /* ZILLIANS_FRAMEWORK_PROCESSORS_CUDA_KERNEL_H_ */
