/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSORS_KERNELMT_H_
#define ZILLIANS_FRAMEWORK_PROCESSORS_KERNELMT_H_

#include <cstddef>
#include <cstdint>

#include <atomic>
#include <memory>
#include <string>
#include <vector>

#include "framework/processor/detail/SimpleInvocationsLauncher.h"
#include "framework/service/runtime/RuntimeServiceBuffer.h"
#include "framework/Kernel.h"

namespace llvm {

class ExecutionEngine;

}

namespace zillians { namespace framework { namespace processor { namespace mt {

class Kernel : public KernelBase
{
public:
    Kernel(bool enable_server_function, bool enable_client_function, bool enable_mt);
    virtual ~Kernel();

    virtual bool load(const boost::filesystem::path& main_ast_path,
                      const boost::filesystem::path& main_runtime_path,
                      const std::vector<boost::filesystem::path>& dep_paths
                      ) override;
    void unload();

    uint32 getCurrentThreadId();
    int32  getCurrentThreadInvocationId();

    service::Invocation* getCurrentInvocation();

    virtual void launch(Processor* p) override;

    void* queryCallableImpl(const std::string& name) const;

private:
           bool launchRange(void* stack, std::size_t stack_size, std::size_t stack_index, service::InvocationRequestBuffer& invocations, int32 beg, int32 end);
    static void launchSingle(std::intptr_t ptr);

public:
    void exitLaunch();

    uint64 getGenerationId();

    template<typename FunctionType>
    FunctionType* queryCallable(const std::string& name) const
    {
        return reinterpret_cast<FunctionType*>(queryCallableImpl(name));
    }

private:
    bool generateGlobalDispatcher();

private:
    typedef void (*entry_t)(long long, char*, char*);

    entry_t                                mGlobalDispatcherFunc;
    std::unique_ptr<llvm::ExecutionEngine> mEngine;

    friend class detail::SimpleInvocationsLauncher;

    std::atomic<bool>                 mIsExited;
    detail::SimpleInvocationsLauncher mLauncher;

    log4cxx::LoggerPtr mLogger;

    void*              mMainHandle;
    std::vector<void*> mDepHandles;

    // Each generation means a kernel launch from main thread is executed.
    // So we increase the ID. This one is used to sync between each 
    // worker threads.
    std::atomic<uint64>      mGenerationId;
};

} } } }

#endif /* ZILLIANS_FRAMEWORK_PROCESSORS_KERNELMT_H_ */
