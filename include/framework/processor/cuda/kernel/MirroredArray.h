/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MIRROREDARRAY_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MIRROREDARRAY_H_

#include "core/Prerequisite.h"
#include "core/FragmentFreeAllocator.h"
#include "framework/processor/cuda/kernel/HostArray.h"
#include "framework/processor/cuda/kernel/DeviceArray.h"
#include "framework/processor/cuda/kernel/ArrayHandle.h"

/**
 * Since you don't read/write the data directly through MirroredArray but use
 * ArrayHandle, each ArrayHandle tries to acquire the actual data pointer
 * in the constructor, and release the data pointer in the destructor. So
 * when the MirroredArray goes to destruct itself, the acquire count should
 * equal to zero, which means there's no ArrayHandle instance referencing to
 * it. You may enable the acquire check to track the acquire/release count
 * to debug some memory issue by setting this flag to 1 (true); otherwise
 * set it to 0 (false).
 */
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MIRROREDARRAY_CHECK_ACQUIRE_RELEASE 1

#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MIRROREDARRAY_CHECK_ACQUIRE_RELEASE
#include <tbb/atomic.h>
#endif

namespace zillians { namespace framework { namespace processor { namespace cuda {

/**
 * @brief MirroredArray represent an mirrored host and device array.
 *
 * MirroredArray internally wraps a HostArray and a DeviceArray instance and provides
 * syncHostToDevice() and syncDeviceToHost() to manually synchronize data in-between.
 *
 * @note MirroredArray is a templated class which takes the KernelApi as the template
 * parameter to define the underlying kernel api. So by giving different
 * KernelApi (i.e. RuntimeApi or DriverApi for now, we may add OpenCLApi or
 * more kernels later), you can customize the memory management scheme of the
 * array.
 *
 * @note Usually you don't manipulate the array data through HostArray directly,
 * but instead, you have to use ArrayHandle to obtain the pointer for actual
 * data access.
 *
 * @see RuntimeApi, DriverApi, ArrayHandle, HostArray, DeviceArray
 */
template<typename KernelApi>
class MirroredArray
{
	template<typename ArrayType> friend class ArrayHandle;
public:
	typedef KernelApi kernel;

	MirroredArray(const KernelApi& api) :
		mKernelApi(api),
		mHost(api), mDevice(api), mAllocatedSize(0)
	{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MIRROREDARRAY_CHECK_ACQUIRE_RELEASE
		mAcquireCount = 0;
#endif
	}

	/**
	 * @brief Construct a MirroredArray.
	 *
	 * @param api The underlying kernel reference.
	 * @param size The size of the array in bytes.
	 * @param pagelocked True for page-locked array (for host array). False otherwise.
	 * @param portable True for portable array (see CUDA portable pinned memory) (for host array). False otherwise.
	 * @param writecombined True for write-combined (see CUDA write-combined memory) array (for host array). False otherwise.
	 */
	MirroredArray(const KernelApi& api, std::size_t size, bool pagelocked = false, bool portable = false, bool writecombined = false) :
		mKernelApi(api),
		mHost(api, size, pagelocked, portable, writecombined), mDevice(api, size), mAllocatedSize(size)
	{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MIRROREDARRAY_CHECK_ACQUIRE_RELEASE
		mAcquireCount = 0;
#endif
	}

	/**
	 * @brief Construct a MirroredArray from another MirroredArray (copy constructor).
	 *
	 * When constructing a HostArray instance by using copy constructor,
	 * the actual data of the given referenced instance will be copied.
	 *
	 * @param reference The referenced instance.
	 */
	MirroredArray(const MirroredArray& reference) :
		mKernelApi(reference.mKernelApi),
		mHost(reference.mHost), mDevice(reference.mDevice), mAllocatedSize(reference.mAllocatedSize)
	{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MIRROREDARRAY_CHECK_ACQUIRE_RELEASE
		mAcquireCount = 0;
#endif
	}

	/**
	 * @brief Destruct a MirroredArray.
	 */
	~MirroredArray()
	{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MIRROREDARRAY_CHECK_ACQUIRE_RELEASE
		BOOST_ASSERT(mAcquireCount == 0);
#endif
		mAllocatedSize = 0;
	}

	/**
	 * @brief Get the internal HostArray reference.
	 *
	 * @return The internal HostArray reference.
	 */
	HostArray<KernelApi>&   getHostArray()   { return mHost; }

	/**
	 * @brief Get the internal DeviceArray reference.
	 *
	 * @return The internal DeviceArray reference.
	 */
	DeviceArray<KernelApi>& getDeviceArray() { return mDevice; }

	/**
	 * @brief Copy data from host array to device array.
	 *
	 * @param size The size of the copy.
	 * @param offset The offset the the copy, default is zero.
	 */
	void syncHostToDevice(std::size_t size, std::size_t offset = 0)
	{
		ArrayHandle<DeviceArray<KernelApi> > handleDevice(mDevice, accessLocation::device, accessMode::readwrite);
		ArrayHandle<HostArray<KernelApi> > handleHost(mHost, accessLocation::host, accessMode::read);
		handleHost.copyTo(handleDevice, size, offset, offset);
	}

	/**
	 * @brief Copy data from device array to host array.
	 *
	 * @param size The size of the copy.
	 * @param offset The offset the the copy, default is zero.
	 */
	void syncDeviceToHost(std::size_t size, std::size_t offset = 0)
	{
		ArrayHandle<DeviceArray<KernelApi> > handleDevice(mDevice, accessLocation::device, accessMode::read);
		ArrayHandle<HostArray<KernelApi> > handleHost(mHost, accessLocation::host, accessMode::readwrite);
		handleDevice.copyTo(handleHost, size, offset, offset);
	}

	/**
	 * @brief Return the size of the array.
	 *
	 * @return The size of the array in bytes.
	 */
	inline std::size_t size() const
	{ return mAllocatedSize; }

	friend std::ostream &operator << (std::ostream &stream, const MirroredArray<KernelApi>& object)
	{
		stream << "[ HOST PTR =" << (void*)object.mHost.mData->data() << ", DEVICE PTR = " << (void*)object.mDevice.mData->data() << ", SIZE = " << object.mAllocatedSize << " ]";
		return stream;
	}

//private:
	/**
	 * @brief Acquire the actual data pointer for given access location and access mode.
	 *
	 * @param location The location that we are going to access.
	 * @param mode The read/write mode request for the access.
	 * @param device The GPU device index for device access.
	 * @return The accessible data pointer.
	 */
	inline MutablePointer* acquire(const accessLocation::type location, const accessMode::type mode, std::size_t offset, std::size_t size, uint32 device)
	{
		BOOST_ASSERT(size > 0);
		BOOST_ASSERT(mAllocatedSize >= offset + size);
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MIRROREDARRAY_CHECK_ACQUIRE_RELEASE
		++mAcquireCount;
#endif
		if(location == accessLocation::host)
		{
			return mHost.acquire(location, mode, offset, size, device);
		}
		else
		{
			return mDevice.acquire(location, mode, offset, size, device);
		}
	}

	/**
	 * @brief Release the acquired data pointer.
	 *
	 * @param pointer The pointer to be released.
	 * @param location The access location of the pointer. Must be consistent "location" in acquire().
	 * @param mode The access mode of the pointer, Must be consistent "mode" in acquire().
	 * @param device The GPU device index for device access.
	 * @param dirty The flag to identify the content of the pointer is changed from outside.
	 */
	inline void release(MutablePointer* pointer, const accessLocation::type location, const accessMode::type mode, std::size_t offset, std::size_t size, uint32 device, bool dirty)
	{
		BOOST_ASSERT(size > 0);
		BOOST_ASSERT(mAllocatedSize >= offset + size);
		BOOST_ASSERT(pointer != NULL);
		BOOST_ASSERT(!dirty || (dirty && mode != accessMode::read));

#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MIRROREDARRAY_CHECK_ACQUIRE_RELEASE
		--mAcquireCount;
		BOOST_ASSERT(mAcquireCount >= 0);
#endif
		if(location == accessLocation::host)
		{
			if(dirty)
			{
				syncHostToDevice(mAllocatedSize);
			}
			mHost.release(pointer, location, mode, offset, size, device, dirty);
		}
		else
		{
			if(dirty)
			{
				syncDeviceToHost(mAllocatedSize);
			}
			mDevice.release(pointer, location, mode, offset, size, device, dirty);
		}
	}

	/**
	 * @brief Commit all changes in the acquired data pointer without releasing it.
	 *
	 * @param pointer The pointer acquired.
	 * @param location The access location of the pointer. Must be consistent "location" in acquire().
	 * @param mode The access mode of the pointer, Must be consistent "mode" in acquire().
	 * @param device The GPU device index for device access.
	 * @param dirty The flag to identify the content of the pointer is changed from outside.
	 */
	inline void commit(MutablePointer* pointer, const accessLocation::type location, const accessMode::type mode, std::size_t offset, std::size_t size, uint32 device, bool dirty)
	{
		BOOST_ASSERT(size > 0);
		BOOST_ASSERT(mAllocatedSize >= offset + size);
		BOOST_ASSERT(pointer != NULL);
		BOOST_ASSERT(!dirty || (dirty && mode != accessMode::read));

		if(location == accessLocation::host)
		{
			if(dirty)
			{
				syncHostToDevice(size, offset);
			}
		}
		else
		{
			if(dirty)
			{
				syncDeviceToHost(size, offset);
			}
		}
	}

	/**
	 * @brief Reload the data from the referenced array without re-allocate the buffer.
	 *
	 * @note The given parameters must be the same with those in acquire()
	 *
	 * @param pointer The pointer acquired.
	 * @param location The access location of the pointer. Must be consistent "location" in acquire().
	 * @param mode The access mode of the pointer, Must be consistent "mode" in acquire().
	 * @param device The GPU device index for device access.
	 */
	inline void reload(MutablePointer* pointer, const accessLocation::type location, const accessMode::type mode, std::size_t offset, std::size_t size, uint32 device)
	{
		BOOST_ASSERT(size > 0);
		BOOST_ASSERT(mAllocatedSize >= offset + size);
		BOOST_ASSERT(pointer != NULL);

		if(location == accessLocation::host)
		{
			mHost.reload(pointer, location, mode, offset, size, device);
		}
		else
		{
			mDevice.reload(pointer, location, mode, offset, size, device);
		}
	}

	/// The underlying kernel reference.
	const KernelApi& mKernelApi;

	/// The internal host array.
	HostArray<KernelApi> mHost;

	/// The internal device array.
	DeviceArray<KernelApi> mDevice;

	/// The size of the array.
	std::size_t mAllocatedSize;

#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MIRROREDARRAY_CHECK_ACQUIRE_RELEASE
	/// The counter to keep tract of acquire/release calls
	tbb::atomic<long> mAcquireCount;
#endif

};

} } } }

#endif/*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MIRROREDARRAY_H_*/
