/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDVARIABLE_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDVARIABLE_H_

#include "core/Prerequisite.h"
#include "framework/processor/cuda/kernel/ArrayCommon.h"

namespace zillians { namespace framework { namespace processor { namespace cuda {

template<typename KernelApi, typename T>
class MappedVariable
{
public:
	MappedVariable(KernelApi& api) : mKernelApi(api), mHostPtr(NULL), mDevicePtr(NULL)
	{
		mKernelApi.memory.host.allocateRaw(
				(byte**)&mHostPtr, sizeof(T),
				/*is pagelocked*/ true,
				/*is portable*/ false,
				/*is mapped*/ true,
				/*no writecombine*/ false);

		mKernelApi.memory.host.mapToDeviceRaw((byte**)&mDevicePtr, (byte*)mHostPtr);

		//printf("create host ptr = %p, device ptr = %p\n", mHostPtr, mDevicePtr);
	}

	~MappedVariable()
	{
		//printf("destroy host ptr = %p\n", mHostPtr);
		// TODO fix this memory leak here
		// TODO driver error will be reported in some cases for unknown reason
		//mKernelApi.memory.host.freeRaw((byte*)mHostPtr, /*is pagelocked*/ true);
		SAFE_NULL(mHostPtr); SAFE_NULL(mDevicePtr);
	}

public:
	T* ptr(const accessLocation::type location = accessLocation::device)
	{
		return (location == accessLocation::host) ? mHostPtr : mDevicePtr;
	}

	T get()
	{
		__sync_synchronize();
		return *((volatile T*)mHostPtr);
	}

	void set(const T& v)
	{
		*mHostPtr = v;
		__sync_synchronize();
	}

private:
	KernelApi& mKernelApi;
	T* mHostPtr;
	T* mDevicePtr;
};

} } } }

#endif /* ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDVARIABLE_H_ */
