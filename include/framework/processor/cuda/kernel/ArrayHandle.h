/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_ARRAYHANDLE_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_ARRAYHANDLE_H_

#include "core/Prerequisite.h"
#include "core/FragmentFreeAllocator.h"
#include "framework/processor/cuda/kernel/ArrayCommon.h"
#include <cuda_runtime_api.h>

#ifdef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_ARRAYHANDLE_AUTO_COMMIT_BY_DEFAULT
#define AUTO_COMMIT true
#else
#define AUTO_COMMIT false
#endif

namespace zillians { namespace framework { namespace processor { namespace cuda {

// forward declaration for MappedArray
// (because we have some special handling for mapped array in ArrayHandle::copyImpl())
template<typename KernelApi> class MappedArray;

namespace detail {

extern bool ArrayHandle_DeviceFillConstant8(byte* array, byte* constant, uint elements);
extern bool ArrayHandle_DeviceFillConstant16(byte* array, byte* constant, uint elements);
extern bool ArrayHandle_DeviceFillConstant32(byte* array, byte* constant, uint elements);
extern bool ArrayHandle_DeviceFillConstant64(byte* array, byte* constant, uint elements);
extern bool ArrayHandle_DeviceFillConstant128(byte* array, byte* constant, uint elements);

}

/**
 * @brief ArrayHandle is a utility to access actual CUDA array data.
 *
 * To access a CUDA array, you can construct a CUDA array object (i.e.
 * HostArray, DeviceArray, MappedArray, or MirroredArray) and use the
 * following code to access the actual array data (i.e. to obtain the
 * data pointer to the data)
 *
 * ArrayHandle provides copyTo()/copyFrom() to move data between different
 * array handle specializations. You can pass ArrayHandle<DeviceArray> to
 * ArrayHandle<HostArray>::copyTo() (or any combination among DeviceArray,
 * HostArray, MappedArray, and MirroredArray)
 *
 * When the data in ArrayHandle is modified by fill() or copyTo() or copyFrom(),
 * dirty() is called automatically to flag the ArrayHandle instance so that
 * the data can be committed during the ArrayHandle destruction (or just call
 * commmit() manually)
 *
 * When the data in ArrayHandle is modified by changing the content of data
 * pointer directly, dirty() must be called explicitly by the user to ensure
 * the changed data will be updated to original referenced array object.
 *
 * @code
 * 		// construct the array
 * 		DeviceArray<RuntimeApi> array(api, 1024);
 *
 * 		// obtain the array handle on the host (the accessible data pointer on the host)
 * 		ArrayHandle<DeviceArray<RuntimeApi> > handle_host(array, accessLocation::host);
 *
 *		// ...now you may access "handle_host->data()" on the host...
 *
 * 		// obtain the array handle on the GPU device (the accessible data pointer on the GPU device)
 * 		ArrayHandle<DeviceArray<RuntimeApi> > handle_device(array, accessLocation::device);
 *
 * 		// ...now you may use "handle_device->data()" on the GPU...
 * @endcode
 *
 * @see DeviceArray, HostArray, MappedArray, MirroredArray
 */
template<typename ArrayType>
class ArrayHandle
{
	template<typename AnotherArrayType> friend class ArrayHandle;
public:
	/**
	 * Construct an ArrayHandle instance.
	 *
	 * @param array The referenced array object.
	 * @param location The host/device location which you would like to
	 * access the data.
	 * @param mode The read/write mode for this access.
	 * @param offset The offset acquired for this access. The default is 0.
	 * @param size The size acquired for this access. The default is 0, which means to acquire the rest of the array (from offset).
	 * @param autoCommit Whether or not to enable auto commit (commit changes right after the dirty flag is set)
	 */
	ArrayHandle(ArrayType& array,
			const accessLocation::type location = accessLocation::host, const accessMode::type mode = accessMode::readwrite,
			std::size_t offset = 0, std::size_t size = 0,
			bool autoCommit = AUTO_COMMIT) :
		mArray(array), mLocation(location), mMode(mode), mAutoCommit(autoCommit),
		mOffset(offset), mSize(size == 0 ? mArray.size() - offset : size),
		mData(array.acquire(mLocation, mMode, mOffset, mSize, 0)), mDirtyFlag(false)
	{ }

	/**
	 * Copy constructor to construct an ArrayHandle instance.
	 *
	 * @param ref The copy reference.
	 */
	ArrayHandle(const ArrayHandle<ArrayType>& ref) :
		mArray(ref.mArray), mLocation(ref.mLocation), mMode(ref.mMode), mAutoCommit(ref.mAutoCommit),
		mOffset(ref.mOffset), mSize(ref.mSize),
		mData(mArray.acquire(mLocation, mMode, mOffset, mSize, 0)), mDirtyFlag(false)
	{ }

	/**
	 * Destruct an ArrayHandle instance.
	 */
	~ArrayHandle()
	{
		mArray.release(mData, mLocation, mMode, mOffset, mSize, 0, (mMode == accessMode::read) ? false : mDirtyFlag);
	}

	/**
	 * Get the referenced array.
	 *
	 * @return The referenced array.
	 */
	inline ArrayType& array() const
	{ return mArray; }

	/**
	 * Get the data pointer of type T to the referenced array object.
	 *
	 * @return The data pointer of type T.
	 */
	template<typename T = byte>
	inline T* data() const
	{
		BOOST_ASSERT(mData != NULL);
		return (T*)mData->data();
	}

	template<typename T = byte>
	inline T& at(std::size_t ith)
	{
		BOOST_ASSERT(mData != NULL);
		return ((T*)mData->data())[ith];
	}

	/**
	 * Get the number of elements of type T.
	 *
	 * @return The number of elements of type T.
	 */
	template<typename T = byte>
	inline std::size_t size() const
	{ return mSize / sizeof(T); }

	/**
	 * Mark the data as dirty so the referenced array will be updated
	 * if necessary.
	 */
	inline void dirty()
	{
		BOOST_ASSERT(mMode != accessMode::read);
		mDirtyFlag = true;

		if(mAutoCommit)
		{
			commit();
		}
	}

	/**
	 * Enable or disable auto commit.
	 *
	 * @param autoCommit True to enable auto commit. False otherwise.
	 */
	inline void enableAutoCommit(bool autoCommit)
	{
		mAutoCommit = autoCommit;
	}

	/**
	 * Commit all previous changes to referenced array.
	 *
	 * @note Although ArrayHandle provides an indirect access to the
	 * referenced array object, the access could be still direct, meaning
	 * that changes to the data pointer is committed to memory implicitly
	 * so calling commit() is just to ensure all memory changes have been
	 * written, just like a memory fence.
	 */
	void commit()
	{
		BOOST_ASSERT(mMode != accessMode::read);

		mArray.commit(mData, mLocation, mMode, mOffset, mSize, 0, mDirtyFlag);
		mDirtyFlag = false;
	}

	/**
	 * Reload the data from referenced array.
	 *
	 * @note If there's pending changes waiting to be committed, reload()
	 * would simply discard all of them.
	 */
	void reload()
	{
		mArray.reload(mData, mLocation, mMode, mOffset, mSize, 0);
		mDirtyFlag = false;
	}

	/**
	 * Fill the referenced array with given constant.
	 *
	 * @param constant The given constant. For device pointer, the size
	 * could be 8 to 64bit. For host pointer, there's no limit on the
	 * size.
	 */
	template<typename ElementType>
	inline void fill(const ElementType& constant)
	{
		BOOST_ASSERT(mMode != accessMode::read);

		if(mLocation == accessLocation::host)
		{
			uint32 count = mArray.size() / sizeof(ElementType);
			ElementType *array = (ElementType*)mData->data();

			if(count)
			{
				do
				{
					*array++ = constant;
				} while (--count);
			}
		}
		else
		{
			if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint8) >::value)
			{
				if(!detail::ArrayHandle_DeviceFillConstant8(this->template data<byte>(), (byte*)&constant, mArray.size() / sizeof(ElementType)))
					throw std::runtime_error("failed to fill array");
			}
			else if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint16) >::value)
			{
				if(!detail::ArrayHandle_DeviceFillConstant16(this->template data<byte>(), (byte*)&constant, mArray.size() / sizeof(ElementType)))
					throw std::runtime_error("failed to fill array");
			}
			else if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint32) >::value)
			{
				if(!detail::ArrayHandle_DeviceFillConstant32(this->template data<byte>(), (byte*)&constant, mArray.size() / sizeof(ElementType)))
					throw std::runtime_error("failed to fill array");
			}
			else if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint64) >::value)
			{
				if(!detail::ArrayHandle_DeviceFillConstant64(this->template data<byte>(), (byte*)&constant, mArray.size() / sizeof(ElementType)))
					throw std::runtime_error("failed to fill array");
			}
			else if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint4) >::value)
			{
				if(!detail::ArrayHandle_DeviceFillConstant128(this->template data<byte>(), (byte*)&constant, mArray.size() / sizeof(ElementType)))
					throw std::runtime_error("failed to fill array");
			}
			else
			{
				throw std::invalid_argument("invalid compact element size requested");
			}
		}

		dirty();
	}

	inline void copyFromRaw(const byte* src, std::size_t size, const accessLocation::type src_location, std::size_t dest_offset = 0, std::size_t src_offset = 0)
	{
		typedef typename ArrayType::kernel KernelApi;
		typename KernelApi::location::type destLocation = (mLocation == accessLocation::host) ? KernelApi::location::host : KernelApi::location::device;
		typename KernelApi::location::type srcLocation  = (src_location == accessLocation::host) ? KernelApi::location::host : KernelApi::location::device;
		mArray.mKernelApi.memory.copyRaw(data<byte>(), src, dest_offset, src_offset, size, destLocation, srcLocation);
		dirty();
	}

	inline void copyToRaw(byte* dest, std::size_t size, const accessLocation::type dest_location, std::size_t dest_offset = 0, std::size_t src_offset = 0)
	{
		typedef typename ArrayType::kernel KernelApi;
		typename KernelApi::location::type destLocation = (dest_location == accessLocation::host) ? KernelApi::location::host : KernelApi::location::device;
		typename KernelApi::location::type srcLocation  = (mLocation == accessLocation::host) ? KernelApi::location::host : KernelApi::location::device;
		mArray.mKernelApi.memory.copyRaw(dest, data<byte>(), dest_offset, src_offset, size, destLocation, srcLocation);
	}

	/**
	 * Copy from another ArrayHandle instance.
	 *
	 * @param src Another ArrayHandle instance (as the source of the copy.)
	 * @param size The number of bytes to copy.
	 * @param src_offset The offset of the source data pointer.
	 * @param dest_offset The offset of the destination (myself) data pointer.
	 */
	template<typename Source>
	inline void copyFrom(ArrayHandle<Source>& src, std::size_t size, std::size_t dest_offset = 0, std::size_t src_offset = 0)
	{
		BOOST_ASSERT(mMode != accessMode::read);

		copyImpl<ArrayType, Source>(*this, src, size, dest_offset, src_offset);
	}

	/**
	 * Copy to another ArrayHandle instance.
	 *
	 * @param dest Another ArrayHandle instance (as the destination of the copy.)
	 * @param size The number of bytes to copy.
	 * @param src_offset The offset of the source (myself) data pointer.
	 * @param dest_offset The offset of the destination data pointer.
	 */
	template<typename Destination>
	inline void copyTo(ArrayHandle<Destination>& dest, std::size_t size, std::size_t dest_offset = 0, std::size_t src_offset = 0)
	{
		BOOST_ASSERT(dest.mMode != accessMode::read);

		copyImpl<Destination, ArrayType>(dest, *this, size, dest_offset, src_offset);
	}

private:
	template<typename Destination, typename Source>
	inline static void copyImpl(ArrayHandle<Destination>& dest, ArrayHandle<Source>& src, std::size_t size, std::size_t dest_offset, std::size_t src_offset)
	{
		BOOST_ASSERT(dest.mSize >= size + dest_offset);
		BOOST_ASSERT(src.mSize >= size + src_offset);

		typedef typename Destination::kernel KernelApi;
		const KernelApi& api = dest.mArray.mKernelApi;

		MutablePointer* destPointer;
		typename KernelApi::location::type destLocation;
		bool destPointerRequireRelease = false;

		MutablePointer* srcPointer;
		typename KernelApi::location::type srcLocation;
		bool srcPointerRequireRelease = false;

		// static assertion to check whether the source and destination have same kernel api
		//BOOST_MPL_ASSERT((is_same<Destination::kernel, Source::kernel>));

		// for mapped array, the mapped pointer can't be used in conjunction with cudaMemcpy(), so we must use the host pointer instead.
		if( boost::is_same<Destination, MappedArray<KernelApi> >::value )
		{
			destPointer = dest.mArray.acquire(accessLocation::host, accessMode::readwrite, 0, 0, 0);
			destLocation = KernelApi::location::host;
			destPointerRequireRelease = true;
		}
		else
		{
			destPointer = dest.mData;
			destLocation = (dest.mLocation == accessLocation::host) ? KernelApi::location::host : KernelApi::location::device;
			destPointerRequireRelease = false;
		}

		// for mapped array, the mapped pointer can't be used in conjunction with cudaMemcpy(), so we must use the host pointer instead.
		if( boost::is_same<Source, MappedArray<KernelApi> >::value )
		{
			srcPointer = src.mArray.acquire(accessLocation::host, accessMode::read, 0, 0, 0);
			srcLocation = KernelApi::location::host;
			srcPointerRequireRelease = true;
		}
		else
		{
			srcPointer = src.mData;
			srcLocation = (src.mLocation == accessLocation::host) ? KernelApi::location::host : KernelApi::location::device;
			srcPointerRequireRelease = false;
		}

		// perform copy through kernal api
		api.memory.copy(destPointer, srcPointer, dest_offset, src_offset, size, destLocation, srcLocation);

		// for mapped array, as we acquire the array before copying the actual data, the acquired pointer must be released in the end
		if(destPointerRequireRelease)
		{
			dest.mArray.release(destPointer, accessLocation::host, accessMode::readwrite, 0, 0, 0, true);
		}

		// for mapped array, as we acquire the array before copying the actual data, the acquired pointer must be released in the end
		if(srcPointerRequireRelease)
		{
			src.mArray.release(srcPointer, accessLocation::host, accessMode::read, 0, 0, 0, false);
		}

		// mark the destination handle as dirty
		dest.dirty();
	}

	ArrayType& mArray;
	const accessLocation::type mLocation;
	const accessMode::type mMode;
	std::size_t mOffset;
	std::size_t mSize;
	bool mAutoCommit;
	MutablePointer* mData;
	bool mDirtyFlag;
};

} } } }

#undef AUTO_COMMIT

#endif/*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_ARRAYHANDLE_H_*/
