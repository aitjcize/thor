/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_H_

//#include "core/Prerequisite.h"
#include "core/FragmentFreeAllocator.h"
#include "framework/processor/cuda/kernel/ArrayCommon.h"

/**
 * The underlying KernelApi may provide pooled allocation, that is, to
 * allocate from pool instead of the runtime memory management. So, to enable
 * the pooled allocation support of all HostArray instances, you have to
 * set the flag to 1 (true); otherwise set it to 0 (false).
 */
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_ENABLE_POOLED_ALLOCATION 0

/**
 * Since you don't read/write the data directly through HostArray but use
 * ArrayHandle, each ArrayHandle tries to acquire the actual data pointer
 * in the constructor, and release the data pointer in the destructor. So
 * when the HostArray goes to destruct itself, the acquire count should
 * equal to zero, which means there's no ArrayHandle instance referencing to
 * it. You may enable the acquire check to track the acquire/release count
 * to debug some memory issue by setting this flag to 1 (true); otherwise
 * set it to 0 (false).
 */
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_CHECK_ACQUIRE_RELEASE 1

#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_CHECK_ACQUIRE_RELEASE
#include <tbb/atomic.h>
#endif

namespace zillians { namespace framework { namespace processor { namespace cuda {

// forward declaration for MirroredArray
template<typename KernelApi> class MirroredArray;

/**
 * @brief HostArray represent an array on the host (CPU)
 *
 * @note HostArray is a templated class which takes the KernelApi as the template
 * parameter to define the underlying kernel api. So by giving different
 * KernelApi (i.e. RuntimeApi or DriverApi for now, we may add OpenCLApi or
 * more kernels later), you can customize the memory management scheme of the
 * array.
 *
 * @note Usually you don't manipulate the array data through HostArray directly,
 * but instead, you have to use ArrayHandle to obtain the pointer for actual
 * data access.
 *
 * @see RuntimeApi, DriverApi, ArrayHandle
 */
template<typename KernelApi>
class HostArray
{
	template<typename ArrayType> friend class ArrayHandle;
	friend class MirroredArray<KernelApi>;
public:
	typedef KernelApi kernel;

	/**
	 * @brief Construct a HostArray.
	 *
	 * @param api The underlying kernel reference.
	 * @param size The size of the array in bytes.
	 * @param pagelocked True for page-locked array. False otherwise.
	 * @param portable True for portable array (see CUDA portable pinned memory). False otherwise.
	 * @param writecombined True for write-combined (see CUDA write-combined memory) array. False otherwise.
	 */
	HostArray(const KernelApi& api, std::size_t size, bool pagelocked = false, bool portable = false, bool writecombined = false) :
		mKernelApi(api),
		mDataOwner(true), mData(NULL), mAllocatedSize(size),
		mPageLocked(pagelocked), mPortable(portable), mWriteCombined(writecombined)
	{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_CHECK_ACQUIRE_RELEASE
		mAcquireCount = 0;
#endif

#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_ENABLE_POOLED_ALLOCATION
		mKernelApi.memory.host.allocate(&mData, mAllocatedSize, true, mPageLocked, mPortable, false, mWriteCombined);
#else
		mKernelApi.memory.host.allocate(&mData, mAllocatedSize, false, mPageLocked, mPortable, false, mWriteCombined);
#endif
	}

	/**
	 * @brief Construct a HostArray from another HostArray (copy constructor).
	 *
	 * When constructing a HostArray instance by using copy constructor,
	 * the actual data of the given referenced instance will be copied.
	 *
	 * @param reference The referenced instance.
	 */
	HostArray(const HostArray<KernelApi>& reference) :
		mKernelApi(reference.mKernelApi),
		mDataOwner(true), mData(NULL), mAllocatedSize(reference.mAllocatedSize),
		mPageLocked(reference.mPageLocked), mPortable(reference.mPortable), mWriteCombined(reference.mWriteCombined)
	{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_CHECK_ACQUIRE_RELEASE
		mAcquireCount = 0;
#endif

#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_ENABLE_POOLED_ALLOCATION
		mKernelApi.memory.host.allocate(&mData, mAllocatedSize, true, mPageLocked, mPortable, false, mWriteCombined);
#else
		mKernelApi.memory.host.allocate(&mData, mAllocatedSize, false, mPageLocked, mPortable, false, mWriteCombined);
#endif
		mKernelApi.memory.copy(mData, reference.mData, 0, 0, mAllocatedSize, KernelApi::location::host, KernelApi::location::host);
	}

	/**
	 * @brief Construct a HostArray from foreign pointer.
	 *
	 * @param reference The referenced instance.
	 */
	HostArray(const KernelApi& api, byte* data, std::size_t size, bool pagelocked = false, bool portable = false, bool writecombined = false) :
		mKernelApi(api),
		mDataOwner(false), mData(new MutablePointer(data)), mAllocatedSize(size),
		mPageLocked(pagelocked), mPortable(portable), mWriteCombined(writecombined)
	{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_DEVICEARRAY_CHECK_ACQUIRE_RELEASE
		mAcquireCount = 0;
#endif
	}

	/**
	 * @brief Destruct a HostArray.
	 */
	~HostArray()
	{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_CHECK_ACQUIRE_RELEASE
		BOOST_ASSERT(mAcquireCount == 0);
#endif
		if(mDataOwner)
		{
			if(mData)
			{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_ENABLE_POOLED_ALLOCATION
				mKernelApi.memory.host.free(mData, true, mPageLocked, mPortable, false, mWriteCombined);
#else
				mKernelApi.memory.host.free(mData, false, mPageLocked, mPortable, false, mWriteCombined);
#endif
				SAFE_NULL(mData);
			}
		}
		else
		{
			SAFE_DELETE(mData);
		}
		mAllocatedSize = 0;
	}

	/**
	 * @brief Return the size of the array.
	 *
	 * @return The size of the array in bytes.
	 */
	inline std::size_t size() const
	{ return mAllocatedSize; }

	friend std::ostream &operator << (std::ostream &stream, const HostArray<KernelApi>& object)
	{
		stream << "[ HOST PTR =" << (void*)object.mData->data() << ", SIZE = " << object.mAllocatedSize << " ]";
		return stream;
	}
//private:
	/**
	 * @brief Acquire the actual data pointer for given access location and access mode.
	 *
	 * @param location The location that we are going to access.
	 * @param mode The read/write mode request for the access.
	 * @param device The GPU device index for device access.
	 * @return The accessible data pointer.
	 */
	inline MutablePointer* acquire(const accessLocation::type location, const accessMode::type mode, std::size_t offset, std::size_t size, uint32 device)
	{
		BOOST_ASSERT(size > 0);
		BOOST_ASSERT(mAllocatedSize >= offset + size);
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_CHECK_ACQUIRE_RELEASE
		++mAcquireCount;
#endif
		if(location == accessLocation::host)
		{
			return mData;
		}
		else
		{
			MutablePointer* pointer = NULL;
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_ENABLE_POOLED_ALLOCATION
			mKernelApi.memory.device.allocate(&pointer, size, true);
#else
			mKernelApi.memory.device.allocate(&pointer, size, false);
#endif
			BOOST_ASSERT(pointer != NULL);
			mKernelApi.memory.copy(pointer, mData, 0, offset, size, KernelApi::location::device, KernelApi::location::host);
			return pointer;
		}
	}

	/**
	 * @brief Release the acquired data pointer.
	 *
	 * @param pointer The pointer to be released.
	 * @param location The access location of the pointer. Must be consistent "location" in acquire().
	 * @param mode The access mode of the pointer, Must be consistent "mode" in acquire().
	 * @param device The GPU device index for device access.
	 * @param dirty The flag to identify the content of the pointer is changed from outside.
	 */
	inline void release(MutablePointer* pointer, const accessLocation::type location, const accessMode::type mode, std::size_t offset, std::size_t size, uint32 device, bool dirty)
	{
		BOOST_ASSERT(size > 0);
		BOOST_ASSERT(mAllocatedSize >= offset + size);
		BOOST_ASSERT(pointer != NULL);
		BOOST_ASSERT(!dirty || (dirty && mode != accessMode::read));

#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_CHECK_ACQUIRE_RELEASE
		--mAcquireCount;
		BOOST_ASSERT(mAcquireCount >= 0);
#endif
		if(location == accessLocation::host)
		{
			// DO NOTHING
		}
		else
		{
			// if the acquired device array is modified (dirty), update the host array
			if(dirty)
			{
				mKernelApi.memory.copy(mData, pointer, offset, 0, size, KernelApi::location::host, KernelApi::location::device);
			}
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_ENABLE_POOLED_ALLOCATION
			mKernelApi.memory.device.free(pointer, true);
#else
			mKernelApi.memory.device.free(pointer, false);
#endif
		}
	}

	/**
	 * @brief Commit all changes in the acquired data pointer without releasing it.
	 *
	 * @param pointer The pointer acquired.
	 * @param location The access location of the pointer. Must be consistent "location" in acquire().
	 * @param mode The access mode of the pointer, Must be consistent "mode" in acquire().
	 * @param device The GPU device index for device access.
	 * @param dirty The flag to identify the content of the pointer is changed from outside.
	 */
	inline void commit(MutablePointer* pointer, const accessLocation::type location, const accessMode::type mode, std::size_t offset, std::size_t size, uint32 device, bool dirty)
	{
		BOOST_ASSERT(size > 0);
		BOOST_ASSERT(mAllocatedSize >= offset + size);
		BOOST_ASSERT(pointer != NULL);
		BOOST_ASSERT(!dirty || (dirty && mode != accessMode::read));

		if(location == accessLocation::host)
		{
			// DO NOTHING
		}
		else
		{
			// if the acquired device array is modified (dirty), update the host array
			if(dirty)
			{
				mKernelApi.memory.copy(mData, pointer, offset, 0, size, KernelApi::location::host, KernelApi::location::device);
			}
		}
	}

	/**
	 * @brief Reload the data from the referenced array without re-allocate the buffer.
	 *
	 * @note The given parameters must be the same with those in acquire()
	 *
	 * @param pointer The pointer acquired.
	 * @param location The access location of the pointer. Must be consistent "location" in acquire().
	 * @param mode The access mode of the pointer, Must be consistent "mode" in acquire().
	 * @param device The GPU device index for device access.
	 */
	inline void reload(MutablePointer* pointer, const accessLocation::type location, const accessMode::type mode, std::size_t offset, std::size_t size, uint32 device)
	{
		BOOST_ASSERT(size > 0);
		BOOST_ASSERT(mAllocatedSize >= offset + size);
		BOOST_ASSERT(pointer != NULL);

		if(location == accessLocation::host)
		{
		}
		else
		{
			mKernelApi.memory.copy(pointer, mData, 0, offset, size, KernelApi::location::device, KernelApi::location::host);
		}
	}

	/// The underlying kernel reference.
	const KernelApi& mKernelApi;

	/// Flag to indicate whether the array itself owns the pointer
	bool mDataOwner;

	/// The actual data pointer.
	MutablePointer* mData;

	/// The size of the array.
	std::size_t mAllocatedSize;

	/// True for page-locked array. False otherwise.
	bool mPageLocked;

	/// True for portable array (see CUDA portable pinned memory). False otherwise.
	bool mPortable;

	/// True for write-combined (see CUDA write-combined memory) array. False otherwise.
	bool mWriteCombined;

#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_CHECK_ACQUIRE_RELEASE
	/// The counter to keep tract of acquire/release calls
	tbb::atomic<long> mAcquireCount;
#endif
};

} } } }

#endif/*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HOSTARRAY_H_*/
