
/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_KERNELAPI_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_KERNELAPI_H_

#include "core/Prerequisite.h"
#include "core/FragmentFreeAllocator.h"
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <driver_types.h>
#include <driver_functions.h>
#include <channel_descriptor.h>
#include <vector_functions.h>

namespace zillians { namespace framework { namespace processor { namespace cuda {

/**
 * RuntimeApi provides an wrapped interface for CUDA Runtime API.
 */
struct KernelApi
{
	KernelApi();
	~KernelApi();

	bool good();
	bool initialize();
	void finalize();

	typedef cudaEvent_t event_type;

	typedef cudaStream_t stream_type;
	const static stream_type default_stream;

	struct location { enum type { host, device }; };
	struct scheduling { enum type { scheduleAuto, scheduleSpin, scheduleYield }; };

	static void throwOnError(const cudaError_t& error);

	struct DeviceManagement
	{
		int countAllDevices() const;
		void use(int dev) const;
		int current() const;
		inline void enableMappedMemory() const;

		struct Properties
		{
			void probe(int dev) const;
			std::string name() const;
			std::size_t globalMemorySize() const;
			std::size_t sharedMemorySizePerBlock() const;
			std::size_t registerMemorySizePerBlock() const;
			std::size_t constantMemorySize() const;
			int warpSize() const;
			int maxThreadsPerBlock() const;
			int maxBlockDimX() const;
			int maxBlockDimY() const;
			int maxBlockDimZ() const;
			int maxGridDimX() const;
			int maxGridDimY() const;
			int maxGridDimZ() const;
			int clockRate() const;
			std::pair<int,int> driverVersion() const;
			std::pair<int,int> runtimeVersion() const;
			std::pair<int,int> isaVersion() const;
			std::pair<int,int> computeCapability() const;
			bool supportOverlappedExecution() const;
			int numMultiProcessors() const;
			int numCorePerMultiProcessor() const;
			bool hasKernelExecutionTimeLimit() const;
			bool supportMappedMemory() const;
			bool supportECCMemory() const;
			bool supportConcurrentExecution() const;

			mutable cudaDeviceProp prop;
			mutable int propDriverVersion;
			mutable int propRuntimeVersion;
		} properties;
	} device;

	struct EventManagement
	{
		void create(event_type* e) const;
		void destroy(const event_type& e) const;
		void record(const event_type& e, const stream_type& s = default_stream) const;
		bool queryCompleted(const event_type& e) const;
		void synchronize(const event_type& e) const;
		float queryElapsed(const event_type& start, const event_type& stop) const;
	} event;

	struct StreamManagement
	{
		void create(stream_type* s) const;
		void destroy(const stream_type& s) const;
		bool queryCompleted(const stream_type& s) const;
		void synchronize(const stream_type& s) const;
	} stream;
//
//	struct Texture
//	{
//		cudaArray* array;
//		uint8 dimension;
//		cudaChannelFormatDesc desc;
//		cudaExtent extent;
//		std::size_t element_size;
//		bool binded;
//		const textureReference* texref;
//
//		inline std::size_t width()  const { return extent.width;  }
//		inline std::size_t height() const { return extent.height; }
//		inline std::size_t depth()  const { return extent.depth;  }
//
//		inline bool unbind()
//		{
//			if(binded)
//			{
//				binded = false;
//
//				cudaError_t error = cudaUnbindTexture(texref);
//
//				texref = NULL;
//				throwOnError(error);
//
//				return true;
//			}
//
//			return false;
//		}
//
//		inline bool bind(const char* name)
//		{
//			cudaError_t error = cudaGetTextureReference(&texref, name);
//			throwOnError(error);
//
//			error = cudaBindTextureToArray(texref, array, &desc);
//			throwOnError(error);
//
//			binded = true;
//
//			return true;
//		}
//
//		inline void copyFrom(MutablePointer* src, std::size_t src_offset, std::size_t size, const location::type& src_type)
//		{
//			copyFromRaw(src->data(), src_offset, size, src_type);
//		}
//
//		inline void copyFromRaw(byte* src, std::size_t src_offset, std::size_t size, const location::type& src_type)
//		{
//			BOOST_ASSERT(src != NULL);
//			BOOST_ASSERT(size > 0 && size <= extent.width * extent.height * extent.depth * element_size);
//			cudaMemcpyKind kind;
//			cudaError_t error;
//
//			// find out the copy type by the source and destination memory location
//			if(src_type == location::host)
//			{
//				kind = cudaMemcpyHostToDevice;
//			}
//			else // src_type == location::device
//			{
//				kind = cudaMemcpyDeviceToDevice;
//			}
//
//			if(dimension == 1)
//			{
//				error = cudaMemcpyToArray(array, 0, 0, src + src_offset, size, kind);
//				throwOnError(error);
//			}
//			else if(dimension == 2)
//			{
//				error = cudaMemcpyToArray(array, 0, 0, src + src_offset, size, kind);
//				throwOnError(error);
//			}
//			else
//			{
//				cudaMemcpy3DParms param = { 0 };
//			    param.dstArray = array;
//			    param.dstPos = make_cudaPos(0,0,0);
//			    param.srcPtr = make_cudaPitchedPtr((void*)src, extent.width * element_size, extent.width, extent.height);
//			    param.srcPos = make_cudaPos(0,0,0);
//			    param.kind = kind;
//			    param.extent = extent;
//
//				error = cudaMemcpy3D(&param);
//				throwOnError(error);
//			}
//		}
//
//		inline void copyTo(MutablePointer* dest, std::size_t dest_offset, std::size_t size, const location::type& dest_type)
//		{
//			copyToRaw(dest->data(), dest_offset, size, dest_type);
//		}
//
//		inline void copyToRaw(byte* dest, std::size_t dest_offset, std::size_t size, const location::type& dest_type)
//		{
//			BOOST_ASSERT(dest != NULL);
//			BOOST_ASSERT(size > 0 && size <= extent.width * extent.height * extent.depth * element_size);
//			cudaMemcpyKind kind;
//			cudaError_t error;
//
//			// find out the copy type by the source and destination memory location
//			if(dest_type == location::host)
//			{
//				kind = cudaMemcpyDeviceToHost;
//			}
//			else // dest_type == location::device
//			{
//				kind = cudaMemcpyDeviceToDevice;
//			}
//
//			if(dimension == 1)
//			{
//				error = cudaMemcpyFromArray(dest + dest_offset, array, 0, 0, size, kind);
//				throwOnError(error);
//			}
//			else if(dimension == 2)
//			{
//				error = cudaMemcpyFromArray(dest + dest_offset, array, 0, 0, size, kind);
//				throwOnError(error);
//			}
//			else
//			{
//				cudaMemcpy3DParms param = { 0 };
//				param.dstPtr = make_cudaPitchedPtr((void*)dest, extent.width * element_size, extent.width, extent.height);;
//			    param.dstPos = make_cudaPos(0,0,0);
//			    param.srcArray = array;
//			    param.srcPos = make_cudaPos(0,0,0);
//			    param.kind = kind;
//			    param.extent = extent;
//
//				error = cudaMemcpy3D(&param);
//				throwOnError(error);
//			}
//		}
//	};
//
//	struct TextureManagement
//	{
//		template <typename T>
//		inline void create1D(std::size_t width, Texture** texture) const
//		{
//			BOOST_ASSERT(texture != NULL);
//
//			*texture = new Texture;
//
//			(*texture)->dimension = 1;
//			(*texture)->desc = cudaCreateChannelDesc<T>();
//			(*texture)->extent.width  = width;
//			(*texture)->extent.height = 1;
//			(*texture)->extent.depth  = 1;
//			(*texture)->element_size  = sizeof(T);
//			(*texture)->binded        = false;
//
//			cudaError_t error = cudaMallocArray(&((*texture)->array), &((*texture)->desc), width, 1);
//			if(error != cudaSuccess)
//			{
//				SAFE_DELETE(*texture);
//				throwOnError(error);
//			}
//		}
//
//		template <typename T>
//		inline void create2D(std::size_t width, std::size_t height, Texture** texture) const
//		{
//			BOOST_ASSERT(texture != NULL);
//
//			*texture = new Texture;
//
//			(*texture)->dimension = 2;
//			(*texture)->desc = cudaCreateChannelDesc<T>();
//			(*texture)->extent.width  = width;
//			(*texture)->extent.height = height;
//			(*texture)->extent.depth  = 1;
//			(*texture)->element_size  = sizeof(T);
//			(*texture)->binded        = false;
//
//			cudaError_t error = cudaMallocArray(&((*texture)->array), &((*texture)->desc), width, height);
//			if(error != cudaSuccess)
//			{
//				SAFE_DELETE(*texture);
//				throwOnError(error);
//			}
//		}
//
//		template <typename T>
//		inline void create3D(std::size_t width, std::size_t height, std::size_t depth, Texture** texture) const
//		{
//			*texture = new Texture;
//
//			(*texture)->dimension = 3;
//			(*texture)->desc = cudaCreateChannelDesc<T>();
//			(*texture)->extent.width  = width;
//			(*texture)->extent.height = height;
//			(*texture)->extent.depth  = depth;
//			(*texture)->element_size  = sizeof(T);
//			(*texture)->binded        = false;
//
//			cudaError_t error = cudaMalloc3DArray(&((*texture)->array), &((*texture)->desc), (*texture)->extent);
//			if(error != cudaSuccess)
//			{
//				SAFE_DELETE(*texture);
//				throwOnError(error);
//			}
//		}
//
//		inline void destroy(Texture* texture) const
//		{
//			BOOST_ASSERT(texture != NULL);
//
//			cudaError_t error = cudaFreeArray(texture->array);
//			SAFE_DELETE(texture);
//
//			if(error != cudaSuccess)
//			{
//				throwOnError(error);
//			}
//		}
//
//		template <typename T>
//		inline size_t bind1D(const char* name, MutablePointer* devicePointer, std::size_t size)
//		{
//			return bind1D<T>(name, devicePointer->data(), size);
//		}
//
//		template <typename T>
//		inline size_t bind1D(const char* name, byte* devicePointerRaw, std::size_t size)
//		{
//			const textureReference* texref;
//			cudaError_t error = cudaGetTextureReference(&texref, name);
//			throwOnError(error);
//
//			cudaChannelFormatDesc desc = cudaCreateChannelDesc<T>();
//
//			std::size_t offset = 0;
//			error = cudaBindTexture(&offset, texref, devicePointerRaw, &desc, size);
//			throwOnError(error);
//
//			return offset;
//		}
//
//	} texture;
//
	struct MemoryManagement
	{
		struct HostMemoryManagement
		{
			HostMemoryManagement();
			~HostMemoryManagement();
			void reserve(std::size_t size, bool pagelocked = false, bool portable = false, bool mapped = false, bool writecombined = false) const;
			void allocate(MutablePointer** pointer, std::size_t size, bool from_reserved = false, bool pagelocked = false, bool portable = false, bool mapped = false, bool writecombined = false) const;
			void allocateRaw(byte** pointer, std::size_t size, bool pagelocked = false, bool portable = false, bool mapped = false, bool writecombined = false) const;
			void free(MutablePointer* pointer, bool from_reserved = false, bool pagelocked = false, bool portable = false, bool mapped = false, bool writecombined = false) const;
			void freeRaw(byte* pointer, bool pagelocked = false) const;
			void set(byte* data, int value, std::size_t n) const;
			void mapToDevice(MutablePointer** device, MutablePointer* host) const;
			void mapToDeviceRaw(byte** device, byte* host) const;
		private:
			uint8 keyForReserved(bool pagelocked, bool portable, bool mapped, bool writecombined) const;

			mutable FragmentAllocator* allocators[9];
			mutable byte* reservedMemories[9];
		} host;

		struct DeviceMemoryManagement
		{
			DeviceMemoryManagement();
			~DeviceMemoryManagement();
			void reserve(std::size_t size) const;
			void allocate(MutablePointer** pointer, std::size_t size, bool from_reserved = false) const;
			void allocateRaw(byte** pointer, std::size_t size) const;
			void free(MutablePointer* pointer, bool from_reserved = false) const;
			void freeRaw(byte* pointer) const;
			void set(MutablePointer* pointer, int value, std::size_t n) const;
			void setRaw(byte* pointer, int value, std::size_t n) const;

		private:
			mutable FragmentAllocator* allocator;
			mutable byte* reservedMemory;
		} device;

		template<typename T>
		inline void copyToSymbol(const char* symbol_name, T& src) const
		{
			copyToSymbolImpl(symbol_name, &src, sizeof(T));
		}

		template<typename T>
		inline void copyToSymbolAsync(const char* symbol_name, T& src, const stream_type& stream) const
		{
			copyToSymbolAsyncImpl(symbol_name, &src, sizeof(T), stream);
		}

		void copyToSymbolImpl(const char* symbol_name, const void* src, std::size_t size) const;
		void copyToSymbolAsyncImpl(const char* symbol_name, const void* src, std::size_t size, const stream_type& stream) const;
		void copy(MutablePointer* dest, MutablePointer* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type) const;
		void copyRaw(byte* dest, const byte* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type) const;
		void copyAsync(MutablePointer* dest, MutablePointer* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type, const stream_type& stream) const;
		void copyAsyncRaw(byte* dest, const byte* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type, const stream_type& stream) const;
	} memory;

	struct ExecutionManagement
	{
		void synchronize() const;
		void configureKernel(const dim3& grid, const dim3& block, std::size_t shared_memory = 0, const stream_type& s = default_stream) const;
		void setupParameter(const byte* args, std::size_t size, std::size_t offset) const;
		void launchKernel(const void* entry) const;

		struct FunctionTraits
		{
			void probe(const char* name) const;
			std::size_t constantMemorySize() const;
			std::size_t sharedMemorySize() const;
			std::size_t localMemorySize() const;
			int maxThreadsPerBlock() const;
			int registerUsed() const;

			mutable cudaFuncAttributes attr;
		} function_traits;
	} execution;
	void* cuda_lib_handle;
	void* cuda_rt_lib_handle;
};

extern KernelApi& GetKernelApi();

} } } }

#endif/*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_KERNELAPI_H_*/
