/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_DRIVERAPI_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_DRIVERAPI_H_

#include "core/Prerequisite.h"
#include "core/FragmentFreeAllocator.h"

#define IGNORE_CUDA_SPECIFIER_ENABLE 1
#include "framework/processor/cuda/CudaSpecifiers.h"
#undef IGNORE_CUDA_SPECIFIER_ENABLE

#include <cuda.h>
#include <cuda_runtime_api.h>
#include <driver_types.h>
#include <driver_functions.h>
#include <channel_descriptor.h>

//#pragma GCC diagnostic ignored "-Wall"
#pragma GCC system_header

//#define ENABLE_CUDA_POINTER_64BIT

namespace zillians { namespace framework { namespace processor {

/**
 * DriverApi provides an wrapped interface for CUDA Driver API.
 */
struct DriverApi
{
	struct {

	};
	typedef CUevent event_type;

	typedef CUstream stream_type;
	const static stream_type default_stream;

	typedef CUmodule module_type;
	typedef CUfunction function_type;

	struct location { enum type { host, device }; };
	struct scheduling { enum type { scheduleAuto, scheduleSpin, scheduleYield }; };

	struct jit_option { enum type { max_register, thread_per_block, compilation_time, optimization_level, target_from_current_context, target }; };
	struct jit_target { enum type { target_compute_10, target_compute_11, target_compute_12, target_compute_13, target_compute_20 }; };
	struct jit_value  { union type { unsigned int ui; float f; jit_target::type t; }; };

	struct jit_options
	{
		typedef std::vector<std::pair<jit_option::type, jit_value::type> > type;
		static std::pair<jit_option::type, jit_value::type> create(jit_option::type option, unsigned int ui)
		{
			jit_value::type value; value.ui = ui;
			return std::make_pair(option, value);
		}
		static std::pair<jit_option::type, jit_value::type> create(jit_option::type option, float f)
		{
			jit_value::type value; value.f = f;
			return std::make_pair(option, value);
		}
		static std::pair<jit_option::type, jit_value::type> create(jit_option::type option, jit_target::type t)
		{
			jit_value::type value; value.t = t;
			return std::make_pair(option, value);
		}
	};

	static void throwOnError(const CUresult& error)
	{
		if(error != CUDA_SUCCESS)
		{
			switch(error)
			{
			case CUDA_ERROR_INVALID_VALUE:
				throw std::runtime_error("Invalid value");
			case CUDA_ERROR_OUT_OF_MEMORY:
				throw std::runtime_error("Out of memory");
			case CUDA_ERROR_NOT_INITIALIZED:
				throw std::runtime_error("Driver not initialized");
			case CUDA_ERROR_DEINITIALIZED:
				throw std::runtime_error("Driver deinitialized");
			case CUDA_ERROR_NO_DEVICE:
				throw std::runtime_error("No CUDA-capable device available");
			case CUDA_ERROR_INVALID_DEVICE:
				throw std::runtime_error("Invalid device");
			case CUDA_ERROR_INVALID_IMAGE:
				throw std::runtime_error("Invalid kernel image");
			case CUDA_ERROR_INVALID_CONTEXT:
				throw std::runtime_error("Invalid context");
			case CUDA_ERROR_CONTEXT_ALREADY_CURRENT:
				throw std::runtime_error("Context already current");
			case CUDA_ERROR_MAP_FAILED:
				throw std::runtime_error("Map failed");
			case CUDA_ERROR_UNMAP_FAILED:
				throw std::runtime_error("Unmap failed");
			case CUDA_ERROR_ARRAY_IS_MAPPED:
				throw std::runtime_error("Array is mapped");
			case CUDA_ERROR_ALREADY_MAPPED:
				throw std::runtime_error("Already mapped");
			case CUDA_ERROR_NO_BINARY_FOR_GPU:
				throw std::runtime_error("No binary for GPU");
			case CUDA_ERROR_ALREADY_ACQUIRED:
				throw std::runtime_error("Already acquired");
			case CUDA_ERROR_NOT_MAPPED:
				throw std::runtime_error("Not mapped");
			case CUDA_ERROR_INVALID_SOURCE:
				throw std::runtime_error("Invalid source");
			case CUDA_ERROR_FILE_NOT_FOUND:
				throw std::runtime_error("File not found");
			case CUDA_ERROR_INVALID_HANDLE:
				throw std::runtime_error("Invalid handle");
			case CUDA_ERROR_NOT_FOUND:
				throw std::runtime_error("Not found");
			case CUDA_ERROR_NOT_READY:
				throw std::runtime_error("CUDA not ready");
			case CUDA_ERROR_LAUNCH_FAILED:
				throw std::runtime_error("Launch failed");
			case CUDA_ERROR_LAUNCH_OUT_OF_RESOURCES:
				throw std::runtime_error("Launch exceeded resources");
			case CUDA_ERROR_LAUNCH_TIMEOUT:
				throw std::runtime_error("Launch exceeded timeout");
			case CUDA_ERROR_LAUNCH_INCOMPATIBLE_TEXTURING:
				throw std::runtime_error("Launch with incompatible texturing");
			case CUDA_ERROR_UNKNOWN:
				throw std::runtime_error("Unknown error");
			default:
				throw std::runtime_error("Unspecified error");
			}
		}
	}

	DriverApi()
	{
		device.use(0);
	}

	~DriverApi()
	{
		// TODO we may need more clean up function here to wipe out all CUDA internal resources
	}

	struct DeviceManagement
	{
		DeviceManagement()
		{
			CUresult result;

			// initialize CUDA driver API
			result = cuInit(0);
			throwOnError(result);

			result = cuDeviceGetCount(&mDeviceCount);
			throwOnError(result);

			// create all device context
			if((mDeviceCount = countAllDevices()) == 0)
				throwOnError(CUDA_ERROR_NO_DEVICE);

			mAllContext = new CUcontext[mDeviceCount];
			mContextFlags = new uint32[mDeviceCount];

			for(int i=0;i<mDeviceCount;++i)
			{
				mContextFlags[i] = CU_CTX_SCHED_AUTO;
				result = cuCtxCreate(&mAllContext[i], mContextFlags[i], i);
				throwOnError(result);

				result = cuCtxPopCurrent(NULL);
				throwOnError(result);
			}

			mCurrentDeviceIndex = -1;
		}

		~DeviceManagement()
		{
			CUresult result;

			use(-1);

			for(int i=0;i<mDeviceCount;++i)
			{
				result = cuCtxDestroy(mAllContext[i]);
				//throwOnError(result);
			}
		}

		inline void threadInit()
		{
			use(0);
		}

		inline void threadExit() const
		{
			CUresult result;
			result = cuCtxPopCurrent(NULL);
			throwOnError(result);
		}

		inline int countAllDevices() const
		{
			return mDeviceCount;
		}

		inline void use(int dev)
		{
			CUresult result;

			if(mCurrentDeviceIndex != dev)
			{
				if(mCurrentDeviceIndex != -1)
				{
					result = cuCtxPopCurrent(NULL);
					throwOnError(result);
				}

				if(dev != -1)
				{
					if(dev >= mDeviceCount)
						throwOnError(CUDA_ERROR_INVALID_DEVICE);

					result = cuCtxPushCurrent(mAllContext[dev]);
					throwOnError(result);
				}

				mCurrentDeviceIndex = dev;
			}
		}

		inline int current() const
		{
			return mCurrentDeviceIndex;
		}

		inline void enableMappedMemory() const
		{
			CUresult result;

			if((mContextFlags[mCurrentDeviceIndex] & CU_CTX_MAP_HOST) == 0)
			{
				result = cuCtxPopCurrent(NULL);
				throwOnError(result);

				result = cuCtxDestroy(mAllContext[mCurrentDeviceIndex]);
				throwOnError(result);

				uint32 flag = mContextFlags[mCurrentDeviceIndex] = CU_CTX_MAP_HOST;
				result = cuCtxCreate(&mAllContext[mCurrentDeviceIndex], mContextFlags[mCurrentDeviceIndex], mCurrentDeviceIndex);
				throwOnError(result);
				mContextFlags[mCurrentDeviceIndex] = flag;
			}
		}

		struct
		{
			inline void probe(int dev)
			{
				CUresult result;

				char nameBuffer[256];
				result = cuDeviceGetName(nameBuffer, 256, dev);
				throwOnError(result);
				mName = nameBuffer;

				result = cuDriverGetVersion(&mDriverVersion);
				throwOnError(result);

				result = cuDeviceComputeCapability(&mComputeCapabilityMajor, &mComputeCapabilityMinor, dev);
				throwOnError(result);

				result = cuDeviceGetAttribute(&mMultiProcessorCount, CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT, dev);
				throwOnError(result);

				result = cuDeviceTotalMem(&mTotalDeviceMem, dev);
				throwOnError(result);

				result = cuDeviceGetAttribute(&mTotalConstantMem, CU_DEVICE_ATTRIBUTE_TOTAL_CONSTANT_MEMORY, dev);
				throwOnError(result);

				result = cuDeviceGetAttribute(&mSharedMemPerBlock, CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK, dev);
				throwOnError(result);

				result = cuDeviceGetAttribute(&mRegMemPerBlock, CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK, dev);
				throwOnError(result);

				result = cuDeviceGetAttribute(&mWarpSize, CU_DEVICE_ATTRIBUTE_WARP_SIZE, dev);
				throwOnError(result);

				result = cuDeviceGetAttribute(&mMaxThreadPerBlock, CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK, dev);
				throwOnError(result);

				result = cuDeviceGetAttribute(&mBlockDim[0], CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X, dev);
				throwOnError(result);

				result = cuDeviceGetAttribute(&mBlockDim[1], CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y, dev);
				throwOnError(result);

				result = cuDeviceGetAttribute(&mBlockDim[2], CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z, dev);
				throwOnError(result);

				result = cuDeviceGetAttribute(&mGridDim[0], CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X, dev);
				throwOnError(result);

				result = cuDeviceGetAttribute(&mGridDim[1], CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y, dev);
				throwOnError(result);

				result = cuDeviceGetAttribute(&mGridDim[2], CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Z, dev);
				throwOnError(result);

				result = cuDeviceGetAttribute(&mClockRate, CU_DEVICE_ATTRIBUTE_CLOCK_RATE, dev);
				throwOnError(result);

				int gpuOverlap = 0;
				cuDeviceGetAttribute(&gpuOverlap, CU_DEVICE_ATTRIBUTE_GPU_OVERLAP, dev);
				throwOnError(result);
				mOverlappedExecution = gpuOverlap ? true : false;

				int kernelExecTimeoutEnabled = 0;
				result = cuDeviceGetAttribute(&kernelExecTimeoutEnabled, CU_DEVICE_ATTRIBUTE_KERNEL_EXEC_TIMEOUT, dev);
				throwOnError(result);
				mHasKernelExecutionTimeLimit = kernelExecTimeoutEnabled ? true : false;

				int supportMappedMemory = 0;
				result = cuDeviceGetAttribute(&supportMappedMemory, CU_DEVICE_ATTRIBUTE_CAN_MAP_HOST_MEMORY, dev);
				throwOnError(result);
				mSupportMappedMemory = supportMappedMemory ? true : false;

				int supportECCMemory = 0;
				result = cuDeviceGetAttribute(&supportECCMemory, CU_DEVICE_ATTRIBUTE_ECC_ENABLED, dev);
				throwOnError(result);
				mSupportECCMemory = supportECCMemory ? true : false;

				int supportConcurrentExecution = 0;
				result = cuDeviceGetAttribute(&supportConcurrentExecution, CU_DEVICE_ATTRIBUTE_CONCURRENT_KERNELS, dev);
				throwOnError(result);
				mSupportConcurrentExecution = supportConcurrentExecution ? true : false;
			}

			inline std::string name() const
			{
				return mName;
			}

			inline std::size_t globalMemorySize() const
			{
				return mTotalDeviceMem;
			}

			inline std::size_t sharedMemorySizePerBlock() const
			{
				return mSharedMemPerBlock;
			}

			inline std::size_t registerMemorySizePerBlock() const
			{
				return mRegMemPerBlock*4;
			}

			inline std::size_t constantMemorySize() const
			{
				return mTotalConstantMem;
			}

			inline int warpSize() const
			{
				return mWarpSize;
			}

			inline int maxThreadsPerBlock() const
			{
				return mMaxThreadPerBlock;
			}

			inline int maxBlockDimX() const
			{
				return mBlockDim[0];
			}

			inline int maxBlockDimY() const
			{
				return mBlockDim[1];
			}

			inline int maxBlockDimZ() const
			{
				return mBlockDim[2];
			}

			inline int maxGridDimX() const
			{
				return mGridDim[0];
			}

			inline int maxGridDimY() const
			{
				return mGridDim[1];
			}

			inline int maxGridDimZ() const
			{
				return mGridDim[2];
			}

			inline int clockRate() const
			{
				return mClockRate;
			}

			inline std::pair<int,int> driverVersion() const
			{
				return std::make_pair(mDriverVersion/1000, mDriverVersion%100);
			}

			inline std::pair<int,int> computeCapability() const
			{
				return std::make_pair(mComputeCapabilityMajor, mComputeCapabilityMinor);
			}

			inline bool supportOverlappedExecution() const
			{
				return mOverlappedExecution;
			}

			inline int numMultiProcessors() const
			{
				return mMultiProcessorCount;
			}

			inline int numCorePerMultiProcessor() const
			{
			    // Defines for GPU Architecture types (using the SM version to determine the # of cores per SM
			    typedef struct {
			        int SM; // 0xMm (hexidecimal notation), M = SM Major version, and m = SM minor version
			        int Cores;
			    } sSMtoCores;

				sSMtoCores nGpuArchCoresPerSM[] =
				{ { 0x10,  8 },
				  { 0x11,  8 },
				  { 0x12,  8 },
				  { 0x13,  8 },
				  { 0x20, 32 },
				  { 0x21, 48 },
				  {   -1, -1 }
				};

			    int index = 0;
			    while (nGpuArchCoresPerSM[index].SM != -1) {
			        if (nGpuArchCoresPerSM[index].SM == ((mComputeCapabilityMajor << 4) + mComputeCapabilityMinor) ) {
			            return nGpuArchCoresPerSM[index].Cores;
			        }
			        index++;
			    }

			    throwOnError(CUDA_ERROR_INVALID_VALUE);
			    return -1;
			}

			inline bool hasKernelExecutionTimeLimit() const
			{
				return mHasKernelExecutionTimeLimit;
			}

			inline bool supportMappedMemory() const
			{
				return mSupportMappedMemory;
			}

			inline bool supportECCMemory() const
			{
				return mSupportECCMemory;
			}

			inline bool supportConcurrentExecution() const
			{
				return mSupportConcurrentExecution;
			}

			int mDriverVersion;

			std::string mName;
			int mComputeCapabilityMajor;
			int mComputeCapabilityMinor;
			int mMultiProcessorCount;

#ifdef ENABLE_CUDA_POINTER_64BIT
			std::size_t mTotalDeviceMem;
#else
			unsigned int mTotalDeviceMem;
#endif
			int mTotalConstantMem;
			int mSharedMemPerBlock;
			int mRegMemPerBlock;

			int mWarpSize;
			int mMaxThreadPerBlock;
			int mBlockDim[3];
			int mGridDim[3];

			bool mOverlappedExecution;
			bool mHasKernelExecutionTimeLimit;
			bool mSupportMappedMemory;
			bool mSupportECCMemory;
			bool mSupportConcurrentExecution;

			int mClockRate;
		} properties;

	private:
		static __thread int mCurrentDeviceIndex;
		int mDeviceCount;
		mutable CUcontext* mAllContext;
		mutable uint32* mContextFlags;
	} device;

	struct EventManagement
	{
		inline void create(event_type* e) const
		{
			CUresult result = cuEventCreate(e, CU_EVENT_DEFAULT);
			throwOnError(result);
		}

		inline void destroy(const event_type& e) const
		{
			CUresult result = cuEventDestroy(const_cast<event_type&>(e));
			throwOnError(result);
		}

		inline void record(const event_type& e, const stream_type& s = default_stream) const
		{
			CUresult result = cuEventRecord(const_cast<event_type&>(e), const_cast<stream_type&>(s));
			throwOnError(result);
		}

		inline bool queryCompleted(const event_type& e) const
		{
			CUresult result = cuEventQuery(const_cast<event_type&>(e));

			if(result == CUDA_SUCCESS)
			{
				return true;
			}
			else if(result == CUDA_ERROR_NOT_READY)
			{
				return false;
			}

			throwOnError(result);
		}

		inline void synchronize(const event_type& e) const
		{
			CUresult result = cuEventSynchronize(const_cast<event_type&>(e));
			throwOnError(result);
		}

		inline float queryElapsed(const event_type& start, const event_type& stop) const
		{
			float elapsed = 0.0f;
			CUresult result = cuEventElapsedTime(&elapsed, start, stop);
			throwOnError(result);
			return elapsed;
		}
	} event;

	struct StreamManagement
	{
		inline void create(stream_type* s) const
		{
			CUresult result = cuStreamCreate(s, 0);
			throwOnError(result);
		}

		inline void destroy(const stream_type& s) const
		{
			CUresult result = cuStreamDestroy(const_cast<stream_type&>(s));
			throwOnError(result);
		}

		inline bool queryCompleted(const stream_type& s) const
		{
			CUresult result = cuStreamQuery(const_cast<stream_type&>(s));

			if(result == CUDA_SUCCESS)
			{
				return true;
			}
			else if(result == CUDA_ERROR_NOT_READY)
			{
				return false;
			}

			throwOnError(result);
		}

		inline void synchronize(const stream_type& s) const
		{
			CUresult result = cuStreamSynchronize(const_cast<stream_type&>(s));
			throwOnError(result);
		}
	} stream;

	struct Texture
	{
		/*
		cudaArray* array;
		uint8 dimension;
		cudaChannelFormatDesc desc;
		cudaExtent extent;
		std::size_t element_size;
		bool binded;
		CUtexref* texref;

		inline const std::size_t width()  const { return extent.width;  }
		inline const std::size_t height() const { return extent.height; }
		inline const std::size_t depth()  const { return extent.depth;  }

		inline bool unbind()
		{
			if(binded)
			{
				binded = false;

				cudaError_t error = cudaUnbindTexture(texref);

				texref = NULL;
				throwOnError(error);
			}
		}

		inline bool bind(const char* name, module_type* module = NULL)
		{
			CUresult result = cuModuleGetTexRef(&texref, module, name);
			throwOnError(result);

			error = cudaBindTextureToArray(texref, array, &desc);
			throwOnError(error);

			binded = true;
		}

		inline void copyFrom(MutablePointer* src, std::size_t src_offset, std::size_t size, const location::type& src_type)
		{
			copyFromRaw(src->data(), src_offset, size, src_type);
		}

		inline void copyFromRaw(byte* src, std::size_t src_offset, std::size_t size, const location::type& src_type)
		{
			BOOST_ASSERT(src != NULL);
			BOOST_ASSERT(size > 0 && size <= extent.width * extent.height * extent.depth * element_size);
			cudaMemcpyKind kind;
			cudaError_t error;

			// find out the copy type by the source and destination memory location
			if(src_type == location::host)
			{
				kind = cudaMemcpyHostToDevice;
			}
			else // src_type == location::device
			{
				kind = cudaMemcpyDeviceToDevice;
			}

			if(dimension == 1)
			{
				error = cudaMemcpyToArray(array, 0, 0, src + src_offset, size, kind);
				throwOnError(error);
			}
			else if(dimension == 2)
			{
				error = cudaMemcpyToArray(array, 0, 0, src + src_offset, size, kind);
				throwOnError(error);
			}
			else
			{
				cudaMemcpy3DParms param = { 0 };
			    param.dstArray = array;
			    param.dstPos = make_cudaPos(0,0,0);
			    param.srcPtr = make_cudaPitchedPtr((void*)src, extent.width * element_size, extent.width, extent.height);
			    param.srcPos = make_cudaPos(0,0,0);
			    param.kind = kind;
			    param.extent = extent;

				error = cudaMemcpy3D(&param);
				throwOnError(error);
			}
		}

		inline void copyTo(MutablePointer* dest, std::size_t dest_offset, std::size_t size, const location::type& dest_type)
		{
			copyToRaw(dest->data(), dest_offset, size, dest_type);
		}

		inline void copyToRaw(byte* dest, std::size_t dest_offset, std::size_t size, const location::type& dest_type)
		{
			BOOST_ASSERT(dest != NULL);
			BOOST_ASSERT(size > 0 && size <= extent.width * extent.height * extent.depth * element_size);
			cudaMemcpyKind kind;
			cudaError_t error;

			// find out the copy type by the source and destination memory location
			if(dest_type == location::host)
			{
				kind = cudaMemcpyDeviceToHost;
			}
			else // dest_type == location::device
			{
				kind = cudaMemcpyDeviceToDevice;
			}

			if(dimension == 1)
			{
				error = cudaMemcpyFromArray(dest + dest_offset, array, 0, 0, size, kind);
				throwOnError(error);
			}
			else if(dimension == 2)
			{
				error = cudaMemcpyFromArray(dest + dest_offset, array, 0, 0, size, kind);
				throwOnError(error);
			}
			else
			{
				cudaMemcpy3DParms param = { 0 };
				param.dstPtr = make_cudaPitchedPtr((void*)dest, extent.width * element_size, extent.width, extent.height);;
			    param.dstPos = make_cudaPos(0,0,0);
			    param.srcArray = array;
			    param.srcPos = make_cudaPos(0,0,0);
			    param.kind = kind;
			    param.extent = extent;

				error = cudaMemcpy3D(&param);
				throwOnError(error);
			}
		}
		*/
	};

	struct TextureManagement
	{
		/*
		template <typename T>
		inline void create1D(std::size_t width, Texture** texture) const
		{
			BOOST_ASSERT(texture != NULL);

			*texture = new Texture;

			(*texture)->dimension = 1;
			(*texture)->desc = cudaCreateChannelDesc<T>();
			(*texture)->extent.width  = width;
			(*texture)->extent.height = 1;
			(*texture)->extent.depth  = 1;
			(*texture)->element_size  = sizeof(T);
			(*texture)->binded        = false;

			cudaError_t error = cudaMallocArray(&((*texture)->array), &((*texture)->desc), width, 1);
			if(error != cudaSuccess)
			{
				SAFE_DELETE(*texture);
				throwOnError(error);
			}
		}

		template <typename T>
		inline void create2D(std::size_t width, std::size_t height, Texture** texture) const
		{
			BOOST_ASSERT(texture != NULL);

			*texture = new Texture;

			(*texture)->dimension = 2;
			(*texture)->desc = cudaCreateChannelDesc<T>();
			(*texture)->extent.width  = width;
			(*texture)->extent.height = height;
			(*texture)->extent.depth  = 1;
			(*texture)->element_size  = sizeof(T);
			(*texture)->binded        = false;

			cudaError_t error = cudaMallocArray(&((*texture)->array), &((*texture)->desc), width, height);
			if(error != cudaSuccess)
			{
				SAFE_DELETE(*texture);
				throwOnError(error);
			}
		}

		template <typename T>
		inline void create3D(std::size_t width, std::size_t height, std::size_t depth, Texture** texture) const
		{
			*texture = new Texture;

			(*texture)->dimension = 3;
			(*texture)->desc = cudaCreateChannelDesc<T>();
			(*texture)->extent.width  = width;
			(*texture)->extent.height = height;
			(*texture)->extent.depth  = depth;
			(*texture)->element_size  = sizeof(T);
			(*texture)->binded        = false;

			cudaError_t error = cudaMalloc3DArray(&((*texture)->array), &((*texture)->desc), (*texture)->extent);
			if(error != cudaSuccess)
			{
				SAFE_DELETE(*texture);
				throwOnError(error);
			}
		}

		inline void destroy(Texture* texture) const
		{
			BOOST_ASSERT(texture != NULL);

			cudaError_t error = cudaFreeArray(texture->array);
			SAFE_DELETE(texture);

			if(error != cudaSuccess)
			{
				throwOnError(error);
			}
		}

		template <typename T>
		inline size_t bind1D(const char* name, MutablePointer* devicePointer, std::size_t size)
		{
			bind1D<T>(name, devicePointer->data(), size);
		}

		template <typename T>
		inline size_t bind1D(const char* name, byte* devicePointerRaw, std::size_t size)
		{
			const textureReference* texref;
			cudaError_t error = cudaGetTextureReference(&texref, name);
			throwOnError(error);

			cudaChannelFormatDesc desc = cudaCreateChannelDesc<T>();

			std::size_t offset = 0;
			error = cudaBindTexture(&offset, texref, devicePointerRaw, &desc, size);
			throwOnError(error);

			return offset;
		}
		*/

	} texture;

	struct MemoryManagement
	{
		struct HostMemoryManagement
		{
			HostMemoryManagement()
			{
				// About the allocators/reservedMemories array...
				// [0]: pagelock = false
				// [1]: pagelock = true, portable = false, mapped = false, writecombined = false
				// [2]: pagelock = true, portable = false, mapped = false, writecombined = true
				// [3]: pagelock = true, portable = false, mapped = true,  writecombined = false
				// [4]: pagelock = true, portable = false, mapped = true,  writecombined = true
				// [5]: pagelock = true, portable = true,  mapped = false, writecombined = false
				// [6]: pagelock = true, portable = true,  mapped = false, writecombined = true
				// [7]: pagelock = true, portable = true,  mapped = true,  writecombined = false
				// [8]: pagelock = true, portable = true,  mapped = true,  writecombined = true

				allocators[0] = NULL;
				allocators[1] = NULL;
				allocators[2] = NULL;
				allocators[3] = NULL;
				allocators[4] = NULL;
				allocators[5] = NULL;
				allocators[6] = NULL;
				allocators[7] = NULL;
				allocators[8] = NULL;

				reservedMemories[0] = NULL;
				reservedMemories[1] = NULL;
				reservedMemories[2] = NULL;
				reservedMemories[3] = NULL;
				reservedMemories[4] = NULL;
				reservedMemories[5] = NULL;
				reservedMemories[6] = NULL;
				reservedMemories[7] = NULL;
				reservedMemories[8] = NULL;
			}

			~HostMemoryManagement()
			{
				SAFE_DELETE(allocators[0]);
				SAFE_DELETE(allocators[1]);
				SAFE_DELETE(allocators[2]);
				SAFE_DELETE(allocators[3]);
				SAFE_DELETE(allocators[4]);
				SAFE_DELETE(allocators[5]);
				SAFE_DELETE(allocators[6]);
				SAFE_DELETE(allocators[7]);
				SAFE_DELETE(allocators[8]);

				if(reservedMemories[0]) freeRaw(reservedMemories[0], false);
				if(reservedMemories[1]) freeRaw(reservedMemories[1], true);
				if(reservedMemories[2]) freeRaw(reservedMemories[2], true);
				if(reservedMemories[3]) freeRaw(reservedMemories[3], true);
				if(reservedMemories[4]) freeRaw(reservedMemories[4], true);
				if(reservedMemories[5]) freeRaw(reservedMemories[5], true);
				if(reservedMemories[6]) freeRaw(reservedMemories[6], true);
				if(reservedMemories[7]) freeRaw(reservedMemories[7], true);
				if(reservedMemories[8]) freeRaw(reservedMemories[8], true);
			}

			inline void reserve(std::size_t size, bool pagelocked = false, bool portable = false, bool mapped = false, bool writecombined = false) const
			{
				uint8 key = keyForReserved(pagelocked, portable, mapped, writecombined);

				SAFE_DELETE(allocators[key]);
				if(reservedMemories[key]) freeRaw(reservedMemories[key], pagelocked); reservedMemories[key] = NULL;

				if(size > 0)
				{
					allocateRaw(&reservedMemories[key], size, pagelocked, portable, mapped, writecombined);
					allocators[key] = new FragmentAllocator(reservedMemories[key], size);
				}
			}

			inline void allocate(MutablePointer** pointer, std::size_t size, bool from_reserved = false, bool pagelocked = false, bool portable = false, bool mapped = false, bool writecombined = false) const
			{
				BOOST_ASSERT(pointer != NULL);

				if(from_reserved)
				{
					uint8 key = keyForReserved(pagelocked, portable, mapped, writecombined);
					FragmentAllocator* allocator = allocators[key];
					if(UNLIKELY(!allocator))
					{
						throw std::runtime_error("Invalid allocator, must call reserved() before using memory from pool");
					}

					if(!allocator->allocate(pointer, size))
					{
						throw std::runtime_error("Fail to allocate from pool, out of memory");
					}
				}
				else
				{
					byte* data = NULL;
					allocateRaw(&data, size, pagelocked, portable, mapped, writecombined);
					*pointer = new MutablePointer(data);
				}
			}

			inline void allocateRaw(byte** pointer, std::size_t size, bool pagelocked = false, bool portable = false, bool mapped = false, bool writecombined = false) const
			{
				BOOST_ASSERT(pointer != NULL);

				if(pagelocked)
				{
					unsigned int flags = 0;

					if(portable)      flags |= CU_MEMHOSTALLOC_PORTABLE;
					if(mapped)        flags |= CU_MEMHOSTALLOC_DEVICEMAP;
					if(writecombined) flags |= CU_MEMHOSTALLOC_WRITECOMBINED;

					CUresult error = cuMemHostAlloc(reinterpret_cast<void**>(pointer), size, flags);
					throwOnError(error);
				}
				else
				{
					BOOST_ASSERT(!mapped);
					BOOST_ASSERT(!writecombined);

					*pointer = reinterpret_cast<byte*>(::malloc(size));
				}
			}

			inline void free(MutablePointer* pointer, bool from_reserved = false, bool pagelocked = false, bool portable = false, bool mapped = false, bool writecombined = false) const
			{
				BOOST_ASSERT(pointer != NULL);

				if(from_reserved)
				{
					uint8 key = keyForReserved(pagelocked, portable, mapped, writecombined);
					FragmentAllocator* allocator = allocators[key];
					if(UNLIKELY(!allocator))
					{
						throw std::runtime_error("Invalid allocator, must call reserved() before using memory from pool");
					}

					if(!allocator->deallocate(pointer))
					{
						throw std::runtime_error("Failed to deallocate, might be invalid pointer");
					}
				}
				else
				{
					freeRaw(pointer->data(), pagelocked);
					delete pointer;
				}
			}

			inline void freeRaw(byte* pointer, bool pagelocked = false) const
			{
				BOOST_ASSERT(pointer != NULL);

				if(pagelocked)
				{
					CUresult result = cuMemFree(reinterpret_cast<CUdeviceptr>(pointer));
					throwOnError(result);
				}
				else
				{
					::free(reinterpret_cast<void*>(pointer));
				}
			}

			inline void set(byte* data, int value, std::size_t n) const
			{
				::memset(data, value, n);
			}

			inline void mapToDevice(MutablePointer** device, MutablePointer* host) const
			{
				byte* pointer = NULL;
				mapToDeviceRaw(&pointer, host->data());
				*device = new MutablePointer(pointer);
			}

			inline void mapToDeviceRaw(byte** device, byte* host) const
			{
				CUdeviceptr ptr;
				CUresult error = cuMemHostGetDevicePointer(&ptr, reinterpret_cast<void*>(host), 0);
				throwOnError(error);
				*device = reinterpret_cast<byte*>(ptr);
			}
		private:
			inline uint8 keyForReserved(bool pagelocked, bool portable, bool mapped, bool writecombined) const
			{
				uint8 key = 0;
				if(pagelocked) key += 1; if(writecombined) key += 1; if(mapped) key += 2; if(portable) key += 4;
				return key;
			}

			mutable FragmentAllocator* allocators[9];
			mutable byte* reservedMemories[9];
		} host;

		struct DeviceMemoryManagement
		{
			DeviceMemoryManagement()
			{
				allocator = NULL;
				reservedMemory = NULL;
			}

			~DeviceMemoryManagement()
			{
				SAFE_DELETE(allocator);
				if(reservedMemory) freeRaw(reservedMemory); reservedMemory = NULL;
			}

			inline void reserve(std::size_t size) const
			{
				SAFE_DELETE(allocator);
				if(reservedMemory) freeRaw(reservedMemory); reservedMemory = NULL;

				if(size > 0)
				{
					allocateRaw(&reservedMemory, size);
					allocator = new FragmentAllocator(reservedMemory, size);
				}
			}

			inline void allocate(MutablePointer** pointer, std::size_t size, bool from_reserved = false) const
			{
				if(from_reserved)
				{
					if(UNLIKELY(!allocator))
					{
						throw std::runtime_error("Invalid allocator, must call reserved() before using memory from pool");
					}

					if(!allocator->allocate(pointer, size))
					{
						throw std::runtime_error("Fail to allocate from pool, out of memory");
					}
				}
				else
				{
					byte* data = NULL;
					allocateRaw(&data, size);
					*pointer = new MutablePointer(data);
				}
			}

			inline void allocateRaw(byte** pointer, std::size_t size) const
			{
				CUdeviceptr ptr;
				CUresult error = cuMemAlloc(&ptr, size);
				throwOnError(error);
				*pointer = reinterpret_cast<byte*>(ptr);
			}

			inline void free(MutablePointer* pointer, bool from_reserved = false) const
			{
				if(from_reserved)
				{
					if(UNLIKELY(!allocator))
					{
						throw std::runtime_error("Invalid allocator, must call reserved() before using memory from pool");
					}

					if(!allocator->deallocate(pointer))
					{
						throw std::runtime_error("Failed to deallocate, might be invalid pointer");
					}
				}
				else
				{
					freeRaw(pointer->data());
					delete pointer;
				}
			}

			inline void freeRaw(byte* pointer) const
			{
				CUdeviceptr ptr = reinterpret_cast<CUdeviceptr>(pointer);
				CUresult error = cuMemFree(ptr);
				throwOnError(error);
			}

			inline void set(MutablePointer* pointer, int value, std::size_t n) const
			{
				setRaw(pointer->data(), value, n);
			}

			inline void setRaw(byte* pointer, int value, std::size_t n) const
			{
				CUdeviceptr ptr = reinterpret_cast<CUdeviceptr>(pointer);
				CUresult error = cuMemsetD8(ptr, (unsigned char)value, n);
			}

		private:
			mutable FragmentAllocator* allocator;
			mutable byte* reservedMemory;
		} device;

		template<typename T>
		inline void copyToSymbol(const char* symbol_name, T& src) const
		{
			// TODO
//			CUresult error = cudaMemcpyToSymbol(symbol_name, &src, sizeof(T), 0, cudaMemcpyHostToDevice);
//			throwOnError(error);
		}

		template<typename T>
		inline void copyToSymbolAsync(const char* symbol_name, T& src, const stream_type& stream) const
		{
			// TODO
//			CUresult error = cudaMemcpyToSymbolAsync(symbol_name, &src, sizeof(T), 0, cudaMemcpyHostToDevice, stream);
//			throwOnError(error);
		}

		inline void copy(MutablePointer* dest, MutablePointer* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type) const
		{
			copyRaw(dest->data(), src->data(), dest_offset, src_offset, size, dest_type, src_type);
		}

		inline void copyRaw(byte* dest, const byte* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type) const
		{
			BOOST_ASSERT(size > 0);

			if(dest == src)
				return;

			CUresult result;

			// since the memory copy does not support overlapped memory region, we need to handle this carefully
			// the limit only apply in same memory context (i.e. host to host or device to device)
			if( (src_type == location::device && dest_type == location::device) || (src_type == location::host && dest_type == location::host) )
			{
				if(src < dest && src + size > dest)
				{
					throw std::range_error("Overlapped memory copy is not supported");
				}
				else if(dest < src && dest + size > src)
				{
					throw std::range_error("Overlapped memory copy is not supported");
				}
			}

			// find out the copy type by the source and destination memory location
			if(dest_type == location::host)
			{
				if(src_type == location::host)
				{
					::memcpy(dest + dest_offset, src + src_offset, size);
				}
				else // src_type == location::device
				{
					CUdeviceptr devptr = reinterpret_cast<CUdeviceptr>(src + src_offset);
					result = cuMemcpyDtoH(dest + dest_offset, devptr, size);
				}
			}
			else // dest_type == location::device
			{
				if(src_type == location::host)
				{
					CUdeviceptr devptr = reinterpret_cast<CUdeviceptr>(dest + dest_offset);
					result = cuMemcpyHtoD(devptr, src + src_offset, size);
				}
				else // src_type == location::device
				{
					CUdeviceptr devptr_dest = reinterpret_cast<CUdeviceptr>(dest + dest_offset);
					CUdeviceptr devptr_src  = reinterpret_cast<CUdeviceptr>(src + src_offset);
					result = cuMemcpyDtoH(devptr_dest, devptr_src, size);
				}
			}

			throwOnError(result);
		}

		inline void copyAsync(MutablePointer* dest, MutablePointer* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type, const stream_type& stream) const
		{
			copyAsyncRaw(dest->data(), src->data(), dest_offset, src_offset, size, dest_type, src_type, stream);
		}

		inline void copyAsyncRaw(byte* dest, const byte* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type, const stream_type& stream) const
		{
			BOOST_ASSERT(size > 0);

			if(dest == src)
				return;

			CUresult result;

			// since the memory copy does not support overlapped memory region, we need to handle this carefully
			// the limit only apply in same memory context (i.e. host to host or device to device)
			if( (src_type == location::device && dest_type == location::device) || (src_type == location::host && dest_type == location::host) )
			{
				if(src < dest && src + size > dest)
				{
					throw std::range_error("Overlapped memory copy is not supported");
				}
				else if(dest < src && dest + size > src)
				{
					throw std::range_error("Overlapped memory copy is not supported");
				}
			}

			// find out the copy type by the source and destination memory location
			if(dest_type == location::host)
			{
				if(src_type == location::host)
				{
					// TODO there's no asynchronous memcpy, wondering how it gets implemented in CUDA runtime API? maybe it's fake? need some more testing.
					// TODO so right now we just use memcpy directly.
					::memcpy(dest + dest_offset, src + src_offset, size);
				}
				else // src_type == location::device
				{
					CUdeviceptr devptr = reinterpret_cast<CUdeviceptr>(src + src_offset);
					result = cuMemcpyDtoHAsync(dest + dest_offset, devptr, size, stream);
				}
			}
			else // dest_type == location::device
			{
				if(src_type == location::host)
				{
					CUdeviceptr devptr = reinterpret_cast<CUdeviceptr>(dest + dest_offset);
					result = cuMemcpyDtoHAsync(devptr, src + src_offset, size, stream);
				}
				else // src_type == location::device
				{
					CUdeviceptr devptr_dest = reinterpret_cast<CUdeviceptr>(dest + dest_offset);
					CUdeviceptr devptr_src  = reinterpret_cast<CUdeviceptr>(src + src_offset);
					result = cuMemcpyDtoHAsync(devptr_dest, devptr_src, size, stream);
				}
			}

			throwOnError(result);
		}
	} memory;

	struct ExecutionManagement
	{
		struct Module
		{
			struct Symbol
			{
				explicit Symbol(byte* pointer, std::size_t size) : mPointer(pointer), mSize(size)
				{ }

			private:
				byte* mPointer;
				std::size_t mSize;
			};

			struct Function
			{
				explicit Function(function_type& f) : mFunctionObj(f)
				{
					CUresult result;

					result = cuFuncGetAttribute(&mConstantMemSize, CU_FUNC_ATTRIBUTE_CONST_SIZE_BYTES, mFunctionObj);
					throwOnError(result);

					result = cuFuncGetAttribute(&mSharedMemSize, CU_FUNC_ATTRIBUTE_SHARED_SIZE_BYTES, mFunctionObj);
					throwOnError(result);

					result = cuFuncGetAttribute(&mLocalMemSize, CU_FUNC_ATTRIBUTE_LOCAL_SIZE_BYTES, mFunctionObj);
					throwOnError(result);

					result = cuFuncGetAttribute(&mMaxThreadPerBlock, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, mFunctionObj);
					throwOnError(result);

					result = cuFuncGetAttribute(&mNumRegs, CU_FUNC_ATTRIBUTE_NUM_REGS, mFunctionObj);
					throwOnError(result);

					mMaxParameterSize = 0;
				}

				inline void configure(const dim3& grid, const dim3& block, std::size_t shared_memory) const
				{
					CUresult result;

					if(grid.z != 1)
						throwOnError(CUDA_ERROR_LAUNCH_FAILED);

					result = cuFuncSetBlockShape(mFunctionObj, block.x, block.y, block.z);
					throwOnError(result);

					result = cuFuncSetSharedSize(mFunctionObj, shared_memory);
					throwOnError(result);

					mGridDim = grid;
				}

				inline void setupParameter(unsigned int value, std::size_t offset) const
				{
					CUresult result;

					result = cuParamSeti(mFunctionObj, offset, value);
					throwOnError(result);

					if(mMaxParameterSize < offset + sizeof(unsigned int))
					{
						result = cuParamSetSize(mFunctionObj, offset + sizeof(unsigned int));
						throwOnError(result);
						mMaxParameterSize = offset + sizeof(unsigned int);
					}
				}

				inline void setupParameter(float value, std::size_t offset) const
				{
					CUresult result;

					result = cuParamSetf(mFunctionObj, offset, value);
					throwOnError(result);

					if(mMaxParameterSize < offset + sizeof(float))
					{
						result = cuParamSetSize(mFunctionObj, offset + sizeof(float));
						throwOnError(result);
						mMaxParameterSize = offset + sizeof(float);
					}
				}

				inline void setupParameterV(void* args, std::size_t size, std::size_t offset) const
				{
					CUresult result;

					result = cuParamSetv(mFunctionObj, offset, args, size);
					throwOnError(result);

					if(mMaxParameterSize < offset + size)
					{
						result = cuParamSetSize(mFunctionObj, offset + size);
						throwOnError(result);
						mMaxParameterSize = offset + size;
					}
				}

				inline void launch() const
				{
					CUresult result = cuLaunchGrid(mFunctionObj, mGridDim.x, mGridDim.y);
					throwOnError(result);
				}

				inline void launchAsync(stream_type& s) const
				{
					CUresult result = cuLaunchGridAsync(mFunctionObj, mGridDim.x, mGridDim.y, s);
					throwOnError(result);
				}

				inline std::size_t constantMemorySize() const
				{
					return mConstantMemSize;
				}

				inline std::size_t sharedMemorySize() const
				{
					return mSharedMemSize;
				}

				inline std::size_t localMemorySize() const
				{
					return mLocalMemSize;
				}

				inline int maxThreadsPerBlock() const
				{
					return mMaxThreadPerBlock;
				}

				inline int registerUsed() const
				{
					return mNumRegs;
				}

			private:
				function_type mFunctionObj;

				mutable int mConstantMemSize;
				mutable int mSharedMemSize;
				mutable int mLocalMemSize;
				mutable int mMaxThreadPerBlock;
				mutable int mNumRegs;

				mutable std::size_t mMaxParameterSize;

				mutable dim3 mGridDim;
			};

			explicit Module(module_type& m) : mModuleObj(m)
			{ }

			inline Symbol* getSymbol(const char* name) const
			{
				CUdeviceptr ptr;
#ifdef ENABLE_CUDA_POINTER_64BIT
				std::size_t s;
#else
				unsigned int s;
#endif

				CUresult result = cuModuleGetGlobal(&ptr, &s, mModuleObj, name);
				throwOnError(result);

				return new Symbol(reinterpret_cast<byte*>(ptr), s);
			}

			inline Function* getFunction(const char* name)
			{
				function_type f;

				CUresult result = cuModuleGetFunction(&f, mModuleObj, name);
				throwOnError(result);

				return new Function(f);
			}

		private:
			module_type mModuleObj;
		};

		inline void synchronize() const
		{
			CUresult error = cuCtxSynchronize();
			throwOnError(error);
		}

		inline Module* compile(const void* image, jit_options::type& options, std::string& info_log, std::string& error_log) const
		{
			CUresult result;

			module_type m;

			std::size_t option_size = options.size();

			// convert to CUjin_option list
			CUjit_option* option_types = new CUjit_option[option_size + 4];
			void** option_values = new void*[option_size + 4];
			CUjit_target target;

			int i = 0;
			for(jit_options::type::const_iterator it = options.begin(); it != options.end(); ++it, ++i)
			{
				switch(it->first)
				{
				case jit_option::max_register:
					option_types[i] = CU_JIT_MAX_REGISTERS;
					option_values[i] = (void*)(it->second.ui);
					break;
				case jit_option::thread_per_block:
					option_types[i] = CU_JIT_THREADS_PER_BLOCK;
					option_values[i] = (void*)(it->second.ui);
					break;
				case jit_option::optimization_level:
					option_types[i] = CU_JIT_OPTIMIZATION_LEVEL;
					option_values[i] = (void*)(it->second.ui);
					break;
				case jit_option::compilation_time:
					option_types[i] = CU_JIT_WALL_TIME;
					option_values[i] = (void*)(&it->second.f);
					break;
				case jit_option::target_from_current_context:
					option_types[i] = CU_JIT_TARGET_FROM_CUCONTEXT;
					option_values[i] = 0;
					break;
				case jit_option::target:
					option_types[i] = CU_JIT_TARGET;
					switch(it->second.t)
					{
					case jit_target::target_compute_10:
						target = CU_TARGET_COMPUTE_10; break;
					case jit_target::target_compute_11:
						target = CU_TARGET_COMPUTE_11; break;
					case jit_target::target_compute_12:
						target = CU_TARGET_COMPUTE_12; break;
					case jit_target::target_compute_13:
						target = CU_TARGET_COMPUTE_13; break;
					case jit_target::target_compute_20:
						target = CU_TARGET_COMPUTE_20; break;
					}
					option_values[i] = (void*)target;
					break;
				}
			}

			// append option for info log buffer and error log buffer
			unsigned int info_buffer_size = 65536;
			char* info_buffer = new char[info_buffer_size];
			option_types [option_size + 0] = CU_JIT_INFO_LOG_BUFFER_SIZE_BYTES;
			option_values[option_size + 0] = (void*)info_buffer_size;
			option_types [option_size + 1] = CU_JIT_INFO_LOG_BUFFER;
			option_values[option_size + 1] = info_buffer;

			unsigned int error_buffer_size = 65536;
			char* error_buffer = new char[error_buffer_size];
			option_types [option_size + 2] = CU_JIT_ERROR_LOG_BUFFER_SIZE_BYTES;
			option_values[option_size + 2] = (void*)error_buffer_size;
			option_types [option_size + 3] = CU_JIT_ERROR_LOG_BUFFER;
			option_values[option_size + 3] = error_buffer;

			// compile the PTX code
			result = cuModuleLoadDataEx(&m, image, option_size + 4, option_types, option_values);
			//result = cuModuleLoadDataEx(&m, image, option_size + 2, option_types, option_values);
			//result = cuModuleLoadDataEx(&m, image, 0, 0, 0);

			// append the log output
			info_log.clear();
			info_log.append(info_buffer);

			error_log.clear();
			error_log.append(error_buffer);

			// cleanup the temporary buffers
			SAFE_DELETE_ARRAY(info_buffer);
			SAFE_DELETE_ARRAY(error_buffer);

			SAFE_DELETE_ARRAY(option_types);
			SAFE_DELETE_ARRAY(option_values);

			throwOnError(result);

			return new Module(m);
		}
	} execution;
};

extern DriverApi& GetDriverApi();

} } }

#define IGNORE_CUDA_SPECIFIER_ENABLE 0
#include "framework/processor/cuda/CudaSpecifiers.h"
#undef IGNORE_CUDA_SPECIFIER_ENABLE

#endif /* ZILLIANS_FRAMEWORK_PROCESSOR_CUDAAPI_H_ */
