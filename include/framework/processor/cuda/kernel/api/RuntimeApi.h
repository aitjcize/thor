/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_RUNTIMEAPI_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_RUNTIMEAPI_H_

#include "core/FragmentFreeAllocator.h"

#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <driver_types.h>
#include <driver_functions.h>
#include <channel_descriptor.h>
#include <vector_functions.h>

namespace zillians { namespace framework { namespace processor { namespace cuda {

/**
 * RuntimeApi provides an wrapped interface for CUDA Runtime API.
 */
struct RuntimeApi
{
	typedef cudaEvent_t event_type;

	typedef cudaStream_t stream_type;
	const static stream_type default_stream;

	struct location { enum type { host, device }; };
	struct scheduling { enum type { scheduleAuto, scheduleSpin, scheduleYield }; };

	static void throwOnError(const cudaError_t& error)
	{
		if(error != cudaSuccess)
		{
			printf("RuntimeApi exception: %s\n", cudaGetErrorString(error));
			throw std::runtime_error(cudaGetErrorString(error));
		}
	}

	struct DeviceManagement
	{
		inline int countAllDevices() const
		{
			int count = 0;

			cudaError_t error = cudaGetDeviceCount(&count);
			if(error == cudaErrorNoDevice)
				return 0;

			throwOnError(error);

			return count;
		}

		inline void use(int dev) const
		{
			cudaError_t error = cudaSetDevice(dev);
			throwOnError(error);
		}

		inline int current() const
		{
			int dev = 0;

			cudaError_t error = cudaGetDevice(&dev);

			if(error == cudaErrorNoDevice)
				return -1;

			throwOnError(error);

			return dev;
		}

		inline void enableMappedMemory() const
		{
			cudaError_t error = cudaSetDeviceFlags(cudaDeviceMapHost);
			throwOnError(error);
		}

		struct
		{
			inline void probe(int dev) const
			{
				cudaError_t error = cudaDriverGetVersion(&propDriverVersion);
				throwOnError(error);

				error = cudaRuntimeGetVersion(&propRuntimeVersion);
				throwOnError(error);

				error = cudaGetDeviceProperties(&prop, dev);
				throwOnError(error);
			}

			inline std::string name() const
			{
				return prop.name;
			}

			inline std::size_t globalMemorySize() const
			{
				return prop.totalGlobalMem;
			}

			inline std::size_t sharedMemorySizePerBlock() const
			{
				return prop.sharedMemPerBlock;
			}

			inline std::size_t registerMemorySizePerBlock() const
			{
				return prop.regsPerBlock*4;
			}

			inline std::size_t constantMemorySize() const
			{
				return prop.totalConstMem;
			}

			inline int warpSize() const
			{
				return prop.warpSize;
			}

			inline int maxThreadsPerBlock() const
			{
				return prop.maxThreadsPerBlock;
			}

			inline int maxBlockDimX() const
			{
				return prop.maxThreadsDim[0];
			}

			inline int maxBlockDimY() const
			{
				return prop.maxThreadsDim[1];
			}

			inline int maxBlockDimZ() const
			{
				return prop.maxThreadsDim[2];
			}

			inline int maxGridDimX() const
			{
				return prop.maxGridSize[0];
			}

			inline int maxGridDimY() const
			{
				return prop.maxGridSize[1];
			}

			inline int maxGridDimZ() const
			{
				return prop.maxGridSize[2];
			}

			inline int clockRate() const
			{
				return prop.clockRate;
			}

			inline std::pair<int,int> driverVersion() const
			{
				return std::make_pair(propDriverVersion/1000, propDriverVersion%100);
			}

			inline std::pair<int,int> runtimeVersion() const
			{
				return std::make_pair(propRuntimeVersion/1000, propRuntimeVersion%100);
			}

			inline std::pair<int,int> isaVersion() const
			{
				if(propDriverVersion == 3020)
					return std::make_pair(2,2);
				else if(propDriverVersion == 3010)
					return std::make_pair(2,1);
				else
					return std::make_pair(1,4);
			}

			inline std::pair<int,int> computeCapability() const
			{
				return std::make_pair(prop.major, prop.minor);
			}

			inline bool supportOverlappedExecution() const
			{
				return prop.deviceOverlap > 0 ? true : false;
			}

			inline int numMultiProcessors() const
			{
				return prop.multiProcessorCount;
			}

			inline int numCorePerMultiProcessor() const
			{
			    // Defines for GPU Architecture types (using the SM version to determine the # of cores per SM
			    typedef struct {
			        int SM; // 0xMm (hexidecimal notation), M = SM Major version, and m = SM minor version
			        int Cores;
			    } sSMtoCores;

				sSMtoCores nGpuArchCoresPerSM[] =
				{ { 0x10,  8 },
				  { 0x11,  8 },
				  { 0x12,  8 },
				  { 0x13,  8 },
				  { 0x20, 32 },
				  { 0x21, 48 },
				  {   -1, -1 }
				};

			    int index = 0;
			    while (nGpuArchCoresPerSM[index].SM != -1) {
			        if (nGpuArchCoresPerSM[index].SM == ((prop.major << 4) + prop.minor) ) {
			            return nGpuArchCoresPerSM[index].Cores;
			        }
			        index++;
			    }

			    throwOnError(cudaErrorInvalidConfiguration);
			    return -1;
			}

			inline bool hasKernelExecutionTimeLimit() const
			{
				return prop.kernelExecTimeoutEnabled > 0 ? true : false;
			}

			inline bool supportMappedMemory() const
			{
				return prop.canMapHostMemory > 0 ? true : false;
			}

			inline bool supportECCMemory() const
			{
				return prop.ECCEnabled > 0 ? true : false;
			}

			inline bool supportConcurrentExecution() const
			{
				return prop.concurrentKernels > 0 ? true : false;
			}

			mutable cudaDeviceProp prop;
			mutable int propDriverVersion;
			mutable int propRuntimeVersion;
		} properties;
	} device;

	struct EventManagement
	{
		inline void create(event_type* e) const
		{
			cudaError_t error = cudaEventCreate(e);
			throwOnError(error);
		}

		inline void destroy(const event_type& e) const
		{
			cudaError_t error = cudaEventDestroy(const_cast<event_type&>(e));
			throwOnError(error);
		}

		inline void record(const event_type& e, const stream_type& s = default_stream) const
		{
			cudaError_t error = cudaEventRecord(const_cast<event_type&>(e), const_cast<stream_type&>(s));
			throwOnError(error);
		}

		inline bool queryCompleted(const event_type& e) const
		{
			cudaError_t error = cudaEventQuery(const_cast<event_type&>(e));

			if(error == cudaSuccess)
			{
				return true;
			}
			else if(error == cudaErrorNotReady)
			{
				return false;
			}
			else
			{
				throwOnError(error);
				return false;
			}
		}

		inline void synchronize(const event_type& e) const
		{
			cudaError_t error = cudaEventSynchronize(const_cast<event_type&>(e));
			throwOnError(error);
		}

		inline float queryElapsed(const event_type& start, const event_type& stop) const
		{
			float elapsed = 0.0f;

			cudaError_t error = cudaEventElapsedTime(&elapsed, start, stop);
			throwOnError(error);

			return elapsed;
		}
	} event;

	struct StreamManagement
	{
		inline void create(stream_type* s) const
		{
			cudaError_t error = cudaStreamCreate(s);
			throwOnError(error);
		}

		inline void destroy(const stream_type& s) const
		{
			cudaError_t error = cudaStreamDestroy(const_cast<stream_type&>(s));
			throwOnError(error);
		}

		inline bool queryCompleted(const stream_type& s) const
		{
			cudaError_t error = cudaStreamQuery(const_cast<stream_type&>(s));

			if(error == cudaSuccess)
			{
				return true;
			}
			else if(error == cudaErrorNotReady)
			{
				return false;
			}
			else
			{
				throwOnError(error);
				return false;
			}
		}

		inline void synchronize(const stream_type& s) const
		{
			cudaError_t error = cudaStreamSynchronize(const_cast<stream_type&>(s));
			throwOnError(error);
		}
	} stream;

	struct Texture
	{
		cudaArray* array;
		uint8 dimension;
		cudaChannelFormatDesc desc;
		cudaExtent extent;
		std::size_t element_size;
		bool binded;
		const textureReference* texref;

		inline std::size_t width()  const { return extent.width;  }
		inline std::size_t height() const { return extent.height; }
		inline std::size_t depth()  const { return extent.depth;  }

		inline bool unbind()
		{
			if(binded)
			{
				binded = false;

				cudaError_t error = cudaUnbindTexture(texref);

				texref = NULL;
				throwOnError(error);

				return true;
			}

			return false;
		}

		inline bool bind(const char* name)
		{
			cudaError_t error = cudaGetTextureReference(&texref, name);
			throwOnError(error);

			error = cudaBindTextureToArray(texref, array, &desc);
			throwOnError(error);

			binded = true;

			return true;
		}

		inline void copyFrom(MutablePointer* src, std::size_t src_offset, std::size_t size, const location::type& src_type)
		{
			copyFromRaw(src->data(), src_offset, size, src_type);
		}

		inline void copyFromRaw(byte* src, std::size_t src_offset, std::size_t size, const location::type& src_type)
		{
			BOOST_ASSERT(src != NULL);
			BOOST_ASSERT(size > 0 && size <= extent.width * extent.height * extent.depth * element_size);
			cudaMemcpyKind kind;
			cudaError_t error;

			// find out the copy type by the source and destination memory location
			if(src_type == location::host)
			{
				kind = cudaMemcpyHostToDevice;
			}
			else // src_type == location::device
			{
				kind = cudaMemcpyDeviceToDevice;
			}

			if(dimension == 1)
			{
				error = cudaMemcpyToArray(array, 0, 0, src + src_offset, size, kind);
				throwOnError(error);
			}
			else if(dimension == 2)
			{
				error = cudaMemcpyToArray(array, 0, 0, src + src_offset, size, kind);
				throwOnError(error);
			}
			else
			{
				cudaMemcpy3DParms param = { 0 };
			    param.dstArray = array;
			    param.dstPos = make_cudaPos(0,0,0);
			    param.srcPtr = make_cudaPitchedPtr((void*)src, extent.width * element_size, extent.width, extent.height);
			    param.srcPos = make_cudaPos(0,0,0);
			    param.kind = kind;
			    param.extent = extent;

				error = cudaMemcpy3D(&param);
				throwOnError(error);
			}
		}

		inline void copyTo(MutablePointer* dest, std::size_t dest_offset, std::size_t size, const location::type& dest_type)
		{
			copyToRaw(dest->data(), dest_offset, size, dest_type);
		}

		inline void copyToRaw(byte* dest, std::size_t dest_offset, std::size_t size, const location::type& dest_type)
		{
			BOOST_ASSERT(dest != NULL);
			BOOST_ASSERT(size > 0 && size <= extent.width * extent.height * extent.depth * element_size);
			cudaMemcpyKind kind;
			cudaError_t error;

			// find out the copy type by the source and destination memory location
			if(dest_type == location::host)
			{
				kind = cudaMemcpyDeviceToHost;
			}
			else // dest_type == location::device
			{
				kind = cudaMemcpyDeviceToDevice;
			}

			if(dimension == 1)
			{
				error = cudaMemcpyFromArray(dest + dest_offset, array, 0, 0, size, kind);
				throwOnError(error);
			}
			else if(dimension == 2)
			{
				error = cudaMemcpyFromArray(dest + dest_offset, array, 0, 0, size, kind);
				throwOnError(error);
			}
			else
			{
				cudaMemcpy3DParms param = { 0 };
				param.dstPtr = make_cudaPitchedPtr((void*)dest, extent.width * element_size, extent.width, extent.height);;
			    param.dstPos = make_cudaPos(0,0,0);
			    param.srcArray = array;
			    param.srcPos = make_cudaPos(0,0,0);
			    param.kind = kind;
			    param.extent = extent;

				error = cudaMemcpy3D(&param);
				throwOnError(error);
			}
		}
	};

	struct TextureManagement
	{
		template <typename T>
		inline void create1D(std::size_t width, Texture** texture) const
		{
			BOOST_ASSERT(texture != NULL);

			*texture = new Texture;

			(*texture)->dimension = 1;
			(*texture)->desc = cudaCreateChannelDesc<T>();
			(*texture)->extent.width  = width;
			(*texture)->extent.height = 1;
			(*texture)->extent.depth  = 1;
			(*texture)->element_size  = sizeof(T);
			(*texture)->binded        = false;

			cudaError_t error = cudaMallocArray(&((*texture)->array), &((*texture)->desc), width, 1);
			if(error != cudaSuccess)
			{
				SAFE_DELETE(*texture);
				throwOnError(error);
			}
		}

		template <typename T>
		inline void create2D(std::size_t width, std::size_t height, Texture** texture) const
		{
			BOOST_ASSERT(texture != NULL);

			*texture = new Texture;

			(*texture)->dimension = 2;
			(*texture)->desc = cudaCreateChannelDesc<T>();
			(*texture)->extent.width  = width;
			(*texture)->extent.height = height;
			(*texture)->extent.depth  = 1;
			(*texture)->element_size  = sizeof(T);
			(*texture)->binded        = false;

			cudaError_t error = cudaMallocArray(&((*texture)->array), &((*texture)->desc), width, height);
			if(error != cudaSuccess)
			{
				SAFE_DELETE(*texture);
				throwOnError(error);
			}
		}

		template <typename T>
		inline void create3D(std::size_t width, std::size_t height, std::size_t depth, Texture** texture) const
		{
			*texture = new Texture;

			(*texture)->dimension = 3;
			(*texture)->desc = cudaCreateChannelDesc<T>();
			(*texture)->extent.width  = width;
			(*texture)->extent.height = height;
			(*texture)->extent.depth  = depth;
			(*texture)->element_size  = sizeof(T);
			(*texture)->binded        = false;

			cudaError_t error = cudaMalloc3DArray(&((*texture)->array), &((*texture)->desc), (*texture)->extent);
			if(error != cudaSuccess)
			{
				SAFE_DELETE(*texture);
				throwOnError(error);
			}
		}

		inline void destroy(Texture* texture) const
		{
			BOOST_ASSERT(texture != NULL);

			cudaError_t error = cudaFreeArray(texture->array);
			SAFE_DELETE(texture);

			if(error != cudaSuccess)
			{
				throwOnError(error);
			}
		}

		template <typename T>
		inline size_t bind1D(const char* name, MutablePointer* devicePointer, std::size_t size)
		{
			return bind1D<T>(name, devicePointer->data(), size);
		}

		template <typename T>
		inline size_t bind1D(const char* name, byte* devicePointerRaw, std::size_t size)
		{
			const textureReference* texref;
			cudaError_t error = cudaGetTextureReference(&texref, name);
			throwOnError(error);

			cudaChannelFormatDesc desc = cudaCreateChannelDesc<T>();

			std::size_t offset = 0;
			error = cudaBindTexture(&offset, texref, devicePointerRaw, &desc, size);
			throwOnError(error);

			return offset;
		}

	} texture;

	struct MemoryManagement
	{
		struct HostMemoryManagement
		{
			HostMemoryManagement()
			{
				// About the allocators/reservedMemories array...
				// [0]: pagelock = false
				// [1]: pagelock = true, portable = false, mapped = false, writecombined = false
				// [2]: pagelock = true, portable = false, mapped = false, writecombined = true
				// [3]: pagelock = true, portable = false, mapped = true,  writecombined = false
				// [4]: pagelock = true, portable = false, mapped = true,  writecombined = true
				// [5]: pagelock = true, portable = true,  mapped = false, writecombined = false
				// [6]: pagelock = true, portable = true,  mapped = false, writecombined = true
				// [7]: pagelock = true, portable = true,  mapped = true,  writecombined = false
				// [8]: pagelock = true, portable = true,  mapped = true,  writecombined = true

				allocators[0] = NULL;
				allocators[1] = NULL;
				allocators[2] = NULL;
				allocators[3] = NULL;
				allocators[4] = NULL;
				allocators[5] = NULL;
				allocators[6] = NULL;
				allocators[7] = NULL;
				allocators[8] = NULL;

				reservedMemories[0] = NULL;
				reservedMemories[1] = NULL;
				reservedMemories[2] = NULL;
				reservedMemories[3] = NULL;
				reservedMemories[4] = NULL;
				reservedMemories[5] = NULL;
				reservedMemories[6] = NULL;
				reservedMemories[7] = NULL;
				reservedMemories[8] = NULL;
			}

			~HostMemoryManagement()
			{
				SAFE_DELETE(allocators[0]);
				SAFE_DELETE(allocators[1]);
				SAFE_DELETE(allocators[2]);
				SAFE_DELETE(allocators[3]);
				SAFE_DELETE(allocators[4]);
				SAFE_DELETE(allocators[5]);
				SAFE_DELETE(allocators[6]);
				SAFE_DELETE(allocators[7]);
				SAFE_DELETE(allocators[8]);

				if(reservedMemories[0]) freeRaw(reservedMemories[0], false);
				if(reservedMemories[1]) freeRaw(reservedMemories[1], true);
				if(reservedMemories[2]) freeRaw(reservedMemories[2], true);
				if(reservedMemories[3]) freeRaw(reservedMemories[3], true);
				if(reservedMemories[4]) freeRaw(reservedMemories[4], true);
				if(reservedMemories[5]) freeRaw(reservedMemories[5], true);
				if(reservedMemories[6]) freeRaw(reservedMemories[6], true);
				if(reservedMemories[7]) freeRaw(reservedMemories[7], true);
				if(reservedMemories[8]) freeRaw(reservedMemories[8], true);
			}

			inline void reserve(std::size_t size, bool pagelocked = false, bool portable = false, bool mapped = false, bool writecombined = false) const
			{
				uint8 key = keyForReserved(pagelocked, portable, mapped, writecombined);

				SAFE_DELETE(allocators[key]);
				if(reservedMemories[key]) freeRaw(reservedMemories[key], pagelocked); reservedMemories[key] = NULL;

				if(size > 0)
				{
					allocateRaw(&reservedMemories[key], size, pagelocked, portable, mapped, writecombined);
					allocators[key] = new FragmentAllocator(reservedMemories[key], size);
				}
			}

			inline void allocate(MutablePointer** pointer, std::size_t size, bool from_reserved = false, bool pagelocked = false, bool portable = false, bool mapped = false, bool writecombined = false) const
			{
				BOOST_ASSERT(pointer != NULL);

				if(from_reserved)
				{
					uint8 key = keyForReserved(pagelocked, portable, mapped, writecombined);
					FragmentAllocator* allocator = allocators[key];
					if(UNLIKELY(!allocator))
					{
						throw std::runtime_error("Invalid allocator, must call reserved() before using memory from pool");
					}

					if(!allocator->allocate(pointer, size))
					{
						throw std::runtime_error("Fail to allocate from pool, out of memory");
					}
				}
				else
				{
					byte* data = NULL;
					allocateRaw(&data, size, pagelocked, portable, mapped, writecombined);
					*pointer = new MutablePointer(data);
				}
			}

			inline void allocateRaw(byte** pointer, std::size_t size, bool pagelocked = false, bool portable = false, bool mapped = false, bool writecombined = false) const
			{
				BOOST_ASSERT(pointer != NULL);

				if(pagelocked)
				{
					unsigned int flags = cudaHostAllocDefault;

					if(portable)      flags |= cudaHostAllocPortable;
					if(mapped)        flags |= cudaHostAllocMapped;
					if(writecombined) flags |= cudaHostAllocWriteCombined;

					cudaError_t error = cudaHostAlloc(reinterpret_cast<void**>(pointer), size, flags);
					throwOnError(error);
				}
				else
				{
					BOOST_ASSERT(!mapped);
					BOOST_ASSERT(!writecombined);

					*pointer = reinterpret_cast<byte*>(::malloc(size));
				}
			}

			inline void free(MutablePointer* pointer, bool from_reserved = false, bool pagelocked = false, bool portable = false, bool mapped = false, bool writecombined = false) const
			{
				BOOST_ASSERT(pointer != NULL);

				if(from_reserved)
				{
					uint8 key = keyForReserved(pagelocked, portable, mapped, writecombined);
					FragmentAllocator* allocator = allocators[key];
					if(UNLIKELY(!allocator))
					{
						throw std::runtime_error("Invalid allocator, must call reserved() before using memory from pool");
					}

					if(!allocator->deallocate(pointer))
					{
						throw std::runtime_error("Failed to deallocate, might be invalid pointer");
					}
				}
				else
				{
					freeRaw(pointer->data(), pagelocked);
					delete pointer;
				}
			}

			inline void freeRaw(byte* pointer, bool pagelocked = false) const
			{
				BOOST_ASSERT(pointer != NULL);

				if(pagelocked)
				{
					cudaError_t error = cudaFreeHost(pointer);
					throwOnError(error);
				}
				else
				{
					::free(reinterpret_cast<void*>(pointer));
				}
			}

			inline void set(byte* data, int value, std::size_t n) const
			{
				::memset(data, value, n);
			}

			inline void mapToDevice(MutablePointer** device, MutablePointer* host) const
			{
				byte* pointer = NULL;
				mapToDeviceRaw(&pointer, host->data());
				*device = new MutablePointer(pointer);
			}

			inline void mapToDeviceRaw(byte** device, byte* host) const
			{
				cudaError_t error = cudaHostGetDevicePointer(reinterpret_cast<void**>(device), reinterpret_cast<void*>(host), 0);
				throwOnError(error);
			}
		private:
			inline uint8 keyForReserved(bool pagelocked, bool portable, bool mapped, bool writecombined) const
			{
				uint8 key = 0;
				if(pagelocked) key += 1; if(writecombined) key += 1; if(mapped) key += 2; if(portable) key += 4;
				return key;
			}

			mutable FragmentAllocator* allocators[9];
			mutable byte* reservedMemories[9];
		} host;

		struct DeviceMemoryManagement
		{
			DeviceMemoryManagement()
			{
				allocator = NULL;
				reservedMemory = NULL;
			}

			~DeviceMemoryManagement()
			{
				SAFE_DELETE(allocator);
				if(reservedMemory) freeRaw(reservedMemory); reservedMemory = NULL;
			}

			inline void reserve(std::size_t size) const
			{
				SAFE_DELETE(allocator);
				if(reservedMemory) freeRaw(reservedMemory); reservedMemory = NULL;

				if(size > 0)
				{
					allocateRaw(&reservedMemory, size);
					allocator = new FragmentAllocator(reservedMemory, size);
				}
			}

			inline void allocate(MutablePointer** pointer, std::size_t size, bool from_reserved = false) const
			{
				if(from_reserved)
				{
					if(UNLIKELY(!allocator))
					{
						throw std::runtime_error("Invalid allocator, must call reserved() before using memory from pool");
					}

					if(!allocator->allocate(pointer, size))
					{
						throw std::runtime_error("Fail to allocate from pool, out of memory");
					}
				}
				else
				{
					byte* data = NULL;
					allocateRaw(&data, size);
					*pointer = new MutablePointer(data);
				}
			}

			// CODEREVIEW: Why need this? Need more documentation or just remove it!
			inline void allocateRaw(byte** pointer, std::size_t size) const
			{
				cudaError_t error = cudaMalloc(reinterpret_cast<void**>(pointer), size);
				throwOnError(error);
			}

			inline void free(MutablePointer* pointer, bool from_reserved = false) const
			{
				if(from_reserved)
				{
					if(UNLIKELY(!allocator))
					{
						throw std::runtime_error("Invalid allocator, must call reserved() before using memory from pool");
					}

					if(!allocator->deallocate(pointer))
					{
						throw std::runtime_error("Failed to deallocate, might be invalid pointer");
					}
				}
				else
				{
					freeRaw(pointer->data());
					delete pointer;
				}
			}

			// CODEREVIEW: Why need this? Need more documentation or just remove it!
			inline void freeRaw(byte* pointer) const
			{
				cudaError_t error = cudaFree(reinterpret_cast<void*>(pointer));
				throwOnError(error);
			}

			inline void set(MutablePointer* pointer, int value, std::size_t n) const
			{
				setRaw(pointer->data(), value, n);
			}

			inline void setRaw(byte* pointer, int value, std::size_t n) const
			{
				cudaError_t error = cudaMemset(reinterpret_cast<void*>(pointer), value, n);
				throwOnError(error);
			}

		private:
			mutable FragmentAllocator* allocator;
			mutable byte* reservedMemory;
		} device;

		template<typename T>
		inline void copyToSymbol(const char* symbol_name, T& src) const
		{
			cudaError_t error = cudaMemcpyToSymbol(symbol_name, &src, sizeof(T), 0, cudaMemcpyHostToDevice);
			throwOnError(error);
		}

		template<typename T>
		inline void copyToSymbolAsync(const char* symbol_name, T& src, const stream_type& stream) const
		{
			cudaError_t error = cudaMemcpyToSymbolAsync(symbol_name, &src, sizeof(T), 0, cudaMemcpyHostToDevice, stream);
			throwOnError(error);
		}

		inline void copy(MutablePointer* dest, MutablePointer* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type) const
		{
			copyRaw(dest->data(), src->data(), dest_offset, src_offset, size, dest_type, src_type);
		}

		inline void copyRaw(byte* dest, const byte* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type) const
		{
			BOOST_ASSERT(size > 0);

			if(dest == src)
				return;

			cudaMemcpyKind kind;

			// find out the copy type by the source and destination memory location
			if(dest_type == location::host)
			{
				if(src_type == location::host)
				{
					kind = cudaMemcpyHostToHost;
				}
				else // src_type == location::device
				{
					kind = cudaMemcpyDeviceToHost;
				}
			}
			else // dest_type == location::device
			{
				if(src_type == location::host)
				{
					kind = cudaMemcpyHostToDevice;
				}
				else // src_type == location::device
				{
					kind = cudaMemcpyDeviceToDevice;
				}
			}

			// since the memory copy does not support overlapped memory region, we need to handle this carefully
			// the limit only apply in same memory context (i.e. host to host or device to device)
			if(kind == cudaMemcpyDeviceToDevice || kind == cudaMemcpyHostToHost)
			{
				if(src < dest && src + size > dest)
				{
					throw std::range_error("Overlapped memory copy is not supported");
				}
				else if(dest < src && dest + size > src)
				{
					throw std::range_error("Overlapped memory copy is not supported");
				}
			}

			cudaError_t error = cudaMemcpy(dest + dest_offset, src + src_offset, size, kind);
			throwOnError(error);
		}

		inline void copyAsync(MutablePointer* dest, MutablePointer* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type, const stream_type& stream) const
		{
			copyAsyncRaw(dest->data(), src->data(), dest_offset, src_offset, size, dest_type, src_type, stream);
		}

		inline void copyAsyncRaw(byte* dest, const byte* src, std::size_t dest_offset, std::size_t src_offset, std::size_t size, const location::type& dest_type, const location::type& src_type, const stream_type& stream) const
		{
			BOOST_ASSERT(size > 0);

			if(dest == src)
				return;

			cudaMemcpyKind kind;

			// find out the copy type by the source and destination memory location
			if(dest_type == location::host)
			{
				if(src_type == location::host)
				{
					kind = cudaMemcpyHostToHost;
				}
				else // src_type == location::device
				{
					kind = cudaMemcpyDeviceToHost;
				}
			}
			else // dest_type == location::device
			{
				if(src_type == location::host)
				{
					kind = cudaMemcpyHostToDevice;
				}
				else // src_type == location::device
				{
					kind = cudaMemcpyDeviceToDevice;
				}
			}

			// since the memory copy does not support overlapped memory region, we need to handle this carefully
			// the limit only apply in same memory context (i.e. host to host or device to device)
			if(kind == cudaMemcpyDeviceToDevice || kind == cudaMemcpyHostToHost)
			{
				if(src < dest && src + size > dest)
				{
					throw std::range_error("Overlapped memory copy is not supported");
				}
				else if(dest < src && dest + size > src)
				{
					throw std::range_error("Overlapped memory copy is not supported");
				}
			}

			cudaError_t error = cudaMemcpyAsync(dest + dest_offset, src + src_offset, size, kind, stream);
			throwOnError(error);
		}
	} memory;

	struct ExecutionManagement
	{
		inline void synchronize() const
		{
			cudaError_t error = cudaThreadSynchronize();
			throwOnError(error);
		}

		inline void configureKernel(const dim3& grid, const dim3& block, std::size_t shared_memory, stream_type& s) const
		{
			cudaError_t error = cudaConfigureCall(grid, block, shared_memory, s);
			throwOnError(error);
		}

		inline void setupParameter(const byte* args, std::size_t size, std::size_t offset) const
		{
			cudaError_t error = cudaSetupArgument(args, size, offset);
			throwOnError(error);
		}

		inline void launchKernel(const char* name) const
		{
			cudaError_t error = cudaLaunch(name);
			throwOnError(error);
		}

		struct FunctionTraits
		{
			inline void probe(const char* name) const
			{
				cudaError_t error = cudaFuncGetAttributes(&attr, name);
				throwOnError(error);
			}

			inline std::size_t constantMemorySize() const
			{
				return attr.constSizeBytes;
			}

			inline std::size_t sharedMemorySize() const
			{
				return attr.sharedSizeBytes;
			}

			inline std::size_t localMemorySize() const
			{
				return attr.localSizeBytes;
			}

			inline int maxThreadsPerBlock() const
			{
				return attr.maxThreadsPerBlock;
			}

			inline int registerUsed() const
			{
				return attr.numRegs;
			}

			mutable cudaFuncAttributes attr;
		} function_traits;
	} execution;
};

extern RuntimeApi& GetRuntimeApi();

} } } }

#endif/*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_RUNTIMEAPI_H_*/
