/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_H_

#include "core/Prerequisite.h"
#include "core/FragmentFreeAllocator.h"
#include "framework/processor/cuda/kernel/ArrayCommon.h"

/**
 * The underlying KernelApi may provide pooled allocation, that is, to
 * allocate from pool instead of the runtime memory management. So, to enable
 * the pooled allocation support of all HostArray instances, you have to
 * set the flag to 1 (true); otherwise set it to 0 (false).
 */
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_ENABLE_POOL_ALLOCATION 0

/**
 * Since you don't read/write the data directly through HostArray but use
 * ArrayHandle, each ArrayHandle tries to acquire the actual data pointer
 * in the constructor, and release the data pointer in the destructor. So
 * when the HostArray goes to destruct itself, the acquire count should
 * equal to zero, which means there's no ArrayHandle instance referencing to
 * it. You may enable the acquire check to track the acquire/release count
 * to debug some memory issue by setting this flag to 1 (true); otherwise
 * set it to 0 (false).
 */
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_CHECK_ACQUIRE_RELEASE 1

#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_CHECK_ACQUIRE_RELEASE
#include <tbb/atomic.h>
#endif

namespace zillians { namespace framework { namespace processor { namespace cuda {

/**
 * @brief MappedArray represent an device-mapped array on the host and device.
 *
 * MappedArray wraps the latest CUDA 1.3 feature which support DMA to access
 * host memory directly (no need to copy the data to the device). By using
 * mapped memory we can overlap the computation and data transfer time to
 * improve computation efficiency.
 *
 * @note MappedArray is always a page-locked memory on the host (CPU) and can be
 * accessed directly on the device (GPU).
 *
 * @note MappedArray is a templated class which takes the KernelApi as the template
 * parameter to define the underlying kernel api. So by giving different
 * KernelApi (i.e. RuntimeApi or DriverApi for now, we may add OpenCLApi or
 * more kernels later), you can customize the memory management scheme of the
 * array.
 *
 * @note Usually you don't manipulate the array data through HostArray directly,
 * but instead, you have to use ArrayHandle to obtain the pointer for actual
 * data access.
 *
 * @see RuntimeApi, DriverApi, ArrayHandle
 */
template<typename KernelApi>
class MappedArray
{
	template<typename ArrayType> friend class ArrayHandle;
public:
	typedef KernelApi kernel;

	/**
	 * @brief Construct a MappedArray.
	 *
	 * @param api The underlying kernel reference.
	 * @param size The size of the array in bytes.
	 * @param portable True for portable array (see CUDA portable pinned memory). False otherwise.
	 * @param writecombined True for write-combined (see CUDA write-combined memory) array. False otherwise.
	 */
	MappedArray(const KernelApi& api, std::size_t size, bool portable = false, bool writecombined = false) :
		mKernelApi(api),
		mDataOwner(true), mHost(NULL), mDevice(NULL), mAllocatedSize(size),
		mPortable(portable), mWriteCombined(writecombined)
	{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_CHECK_ACQUIRE_RELEASE
		mAcquireCount = 0;
#endif

#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_ENABLE_POOL_ALLOCATION
		mKernelApi.memory.host.allocate(&mHost, mAllocatedSize, true, true, mPortable, true, mWriteCombined);
#else
		mKernelApi.memory.host.allocate(&mHost, mAllocatedSize, false, true, mPortable, true, mWriteCombined);
#endif
		mKernelApi.memory.host.mapToDevice(&mDevice, mHost);
	}

	/**
	 * @brief Construct a MappedArray from another MappedArray (copy constructor).
	 *
	 * When constructing a MappedArray instance by using copy constructor,
	 * the actual data of the given referenced instance will be copied.
	 *
	 * @param reference The referenced instance.
	 */
	MappedArray(const MappedArray& reference) :
		mKernelApi(reference.mKernelApi),
		mDataOwner(true), mHost(NULL), mDevice(NULL), mAllocatedSize(reference.mAllocatedSize),
		mPortable(reference.mPortable), mWriteCombined(reference.mWriteCombined)
	{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_CHECK_ACQUIRE_RELEASE
		mAcquireCount = 0;
#endif

#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_ENABLE_POOL_ALLOCATION
		mKernelApi.memory.host.allocate(&mHost, mAllocatedSize, true, true, mPortable, true, mWriteCombined);
#else
		mKernelApi.memory.host.allocate(&mHost, mAllocatedSize, false, true, mPortable, true, mWriteCombined);
#endif
		mKernelApi.memory.host.mapToDevice(&mDevice, mHost);
		mKernelApi.memory.copy(mHost, reference.mHost, 0, 0, mAllocatedSize, KernelApi::location::host, KernelApi::location::host);
	}

	/**
	 * @brief Construct a MappedArray from foreign pointer.
	 *
	 * When constructing a MappedArray instance by using copy constructor,
	 * the actual data of the given referenced instance will be copied.
	 *
	 * @param reference The referenced instance.
	 */
	MappedArray(const KernelApi& api, byte* data, std::size_t size, bool portable = false, bool writecombined = false) :
		mKernelApi(api),
		mDataOwner(false), mHost(new MutablePointer(data)), mDevice(NULL), mAllocatedSize(size),
		mPortable(portable), mWriteCombined(writecombined)
	{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_CHECK_ACQUIRE_RELEASE
		mAcquireCount = 0;
#endif
		mKernelApi.memory.host.mapToDevice(&mDevice, mHost);
	}

	/**
	 * @brief Destruct a MappedArray.
	 */
	~MappedArray()
	{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_CHECK_ACQUIRE_RELEASE
		BOOST_ASSERT(mAcquireCount == 0);
#endif
		if(mDataOwner)
		{
			if(mHost)
			{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_ENABLE_POOL_ALLOCATION
				mKernelApi.memory.host.free(mHost, true, true, mPortable, true, mWriteCombined);
#else
				mKernelApi.memory.host.free(mHost, false, true, mPortable, true, mWriteCombined);
#endif
				SAFE_NULL(mHost);
			}
			if(mDevice)
			{
				SAFE_DELETE(mDevice);
			}
		}
		else
		{
			SAFE_DELETE(mHost);
			SAFE_DELETE(mDevice);
		}
		mAllocatedSize = 0;
	}

	/**
	 * @brief Return the size of the array.
	 *
	 * @return The size of the array in bytes.
	 */
	inline std::size_t size() const
	{ return mAllocatedSize; }

	friend std::ostream &operator << (std::ostream &stream, const MappedArray<KernelApi>& object)
	{
		stream << "[ HOST PTR =" << (void*)object.mHost->data() << ", MAPPED PTR = " << (void*)object.mDevice->data() << ", SIZE = " << object.mAllocatedSize << " ]";
		return stream;
	}

//private:
	/**
	 * @brief Acquire the actual data pointer for given access location and access mode.
	 *
	 * @note There can be only one access at a time (i.e. no further acquire
	 * can be called before releasing the previous one).
	 *
	 * @note We're going to support multiple acquire calls for different access
	 * location.
	 *
	 * @param location The location that we are going to access.
	 * @param mode The read/write mode request for the access.
	 * @param device If the access location is device, this parameter is used to tell which GPU device. Not implemented right now.
	 * @return The accessible data pointer.
	 */
	inline MutablePointer* acquire(const accessLocation::type location, const accessMode::type mode, std::size_t offset, std::size_t size, uint32 device)
	{
		BOOST_ASSERT(size > 0);
		BOOST_ASSERT(mAllocatedSize >= offset + size);
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_CHECK_ACQUIRE_RELEASE
		++mAcquireCount;
#endif
		if(location == accessLocation::host)
		{
			return new MutablePointer(*mHost, offset);
		}
		else
		{
			return new MutablePointer(*mDevice, offset);
		}
	}

	/**
	 * @brief Release the acquired data pointer.
	 *
	 * @param pointer The pointer to be released.
	 * @param location The access location of the pointer. Must be consistent "location" in acquire().
	 * @param mode The access mode of the pointer, Must be consistent "mode" in acquire().
	 * @param device The GPU device index for device access.
	 * @param dirty The flag to identify the content of the pointer is changed from outside.
	 */
	inline void release(MutablePointer* pointer, const accessLocation::type location, const accessMode::type mode, std::size_t offset, std::size_t size, uint32 device, bool dirty)
	{
		BOOST_ASSERT(size > 0);
		BOOST_ASSERT(mAllocatedSize >= offset + size);
		BOOST_ASSERT(pointer != NULL);
		BOOST_ASSERT(!dirty || (dirty && mode != accessMode::read));

#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_CHECK_ACQUIRE_RELEASE
		--mAcquireCount;
		BOOST_ASSERT(mAcquireCount >= 0);
#endif
		delete pointer;
	}

	/**
	 * @brief Commit all changes in the acquired data pointer without releasing it.
	 *
	 * @param pointer The pointer acquired.
	 * @param location The access location of the pointer. Must be consistent "location" in acquire().
	 * @param mode The access mode of the pointer, Must be consistent "mode" in acquire().
	 * @param device The GPU device index for device access.
	 * @param dirty The flag to identify the content of the pointer is changed from outside.
	 */
	inline void commit(MutablePointer* pointer, const accessLocation::type location, const accessMode::type mode, std::size_t offset, std::size_t size, uint32 device, bool dirty)
	{
		BOOST_ASSERT(size > 0);
		BOOST_ASSERT(mAllocatedSize >= offset + size);
		BOOST_ASSERT(pointer != NULL);
		BOOST_ASSERT(!dirty || (dirty && mode != accessMode::read));
	}

	/**
	 * @brief Reload the data from the referenced array without re-allocate the buffer.
	 *
	 * @note The given parameters must be the same with those in acquire()
	 *
	 * @param pointer The pointer acquired.
	 * @param location The access location of the pointer. Must be consistent "location" in acquire().
	 * @param mode The access mode of the pointer, Must be consistent "mode" in acquire().
	 * @param device The GPU device index for device access.
	 */
	inline void reload(MutablePointer* pointer, const accessLocation::type location, const accessMode::type mode, std::size_t offset, std::size_t size, uint32 device)
	{
		BOOST_ASSERT(size > 0);
		BOOST_ASSERT(mAllocatedSize >= offset + size);
		BOOST_ASSERT(pointer != NULL);
	}

	/// The underlying kernel reference.
	const KernelApi& mKernelApi;

	/// Flag to indicate whether the array itself owns the pointer
	bool mDataOwner;

	/// The actual host data pointer.
	MutablePointer* mHost;

	/// The actual mapped pointer on the device (GPU).
	MutablePointer* mDevice;

	/// The size of the array.
	std::size_t mAllocatedSize;

	/// True for portable array (see CUDA portable pinned memory). False otherwise.
	bool mPortable;

	/// True for write-combined (see CUDA write-combined memory) array. False otherwise.
	bool mWriteCombined;

#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_CHECK_ACQUIRE_RELEASE
	/// The counter to keep tract of acquire/release calls
	tbb::atomic<long> mAcquireCount;
#endif
};


} } } }

#endif/*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MAPPEDARRAY_H_*/
