/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_VW_CUDASPECIFIER_H_
#define ZILLIANS_VW_CUDASPECIFIER_H_

#ifndef __CUDACC__

#ifndef __decl_host
#define __decl_host
#endif

#ifndef __decl_device
#define __decl_device
#endif

#ifndef __decl_global
#define __decl_global
#endif

#ifndef __decl_shared
#define __decl_shared
#endif

#ifndef __decl_noinline
#define __decl_noinline __attribute__((noinline))
#endif

#ifndef __decl_forceinline
#define __decl_forceinline
#endif

#ifndef __decl_constant
#define __decl_constant
#endif

#else

#ifndef __decl_host
#define __decl_host __host__
#endif

#ifndef __decl_device
#define __decl_device __device__
#endif

#ifndef __decl_global
#define __decl_global __global__
#endif

#ifndef __decl_shared
#define __decl_shared __shared__
#endif

#ifndef __decl_noinline
#define __decl_noinline __noinline__
#endif

#ifndef __decl_forceinline
#define __decl_forceinline __forceinline__
#endif

#ifndef __decl_constant
#define __decl_constant __constant__
#endif

#endif

#define ENABLE_LAUNCH_CHECK_ON_EVERY_KERNEL 1

#if ENABLE_LAUNCH_CHECK_ON_EVERY_KERNEL

#define CHECK_LAUNCH(kernel) \
	{ \
		cudaError_t result = cudaDeviceSynchronize(); \
		if(result != cudaSuccess) \
		{ \
			printf("\t--> [KernelLaunch] failed to launch \"%s\" kernel, result = %s\n", #kernel, cudaGetErrorString(result)); \
			BOOST_ASSERT(false); \
		} \
		else \
		{ \
			/*printf("\t--> [KernelLaunch] successfully launched \"%s\" kernel\n", #kernel);*/ \
		} \
	}

#else
#define CHECK_LAUNCH(kernel)
#endif

#endif
