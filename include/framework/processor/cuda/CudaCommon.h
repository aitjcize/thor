/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_KERNEL_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_KERNEL_H_

#include "framework/processor/cuda/kernel/api/RuntimeApi.h"
#include "framework/processor/cuda/kernel/DeviceArray.h"
#include "framework/processor/cuda/kernel/HostArray.h"
#include "framework/processor/cuda/kernel/MirroredArray.h"
#include "framework/processor/cuda/kernel/MappedArray.h"
#include "framework/processor/cuda/kernel/ArrayHandle.h"
#include "framework/processor/cuda/primitives/Scanner.h"
#include "framework/processor/cuda/primitives/Sorter.h"
#include "framework/processor/cuda/primitives/Partitioner.h"
#include "framework/processor/cuda/primitives/Compactor.h"
#include "framework/processor/cuda/primitives/Transposer.h"

namespace zillians { namespace framework { namespace processor { namespace cuda {

typedef DeviceArray<RuntimeApi> CudaDeviceArray;
typedef HostArray<RuntimeApi> CudaHostArray;
typedef MappedArray<RuntimeApi> CudaMappedArray;
typedef MirroredArray<RuntimeApi> CudaMirroredArray;

typedef Sorter<RuntimeApi> CudaSorter;
typedef Scanner<RuntimeApi> CudaScanner;
typedef Partitioner<RuntimeApi> CudaPartitioner;
typedef Compactor<RuntimeApi> CudaCompactor;
typedef Transposer<RuntimeApi> CudaTransposer;

} } } }

#endif/*CUDAKERNEL_H_*/
