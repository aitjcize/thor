/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHMAP_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHMAP_H_

#include "core/Prerequisite.h"
#include "framework/processor/cuda/CudaCommon.h"
#include "framework/processor/cuda/primitives/HashMapApi.h"
#include <boost/tr1/unordered_map.hpp>

#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHMAP_AVG_LOAD_PER_BUCKET		389
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHMAP_REPORTING_RESTART		1

namespace zillians { namespace framework { namespace processor { namespace cuda {

namespace detail {

extern bool HashMapKernel_RedistributePairsIntoBucket(uint32 pairCount, uint2* pairs, uint32 bucketCount, uint32* bucketSizes, uint2* pairsInBucket, uint32 bucketHash0, uint32 bucketHash1);
extern bool HashMapKernel_BuildCuckooHashingInBucket(uint32 bucketCount, uint32* bucketSizes, uint2* pairsInBucket, uint32 cuckooHash, uint2* hashTables, uint32* buildErrors);
extern bool HashMapKernel_FindInParallel(HASHMAPAPI_DECLARE_PARAMETERS, uint32 keyCount, uint32* keys, uint32* valueFound, uint32* positionFound);

}

template<typename KernelApi>
class HashMap
{
public:
	HashMap(const KernelApi& api) :
		mKernelApi(api),
		mMaxElements(0), mBucketCount(0),
		mPairs(NULL), mPairsInBuckets(NULL), mBucketSizes(NULL), mBuildErrors(NULL), mHashMapDevice(NULL),
		mBucketHash0(rand()), mBucketHash1(rand()), mCuckooHash(rand()),
		mDirty(true)
	{
	}

	~HashMap()
	{
		finalize();
	}

public:
	void initialize(int maxElements)
	{
		BOOST_ASSERT(maxElements > 0);

		mMaxElements = maxElements;
		mBucketCount = (maxElements / ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHMAP_AVG_LOAD_PER_BUCKET) + ((maxElements % ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHMAP_AVG_LOAD_PER_BUCKET == 0) ? 0 : 1);

		mPairs = new DeviceArray<KernelApi>(mKernelApi, maxElements * sizeof(uint2));
		mPairsInBuckets = new DeviceArray<KernelApi>(mKernelApi, mBucketCount * 512 * sizeof(uint2));
		mBucketSizes = new DeviceArray<KernelApi>(mKernelApi, mBucketCount * sizeof(uint32));
		mBuildErrors = new DeviceArray<KernelApi>(mKernelApi, mBucketCount * sizeof(uint32));
		mHashMapDevice = new DeviceArray<KernelApi>(mKernelApi, mBucketCount * 192 * 3 * sizeof(uint2));

		mBucketHash0 = rand();
		mBucketHash1 = rand();
		mCuckooHash  = rand();
	}

	void finalize()
	{
		SAFE_DELETE(mPairs);
		SAFE_DELETE(mPairsInBuckets);
		SAFE_DELETE(mBucketSizes);
		SAFE_DELETE(mBuildErrors);
		SAFE_DELETE(mHashMapDevice);
		mHashMapHost.clear();
		mMaxElements = mBucketCount = 0;
	}

public:
	HashMapKernelParameters& getKernelParameters()
	{
		if(mDirty)
		{
			commit();
		}

		ArrayHandle<DeviceArray<KernelApi> > handleHashMapDevice(*mHashMapDevice, accessLocation::device, accessMode::read);

		mKernelParameters.hashMap = handleHashMapDevice.template data<uint2>();
		mKernelParameters.bucketCount = mBucketCount;
		mKernelParameters.bucketHash0 = mBucketHash0;
		mKernelParameters.bucketHash1 = mBucketHash1;
		mKernelParameters.cuckooHash = mCuckooHash;

		return mKernelParameters;
	}

public:
	void clear()
	{
		mHashMapHost.clear();
		mDirty = true;
	}

	bool add(uint32 key, uint32 value)
	{
		BOOST_ASSERT(mMaxElements > 0);
		BOOST_ASSERT(mHashMapHost.size() < mMaxElements);

		boost::unordered_map<uint32, uint32>::iterator it;
		bool result;
		boost::tie(it, result) = mHashMapHost.insert(std::make_pair(key, value));

		if(result)
		{
			mDirty = true;
			return true;
		}
		else
		{
			return false;
		}
	}

	bool remove(uint32 key)
	{
		BOOST_ASSERT(mMaxElements > 0);

		boost::unordered_map<uint32, uint32>::iterator it = mHashMapHost.find(key);
		if(it != mHashMapHost.end())
		{
			mHashMapHost.erase(it);
			mDirty = true;
			return true;
		}
		else
		{
			return false;
		}
	}

	void commit()
	{
		BOOST_ASSERT(mMaxElements > 0);

		// compile pair hash map into array and upload the pairs
		uint32 pairCount = 0;
		{
			ArrayHandle<DeviceArray<KernelApi> > handlePairs(*mPairs, accessLocation::host, accessMode::readwrite);
			uint2* pointerPairs = handlePairs.template data<uint2>();

			int i = 0;
			for(boost::unordered_map<uint32, uint32>::iterator it = mHashMapHost.begin(); it != mHashMapHost.end(); i++, it++)
			{
				pointerPairs[i] = make_uint2(it->first, it->second);
			}
			pairCount = i;

			handlePairs.dirty();
		}

		// reset temporary data
		{
			ArrayHandle<DeviceArray<KernelApi> > handlePairsInBuckets(*mPairsInBuckets, accessLocation::host, accessMode::readwrite);
			handlePairsInBuckets.template fill<uint2>(make_uint2(std::numeric_limits<uint32>::max(), std::numeric_limits<uint32>::max()));

			ArrayHandle<DeviceArray<KernelApi> > handleHashMapDevice(*mHashMapDevice, accessLocation::host, accessMode::readwrite);
			handleHashMapDevice.template fill<uint2>(make_uint2(std::numeric_limits<uint32>::max(), std::numeric_limits<uint32>::max()));

			ArrayHandle<DeviceArray<KernelApi> > handleBucketSizes(*mBucketSizes, accessLocation::host, accessMode::readwrite);
			handleBucketSizes.template fill<uint32>(0);

			ArrayHandle<DeviceArray<KernelApi> > handleBuildErrors(*mBuildErrors, accessLocation::host, accessMode::readwrite);
			handleBuildErrors.template fill<uint32>(0);
		}

		// build for the first phase: FSM hash redistribution to make each block process its data independently
		while(true)
		{
			// launch the kernel to redistribute pairs into buckets
			{
				ArrayHandle<DeviceArray<KernelApi> > handlePairs(*mPairs, accessLocation::device, accessMode::read);
				ArrayHandle<DeviceArray<KernelApi> > handleBucketSizes(*mBucketSizes, accessLocation::device, accessMode::readwrite);
				ArrayHandle<DeviceArray<KernelApi> > handlePairsInBuckets(*mPairsInBuckets, accessLocation::device, accessMode::readwrite);

				detail::HashMapKernel_RedistributePairsIntoBucket(
						pairCount, handlePairs.template data<uint2>(),
						mBucketCount, handleBucketSizes.template data<uint32>(), handlePairsInBuckets.template data<uint2>(), mBucketHash0, mBucketHash1);

				handleBucketSizes.dirty();
				handlePairsInBuckets.dirty();
			}

			// check if there's any bucket contains more than 512 elements; if so, restart with different bucket hash
			bool overflowed = false;
			{
				ArrayHandle<DeviceArray<KernelApi> > handleBucketSizes(*mBucketSizes, accessLocation::host, accessMode::read);

				uint totalPairsRedistributed = 0;
				for(int i=0;i<mBucketCount;++i)
				{
					uint32 size = handleBucketSizes.template data<uint>()[i];
					totalPairsRedistributed += size;
					if(size >= 512)
					{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHMAP_REPORTING_RESTART
						printf("bucketCounts[%d] = %d, overflow, restarting...\n", i, size);
#endif
						overflowed = true;
					}
				}

				if(totalPairsRedistributed != pairCount)
				{
					printf("Fatal error, missing %d pairs in first phase (coould be some bug in atomic add)\n", pairCount - totalPairsRedistributed);
					throw std::runtime_error("pair missing while building the hash set in first phase");
				}
			}

			if(overflowed)
			{
				// reset the bucket size counters
				ArrayHandle<DeviceArray<KernelApi> > handleBucketSizes(*mBucketSizes, accessLocation::device, accessMode::readwrite);
				handleBucketSizes.template fill<uint32>(0);

				// choose another set of bucket hash
				mBucketHash0 = rand();
				mBucketHash1 = rand();
			}
			else
			{
				// success, stop building
				break;
			}
		}

		// perform data integrity check (optional)
		if(1)
		{
			ArrayHandle<DeviceArray<KernelApi> > handlePairsInBuckets(*mPairsInBuckets, accessLocation::host, accessMode::read);
			uint2* pointerPairsInBuckets = handlePairsInBuckets.template data<uint2>();

			uint totalPairsRedistributed = 0;
			for(int i=0; i<mBucketCount * 512; ++i)
			{
				if(pointerPairsInBuckets[i].x != std::numeric_limits<uint32>::max())
				{
					++totalPairsRedistributed;
				}
			}

			if(totalPairsRedistributed != pairCount)
			{
				printf("Fatal error, missing %d pairs in first phase (could be some bug in memory model)\n", pairCount - totalPairsRedistributed);
				throw std::runtime_error("pair missing while building the hash set in first phase");
			}
		}

		// build for the second phase: parallel cuckoo hashing
		while(true)
		{
			{
				ArrayHandle<DeviceArray<KernelApi> > handleBucketSizes(*mBucketSizes, accessLocation::device, accessMode::read);
				ArrayHandle<DeviceArray<KernelApi> > handlePairsInBuckets(*mPairsInBuckets, accessLocation::device, accessMode::read);
				ArrayHandle<DeviceArray<KernelApi> > handleBuildErrors(*mBuildErrors, accessLocation::device, accessMode::readwrite);
				ArrayHandle<DeviceArray<KernelApi> > handleHashMapDevice(*mHashMapDevice, accessLocation::device, accessMode::readwrite);

				detail::HashMapKernel_BuildCuckooHashingInBucket(
						mBucketCount, handleBucketSizes.template data<uint32>(), handlePairsInBuckets.template data<uint2>(), mCuckooHash, handleHashMapDevice.template data<uint2>(), handleBuildErrors.template data<uint32>());
			}

			// check if there's any bucket failed to perform cuckoo hashing; if so, restart with different seed
			bool failed = false;
			{
				ArrayHandle<DeviceArray<KernelApi> > handleBuildErrors(*mBuildErrors, accessLocation::host, accessMode::read);

				for(int i=0; i<mBucketCount; ++i)
				{
					if(handleBuildErrors.template data<uint32>()[i] > 0)
					{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHMAP_REPORTING_RESTART
						printf("Failed to find placement for bucket #%d, restarting...\n", i);
#endif
						failed = true;
					}
				}
			}

			if(failed)
			{
				// reset the build error states
				ArrayHandle<DeviceArray<KernelApi> > handleBuildErrors(*mBuildErrors, accessLocation::device, accessMode::readwrite);
				handleBuildErrors.template fill<uint32>(0);

				// choose another seed for cuckoo hashing
				mCuckooHash = rand();
			}
			else
			{
				// success, stop building
				break;
			}
		}

		// perform data integrity check (optional)
		if(1)
		{
			ArrayHandle<DeviceArray<KernelApi> > handleHashMapDevice(*mHashMapDevice, accessLocation::host, accessMode::read);
			uint2* pointerHashMapDevice = handleHashMapDevice.template data<uint2>();

			uint totalPairsHashed = 0;
			for(int i=0; i<mBucketCount*576; ++i)
			{
				if(pointerHashMapDevice[i].x != std::numeric_limits<uint32>::max())
				{
					++totalPairsHashed;
				}
			}

			if(totalPairsHashed != pairCount)
			{
				printf("Fatal error, missing %d pairs in second phase (could be some bug in synchronization)\n", pairCount - totalPairsHashed);
				throw std::runtime_error("pair missing while building the hash set in second phase");
			}
		}

		mDirty = false;
	}

	bool find(uint32 key, /*OUT*/ uint32& value)
	{
		BOOST_ASSERT(mMaxElements > 0);

		boost::unordered_map<uint32, uint32>::iterator it = mHashMapHost.find(key);
		if(it != mHashMapHost.end())
		{
			value = it->second;
			return true;
		}
		else
		{
			return false;
		}
	}

	bool findInParallel(/*IN*/ const std::vector<uint32>& keys, /*OUT*/ std::vector<uint32>& value, /*OUT*/ std::vector<bool>& result)
	{
		BOOST_ASSERT(mMaxElements > 0);

		uint32 keyCount = keys.size();

		// prepare the keys to search device array
		DeviceArray<KernelApi> keysToSearch(mKernelApi, keyCount * sizeof(uint32));
		{
			ArrayHandle<DeviceArray<KernelApi> > handleKeyToSearch(keysToSearch, accessLocation::host, accessMode::readwrite);
			uint32* pointerKeyToSearch = handleKeyToSearch.template data<uint32>();

			int i = 0;
			for(std::vector<uint32>::const_iterator it = keys.begin(); it != keys.end(); ++it, ++i)
			{
				pointerKeyToSearch[i] = *it;
			}

			handleKeyToSearch.dirty();
		}

		// prepare the result array
		DeviceArray<KernelApi> valueFound(mKernelApi, keyCount * sizeof(uint32));
		DeviceArray<KernelApi> positionFound(mKernelApi, keyCount * sizeof(uint32));

		// perform search in parallel
		{
			ArrayHandle<DeviceArray<KernelApi> > handleKeyToSearch(keysToSearch, accessLocation::device, accessMode::read);
			ArrayHandle<DeviceArray<KernelApi> > handleHashMapDevice(*mHashMapDevice, accessLocation::device, accessMode::read);
			ArrayHandle<DeviceArray<KernelApi> > handleValueFound(valueFound, accessLocation::device, accessMode::readwrite);
			ArrayHandle<DeviceArray<KernelApi> > handlePositionFound(positionFound, accessLocation::device, accessMode::readwrite);

			HashMapKernelParameters& param = getKernelParameters();
			detail::HashMapKernel_FindInParallel(
					HASHMAPAPI_EXPAND_PARAMETERS(param),
					keyCount,
					handleKeyToSearch.template data<uint32>(),
					handleValueFound.template data<uint32>(), handlePositionFound.template data<uint32>());

			handlePositionFound.dirty();
		}

		// populate all searched position and generate results
		{
			ArrayHandle<DeviceArray<KernelApi> > handleValueFound(valueFound, accessLocation::host, accessMode::read);
			ArrayHandle<DeviceArray<KernelApi> > handlePositionFound(positionFound, accessLocation::host, accessMode::read);
			uint32* pointerValueFound = handleValueFound.template data<uint32>();
			uint32* pointerPositionFound = handlePositionFound.template data<uint32>();

			result.clear();
			result.reserve(keyCount);

			uint failed_to_find = 0;
			for(int i=0; i<keyCount; ++i)
			{
				if(pointerPositionFound[i] == std::numeric_limits<uint32>::max())
				{
					result.push_back(false);
					value.push_back(0);
				}
				else
				{
					result.push_back(true);
					value.push_back(pointerValueFound[i]);
				}
			}
		}
	}

private:
	const KernelApi& mKernelApi;

	std::size_t mMaxElements;

	uint32 mBucketCount;
	uint32 mBucketHash0;
	uint32 mBucketHash1;
	uint32 mCuckooHash;

	DeviceArray<KernelApi>* mPairs;
	DeviceArray<KernelApi>* mPairsInBuckets;
	DeviceArray<KernelApi>* mBucketSizes;
	DeviceArray<KernelApi>* mBuildErrors;
	DeviceArray<KernelApi>* mHashMapDevice;
	boost::unordered_map<uint32, uint32> mHashMapHost;
	bool mDirty;

	HashMapKernelParameters mKernelParameters;
};

} } } }

#endif/*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHMAP_H_*/
