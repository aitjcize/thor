/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_SCANNER_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_SCANNER_H_

#include "core/Prerequisite.h"
#include "framework/processor/cuda/kernel/ArrayHandle.h"
#include "framework/processor/cuda/kernel/DeviceArray.h"
#include "framework/processor/cuda/primitives/detail/ThrustKernel.h"

namespace zillians { namespace framework { namespace processor { namespace cuda {

struct ScannerDataType 	{ enum type { type_uint64, type_int64, type_uint32, type_int32, type_uint16, type_int16, type_double, type_float }; };
struct ScannerMode 		{ enum type { exclusive, inclusive }; };
struct ScannerOp		{ enum type { maximum, minimum, plus, multiply }; };

template<typename KernelApi>
class Compactor;

template<typename KernelApi>
class Scanner
{
public:
	Scanner(const KernelApi& api) : mKernelApi(api), mMaxElements(0)
	{ }

	~Scanner()
	{
		finalize();
	}

	void initialize(std::size_t maxElements, ScannerDataType::type type = ScannerDataType::type_int32, ScannerMode::type mode = ScannerMode::exclusive, ScannerOp::type op = ScannerOp::plus, bool forceUsingThrust = false)
	{
		BOOST_ASSERT(maxElements > 0);

		finalize();

		mMaxElements = maxElements;
		mElementType = type;
		mScannerMode = mode;
		mScannerOp   = op;
	}

	void finalize()
	{
		mMaxElements = 0;
	}

	/**
	 * @brief Perform an exclusive prefix sum (scan) on the deviceIn and store the result in deviceOut.
	 *
	 * @note The output array can be the same as input array.
	 *
	 * @param deviceOut The device pointer to output result.
	 * @param deviceIn The device pointer to input data.
	 * @param elements Number of elements to be scanned.
	 * @param init The initial value for the exclusive prefix sum
	 * @return
	 */
	template<typename T>
	void scanExclusive(ArrayHandle<DeviceArray<KernelApi> >* dest, ArrayHandle<DeviceArray<KernelApi> >* src, const T& init, std::size_t elements = 0)
	{

		switch(mElementType)
		{
		case ScannerDataType::type_double:
			detail::thrust_exclusive_scan<double>(src->template data<double>(), dest->template data<double>(), init, elements, toThrustOperator(mScannerOp)); break;
		case ScannerDataType::type_float:
			detail::thrust_exclusive_scan<float>(src->template data<float>(), dest->template data<float>(), init, elements, toThrustOperator(mScannerOp)); break;
		case ScannerDataType::type_uint64:
			detail::thrust_exclusive_scan<unsigned long long int>(src->template data<unsigned long long int>(), dest->template data<unsigned long long int>(), init, elements, toThrustOperator(mScannerOp)); break;
		case ScannerDataType::type_int64:
			detail::thrust_exclusive_scan<long long int>(src->template data<long long int>(), dest->template data<long long int>(), init, elements, toThrustOperator(mScannerOp)); break;
		case ScannerDataType::type_uint32:
			detail::thrust_exclusive_scan<unsigned int>(src->template data<unsigned int>(), dest->template data<unsigned int>(), init, elements, toThrustOperator(mScannerOp)); break;
		case ScannerDataType::type_int32:
			detail::thrust_exclusive_scan<int>(src->template data<int>(), dest->template data<int>(), init, elements, toThrustOperator(mScannerOp)); break;
		case ScannerDataType::type_uint16:
			detail::thrust_exclusive_scan<unsigned short>(src->template data<unsigned short>(), dest->template data<unsigned short>(), init, elements, toThrustOperator(mScannerOp)); break;
		case ScannerDataType::type_int16:
			detail::thrust_exclusive_scan<short>(src->template data<short>(), dest->template data<short>(), init, elements, toThrustOperator(mScannerOp)); break;
		default:
			BOOST_ASSERT(false);
		}

		dest->dirty();
	}

	/**
	 * @brief Perform an inclusive prefix sum (scan) on the deviceIn and store the result in deviceOut.
	 *
	 * @note The output array can be the same as input array.
	 *
	 * @param deviceOut The device pointer to output result.
	 * @param deviceIn The device pointer to input data.
	 * @param elements Number of elements to be scanned.
	 * @return
	 */
	void scanInclusive(ArrayHandle<DeviceArray<KernelApi> >* dest, ArrayHandle<DeviceArray<KernelApi> >* src, std::size_t elements = 0)
	{
		switch(mElementType)
		{
		case ScannerDataType::type_double:
			detail::thrust_inclusive_scan(src->template data<double>(), dest->template data<double>(), elements, toThrustOperator(mScannerOp)); break;
		case ScannerDataType::type_float:
			detail::thrust_inclusive_scan(src->template data<float>(), dest->template data<float>(), elements, toThrustOperator(mScannerOp)); break;
		case ScannerDataType::type_uint64:
			detail::thrust_inclusive_scan(src->template data<unsigned long long int>(), dest->template data<unsigned long long int>(), elements, toThrustOperator(mScannerOp)); break;
		case ScannerDataType::type_int64:
			detail::thrust_inclusive_scan(src->template data<long long int>(), dest->template data<long long int>(), elements, toThrustOperator(mScannerOp)); break;
		case ScannerDataType::type_uint32:
			detail::thrust_inclusive_scan(src->template data<unsigned int>(), dest->template data<unsigned int>(), elements, toThrustOperator(mScannerOp)); break;
		case ScannerDataType::type_int32:
			detail::thrust_inclusive_scan(src->template data<int>(), dest->template data<int>(), elements, toThrustOperator(mScannerOp)); break;
		case ScannerDataType::type_uint16:
			detail::thrust_inclusive_scan(src->template data<unsigned short>(), dest->template data<unsigned short>(), elements, toThrustOperator(mScannerOp)); break;
		case ScannerDataType::type_int16:
			detail::thrust_inclusive_scan(src->template data<short>(), dest->template data<short>(), elements, toThrustOperator(mScannerOp)); break;
		default:
			BOOST_ASSERT(false);
		}

		dest->dirty();
	}

private:
	inline detail::scan_op::type toThrustOperator(ScannerOp::type op)
	{
		switch(op)
		{
		case ScannerOp::minimum:
			return detail::scan_op::minimum;
		case ScannerOp::maximum:
			return detail::scan_op::maximum;
		case ScannerOp::plus:
			return detail::scan_op::plus;
		case ScannerOp::multiply:
			return detail::scan_op::multiply;
		}
	}

	template<class T>
	void emulateScan(T* dataOut, T* dataIn, int n)
	{
		T sum = 0;
		for(int i=0;i<n;++i)
		{
			dataOut[i] = sum;
			sum += dataIn[i];
		}
	}

private:
	const KernelApi& mKernelApi;

	std::size_t				mMaxElements;
	ScannerDataType::type	mElementType;
	ScannerMode::type		mScannerMode;
	ScannerOp::type			mScannerOp;
};

} } } }

#endif/*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_SCANNER_H_*/
