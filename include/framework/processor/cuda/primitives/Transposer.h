/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef CUDATRANSPOSER_H_
#define CUDATRANSPOSER_H_

#include "core/Prerequisite.h"
#include <boost/type_traits.hpp>
#include <boost/mpl/sizeof.hpp>
#include <boost/mpl/bool.hpp>

namespace zillians { namespace framework { namespace processor { namespace cuda {

namespace detail {

extern bool TransposerKernel8(byte *output, byte* input, uint32 width, uint32 height);
extern bool TransposerKernel16(byte *output, byte* input, uint32 width, uint32 height);
extern bool TransposerKernel32(byte *output, byte* input, uint32 width, uint32 height);
extern bool TransposerKernel64(byte *output, byte* input, uint32 width, uint32 height);

bool ExtendedTransposerKernel8(byte *output, byte* input, uint32 total_width, uint32 total_height, uint32 width, uint32 height);
bool ExtendedTransposerKernel16(byte *output, byte* input, uint32 total_width, uint32 total_height, uint32 width, uint32 height);
bool ExtendedTransposerKernel32(byte *output, byte* input, uint32 total_width, uint32 total_height, uint32 width, uint32 height);
bool ExtendedTransposerKernel64(byte *output, byte* input, uint32 total_width, uint32 total_height, uint32 width, uint32 height);

}

template<typename KernelApi>
class Transposer
{
public:
	Transposer(const KernelApi& api) : mKernelApi(api)
	{
	}

	~Transposer()
	{
		finalize();
	}

public:
	void initialize()
	{
	}

	void finalize()
	{

	}

	template<typename ElementType, typename DestinationType, typename SourceType>
	bool transpose(/*OUT*/ DestinationType* dest, /*IN*/ SourceType* src, uint32 width, uint32 height)
	{
		// The width and height must be multiple of 16
		BOOST_ASSERT(width % 16 == 0);
		BOOST_ASSERT(width / 16 <= 65535);
		BOOST_ASSERT(width >= 16);

		BOOST_ASSERT(height % 16 == 0);
		BOOST_ASSERT(height / 16 <= 65535);
		BOOST_ASSERT(height >= 16);

		BOOST_ASSERT(dest->size() == src->size());
		BOOST_ASSERT(src->size() >= width * height * sizeof(ElementType));

		if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint8) >::value)
		{
			return detail::TransposerKernel8(dest->template data<byte>(), src->template data<byte>(), width, height);
		}
		else if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint16) >::value)
		{
			return detail::TransposerKernel16(dest->template data<byte>(), src->template data<byte>(), width, height);
		}
		else if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint32) >::value)
		{
			return detail::TransposerKernel32(dest->template data<byte>(), src->template data<byte>(), width, height);
		}
		else if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint64) >::value)
		{
			return detail::TransposerKernel64(dest->template data<byte>(), src->template data<byte>(), width, height);
		}
		else
		{
			throw std::invalid_argument("invalid transpose element size requested");
		}

		dest->dirty();

		return false;
	}

	template<typename ElementType, typename DestinationType, typename SourceType>
	bool transpose(/*OUT*/ DestinationType* dest, /*IN*/ SourceType* src, uint32 width, uint32 height, uint32 total_width, uint32 total_height)
	{
		// The width and height must be multiple of 16
//		BOOST_ASSERT(width % 16 == 0);
//		BOOST_ASSERT(width / 16 <= 65535);
//		BOOST_ASSERT(width >= 16);
//
//		BOOST_ASSERT(height % 16 == 0);
//		BOOST_ASSERT(height / 16 <= 65535);
//		BOOST_ASSERT(height >= 16);
//
//		BOOST_ASSERT(dest->size() == src->size());
//		BOOST_ASSERT(src->size() >= width * height * sizeof(ElementType));

		if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint8) >::value)
		{
			return detail::ExtendedTransposerKernel8(dest->template data<byte>(), src->template data<byte>(), total_width, total_height, width, height);
		}
		else if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint16) >::value)
		{
			return detail::ExtendedTransposerKernel16(dest->template data<byte>(), src->template data<byte>(), total_width, total_height, width, height);
		}
		else if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint32) >::value)
		{
			return detail::ExtendedTransposerKernel32(dest->template data<byte>(), src->template data<byte>(), total_width, total_height, width, height);
		}
		else if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint64) >::value)
		{
			return detail::ExtendedTransposerKernel64(dest->template data<byte>(), src->template data<byte>(), total_width, total_height, width, height);
		}
		else
		{
			throw std::invalid_argument("invalid transpose element size requested");
		}

		dest->dirty();

		return false;
	}

private:
	const KernelApi& mKernelApi;
};

} } } }

#endif/*CUDATRANSPOSER_H_*/
