/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_COMPACTOR_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_COMPACTOR_H_

#include "core/Prerequisite.h"
#include "framework/processor/cuda/primitives/Scanner.h"
#include <boost/type_traits.hpp>
#include <boost/mpl/sizeof.hpp>
#include <boost/mpl/bool.hpp>

namespace zillians { namespace framework { namespace processor { namespace cuda {

namespace detail {

extern bool CompactorKernel_Compact8(/*IN*/ uint32* flags, /*IN*/ uint32* flags_scanned, /*OUT*/ byte* dest, /*IN*/ byte* src, uint32 elements);
extern bool CompactorKernel_Compact16(/*IN*/ uint32* flags, /*IN*/ uint32* flags_scanned, /*OUT*/ byte* dest, /*IN*/ byte* src, uint32 elements);
extern bool CompactorKernel_Compact32(/*IN*/ uint32* flags, /*IN*/ uint32* flags_scanned, /*OUT*/ byte* dest, /*IN*/ byte* src, uint32 elements);
extern bool CompactorKernel_Compact64(/*IN*/ uint32* flags, /*IN*/ uint32* flags_scanned, /*OUT*/ byte* dest, /*IN*/ byte* src, uint32 elements);
extern bool CompactorKernel_Compact128(/*IN*/ uint32* flags, /*IN*/ uint32* flags_scanned, /*OUT*/ byte* dest, /*IN*/ byte* src, uint32 elements);
extern bool CompactorKernel_FindCompactIndices(/*IN*/ uint32* flags, /*IN*/ uint32* flags_scanned, /*OUT*/ uint32* indices, uint32 elements);

}

template<typename KernelApi>
class Compactor
{
public:
	Compactor(const KernelApi& api) :
		mKernelApi(api),
		mScannerOwner(false), mScanner(NULL), mMaxElements(0),
		mValidFlagsOwner(false), mValidFlags(NULL), mHandleToValidFlags(NULL), mFlagsCount(0),
		mValidFlagsScanned(NULL), mHandleToValidFlagsScanned(NULL)
	{
	}

	~Compactor()
	{
		finalize();
	}

public:
	void initialize(Scanner<KernelApi>* scanner)
	{
		// make sure the given scanner is properly initialized & compatible
		BOOST_ASSERT(scanner->mMaxElements > 0);

		finalize();

		mScannerOwner = false;
		mScanner = scanner;
		mMaxElements = scanner->mMaxElements;

		mValidFlagsScanned = new DeviceArray<KernelApi>(mKernelApi, mMaxElements * sizeof(uint32));
		mHandleToValidFlagsScanned = new ArrayHandle<DeviceArray<KernelApi> >(*mValidFlagsScanned, accessLocation::device, accessMode::readwrite);
	}

	void initialize(int maxElements)
	{
		finalize();

		mScannerOwner = true;
		mScanner = new Scanner<KernelApi>(mKernelApi);
		BOOST_ASSERT(mScanner);
		mScanner->initialize(maxElements, ScannerDataType::type_int32, ScannerMode::exclusive, ScannerOp::plus);

		mMaxElements = maxElements;

		mValidFlagsScanned = new DeviceArray<KernelApi>(mKernelApi, mMaxElements * sizeof(uint32));
		mHandleToValidFlagsScanned = new ArrayHandle<DeviceArray<KernelApi> >(*mValidFlagsScanned, accessLocation::device, accessMode::readwrite);
	}

	void finalize()
	{
		if(mScannerOwner)	{ SAFE_DELETE(mScanner); }
		else				{ SAFE_NULL  (mScanner); }
		mMaxElements = 0;

		SAFE_DELETE(mHandleToValidFlagsScanned);
		SAFE_DELETE(mValidFlagsScanned);

		SAFE_DELETE(mHandleToValidFlags);
		if(mValidFlagsOwner)	{ SAFE_DELETE(mValidFlags); }
		else					{ SAFE_NULL  (mValidFlags); }
		mFlagsCount = 0;
		mValidFlagsOwner = false;
	}

public:
	uint32 setValidFlags(/*IN*/ ArrayHandle<DeviceArray<KernelApi> >* flags, std::size_t elements = 0, bool makeCopy = false)
	{
		BOOST_ASSERT(elements <= mMaxElements);

		// release previously set flags
		SAFE_DELETE(mHandleToValidFlags);
		if(mValidFlagsOwner)	{ SAFE_DELETE(mValidFlags); }
		else					{ SAFE_NULL  (mValidFlags); }
		mFlagsCount = 0;
		mValidFlagsOwner = false;

		// determine the number of elements to compact
		if(elements == 0)
		{
			mFlagsCount = flags->size() / sizeof(uint32);
		}
		else
		{
			mFlagsCount = elements;
		}

		BOOST_ASSERT(mFlagsCount <= mMaxElements);

		// create valid flags array and handle (make copy if necessary)
		if(makeCopy)
		{
			mValidFlagsOwner = true;
			mValidFlags = new DeviceArray<KernelApi>(mKernelApi, mFlagsCount * sizeof(uint32));
			mHandleToValidFlags = new ArrayHandle<DeviceArray<KernelApi> >(*mValidFlags, accessLocation::device, accessMode::readwrite);
			mHandleToValidFlags->copyFrom(*flags, mFlagsCount * sizeof(uint32));
		}
		else
		{
			mValidFlagsOwner = false;
			mValidFlags = &flags->array();
			mHandleToValidFlags = new ArrayHandle<DeviceArray<KernelApi> >(*flags);
		}

		// scan the valid flags to calculate the compact indices
		ArrayHandle<DeviceArray<KernelApi> > srcHandleToScan(*mValidFlags, accessLocation::device, accessMode::read);
		ArrayHandle<DeviceArray<KernelApi> > scanDestHandle(*mValidFlagsScanned, accessLocation::device, accessMode::readwrite);

		mScanner->scanExclusive(mHandleToValidFlagsScanned, mHandleToValidFlags, 0, mFlagsCount);

		uint32 count = 0;
		{
			ArrayHandle<DeviceArray<KernelApi> > handleToScannedCount(*mValidFlagsScanned, accessLocation::host, accessMode::read, (mFlagsCount - 1) * sizeof(uint32), sizeof(uint32));
			count = handleToScannedCount.template at<uint32>(0);

			ArrayHandle<DeviceArray<KernelApi> > handleToLastFlag(*mValidFlags, accessLocation::host, accessMode::read, (mFlagsCount - 1) * sizeof(uint32), sizeof(uint32));
			count += handleToLastFlag.template at<uint32>(0);
		}

		BOOST_ASSERT(count <= elements);

		return count;
	}

public:
	template<typename ElementType>
	bool compact(/*OUT*/ ArrayHandle<DeviceArray<KernelApi> >* dest, /*IN*/ ArrayHandle<DeviceArray<KernelApi> >* src)
	{
		BOOST_ASSERT(dest->size() >= src->size());
		BOOST_ASSERT(src->size() >= mFlagsCount * sizeof(ElementType));

		if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint8) >::value)
		{
			return detail::CompactorKernel_Compact8(mHandleToValidFlags->template data<uint32>(), mHandleToValidFlagsScanned->template data<uint32>(), dest->template data<byte>(), src->template data<byte>(), mFlagsCount);
		}
		else if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint16) >::value)
		{
			return detail::CompactorKernel_Compact16(mHandleToValidFlags->template data<uint32>(), mHandleToValidFlagsScanned->template data<uint32>(), dest->template data<byte>(), src->template data<byte>(), mFlagsCount);
		}
		else if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint32) >::value)
		{
			return detail::CompactorKernel_Compact32(mHandleToValidFlags->template data<uint32>(), mHandleToValidFlagsScanned->template data<uint32>(), dest->template data<byte>(), src->template data<byte>(), mFlagsCount);
		}
		else if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint64) >::value)
		{
			return detail::CompactorKernel_Compact64(mHandleToValidFlags->template data<uint32>(), mHandleToValidFlagsScanned->template data<uint32>(), dest->template data<byte>(), src->template data<byte>(), mFlagsCount);
		}
		else if(boost::mpl::bool_< sizeof(ElementType) == sizeof(uint4) >::value)
		{
			return detail::CompactorKernel_Compact128(mHandleToValidFlags->template data<uint32>(), mHandleToValidFlagsScanned->template data<uint32>(), dest->template data<byte>(), src->template data<byte>(), mFlagsCount);
		}
		else
		{
			throw std::invalid_argument("invalid compact element size requested");
		}

		dest->dirty();

		return false;
	}

	bool findValidIndices(/*OUT*/ ArrayHandle<DeviceArray<KernelApi> >* indices)
	{
		BOOST_ASSERT(indices->size() >= mFlagsCount * sizeof(uint32));
		return detail::CompactorKernel_FindCompactIndices(mHandleToValidFlags->template data<uint32>(), mHandleToValidFlagsScanned->template data<uint32>(), indices->template data<uint32>(), mFlagsCount);
		indices->dirty();
	}

private:
	const KernelApi& mKernelApi;

	bool mScannerOwner;
	Scanner<KernelApi>* mScanner;
	std::size_t mMaxElements;

private:
	bool mValidFlagsOwner;
	std::size_t mFlagsCount;
	DeviceArray<KernelApi>* mValidFlags;
	ArrayHandle<DeviceArray<KernelApi> >* mHandleToValidFlags;

	DeviceArray<KernelApi>* mValidFlagsScanned;
	ArrayHandle<DeviceArray<KernelApi> >* mHandleToValidFlagsScanned;
};

} } } }

#endif/*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_COMPACTOR_H_*/

