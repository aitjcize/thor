/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHMAPAPI_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHMAPAPI_H_

#include "core/Types.h"
#include <vector_types.h>

namespace zillians { namespace framework { namespace processor { namespace cuda {

#define HASHMAPAPI_ENABLE_TEXTURE_LOOKUP_ACCELERATION	0

struct HashMapKernelParameters
{
	uint2* hashMap;
	zillians::uint32 bucketCount;
	zillians::uint32 bucketHash0;
	zillians::uint32 bucketHash1;
	zillians::uint32 cuckooHash;
};

#if HASHMAPAPI_ENABLE_TEXTURE_LOOKUP_ACCELERATION

#define HASHMAPAPI_DECLARE_GLOBAL	\
	texture<uint2, 1, cudaReadModeElementType> texHashMap;

#define HASHMAPAPI_BEFORE_KERNEL_INIT	\
	{ \
		std::size_t offset = 0; \
		cudaError error; \
		const textureReference* texref; \
		error = cudaGetTextureReference(&texref, "texHashMap"); \
		if(error != cudaSuccess) \
		{ \
			printf("failed to get texture reference for hash map\n"); \
		} \
		cudaChannelFormatDesc desc = cudaCreateChannelDesc<uint2>(); \
		error = cudaBindTexture(&offset, texref, (void*)HashMapParam_HashMap, &desc, HashMapParam_BucketCount * 3 * 192 * sizeof(uint2)); \
		if(error != cudaSuccess) \
		{ \
			printf("failed to bind texture for hash map\n"); \
		} \
		HashMapParam_TextureLookupOffset = offset; \
		printf("offset = %d\n", HashMapParam_TextureLookupOffset); \
	}

#define HASHMAPAPI_EXPAND_PARAMETERS(param)	\
	param.hashMap, param.bucketCount, param.bucketHash0, param.bucketHash1, param.cuckooHash, 0

#define HASHMAPAPI_DECLARE_PARAMETERS	\
	uint2* HashMapParam_HashMap, zillians::uint32 HashMapParam_BucketCount, zillians::uint32 HashMapParam_BucketHash0, zillians::uint32 HashMapParam_BucketHash1, zillians::uint32 HashMapParam_CuckooHash, zillians::uint32 HashMapParam_TextureLookupOffset

#define HASHMAPAPI_FORWARD_PARAMETERS	\
	HashMapParam_HashMap, HashMapParam_BucketCount, HashMapParam_BucketHash0, HashMapParam_BucketHash1, HashMapParam_CuckooHash, HashMapParam_TextureLookupOffset

#define HASHMAPAPI_KERNEL_INIT	\
	uint HashMapVar_c00 = HashMapParam_CuckooHash & 0x2B780D61; \
	uint HashMapVar_c01 = HashMapParam_CuckooHash & 0xF07BE535; \
	uint HashMapVar_c10 = HashMapParam_CuckooHash & 0x2B07D70A; \
	uint HashMapVar_c11 = HashMapParam_CuckooHash & 0xABFB9D52; \
	uint HashMapVar_c20 = HashMapParam_CuckooHash & 0x9D52B07D; \
	uint HashMapVar_c21 = HashMapParam_CuckooHash & 0x8F5C28F6;

#define HASHMAPAPI_FIND(key, value, position)	\
	{ \
		zillians::uint32 HashMapVar_bucketId = ((HashMapParam_BucketHash0 + HashMapParam_BucketHash1 * key) % 1900813) % HashMapParam_BucketCount; \
		zillians::uint32 HashMapVar_p0 = (HashMapVar_bucketId * 576 + 192 * 0) + (((HashMapVar_c00 + HashMapVar_c01 * key) % 1900813) % 192) + HashMapParam_TextureLookupOffset; \
		zillians::uint32 HashMapVar_p1 = (HashMapVar_bucketId * 576 + 192 * 1) + (((HashMapVar_c10 + HashMapVar_c11 * key) % 1900813) % 192) + HashMapParam_TextureLookupOffset; \
		zillians::uint32 HashMapVar_p2 = (HashMapVar_bucketId * 576 + 192 * 2) + (((HashMapVar_c20 + HashMapVar_c21 * key) % 1900813) % 192) + HashMapParam_TextureLookupOffset; \
		\
		bool found = false; \
		uint2 HashMapVar_check; \
		\
		if(!found) HashMapVar_check = tex1Dfetch(texHashMap, HashMapVar_p0); \
		\
		if(!found && HashMapVar_check.x == key) \
		{ \
			position = HashMapVar_p0; \
			value = HashMapVar_check.y; \
			found = true; \
		} \
		\
		if(!found) HashMapVar_check = tex1Dfetch(texHashMap, HashMapVar_p1); \
		\
		if(!found && HashMapVar_check.x == key) \
		{ \
			position = HashMapVar_p1; \
			value = HashMapVar_check.y; \
			found = true; \
		} \
		\
		if(!found) HashMapVar_check = tex1Dfetch(texHashMap, HashMapVar_p2); \
		\
		if(!found && HashMapVar_check.x == key) \
		{ \
			position = HashMapVar_p2; \
			value = HashMapVar_check.y; \
			found = true; \
		} \
		\
		if(!found) \
		{ \
			position = UINT_MAX; \
		} \
	}

#else

#define HASHMAPAPI_DECLARE_GLOBAL

#define HASHMAPAPI_BEFORE_KERNEL_INIT

#define HASHMAPAPI_EXPAND_PARAMETERS(param)	\
	param.hashMap, param.bucketCount, param.bucketHash0, param.bucketHash1, param.cuckooHash

#define HASHMAPAPI_DECLARE_PARAMETERS	\
	uint2* HashMapParam_HashMap, zillians::uint32 HashMapParam_BucketCount, zillians::uint32 HashMapParam_BucketHash0, zillians::uint32 HashMapParam_BucketHash1, zillians::uint32 HashMapParam_CuckooHash

#define HASHMAPAPI_FORWARD_PARAMETERS	\
	HashMapParam_HashMap, HashMapParam_BucketCount, HashMapParam_BucketHash0, HashMapParam_BucketHash1, HashMapParam_CuckooHash

#define HASHMAPAPI_KERNEL_INIT	\
	uint HashMapVar_c00 = HashMapParam_CuckooHash & 0x2B780D61; \
	uint HashMapVar_c01 = HashMapParam_CuckooHash & 0xF07BE535; \
	uint HashMapVar_c10 = HashMapParam_CuckooHash & 0x2B07D70A; \
	uint HashMapVar_c11 = HashMapParam_CuckooHash & 0xABFB9D52; \
	uint HashMapVar_c20 = HashMapParam_CuckooHash & 0x9D52B07D; \
	uint HashMapVar_c21 = HashMapParam_CuckooHash & 0x8F5C28F6;

#define HASHMAPAPI_FIND(key, value, position)	\
	{ \
		zillians::uint32 HashMapVar_bucketId = ((HashMapParam_BucketHash0 + HashMapParam_BucketHash1 * key) % 1900813) % HashMapParam_BucketCount; \
		zillians::uint32 HashMapVar_p0 = (HashMapVar_bucketId * 576 + 192 * 0) + (((HashMapVar_c00 + HashMapVar_c01 * key) % 1900813) % 192); \
		zillians::uint32 HashMapVar_p1 = (HashMapVar_bucketId * 576 + 192 * 1) + (((HashMapVar_c10 + HashMapVar_c11 * key) % 1900813) % 192); \
		zillians::uint32 HashMapVar_p2 = (HashMapVar_bucketId * 576 + 192 * 2) + (((HashMapVar_c20 + HashMapVar_c21 * key) % 1900813) % 192); \
		\
		bool found = false; \
		uint2 HashMapVar_check; \
		\
		if(!found) HashMapVar_check = HashMapParam_HashMap[HashMapVar_p0]; \
		\
		if(HashMapVar_check.x == key) \
		{ \
			position = HashMapVar_p0; \
			value = HashMapVar_check.y; \
			found = true; \
		} \
		\
		if(!found) HashMapVar_check = HashMapParam_HashMap[HashMapVar_p1]; \
		\
		if(!found && HashMapVar_check.x == key) \
		{ \
			position = HashMapVar_p1; \
			value = HashMapVar_check.y; \
			found = true; \
		} \
		\
		if(!found) HashMapVar_check = HashMapParam_HashMap[HashMapVar_p2]; \
		\
		if(!found && HashMapVar_check.x == key) \
		{ \
			position = HashMapVar_p2; \
			value = HashMapVar_check.y; \
			found = true; \
		} \
		\
		if(!found) \
		{ \
			position = UINT_MAX; \
		} \
	}
#endif

} } } }

#endif/*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHTABLEAPI_H_*/
