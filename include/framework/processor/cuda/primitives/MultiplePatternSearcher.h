/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MULTIPLEPATTERNSEARCHER_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MULTIPLEPATTERNSEARCHER_H_

#include "core/Prerequisite.h"
#include "framework/processor/cuda/primitives/detail/MultiplePatternSearcherKernel.h"
#include "framework/processor/cuda/kernel/DeviceArray.h"
#include "framework/processor/cuda/kernel/ArrayHandle.h"
#include "framework/processor/cuda/primitives/HashMap.h"
#include "framework/processor/cuda/primitives/Transposer.h"
#include "framework/processor/cuda/kernel/api/DriverApi.h"
#include "framework/processor/cuda/primitives/MultiplePatternSearcher_JIT.h"
#include "framework/processor/cuda/primitives/jit/JITContext.h"
#include <boost/unordered_map.hpp>

#define ENABLE_GPU_EXACT_MATCH_VERIFICATION 0

namespace zillians { namespace framework { namespace processor { namespace cuda {

namespace zvpcpj = zillians::vw::processors::cuda::primitives::jit;

template<typename KernelApi>
class MultiplePatternSearcher
{
public:
	MultiplePatternSearcher(const KernelApi& api) :
		mKernelApi(api), mPatternHash(api), mTransposer(api),
		mPatternOwner(false), mPatterns(NULL),
		mTextOwner(false), mText(NULL),
		mPatternMinimalLength(0), mTextMaximumLength(0),
		mPatternHashComponent(101),
		mJITContext(NULL)
	{

	}

	~MultiplePatternSearcher()
	{
		finalize();
	}

public:
	void setJITContext(zvpcpj::JITContext_t *JITContext)
	{
		mJITContext = JITContext;
	}

	void initialize(uint32 maxPatternCount, uint32 maxMatchCount, uint32 maxTextLength, uint32 hashComponent)
	{
		mPatternHash.initialize(maxPatternCount);
		mTransposer.initialize();
		mPatternHashComponent = hashComponent;

		uint32 textDeviceSize = ROUND_UP_TO_MULTIPLE(maxTextLength, 256 * BLOCK_PER_THREAD * BLOCK_STRIPE);	// 32768 (bytes) = 16 (the least multiple transpose width) * 256 (the least multiple of transosed height) * 8 (each uint64 holds 8 char)

		mDeviceTextData = new DeviceArray<KernelApi>(mKernelApi, textDeviceSize);
		mDeviceTextDataTransposed = new DeviceArray<KernelApi>(mKernelApi, textDeviceSize);
		mDeviceMatchedTextPosition = new DeviceArray<KernelApi>(mKernelApi, maxMatchCount * sizeof(uint32));
		mDeviceMatchedPatternPosition = new DeviceArray<KernelApi>(mKernelApi, maxMatchCount * sizeof(uint32));

		mDeviceMatchedCount = new DeviceArray<KernelApi>(mKernelApi, sizeof(uint32));

		mTextMaximumLength = maxTextLength;
		mMaximumMatchedCount = maxMatchCount;
	}

	void finalize()
	{
		mTransposer.finalize();
		mPatternHash.finalize();

		clearPatterns();

		if(mTextOwner)	{ SAFE_DELETE(mText); }
		else			{ SAFE_NULL(mText); }
		mTextOwner = false;

		SAFE_DELETE(mDeviceTextData);
		SAFE_DELETE(mDeviceTextDataTransposed);
		SAFE_DELETE(mDeviceMatchedTextPosition);
		SAFE_DELETE(mDeviceMatchedPatternPosition);
		SAFE_DELETE(mDeviceMatchedCount);

		mTextMaximumLength = 0;
		mMaximumMatchedCount = 0;
	}

	void setPatterns(const std::vector<std::string>& patterns)
	{
		// clean-up previous given patterns
		clearPatterns();

		// compute the shortest length among all patterns
		uint32 min_length = std::numeric_limits<uint32>::max();
		for(std::vector<std::string>::const_iterator it = patterns.begin(); it != patterns.end(); ++it)
		{
			uint32 l = it->length();
			if(l < min_length) min_length = l;
		}
		mPatternMinimalLength = min_length;

		// add pattern hashes into hash map
		int i=0;
		mPatternHash.clear();
		for(std::vector<std::string>::const_iterator it = patterns.begin(); it != patterns.end(); ++it, ++i)
		{
			uint32 h = hashPattern(*it, mPatternMinimalLength);
			uint32 positionFound;
			if(UNLIKELY(mPatternHash.find(h, positionFound)))
			{
				bool duplicate_found = false;
				for(std::vector<std::pair<std::string,uint32> >::iterator it_dup = mPatterns[positionFound]->begin(); it_dup != mPatterns[positionFound]->end(); ++it_dup)
				{
					if(strcmp(it_dup->first.c_str(), it->c_str()) == 0)
					{
						duplicate_found = true;
						break;
					}
				}

				if(!duplicate_found)
				{
					mPatterns[positionFound]->push_back(std::make_pair(*it, i));
				}
				else
				{
					//throw std::invalid_argument("duplicated pattern");
				}
			}
			else
			{
				std::vector<std::pair<std::string,uint32> >* q = new std::vector<std::pair<std::string,uint32> >;
				q->push_back(std::make_pair(*it, i));
				mPatterns.push_back(q);
				mPatternHash.add(h, mPatterns.size() - 1);
			}
		}

		// build pattern hash used by rabin-karp algorithm
		mPatternHash.commit();
	}

	void setText(const std::string& text, bool copy = false)
	{
		BOOST_ASSERT(text.length() <= mTextMaximumLength);

		// clean-up previous given text
		if(mTextOwner)	{ SAFE_DELETE(mText); }
		else			{ SAFE_NULL(mText); }
		mTextOwner = false;

		// save the new text
		if(copy)
		{
			mText = new std::string(text);
			mTextOwner = true;
		}
		else
		{
			mText = &text;
			mTextOwner = false;
		}
	}

public:
	bool search(std::vector<std::pair<uint32, uint32> >& matchedPatterns)
	{
		BOOST_ASSERT(mPatternMinimalLength > 0);
		BOOST_ASSERT(mText != NULL);

		if(mText->length() < mPatternMinimalLength)
			return false;

		// search on CPU
		uint32 current_hash = hashPattern(*mText, mPatternMinimalLength);
		uint32 component_max = pow(mPatternHashComponent, mPatternMinimalLength - 1);

		int i = mPatternMinimalLength;
		do
		{
			uint32 position;
			if(mPatternHash.find(current_hash, position))
			{
				const char* text = &(mText->c_str()[i-mPatternMinimalLength]);
				for(std::vector<std::pair<std::string,uint32> >::iterator it = mPatterns[position]->begin(); it != mPatterns[position]->end(); ++it)
				{
					const char* pattern = it->first.c_str();
					std::size_t pattern_length = it->first.length();
					if(strncmp(pattern, text, pattern_length) == 0)
					{
						matchedPatterns.push_back(std::make_pair(i-mPatternMinimalLength, it->second));
					}
					BOOST_ASSERT(it == mPatterns[position]->begin());
				}
			}

			if(i >= mText->length()) break;

			unsigned char first_character = (*mText)[i-mPatternMinimalLength];
			unsigned char next_character = (*mText)[i];
			current_hash -= first_character * component_max;
			current_hash *= mPatternHashComponent;
			current_hash += next_character;
			i++;
		} while(true);
	}

	bool searchInParallel(std::vector<std::pair<uint32, uint32> >& matchedPatterns)
	{
		BOOST_ASSERT(mTextMaximumLength > 0);
		BOOST_ASSERT(mPatternMinimalLength > 0);
		BOOST_ASSERT(mText != NULL);
		BOOST_ASSERT(mDeviceTextData);
		BOOST_ASSERT(mDeviceTextDataTransposed);
		BOOST_ASSERT(mDeviceMatchedTextPosition);
		BOOST_ASSERT(mDeviceMatchedPatternPosition);
		BOOST_ASSERT(mDeviceMatchedCount);

		uint32 textLength = mText->length();
		if(textLength < mPatternMinimalLength)
			return false;

		// search on GPU
		// prepare and upload data onto GPU
		uint32 hashComponentMax = pow(mPatternHashComponent, mPatternMinimalLength - 1);
		uint32 textLengthToProcess = ROUND_UP_TO_MULTIPLE(textLength, 256 * BLOCK_PER_THREAD * BLOCK_STRIPE);
		{
			ArrayHandle<DeviceArray<KernelApi> > handleTextData(*mDeviceTextData, accessLocation::device, accessMode::readwrite);
			handleTextData.copyFromRaw((byte*)mText->c_str(), textLength, accessLocation::host);
			ArrayHandle<DeviceArray<KernelApi> > handleMatchedCount(*mDeviceMatchedCount, accessLocation::device, accessMode::readwrite);
			handleMatchedCount.template fill<uint32>(0);
		}

		// transpose the text into column-major format
		{
			ArrayHandle<DeviceArray<KernelApi> > handleTextData(*mDeviceTextData, accessLocation::device, accessMode::read);
			ArrayHandle<DeviceArray<KernelApi> > handleTextDataTransposed(*mDeviceTextDataTransposed, accessLocation::device, accessMode::readwrite);
			uint32 width = BLOCK_PER_THREAD;
			uint32 height = textLengthToProcess / (BLOCK_PER_THREAD * BLOCK_STRIPE);
			//printf("width = %d, height = %d\n", width, height);
#if BLOCK_STRIPE == 4
			mTransposer.template transpose<uint32>(&handleTextDataTransposed, &handleTextData, width, height);
#elif BLOCK_STRIPE == 8
			mTransposer.template transpose<uint64>(&handleTextDataTransposed, &handleTextData, width, height);
#endif
			handleTextDataTransposed.dirty();
		}

		// launch rabin-karp kernel
		{
			ArrayHandle<DeviceArray<KernelApi> > handleTextDataTransposed(*mDeviceTextDataTransposed, accessLocation::device, accessMode::read);
			ArrayHandle<DeviceArray<KernelApi> > handleMatchedCount(*mDeviceMatchedCount, accessLocation::device, accessMode::readwrite);
			ArrayHandle<DeviceArray<KernelApi> > handleMatchedTextPosition(*mDeviceMatchedTextPosition, accessLocation::device, accessMode::readwrite);
			ArrayHandle<DeviceArray<KernelApi> > handleMatchedPatternPosition(*mDeviceMatchedPatternPosition, accessLocation::device, accessMode::readwrite);

			HashMapKernelParameters& param = mPatternHash.getKernelParameters();

			MultiplePatternSearchKernel_RabinKarp_Spec<KernelApi, BLOCK_STRIPE>(
					mJITContext,
					HASHMAPAPI_EXPAND_PARAMETERS(param),
					handleTextDataTransposed.template data<typename BLOCK_STRIPE_TO_TYPE<BLOCK_STRIPE>::type>(), textLength, mPatternMinimalLength,
					mPatternHashComponent, hashComponentMax,
					mMaximumMatchedCount, handleMatchedCount.template data<uint32>(), handleMatchedTextPosition.template data<uint32>(), handleMatchedPatternPosition.template data<uint32>()
					);

			handleMatchedCount.dirty();
			handleMatchedTextPosition.dirty();
			handleMatchedPatternPosition.dirty();
		}

#if ENABLE_GPU_EXACT_MATCH_VERIFICATION
		// filter the result and find exact match
		{

		}
#endif

		// download the result back
		{
			ArrayHandle<DeviceArray<KernelApi> > handleMatchedCount(*mDeviceMatchedCount, accessLocation::host, accessMode::read);
			uint32 count = handleMatchedCount.template at<uint32>(0);

			if(count > mMaximumMatchedCount)
			{
				printf("exceeding maximum matched count, some results are ignored\n");
				count = mMaximumMatchedCount;
			}

			if(count > 0)
			{
				ArrayHandle<DeviceArray<KernelApi> > handleMatchedTextPosition(*mDeviceMatchedTextPosition, accessLocation::device, accessMode::read);
				ArrayHandle<DeviceArray<KernelApi> > handleMatchedPatternPosition(*mDeviceMatchedPatternPosition, accessLocation::device, accessMode::read);

				uint32* textPositions = new uint32[count];
				uint32* patternPositions = new uint32[count];

				handleMatchedTextPosition.copyToRaw((byte*)textPositions, count * sizeof(uint32), accessLocation::host);
				handleMatchedPatternPosition.copyToRaw((byte*)patternPositions, count * sizeof(uint32), accessLocation::host);

				for(int i=0;i<count;++i)
				{
#if ENABLE_GPU_EXACT_MATCH_VERIFICATION
					matchedPatterns.push_back(std::make_pair(textPositions[i], patternPositions[i]));
#else
					for(std::vector<std::pair<std::string,uint32> >::iterator it = mPatterns[patternPositions[i]]->begin(); it != mPatterns[patternPositions[i]]->end(); ++it)
					{
						if(strncmp(it->first.c_str(), mText->c_str() + textPositions[i], it->first.length()) == 0)
						{
							matchedPatterns.push_back(std::make_pair(textPositions[i], it->second));
							break;
						}
					}
#endif
				}

				SAFE_DELETE_ARRAY(textPositions);
				SAFE_DELETE_ARRAY(patternPositions);
			}
		}

	}

private:
	void clearPatterns()
	{
		for(std::vector<std::vector<std::pair<std::string,uint32> >* >::iterator it = mPatterns.begin(); it != mPatterns.end(); it++)
		{
			SAFE_DELETE(*it);
		}
		mPatterns.clear();
	}

	uint32 hashPattern(const std::string& pattern, uint32 length)
	{
		uint32 h = 0;
		uint32 component = 1;

		for(uint32 i=0; i<length; ++i)
		{
			h += pattern[length - i - 1] * component;
			component *= mPatternHashComponent;
		}

		return h;
	}

private:
	const KernelApi& mKernelApi;

	uint32 mPatternHashComponent;
	HashMap<KernelApi> mPatternHash;
	Transposer<KernelApi> mTransposer;

	bool mPatternOwner;
	std::vector<std::vector<std::pair<std::string,uint32> >* > mPatterns;
	uint32 mPatternMinimalLength;
	uint32 mTextMaximumLength;
	uint32 mMaximumMatchedCount;

	bool mTextOwner;
	const std::string* mText;

	DeviceArray<KernelApi>* mDeviceTextData;
	DeviceArray<KernelApi>* mDeviceTextDataTransposed;
	DeviceArray<KernelApi>* mDeviceMatchedTextPosition;
	DeviceArray<KernelApi>* mDeviceMatchedPatternPosition;
	DeviceArray<KernelApi>* mDeviceMatchedCount;

	zvpcpj::JITContext_t *mJITContext;
};

} } } } /*zillians::vw::processors::cuda*/

#undef ROUND_UP_TO_MULTIPLE

#endif/*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MULTIPLEPATTERNSEARCHER_H_*/
