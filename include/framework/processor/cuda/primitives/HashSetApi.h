/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHSETAPI_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHSETAPI_H_

#include "core/Types.h"

namespace zillians { namespace framework { namespace processor { namespace cuda {

struct HashSetKernelParameters
{
	uint32* hashSet;
	uint32  bucketCount;
	uint32  bucketHash0;
	uint32  bucketHash1;
	uint32  cuckooHash;
};

#define HASHSETAPI_EXPAND_PARAMETERS(param)	\
	param.hashSet, param.bucketCount, param.bucketHash0, param.bucketHash1, param.cuckooHash

#define HASHSETAPI_DECLARE_PARAMETERS	\
	uint32* HashSetParam_HashSet, uint32 HashSetParam_BucketCount, uint32 HashSetParam_BucketHash0, uint32 HashSetParam_BucketHash1, uint32 HashSetParam_CuckooHash

#define HASHSETAPI_FORWARD_PARAMETERS	\
	HashSetParam_HashSet, HashSetParam_BucketCount, HashSetParam_BucketHash0, HashSetParam_BucketHash1, HashSetParam_CuckooHash

#define HASHSETAPI_INIT	\
	uint HashSetVar_c00 = HashSetParam_CuckooHash & 0x2B780D61; \
	uint HashSetVar_c01 = HashSetParam_CuckooHash & 0xF07BE535; \
	uint HashSetVar_c10 = HashSetParam_CuckooHash & 0x2B07D70A; \
	uint HashSetVar_c11 = HashSetParam_CuckooHash & 0xABFB9D52; \
	uint HashSetVar_c20 = HashSetParam_CuckooHash & 0x9D52B07D; \
	uint HashSetVar_c21 = HashSetParam_CuckooHash & 0x8F5C28F6;

#define HASHSETAPI_FIND(key, position)	\
	{ \
		uint32 HashSetVar_bucketId = ((HashSetParam_BucketHash0 + HashSetParam_BucketHash1 * key) % 1900813) % HashSetParam_BucketCount; \
		uint32 HashSetVar_p0 = (HashSetVar_bucketId * 576 + 192 * 0) + (((HashSetParam_CuckooHash_c00 + HashSetParam_CuckooHash_c01 * key) % 1900813) % 192); \
		uint32 HashSetVar_p1 = (HashSetVar_bucketId * 576 + 192 * 1) + (((HashSetParam_CuckooHash_c10 + HashSetParam_CuckooHash_c11 * key) % 1900813) % 192); \
		uint32 HashSetVar_p2 = (HashSetVar_bucketId * 576 + 192 * 2) + (((HashSetParam_CuckooHash_c20 + HashSetParam_CuckooHash_c21 * key) % 1900813) % 192); \
		\
		if(HashSetParam_HashSet[HashSetVar_p0] == key) \
		{ \
			position = p0; \
		} \
		else if(HashSetParam_HashSet[HashSetVar_p1] == key) \
		{ \
			position = p1; \
		} \
		else if(HashSetParam_HashSet[HashSetVar_p2] == key) \
		{ \
			position = p2; \
		} \
		else \
		{ \
			position = UINT_MAX; \
		} \
	}

} } } }

#endif/*THOR_CONTAINER_MAP*/
