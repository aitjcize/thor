/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_SORTER_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_SORTER_H_

#include "framework/processor/cuda/primitives/detail/ThrustKernel.h"

namespace zillians { namespace framework { namespace processor { namespace cuda {

struct SorterDataType { enum type { type_uint64, type_int64, type_uint32, type_int32, type_uint16, type_int16, type_double, type_float }; };

/**
 * @brief Sorter provides GPU sorter as well as CPU sorter implementations.
 *
 * Sorter internally uses thrust to provide both key-only and key-value
 * radix sort on GPU. Also, to compare the result, it provides CPU-version
 * quick sort (recursive and in-place) and radix sort with row-major
 * key-value pair sorting support.
 *
 * To use GPU sorter, you have to call initialize() to prepare for the
 * data which will be used during sort. And then call sort() to sort the
 * given input keys or key-value pairs.
 */
template<typename KernelApi>
class Sorter
{
public:
	Sorter(const KernelApi& api) :
		mKernelApi(api), mKeysOnly(false), mMaxElements(0)
	{ }

	~Sorter()
	{
		finalize();
	}

	/**
	 * @brief Initialize the internal GPU sorter data.
	 *
	 * @param maxElements Maximum number of elements to be sorted.
	 * @param type The data type of the key, support int32/uint32/int64/uint64/float/double
	 * @param keysOnly True for key-only sorting. False otherwise.
	 */
	void initialize(int maxElements, SorterDataType::type type = SorterDataType::type_uint32, bool keysOnly = false, bool forceUsingThrust = false)
	{
		mElementType = type;
		mMaxElements = maxElements;
		mKeysOnly = keysOnly;
	}

	/**
	 * @brief Cleanup the internal GPU sorter data.
	 *
	 * @note This will be called automatically in the destructor.
	 */
	void finalize()
	{
		mMaxElements = 0;
	}

	/**
	 * @brief Sort the given input (keys only) on GPU asynchronously.
	 *
	 * @param keys The given input keys.
	 * @param elements The number of keys.
	 */
	void sort(ArrayHandle<DeviceArray<KernelApi> >* keys, std::size_t elements)
	{
		switch(mElementType)
		{
		case SorterDataType::type_double:
			detail::thrust_sort<double>(keys->template data<double>(), elements, detail::sort_op::less); break;
		case SorterDataType::type_float:
			detail::thrust_sort<float>(keys->template data<float>(), elements, detail::sort_op::less); break;
		case SorterDataType::type_uint64:
			detail::thrust_sort<unsigned long long int>(keys->template data<unsigned long long int>(), elements, detail::sort_op::less); break;
		case SorterDataType::type_int64:
			detail::thrust_sort<long long int>(keys->template data<long long int>(), elements, detail::sort_op::less); break;
		case SorterDataType::type_uint32:
			detail::thrust_sort<unsigned int>(keys->template data<unsigned int>(), elements, detail::sort_op::less); break;
		case SorterDataType::type_int32:
			detail::thrust_sort<int>(keys->template data<int>(), elements, detail::sort_op::less); break;
		case SorterDataType::type_uint16:
			detail::thrust_sort<unsigned short>(keys->template data<unsigned short>(), elements, detail::sort_op::less); break;
		case SorterDataType::type_int16:
			detail::thrust_sort<short>(keys->template data<short>(), elements, detail::sort_op::less); break;
		default:
			BOOST_ASSERT(false);
		}

		keys->dirty();
	}

	/**
	 * @brief Sort the given input (key-value pair in row-major) on GPU asynchronously
	 *
	 * @param keys The given input keys.
	 * @param values The given input values.
	 * @param elements The number of key-value pairs.
	 */
	void sort(ArrayHandle<DeviceArray<KernelApi> >* keys, ArrayHandle<DeviceArray<KernelApi> >* values, std::size_t elements)
	{
		switch(mElementType)
		{
		case SorterDataType::type_double:
			detail::thrust_sort_by_key<double>(keys->template data<double>(), values->template data<double>(), elements, detail::sort_op::less); break;
		case SorterDataType::type_float:
			detail::thrust_sort_by_key<float>(keys->template data<float>(), values->template data<float>(), elements, detail::sort_op::less); break;
		case SorterDataType::type_uint64:
			detail::thrust_sort_by_key<unsigned long long int>(keys->template data<unsigned long long int>(), values->template data<unsigned long long int>(), elements, detail::sort_op::less); break;
		case SorterDataType::type_int64:
			detail::thrust_sort_by_key<long long int>(keys->template data<long long int>(), values->template data<long long int>(), elements, detail::sort_op::less); break;
		case SorterDataType::type_uint32:
			detail::thrust_sort_by_key<unsigned int>(keys->template data<unsigned int>(), values->template data<unsigned int>(), elements, detail::sort_op::less); break;
		case SorterDataType::type_int32:
			detail::thrust_sort_by_key<int>(keys->template data<int>(), values->template data<int>(), elements, detail::sort_op::less); break;
		case SorterDataType::type_uint16:
			detail::thrust_sort_by_key<unsigned short>(keys->template data<unsigned short>(), values->template data<unsigned short>(), elements, detail::sort_op::less); break;
		case SorterDataType::type_int16:
			detail::thrust_sort_by_key<short>(keys->template data<short>(), values->template data<short>(), elements, detail::sort_op::less); break;
		default:
			BOOST_ASSERT(false);
		}

		keys->dirty();
	}

	/**
	 * @brief Sort the given input (key-only) on CPU
	 *
	 * @note For comparable result, we use the same radix sort used on GPU
	 * which produces stable results.
	 *
	 * @param keys The given input keys.
	 * @param elements The number of keys.
	 */
	template<typename K>
	void emulateSortKey(K* keys, std::size_t elements)
	{
		//quickSortKeyOnly<K>(keys, 0, elements);
		//quickSortKeyOnlyFast<K>(keys, elements);
		radixSortKeyOnly<K>(keys, elements, 10);
	}

	/**
	 * @brief Sort the given input (key-value pair in row-major) on CPU
	 *
	 * @note For comparable result, we use the same radix sort used on GPU
	 * which produces stable results.
	 *
	 * @param keys The given input keys.
	 * @param values The given input values.
	 * @param elements The number of key-value pairs.
	 */
	template<typename K, typename V>
	void emulateSortKeyValue(K* keys, V* values, std::size_t elements)
	{
		//quickSortKeyValue<K,V>(keys, values, 0, elements);
		//quickSortKeyValueFast<K,V>(keys, values, elements);
		radixSortKeyValue<K,V>(keys, values, elements, 10);
	}

private:
	template<typename T>
	inline void swap(T *a, T *b)
	{
		T t = *a;
		*a = *b;
		*b = t;
	}

	/**
	 * @brief Sort keys by traditional recursive quick sort.
	 *
	 * @code
	 * 		quickSortKeyOnly<K>(keys, 0, elements);
	 * @endcode
	 *
	 * @param keys The given input keys.
	 * @param beg The start index to sort.
	 * @param end The end index to sort.
	 */
	template<typename K>
	void quickSortKeyOnly(K* keys, int beg, int end)
	{
		if(end > beg + 1)
		{
			K piv = keys[beg], l = beg + 1, r = end;
			while(l < r)
			{
				if(keys[l] <= piv)
					l++;
				else
				{
					--r;
					swap<K>(&keys[l], &keys[r]);
				}
			}
			--l;
			swap<K>(&keys[l], &keys[beg]);

			quickSortKeyOnly<K>(keys, beg, l);
			quickSortKeyOnly<K>(keys, r, end);
		}
	}

	/**
	 * @brief Sort key-value pairs by traditional recursive quick sort.
	 *
	 * @code
	 * 		quickSortKeyValue<K,V>(keys, values, 0, elements);
	 * @endcode
	 *
	 * @param keys The given input keys.
	 * @param values The given input values.
	 * @param beg The start index to sort.
	 * @param end The end index to sort.
	 */
	template<typename K, typename V>
	void quickSortKeyValue(K* keys, V* values, int beg, int end)
	{
		if(end > beg + 1)
		{
			K piv = keys[beg], l = beg + 1, r = end;
			while(l < r)
			{
				if(keys[l] <= piv)
					l++;
				else
				{
					--r;
					swap<K>(&keys[l], &keys[r]);
					swap<V>(&values[l], &values[r]);
				}
			}
			--l;
			swap<K>(&keys[l], &keys[beg]);
			swap<V>(&values[l], &values[beg]);

			quickSortKeyValue<K,V>(keys, values, beg, l);
			quickSortKeyValue<K,V>(keys, values, r, end);
		}
	}

	/**
	 * @brief Sort keys by in-place (non-recursive) quick sort.
	 *
	 * @code
	 * 		quickSortKeyOnlyFast<K>(keys, elements);
	 * @endcode
	 *
	 * @param keys The given input keys.
	 * @param elements The number of keys.
	 */
	template<typename K>
	void quickSortKeyOnlyFast(K* keys, int elements)
	{
#define  MAX_LEVELS  300

		K piv_key;
		int beg[MAX_LEVELS], end[MAX_LEVELS], i = 0, L, R, swap;


		beg[0] = 0;
		end[0] = elements;
		while (i >= 0)
		{
			L = beg[i];
			R = end[i] - 1;
			if (L < R)
			{
				piv_key = keys[L];
				while (L < R)
				{
					while (keys[R] >= piv_key && L < R)
					{
						R--;
					}
					if (L < R)
					{
						keys[L] = keys[R];
						L++;
					}

					while (keys[L] <= piv_key && L < R)
					{
						L++;
					}
					if (L < R)
					{
						keys[R] = keys[L];
						R--;
					}
				}

				keys[L] = piv_key;

				beg[i + 1] = L + 1;
				end[i + 1] = end[i];
				end[i++] = L;

				if (end[i] - beg[i] > end[i - 1] - beg[i - 1])
				{
					swap = beg[i];
					beg[i] = beg[i - 1];
					beg[i - 1] = swap;

					swap = end[i];
					end[i] = end[i - 1];
					end[i - 1] = swap;
				}
			}
			else
			{
				i--;
			}
		}
	}

	/**
	 * @brief Sort keys by in-place (non-recursive) quick sort.
	 *
	 * @code
	 * 		quickSortKeyOnlyFast<K,V>(keys, values, elements);
	 * @endcode
	 *
	 * @param keys The given input keys.
	 * @param values The given input values.
	 * @param elements The number of key-value pairs.
	 */
	template<typename K, typename V>
	void quickSortKeyValueFast(K* keys, V* values, int elements)
	{
#define  MAX_LEVELS  300

		K piv_key;
		V piv_value;
		int beg[MAX_LEVELS], end[MAX_LEVELS], i = 0, L, R, swap;


		beg[0] = 0;
		end[0] = elements;
		while (i >= 0)
		{
			L = beg[i];
			R = end[i] - 1;
			if (L < R)
			{
				piv_key = keys[L];
				piv_value = values[L];
				while (L < R)
				{
					while (keys[R] >= piv_key && L < R)
					{
						R--;
					}
					if (L < R)
					{
						keys[L] = keys[R];
						values[L] = values[R];
						L++;
					}

					while (keys[L] <= piv_key && L < R)
					{
						L++;
					}
					if (L < R)
					{
						keys[R] = keys[L];
						values[R] = values[L];
						R--;
					}
				}

				keys[L] = piv_key;
				values[L] = piv_value;

				beg[i + 1] = L + 1;
				end[i + 1] = end[i];
				end[i++] = L;

				if (end[i] - beg[i] > end[i - 1] - beg[i - 1])
				{
					swap = beg[i];
					beg[i] = beg[i - 1];
					beg[i - 1] = swap;

					swap = end[i];
					end[i] = end[i - 1];
					end[i - 1] = swap;
				}
			}
			else
			{
				i--;
			}
		}
	}

#define RADIX 10
	/**
	 * @brief Sort keys by radix sort.
	 *
	 * @note The default the radix is 10, so the maximum digits of a
	 * decimal number is 10.
	 *
	 * @code
	 * 		radixSortKeyOnly<K>(keys, elements, 10);
	 * @endcode
	 *
	 * @param keys The given input keys.
	 * @param elements The number of keys.
	 * @param num_digits The maximum number of digits of given input keys.
	 */
	template<typename K>
	void radixSortKeyOnly(K* keys, int elements, int num_digits)
	{
	    int bin[RADIX];
	    int column = 1, i, j;

	    K* auxKeys = new K[elements];

	    for (j = 0; j < num_digits; j++) {

	        /* set up bins, count elements with
	         * each sort of digit
	         */
	        for (i = 0; i < RADIX; i++) {
	            bin[i] = 0;
	        }
	        for (i = 0; i < elements; i++) {
	            int dig = keys[i] / column % RADIX;
	            bin[dig]++;
	        }

	        /* work out the starting index
	         * for each bin
	         */
	        bin[RADIX-1] = elements - bin[RADIX-1];
	        for (i = RADIX - 2; i > -1; i--) {
	            bin[i] = bin[i+1] - bin[i];
	        }

	        /* hash each element by the current digit */
	        for (i = 0; i < elements; i++) {
	            int dig = keys[i] / column % RADIX;
	            auxKeys[bin[dig]] = keys[i];
	            bin[dig]++;
	        }

	        /* copy elements from auxList to list */
	        for (i = 0; i < elements; i++) {
	            keys[i] = auxKeys[i];
	        }

	        column *= RADIX;
	    }

	    delete[] auxKeys;
	}

	/**
	 * @brief Sort key-value pairs by radix sort.
	 *
	 * @note The default the radix is 10, so the maximum digits of a
	 * decimal number is 10.
	 *
	 * @code
	 * 		radixSortKeyValue<K,V>(keys, values, elements, 10);
	 * @endcode
	 *
	 * @param keys The given input keys.
	 * @param values The given input values.
	 * @param elements The number of key-value pairs.
	 * @param num_digits The maximum number of digits of given input keys.
	 */
	template<typename K, typename V>
	void radixSortKeyValue(K* keys, V* values, int elements, int num_digits)
	{
	    int bin[RADIX];
	    int column = 1, i, j;

	    K* auxKeys = new K[elements];
	    V* auxValues = new V[elements];

	    for (j = 0; j < num_digits; j++) {

	        /* set up bins, count elements with
	         * each sort of digit
	         */
	        for (i = 0; i < RADIX; i++) {
	            bin[i] = 0;
	        }
	        for (i = 0; i < elements; i++) {
	            int dig = keys[i] / column % RADIX;
	            bin[dig]++;
	        }

	        /* work out the starting index
	         * for each bin
	         */
	        bin[RADIX-1] = elements - bin[RADIX-1];
	        for (i = RADIX - 2; i > -1; i--) {
	            bin[i] = bin[i+1] - bin[i];
	        }

	        /* hash each element by the current digit */
	        for (i = 0; i < elements; i++) {
	            int dig = keys[i] / column % RADIX;
	            auxKeys[bin[dig]] = keys[i];
	            auxValues[bin[dig]] = values[i];
	            bin[dig]++;
	        }

	        /* copy elements from auxList to list */
	        for (i = 0; i < elements; i++) {
	            keys[i] = auxKeys[i];
	            values[i] = auxValues[i];
	        }

	        column *= RADIX;
	    }

	    delete[] auxKeys;
	    delete[] auxValues;
	}

private:
	const KernelApi& mKernelApi;

	std::size_t				mMaxElements;
	SorterDataType::type	mElementType;

	bool					mKeysOnly;
};

} } } }

#endif/*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_SORTER_H_*/
