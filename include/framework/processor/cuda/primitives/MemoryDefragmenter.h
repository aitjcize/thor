/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MEMORYDEFRAGMENTER_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MEMORYDEFRAGMENTER_H_

#include "core/Singleton.h"
#include "core/Prerequisite.h"
#include "framework/processor/cuda/kernel/ArrayHandle.h"
#include "framework/processor/cuda/kernel/DeviceArray.h"
#include "framework/processor/cuda/primitives/detail/MemoryDefragmenterKernel.h"
#include "framework/processor/cuda/primitives/Scanner.h"
#include "vw/support/CudaHelper.h"

//#define DEBUG
#define DEFAULT_DEFRAG_MODE 3

namespace zvs = zillians::vw::support;

namespace zillians { namespace framework { namespace processor { namespace cuda {

//=====================================
// DefragModeMgr
//=====================================

class DefragModeMgr : public Singleton<DefragModeMgr, SingletonInitialization::automatic>
{
    friend class Singleton<DefragModeMgr, SingletonInitialization::automatic>;
public:
    uint32 &getMode()
    {
        return mMode;
    }
private:
    uint32 mMode;

    DefragModeMgr()
        : mMode(0)
    {
    }
};

//=====================================
// MemoryDefragmenter
//=====================================

/**
 * MemoryDefragmenter is the kernel of GPU memory management
 *
 * The GPU memory system is composed of per-thread local heap, which we called a segment.
 * An allocation on GPU in each segment (performed by a thread) is called a block. Blocks
 * could be arbitrary size but are always aligned on a spcific boundary (for ex,
 * currently we align all allocation to 8bytes multiples). So each local heap/segment
 * can consists of numbers of blocks, which is composed by multiple chunks of the same
 * size (8 bytes). A chunk could be data or special block header to indicate the size of
 * the block as well as the indirection pointer of the block.
 *
 * Since the allocations would be very sparse and hard to manage, MemoryDefragmenter is
 * used to defragment the memory in an extremely efficient manner and update indirection
 * table entries (which is a pointer pointing to the chunk in the memory management system)
 */

template<typename KernelApi>
class MemoryDefragmenter
{
private:
    typedef Scanner<KernelApi> CudaScanner;
    typedef DeviceArray<KernelApi> CudaDeviceArray;
public:
    MemoryDefragmenter(const KernelApi &Api, CudaScanner* _Scanner)
        : mKernelApi(Api),
          mScanner(_Scanner)
    {
        DefragModeMgr::instance()->getMode() = DEFAULT_DEFRAG_MODE;
    }
    void defrag(/*IN*/    ArrayHandle<CudaDeviceArray>* dh_SegmentSize,
                /*IN*/    ArrayHandle<CudaDeviceArray>* dh_SegmentHeap,
                /*INOUT*/ ArrayHandle<CudaDeviceArray>* dh_IndirectionTable,
                /*OUT*/   ArrayHandle<CudaDeviceArray>* dh_SegmentStrip,
                          uint32                        SegmentCount
        )
    {
        BOOST_ASSERT(!!mScanner            && "must be initialized first");
        BOOST_ASSERT(!!dh_SegmentSize      && "must be initialized first");
        BOOST_ASSERT(!!dh_SegmentHeap      && "must be initialized first");
        BOOST_ASSERT(!!dh_IndirectionTable && "must be initialized first");
        BOOST_ASSERT(!!dh_SegmentStrip     && "must be initialized first");
        //=============================================================================
        #ifdef DEBUG
            std::cout << "SegmentCount=" << SegmentCount << std::endl;
            std::cout << "SegmentSize=";
            zvs::CudaHelper<KernelApi>::instance()->template dump<uint32>(*dh_SegmentSize);
        #endif
        //=============================================================================
        CudaDeviceArray d_PrefixSum(
            mKernelApi,
            SegmentCount*sizeof(uint32)
            );
        ArrayHandle<CudaDeviceArray> dh_PrefixSum(
            d_PrefixSum,
            accessLocation::device,
            accessMode::readwrite
            );
        uint32 SumSegmentSize =
            zvs::CudaHelper<KernelApi>::instance()->template scanSum<uint32>(
                mKernelApi, mScanner,
                dh_SegmentSize, SegmentCount, &dh_PrefixSum
                );
        uint32 MaxSegmentSize =
            zvs::CudaHelper<KernelApi>::instance()->template scanMax<uint32>(
                mKernelApi, mScanner,
                dh_SegmentSize, SegmentCount
                );
        //=============================================================================
        #ifdef DEBUG
            std::cout << "PrefixSum=";
            zvs::CudaHelper<KernelApi>::instance()->template dump<uint32>(dh_PrefixSum);
            std::cout << "SumSegmentSize=" << SumSegmentSize << std::endl;
            std::cout << "MaxSegmentSize=" << MaxSegmentSize << std::endl;
        #endif
        //=============================================================================
        uint32 HeapSize = SegmentCount*MaxSegmentSize;
        detail::MemoryDefragmenterKernel_Defrag(
                      DefragModeMgr::instance()->getMode(),
            /*IN*/    dh_SegmentSize->     template data<uint32>(),
            /*IN*/    dh_PrefixSum.        template data<uint32>(), // segment chunk start
            /*IN*/    dh_SegmentHeap->     template data<uint64>(),
            /*INOUT*/ dh_IndirectionTable->template data<uint64>(),
            /*OUT*/   dh_SegmentStrip->    template data<uint64>(),
                      SegmentCount,
                      SumSegmentSize,
                      MaxSegmentSize,
                      HeapSize
            );
        dh_IndirectionTable->dirty();
        dh_SegmentStrip->dirty();
        //=============================================================================
        #ifdef DEBUG
            std::cout << "IndirectionTable=";
            zvs::CudaHelper<KernelApi>::instance()->template dump<uint64>(*dh_IndirectionTable);
            std::cout << "SegmentStrip[" << dh_SegmentStrip->template data<uint64>() << "]=";
            zvs::CudaHelper<KernelApi>::instance()->template dump<uint64>(*dh_SegmentStrip);
        #endif
        //=============================================================================
    }
private:
    const KernelApi &mKernelApi;
    CudaScanner*     mScanner;
};

template<typename KernelApi>
class MemoryAllocator
{
private:
    typedef Scanner<KernelApi> CudaScanner;
    typedef DeviceArray<KernelApi> CudaDeviceArray;
public:
    MemoryAllocator(const KernelApi &Api, CudaScanner* _Scanner, uint32 SegmentCount, uint32 MaxSegmentSize)
        : mMemoryDefragmenter(Api, _Scanner),
          mSegmentCount(SegmentCount),
          md_SegmentSize(Api, SegmentCount*sizeof(uint32))
    {
        // init problem size
        uint32 HeapSize             = SegmentCount*MaxSegmentSize;
        uint32 IndirectionTableSize = SegmentCount; // 1 block per segment
        uint32 SegmentStripSize     = SegmentCount*MaxSegmentSize;

        // allocate input
        md_SegmentSize      = new CudaDeviceArray(Api, SegmentCount        *sizeof(uint32));
        md_SegmentHeap      = new CudaDeviceArray(Api, HeapSize            *sizeof(uint64));
        md_IndirectionTable = new CudaDeviceArray(Api, IndirectionTableSize*sizeof(uint64));
        md_SegmentStrip     = new CudaDeviceArray(Api, SegmentStripSize    *sizeof(uint64));
    }
    virtual ~MemoryAllocator()
    {
    	delete md_SegmentSize;
    	delete md_SegmentHeap;
    	delete md_IndirectionTable;
    	delete md_SegmentStrip;
    }
    void reset(uint32 SegmentIndex)
    {
        zvpc::ArrayHandle<CudaDeviceArray> dh_SegmentSize(*md_SegmentSize, zvpc::accessLocation::host, zvpc::accessMode::readwrite);
        detail::resetArrayValue(
        /*OUT*/ dh_SegmentSize.template data<uint32>(),
                SegmentIndex,
                mSegmentCount
                );
    }
    void allocate(uint32 SegmentIndex, uint32 SizeBytes)
    {
        BOOST_ASSERT(SegmentIndex<mSegmentCount && "invalid thread index");
#ifdef ENABLE_STD_MALLOC
        malloc(sizeof(char)*SizeBytes); // NOTE: should we return this ?? or record it in IndirectionTable ??
#else
        zvpc::ArrayHandle<CudaDeviceArray> dh_SegmentSize(*md_SegmentSize, zvpc::accessLocation::host, zvpc::accessMode::readwrite);
        detail::addArrayValue(
        /*OUT*/ dh_SegmentSize.template data<uint32>(),
                SegmentIndex,
                SizeBytes,
                mSegmentCount
                );
#endif
    }
    void defrag()
    {
        BOOST_ASSERT(!!md_SegmentSize      && "must be initialized first");
        BOOST_ASSERT(!!md_SegmentHeap      && "must be initialized first");
        BOOST_ASSERT(!!md_IndirectionTable && "must be initialized first");
        BOOST_ASSERT(!!md_SegmentStrip     && "must be initialized first");
        zvpc::ArrayHandle<CudaDeviceArray> dh_SegmentSize(     *md_SegmentSize,      zvpc::accessLocation::device, zvpc::accessMode::read);
        zvpc::ArrayHandle<CudaDeviceArray> dh_SegmentHeap(     *md_SegmentHeap,      zvpc::accessLocation::device, zvpc::accessMode::read);
        zvpc::ArrayHandle<CudaDeviceArray> dh_IndirectionTable(*md_IndirectionTable, zvpc::accessLocation::device, zvpc::accessMode::readwrite);
        zvpc::ArrayHandle<CudaDeviceArray> dh_SegmentStrip(    *md_SegmentStrip,     zvpc::accessLocation::device, zvpc::accessMode::readwrite);
    	mMemoryDefragmenter.defrag(
            /*IN*/    &dh_SegmentSize,
            /*IN*/    &dh_SegmentHeap,
            /*INOUT*/ &dh_IndirectionTable,
            /*OUT*/   &dh_SegmentStrip,
                       mSegmentCount
            );
    }
private:
    MemoryDefragmenter<KernelApi> mMemoryDefragmenter;
    uint32 mSegmentCount;
    CudaDeviceArray* md_SegmentSize;
    CudaDeviceArray* md_SegmentHeap;
    CudaDeviceArray* md_IndirectionTable;
    CudaDeviceArray* md_SegmentStrip;
};

} } } } /*zillians::vw::processors::cuda*/

#ifdef DEBUG
    #undef DEBUG
#endif

#endif /*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MEMORYDEFRAGMENTER_H_*/
