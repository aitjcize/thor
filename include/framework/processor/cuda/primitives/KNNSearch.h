/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_KNNSEARCH_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_KNNSEARCH_H_

#include "core/Common.h"
#include "core/Singleton.h"
#include "core/Prerequisite.h"
#include "framework/processor/cuda/kernel/HostArray.h"
#include "framework/processor/cuda/kernel/DeviceArray.h"
#include "framework/processor/cuda/kernel/ArrayHandle.h"
#include "framework/processor/cuda/primitives/detail/KNNSearchKernel.h"
#include "framework/processor/cuda/primitives/Scanner.h"
#include "vw/support/CudaHelper.h"

//#define DEBUG
#define DEFAULT_STEP_MODE  zvpc::StepModeType::  TYPE_3
#define DEFAULT_QUERY_MODE zvpc::QueryModeType:: TYPE_4

#define ENABLE_BLOCK_PADDING

namespace zvpc = zillians::vw::processors::cuda;
namespace zv   = zillians::vw;

namespace zillians { namespace framework { namespace processor { namespace cuda {

//=====================================
// StepModeType
//=====================================

class StepModeType : public Singleton<StepModeType, SingletonInitialization::automatic>
{
    friend class Singleton<StepModeType, SingletonInitialization::automatic>;
public:
    typedef enum
    {
        TYPE_0,
        TYPE_1,
        TYPE_2,
        TYPE_3,
        TYPE_4,
        TYPE_MAX
    } type;

    uint32 &getType()
    {
        return mType;
    }
private:
    uint32 mType;

    StepModeType()
        : mType(DEFAULT_STEP_MODE)
    {
    }
};

//=====================================
// QueryModeType
//=====================================

class QueryModeType : public Singleton<QueryModeType, SingletonInitialization::automatic>
{
    friend class Singleton<QueryModeType, SingletonInitialization::automatic>;
public:
    typedef enum
    {
        TYPE_0,
        TYPE_1,
        TYPE_2,
        TYPE_3,
        TYPE_4,
        TYPE_MAX
    } type;

    uint32 &getType()
    {
        return mType;
    }
private:
    uint32 mType;

    QueryModeType()
        : mType(DEFAULT_QUERY_MODE)
    {
    }
};

//=====================================
// KNNSearch
//=====================================

/**
  *KNNSearch is the kernel of ??
 */
template<typename KernelApi, uint32 Dimension>
class KNNSearch;

template<typename KernelApi>
class KNNSearch<KernelApi, 2>
{
private:
    typedef DeviceArray<KernelApi> CudaDeviceArray;
public:
    KNNSearch(const KernelApi &KernelApiObj)
        : mKernelApi(KernelApiObj),
          mIsInit(false),
          mpScanner(NULL),
          mColCount(0),
          mRowCount(0),
          mCellCount(0),
          mpd_CellIDArr(NULL),
          mpd_CellPosArr(NULL),
          mpd_CellIndexPtrArr(NULL),
          mpd_CellFreeArr(NULL),
          mpCellIDTex(NULL),
          mpCellPosTex(NULL),
          mpCellFreeTex(NULL),
          mIsUseTex(false),
          mIsNeedCacheTex(false)
    {
        StepModeType::instance()->  getType() = DEFAULT_STEP_MODE;
        QueryModeType::instance()-> getType() = DEFAULT_QUERY_MODE;
    }
    virtual ~KNNSearch()
    {
        if(mIsInit)
            finalize();
    }
    CudaDeviceArray *&getCellIDArr()       { return mpd_CellIDArr;       }
    CudaDeviceArray *&getCellPosArr()      { return mpd_CellPosArr;      }
    CudaDeviceArray *&getCellIndexPtrArr() { return mpd_CellIndexPtrArr; }
    CudaDeviceArray *&getCellFreeArr()     { return mpd_CellFreeArr;     }
    //=============================================================================
    void initialize(
        Scanner<KernelApi> *pScanner,
        uint32              ColCount,
        uint32              RowCount,
        bool                IsUseTex = false
        )
    {
        if(mIsInit)
            return;
        BOOST_ASSERT((pScanner != NULL) && "must be initialized first");
        BOOST_ASSERT((ColCount != 0) && (RowCount != 0) && "invalid dimensions");
        mpScanner  = pScanner;
        mColCount  = ColCount;
        mRowCount  = RowCount;
        mCellCount = ColCount*RowCount;
        mpd_CellIDArr       = new CudaDeviceArray(mKernelApi, mCellCount*sizeof(uint32));
        mpd_CellPosArr      = new CudaDeviceArray(mKernelApi, mCellCount*sizeof(float2));
        mpd_CellIndexPtrArr = new CudaDeviceArray(mKernelApi, mCellCount*sizeof(uint64));
        mpd_CellFreeArr     = new CudaDeviceArray(mKernelApi, mCellCount*sizeof(uint32));
        {
            ArrayHandle<CudaDeviceArray> dh_State_CellIDArr(       *mpd_CellIDArr,       accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellPosArr(      *mpd_CellPosArr,      accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellIndexPtrArr( *mpd_CellIndexPtrArr, accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellFreeArr(     *mpd_CellFreeArr,     accessLocation::device, accessMode::readwrite);
            dh_State_CellIDArr       .fill(0);
            dh_State_CellPosArr      .fill(0);
            dh_State_CellIndexPtrArr .fill(0x0);
            dh_State_CellFreeArr     .fill(1);
        }
        if(IsUseTex)
        {
            mKernelApi.texture.template create2D<uint32>(ColCount, RowCount, &mpCellIDTex);
            mKernelApi.texture.template create2D<float2>(ColCount, RowCount, &mpCellPosTex);
            mKernelApi.texture.template create2D<uint32>(ColCount, RowCount, &mpCellFreeTex);
            mpCellIDTex->   bind("CellIDTex");
            mpCellPosTex->  bind("CellPosTex");
            mpCellFreeTex-> bind("CellFreeTex");
        }
        mIsUseTex = IsUseTex;
        mIsInit   = true;
    }
    //=============================================================================
    void finalize()
    {
        if(!mIsInit)
            return;
        mpScanner = NULL;
        SAFE_DELETE(mpd_CellIDArr);
        SAFE_DELETE(mpd_CellPosArr);
        SAFE_DELETE(mpd_CellIndexPtrArr);
        SAFE_DELETE(mpd_CellFreeArr);
        if(mIsUseTex)
        {
            mKernelApi.texture.template destroy(mpCellIDTex);
            mKernelApi.texture.template destroy(mpCellPosTex);
            mKernelApi.texture.template destroy(mpCellFreeTex);
            mpCellIDTex   = NULL;
            mpCellPosTex  = NULL;
            mpCellFreeTex = NULL;
        }
        mIsInit = false;
    }
    //=============================================================================
    void add(
        /*IN*/    ArrayHandle<CudaDeviceArray> *pAddCellIDArr,
        /*IN*/    ArrayHandle<CudaDeviceArray> *pAddCellPosArr,
        /*INOUT*/ ArrayHandle<CudaDeviceArray> *pAddCellIndexPtrArr,
                  uint32                        AddCount
        )
    {
        BOOST_ASSERT((pAddCellIDArr       != NULL) && "must be initialized first");
        BOOST_ASSERT((pAddCellPosArr      != NULL) && "must be initialized first");
        BOOST_ASSERT((pAddCellIndexPtrArr != NULL) && "must be initialized first");
        BOOST_ASSERT((AddCount != 0)               && "invalid dimensions");
        //=============================================================================
        #ifdef DEBUG
            std::cout << "AddCount=" << AddCount << std::endl;
            std::cout << "AddCellIDArr=";
            zv::support::CudaHelper<KernelApi>::instance()->template dump<uint32>(*pAddCellIDArr);
        #endif
        #ifdef DEBUG
            std::cout << "State_CellCount=" << mCellCount << std::endl;
            std::cout << "State_CellIDArr=";
            zv::support::CudaHelper<KernelApi>::instance()->template dump<uint32>(*mpd_CellIDArr);
            std::cout << "State_CellFreeArr=";
            zv::support::CudaHelper<KernelApi>::instance()->template dump<uint32>(*mpd_CellFreeArr);
        #endif
        //=============================================================================
        // Step 2: generate PrefixSum array from CellFree array
        CudaDeviceArray d_PrefixSumArr(  mKernelApi, mCellCount*sizeof(uint32));
        CudaDeviceArray d_WriteIndexArr( mKernelApi, AddCount  *sizeof(uint32));
        {
            ArrayHandle<CudaDeviceArray> dh_State_CellIDArr(       *mpd_CellIDArr,       accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellPosArr(      *mpd_CellPosArr,      accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellIndexPtrArr( *mpd_CellIndexPtrArr, accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellFreeArr(     *mpd_CellFreeArr,     accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_PrefixSumArr(           d_PrefixSumArr,      accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_WriteIndexArr(          d_WriteIndexArr,     accessLocation::device, accessMode::readwrite);
			#ifdef DEBUG
            	uint32 SumCellFree =
			#endif
			zv::support::CudaHelper<KernelApi>::instance()->template scanSum<uint32>(
				mKernelApi, mpScanner,
				&dh_State_CellFreeArr, mCellCount, &dh_PrefixSumArr
				);
            //=============================================================================
            #ifdef DEBUG
                std::cout << "PrefixSumArr=";
                zv::support::CudaHelper<KernelApi>::instance()->template dump<uint32>(dh_PrefixSumArr);
                std::cout << "SumCellFree=" << SumCellFree << std::endl;
            #endif
            //=============================================================================
            detail::KNNSearchKernel_Add(
                /*IN*/    pAddCellIDArr->       template data<uint32>(),
                /*IN*/    pAddCellPosArr->      template data<float2>(),
                /*IN*/    pAddCellIndexPtrArr-> template data<uint64>(),
                          AddCount,
                /*OUT*/   dh_State_CellIDArr       .template data<uint32>(),
                /*OUT*/   dh_State_CellPosArr      .template data<float2>(),
                /*INOUT*/ dh_State_CellIndexPtrArr .template data<uint64>(),
                /*INOUT*/ dh_State_CellFreeArr     .template data<uint32>(),
                          mCellCount,
                /*IN*/    dh_PrefixSumArr.  template data<uint32>(),
                /*INOUT*/ dh_WriteIndexArr. template data<uint32>()
                );
            dh_State_CellIDArr       .dirty();
            dh_State_CellPosArr      .dirty();
            dh_State_CellIndexPtrArr .dirty();
            dh_State_CellFreeArr     .dirty();
            dh_WriteIndexArr         .dirty();
            //=============================================================================
            #ifdef DEBUG
                std::cout << "WriteIndex=";
                zv::support::CudaHelper<KernelApi>::instance()->template dump<uint32>(dh_WriteIndexArr);
            #endif
            //=============================================================================
        }
        //=============================================================================
        #ifdef DEBUG
            std::cout << "State_CellCount=" << mCellCount << std::endl;
            std::cout << "State_CellIDArr=";
            zv::support::CudaHelper<KernelApi>::instance()->template dump<uint32>(*mpd_CellIDArr);
            std::cout << "State_CellFreeArr=";
            zv::support::CudaHelper<KernelApi>::instance()->template dump<uint32>(*mpd_CellFreeArr);
        #endif
        //=============================================================================
    }
    //=============================================================================
    void remove(
        /*IN*/ ArrayHandle<CudaDeviceArray> *pRemoveCellIndexArr,
               uint32                        RemoveCount,
               bool                          IsUpdateCellIndexPtr = false
        )
    {
        BOOST_ASSERT((pRemoveCellIndexArr != NULL) && "must be initialized first");
        BOOST_ASSERT((RemoveCount != 0)            && "invalid dimensions");
        {
            ArrayHandle<CudaDeviceArray> dh_State_CellIDArr(       *mpd_CellIDArr,       accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellPosArr(      *mpd_CellPosArr,      accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellIndexPtrArr( *mpd_CellIndexPtrArr, accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellFreeArr(     *mpd_CellFreeArr,     accessLocation::device, accessMode::readwrite);
            detail::KNNSearchKernel_Remove(
                /*IN*/    pRemoveCellIndexArr->template data<uint32>(),
                          RemoveCount,
                          IsUpdateCellIndexPtr,
                /*OUT*/   dh_State_CellIDArr       .template data<uint32>(),
                /*OUT*/   dh_State_CellPosArr      .template data<float2>(),
                /*INOUT*/ dh_State_CellIndexPtrArr .template data<uint64>(),
                /*INOUT*/ dh_State_CellFreeArr     .template data<uint32>(),
                          mCellCount
                );
            dh_State_CellIDArr       .dirty();
            dh_State_CellPosArr      .dirty();
            dh_State_CellIndexPtrArr .dirty();
            dh_State_CellFreeArr     .dirty();
        }
    }
    //=============================================================================
    void update(
        /*IN*/ ArrayHandle<CudaDeviceArray> *pUpdateCellIndexArr,
        /*IN*/ ArrayHandle<CudaDeviceArray> *pUpdateCellPosArr,
               uint32                        UpdateCount
        )
    {
        BOOST_ASSERT((pUpdateCellIndexArr != NULL) && "must be initialized first");
        BOOST_ASSERT((pUpdateCellPosArr   != NULL) && "must be initialized first");
        BOOST_ASSERT((UpdateCount != 0)            && "invalid dimensions");
        {
            ArrayHandle<CudaDeviceArray> dh_State_CellPosArr(*mpd_CellPosArr, accessLocation::device, accessMode::readwrite);
            detail::KNNSearchKernel_Update(
                /*IN*/  pUpdateCellIndexArr-> template data<uint32>(),
                /*IN*/  pUpdateCellPosArr->   template data<float2>(),
                        UpdateCount,
                /*OUT*/ dh_State_CellPosArr.template data<float2>(),
                        mCellCount
                );
            dh_State_CellPosArr.dirty();
        }
    }
    //=============================================================================
    void step(uint32 RepeatIters = 1)
    {
        if(StepModeType::instance()->getType() >= StepModeType::TYPE_MAX)
            return;
        //=============================================================================
        #ifdef DEBUG
            std::cout << "RepeatIters=" << RepeatIters << std::endl;
        #endif
        #ifdef DEBUG
            std::cout << "State_CellIDArr=";
            zv::support::CudaHelper<KernelApi>::instance()->template dump<uint32>(*mpd_CellIDArr, mColCount);
        #endif
        //=============================================================================
        {
            ArrayHandle<CudaDeviceArray> dh_State_CellIDArr(       *mpd_CellIDArr,       accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellPosArr(      *mpd_CellPosArr,      accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellIndexPtrArr( *mpd_CellIndexPtrArr, accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellFreeArr(     *mpd_CellFreeArr,     accessLocation::device, accessMode::readwrite);
            for(uint32 i = 0; i<RepeatIters; i++)
                detail::KNNSearchKernel_Step(
                              StepModeType::instance()->getType(),
                    /*INOUT*/ dh_State_CellIDArr       .template data<uint32>(),
                    /*INOUT*/ dh_State_CellPosArr      .template data<float2>(),
                    /*INOUT*/ dh_State_CellIndexPtrArr .template data<uint64>(),
                    /*INOUT*/ dh_State_CellFreeArr     .template data<uint32>(),
                              mColCount,
                              mRowCount
                    );
            dh_State_CellIDArr       .dirty();
            dh_State_CellPosArr      .dirty();
            dh_State_CellIndexPtrArr .dirty();
            dh_State_CellFreeArr     .dirty();
        }
        //=============================================================================
        #ifdef DEBUG
            std::cout << "State_CellIDArr=";
            zv::support::CudaHelper<KernelApi>::instance()->template dump<uint32>(*mpd_CellIDArr, mColCount);
        #endif
        //=============================================================================
    }
    //=============================================================================
    static void calcSearchParams(
        uint32  Query_k,
        uint32 *pQueryCellRadius  = NULL,
        uint32 *pJobsPerQueryEdge = NULL,
        uint32 *pJobsPerQuery     = NULL,
        uint32 *pBlockPadding     = NULL
        )
    {
        uint32 IdealJobsPerQuery     = ((Query_k-1)/16+1)*16;
        uint32 IdealJobsPerQueryEdge =
            static_cast<uint32>(sqrt(
                static_cast<double>(IdealJobsPerQuery)
                ));
        uint32 QueryCellRadius  = (IdealJobsPerQueryEdge-1)/2;
        uint32 JobsPerQueryEdge = QueryCellRadius*2+1;
        uint32 JobsPerQuery     = JobsPerQueryEdge*JobsPerQueryEdge;
        #ifdef ENABLE_BLOCK_PADDING
            if(pBlockPadding != NULL)
            {
                uint32 BlockPadding = (IdealJobsPerQuery>JobsPerQuery)?
                    IdealJobsPerQuery-JobsPerQuery:0;
                *pBlockPadding = BlockPadding;
                #ifdef DEBUG
                    std::cout << "BlockPadding=" << BlockPadding << std::endl;
                #endif
            }
            JobsPerQueryEdge = IdealJobsPerQueryEdge;
            JobsPerQuery     = IdealJobsPerQuery;
        #endif
        //=============================================================================
        #ifdef DEBUG
            std::cout << "IdealJobsPerQueryEdge=" << IdealJobsPerQueryEdge << std::endl;
            std::cout << "IdealJobsPerQuery="     << IdealJobsPerQuery     << std::endl;
            std::cout << "QueryCellRadius="       << QueryCellRadius       << std::endl;
            std::cout << "JobsPerQueryEdge="      << JobsPerQueryEdge      << std::endl;
            std::cout << "JobsPerQuery="          << JobsPerQuery          << std::endl;
        #endif
        //=============================================================================
        if(pQueryCellRadius  != NULL) *pQueryCellRadius  = QueryCellRadius;
        if(pJobsPerQueryEdge != NULL) *pJobsPerQueryEdge = JobsPerQueryEdge;
        if(pJobsPerQuery     != NULL) *pJobsPerQuery     = JobsPerQuery;
    }
    //=============================================================================
    static uint32 calcBlockPadding(uint32 Query_k)
    {
        uint32 QueryCellRadius  = 0;
        uint32 JobsPerQueryEdge = 0;
        uint32 JobsPerQuery     = 0;
        uint32 BlockPadding     = 0;
        calcSearchParams(
            Query_k,
            &QueryCellRadius,
            &JobsPerQueryEdge,
            &JobsPerQuery,
            &BlockPadding
            );
        return BlockPadding;
    }
    //=============================================================================
    void cacheTex()
    {
        if(!mIsUseTex)
            return;
        {
            ArrayHandle<CudaDeviceArray> dh_State_CellIDArr(   *mpd_CellIDArr,   accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellPosArr(  *mpd_CellPosArr,  accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellFreeArr( *mpd_CellFreeArr, accessLocation::device, accessMode::readwrite);
            mpCellIDTex->   copyFromRaw(dh_State_CellIDArr   .data(), 0, dh_State_CellIDArr   .array().size(), KernelApi::location::device);
            mpCellPosTex->  copyFromRaw(dh_State_CellPosArr  .data(), 0, dh_State_CellPosArr  .array().size(), KernelApi::location::device);
            mpCellFreeTex-> copyFromRaw(dh_State_CellFreeArr .data(), 0, dh_State_CellFreeArr .array().size(), KernelApi::location::device);
        }
    }
    bool &isNeedCacheTex()
    {
        return mIsNeedCacheTex;
    }
    //=============================================================================
    bool search(
        /*IN*/  ArrayHandle<CudaDeviceArray> *pQueryCellIndexArr,
        /*IN*/  ArrayHandle<CudaDeviceArray> *pQueryRadiusArr,
                uint32                        QueryCount,
                uint32                        Query_k,
        /*OUT*/ ArrayHandle<CudaDeviceArray> *pNeighborCellIDArr,
        /*OUT*/ ArrayHandle<CudaDeviceArray> *pNeighborCellIDCountArr
        )
    {
        if(
            QueryModeType::instance()->getType() >= QueryModeType::TYPE_MAX ||
            (QueryModeType::instance()->getType() == QueryModeType::TYPE_4 && !mIsUseTex)
            )
            return false;
        BOOST_ASSERT((pQueryCellIndexArr      != NULL) && "must be initialized first");
        BOOST_ASSERT((pQueryRadiusArr         != NULL) && "must be initialized first");
        BOOST_ASSERT((QueryCount != 0)                 && "invalid dimensions");
        BOOST_ASSERT((Query_k != 0)                    && "invalid dimensions");
        BOOST_ASSERT((pNeighborCellIDArr      != NULL) && "must be initialized first");
        BOOST_ASSERT((pNeighborCellIDCountArr != NULL) && "must be initialized first");
        #ifdef ENABLE_BLOCK_PADDING
            BOOST_ASSERT((Query_k <= 15) && "Query_k must be less than or equal to 15");
        #endif
        //=============================================================================
        #ifdef DEBUG
            std::cout << "QueryCellIndexArr=";
            zv::support::CudaHelper<KernelApi>::instance()->template dump<uint32>(*pQueryCellIndexArr);
            std::cout << "QueryRadiusArr=";
            zv::support::CudaHelper<KernelApi>::instance()->template dump<float>(*pQueryRadiusArr);
        #endif
        //=============================================================================
        uint32 QueryCellRadius  = 0;
        uint32 JobsPerQueryEdge = 0;
        uint32 JobsPerQuery     = 0;
        uint32 QueryPadded_k    = Query_k;
        #ifdef ENABLE_BLOCK_PADDING
            uint32 BlockPadding = 0;
            calcSearchParams(
                Query_k,
                &QueryCellRadius,
                &JobsPerQueryEdge,
                &JobsPerQuery,
                &BlockPadding
                );
            QueryPadded_k += BlockPadding;
        #else
            calcSearchParams(
                Query_k,
                &QueryCellRadius,
                &JobsPerQueryEdge,
                &JobsPerQuery
                );
        #endif
        if(QueryModeType::instance()->getType() == QueryModeType::TYPE_4 && mIsNeedCacheTex)
        {
            cacheTex();
            mIsNeedCacheTex = false;
        }
        {
            ArrayHandle<CudaDeviceArray> dh_State_CellIDArr(   *mpd_CellIDArr,   accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellPosArr(  *mpd_CellPosArr,  accessLocation::device, accessMode::readwrite);
            ArrayHandle<CudaDeviceArray> dh_State_CellFreeArr( *mpd_CellFreeArr, accessLocation::device, accessMode::readwrite);
            detail::KNNSearchKernel_Search(
                        QueryModeType::instance()->getType(),
                /*IN*/  pQueryCellIndexArr-> template data<uint32>(),
                /*IN*/  pQueryRadiusArr->    template data<float>(),
                        QueryCount,
                        Query_k,
                        QueryPadded_k,
                        QueryCellRadius,
                        JobsPerQueryEdge,
                        JobsPerQuery,
                /*OUT*/ pNeighborCellIDArr->       template data<uint32>(),
                /*OUT*/ pNeighborCellIDCountArr->  template data<uint32>(),
                /*IN*/  dh_State_CellIDArr        .template data<uint32>(),
                /*IN*/  dh_State_CellPosArr       .template data<float2>(),
                /*IN*/  dh_State_CellFreeArr      .template data<uint32>(),
                        mCellCount,
                        mColCount,
                        mRowCount
                );
            pNeighborCellIDArr      ->dirty();
            pNeighborCellIDCountArr ->dirty();
        }
        //=============================================================================
        #ifdef DEBUG
            std::cout << "State_CellIDArr=";
            zv::support::CudaHelper<KernelApi>::instance()->template dump<uint32>(*mpd_CellIDArr, mColCount);
            std::cout << "NeighborCellIDArr=";
            zv::support::CudaHelper<KernelApi>::instance()->template dump<uint32>(*pNeighborCellIDArr, QueryPadded_k);
            std::cout << "NeighborCellIDCountArr=";
            zv::support::CudaHelper<KernelApi>::instance()->template dump<uint32>(*pNeighborCellIDCountArr);
        #endif
        //=============================================================================
        return true;
    }
    //=============================================================================
private:
    const KernelApi             &mKernelApi;
    bool                         mIsInit;
    Scanner<KernelApi>          *mpScanner;
    uint32                       mColCount;
    uint32                       mRowCount;
    uint32                       mCellCount;
    CudaDeviceArray             *mpd_CellIDArr;
    CudaDeviceArray             *mpd_CellPosArr;
    CudaDeviceArray             *mpd_CellIndexPtrArr;
    CudaDeviceArray             *mpd_CellFreeArr;
    typename KernelApi::Texture *mpCellIDTex;
    typename KernelApi::Texture *mpCellPosTex;
    typename KernelApi::Texture *mpCellFreeTex;
    bool                         mIsUseTex;
    bool                         mIsNeedCacheTex;
};

} } } } /*zillians::vw::processors::cuda*/

#endif /*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_KNNSEARCH_H_*/
