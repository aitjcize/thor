/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_PRIMITIVES_JIT_JITCONTEXT_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_PRIMITIVES_JIT_JITCONTEXT_H_

#include "framework/processor/cuda/kernel/api/DriverApi.h"

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace primitives { namespace jit {

//=====================================
// JITContext_t
//=====================================

struct JITContext_t
{
	JITContext_t(const DriverApi& api);
	void compile(
			std::string program,
			std::string entry_point,
			DriverApi::jit_target::type sm_version);

	const DriverApi& mApi;
	DriverApi::ExecutionManagement::Module* mModule;
	DriverApi::ExecutionManagement::Module::Function* mFunction;
};

} } } } } }

#endif
