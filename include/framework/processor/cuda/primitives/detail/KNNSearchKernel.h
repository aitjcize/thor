/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_KNNSEARCHKERNEL_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_KNNSEARCHKERNEL_H_

#include "framework/processor/cuda/primitives/detail/CudaKernelCommon.h"

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail {

extern void KNNSearchKernel_Add(
    /*IN*/    uint32 *pAddCellIDArr,
    /*IN*/    float2 *pAddCellPosArr,
    /*IN*/    uint64 *pAddCellIndexPtrArr,
              uint32  AddCount,
    /*OUT*/   uint32 *pState_CellIDArr,
    /*OUT*/   float2 *pState_CellPosArr,
    /*INOUT*/ uint64 *pState_CellIndexPtrArr,
    /*INOUT*/ uint32 *pState_CellFreeArr,
              uint32  State_CellCount,
    /*IN*/    uint32 *pTemp_PrefixSumArr,
    /*INOUT*/ uint32 *pTemp_WriteIndexArr
    );
extern void KNNSearchKernel_Remove(
    /*IN*/    uint32 *pRemoveCellIndexArr,
              uint32  RemoveCount,
              bool    IsUpdateCellIndexPtr,
    /*OUT*/   uint32 *pState_CellIDArr,
    /*OUT*/   float2 *pState_CellPosArr,
    /*INOUT*/ uint64 *pState_CellIndexPtrArr,
    /*INOUT*/ uint32 *pState_CellFreeArr,
              uint32  State_CellCount
    );
extern void KNNSearchKernel_Update(
    /*IN*/  uint32 *pUpdateCellIndexArr,
    /*IN*/  float2 *pUpdateCellPosArr,
            uint32  UpdateCount,
    /*OUT*/ float2 *pState_CellPosArr,
            uint32  State_CellCount
    );
extern void KNNSearchKernel_Step(
              uint32  StepMode,
    /*INOUT*/ uint32 *pState_CellIDArr,
    /*INOUT*/ float2 *pState_CellPosArr,
    /*INOUT*/ uint64 *pState_CellIndexPtrArr,
    /*INOUT*/ uint32 *pState_CellFreeArr,
              uint32  State_ColCount,
              uint32  State_RowCount
    );
extern void KNNSearchKernel_Search(
            uint32  SearchMode,
    /*IN*/  uint32 *pQueryCellIndexArr,
    /*IN*/  float  *pQueryRadiusArr,
            uint32  QueryCount,
            uint32  Query_k,
            uint32  QueryPadded_k,
            uint32  QueryCellRadius,
            uint32  JobsPerQueryEdge,
            uint32  JobsPerQuery,
    /*OUT*/ uint32 *pNeighborCellIDArr,
    /*OUT*/ uint32 *pNeighborCellIDCountArr,
    /*IN*/  uint32 *pState_CellIDArr,
    /*IN*/  float2 *pState_CellPosArr,
    /*IN*/  uint32 *pState_CellFreeArr,
            uint32  State_CellCount,
            uint32  State_ColCount,
            uint32  State_RowCount
    );
extern void KNNSearchKernel_TextureTest(
    /*IN*/  uint32 *pInputArr,
    /*OUT*/ uint32 *pOutputArr,
            uint32  ColCount,
            uint32  RowCount
    );

} } } } } /*zillians::vw::processors::cuda::detail*/

#endif /* ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_KNNSEARCHKERNEL_H_ */
