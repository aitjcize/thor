/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_SEGMENTCOMPACTORKERNEL_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_SEGMENTCOMPACTORKERNEL_H_

#include "framework/processor/cuda/primitives/detail/CudaKernelCommon.h"

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail {

//=============================================================================
// Malloc Benchmark
//=============================================================================

__global__ void resetArrayValue(
    /*OUT*/ uint32* Array,
            uint32  Index,
            uint32  Count
	);

__global__ void addArrayValue(
    /*OUT*/ uint32* Array,
            uint32  Index,
            uint32  Value,
            uint32  Count
	);

//=====================================
// MemoryDefragmenterKernel_Defrag
//=====================================

extern void MemoryDefragmenterKernel_Defrag(
              uint32  DefragMode,
    /*IN*/    uint32* SegmentSize,
    /*IN*/    uint32* SegmentStartIndexInHeap,
    /*IN*/    uint64* SegmentHeap,
    /*INOUT*/ uint64* IndirectionTable,
    /*OUT*/   uint64* SegmentStrip,
              uint32  SegmentCount,
              uint32  SegmentStripSize,
              uint32  MaxSegmentSize,
              uint32  HeapSize
    );

} } } } } /*zillians::vw::processors::cuda::detail*/

#endif /* ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_SEGMENTCOMPACTORKERNEL_H_ */
