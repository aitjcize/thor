/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_QUEUEDUMPABLEEXTENDEDKERNEL_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_QUEUEDUMPABLEEXTENDEDKERNEL_H_

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail { namespace queue_dumpable_extended {

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
uint32 dump_used_data_impl(
		uint8* data,
		uint32 global_queue_size,
		uint32 local_queue_count,
		uint32 local_queue_size,
/*OUT*/ T* dump_queue_buffer);

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void check_queue_size(
		uint8* data,
		uint32 global_queue_size,
		uint32 local_queue_count,
		uint32 local_queue_size,
/*OUT*/ uint32* dump_flag,
/*OUT*/ uint32* dump_item_count,
/*OUT*/ uint32* global_mark_size);

void do_dump_compact(
		uint32 local_queue_count,
		uint32* scanned_flag,
		uint32* count_table,
/*OUT*/ uint32* scanned_key,
/*OUT*/ uint32* scanned_count);

void generate_dump_sequence_table(
		uint32 flag_count,
		uint32* compact_index,
		uint32* compact_count,
		uint32* scan_count,
/*OUT*/ uint32* queue_id_table,
/*OUT*/ uint32* queue_offset_table);

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void dump_to_queue(
		uint8* data,
		uint32 local_dump_count,
		uint32 global_mark_size,
		uint32 global_queue_size,
		uint32 local_queue_size,
		uint32* queue_id_table,
		uint32* queue_offset_table,
/*OUT*/	T* dump_queue_buffer);

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void update_global_local_queue_rptr_mark(
		uint8* data,
		uint32 queue_count,
		uint32 global_queue_size,
		uint32 local_queue_size,
		uint32* compacted_index,
		uint32* compacted_count);

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void do_dump_procedure(
		uint8* data,
		uint32 global_queue_size,
		uint32 local_queue_count,
		uint32 local_queue_size,
		uint32 flag_count,
		uint32 total_items,
		uint32 global_mark_size,
		uint32* scanned_flag,
		uint32* item_count,
/*OUT*/	T* dump_queue_buffer);

} } } } } }

#endif /* ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_QUEUEDUMPABLEEXTENDEDKERNEL_H_ */
