/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_QUEUEEXTENDEDKERNEL_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_QUEUEEXTENDEDKERNEL_H_

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail { namespace queue_extended {

/// DYNAMIC ALLOCATIOR

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void reset_full_impl(
		uint8* data,
		uint32 global_queue_size,
		uint32 local_queue_count,
		uint32 local_queue_size);

//////////////////////////////////////////////////////////////////////////
/// STATIC ALLOCATIOR

//template<typename BaseSchema, typename T, std::size_t GlobalQueueSize, std::size_t LocalQueueCount, std::size_t LocalQueueSize, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
//void initialize_impl(uint8* data);

template<typename BaseSchema, typename T, std::size_t GlobalQueueSize, std::size_t LocalQueueCount, std::size_t LocalQueueSize, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void reset_full_impl(uint8* data);

} } } } } }

#endif /* ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_QUEUEEXTENDEDKERNEL_H_ */
