/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/processor/cuda/primitives/detail/QueueDumpableExtendedKernel.h"
#include "vw/buffers/BufferManagerWrapper.h"
#include "vw/buffers/support/BufferStructuralPattern.h"

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail { namespace queue_dumpable_extended {

/**
 * check current local queue size, and mark their dump flag.
 *
 * @param data Buffer to raw buffer
 * @param local_queue_count The number of local queue
 * @param local_queue_size The size of local queue
 * @param dump_flag The flag to indicate the local queue that needs to dump from rptr_mark position
 * @param dump_item_count The count to indicate the number needs to dump from local queue
 * @param global_mark_size The rptr_mark size of global queue
 *
 * for example, this could be output
 * @code
 * dump_flag[5]       = {0, 1, 1, 0, 1} // mark 1 if count != 0
 * dump_item_count[5] = {0, 4, 2, 0, 2} // if count != 0, then flag should be mark as 1, else 0
 * @endcode
 *
 */
template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
__decl_global void check_queue_size_impl(
		uint8* data,
		uint32 global_queue_size,
		uint32 local_queue_count,
		uint32 local_queue_size,
/*OUT*/ uint32* dump_flag,
/*OUT*/ uint32* dump_item_count,
/*OUT*/ uint32* global_used_size)
{
	uint32 thread_id = blockDim.x * blockIdx.x + threadIdx.x;

	if(thread_id < local_queue_count)
	{
		typename threaded_dynamic_dumpable_extended_allocator_fermi_t<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>::local_queue_t lq(
				buffer_t<typename BaseSchema::root_block_type>::template accessor<typename BaseSchema::_::local_queue>::get(data, /*BaseSchema*/ SAIDX(0, /*local_queue*/ VARIDX())), local_queue_size, thread_id);

		/// queue free size means data was poped from queue and shall be used later, so it also means used size
		uint32 used_size = lq.unsafe_free_size();

		dump_item_count[thread_id] = used_size;
		dump_flag[thread_id] = (used_size) ? 1 : 0;

		if(thread_id == 0)
		{
			typename threaded_dynamic_dumpable_extended_allocator_fermi_t<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>::global_queue_t gq(
					buffer_t<typename BaseSchema::root_block_type>::template accessor<typename BaseSchema::_::global_queue>::get(data, /*BaseSchema*/ SAIDX(0, /*global_queue*/ VARIDX())), global_queue_size, 0);

			/// queue free size means data was poped from queue and shall be used later, so it also means used size
			*global_used_size = gq.unsafe_free_size();
		}
	}
}

/**
 * compact the dump index table and its count table
 *
 * @param local_queue_count The number of local queue
 * @param scanned_flag The scanned flag
 * @param count_table The count table
 * @param scanned_key The key index table for output
 * @param scanned_count The item table for output
 *
 * for example, this might be output:
 *
 * @code
 * input:
 *           index:  0  1  2  3  4  5
 *   flag_table[] = {1, 0, 1, 0, 1, 1}
 * scanned_flag[] = {0, 1, 1, 2, 2, 3}
 * count_table[]  = {2, 0, 3, 0, 1, 1}
 *
 * output:
 * scanned_key[]   = {0, 2, 4, 5}
 * scanned_count[] = {2, 3, 1, 1}
 * @endcode
 */
__decl_global void do_dump_compact_impl(
		uint32 local_queue_count,
		uint32* scanned_flag,
		uint32* count_table,
		/*OUT*/ uint32* scanned_key,
		/*OUT*/ uint32* scanned_count)
{
	uint32 thread_id = blockDim.x * blockIdx.x + threadIdx.x;

	if(thread_id < local_queue_count)
	{
		uint32 c = count_table[thread_id];
		if(c > 0)
		{
			uint32 offset = scanned_flag[thread_id];
			scanned_key[offset] = thread_id;
			scanned_count[offset] = c;
		}
	}
}

/**
 * Generate the queue index info and count info for number of thread
 *
 * @param flag_count The number of local queue that needs balance
 * @param compact_index Compacted index for local queue
 * @param compact_count Compacted count for local count
 * @param scan_count The exclusive scan count for compact_count
 * @param queue_id_table Local queue index for each of thread
 * @param queue_offset_table The offset that thread should can get or set
 *
 * for example:
 *
 * @code
 * input:
 *
 * compact_index[] = {0, 2, 4, 5}
 * compact_count[] = {2, 3, 1, 1}
 * scan_count[]    = {0, 2, 5, 6}
 *
 * output:
 *                  index: 0, 1, 2, 3, 4, 5, 6
 * queue_id_table[]     = {0, 0, 2, 2, 2, 4, 5}
 * queue_offset_table[] = {0, 1, 0, 1, 2, 0, 0}
 * @endcode
 */
__decl_global void generate_dump_sequence_table_impl(
		uint32 flag_count,
		uint32* compact_index,
		uint32* compact_count,
		uint32* scan_count,
		/*OUT*/ uint32* queue_id_table,
		/*OUT*/ uint32* queue_offset_table)
{
	uint32 thread_id = blockDim.x * blockIdx.x + threadIdx.x;

	if(thread_id < flag_count)
	{
		uint32 sub_index = 0;
		uint32 offset = 0;

		for(uint32 i = 0; i < compact_count[thread_id]; ++i)
		{
			offset = scan_count[thread_id] + sub_index;
			queue_id_table[offset] = compact_index[thread_id];
			queue_offset_table[offset] = sub_index;
			sub_index++;
		}
	}
}

/**
 * Dump global/local queue items to destination buffer
 *
 * @param data Buffer to raw buffer
 * @param local_dump_count Total size that needs to dump from local queue
 * @param global_mark_size Rptr mark size in global queue
 * @param global_queue_size Global queue size
 * @param local_queue_size Local queue size
 * @param queue_id_table Local queue index for each of thread
 * @param queue_offset_table The offset that thread should can get or set
 * @param dump_queue_buffer Destination buffer for dump
 */
template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
__decl_global void dump_to_queue_impl(
		uint8* data,
		uint32 local_dump_count,
		uint32 global_mark_size,
		uint32 global_queue_size,
		uint32 local_queue_size,
		uint32* queue_id_table,
		uint32* queue_offset_table,
		T* dump_queue_buffer)
{
	uint32 thread_id = blockDim.x * blockIdx.x + threadIdx.x;
	uint32 thread_count = local_dump_count + global_mark_size;

	if(thread_id < local_dump_count)
	{
		uint32 lq_thread_id = queue_id_table[thread_id];
		typename threaded_dynamic_dumpable_extended_allocator_fermi_t<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>::local_queue_t lq(
				buffer_t<typename BaseSchema::root_block_type>::template accessor<typename BaseSchema::_::local_queue>::get(data, /*BaseSchema*/ SAIDX(0, /*local_queue*/ VARIDX())), local_queue_size, lq_thread_id);

		uint64 lq_offset = queue_offset_table[thread_id];
		const T& value = lq.object_accessor.get(lq_offset);

		dump_queue_buffer[thread_id] = value;
	}
	else if(thread_id >= local_dump_count && thread_id < thread_count)
	{
		typename threaded_dynamic_dumpable_extended_allocator_fermi_t<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>::global_queue_t gq(
				buffer_t<typename BaseSchema::root_block_type>::template accessor<typename BaseSchema::_::global_queue>::get(data, /*BaseSchema*/ SAIDX(0, /*global_queue*/ VARIDX())), global_queue_size, 0);

		uint32 offset = local_dump_count;
		uint32 rptr_offset = thread_id - local_dump_count;

		const T& value = gq.object_accessor.get(rptr_offset);
		dump_queue_buffer[offset + rptr_offset] = value;
	}
}

///**
// * Update local queues rptr_mark
// *
// * @param data Buffer pointer
// * @param queue_count The number of thread
// * @param global_queue_size Global queue size
// * @param local_queue_size Local queue size
// * @param compacted_index The compacted index of local queue
// * @param compacted_count The compacted count of local queue
// */
//template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
//__decl_global void update_global_local_queue_rptr_mark_impl(
//		uint8* data,
//		uint32 queue_count,
//		uint32 global_queue_size,
//		uint32 local_queue_size,
//		uint32* compacted_index,
//		uint32* compacted_count)
//{
//	uint32 thread_id = blockDim.x * blockIdx.x + threadIdx.x;
//
//	if(thread_id < queue_count)
//	{
//		uint32 lq_thread_id = compacted_index[thread_id];
//		typename threaded_dynamic_dumpable_allocator_fermi_t<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>::local_queue_t lq(
//				buffer_t<typename BaseSchema::root_block_type>::template accessor<typename BaseSchema::_::local_queue>::get(data, /*BaseSchema*/ SAIDX(0, /*local_queue*/ VARIDX())), local_queue_size, lq_thread_id);
//
//		/// update local queue's rptr_mark
//		lq.mark();
//
//		if(thread_id == 0)
//		{
//			/// update global queue's rptr_mark
//			typename threaded_dynamic_dumpable_allocator_fermi_t<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>::global_queue_t gq(
//					buffer_t<typename BaseSchema::root_block_type>::template accessor<typename BaseSchema::_::global_queue>::get(data, /*BaseSchema*/ SAIDX(0, /*global_queue*/ VARIDX())), global_queue_size, 0);
//
//			gq.mark();
//		}
//	}
//}

//////////////////////////////////////////////////////////////////////////

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void check_queue_size(
		uint8* data,
		uint32 global_queue_size,
		uint32 local_queue_count,
		uint32 local_queue_size,
/*OUT*/ uint32* dump_flag,
/*OUT*/ uint32* dump_item_count,
/*OUT*/ uint32* global_used_size)
{
	uint32 threads = 32;
	uint32 blocks = local_queue_count / threads + ((local_queue_count % threads == 0) ? 0 : 1);

	check_queue_size_impl<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity> <<<blocks, threads>>>(
			data,
			global_queue_size,
			local_queue_count,
			local_queue_size,
			dump_flag,
			dump_item_count,
			global_used_size);
}

void do_dump_compact(
		uint32 local_queue_count,
		uint32* scanned_flag,
		uint32* count_table,
/*OUT*/ uint32* scanned_key,
/*OUT*/ uint32* scanned_count)
{
	uint32 threads = 32;
	uint32 blocks = local_queue_count / threads + ((local_queue_count % threads == 0) ? 0 : 1);

	do_dump_compact_impl<<<blocks, threads>>>(
			local_queue_count,
			scanned_flag,
			count_table,
			scanned_key,
			scanned_count);
}

void generate_dump_sequence_table(
		uint32 flag_count,
		uint32* compact_index,
		uint32* compact_count,
		uint32* scan_count,
/*OUT*/ uint32* queue_id_table,
/*OUT*/ uint32* queue_offset_table)
{
	uint32 threads = 32;
	uint32 blocks = flag_count / threads + ((flag_count % threads == 0) ? 0 : 1);

	generate_dump_sequence_table_impl<<<blocks, threads>>>(
			flag_count,
			compact_index,
			compact_count,
			scan_count,
			queue_id_table,
			queue_offset_table);
}

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void dump_to_queue(
		uint8* data,
		uint32 local_dump_count,
		uint32 global_mark_size,
		uint32 global_queue_size,
		uint32 local_queue_size,
		uint32* queue_id_table,
		uint32* queue_offset_table,
		T* dump_queue_buffer)
{
	uint32 threads = 32;
	uint32 thread_count = local_dump_count + global_mark_size;
	uint32 blocks = thread_count / threads + ((thread_count % threads == 0) ? 0 : 1);

	dump_to_queue_impl<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity> <<<blocks, threads>>>(
			data,
			local_dump_count,
			global_mark_size,
			global_queue_size,
			local_queue_size,
			queue_id_table,
			queue_offset_table,
			dump_queue_buffer);
}

//template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
//void update_global_local_queue_rptr_mark(
//		uint8* data,
//		uint32 queue_count,
//		uint32 global_queue_size,
//		uint32 local_queue_size,
//		uint32* compacted_index,
//		uint32* compacted_count)
//{
//	uint32 threads = 32;
//	uint32 blocks = queue_count / threads + ((queue_count % threads == 0) ? 0 : 1);
//
//	update_global_local_queue_rptr_mark_impl<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity> <<<blocks, threads>>>(
//			data,
//			queue_count,
//			global_queue_size,
//			local_queue_size,
//			compacted_index,
//			compacted_count);
//}

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void do_dump_procedure(
		uint8* data,
		uint32 global_queue_size,
		uint32 local_queue_count,
		uint32 local_queue_size,
		uint32 flag_count,
		uint32 total_local_mark_items,
		uint32 global_used_size,
		uint32* scanned_flag,
		uint32* item_count,
		T* dump_queue_buffer)
{
	static uint32* compacted_index = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count);
	static uint32* compacted_count = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count);

	static uint32* scanned_count = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count);

	static uint32* queue_id_table     = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count * local_queue_size);
	static uint32* queue_offset_table = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count * local_queue_size);

	do_dump_compact(local_queue_count, scanned_flag, item_count, compacted_index, compacted_count);

	thrust::device_ptr<uint32> dp_compacted_count(compacted_count);
	thrust::device_ptr<uint32> dp_scanned_count(scanned_count);

	thrust::copy(dp_compacted_count, dp_compacted_count + local_queue_count, dp_scanned_count);
	thrust::exclusive_scan(dp_scanned_count, dp_scanned_count + local_queue_count, dp_scanned_count, 0);

	/// generate sequence table from local queue here
	generate_dump_sequence_table(flag_count, compacted_index, compacted_count, scanned_count, queue_id_table, queue_offset_table);

	/// dump to queue here
	dump_to_queue<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>(
			data,
			total_local_mark_items,
			global_used_size,
			global_queue_size,
			local_queue_size,
			queue_id_table,
			queue_offset_table,
			dump_queue_buffer);

	/// update local queues' rptr
//	update_global_local_queue_rptr<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>(
//			data,
//			flag_count,
//			global_queue_size,
//			local_queue_size,
//			compacted_index,
//			compacted_count);
}

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
uint32 dump_used_data_impl(
		uint8* data,
		uint32 global_queue_size,
		uint32 local_queue_count,
		uint32 local_queue_size,
/*OUT*/ T* dump_queue_buffer)
{
	static uint32* dump_flag = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count);
	static uint32* dump_item_count = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count);
	static uint32* global_used_size = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32));

	thrust::device_ptr<uint32> dp_dump_flag(dump_flag);
	thrust::device_ptr<uint32> dp_dump_item_count(dump_item_count);
	thrust::device_ptr<uint32> dp_global_used_size(global_used_size);

	thrust::fill(dp_dump_flag, dp_dump_flag + local_queue_count, 0);
	thrust::fill(dp_dump_item_count, dp_dump_item_count + local_queue_count, 0);

	check_queue_size<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>(
			data,
			global_queue_size,
			local_queue_count,
			local_queue_size,
			dump_flag,
			dump_item_count,
			global_used_size);

	uint32 dump_flag_count = thrust::reduce(dp_dump_flag, dp_dump_flag + local_queue_count);

	if(dump_flag_count > 0)
	{
		thrust::exclusive_scan(dp_dump_flag, dp_dump_flag + local_queue_count, dp_dump_flag, 0);
		uint32 total_dump_count = thrust::reduce(dp_dump_item_count, dp_dump_item_count + local_queue_count);
		uint32 total_global_used_size = thrust::reduce(dp_global_used_size, dp_global_used_size + 1); /// get global mark size from gpu to cpu stack

		do_dump_procedure<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>(
				data,
				global_queue_size,
				local_queue_count,
				local_queue_size,
				dump_flag_count,
				total_dump_count,
				total_global_used_size,
				dump_flag,
				dump_item_count,
				dump_queue_buffer);

		return total_dump_count + total_global_used_size;
	}
	else
		return 0;
}

} } } } } }
