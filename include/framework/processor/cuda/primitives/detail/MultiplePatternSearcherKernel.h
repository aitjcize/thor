/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MULTIPLEPATTERNSEARCHERKERNEL_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MULTIPLEPATTERNSEARCHERKERNEL_H_

#include "framework/processor/cuda/primitives/HashMapApi.h"

#define ROUND_UP_TO_MULTIPLE(x,y)			(((x)/(y) + (((x)%(y)==0) ? 0 : 1))*(y))
#define BLOCK_STRIPE						8
#define BLOCK_PER_THREAD					16
#define ENABLE_SHARED_MEMORY_CONVERSION		1

//namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail {
extern "C"
{

#if BLOCK_STRIPE == 4

extern bool MultiplePatternSearchKernel_RabinKarp(
		HASHMAPAPI_DECLARE_PARAMETERS,
		zillians::uint32* textData, zillians::uint32 textLength, zillians::uint32 patternLength,
		zillians::uint32 hashComponent, zillians::uint32 hashComponentMax,
		zillians::uint32 matchedCountMax, zillians::uint32* matchedCount, zillians::uint32* matchedTextPositions, zillians::uint32* matchedPatternPosition);

#elif BLOCK_STRIPE == 8

extern bool MultiplePatternSearchKernel_RabinKarp(
		HASHMAPAPI_DECLARE_PARAMETERS,
		zillians::uint64* textData, zillians::uint32 textLength, zillians::uint32 patternLength,
		zillians::uint32 hashComponent, zillians::uint32 hashComponentMax,
		zillians::uint32 matchedCountMax, zillians::uint32* matchedCount, zillians::uint32* matchedTextPositions, zillians::uint32* matchedPatternPosition);

#endif

//} } } } } /*zillians::vw::processors::cuda::detail*/
}

#endif/*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MULTIPLEPATTERNSEARCHERKERNEL_H_*/
