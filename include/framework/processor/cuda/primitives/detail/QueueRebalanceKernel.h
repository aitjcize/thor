/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_QUEUEBALANCEKERNEL_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_QUEUEBALANCEKERNEL_H_

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail { namespace queue_rebalance {

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void check_queue_size(
		uint8* data,
		uint32 local_queue_count,
		uint32 local_queue_size,
		/*OUT*/ uint32* add_flag,
		/*OUT*/ uint32* add_item_count,
		/*OUT*/ uint32* remove_flag,
		/*OUT*/ uint32* remove_item_count);

void do_add_remove_simple_compact(
		uint32 local_queue_count,
		uint32* scanned_flag,
		uint32* count_table,
		/*OUT*/ uint32* scanned_key,
		/*OUT*/ uint32* scanned_count);

void generate_sequence_table(
		uint32 flag_count,
		uint32* compact_index,
		uint32* compact_count,
		uint32* scan_count,
		/*OUT*/ uint32* queue_id_table,
		/*OUT*/ uint32* queue_offset_table);

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void balance_global_local_queue(
		uint8* data,
		uint32 thread_count,
		bool push_to_local,
		uint32 global_queue_size,
		uint32 local_queue_size,
		uint32* queue_id_table,
		uint32* queue_offset_table);

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void update_global_local_queue_ptr(
		uint8* data,
		uint32 queue_count,
		uint32 total_items,
		bool operation,
		uint32 global_queue_size,
		uint32 local_queue_size,
		uint32* compacted_index,
		uint32* compacted_count);

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void rebalance_impl(
		uint8* data,
		uint32 global_queue_size,
		uint32 local_queue_count,
		uint32 local_queue_size);

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void do_rebalance_procedure(
		uint8* data,
		uint32 global_queue_size,
		uint32 local_queue_count,
		uint32 local_queue_size,
		bool operation,
		uint32 flag_count,
		uint32 total_items,
		uint32* scanned_flag,
		uint32* item_count);

} } } } } }

#endif /* ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_QUEUEBALANCEKERNEL_H_ */
