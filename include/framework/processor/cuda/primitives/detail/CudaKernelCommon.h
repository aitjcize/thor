/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_DETAIL_CUDAKERNELCOMMON_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_DETAIL_CUDAKERNELCOMMON_H_

#include "core/Types.h"
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <driver_types.h>
#include <vector_types.h>

// this is just a trick used to prevent syntax eclipse highlighting error...
#ifndef __CUDACC__

#ifndef __host__
#define __host__
#endif

#ifndef __device__
#define __device__
#endif

#ifndef __global__
#define __global__
#endif

#ifndef __noinline__
#define __noinline__
#endif

#ifndef __constant__
#define __constant__
#endif
#endif

#endif /* ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_DETAIL_CUDAKERNELCOMMON_H_ */
