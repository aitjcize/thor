/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/processor/cuda/primitives/detail/QueueRebalanceKernel.h"
#include "vw/buffers/BufferManagerWrapper.h"
#include "vw/buffers/support/BufferStructuralPattern.h"

using namespace zillians::vw::buffers;

namespace zillians { namespace framework { namespace processor { namespace cuda { namespace detail { namespace queue_rebalance {

/**
 * check current local queue size, and mark their add/remove flag.
 *
 * @param data Buffer to raw buffer
 * @param local_queue_count The number of local queue
 * @param local_queue_size The size of local queue
 * @param add_flag The flag to indicate the local queue that needs push
 * @param add_item_count The count to indicate the number needs to push to local queue
 * @param remove_flag The flag to indicate the local queue that needs pop
 * @param remove_item_count The count to indicate the number needs to pop to local queue
 *
 * for example, this could be output
 * @code
 * add_flag[5]       = {0, 1, 1, 0, 1} // mark 1 if count != 0
 * add_item_count[5] = {0, 4, 2, 0, 2} // if count != 0, then flag should be mark as 1, else 0
 * @endcode
 *
 */
template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
__decl_global void check_queue_size_impl(
		uint8* data,
		uint32 local_queue_count,
		uint32 local_queue_size,
		/*OUT*/ uint32* add_flag,
		/*OUT*/ uint32* add_item_count,
		/*OUT*/ uint32* remove_flag,
		/*OUT*/ uint32* remove_item_count)
{
	uint32 thread_id = blockDim.x * blockIdx.x + threadIdx.x;

	if(thread_id < local_queue_count)
	{
		typename threaded_dynamic_allocator_fermi_t<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>::local_queue_t lq(
				buffer_t<typename BaseSchema::root_block_type>::template accessor<typename BaseSchema::_::local_queue>::get(data, /*BaseSchema*/ SAIDX(0, /*local_queue*/ VARIDX())), local_queue_size, thread_id);
		uint32 data_size = lq.unsafe_data_size();
		if(data_size > local_queue_size * 2 / 3)
		{
			/// too many items in queue, count them and store to remove_items
			remove_item_count[thread_id] = data_size - (local_queue_size / 2);
			remove_flag[thread_id] = 1;
		}
		else if(data_size < local_queue_size / 3)
		{
			add_item_count[thread_id] = (local_queue_size/ 2 ) - data_size;
			add_flag[thread_id] = 1;
		}
	}
}

/**
 * compact the add/remove index table and its count table
 *
 * @param local_queue_count The number of local queue
 * @param scanned_flag The scanned flag
 * @param count_table The count table
 * @param scanned_key The key index table for output
 * @param scanned_count The item table for output
 *
 * for example, this might be output:
 *
 * @code
 * input:
 *           index:  0  1  2  3  4  5
 *   flag_table[] = {1, 0, 1, 0, 1, 1}
 * scanned_flag[] = {0, 1, 1, 2, 2, 3}
 * count_table[]  = {2, 0, 3, 0, 1, 1}
 *
 * output:
 * scanned_key[]   = {0, 2, 4, 5}
 * scanned_count[] = {2, 3, 1, 1}
 * @endcode
 */
template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
__decl_global void do_add_remove_simple_compact_impl(
		uint32 local_queue_count,
		uint32* scanned_flag,
		uint32* count_table,
		/*OUT*/ uint32* scanned_key,
		/*OUT*/ uint32* scanned_count)
{
	uint32 thread_id = blockDim.x * blockIdx.x + threadIdx.x;

	if(thread_id < local_queue_count)
	{
		uint32 c = count_table[thread_id];
		if(c > 0)
		{
			uint32 offset = scanned_flag[thread_id];
			scanned_key[offset] = thread_id;
			scanned_count[offset] = c;
		}
	}
}

/**
 * Generate the queue index info and count info for number of thread
 *
 * @param flag_count The number of local queue that needs balance
 * @param compact_index Compacted index for local queue
 * @param compact_count Compacted count for local count
 * @param scan_count The exclusive scan count for compact_count
 * @param queue_id_table Local queue index for each of thread
 * @param queue_offset_table The offset that thread should can get or set
 *
 * for example:
 *
 * @code
 * input:
 *
 * compact_index[] = {0, 2, 4, 5}
 * compact_count[] = {2, 3, 1, 1}
 * scan_count[]    = {0, 2, 5, 6}
 *
 * output:
 *                  index: 0, 1, 2, 3, 4, 5, 6
 * queue_id_table[]     = {0, 0, 2, 2, 2, 4, 5}
 * queue_offset_table[] = {0, 1, 0, 1, 2, 0, 0}
 * @endcode
 */
template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
__decl_global void generate_sequence_table_impl(
		uint32 flag_count,
		uint32* compact_index,
		uint32* compact_count,
		uint32* scan_count,
		/*OUT*/ uint32* queue_id_table,
		/*OUT*/ uint32* queue_offset_table)
{
	uint32 thread_id = blockDim.x * blockIdx.x + threadIdx.x;

	if(thread_id < flag_count)
	{
		uint32 sub_index = 0;
		uint32 offset = 0;

		for(uint32 i = 0; i < compact_count[thread_id]; ++i)
		{
			offset = scan_count[thread_id] + sub_index;
			queue_id_table[offset] = compact_index[thread_id];
			queue_offset_table[offset] = sub_index;
			sub_index++;
		}
	}
}

/**
 * Balance global queue and local queue
 *
 * @param data Buffer pointer
 * @param thread_count The number of thread
 * @param push_to_local True if pop from global and push back to local, vice versa
 * @param global_queue_size Global queue size
 * @param local_queue_size Local queue size
 * @param queue_id_table Local queue id for each thread
 * @param queue_offset_table Local queue offset for thread to get or push
 */
template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
__decl_global void balance_global_local_queue_impl(
		uint8* data,
		uint32 thread_count,
		bool push_to_local,
		uint32 global_queue_size,
		uint32 local_queue_size,
		uint32* queue_id_table,
		uint32* queue_offset_table)
{
	uint32 thread_id = blockDim.x * blockIdx.x + threadIdx.x;

	if(thread_id < thread_count)
	{
		typename threaded_dynamic_allocator_fermi_t<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>::global_queue_t gq(
				buffer_t<typename BaseSchema::root_block_type>::template accessor<typename BaseSchema::_::global_queue>::get(data, /*BaseSchema*/ SAIDX(0, /*global_queue*/ VARIDX())), global_queue_size, 0);

		uint32 lq_thread_id = queue_id_table[thread_id];
		typename threaded_dynamic_allocator_fermi_t<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>::local_queue_t lq(
				buffer_t<typename BaseSchema::root_block_type>::template accessor<typename BaseSchema::_::local_queue>::get(data, /*BaseSchema*/ SAIDX(0, /*local_queue*/ VARIDX())), local_queue_size, lq_thread_id);

		if(push_to_local)
		{
			/// get from global queue, then set to local queue
			uint64 gq_rptr = gq.rptr_accessor.get();
			uint64 gq_offset = gq_rptr + thread_id;
			const T& value = gq.object_accessor.get(gq_offset);
			gq.sequence_accessor.set(gq_offset, gq_offset + gq.buffer_mask + 1);

			uint64 lq_wptr = lq.wptr_accessor.get();
			uint64 lq_offset = lq_wptr + queue_offset_table[thread_id];

			lq.object_accessor.set(lq_offset, value);
//			lq.sequence_accessor.set(lq_offset, lq_offset + 1);
		}
		else
		{
			/// get from local queue, then set to global queue
			uint64 lq_rptr = lq.rptr_accessor.get();
			uint64 lq_offset = lq_rptr + queue_offset_table[thread_id];
			const T& value = lq.object_accessor.get(lq_offset);
//			lq.sequence_accessor.set(lq_offset, lq_offset + lq.buffer_mask + 1);

			uint64 gq_wptr = gq.wptr_accessor.get();
			uint64 gq_offset = gq_wptr + thread_id;
			gq.object_accessor.set(gq_offset, value);
			gq.sequence_accessor.set(gq_offset, gq_offset + 1);
		}
	}
}

/**
 * Update local queues rptr(or wptr)
 *
 * @param data Buffer pointer
 * @param queue_count The number of thread
 * @param local_queue_size Local queue size
 * @param operation True if pop from global and push back to local, vice versa
 * @param compacted_index The compacted index of local queue
 * @param compacted_count The compacted count of local queue
 */
template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
__decl_global void update_global_local_queue_ptr_impl(
		uint8* data,
		uint32 queue_count,
		uint32 total_items,
		bool operation,
		uint32 global_queue_size,
		uint32 local_queue_size,
		uint32* compacted_index,
		uint32* compacted_count)
{
	uint32 thread_id = blockDim.x * blockIdx.x + threadIdx.x;

	if(thread_id < queue_count)
	{
		uint32 lq_thread_id = compacted_index[thread_id];
		uint32 lq_count = compacted_count[thread_id];
		typename threaded_dynamic_allocator_fermi_t<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>::local_queue_t lq(
				buffer_t<typename BaseSchema::root_block_type>::template accessor<typename BaseSchema::_::local_queue>::get(data, /*BaseSchema*/ SAIDX(0, /*local_queue*/ VARIDX())), local_queue_size, lq_thread_id);

		if(operation)
		{
			/// set(push) to local queue, so we should update local queue's wptr
			lq.wptr_accessor.set(lq.wptr_accessor.get() + lq_count);

			if(thread_id == 0)
			{
				/// get(pop) from global queue, so we should update global queue's rptr
				typename threaded_dynamic_allocator_fermi_t<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>::global_queue_t gq(
						buffer_t<typename BaseSchema::root_block_type>::template accessor<typename BaseSchema::_::global_queue>::get(data, /*BaseSchema*/ SAIDX(0, /*global_queue*/ VARIDX())), global_queue_size, 0);
				gq.rptr_accessor.set(gq.rptr_accessor.get() + total_items);
			}
		}
		else
		{
			/// get(pop) from local queue, so we should update local queue's rptr
			lq.rptr_accessor.set(lq.rptr_accessor.get() + lq_count);

			if(thread_id == 0)
			{
				/// set(push) to global queue, so we should update global queue's wptr
				typename threaded_dynamic_allocator_fermi_t<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>::global_queue_t gq(
						buffer_t<typename BaseSchema::root_block_type>::template accessor<typename BaseSchema::_::global_queue>::get(data, /*BaseSchema*/ SAIDX(0, /*global_queue*/ VARIDX())), global_queue_size, 0);
				gq.wptr_accessor.set(gq.wptr_accessor.get() + total_items);
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void check_queue_size(
		uint8* data,
		uint32 local_queue_count,
		uint32 local_queue_size,
		/*OUT*/ uint32* add_flag,
		/*OUT*/ uint32* add_item_count,
		/*OUT*/ uint32* remove_flag,
		/*OUT*/ uint32* remove_item_count)
{
	uint32 threads = 32;
	uint32 blocks = local_queue_count / threads + ((local_queue_count % threads == 0) ? 0 : 1);

	check_queue_size_impl<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity> <<<blocks, threads>>>(
			data,
			local_queue_count,
			local_queue_size,
			add_flag,
			add_item_count,
			remove_flag,
			remove_item_count);
}

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void do_add_remove_simple_compact(
		uint32 local_queue_count,
		uint32* scanned_flag,
		uint32* count_table,
		/*OUT*/ uint32* scanned_key,
		/*OUT*/ uint32* scanned_count)
{
	uint32 threads = 32;
	uint32 blocks = local_queue_count / threads + ((local_queue_count % threads == 0) ? 0 : 1);

	do_add_remove_simple_compact_impl<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity> <<<blocks, threads>>>(
			local_queue_count,
			scanned_flag,
			count_table,
			scanned_key,
			scanned_count);
}

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void generate_sequence_table(
		uint32 flag_count,
		uint32* compact_index,
		uint32* compact_count,
		uint32* scan_count,
		/*OUT*/ uint32* queue_id_table,
		/*OUT*/ uint32* queue_offset_table)
{
	uint32 threads = 32;
	uint32 blocks = flag_count / threads + ((flag_count % threads == 0) ? 0 : 1);

	generate_sequence_table_impl<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity> <<<blocks, threads>>>(
			flag_count,
			compact_index,
			compact_count,
			scan_count,
			queue_id_table,
			queue_offset_table);
}

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void balance_global_local_queue(
		uint8* data,
		uint32 thread_count,
		bool push_to_local,
		uint32 global_queue_size,
		uint32 local_queue_size,
		uint32* queue_id_table,
		uint32* queue_offset_table)
{
	uint32 threads = 32;
	uint32 blocks = thread_count / threads + ((thread_count % threads == 0) ? 0 : 1);

	balance_global_local_queue_impl<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity> <<<blocks, threads>>>(
			data,
			thread_count,
			push_to_local,
			global_queue_size,
			local_queue_size,
			queue_id_table,
			queue_offset_table);
}

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void update_global_local_queue_ptr(
		uint8* data,
		uint32 queue_count,
		uint32 total_items,
		bool operation,
		uint32 global_queue_size,
		uint32 local_queue_size,
		uint32* compacted_index,
		uint32* compacted_count)
{
	uint32 threads = 32;
	uint32 blocks = queue_count / threads + ((queue_count % threads == 0) ? 0 : 1);

	update_global_local_queue_ptr_impl<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity> <<<blocks, threads>>>(
			data,
			queue_count,
			total_items,
			operation,
			global_queue_size,
			local_queue_size,
			compacted_index,
			compacted_count);
}

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
__decl_noinline void rebalance_impl(
		uint8* data,
		uint32 global_queue_size,
		uint32 local_queue_count,
		uint32 local_queue_size)
{
	static uint32* add_flag    = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count);
	static uint32* remove_flag = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count);

	static uint32* add_item_count    = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count);	// how many items need to be pushed to local queue
	static uint32* remove_item_count = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count);	// how many items need to be popped from local queue

	thrust::device_ptr<uint32> dp_add_flag(add_flag);
	thrust::device_ptr<uint32> dp_remove_flag(remove_flag);
	thrust::device_ptr<uint32> dp_add_item_count(add_item_count);
	thrust::device_ptr<uint32> dp_remove_item_count(remove_item_count);
	thrust::fill(dp_add_flag, dp_add_flag + local_queue_count, 0);
	thrust::fill(dp_remove_flag, dp_remove_flag + local_queue_count, 0);
	thrust::fill(dp_add_item_count, dp_add_item_count + local_queue_count, 0);
	thrust::fill(dp_remove_item_count, dp_remove_item_count + local_queue_count, 0);

	check_queue_size<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>(
			data,
			local_queue_count,
			local_queue_size,
			add_flag,
			add_item_count,
			remove_flag,
			remove_item_count);

	uint32 add_flag_count    = thrust::reduce(dp_add_flag, dp_add_flag + local_queue_count);
	uint32 remove_flag_count = thrust::reduce(dp_remove_flag, dp_remove_flag + local_queue_count);

	if(add_flag_count > 0)
	{
		bool add_operation = true;

		thrust::exclusive_scan(dp_add_flag, dp_add_flag + local_queue_count, dp_add_flag, 0);
		uint32 total_add_count = thrust::reduce(dp_add_item_count, dp_add_item_count + local_queue_count);

		do_rebalance_procedure<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>(
				data,
				global_queue_size,
				local_queue_count,
				local_queue_size,
				add_operation,
				add_flag_count,
				total_add_count,
				add_flag,
				add_item_count);
	}

	if(remove_flag_count > 0)
	{
		bool add_operation = false;

		thrust::exclusive_scan(dp_remove_flag, dp_remove_flag + local_queue_count, dp_remove_flag, 0);
		uint32 total_remove_count = thrust::reduce(dp_remove_item_count, dp_remove_item_count + local_queue_count);

		do_rebalance_procedure<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>(
				data,
				global_queue_size,
				local_queue_count,
				local_queue_size,
				add_operation,
				remove_flag_count,
				total_remove_count,
				remove_flag,
				remove_item_count);
	}
}

template<typename BaseSchema, typename T, bool LocalQueueAtomicity, bool GlobalQueueAtomicity>
void do_rebalance_procedure(
		uint8* data,
		uint32 global_queue_size,
		uint32 local_queue_count,
		uint32 local_queue_size,
		bool operation,
		uint32 flag_count,
		uint32 total_items,
		uint32* scanned_flag,
		uint32* item_count)
{
//		uint32 threads = 32;
//		uint32 blocks;
	static uint32* compacted_index = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count);
	static uint32* compacted_count = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count);

	static uint32* scanned_count = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count);

	static uint32* queue_id_table     = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count * local_queue_size);
	static uint32* queue_offset_table = (uint32*)allocate(buffer_location_t::device_ptx_pinned_0, sizeof(uint32) * local_queue_count * local_queue_size);

	do_add_remove_simple_compact<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>(local_queue_count, scanned_flag, item_count, compacted_index, compacted_count);

	thrust::device_ptr<uint32> dp_compacted_count(compacted_count);
	thrust::device_ptr<uint32> dp_scanned_count(scanned_count);

	thrust::copy(dp_compacted_count, dp_compacted_count + local_queue_count, dp_scanned_count);
	thrust::exclusive_scan(dp_scanned_count, dp_scanned_count + local_queue_count, dp_scanned_count, 0);

	/// generate sequece table here
	generate_sequence_table<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>(flag_count, compacted_index, compacted_count, scanned_count, queue_id_table, queue_offset_table);

	/// do balance queue here
	balance_global_local_queue<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>(
			data,
			total_items,
			operation,
			global_queue_size,
			local_queue_size,
			queue_id_table,
			queue_offset_table);

	/// update local queues' rptr(or wptr)
	update_global_local_queue_ptr<BaseSchema, T, LocalQueueAtomicity, GlobalQueueAtomicity>(
			data,
			flag_count,
			total_items,
			operation,
			global_queue_size,
			local_queue_size,
			compacted_index,
			compacted_count);
}


} } } } } }
