/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MULTIPLEPATTERNSEARCHER_JIT_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_MULTIPLEPATTERNSEARCHER_JIT_H_

#include "core/Prerequisite.h"
#include "framework/processor/cuda/primitives/detail/MultiplePatternSearcherKernel.h"
#include "framework/processor/cuda/kernel/DeviceArray.h"
#include "framework/processor/cuda/kernel/ArrayHandle.h"
#include "framework/processor/cuda/primitives/HashMap.h"
#include "framework/processor/cuda/primitives/Transposer.h"
#include "framework/processor/cuda/kernel/api/DriverApi.h"
#include "framework/processor/cuda/primitives/jit/JITContext.h"
#include <boost/unordered_map.hpp>

namespace zillians { namespace framework { namespace processor { namespace cuda {

namespace zvpcpj = zillians::framework::processor::cuda::primitives::jit;

template<int _BLOCK_STRIPE>
struct BLOCK_STRIPE_TO_TYPE
{
};

template<>
struct BLOCK_STRIPE_TO_TYPE<4>
{
	typedef uint32 type;
};

template<>
struct BLOCK_STRIPE_TO_TYPE<8>
{
	typedef uint64 type;
};

//=====================================
// Generic Template
//=====================================

template<typename KernelApi, int _BLOCK_STRIPE>
bool MultiplePatternSearchKernel_RabinKarp_Spec(
		const zvpcpj::JITContext_t* JITContext,
		HASHMAPAPI_DECLARE_PARAMETERS,
		typename BLOCK_STRIPE_TO_TYPE<_BLOCK_STRIPE>::type* textData, uint32 textLength, uint32 patternLength,
		uint32 hashComponent, uint32 hashComponentMax,
		uint32 matchedCountMax, uint32* matchedCount, uint32* matchedTextPositions, uint32* matchedPatternPosition);

//=====================================
// RuntimeApi Full Specialization Prototype
//=====================================

#if BLOCK_STRIPE == 4

template<>
bool MultiplePatternSearchKernel_RabinKarp_Spec<RuntimeApi, 4>(
		const zvpcpj::JITContext_t* JITContext,
		HASHMAPAPI_DECLARE_PARAMETERS,
		BLOCK_STRIPE_TO_TYPE<4>::type* textData, uint32 textLength, uint32 patternLength,
		uint32 hashComponent, uint32 hashComponentMax,
		uint32 matchedCountMax, uint32* matchedCount, uint32* matchedTextPositions, uint32* matchedPatternPosition);

#elif BLOCK_STRIPE == 8

template<>
bool MultiplePatternSearchKernel_RabinKarp_Spec<RuntimeApi, 8>(
		const zvpcpj::JITContext_t* JITContext,
		HASHMAPAPI_DECLARE_PARAMETERS,
		BLOCK_STRIPE_TO_TYPE<8>::type* textData, uint32 textLength, uint32 patternLength,
		uint32 hashComponent, uint32 hashComponentMax,
		uint32 matchedCountMax, uint32* matchedCount, uint32* matchedTextPositions, uint32* matchedPatternPosition);

#endif

//=====================================
// DriverApi Full Specialization Prototype
//=====================================

#if BLOCK_STRIPE == 4

template<>
bool MultiplePatternSearchKernel_RabinKarp_Spec<DriverApi, 4>(
		const zvpcpj::JITContext_t* JITContext,
		HASHMAPAPI_DECLARE_PARAMETERS,
		BLOCK_STRIPE_TO_TYPE<4>::type* textData, uint32 textLength, uint32 patternLength,
		uint32 hashComponent, uint32 hashComponentMax,
		uint32 matchedCountMax, uint32* matchedCount, uint32* matchedTextPositions, uint32* matchedPatternPosition);

#elif BLOCK_STRIPE == 8

template<>
bool MultiplePatternSearchKernel_RabinKarp_Spec<DriverApi, 8>(
		const zvpcpj::JITContext_t* JITContext,
		HASHMAPAPI_DECLARE_PARAMETERS,
		BLOCK_STRIPE_TO_TYPE<8>::type* textData, uint32 textLength, uint32 patternLength,
		uint32 hashComponent, uint32 hashComponentMax,
		uint32 matchedCountMax, uint32* matchedCount, uint32* matchedTextPositions, uint32* matchedPatternPosition);

#endif

} } } }

#endif
