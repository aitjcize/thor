/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHSET_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHSET_H_

#include "core/Prerequisite.h"
#include "framework/processor/cuda/primitives/HashSetApi.h"
#include <boost/tr1/unordered_set.hpp>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <driver_types.h>

#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHSET_AVG_LOAD_PER_BUCKET		389
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHSET_REPORTING_RESTART		1

namespace zillians { namespace framework { namespace processor { namespace cuda {

namespace detail {

extern bool HashSetKernel_KeyRedistributeIntoBucket(uint32 keyCount, uint32* keys, uint32 bucketCount, uint32* bucketSizes, uint32* keysInBucket, uint32 bucketHash0, uint32 bucketHash1);
extern bool HashSetKernel_BuildCuckooHashingInBucket(uint32 bucketCount, uint32* bucketSizes, uint32* keysInBucket, uint32 cuckooHash, uint32* hashTables, uint32* buildErrors);
extern bool HashSetKernel_FindInParallel(uint32 keyCount, uint32* keys, uint32 bucketCount, uint32 bucketHash0, uint32 bucketHash1, uint cuckooHash, uint32* hashTables, uint32* positionFound);

}

template<typename KernelApi>
class HashSet
{
public:
	HashSet(const KernelApi& api) :
		mKernelApi(api),
		mMaxElements(0), mBucketCount(0),
		mKeys(NULL), mKeysInBuckets(NULL), mBucketSizes(NULL), mBuildErrors(NULL), mHashSetDevice(NULL),
		mBucketHash0(rand()), mBucketHash1(rand()), mCuckooHash(rand()),
		mDirty(true)
	{
	}

	~HashSet()
	{
		finalize();
	}

public:
	void initialize(int maxElements)
	{
		BOOST_ASSERT(maxElements > 0);

		mMaxElements = maxElements;
		mBucketCount = (maxElements / ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHSET_AVG_LOAD_PER_BUCKET) + ((maxElements % ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHSET_AVG_LOAD_PER_BUCKET == 0) ? 0 : 1);

		mKeys = new DeviceArray<KernelApi>(mKernelApi, maxElements * sizeof(uint32));
		mKeysInBuckets = new DeviceArray<KernelApi>(mKernelApi, mBucketCount * 512 * sizeof(uint32));
		mBucketSizes = new DeviceArray<KernelApi>(mKernelApi, mBucketCount * sizeof(uint32));
		mBuildErrors = new DeviceArray<KernelApi>(mKernelApi, mBucketCount * sizeof(uint32));
		mHashSetDevice = new DeviceArray<KernelApi>(mKernelApi, mBucketCount * 192 * 3 * sizeof(uint32));

		mBucketHash0 = rand();
		mBucketHash1 = rand();
		mCuckooHash  = rand();
	}

	void finalize()
	{
		SAFE_DELETE(mKeys);
		SAFE_DELETE(mKeysInBuckets);
		SAFE_DELETE(mBucketSizes);
		SAFE_DELETE(mBuildErrors);
		SAFE_DELETE(mHashSetDevice);
		mHashSetHost.clear();
		mMaxElements = mBucketCount = 0;
	}

public:
	HashSetKernelParameters& getKernelParameters()
	{
		if(mDirty)
		{
			commit();
		}

		ArrayHandle<DeviceArray<KernelApi> > handleHashSetDevice(mHashSetDevice, accessLocation::device, accessMode::read);

		mKernelParameters.hashSet = handleHashSetDevice.template data<uint32>();
		mKernelParameters.bucketCount = mBucketCount;
		mKernelParameters.bucketHash0 = mBucketHash0;
		mKernelParameters.bucketHash1 = mBucketHash1;
		mKernelParameters.cuckooHash = mCuckooHash;

		return mKernelParameters;
	}

public:
	void clear()
	{
		mHashSetHost.clear();
		mDirty = true;
	}

	bool add(uint32 key)
	{
		BOOST_ASSERT(mMaxElements > 0);
		BOOST_ASSERT(mHashSetHost.size() < mMaxElements);

		boost::unordered_set<uint32>::iterator it;
		bool result;
		boost::tie(it, result) = mHashSetHost.insert(key);

		if(result)
		{
			mDirty = true;
			return true;
		}
		else
		{
			return false;
		}
	}

	bool remove(uint32 key)
	{
		BOOST_ASSERT(mMaxElements > 0);

		boost::unordered_set<uint32>::iterator it = mHashSetHost.find(key);
		if(it != mHashSetHost.end())
		{
			mHashSetHost.erase(it);
			mDirty = true;
			return true;
		}
		else
		{
			return false;
		}
	}

	void commit()
	{
		BOOST_ASSERT(mMaxElements > 0);

		// compile key hash set into array and upload the keys
		uint32 keyCount = 0;
		{
			ArrayHandle<DeviceArray<KernelApi> > handleKeys(*mKeys, accessLocation::host, accessMode::readwrite);
			uint32* pointerKeys = handleKeys.template data<uint32>();

			int i = 0;
			for(boost::unordered_set<uint32>::iterator it = mHashSetHost.begin(); it != mHashSetHost.end(); i++, it++)
			{
				pointerKeys[i] = *it;
			}
			keyCount = i;

			handleKeys.dirty();
		}

		// reset temporary data
		{
			ArrayHandle<DeviceArray<KernelApi> > handleKeysInBuckets(*mKeysInBuckets, accessLocation::host, accessMode::readwrite);
			handleKeysInBuckets.template fill<uint32>(std::numeric_limits<uint32>::max());

			ArrayHandle<DeviceArray<KernelApi> > handleBucketSizes(*mBucketSizes, accessLocation::host, accessMode::readwrite);
			handleBucketSizes.template fill<uint32>(0);

			ArrayHandle<DeviceArray<KernelApi> > handleBuildErrors(*mBuildErrors, accessLocation::host, accessMode::readwrite);
			handleBuildErrors.template fill<uint32>(0);

			ArrayHandle<DeviceArray<KernelApi> > handleHashSetDevice(*mHashSetDevice, accessLocation::host, accessMode::readwrite);
			handleHashSetDevice.template fill<uint32>(std::numeric_limits<uint32>::max());
		}

		// build for the first phase: FSM hash redistribution to make each block process its data independently
		while(true)
		{
			// launch the kernel to redistribute keys into buckets
			{
				ArrayHandle<DeviceArray<KernelApi> > handleKeys(*mKeys, accessLocation::device, accessMode::read);
				ArrayHandle<DeviceArray<KernelApi> > handleBucketSizes(*mBucketSizes, accessLocation::device, accessMode::readwrite);
				ArrayHandle<DeviceArray<KernelApi> > handleKeysInBuckets(*mKeysInBuckets, accessLocation::device, accessMode::readwrite);

				detail::HashSetKernel_KeyRedistributeIntoBucket(
						keyCount, handleKeys.template data<uint32>(),
						mBucketCount, handleBucketSizes.template data<uint32>(), handleKeysInBuckets.template data<uint32>(), mBucketHash0, mBucketHash1);

				handleBucketSizes.dirty();
				handleKeysInBuckets.dirty();
			}

			// check if there's any bucket contains more than 512 elements; if so, restart with different bucket hash
			bool overflowed = false;
			{
				ArrayHandle<DeviceArray<KernelApi> > handleBucketSizes(*mBucketSizes, accessLocation::host, accessMode::read);

				uint totalKeysRedistributed = 0;
				for(int i=0;i<mBucketCount;++i)
				{
					uint32 size = handleBucketSizes.template data<uint>()[i];
					totalKeysRedistributed += size;
					if(size >= 512)
					{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHSET_REPORTING_RESTART
						printf("bucketCounts[%d] = %d, overflow, restarting...\n", i, size);
#endif
						overflowed = true;
					}
				}

				if(totalKeysRedistributed != keyCount)
				{
					printf("Fatal error, missing %d keys in first phase (coould be some bug in atomic add)\n", keyCount - totalKeysRedistributed);
					throw std::runtime_error("key missing while building the hash set in first phase");
				}
			}

			if(overflowed)
			{
				// reset the bucket size counters
				ArrayHandle<DeviceArray<KernelApi> > handleBucketSizes(*mBucketSizes, accessLocation::device, accessMode::readwrite);
				handleBucketSizes.template fill<uint32>(0);

				// choose another set of bucket hash
				mBucketHash0 = rand();
				mBucketHash1 = rand();
			}
			else
			{
				// success, stop building
				break;
			}
		}

		// perform data integrity check (optional)
		if(1)
		{
			ArrayHandle<DeviceArray<KernelApi> > handleKeysInBuckets(*mKeysInBuckets, accessLocation::host, accessMode::read);
			uint32* pointerKeysInBuckets = handleKeysInBuckets.template data<uint32>();

			uint totalKeysRedistributed = 0;
			for(int i=0; i<mBucketCount * 512; ++i)
			{
				if(pointerKeysInBuckets[i] != std::numeric_limits<uint32>::max())
				{
					++totalKeysRedistributed;
				}
			}

			if(totalKeysRedistributed != keyCount)
			{
				printf("Fatal error, missing %d keys in first phase (could be some bug in memory model)\n", keyCount - totalKeysRedistributed);
				throw std::runtime_error("key missing while building the hash set in first phase");
			}
		}

		// build for the second phase: parallel cuckoo hashing
		while(true)
		{
			{
				ArrayHandle<DeviceArray<KernelApi> > handleBucketSizes(*mBucketSizes, accessLocation::device, accessMode::read);
				ArrayHandle<DeviceArray<KernelApi> > handleKeysInBuckets(*mKeysInBuckets, accessLocation::device, accessMode::read);
				ArrayHandle<DeviceArray<KernelApi> > handleBuildErrors(*mBuildErrors, accessLocation::device, accessMode::readwrite);
				ArrayHandle<DeviceArray<KernelApi> > handleHashSetDevice(*mHashSetDevice, accessLocation::device, accessMode::readwrite);

				detail::HashSetKernel_BuildCuckooHashingInBucket(
						mBucketCount, handleBucketSizes.template data<uint32>(), handleKeysInBuckets.template data<uint32>(), mCuckooHash, handleHashSetDevice.template data<uint32>(), handleBuildErrors.template data<uint32>());
			}

			// check if there's any bucket failed to perform cuckoo hashing; if so, restart with different seed
			bool failed = false;
			{
				ArrayHandle<DeviceArray<KernelApi> > handleBuildErrors(*mBuildErrors, accessLocation::host, accessMode::read);

				for(int i=0; i<mBucketCount; ++i)
				{
					if(handleBuildErrors.template data<uint32>()[i] > 0)
					{
#if ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHSET_REPORTING_RESTART
						printf("Failed to find placement for bucket #%d, restarting...\n", i);
#endif
						failed = true;
					}
				}
			}

			if(failed)
			{
				// reset the build error states
				ArrayHandle<DeviceArray<KernelApi> > handleBuildErrors(*mBuildErrors, accessLocation::device, accessMode::readwrite);
				handleBuildErrors.template fill<uint32>(0);

				// choose another seed for cuckoo hashing
				mCuckooHash = rand();
			}
			else
			{
				// success, stop building
				break;
			}
		}

		// perform data integrity check (optional)
		if(1)
		{
			ArrayHandle<DeviceArray<KernelApi> > handleHashSetDevice(*mHashSetDevice, accessLocation::host, accessMode::read);
			uint32* pointerHashSetDevice = handleHashSetDevice.template data<uint32>();

			uint totalKeysHashed = 0;
			for(int i=0; i<mBucketCount*576; ++i)
			{
				if(pointerHashSetDevice[i] != std::numeric_limits<uint32>::max())
				{
					++totalKeysHashed;
				}
			}

			if(totalKeysHashed != keyCount)
			{
				printf("Fatal error, missing %d keys in second phase (could be some bug in synchronization)\n", keyCount - totalKeysHashed);
				throw std::runtime_error("key missing while building the hash set in second phase");
			}
		}

		mDirty = false;
	}

	bool find(uint32 key)
	{
		BOOST_ASSERT(mMaxElements > 0);

		boost::unordered_set<uint32>::iterator it = mHashSetHost.find(key);
		if(it != mHashSetHost.end())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool findInParallel(/*IN*/ const std::vector<uint32>& keys, /*OUT*/ std::vector<bool>& result)
	{
		BOOST_ASSERT(mMaxElements > 0);

		uint32 keyCount = keys.size();

		// prepare the keys to search device array
		DeviceArray<KernelApi> keysToSearch(mKernelApi, keyCount * sizeof(uint32));
		{
			ArrayHandle<DeviceArray<KernelApi> > handleKeyToSearch(keysToSearch, accessLocation::host, accessMode::readwrite);
			uint32* pointerKeyToSearch = handleKeyToSearch.template data<uint32>();

			int i = 0;
			for(std::vector<uint32>::const_iterator it = keys.begin(); it != keys.end(); ++it, ++i)
			{
				pointerKeyToSearch[i] = *it;
			}

			handleKeyToSearch.dirty();
		}

		// prepare the result array
		DeviceArray<KernelApi> positionFound(mKernelApi, keyCount * sizeof(uint32));

		// perform search in parallel
		{
			ArrayHandle<DeviceArray<KernelApi> > handleKeyToSearch(keysToSearch, accessLocation::device, accessMode::read);
			ArrayHandle<DeviceArray<KernelApi> > handleHashSetDevice(*mHashSetDevice, accessLocation::device, accessMode::read);
			ArrayHandle<DeviceArray<KernelApi> > handlePositionFound(positionFound, accessLocation::device, accessMode::readwrite);

			detail::HashSetKernel_FindInParallel(
					keyCount,
					handleKeyToSearch.template data<uint32>(),
					mBucketCount, mBucketHash0, mBucketHash1, mCuckooHash,
					handleHashSetDevice.template data<uint32>(), handlePositionFound.template data<uint32>());

			handlePositionFound.dirty();
		}

		// populate all searched position and generate results
		{
			ArrayHandle<DeviceArray<KernelApi> > handlePositionFound(positionFound, accessLocation::host, accessMode::read);
			uint32* pointerPositionFound = handlePositionFound.template data<uint32>();

			result.clear();
			result.reserve(keyCount);

			uint failed_to_find = 0;
			for(int i=0; i<keyCount; ++i)
			{
				if(pointerPositionFound[i] == std::numeric_limits<uint32>::max())
				{
					result.push_back(false);
				}
				else
				{
					result.push_back(true);
				}
			}
		}
	}

private:
	const KernelApi& mKernelApi;

	std::size_t mMaxElements;

	uint32 mBucketCount;
	uint32 mBucketHash0;
	uint32 mBucketHash1;
	uint32 mCuckooHash;

	DeviceArray<KernelApi>* mKeys;
	DeviceArray<KernelApi>* mKeysInBuckets;
	DeviceArray<KernelApi>* mBucketSizes;
	DeviceArray<KernelApi>* mBuildErrors;
	DeviceArray<KernelApi>* mHashSetDevice;
	boost::unordered_set<uint32> mHashSetHost;
	bool mDirty;

	HashSetKernelParameters mKernelParameters;
};

} } } }

#endif/*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_HASHSET_H_*/
