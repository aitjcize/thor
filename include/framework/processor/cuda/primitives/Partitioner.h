/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_PARTITIONER_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_PARTITIONER_H_

#include "core/Prerequisite.h"
#include "framework/processor/cuda/primitives/Scanner.h"
#include "framework/processor/cuda/kernel/DeviceArray.h"
#include "framework/processor/cuda/kernel/ArrayHandle.h"

namespace zillians { namespace framework { namespace processor { namespace cuda {

namespace detail {

extern bool PartitionerKernel_CreateDifferentFlags(/*IN*/ uint32* keys, /*OUT*/ uint32 *flags, uint elements);
extern bool PartitionerKernel_CreateDifferentIndices(/*IN*/ uint32* flags, /*IN*/ uint32* flagsScanned, /*OUT*/ uint32* indices, uint32 elements);
extern bool PartitionerKernel_CreatePartitionRanges(/*IN*/ uint32* indices, /*OUT*/ uint32* fromIndices, /*OUT*/ uint32* toIndices, uint32 elements);

}

template<typename KernelApi>
class Partitioner
{
public:
	Partitioner(const KernelApi& api) :
		mKernelApi(api),
		mMaxElements(0), mScannerOwner(false), mScanner(NULL),
		mDifferentFlags(NULL), mDifferentFlagsScanned(NULL), mDifferentIndices(NULL)
	{

	}

	~Partitioner()
	{
		finalize();
	}

	void initialize(Scanner<KernelApi>* scanner)
	{
		// make sure the given scanner is properly initialized & compatible
		BOOST_ASSERT(scanner->mMaxElements > 0);

		finalize();

		mScannerOwner = false;
		mScanner = scanner;
		mMaxElements = scanner->mMaxElements;

		// mDifferentFlags has mMaxElements elements at most (since it's the flags in-between plus the last dummy flag)
		mDifferentFlags        = new DeviceArray<KernelApi>(mKernelApi, mMaxElements * sizeof(uint32));

		// mDifferentFlagsScanned has the same size as mDifferentFlags
		mDifferentFlagsScanned = new DeviceArray<KernelApi>(mKernelApi, mMaxElements * sizeof(uint32));

		// mDifferentIndices has the same size of mDifferentFlagsScanned
		mDifferentIndices      = new DeviceArray<KernelApi>(mKernelApi, mMaxElements * sizeof(uint32));
	}

	void initialize(int maxElements)
	{
		finalize();

		mScannerOwner = true;
		mScanner = new Scanner<KernelApi>(mKernelApi);
		BOOST_ASSERT(mScanner);
		mScanner->initialize(maxElements, ScannerDataType::type_int32, ScannerMode::exclusive, ScannerOp::plus);

		mMaxElements = maxElements;

		// mDifferentFlags has mMaxElements-1 elements at most (since it's the flags in-between)
		mDifferentFlags        = new DeviceArray<KernelApi>(mKernelApi, mMaxElements * sizeof(uint32));

		// mDifferentFlagsScanned has the same size as mDifferentFlags
		mDifferentFlagsScanned = new DeviceArray<KernelApi>(mKernelApi, mMaxElements * sizeof(uint32));

		// mDifferentIndices has the same size of mDifferentFlagsScanned
		mDifferentIndices      = new DeviceArray<KernelApi>(mKernelApi, mMaxElements * sizeof(uint32));
	}

	void finalize()
	{
		if(mScannerOwner)
		{
			SAFE_DELETE(mScanner);
		}
		else
		{
			SAFE_NULL(mScanner);
		}
		mScannerOwner = false;

		SAFE_DELETE(mDifferentFlags);
		SAFE_DELETE(mDifferentFlagsScanned);
		SAFE_DELETE(mDifferentIndices);
	}

	uint32 partition(/*IN*/ ArrayHandle<DeviceArray<KernelApi> >* keys, /*OUT*/ ArrayHandle<DeviceArray<KernelApi> >* fromIndices, /*OUT*/ ArrayHandle<DeviceArray<KernelApi> >* toIndices, uint32 elements)
	{
		using namespace detail;

		ArrayHandle<DeviceArray<KernelApi> > mHandleToDifferentFlags(*mDifferentFlags, accessLocation::device, accessMode::readwrite);
		ArrayHandle<DeviceArray<KernelApi> > mHandleToDifferentFlagsScanned(*mDifferentFlagsScanned, accessLocation::device, accessMode::readwrite);
		ArrayHandle<DeviceArray<KernelApi> > mHandleToDifferentIndices(*mDifferentIndices, accessLocation::device, accessMode::readwrite);

		// first we create all different flags which indicate the differences between left and right element of each thread
		PartitionerKernel_CreateDifferentFlags(keys->template data<uint32>(), mHandleToDifferentFlags.template data<uint32>(), elements);
		mHandleToDifferentFlags.dirty();

		if(0)
		{
			printf("===[diff flags]===\n");
			ArrayHandle<DeviceArray<KernelApi> > dbgHandle(*mDifferentFlags, accessLocation::host, accessMode::read);
			for(int i=0;i<elements;++i)
			{
				printf("[%d] = %d\n", i, dbgHandle.template data<uint32>()[i]);
			}
		}

		// scan the different flags (N input elements => N-1 difference flags)
		mScanner->scanExclusive(&mHandleToDifferentFlagsScanned, &mHandleToDifferentFlags, elements);
		mHandleToDifferentFlagsScanned.dirty();

		if(0)
		{
			printf("===[diff flags scanned]===\n");
			ArrayHandle<DeviceArray<KernelApi> > dbgHandle(*mDifferentFlagsScanned, accessLocation::host, accessMode::read);
			for(int i=0;i<elements;++i)
			{
				printf("[%d] = %d\n", i, dbgHandle.template data<uint32>()[i]);
			}
		}

		uint32 count = 0;
		{
			ArrayHandle<DeviceArray<KernelApi> > handleToScannedCount(*mDifferentFlagsScanned, accessLocation::host, accessMode::read, (elements - 1) * sizeof(uint32), sizeof(uint32));
			count = handleToScannedCount.template at<uint32>(0);

			ArrayHandle<DeviceArray<KernelApi> > handleToLastFlag(*mDifferentFlags, accessLocation::host, accessMode::read, (elements - 1) * sizeof(uint32), sizeof(uint32));
			count += handleToLastFlag.template at<uint32>(0);
		}

		BOOST_ASSERT(count > 0);

		PartitionerKernel_CreateDifferentIndices(mHandleToDifferentFlags.template data<uint32>(), mHandleToDifferentFlagsScanned.template data<uint32>(), mHandleToDifferentIndices.template data<uint32>(), elements);

		if(0)
		{
			printf("===[diff indices]===\n");
			ArrayHandle<DeviceArray<KernelApi> > dbgHandle(*mDifferentIndices, accessLocation::host, accessMode::read);
			for(int i=0;i<count;++i)
			{
				printf("[%d] = %d\n", i, dbgHandle.template data<uint32>()[i]);
			}
		}

		PartitionerKernel_CreatePartitionRanges(mHandleToDifferentIndices.template data<uint32>(), fromIndices->template data<uint32>(), toIndices->template data<uint32>(), count);

		if(0)
		{
			printf("===[diff ranges from]===\n");
			ArrayHandle<DeviceArray<KernelApi> > dbgHandle(fromIndices->array(), accessLocation::host, accessMode::read);
			for(int i=0;i<count;++i)
			{
				printf("[%d] = %d\n", i, dbgHandle.template data<uint32>()[i]);
			}
		}
		if(0)
		{
			printf("===[diff ranges to]===\n");
			ArrayHandle<DeviceArray<KernelApi> > dbgHandle(toIndices->array(), accessLocation::host, accessMode::read);
			for(int i=0;i<count;++i)
			{
				printf("[%d] = %d\n", i, dbgHandle.template data<uint32>()[i]);
			}
		}

		fromIndices->dirty();
		toIndices->dirty();

		return count;
	}

private:
	const KernelApi& mKernelApi;

	bool mScannerOwner;
	Scanner<KernelApi>* mScanner;
	std::size_t mMaxElements;

	DeviceArray<KernelApi>* mDifferentFlags;
	DeviceArray<KernelApi>* mDifferentFlagsScanned;
	DeviceArray<KernelApi>* mDifferentIndices;
};

} } } }

#endif/*ZILLIANS_FRAMEWORK_PROCESSOR_CUDA_PARTITIONER_H_*/
