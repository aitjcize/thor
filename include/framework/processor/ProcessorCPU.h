/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_PROCESSORCPU_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_PROCESSORCPU_H_

#include <atomic>

#include "core/IntTypes.h"
#include "core/SharedPtr.h"

#include "framework/Kernel.h"
#include "framework/Processor.h"

namespace zillians { namespace framework {

class ExecutionMonitor;
class KernelBase;

} }

namespace zillians { namespace framework { namespace processor {

/**
 * ProcessorCPU is the logic processor utilize TBB task framework
 */
class ProcessorCPU : public ProcessorRT
{
public:
    explicit ProcessorCPU(uint32 local_id);
    virtual ~ProcessorCPU();

    bool isMultiThread() const noexcept;
    void setMultiThread(bool enable) noexcept;

    virtual void initialize() override;
    virtual void finalize() override;

    virtual void  exit(int32 exit_code) override; // no return
    virtual int32 getExitCode() const override;

private:
    virtual void run(const shared_ptr<ExecutionMonitor>& monitor) override;

public:
    virtual void pushImplicitPendingCompletion(int count);
    virtual void popImplicitPendingCompletion(int count);

public:
    virtual bool isCompleted();
    virtual bool isRunning();

private:
    void runImpl();

private:
    bool              mMultiThread;
    std::atomic<bool> mIsExited;
    int32             mExitCode;
    std::atomic<int>  mImplicitPendingCompletion;
};

} } }

#endif /* ZILLIANS_FRAMEWORK_PROCESSOR_PROCESSORCPU_H_ */
