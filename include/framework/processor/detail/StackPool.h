/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSORS_DETAIL_STACKPOOL_H_
#define ZILLIANS_FRAMEWORK_PROCESSORS_DETAIL_STACKPOOL_H_

#include <cstddef>

#include <vector>

#include <boost/coroutine/stack_allocator.hpp>
#include <boost/noncopyable.hpp>
#include <boost/version.hpp>

// fr3@K
// stack_allocator interface changed in 1.55, requiring stack_context
#if(BOOST_VERSION >= 105500)
#include <boost/coroutine/stack_context.hpp>
#endif

namespace zillians { namespace framework { namespace processor { namespace mt { namespace detail {

class StackPool : boost::noncopyable
{
public:
              StackPool();
    explicit  StackPool(std::size_t concurrency);
             ~StackPool();

    std::size_t               getConcurrency() const noexcept;
    const std::vector<void*>& getStacks     () const noexcept;
    std::size_t               getStackSize  () const noexcept;

private:
    std::size_t                        mStackSize;
    std::vector<void*>                 mStacks;
    boost::coroutines::stack_allocator mAllocator;
#if(BOOST_VERSION >= 105500)
    std::vector<boost::coroutines::stack_context> mContexts;
#endif
};

} } } } }

#endif /* ZILLIANS_FRAMEWORK_PROCESSORS_DETAIL_STACKPOOL_H_ */
