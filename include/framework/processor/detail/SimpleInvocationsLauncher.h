/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSORS_DETAIL_SIMPLEINVOCATIONSLAUNCHER_H_
#define ZILLIANS_FRAMEWORK_PROCESSORS_DETAIL_SIMPLEINVOCATIONSLAUNCHER_H_

#include <condition_variable>
#include <mutex>
#include <thread>
#include <vector>

#include <boost/asio/io_service.hpp>
#include <boost/noncopyable.hpp>

#include "framework/processor/detail/StackPool.h"

namespace zillians { namespace framework { namespace service {

class InvocationRequestBuffer;

} } }

namespace zillians { namespace framework { namespace processor { namespace mt {

class Kernel;

} } } }

namespace zillians { namespace framework { namespace processor { namespace mt { namespace detail {

class SimpleInvocationsLauncher : boost::noncopyable
{
public:
    explicit SimpleInvocationsLauncher(bool enable_mt);
    ~SimpleInvocationsLauncher();

    void launch(Kernel& kernel, service::InvocationRequestBuffer& invocations);

private:
    StackPool                           stack_pool;
    boost::asio::io_service             service;
    std::vector<std::thread>            workers;
    std::vector<std::thread>::size_type unfinished_count;
    std::mutex                          unfinished_guard;
    std::condition_variable             unfinished_cond;
};

} } } } }

#endif /* ZILLIANS_FRAMEWORK_PROCESSORS_DETAIL_SIMPLEINVOCATIONSLAUNCHER_H_ */
