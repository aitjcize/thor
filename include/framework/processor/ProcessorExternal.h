/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_PROCESSOREXTERNAL_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_PROCESSOREXTERNAL_H_

#include "core/ConditionVariable.h"
#include "framework/Processor.h"
#include <tbb/atomic.h>

namespace zillians { namespace framework { namespace processor {

/**
 * ProcessorExternal is an external interface for others component to interact with process/services in a thread-safe manner
 */
class ProcessorExternal : public Processor
{
	friend class ExecutionEngine;
public:
	ProcessorExternal(uint32 local_id);
	virtual ~ProcessorExternal();

private:
	virtual void run(const shared_ptr<ExecutionMonitor>& monitor) override;

public:
	virtual bool isCompleted();
	virtual bool isRunning();

public:
    virtual void pushImplicitPendingCompletion(int count);
    virtual void popImplicitPendingCompletion(int count);

public:
	void processITC(bool blocking = false, bool one_or_all = false);
	void enqueueITC(uint32 processor_id, KernelITC& itc);
	void enqueueITC(uint32 processor_id, KernelITC& itc, ExecutionITC::kernel_itc_handler_t continuation_handler);

private:
	tbb::concurrent_queue< boost::tuple<uint32, KernelITC, ExecutionITC::kernel_itc_handler_t > >mITCQueue;

private:
	tbb::atomic<int> mImplicitPendingCompletion;
};

} } }

#endif /* ZILLIANS_FRAMEWORK_PROCESSOR_PROCESSOREXTERNAL_H_ */
