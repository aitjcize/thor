/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_PROCESSORCUDA_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_PROCESSORCUDA_H_

#include "threading/AdaptiveWait.h"
#include "framework/Processor.h"
#include "framework/processor/cuda/kernel/KernelApi.h"

using namespace zillians::threading;

namespace zillians { namespace framework { namespace processor {

/**
 * ProcessorCUDA is a game logic processor utilize CUDA on NVIDIA GPUs
 */
class ProcessorCUDA : public ProcessorRT
{
public:
	ProcessorCUDA(uint32 local_id, uint32 device_id);
	virtual ~ProcessorCUDA();

public:
	virtual void initialize();
	virtual void finalize();

	virtual void  exit(int32 exit_code) override; // no return
	virtual int32 getExitCode() const override;

private:
	virtual void run(const shared_ptr<ExecutionMonitor>& monitor);

public:
    virtual void pushImplicitPendingCompletion(int count);
    virtual void popImplicitPendingCompletion(int count);

public:
	virtual bool isCompleted();
	virtual bool isRunning();

public:
	uint32 getDeviceId();

public:
	void handleProcessorInvocation(uint32 source, KernelITC& itc);

private:
	shared_ptr<ExecutionMonitor> mMonitor;

private:
	volatile int mImplicitPendingCompletion;

private:
	uint32 mDeviceId;
	std::list<std::pair<uint32, KernelITC>> mDispatchedInvocations;

private:
	cuda::KernelApi& mKernelApi;
};

} } }

#endif /* ZILLIANS_FRAMEWORK_PROCESSOR_PROCESSORCUDA_H_ */
