/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_PROCESSORGATEWAY_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_PROCESSORGATEWAY_H_

#include <atomic>
#include <cstdint>
#include <memory>
#include <mutex>
#include <thread>

#include <boost/optional/optional_fwd.hpp>
#include <boost/bimap/bimap.hpp>
#include <boost/bimap/multiset_of.hpp>

#include <tbb/concurrent_queue.h>

#include <websocketpp/websocketpp.hpp>

#include "core/SimpleSlotAllocator.h"

#include "utility/UUIDUtil.h"

#include "framework/ExecutionITC.h"
#include "framework/Processor.h"
#include "framework/processor/websocket/WebSocketGatewayHandler.h"

#include "network/session/TCPSessionEngine.h"
#include "network/session/TCPSession.h"
#include "network/session/stream/SegmentedStream.h"

#include "protocol/SequenceMessage.h"
#include "protocol/RemoteInvocationMessage.h"

namespace zillians { namespace framework { namespace processor {

// Responsibilities:
// accept client connection and create remote domain object
// translate the RPC from/to clients
// listen on specific port for client connection
// plugable transport module (tcp/infiniband/...)
// => GatewayInterface { listen(string); accept(string); open(string); rpc_recv, rpc_send, object_push(id, , object_pull(
// => TCPGateway
// => IBGateway
// problem: how to represent the function call? as an array of parameters
// problem: how to represent the object tree? as an array of serialized buffer?
// for infiniband, we can probably copy the table entry to the remote without serialization
// then how does the interface expose such flexiblity to allow different implementation does different thing?
// just object id? okay, then the Gateway need to be part of the processor and involve the ITC
// or it can directly talks to other object service?

// using object service itc to access object information
// pros: seperation of concern, different execution context
// cons: slow? (maybe not, if we only ask for raw data pointers instead of memory copies)
// the plugin isself is a processor!

struct DomainContext
{
    explicit DomainContext(network::session::TCPSession* session) : session_id(-1), session(session), stream(new network::session::SegmentedStream<network::session::TCPSession>(*session))
    { }

    ~DomainContext()
    {
        SAFE_DELETE(stream);
        SAFE_DELETE(session);
    }

    bool is_websocket() { return bool(connection_ptr) ? true : false ; }

    int64 session_id;
    WebSocketGateway::connection_ptr connection_ptr;
    network::session::TCPSession* session;
    network::session::SegmentedStream<network::session::TCPSession>* stream;

    SequenceMessage          sequence_message_in;
    SequenceMessage          sequence_message_out;
    RemoteInvocationMessage  remote_invocation_message_in;
    RemoteInvocationMessage  remote_invocation_message_out;
};

class ProcessorGateway : public Processor
{
public:
    explicit ProcessorGateway(uint32 local_id);
    virtual ~ProcessorGateway();

public:
    virtual void initialize();
    virtual void finalize();

    void                          setKernel(const shared_ptr<KernelBase>& kernel);
    const shared_ptr<KernelBase>& getKernel() const noexcept;

private:
    virtual void run(const shared_ptr<ExecutionMonitor>& monitor) override;

public:
    virtual bool start(const std::shared_ptr<ExecutionMonitor>& monitor) override;
    virtual bool stop() override;

public:
    virtual void pushImplicitPendingCompletion(int count);
    virtual void popImplicitPendingCompletion(int count);

public:
    virtual bool isCompleted();
    virtual bool isRunning();

public:
    KernelITC::DomainError listen(uint32 source, const UUID& id, const std::string& endpoint);
    KernelITC::DomainError connect(uint32 continuation_id, uint32 source, const UUID& id, const std::string& endpoint);
    KernelITC::DomainError cancel(const UUID& id);

private:
    template<unsigned version>
    KernelITC::DomainError listenImplTcp(uint32 source, const UUID& id, const boost::optional<std::uint16_t>& port);
    KernelITC::DomainError listenImplWs (uint32 source, const UUID& id, const boost::optional<std::uint16_t>& port);

    template<unsigned version>
    KernelITC::DomainError connectImplTcp(uint32 continuation_id, uint32 source, const UUID& id, const std::string& address, const boost::optional<std::uint16_t>& port);

    KernelITC::DomainError cancelImplListen(const UUID& id);
    KernelITC::DomainError cancelImplConnect(const UUID& id);

public:
    void handleAccepted(uint32 source, const UUID& id, network::session::TCPSession* session, const boost::system::error_code& ec);
    void handleConnected(uint32 continuation_id, uint32 source, const UUID& id, network::session::TCPSession* session, const boost::system::error_code& ec);

public:
    void handleSequenceMessage(uint32 source, const UUID& id, DomainContext* context, const boost::system::error_code& ec, std::size_t bytes_transferred);
    void handleInvocationMessage(uint32 source, const UUID& id, DomainContext* context, const boost::system::error_code& ec, std::size_t bytes_transferred);

public:
    void handleInvocationITC(uint32 source, KernelITC& itc);

    void handleITCDomainListen (uint32 source, KernelITC& request_itc);
    void handleITCDomainConnect(uint32 source, KernelITC& request_itc);
    void handleITCDomainCancel (uint32 source, KernelITC& request_itc);

public:
    // FIXME:
    // This function definition should be in implementation file, but because of a clang bug,
    // which is fixed in trunk (http://llvm.org/bugs/show_bug.cgi?id=18408), it would need to
    // be in header for now.
    DomainContext* createDomainContext(network::session::TCPSession* session, WebSocketGateway::connection_ptr con_ptr)
    {
        DomainContext* context = new DomainContext(session);

        // allocate a session it mapping here
        int32 local_id = mSessionMap.allocate();
        mSessionMap[local_id] = context;
        int64 session_id = getId(); session_id <<= 32; session_id |= local_id;
        context->session_id = session_id;
        context->connection_ptr = con_ptr;

        return context;
    }
    tbb::concurrent_queue<KernelITC>& getSessionITCs() { return mSessionITCs; }

private:
    std::atomic<int> mImplicitPendingCompletion;
    UUID mListenerId;
    std::unique_ptr<network::session::TCPSessionEngine> mSessionEngine;
    tbb::concurrent_queue<KernelITC> mSessionITCs;
    std::mutex mIdSessionMapMutex;
    boost::bimaps::bimap<UUID, boost::bimaps::multiset_of<network::session::TCPSession*>> mIdSessionMap;
    SimpleSlotAllocator<int32, DomainContext*, 65536, true> mSessionMap;

    shared_ptr<KernelBase> mKernel;
    WebSocketGateway       mWebSocketGateway;
};

} } }

#endif /* ZILLIANS_FRAMEWORK_PROCESSOR_PROCESSORGATEWAY_H_ */
