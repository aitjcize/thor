/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSOR_WEBSOCKET_WEBSOCKETGATEWAYHANDLER_H_
#define ZILLIANS_FRAMEWORK_PROCESSOR_WEBSOCKET_WEBSOCKETGATEWAYHANDLER_H_

#include <cstdint>
#include <map>

#include <boost/optional/optional.hpp>

#include <websocketpp/websocketpp.hpp>

#include "core/IntTypes.h"

#include "utility/UUIDUtil.h"

namespace zillians { namespace framework { namespace processor {

class ProcessorGateway;
class DomainContext;

class WebSocketGateway
{
public:
    using connection_ptr = websocketpp::server::handler::connection_ptr;
    using message_ptr    = websocketpp::server::handler::message_ptr;

public:
    explicit WebSocketGateway(ProcessorGateway* proc_gateway);

    bool is_started() noexcept;
    bool start(uint32 new_source, const UUID& new_id, std::uint16_t port);
    bool stop();

private:
    void on_open(websocketpp::server::handler::connection_ptr con);
    void on_close(websocketpp::server::handler::connection_ptr con);
    void on_message(websocketpp::server::handler::connection_ptr con, websocketpp::server::handler::message_ptr msg);

private:
    uint32 source;
    UUID   id;

    boost::optional<websocketpp::server> server;
    websocketpp::server::handler_ptr     handler;

    ProcessorGateway*                        proc_gateway;
    std::map<connection_ptr, DomainContext*> con_domain_map;
};

} } }

#endif /* ZILLIANS_FRAMEWORK_PROCESSOR_WEBSOCKET_WEBSOCKETGATEWAYHANDLER_H_ */
