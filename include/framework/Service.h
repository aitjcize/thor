/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_SERVICE_H_
#define ZILLIANS_FRAMEWORK_SERVICE_H_

#include "core/Prerequisite.h"
#include "core/ContextHub.h"
#include "framework/Kernel.h"
#include "framework/service/Common.h"

#include <ext/hash_map>

#ifdef __PLATFORM_LINUX__
namespace __gnu_cxx
{
    template<>
    struct hash<std::pair<zillians::uint32, zillians::uint64> >
    {
        size_t operator()(const std::pair<zillians::uint32, zillians::uint64>& __x) const
        {
            return ((zillians::uint64)__x.first << 32) | __x.second;
        }
    };
}
#endif

namespace zillians { namespace framework {

class Processor;

struct Service : public ContextHub<ContextOwnership::transfer>
{
    virtual ~Service() = 0;
    virtual bool isCompleted() = 0;
    virtual bool isInvocationPending() = 0;
    virtual void initialize() = 0;
    virtual void finalize() = 0;
    virtual void beforeInitializeOnKernel(shared_ptr<KernelBase> kernel);
    virtual void afterInitializeOnKernel(shared_ptr<KernelBase> kernel);
    virtual void beforeFinalizeOnKernel(shared_ptr<KernelBase> kernel);
    virtual void afterFinalizeOnKernel(shared_ptr<KernelBase> kernel);
    virtual void* getInternalBufferDataPtr();
    virtual void* getInternalBufferPtr();

    struct events
    {
        enum type
        {
            idletime_heartbeat,
            before_invocation_dispatch,
            after_invocation_dispatch,
        };
    };

    virtual void handleEvent(events::type event) = 0;
};

} }

// 2011-05-27: add by maple - for information
#ifdef DEBUG_VW_EVENT_HANDLER
    #define DEBUG_VW_EVENT_HANDLER_BEGIN( svc_name ) {}
    #define DEBUG_VW_EVENT_HANDLER_END( svc_name ) {}
#else
    #include <tbb/tick_count.h>
    #define DEBUG_VW_EVENT_HANDLER_BEGIN( svc_name ) \
        tbb::tick_count time_before = tbb::tick_count::now();
    #define DEBUG_VW_EVENT_HANDLER_END( svc_name ) \
        { tbb::tick_count time_after = tbb::tick_count::now(); \
        if(((time_after-time_before).seconds()*1000) > 2) \
        {std::cout << "ServiceName " QUOTEME(svc_name) "- launch finished - spend "<< ((time_after-time_before).seconds()*1000) << " millisecond "<<std::endl; }}
#endif

#endif /* ZILLIANS_FRAMEWORK_SERVICE_H_ */
