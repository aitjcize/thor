/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_EXECUTIONITC_H_
#define ZILLIANS_FRAMEWORK_EXECUTIONITC_H_

#include <unordered_map>
#include <utility>

#include <boost/function.hpp>
#include <boost/none_t.hpp>
#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/seq/cat.hpp>
#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/enum.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/seq/transform.hpp>
#include <boost/variant/get.hpp>
#include <boost/variant/variant.hpp>

#include "core/Prerequisite.h"
#include "core/Hash.h"

#include "utility/UUIDUtil.h"

#include "threading/Dispatcher.h"

#include "framework/buffer/Common.h"
#include "framework/service/runtime/RuntimeServiceBuffer.h"

#include "thor/lang/Domain.h"

using namespace zillians::framework::buffer;
using namespace zillians::threading;

namespace zillians { namespace framework {

struct KernelITC
{
#define KERNEL_ITC_TYPES                                                                       \
    /* ITC prefix     -  ITC name                       <structure-name>                */     \
    (((processor_itc)(_)(invocation_request          ))(InvocationRequest               ))     \
    (((processor_itc)(_)(invocation_response         ))(InvocationResponse              ))     \
    (((   domain_itc)(_)(listen_request              ))(DomainListenRequest             ))     \
    (((   domain_itc)(_)(listen_response             ))(DomainListenResponse            ))     \
    (((   domain_itc)(_)(connect_request             ))(DomainConnectRequest            ))     \
    (((   domain_itc)(_)(connect_response            ))(DomainConnectResponse           ))     \
    (((   domain_itc)(_)(cancel_request              ))(DomainCancelRequest             ))     \
    (((   domain_itc)(_)(cancel_response             ))(DomainCancelResponse            ))     \
    (((   domain_itc)(_)(signal_connected_request    ))(DomainSignalConnectedRequest    ))     \
    (((   domain_itc)(_)(signal_connected_response   ))(DomainSignalConnectedResponse   ))     \
    (((   domain_itc)(_)(signal_disconnected_request ))(DomainSignalDisconnectedRequest ))     \
    (((   domain_itc)(_)(signal_disconnected_response))(DomainSignalDisconnectedResponse))

#define KERNEL_ITC_ENUM_NAMES_IMPL(r, _, elem)          BOOST_PP_SEQ_CAT(BOOST_PP_SEQ_ELEM(0, elem))
#define KERNEL_ITC_ENUM_NAMES                           BOOST_PP_SEQ_TRANSFORM(KERNEL_ITC_ENUM_NAMES_IMPL, _, KERNEL_ITC_TYPES)

#define KERNEL_ITC_STRUCT_NAMES_IMPL(r, _, elem)        BOOST_PP_SEQ_ELEM(1, elem)
#define KERNEL_ITC_STRUCT_NAMES                         BOOST_PP_SEQ_TRANSFORM(KERNEL_ITC_STRUCT_NAMES_IMPL, _, KERNEL_ITC_TYPES)

#define KERNEL_ITC_STRUCT_FWD_DECL_IMPL(r, _, elem)     struct elem;
#define KERNEL_ITC_STRUCT_GETTER_DEF_IMPL(r, _, elem)   elem& BOOST_PP_CAT(get, elem)() { return boost::get<elem>(itc_variant); }
#define KERNEL_ITC_STRUCT_IS_DEF_IMPL(r, _, elem)       bool BOOST_PP_CAT(is, KERNEL_ITC_STRUCT_NAMES_IMPL(, , elem))() { return KERNEL_ITC_ENUM_NAMES_IMPL(, , elem) == itc_variant.which(); }

    BOOST_PP_SEQ_FOR_EACH(KERNEL_ITC_STRUCT_FWD_DECL_IMPL, _, KERNEL_ITC_STRUCT_NAMES)

    using VariantType = boost::variant<
        BOOST_PP_SEQ_ENUM(KERNEL_ITC_STRUCT_NAMES),
        boost::none_t
    >;

    enum type : uint32
    {
        BOOST_PP_SEQ_ENUM(KERNEL_ITC_ENUM_NAMES)
    };

    struct InvocationRequest
    {
        int64 session_id;
        service::InvocationRequestBuffer* buffer;
    };

    struct InvocationResponse
    {
        service::InvocationResponseBuffer buffer;
    };

    enum DomainError
    {
        OK                      = static_cast<int64>(::thor::lang::DomainError::OK                     ),
        OPERATION_CANCELED      = static_cast<int64>(::thor::lang::DomainError::OPERATION_CANCELED     ),
        INVALID_ENDPOINT_SPEC   = static_cast<int64>(::thor::lang::DomainError::INVALID_ENDPOINT_SPEC  ),
        UNSUPPORTED_TRANSPORT   = static_cast<int64>(::thor::lang::DomainError::UNSUPPORTED_TRANSPORT  ),
        EXCEED_CONCURRENT_LIMIT = static_cast<int64>(::thor::lang::DomainError::EXCEED_CONCURRENT_LIMIT),
        NO_SUCH_REQUEST         = static_cast<int64>(::thor::lang::DomainError::NO_SUCH_REQUEST        ),
        UNKNOWN_ERROR           = static_cast<int64>(::thor::lang::DomainError::UNKNOWN_ERROR          ),
    };

    struct DomainListenRequest   { UUID id; std::string endpoint  ; };
    struct DomainListenResponse  { UUID id; DomainError error     ; };
    struct DomainConnectRequest  { UUID id; std::string endpoint  ; };
    struct DomainConnectResponse { UUID id; DomainError error     ; };
    struct DomainCancelRequest   { UUID id;                         };
    struct DomainCancelResponse  { UUID id; DomainError error     ; };

    struct DomainSignalConnectedRequest     { UUID id; int64 session_id; };
    struct DomainSignalConnectedResponse    {                            }; // not used
    struct DomainSignalDisconnectedRequest  { UUID id; int64 session_id; };
    struct DomainSignalDisconnectedResponse {                            }; // not used

    inline static bool is_request(const KernelITC::type type) noexcept { return !is_response(type); }
    inline static bool is_response(const KernelITC::type type) noexcept { return type & 0x1; }

    inline bool is_request() const noexcept { return is_request(get_type()); }
    inline bool is_response() const noexcept { return is_response(get_type()); }

    KernelITC()
        : continuation_id(0)
        , itc_variant(boost::none)
    {
    }

private:
    template<typename ITCType, typename... ArgTypes>
    explicit KernelITC(ITCType*, uint32 continuation_id, ArgTypes&&... args)
        : continuation_id(continuation_id)
        , itc_variant(ITCType{std::forward<ArgTypes>(args)...})
    {
    }

public:
    template<typename ITCType, typename... ArgTypes>
    static KernelITC make(uint32 continuation_id, ArgTypes&&... args)
    {
        return KernelITC(
            static_cast<ITCType*>(nullptr),
            continuation_id,
            std::forward<ArgTypes>(args)...
        );
    }

    void destroy();

    KernelITC::type get_type() const noexcept { return static_cast<KernelITC::type>(itc_variant.which()); }

    BOOST_PP_SEQ_FOR_EACH(KERNEL_ITC_STRUCT_GETTER_DEF_IMPL, _, KERNEL_ITC_STRUCT_NAMES)
    BOOST_PP_SEQ_FOR_EACH(KERNEL_ITC_STRUCT_IS_DEF_IMPL    , _, KERNEL_ITC_TYPES       )

    int64 get_session_id() const;

    uint32      continuation_id;
    VariantType itc_variant;

#undef KERNEL_ITC_STRUCT_IS_DEF_IMPL
#undef KERNEL_ITC_STRUCT_GETTER_DEF_IMPL
#undef KERNEL_ITC_STRUCT_FWD_DECL_IMPL
#undef KERNEL_ITC_STRUCT_NAMES
#undef KERNEL_ITC_STRUCT_NAMES_IMPL
#undef KERNEL_ITC_ENUM_NAMES
#undef KERNEL_ITC_ENUM_NAMES_IMPL
#undef KERNEL_ITC_TYPES
};

class ExecutionITC
{
public:
    typedef boost::function< void(uint32 source, KernelITC& itc) > kernel_itc_handler_t;

public:
    ExecutionITC(uint32 local_target_id, shared_ptr<DispatcherThreadContext<KernelITC> > ctx);
    virtual ~ExecutionITC();

public:
    bool process(bool blocking, bool one_or_all = false);

public:
    void registerHandler(KernelITC::type type, kernel_itc_handler_t request_handler);
    void unregisterHandler(KernelITC::type type);

public:
    void dispatchWithContiuation(uint32 target_id, KernelITC& itc, kernel_itc_handler_t response_handler);
    void dispatch(uint32 target_id, KernelITC& itc);

private:
    void dispatchKernelITC(uint32 source_id, KernelITC& itc);

private:
    uint32 mLocalTargetId;
    shared_ptr<DispatcherThreadContext<KernelITC> > mDispatcherContext;

    std::unordered_map< uint32, kernel_itc_handler_t > mHandlers;
    std::unordered_map< uint32, kernel_itc_handler_t > mContinuations;
    std::unordered_map< uint32, shared_ptr<DispatcherDestination<KernelITC> > > mTargets;

    uint32 mCurrentContinuationId;

private:
    static log4cxx::LoggerPtr mLogger;
};

} }

#endif /* ZILLIANS_FRAMEWORK_EXECUTIONITC_H_ */
