/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_RANGE_UTIL_SET_RANGE_H_
#define ZILLIANS_RANGE_UTIL_SET_RANGE_H_

#include <type_traits>
#include <utility>

#include <boost/blank.hpp>
#include <boost/iterator/iterator_adaptor.hpp>
#include <boost/iterator/iterator_categories.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/preprocessor/cat.hpp>

namespace zillians {

namespace detail {
 
template<typename type, typename value_compare_type, typename impl_type>
struct set_adaptor_traits {
    using container_type  = type;
 
    using traversal       = boost::forward_traversal_tag;
    using iterator        = typename type::iterator;
    using reference       = typename type::iterator::reference;
    using difference_type = typename type::iterator::difference_type;
    using value_type      = typename std::remove_reference<reference>::type;
    using base_type       = std::pair<iterator, iterator>;
    using value_compare   = typename std::conditional<std::is_same<value_compare_type, boost::use_default>::value, typename type::value_compare, value_compare_type>::type;
 
    using adaptor_type    = boost::iterator_adaptor<
        impl_type      ,
        base_type      ,
        value_type     ,
        traversal      ,
        reference      ,
        difference_type
    >;
};
 
struct set_union {
    using state = boost::tribool;
 
    template<typename iterator>
    static const iterator &get(const state &use_first, const iterator &first_i, const iterator &second_i)
    {
        if (use_first)
            return first_i;
        else
            return second_i;
    }
 
    template<typename iterator, typename comparator_type>
    static state compute_state(iterator &first_i, iterator &second_i, const iterator &first_end, const iterator &second_end, const comparator_type &comparator)
    {
        if (first_i == first_end)
            return false;
        else if (second_i == second_end)
            return true;
        else if (comparator(*first_i, *second_i))
            return true;
        else if (comparator(*second_i, *first_i))
            return false;
        else
            return boost::indeterminate;
    }
 
    template<typename iterator, typename comparator_type>
    static void increment(state &use_first, iterator &first_i, iterator &second_i, const iterator &first_end, const iterator &second_end, const comparator_type &comparator)
    {
        if (use_first)
            ++ first_i;
        else if (!use_first)
            ++second_i;
        else
            ++ first_i,
            ++second_i;
 
        use_first = compute_state(first_i, second_i, first_end, second_end, comparator);
    }
};
 
struct set_intersection {
    using state = boost::blank;
 
    template<typename iterator>
    static const iterator &get(const state &, const iterator &first_i, const iterator &)
    {
        return first_i;
    }
 
    template<typename iterator, typename comparator_type>
    static state compute_state(iterator &first_i, iterator &second_i, const iterator &first_end, const iterator &second_end, const comparator_type &comparator)
    {
        for (; first_i != first_end && second_i != second_end; )
        {
            if (comparator(*first_i, *second_i))
                ++first_i;
            else if (comparator(*second_i, *first_i))
                ++second_i;
            else
                return state();
        }
 
        if (first_i == first_end)
            return (second_i = second_end), state();
        else
            return ( first_i =  first_end), state();
    }
 
    template<typename iterator, typename comparator_type>
    static void increment(const state &, iterator &first_i, iterator &second_i, const iterator &first_end, const iterator &second_end, const comparator_type &comparator)
    {
        ++ first_i,
        ++second_i;
 
        compute_state(first_i, second_i, first_end, second_end, comparator);
    }
};

struct set_difference {
    using state = boost::blank;

    template<typename iterator>
    static const iterator &get(const state &, const iterator &first_i, const iterator &)
    {
        return first_i;
    }

    template<typename iterator, typename comparator_type>
    static state compute_state(iterator &first_i, iterator &second_i, const iterator &first_end, const iterator &second_end, const comparator_type &comparator)
    {
        while (first_i != first_end && second_i != second_end)
        {
            // 'first   < second' => 'first'
            // 'second  < first ' => try next second
            // 'first  == second' => next first/second
            if (comparator(*first_i, *second_i))
                return state();
            else if (comparator(*second_i, *first_i))
                ++second_i;
            else
                ++first_i, ++second_i;
        }

        if (first_i == first_end)
            second_i = second_end;

        return state();
    }

    template<typename iterator, typename comparator_type>
    static void increment(const state &, iterator &first_i, iterator &second_i, const iterator &first_end, const iterator &second_end, const comparator_type &comparator)
    {
        ++first_i;

        compute_state(first_i, second_i, first_end, second_end, comparator);
    }
};
 
template<typename type, typename value_compare_type, typename determiner_type>
class set_iterator_impl : public set_adaptor_traits<type, value_compare_type, set_iterator_impl<type, value_compare_type, determiner_type>>::adaptor_type {
public:
    using traits_type  = set_adaptor_traits<type, value_compare_type, set_iterator_impl<type, value_compare_type, determiner_type>>;
 
    using iterator        = typename traits_type::iterator       ;
    using reference       = typename traits_type::reference      ;
    using difference_type = typename traits_type::difference_type;
    using value_type      = typename traits_type::value_type     ;
    using base_type       = typename traits_type::base_type      ;
    using value_compare   = typename traits_type::value_compare  ;
    using state_type      = typename determiner_type::state      ;
 
    set_iterator_impl(iterator first_i, iterator second_i, iterator first_end, iterator second_end, value_compare comp = value_compare())
        : traits_type::adaptor_type({std::move(first_i), std::move(second_i)})
        , state()
        , end(std::move(first_end), std::move(second_end))
        , comp(std::move(comp))
    {
        auto &  pair_i_local = this->base_reference();
        auto & first_i_local = std::get<0>(pair_i_local);
        auto &second_i_local = std::get<1>(pair_i_local);
 
        const auto & first_end_local = std::get<0>(end);
        const auto &second_end_local = std::get<1>(end);
 
        state = determiner_type::compute_state(first_i_local, second_i_local, first_end_local, second_end_local, comp);
    }
 
    reference dereference() const
    {
        const auto &  pair_i = this->base_reference();
        const auto & first_i = std::get<0>(pair_i);
        const auto &second_i = std::get<1>(pair_i);
 
        return *determiner_type::get(state, first_i, second_i);
    }
 
    void increment()
    {
        auto &  pair_i = this->base_reference();
        auto & first_i = std::get<0>(pair_i);
        auto &second_i = std::get<1>(pair_i);
 
        const auto & first_end = std::get<0>(end);
        const auto &second_end = std::get<1>(end);
 
        determiner_type::increment(state, first_i, second_i, first_end, second_end, comp);
    }
 
private:
    state_type     state;
    base_type      end;
    value_compare  comp;
 
};
 
}

#define ZILLIANS_SET_ITERATOR_POLICY(operation)                \
    detail:: BOOST_PP_CAT(set_, operation)

#define ZILLIANS_SET_ITERATOR_NAME(operation)                  \
    BOOST_PP_CAT(set_, BOOST_PP_CAT(operation, _iterator))

#define ZILLIANS_SET_RANGE_NAME(operation)                     \
    BOOST_PP_CAT(make_set_, BOOST_PP_CAT(operation, _range))

#define ZILLIANS_SET_ITERATOR_DEF(operation)                                                       \
    template<typename type, typename value_compare_type = boost::use_default>                      \
    using ZILLIANS_SET_ITERATOR_NAME(operation) = detail::set_iterator_impl<                       \
        type,                                                                                      \
        value_compare_type,                                                                        \
        ZILLIANS_SET_ITERATOR_POLICY(operation)                                                    \
    >;                                                                                             \
    template<typename type>                                                                        \
    inline boost::iterator_range<ZILLIANS_SET_ITERATOR_NAME(operation)<type>>                      \
    ZILLIANS_SET_RANGE_NAME(operation)(const type &first, const type &second)                      \
    {                                                                                              \
        return {                                                                                   \
            ZILLIANS_SET_ITERATOR_NAME(operation)<type>(                                           \
                first.begin(), second.begin(),                                                     \
                first.end  (), second.end  (),                                                     \
                first.value_comp()                                                                 \
            ),                                                                                     \
            ZILLIANS_SET_ITERATOR_NAME(operation)<type>(                                           \
                first.end  (), second.end  (),                                                     \
                first.end  (), second.end  (),                                                     \
                first.value_comp()                                                                 \
            )                                                                                      \
        };                                                                                         \
    }

ZILLIANS_SET_ITERATOR_DEF(union       )
ZILLIANS_SET_ITERATOR_DEF(intersection)
ZILLIANS_SET_ITERATOR_DEF(difference  )

#undef ZILLIANS_SET_ITERATOR_DEF
#undef ZILLIANS_SET_RANGE_NAME
#undef ZILLIANS_SET_ITERATOR_NAME
#undef ZILLIANS_SET_ITERATOR_POLICY

}

#endif/*ZILLIANS_RANGE_UTIL_SET_RANGE_H_*/
