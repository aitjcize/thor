/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_BITTRICKUTIL_H_
#define ZILLIANS_BITTRICKUTIL_H_

#include "core/Types.h"

namespace zillians {

/**
 * Round-up helper function to calculate round-up value
 * @param v the value to round-up
 * @param m the multiple of rounding
 * @return the rounded value (upward)
 */
template<typename Value, typename Multiple>
inline static Value round_up_to_nearest_power(const Value& v, const Multiple& m)
{
    if(v == 0 || m == 0 || v % m == 0)
        return v;
    else
        return (v / m + 1) * m;
}

template<typename T>
struct round_up_to_nearest_power_of_two;

template<>
struct round_up_to_nearest_power_of_two<uint32>
{
    static uint32 apply(uint32 v)
    {
        v--;
        v |= v >> 1;
        v |= v >> 2;
        v |= v >> 4;
        v |= v >> 8;
        v |= v >> 16;
        v++;
        return v;
    }
};

template<>
struct round_up_to_nearest_power_of_two<uint64>
{
    static uint64 apply(uint64 v)
    {
        v--;
        v |= v >> 1;
        v |= v >> 2;
        v |= v >> 4;
        v |= v >> 8;
        v |= v >> 16;
        v |= v >> 32;
        v++;
        return v;
    }
};

}

#endif /* ZILLIANS_BITTRICKUTIL_H_ */
