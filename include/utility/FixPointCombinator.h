/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "core/Prerequisite.h"

namespace zillians { namespace detail {

//
// the following fix-point-combinator trick is found at
// http://codereview.stackexchange.com/questions/3111/my-implementation-of-fixed-point-combinator-in-c1x-compiled-under-vistual-stu
//
// and I slightly modify it to get it compile on gcc and accept both std::function and boost::function
//
template<typename R, typename... V>
struct fixpoint_std
{
    typedef std::function<R (V...)> func_t;
    typedef std::function<func_t (func_t)> tfunc_t;
    typedef std::function<func_t (tfunc_t)> yfunc_t;

    class loopfunc_t {
    public:
        func_t operator()(loopfunc_t v)const {
            return func(v);
        }
        template<typename L>
        loopfunc_t(const L &l):func(l){}
        loopfunc_t(){}
    private:
        std::function<func_t (loopfunc_t)> func;
    };
    static func_t Fix(tfunc_t f){
        return [](loopfunc_t x){ return x(x); }
        ([f](loopfunc_t x){ return [f, x](V... v){ return f(x(x))(v...); }; });
    }
};

template<typename R, typename... V>
struct fixpoint_boost
{
    typedef boost::function<R (V...)> func_t;
    typedef boost::function<func_t (func_t)> tfunc_t;
    typedef boost::function<func_t (tfunc_t)> yfunc_t;

    class loopfunc_t {
    public:
        func_t operator()(loopfunc_t v)const {
            return func(v);
        }
        template<typename L>
        loopfunc_t(const L &l):func(l){}
        loopfunc_t(){}
    private:
        boost::function<func_t (loopfunc_t)> func;
    };
    static func_t Fix(tfunc_t f){
        return [](loopfunc_t x){ return x(x); }
        ([f](loopfunc_t x){ return [f, x](V... v){ return f(x(x))(v...); }; });
    }
};

template<typename T>
struct getfixpoint
{
    typedef typename getfixpoint<decltype(&T::operator())>::fp fp;
};

template<typename R, typename T, typename... V>
struct getfixpoint<std::function<R (V...)> (T::*)(std::function<R (V...)>)>
{
    typedef fixpoint_std<R, V...> fp;
};
template<typename R, typename T, typename... V>
struct getfixpoint<std::function<R (V...)> (T::*)(std::function<R (V...)>)const>
{
    typedef fixpoint_std<R, V...> fp;
};
template<typename R, typename... V>
struct getfixpoint<std::function<R (V...)> (*)(std::function<R (V...)>)>
{
    typedef fixpoint_std<R, V...> fp;
};

template<typename R, typename T, typename... V>
struct getfixpoint<boost::function<R (V...)> (T::*)(boost::function<R (V...)>)>
{
    typedef fixpoint_boost<R, V...> fp;
};
template<typename R, typename T, typename... V>
struct getfixpoint<boost::function<R (V...)> (T::*)(boost::function<R (V...)>)const>
{
    typedef fixpoint_boost<R, V...> fp;
};
template<typename R, typename... V>
struct getfixpoint<boost::function<R (V...)> (*)(boost::function<R (V...)>)>
{
    typedef fixpoint_boost<R, V...> fp;
};

}

template<typename T>
typename detail::getfixpoint<T>::fp::func_t ycombine(T f) {
    return detail::getfixpoint<T>::fp::Fix(f);
}

}
