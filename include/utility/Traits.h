/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_TRAITS_H_
#define ZILLIANS_TRAITS_H_

#include <type_traits>

namespace zillians { namespace traits {

namespace detail {

template<typename T>
static std::false_type has_begin_end_eval(...);

template<typename T>
static std::true_type  has_begin_end_eval(
    typename std::enable_if<
        std::is_same<
            decltype(static_cast<typename T::const_iterator (T::*)() const>(&T::begin)),
                                 typename T::const_iterator (T::*)() const
        >::value &&
        std::is_same<
            decltype(static_cast<typename T::const_iterator (T::*)() const>(&T::end  )),
                                 typename T::const_iterator (T::*)() const
        >::value
    >::type*
);

// It's undefined behavior: sizeof(void)
template<typename T, typename std::enable_if<!std::is_same<void, T>::value>::type * = nullptr>
static std::true_type is_complete_type_eval(typename std::enable_if<(sizeof(T) > 0)>::type*);

template<typename T>
static std::false_type is_complete_type_eval(...);

}

template<typename T>
struct has_begin_end : decltype(detail::has_begin_end_eval<T>(0)) {};

template<typename SameOrBase, typename T>
struct is_same_or_base_of : std::integral_constant<
    bool,
    std::is_same   <SameOrBase, typename std::remove_cv<T>::type>::value ||
    std::is_base_of<SameOrBase, typename std::remove_cv<T>::type>::value
> {};

namespace {

template<typename T>
struct is_complete_type : decltype(detail::is_complete_type_eval<T>(0)) {};

}

} }

#endif /* ZILLIANS_TRAITS_H_ */
