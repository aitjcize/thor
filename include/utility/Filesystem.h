/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FILESYSTEM_H_
#define ZILLIANS_FILESYSTEM_H_

#include <boost/filesystem/path.hpp>

#include <string>
#include <vector>
#include <deque>

namespace zillians {

struct Filesystem
{
private:
    Filesystem() { }
    ~Filesystem() { }

public:
    // see: http://stackoverflow.com/questions/1746136/how-do-i-normalize-a-pathname-using-boostfilesystem/1750710#1750710
    static boost::filesystem::path resolve(const boost::filesystem::path& p);
    static bool enumerate_package(const boost::filesystem::path& root, const boost::filesystem::path& p, std::deque<std::wstring>& sequence);
    static boost::filesystem::path normalize_path(boost::filesystem::path p);
    static boost::filesystem::path current_executable_path();
    static boost::filesystem::path path_difference(const boost::filesystem::path& base_path, const boost::filesystem::path& sub_path);
    static std::vector<boost::filesystem::path> collect_files(const boost::filesystem::path& p, const std::string& extension, bool recursively = false);
    static boost::filesystem::path find_program_by_name(const std::string& program_name);

    static bool is_directory(const boost::filesystem::path& p);
    static bool is_regular_file(const boost::filesystem::path& p);
    static bool have_different_timestamps(const boost::filesystem::path& a, const boost::filesystem::path& b);
    static bool have_different_size(const boost::filesystem::path& a, const boost::filesystem::path& b);
    static bool is_different_files(const boost::filesystem::path& a, const boost::filesystem::path& b);
    static bool copy_file_if_differnt(const boost::filesystem::path& src, const boost::filesystem::path& dest);

}; // class Filesystem

} // namespace zillians

#endif /* ZILLIANS_FILESYSTEM_H_ */
