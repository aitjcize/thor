/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FUNCTIONAL_H_
#define ZILLIANS_FUNCTIONAL_H_

#include <type_traits>
#include <utility>

namespace zillians {

    struct not_null
    {
        using result_type = bool;

        template<typename value_type>
        result_type operator()(const value_type& ptr) const noexcept(noexcept(ptr != nullptr))
        {
            return ptr != nullptr;
        }
    };

    template < typename Callee >
    class invoke_on_destroy_t
    {
        static_assert(!std::is_reference<Callee>::value, "reference will not effect the temporary object lifetime, so we expect non-reference value");

        Callee callee;
    public:
        template<typename CalleeArg>
        explicit invoke_on_destroy_t(CalleeArg&& callee)
        : callee(std::forward<CalleeArg>(callee))
        { }

        ~invoke_on_destroy_t()
        { callee(); }
    };

    template < typename Callee >
    invoke_on_destroy_t<typename std::remove_cv<typename std::remove_reference<Callee>::type>::type>
    invoke_on_destroy(Callee&& callee)
    {
        return invoke_on_destroy_t<typename std::remove_cv<typename std::remove_reference<Callee>::type>::type>(std::forward<Callee>(callee));
    }
}

#endif
