/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_EXPRESSIONPARSER_H_
#define ZILLIANS_EXPRESSIONPARSER_H_

#include <boost/spirit/include/classic_core.hpp>
#include <boost/spirit/include/classic_attribute.hpp>
#include <iostream>
#include <string>

////////////////////////////////////////////////////////////////////////////
using namespace boost::spirit::classic;
using namespace phoenix;

namespace zillians {

namespace {

template<typename T>
struct CalculatorClosure : boost::spirit::classic::closure<CalculatorClosure<T>, T>
{
    typename boost::spirit::classic::closure<CalculatorClosure<T>, T>::member1 val;
};

template<typename T>
struct Calculator : public grammar<Calculator<T>, typename CalculatorClosure<T>::context_t>
{
    template <typename ScannerT>
    struct definition
    {
        definition(Calculator const& self)
        {
            top = expression[self.val = arg1];

            expression
                =   term[expression.val = arg1]
                    >> *(   ('+' >> term[expression.val += arg1])
                        |   ('-' >> term[expression.val -= arg1])
                        )
                ;

            term
                =   factor[term.val = arg1]
                    >> *(   ('*' >> factor[term.val *= arg1])
                        |   ('/' >> factor[term.val /= arg1])
                        )
                ;

            factor
                =   ureal_p[factor.val = arg1]
                |   '(' >> expression[factor.val = arg1] >> ')'
                |   ('-' >> factor[factor.val = -arg1])
                |   ('+' >> factor[factor.val = arg1])
                ;
        }

        typedef rule<ScannerT, typename CalculatorClosure<T>::context_t> rule_t;
        rule_t expression, term, factor;
        rule<ScannerT> top;

        rule<ScannerT> const&
        start() const { return top; }
    };
};

}

struct ExpressionParser
{
    template<typename T>
    static bool evaluate(const std::string& expression, T& value)
    {
        Calculator<T> calc;
        parse_info<> info = parse(expression.c_str(), calc[var(value) = arg1], space_p);
        if (info.full)
            return true;
        else
            return false;
    }
};

}

#endif /* ZILLIANS_EXPRESSIONPARSER_H_ */
