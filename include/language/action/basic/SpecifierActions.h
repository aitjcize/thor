/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_ACTION_BASIC_SPECIFIERACTIONS_H_
#define ZILLIANS_LANGUAGE_ACTION_BASIC_SPECIFIERACTIONS_H_

#include <tuple>
#include <type_traits>
#include <utility>

#include <boost/assert.hpp>

#include "language/action/detail/SemanticActionsDetail.h"

namespace zillians { namespace language { namespace action {

struct expr_init_specifier
{
    DEFINE_ATTRIBUTES(Expression*)
    DEFINE_LOCALS()
};

struct type_initializer_or_specifier
{
    typedef std::tuple<unsigned, TypeSpecifier*> wrapper_type;

    DEFINE_ATTRIBUTES(wrapper_type*)
    DEFINE_LOCALS()

    BEGIN_ACTION(initializer)
    {
        _result = new wrapper_type(0u, _param(0));
    }
    END_ACTION

    BEGIN_ACTION(specifier)
    {
        _result = new wrapper_type(1u, _param(0));
    }
    END_ACTION
};

struct type_specifier
{
    DEFINE_ATTRIBUTES(TypeSpecifier*)
    DEFINE_LOCALS()
};

struct multi_type_specifier_impl
{
    DEFINE_ATTRIBUTES(std::vector<TypeSpecifier*>)
    DEFINE_LOCALS()
};

struct arg_thor_type
{
    DEFINE_ATTRIBUTES(TypeSpecifier*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init_array_type)
    {
#ifdef DEBUG
        printf("arg_thor_type::init_array_type param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("arg_thor_type::init_array_type param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        TypeSpecifier* elem_type = _param(0);
        if(_param(1).is_initialized())
        {
            // determine dimension by int32[......]
            //                              ^^^^^^
            // []         => 1
            // [3333]     => 1
            // [1, 2, 3]  => 3
            // max is 11
            size_t dimension = 0;
            auto dimensions = boost::fusion::at_c<1>(*_param(1));
            if(dimensions.is_initialized())
            {
                //BOOST_ASSERT((*dimensions).size() == 1 && "Multi-dimension array not implemented yet.");
                dimension = (*dimensions).size();
            }
            else
            {
                dimension = 1;
            }

            // 1D Array
            // 2D Array2D
            // 3D Array3D
            // 4D Array4D
            // ...
            // 11D Array11D
            std::wstring array_ND_str;
            if(dimension == 1)
            {
                array_ND_str = L"Array";
            }
            else
            {
                array_ND_str = L"Array" + boost::lexical_cast<std::wstring>(dimension) + L"D";
            }

            // TODO error in dimension > 11

            // bulid thor.lang.Array<ElementType> identifier
            NestedIdentifier* nid = new NestedIdentifier(); BIND_CACHED_LOCATION(nid);
            SimpleIdentifier* thor = new SimpleIdentifier(L"thor" ); BIND_CACHED_LOCATION(thor ); nid->appendIdentifier(thor );
            SimpleIdentifier* lang = new SimpleIdentifier(L"lang" ); BIND_CACHED_LOCATION(lang ); nid->appendIdentifier(lang );

            SimpleIdentifier*    array  = new SimpleIdentifier(array_ND_str); BIND_CACHED_LOCATION(array);
            TemplatedIdentifier* arrayT = new TemplatedIdentifier(TemplatedIdentifier::Usage::ACTUAL_ARGUMENT, array); BIND_CACHED_LOCATION(arrayT);
            SimpleIdentifier* T = new SimpleIdentifier(L"_"); BIND_CACHED_LOCATION(T);
            TypenameDecl* tn_decl = new TypenameDecl(T, elem_type); BIND_CACHED_LOCATION(tn_decl);
            arrayT->append(tn_decl);
            nid->appendIdentifier(arrayT);

            NamedSpecifier* array_ts = new NamedSpecifier(nid);
            _result = array_ts;

        }
        else
        {
            _result = elem_type;
        }
    }
    END_ACTION
} ;

struct arg_thor_non_qualified_type
{
    DEFINE_ATTRIBUTES(TypeSpecifier*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init_type)
    {
#ifdef DEBUG
        printf("arg_thor_non_qualified_type::init_type param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        Identifier* ident = _param(0);
        BIND_CACHED_LOCATION(_result = new NamedSpecifier(ident));
    }
    END_ACTION

    BEGIN_TEMPLATED_ACTION(init_primitive_type, PrimitiveKind::kind Type)
    {
        BIND_CACHED_LOCATION(_result = new PrimitiveSpecifier(Type));
    }
    END_ACTION

    BEGIN_ACTION(init_function_type)
    {
#ifdef DEBUG
        printf("thor_type::init_function_type param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("thor_type::init_function_type param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        typedef std::vector<TypeSpecifier*> type_list_t;

        type_list_t&   parameters    = _param(0);
        TypeSpecifier* return_type   = _param(1).is_initialized() ? *_param(1) : NULL;

        BIND_CACHED_LOCATION(_result = new FunctionSpecifier(std::move(parameters), return_type, false));
    }
    END_ACTION

    BEGIN_ACTION(init_lambda_function_type)
    {
#ifdef DEBUG
        printf("thor_type::init_lambda_function_type param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("thor_type::init_lambda_function_type param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        typedef std::vector<TypeSpecifier*> type_list_t;

        type_list_t&   parameters    = _param(0);
        TypeSpecifier* return_type   = _param(1);

        BIND_CACHED_LOCATION(_result = new FunctionSpecifier(std::move(parameters), return_type, true));
    }
    END_ACTION
};

//typedef thor_type arg_thor_type;
//typedef thor_type param_thor_type;

struct type_list
{
    typedef std::vector<TypeSpecifier*> value_type;
    DEFINE_ATTRIBUTES(value_type)
    DEFINE_LOCALS()
};

typedef type_list type_arg_list;

struct class_member_visibility
{
    DEFINE_ATTRIBUTES(Declaration::VisibilitySpecifier::type)
    DEFINE_LOCALS()

    BEGIN_TEMPLATED_ACTION(init, Declaration::VisibilitySpecifier::type Type)
    {
        _result = Type;
    }
    END_ACTION
};

struct annotation_list
{
    DEFINE_ATTRIBUTES(Annotations*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("annotation_list param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        BIND_CACHED_LOCATION(_result = new Annotations());
        for(auto& annotation: _param(0))
            _result->appendAnnotation(annotation);
    }
    END_ACTION
};

struct annotation
{
    DEFINE_ATTRIBUTES(Annotation*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("annotation::init param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("annotation::init param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        SimpleIdentifier* ident = _param(0);
        if(_param(1).is_initialized())
        {
            _result = *_param(1);
            _result->setName(ident);
        }
        else
            BIND_CACHED_LOCATION(_result = new Annotation(ident));
    }
    END_ACTION
};

struct annotation_body
{
    DEFINE_ATTRIBUTES(Annotation*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("annotation_body::init param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        BIND_CACHED_LOCATION(_result = new Annotation(NULL));
        for(auto& i : _param(0))
        {
            SimpleIdentifier* key = boost::fusion::at_c<0>(i);
            ASTNode* value = NULL;
            switch(boost::fusion::at_c<1>(i).which())
            {
            case 0: value = boost::get<Expression*>(boost::fusion::at_c<1>(i)); break;
            case 1: value = boost::get<Annotation*>(boost::fusion::at_c<1>(i)); break;
            }
            _result->appendKeyValue(key, value);
        }
    }
    END_ACTION
};

} } }

#endif /* ZILLIANS_LANGUAGE_ACTION_BASIC_SPECIFIERACTIONS_H_ */
