/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_ACTION_BASIC_LITERALACTIONS_H_
#define ZILLIANS_LANGUAGE_ACTION_BASIC_LITERALACTIONS_H_

#include <algorithm>
#include <limits>
#include <boost/range/numeric.hpp>
#include <boost/range/adaptor/transformed.hpp>

#include "language/action/detail/SemanticActionsDetail.h"
#include "language/logging/StringTable.h"
#include "language/logging/LoggerWrapper.h"

namespace zillians { namespace language { namespace action {

struct integer_literal
{
    DEFINE_ATTRIBUTES(NumericLiteral*)
    DEFINE_LOCALS(LOCATION_TYPE)

    template<typename CharType>
    static int64 convert_digit(CharType d)
    {
        if(d >= CharType('0') && d <= CharType('9'))
            return static_cast<int64>(d - CharType('0'));

        else if(d >= CharType('a') && d <= CharType('f'))
            return static_cast<int64>(d - CharType('a')) + 10L;

        else if(d >= CharType('A') && d <= CharType('F'))
            return static_cast<int64>(d - CharType('A')) + 10L;
        
        return -1L;
    }

    //Adapt from clang:
    //Firstly do a rough but quick test if there is any possibility of overflow. If not, do the conversion without relatively slow overflow check.
    static bool is_fit_into_64bits(size_t ndigits, int64 radix)
    {
        switch(radix)
        {
            case 10:
                return ndigits <= 18; //clang is 19 because they have unsigned type.
            case 16:
                return ndigits <= 16;
        }
        UNREACHABLE_CODE();
        return false;
    }

    template<typename Sequence>
    static int64 convert_number(const Sequence& seq, int64 radix, bool& overflow_detected)
    {
        overflow_detected = false;
        bool fit_in_64bits = is_fit_into_64bits(seq.size(), radix);
        auto digit_seq = seq | boost::adaptors::transformed(convert_digit<typename Sequence::value_type>);
        std::function<int64 (int64, int64)> accum_func;

        if(fit_in_64bits)
        {
            accum_func = [radix](int64 accum_value, int64 next_digit){
                BOOST_ASSERT((next_digit >= 0 && next_digit < radix) && "Numeric integer parser falsely passed invalid character sequence! ");

                return accum_value * radix + next_digit;
            };
        }
        else
        {
            accum_func = [radix, &overflow_detected](int64 accum_value, int64 next_digit){
                BOOST_ASSERT((next_digit >=0 && next_digit < radix) && "Numeric integer parser falsely passed invalid character sequence! ");

                if(!overflow_detected)
                    if(accum_value > (INT64_MAX - next_digit) / radix)
                        overflow_detected = true;

                return accum_value * radix + next_digit;
            };
        }
        return boost::accumulate(
                digit_seq,
                static_cast<int64>(0L), 
                accum_func);
    }

    BEGIN_TEMPLATED_ACTION(init, int64 Radix)
    {
#ifdef DEBUG
        printf("integer_literal param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        auto sign = _param(0).is_initialized()? -1L : 1L;
        auto digit_sequence = _param(1);
        bool is_overflow = false;
        int64 val = convert_number(digit_sequence, Radix, is_overflow) * sign;
        if( !_param(2).is_initialized() && std::numeric_limits<int32>::max() >= val )
        {
            BIND_CACHED_LOCATION(_result = new NumericLiteral(PrimitiveKind::INT32_TYPE, val));
        }
        else
        {
            BIND_CACHED_LOCATION(_result = new NumericLiteral(PrimitiveKind::INT64_TYPE, val));
        }
        if( is_overflow )
        {
            auto result = _result;
            getParserContext().tree_actions.push_back([result](){
                LOG_MESSAGE(NUMERIC_LITERAL_OVERFLOW, result, _optional_filename(getParserContext().active_source->filename));
            });
        }
    }
    END_ACTION
};

struct float_literal
{
    DEFINE_ATTRIBUTES(NumericLiteral*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("float_literal param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        BIND_CACHED_LOCATION(_result = new NumericLiteral(PrimitiveKind::FLOAT64_TYPE, _param(0)));
    }
    END_ACTION

    BEGIN_ACTION(with_postfix_init)
    {
#ifdef DEBUG
        printf("float_literal param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        BIND_CACHED_LOCATION(_result = new NumericLiteral(PrimitiveKind::FLOAT32_TYPE, _param(0)));

        if( _param(0) < -std::numeric_limits<float>::max() || std::numeric_limits<float>::max() < _param(0) )
        {
            auto result = _result;
            getParserContext().tree_actions.push_back([result](){
                LOG_MESSAGE(IMPLICIT_CAST_PRECISION_LOSS, result, _optional_filename(getParserContext().active_source->filename));
            });
        }
    }
    END_ACTION
};

struct string_literal
{
    DEFINE_ATTRIBUTES(StringLiteral*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("string_literal param(0) type = %s\n", typeid(_param_t(0)).name());
#endif

        BIND_CACHED_LOCATION( _result = new StringLiteral(_param(0)) );

        // check the placeholder format in string literal
        auto& literal = _param(0);
        auto placeholder_begin = StringLiteral::getInternalPlaceholderBegin();

        auto found = std::find( literal.cbegin(), literal.cend(), placeholder_begin );
   
        while( found != literal.cend() )
        {
            typedef StringLiteral::string_type::const_iterator Iterator;
            std::pair<Iterator, Iterator> place;

            if( !StringLiteral::searchPlaceholder(found, literal.cend(), place) )
            {
                auto result = _result;
                getParserContext().tree_actions.push_back([result](){
                    LOG_MESSAGE(STRING_FORMAT_ERROR, result, _optional_filename(getParserContext().active_source->filename));
                });
                break;
            } 
    
            found = std::find( found+1, literal.cend(), placeholder_begin );
        }
    }
    END_ACTION
};

} } }

#endif /* ZILLIANS_LANGUAGE_ACTION_BASIC_LITERALACTIONS_H_ */
