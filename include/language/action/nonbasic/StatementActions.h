/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_ACTION_STATEMENT_STATEMENTACTIONS_H_
#define ZILLIANS_LANGUAGE_ACTION_STATEMENT_STATEMENTACTIONS_H_

#include "language/action/detail/SemanticActionsDetail.h"
#include "language/logging/StringTable.h"
#include "language/logging/LoggerWrapper.h"
#include "language/tree/UserDefinedLiteral.h"
#include "language/Architecture.h"

namespace zillians { namespace language { namespace action {

struct statement
{
    DEFINE_ATTRIBUTES(Statement*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("statement param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("statement param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        _result = _param(1);
        if(_result && _param(0).is_initialized())
        {
            BIND_CACHED_LOCATION(_result);

            Statement* attach_node = _result;

            if(ExpressionStmt* expr_stmt = cast<ExpressionStmt>(_result))
            {
                if(BlockExpr* block_expr = cast<BlockExpr>(expr_stmt->expr))
                {
                    BOOST_ASSERT(block_expr->block && "null pointer exception");

                    if(block_expr->tag == "parser_for")
                    {
                        BOOST_ASSERT(block_expr->block->objects.size() >= 1 && "must contain at least one statement");
                        BOOST_ASSERT(isa<ForStmt>(block_expr->block->objects.back()) && "ForStmt not found!");

                        attach_node = cast<ForStmt>(block_expr->block->objects.back());
                    }
                }
            }

            attach_node->setAnnotations(*_param(0));
        }
    }
    END_ACTION

    BEGIN_ACTION(init_block)
    {
#ifdef DEBUG
        printf("statement::init_block param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("statement::init_block param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        BlockExpr*      block_expr = new BlockExpr(_param(0));
        ExpressionStmt* expr_stmt  = new ExpressionStmt(block_expr);

        BIND_CACHED_LOCATION(block_expr);
        BIND_CACHED_LOCATION(expr_stmt);

        _result = expr_stmt;
    }
    END_ACTION

    BEGIN_ACTION(init_on_domain_block)
    {
#ifdef DEBUG
        printf("statement::init_on_domain_block param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        Block* container_block = new NormalBlock(); BIND_CACHED_LOCATION(container_block);
		BlockExpr* container_block_expr = new BlockExpr(container_block); BIND_CACHED_LOCATION(container_block);
		ExpressionStmt* container_expr_stmt = new ExpressionStmt(container_block_expr); BIND_CACHED_LOCATION(container_expr_stmt);

    	for(auto& i : _param(0))
		{
    		Architecture arch;

    		for(auto& j : boost::fusion::at_c<0>(i))
    		{
    			SimpleIdentifier* arch_name = j;

    			arch |= Architecture(arch_name->name);
    		}

    		Block* block = boost::fusion::at_c<1>(i); BIND_CACHED_LOCATION(block);
    		block->arch = arch;
    		BlockExpr* block_expr = new BlockExpr(block); BIND_CACHED_LOCATION(block_expr);
    		ExpressionStmt* expr_stmt = new ExpressionStmt(block_expr); BIND_CACHED_LOCATION(expr_stmt);

    		container_block->appendObject(expr_stmt);
		}

    	_result = container_expr_stmt;
    }
    END_ACTION
};

struct decl_statement
{
    DEFINE_ATTRIBUTES(Statement*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("decl_statement param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("decl_statement param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        VariableDecl* decl = dynamic_cast<VariableDecl*>(_param(1));
        decl->is_static = _param(0).is_initialized();
        BIND_CACHED_LOCATION(_result = new DeclarativeStmt(decl));
    }
    END_ACTION
};

struct expression_statement
{
    DEFINE_ATTRIBUTES(Statement*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("expression_statement param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        if(_param(0).is_initialized())
            BIND_CACHED_LOCATION(_result = new ExpressionStmt(*_param(0))) // NOTE: omit SEMICOLON
        else
            BIND_CACHED_LOCATION(_result = new ExpressionStmt(new NumericLiteral(0)));
    }
    END_ACTION
};

struct selection
{
    DEFINE_ATTRIBUTES(Selection*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init_selection)
    {
        Expression* cond  = _param(0);
        Block*      block = _param(1);

        BIND_CACHED_LOCATION(_result = new Selection(cond, block));
    }
    END_ACTION
} ;

struct selection_statement
{
    DEFINE_ATTRIBUTES(Statement*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init_if_statement)
    {
#ifdef DEBUG
        printf("selection_statement::init_if_statement param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("selection_statement::init_if_statement param(1) type = %s\n", typeid(_param_t(1)).name());
        printf("selection_statement::init_if_statement param(2) type = %s\n", typeid(_param_t(2)).name());
        printf("selection_statement::init_if_statement param(3) type = %s\n", typeid(_param_t(3)).name());
#endif
        Expression* cond  = _param(0);
        Block*      block = _param(1);
        Selection*  if_   = new Selection(cond, block); BIND_CACHED_LOCATION(if_);
        BIND_CACHED_LOCATION(_result = new IfElseStmt(if_));
        for(auto& i : _param(2))
        {
            Expression* cond  = boost::fusion::at_c<0>(i);
            Block*      block = boost::fusion::at_c<1>(i);
            Selection*  elif  = new Selection(cond, block); BIND_CACHED_LOCATION(elif);
            cast<IfElseStmt>(_result)->addElseIfBranch(elif);
        }
        if(_param(3).is_initialized())
            cast<IfElseStmt>(_result)->setElseBranch(*_param(3));
    }
    END_ACTION

    BEGIN_ACTION(init_switch_statement)
    {
#ifdef DEBUG
        printf("selection_statement::init_switch_statement param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("selection_statement::init_switch_statement param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        bool has_visited_default_label = false;
        bool has_record_error = false;

        SwitchStmt* sw_stmt = new SwitchStmt(_param(0));

        BIND_CACHED_LOCATION(_result = sw_stmt);

        for (const auto& case_or_default: _param(1))
        {
            switch (case_or_default.which())
            {
            case 0: // 'case'
                sw_stmt->addCase(boost::get<Selection*>(case_or_default));
                break;

            case 1: // 'default'
                if (sw_stmt->default_block == nullptr)
                {
                    Block* default_block = boost::get<Block*>(case_or_default);

                    BOOST_ASSERT(default_block && "null pointer exception");

                    sw_stmt->setDefaultCase(default_block);
                }
                else
                {
                    if (!has_record_error)
                    {
                        LOG_MESSAGE(MULTIPLE_DEFAULT_LABELS, _result, _optional_filename(getParserContext().active_source->filename));

                        has_record_error = true;
                    }
                }

                break;

            default:
                UNREACHABLE_CODE();
                break;
            }
        }

        if (sw_stmt->default_block == nullptr && sw_stmt->cases.empty())
        {
            LOG_MESSAGE(EMPTY_SWITCH_STATEMENT, _result, _optional_filename(getParserContext().active_source->filename));
        }
    }
    END_ACTION
};

struct iteration_statement
{
    DEFINE_ATTRIBUTES(Statement*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init_while_loop)
    {
#ifdef DEBUG
        printf("iteration_statement::init_while_loop param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("iteration_statement::init_while_loop param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        Block* block = _param(1);
        BIND_CACHED_LOCATION(_result = new WhileStmt(WhileStmt::Style::WHILE, _param(0), block));
    }
    END_ACTION

    BEGIN_ACTION(init_do_while_loop)
    {
#ifdef DEBUG
        printf("iteration_statement::init_do_while_loop param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("iteration_statement::init_do_while_loop param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        BIND_CACHED_LOCATION(_result = new WhileStmt(WhileStmt::Style::DO_WHILE, _param(1), _param(0)));
    }
    END_ACTION

    BEGIN_ACTION(init_foreach)
    {
#ifdef DEBUG
        printf("iteration_statement::init_foreach param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("iteration_statement::init_foreach param(1) type = %s\n", typeid(_param_t(1)).name());
        printf("iteration_statement::init_foreach param(2) type = %s\n", typeid(_param_t(2)).name());
#endif
        Block* outer_block = new NormalBlock(); BIND_CACHED_LOCATION(outer_block);

        VariableDecl* var_decl     = _param(0);
        Expression* container_expr = _param(1);
        Block* block               = _param(2);
        BOOST_ASSERT(var_decl->initializer == nullptr);

        // create iter declaration
        Identifier* entry_id = var_decl->name;
        std::string str_id = UUID::random().toString();
        std::wstring iter_id = L"foreach-iter-" + s_to_ws(str_id);
        var_decl->name = new SimpleIdentifier(iter_id); BIND_CACHED_LOCATION(var_decl->name);
        var_decl->name->parent = var_decl;
        DeclarativeStmt* decl_stmt = new DeclarativeStmt(var_decl); BIND_CACHED_LOCATION(decl_stmt);
        // transfer annotation to statement (from declaration)
        if(Annotations* anno_list = var_decl->getAnnotations())
        {
            var_decl->setAnnotations(nullptr);
            decl_stmt->setAnnotations(anno_list);
        }
        outer_block->appendObject(decl_stmt);

        // create iter init
        CallExpr* call_expr = new CallExpr(new MemberExpr(container_expr, L"iterator"_sid));
        BinaryExpr* assign_expr = new BinaryExpr(BinaryExpr::OpCode::ASSIGN, new IdExpr(new SimpleIdentifier(iter_id)), call_expr);
        ExpressionStmt* iter_init_stmt = new ExpressionStmt(assign_expr); BIND_CACHED_LOCATION(iter_init_stmt);
        ASTNodeHelper::foreachApply<ASTNode>(*iter_init_stmt, [var_decl](ASTNode& node) { ASTNodeHelper::propogateSourceInfo(node, *var_decl); });
        outer_block->appendObject(iter_init_stmt);

        ASTNode*    for_init_node = new NumericLiteral(true); BIND_CACHED_LOCATION(for_init_node);
        Expression* for_cond_expr = new CallExpr(new MemberExpr(new IdExpr(new SimpleIdentifier(iter_id)), L"hasNext"_sid)); BIND_CACHED_LOCATION(for_cond_expr);
        Expression* for_step_expr = new NumericLiteral(true); BIND_CACHED_LOCATION(for_step_expr);
        ASTNodeHelper::foreachApply<ASTNode>(*for_init_node, [for_init_node](ASTNode& node) { ASTNodeHelper::propogateSourceInfo(node, *for_init_node); });
        ASTNodeHelper::foreachApply<ASTNode>(*for_cond_expr, [for_cond_expr](ASTNode& node) { ASTNodeHelper::propogateSourceInfo(node, *for_cond_expr); });
        ASTNodeHelper::foreachApply<ASTNode>(*for_step_expr, [for_step_expr](ASTNode& node) { ASTNodeHelper::propogateSourceInfo(node, *for_step_expr); });

        CallExpr* call_get_entry = new CallExpr(new MemberExpr(new IdExpr(new SimpleIdentifier(iter_id)), L"next"_sid)); BIND_CACHED_LOCATION(call_get_entry);
        DeclarativeStmt* decl_entry = new DeclarativeStmt(new VariableDecl(ASTNodeHelper::clone(entry_id), nullptr, false, false, false, Declaration::VisibilitySpecifier::DEFAULT, call_get_entry)); BIND_CACHED_LOCATION(decl_entry);
        ASTNodeHelper::foreachApply<ASTNode>(*decl_entry, [decl_entry](ASTNode& node) { ASTNodeHelper::propogateSourceInfo(node, *decl_entry); });

        block->prependObject(decl_entry);

        ForStmt* for_stmt = new ForStmt(for_init_node, for_cond_expr, for_step_expr, block);
        outer_block->appendObject(for_stmt);

        BIND_CACHED_LOCATION(_result = new ExpressionStmt(new BlockExpr(outer_block, "parser_for_each")));
    }
    END_ACTION

    BEGIN_ACTION(init_for)
    {
#ifdef DEBUG
        printf("iteration_statement::init_for param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("iteration_statement::init_for param(1) type = %s\n", typeid(_param_t(1)).name());
        printf("iteration_statement::init_for param(2) type = %s\n", typeid(_param_t(2)).name());
        printf("iteration_statement::init_for param(3) type = %s\n", typeid(_param_t(3)).name());
#endif

        Block* outer_block = new NormalBlock(); BIND_CACHED_LOCATION(outer_block);

        Declaration* init_decl = NULL;
        Expression* init_expr = NULL;

        if(_param(0)) {
            switch(_param(0)->which())
            {
            case 0: 
               {
                   typedef boost::fusion::vector2< 
                                                  boost::optional<Annotations*>,
                                                  Declaration*
                                                 > DeclarationWithAnnotations;

                   DeclarationWithAnnotations decl = boost::get<decltype(decl)>(*_param(0));    
                   init_decl = boost::fusion::at_c<1>(decl);

                   if( boost::fusion::at_c<0>(decl) )
                   {
                       init_decl->setAnnotations( *boost::fusion::at_c<0>(decl) );
                   }
               }
               break;
            case 1: init_expr = boost::get<Expression*>(*_param(0)); break;
            }
        }

        ASTNode* init_node = NULL;
        if(init_decl != NULL)
        {
            DeclarativeStmt* decl_stmt = new DeclarativeStmt(init_decl); BIND_CACHED_LOCATION(decl_stmt);
            // transfer annotation to statement (from declaration)
            if(Annotations* anno_list = init_decl->getAnnotations())
            {
                init_decl->setAnnotations(NULL);
                decl_stmt->setAnnotations(anno_list);
            }
            outer_block->appendObject(decl_stmt);
        }
        else if(init_expr != NULL)
        {
            Block* init_block = new NormalBlock(); BIND_CACHED_LOCATION(init_block);
            ExpressionStmt* expr_stmt = new ExpressionStmt(init_expr); BIND_CACHED_LOCATION(expr_stmt);
            init_block->appendObject(expr_stmt);
            init_node = init_block;
        }

        if(init_node == NULL)
        {
            Literal* literal = new NumericLiteral(true); BIND_CACHED_LOCATION(literal);
            init_node = literal;
        }

        Expression* cond_expression = _param(1).is_initialized() ? *_param(1) : NULL;
        if(cond_expression == NULL)
        {
            Literal* literal = new NumericLiteral(true); BIND_CACHED_LOCATION(literal);
            cond_expression = literal;
        }

        Expression* step_expression = _param(2).is_initialized() ? *_param(2) : NULL;
        if(step_expression == NULL)
        {
            Literal* literal = new NumericLiteral(0); BIND_CACHED_LOCATION(literal);
            step_expression = literal;
        }

        Block* block = _param(3);

        ForStmt* for_stmt = new ForStmt(init_node, cond_expression, step_expression, block); BIND_CACHED_LOCATION(for_stmt);

        outer_block->appendObject(for_stmt);

        BIND_CACHED_LOCATION(_result = new ExpressionStmt(new BlockExpr(outer_block, "parser_for")));
    }
    END_ACTION
};

struct branch_statement
{
    DEFINE_ATTRIBUTES(Statement*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init_return)
    {
#ifdef DEBUG
        printf("branch_statement::init_return param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        Expression* expr = _param(0).is_initialized() ? *_param(0) : NULL;
        BIND_CACHED_LOCATION(_result = new BranchStmt(BranchStmt::OpCode::RETURN, expr));
    }
    END_ACTION

    BEGIN_TEMPLATED_ACTION(init, BranchStmt::OpCode::type Type)
    {
        BIND_CACHED_LOCATION(_result = new BranchStmt(Type));
    }
    END_ACTION
};

} } }

#endif /* ZILLIANS_LANGUAGE_ACTION_STATEMENT_STATEMENTACTIONS_H_ */
