/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_PARSERCONTEXT_H_
#define ZILLIANS_LANGUAGE_PARSERCONTEXT_H_

#include "core/Prerequisite.h"
#include "language/tree/ASTNodeFactory.h"

namespace zillians { namespace language {

struct ParserContext
{
    ParserContext() : enable_semantic_action(true), dump_rule_debug(false), tangle(NULL), active_source(NULL), active_package(NULL), last_modification_time(0)
    { }

    bool enable_semantic_action;
    bool dump_rule_debug;

    tree::Tangle* tangle;
    tree::Source* active_source;
    tree::Package* active_package;
    std::time_t last_modification_time;

    struct
    {
        uint32 line;
        uint32 column;
    } debug;

    std::vector<std::function<void()>> tree_actions;
};

bool hasParserContext();
ParserContext& getParserContext();
void setParserContext(ParserContext* context);

} }

#endif /* ZILLIANS_LANGUAGE_PARSERCONTEXT_H_ */
