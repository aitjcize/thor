/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_CONTEXTBASE_H_
#define ZILLIANS_LANGUAGE_CONTEXTBASE_H_

#include <cstddef>

#include <type_traits>
#include <utility>
#include <vector>

#include <boost/algorithm/cxx11/all_of.hpp>
#include <boost/assert.hpp>

#include "utility/Functional.h"

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/tree/RelinkablePtr.h"
#include "language/tree/UniqueName.h"

namespace zillians { namespace language {

namespace detail {

template<typename Traits, typename MapperType, typename std::enable_if< std::is_pointer<typename Traits::ValueType>::value>::type * = nullptr>
inline void update_reference(MapperType& mapper, typename Traits::ValueType& value)
{
    mapper & value;
}

template<typename Traits, typename MapperType, typename std::enable_if<!std::is_pointer<typename Traits::ValueType>::value>::type * = nullptr>
inline void update_reference(MapperType& mapper, typename Traits::ValueType& value)
{
    Traits::update_reference(mapper, value);
}

template<typename MapperType, typename NodeType>
inline void update_reference_relinkable(MapperType& mapper, tree::relinkable_ptr<NodeType>& value)
{
    if (!value.is_ptr())
        return;

    auto*const old_ptr = value.get_ptr();
    auto*          ptr = old_ptr;

    if (ptr == nullptr)
        return;

    mapper & ptr;

    if (ptr != old_ptr)
        value = ptr;
}

}

template<typename Traits>
struct ContextBase
{
    typedef typename Traits::ValueType  ValueType;
    typedef typename Traits::ResultType ResultType;

    static_assert(std::is_pointer<ResultType>::value, "ResultType must be pointer");

    static ResultType get(const tree::ASTNode* node)
    {
        BOOST_ASSERT(node && "null pointer exception");

        if(ContextBase* context = node->get<ContextBase>())
        {
            return Traits::getImpl(context->value);
        }
        else
        {
            return nullptr;
        }
    }

    template<typename RefType>
    static void set(tree::ASTNode* node, RefType&& ref)
    {
        BOOST_ASSERT(node && "null pointer exception");
        BOOST_ASSERT(Traits::isValid(ref) && "invalid resolved type");

        ContextBase* context = init(node);

        BOOST_ASSERT(context && "null pointer exception");

        Traits::setImpl(context->value, std::forward<RefType>(ref));
    }

    static ContextBase* init(tree::ASTNode* node)
    {
        BOOST_ASSERT(node && "null pointer exception");

        ContextBase* context = node->get<ContextBase>();

        if(context == nullptr)
        {
            context = node->init<ContextBase>();
        }

        return context;
    }

    static bool is_inited(const tree::ASTNode* node)
    {
        BOOST_ASSERT(node && "null pointer exception");

        ContextBase* context = node->get<ContextBase>();

        return context != nullptr;
    }

    ContextBase() : value() {}

    template<typename Archive>
    void serialize(Archive& ar, unsigned int)
    {
        ar & value;
    }

    template<typename Mapper>
    void update_reference(Mapper& mapper)
    {
        detail::update_reference<Traits>(mapper, value);
    }

    ValueType value;
};

template<typename TheNodeType>
struct ContextTraitsBase
{
    typedef TheNodeType  NodeType;
    typedef TheNodeType* ValueType;
    typedef TheNodeType* ResultType;

    template<typename ValueType>
    static ResultType getImpl(ValueType& value)
    {
        return value;
    }

    template<typename ValueType>
    static void setImpl(ValueType& value, NodeType* node)
    {
        static_assert(std::is_same<ValueType, NodeType*>::value, "you should provide specific version if value is not a AST node pointer");

        value = node;
    }
};

template<typename TheNodeType, typename ContainerType = std::vector<TheNodeType*>>
struct ContextMultiItemTraitsBase
{
    typedef TheNodeType    NodeType;
    typedef ContainerType  ValueType;
    typedef ContainerType* ResultType;

    static bool isValid(const ValueType& refs)
    {
        return boost::algorithm::all_of(
            refs,
            not_null()
        );
    }

    static ResultType getImpl(ValueType& value)
    {
        return &value;
    }

    static void setImpl(ValueType& value, ValueType&& new_value)
    {
        value = std::move(new_value);
    }
};

template<typename TheNodeType>
struct ContextRelinkableTraitsBase
{
    typedef TheNodeType                       NodeType;
    typedef tree::relinkable_ptr<TheNodeType> ValueType;
    typedef TheNodeType*                      ResultType;

    static ResultType getImpl(ValueType& value)
    {
        return value.get_ptr();
    }

    static void setImpl(ValueType& value, std::nullptr_t                         ) { value =           nullptr     ; }
    static void setImpl(ValueType& value, NodeType*                   ref        ) { value =           ref         ; }
    static void setImpl(ValueType& value, const tree::unique_name_t&  unique_name) { value =           unique_name ; }
    static void setImpl(ValueType& value,       tree::unique_name_t&& unique_name) { value = std::move(unique_name); }
    static void setImpl(ValueType& value, const ValueType&            new_value  ) { value =           new_value   ; }
    static void setImpl(ValueType& value,       ValueType&&           new_value  ) { value = std::move(new_value  ); }

    template<typename MapperType>
    static void update_reference(MapperType& mapper, ValueType& value)
    {
        detail::update_reference_relinkable(mapper, value);
    }
};

} }

#endif /* ZILLIANS_LANGUAGE_CONTEXTBASE_H_ */
