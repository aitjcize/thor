/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_ARCHITECTURE_H_
#define ZILLIANS_LANGUAGE_ARCHITECTURE_H_

#include <string>
#include <vector>

#include "core/IntTypes.h"

namespace zillians { namespace language {

class Architecture
{
public:
    typedef uint64 flag_t;

             Architecture() noexcept;
    explicit Architecture(flag_t flags) noexcept;
    explicit Architecture(const std::string& arch);
    explicit Architecture(const std::wstring& arch);

    static bool is_valid(const std::string& arch);
    static bool is_valid(const std::wstring& arch);

    bool is_zero() const noexcept;
    bool is_not_zero() const noexcept;

    bool is_any(const Architecture arch) const noexcept;
    bool is_any_or_zero(const Architecture arch) const noexcept;

    bool is_x86   () const noexcept;
    bool is_arm   () const noexcept;
    bool is_cpu   () const noexcept;
    bool is_opencl() const noexcept;
    bool is_sm_10 () const noexcept;
    bool is_sm_12 () const noexcept;
    bool is_sm_13 () const noexcept;
    bool is_sm_20 () const noexcept;
    bool is_sm_30 () const noexcept;
    bool is_sm_35 () const noexcept;
    bool is_cuda  () const noexcept;
    bool is_gpu   () const noexcept;

    bool is_x86_or_zero   () const noexcept;
    bool is_arm_or_zero   () const noexcept;
    bool is_cpu_or_zero   () const noexcept;
    bool is_opencl_or_zero() const noexcept;
    bool is_sm_10_or_zero () const noexcept;
    bool is_sm_12_or_zero () const noexcept;
    bool is_sm_13_or_zero () const noexcept;
    bool is_sm_20_or_zero () const noexcept;
    bool is_sm_30_or_zero () const noexcept;
    bool is_sm_35_or_zero () const noexcept;
    bool is_gpu_or_zero   () const noexcept;
    bool is_cuda_or_zero  () const noexcept;

    bool is_not_x86   () const noexcept;
    bool is_not_arm   () const noexcept;
    bool is_not_cpu   () const noexcept;
    bool is_not_opencl() const noexcept;
    bool is_not_sm_10 () const noexcept;
    bool is_not_sm_12 () const noexcept;
    bool is_not_sm_13 () const noexcept;
    bool is_not_sm_20 () const noexcept;
    bool is_not_sm_30 () const noexcept;
    bool is_not_sm_35 () const noexcept;
    bool is_not_cuda  () const noexcept;
    bool is_not_gpu   () const noexcept;

    void reset() noexcept;
    bool reset(const std::string& arch);
    bool reset(const std::wstring& arch);
    bool reset(const std::string& arch, const std::string& delim);
    bool reset(const std::wstring& arch, const std::wstring& delim);

    std::string  toString(const std::string& delim) const;
    std::wstring toString(const std::wstring& delim) const;

    flag_t getFlags() const noexcept;
    std::vector<Architecture> splitFlags(const Architecture interested = all()) const;

    static Architecture zero  () noexcept;
    static Architecture x86   () noexcept;
    static Architecture arm   () noexcept;
    static Architecture cpu   () noexcept;
    static Architecture opencl() noexcept;
    static Architecture cuda  () noexcept;
    static Architecture gpu   () noexcept;
    static Architecture all   () noexcept;

    bool operator==(const Architecture& rhs) const noexcept;
    bool operator!=(const Architecture& rhs) const noexcept;

    Architecture& operator&=(const Architecture& rhs) noexcept;
    Architecture& operator|=(const Architecture& rhs) noexcept;

    Architecture operator&(const Architecture& rhs) const noexcept;
    Architecture operator|(const Architecture& rhs) const noexcept;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        ar & flags;
    }

private:
    flag_t flags;

public:
    static Architecture default_;
};

} }

#endif /* ZILLIANS_LANGUAGE_ARCHITECTURE_H_ */
