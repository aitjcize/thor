/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_PRIMITIVEKIND_H_
#define ZILLIANS_LANGUAGE_TREE_PRIMITIVEKIND_H_

#include <string>

#include "core/Types.h"

#include "language/tree/basic/Type.h"

namespace zillians { namespace language { namespace tree {

struct PrimitiveKind
{
    enum kind {
        VOID_TYPE   ,

        BOOL_TYPE   ,

        INT8_TYPE   ,
        INT16_TYPE  ,
        INT32_TYPE  ,
        INT64_TYPE  ,

        FLOAT32_TYPE,
        FLOAT64_TYPE,
    };

    PrimitiveKind();
    PrimitiveKind(const kind k);

    operator kind() const;

    kind getKindValue() const;
    void setKindValue(const kind v);

    std::wstring toString() const;

    bool isVoidType() const noexcept;
    bool isBoolType() const noexcept;
    bool isIntegerType() const noexcept;
    bool isArithmeticCapable() const noexcept;
    bool isFloatType() const noexcept;

    kind promote(const PrimitiveKind t1) const noexcept;
    Type::ConversionRank getConversionRank(PrimitiveKind target, const Type::ConversionPolicy policy = Type::ConversionPolicy::Implicitly) const;

    int byteSize() const;
    int bitSize() const;

    bool isImplicitConvertible(const PrimitiveKind to, bool& precision_loss) const;

    static int64 maxIntegerValue(const PrimitiveKind k);
    static int64 minIntegerValue(const PrimitiveKind k);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        ar & reinterpret_cast<int&>(kind_);
    }

private:
    kind kind_;
} ;

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_PRIMITIVEKIND_H_ */
