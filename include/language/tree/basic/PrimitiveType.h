/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_PRIMITIVETYPE_H_
#define ZILLIANS_LANGUAGE_TREE_PRIMITIVETYPE_H_

#include "core/Types.h"
#include "PrimitiveKind.h"
#include "language/tree/ASTNode.h"
#include "language/tree/basic/Type.h"

namespace zillians { namespace language { namespace tree {

struct Internal;

struct PrimitiveType : public Type
{
    friend class boost::serialization::access;
    friend struct Internal;

    AST_NODE_HIERARCHY(PrimitiveType, Type);

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);
        ar & boost::serialization::base_object<Type>(*this);
        ar & primitive_kind_;
    }

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;
    virtual PrimitiveType* clone() const;
    virtual bool isSame(const Type& rhs) const;
    virtual bool isLessThan(const Type& rhs) const;
    virtual std::wstring toString() const;
    virtual size_t getTypeClassSerialNumber() const;
    virtual ConversionRank getConversionRank(const Type& target, const ConversionPolicy policy = Type::ConversionPolicy::Implicitly) const override;

    virtual PrimitiveType* getCanonicalType() const override;

    virtual PrimitiveType* getArithmeticCompatibleType() const override;

    PrimitiveKind getKind() const
    {
        return primitive_kind_;
    }

    bool isVoidType() const
    {
        return primitive_kind_.isVoidType();
    }

    bool isBoolType() const
    {
        return primitive_kind_.isBoolType();
    }

    bool isIntegerType() const
    {
        return primitive_kind_.isIntegerType();
    }

    bool isArithmeticCapable() const
    {
        return primitive_kind_.isArithmeticCapable();
    }

    bool isFloatType() const
    {
        return primitive_kind_.isFloatType();
    }

    PrimitiveKind promote(PrimitiveType* t1) const noexcept
    {
        return getKind().promote(t1->getKind());
    }

    int byteSize() const
    {
        return primitive_kind_.byteSize();
    }

    bool isImplicitConvertible(const PrimitiveType* to, bool& precision_loss) const
    {
        return getKind().isImplicitConvertible(to->getKind(), precision_loss);
    }

protected:
    PrimitiveType();

private:
    explicit PrimitiveType(const PrimitiveKind ty);
    PrimitiveKind primitive_kind_;
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_PRIMITIVETYPE_H_ */
