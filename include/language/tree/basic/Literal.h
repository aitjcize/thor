/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_LITERAL_H_
#define ZILLIANS_LANGUAGE_TREE_LITERAL_H_

#include <ostream>

#include <boost/serialization/access.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/tree/basic/PrimitiveKind.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace tree {

struct Literal : public Expression
{
    AST_NODE_HIERARCHY(Literal, Expression);

    virtual std::wstring toString() const = 0;

    virtual bool isRValue() const override;

    virtual Literal* clone() const = 0;

    virtual Type* getCanonicalType() const = 0;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
        );
    }
};

struct ObjectLiteral : public Literal
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(ObjectLiteral, Literal);

    struct LiteralType {
        enum type {
            NULL_OBJECT,
//            SELF_OBJECT,
            THIS_OBJECT,
            SUPER_OBJECT,
        };

        static const wchar_t* toString(type t);
    };

    explicit ObjectLiteral(LiteralType::type t);

    bool isNullLiteral() const;
    bool isThisLiteral() const;
    bool isSuperLiteral() const;

    bool hasValue() const override;

    virtual Type* getCanonicalType() const override;

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual ObjectLiteral* clone() const override;

    using BaseNode::toSource;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override;

    virtual std::wstring toString() const override;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (type)
        );
    }

    LiteralType::type type;

protected:
    ObjectLiteral();
};

struct NumericLiteral : public Literal
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(NumericLiteral, Literal);

    explicit NumericLiteral(bool   v);
    explicit NumericLiteral(int8   v);
    explicit NumericLiteral(int16  v);
    explicit NumericLiteral(int32  v);
    explicit NumericLiteral(int64  v);
    explicit NumericLiteral(float  v);
    explicit NumericLiteral(double v);

    template<typename T>
    explicit NumericLiteral(const PrimitiveKind k, T v)
    {
        using namespace std;
        switch(k)
        {
        case PrimitiveKind::BOOL_TYPE   : value.b   = (bool  )v; break;
        case PrimitiveKind::INT8_TYPE   : value.i8  = (int8  )v; break;
        case PrimitiveKind::INT16_TYPE  : value.i16 = (int16 )v; break;
        case PrimitiveKind::INT32_TYPE  : value.i32 = (int32 )v; break;
        case PrimitiveKind::INT64_TYPE  : value.i64 = (int64 )v; break;
        case PrimitiveKind::FLOAT32_TYPE: value.f32 = (float )v; break;
        case PrimitiveKind::FLOAT64_TYPE: value.f64 = (double)v; break;
        default: break;
        }

        primitive_kind.setKindValue(k);
    }

    template <typename R>
    R get() const
    {
        switch( primitive_kind )
        {
        case PrimitiveKind::BOOL_TYPE   : return value.b  ;
        case PrimitiveKind::INT8_TYPE   : return value.i8 ;
        case PrimitiveKind::INT16_TYPE  : return value.i16;
        case PrimitiveKind::INT32_TYPE  : return value.i32;
        case PrimitiveKind::INT64_TYPE  : return value.i64;
        case PrimitiveKind::FLOAT32_TYPE: return value.f32;
        case PrimitiveKind::FLOAT64_TYPE: return value.f64;
        default: BOOST_ASSERT( false && "should not reach here" );
        return 0;
        }
    }

    size_t byteSize() const;

    bool hasValue() const override;

    virtual Type* getCanonicalType() const override;

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual NumericLiteral* clone() const override;

    using BaseNode::toSource;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override;

    virtual std::wstring toString() const override;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        ar & boost::serialization::base_object<Literal>(*this);
        ar & primitive_kind;
        switch(primitive_kind)
        {
        case PrimitiveKind::BOOL_TYPE    : ar & value.b  ; break;
        case PrimitiveKind::INT8_TYPE    : ar & value.i8 ; break;
        case PrimitiveKind::INT16_TYPE   : ar & value.i16; break;
        case PrimitiveKind::INT32_TYPE   : ar & value.i32; break;
        case PrimitiveKind::INT64_TYPE   : ar & value.i64; break;
        case PrimitiveKind::FLOAT32_TYPE : ar & value.f32; break;
        case PrimitiveKind::FLOAT64_TYPE : ar & value.f64; break;
        default: break;
        }
    }

    PrimitiveKind primitive_kind;

    union ValueUnion
    {
        bool  b;
        int8  i8;
        int16 i16;
        int32 i32;
        int64 i64;

        float  f32;
        double f64;
    } value;

protected:
    NumericLiteral();
};

struct StringLiteral : public Literal
{
    friend class boost::serialization::access;
    typedef std::wstring string_type;

    AST_NODE_HIERARCHY(StringLiteral, Literal);

    explicit StringLiteral(const std::wstring& s);

    template<typename Iterator>
    explicit StringLiteral(Iterator begin, Iterator end)
    : value(begin, end)
    { }

    bool hasValue() const override;

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual StringLiteral* clone() const override;

    using BaseNode::toSource;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override;

    virtual Type* getCanonicalType() const override;

    virtual std::wstring toString() const override;

    static bool searchPlaceholder( string_type::const_iterator begin,
                                   string_type::const_iterator end,
                                   std::pair< 
                                             string_type::const_iterator,
                                             string_type::const_iterator
                                            >& place );
    static constexpr string_type::value_type getInternalPlaceholderBegin()
    {
        return L'\030';
    }

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (value)
        );
    }

    string_type value;

protected:
    StringLiteral();
};

struct IdLiteral : public Literal
{
    AST_NODE_HIERARCHY(IdLiteral, Literal);

    explicit IdLiteral(const std::wstring& tag = L"", int64 local_id = 0, const UUID& tangle_uuid = UUID::nil());

    bool hasValue() const override;

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual Type* getCanonicalType() const override;

    using BaseNode::toSource;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override;

    virtual std::wstring toString() const;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (tag     )
            (local_id)
            (tangle_uuid)
        );
    }

    std::wstring tag;
    int64        local_id;
    UUID         tangle_uuid;
};

#define TREE_DEFINE_FINAL_ID_LITERAL(name)                              \
    struct BOOST_PP_CAT(name, IdLiteral) : public IdLiteral             \
    {                                                                   \
        AST_NODE_HIERARCHY(BOOST_PP_CAT(name, IdLiteral), IdLiteral);   \
                                                                        \
        using IdLiteral::IdLiteral;                                     \
                                                                        \
        virtual BOOST_PP_CAT(name, IdLiteral)* clone() const override;  \
                                                                        \
        template<typename Archive>                                      \
        void serialize(Archive& ar, const unsigned int)                 \
        {                                                               \
            AST_NODE_SERIALIZE(                                         \
                ar,                                                     \
            );                                                          \
        }                                                               \
    }

TREE_DEFINE_FINAL_ID_LITERAL(Function);
TREE_DEFINE_FINAL_ID_LITERAL(Symbol);
TREE_DEFINE_FINAL_ID_LITERAL(Type);

#undef TREE_DEFINE_FINAL_ID_LITERAL

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_LITERAL_H_ */
