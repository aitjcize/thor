/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_DECLARATION_H_
#define ZILLIANS_LANGUAGE_TREE_DECLARATION_H_

#include "language/tree/ASTNode.h"
#include "language/tree/RelinkablePtr.h"
#include "language/tree/UniqueName.h"
#include "language/tree/basic/Annotations.h"
#include "language/tree/basic/Identifier.h"
#include "language/Architecture.h"

namespace zillians { namespace language { namespace tree {

struct Declaration : public Annotatable
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(Declaration, Annotatable);

    struct VisibilitySpecifier
    {
        enum type {
            DEFAULT,
            PUBLIC,
            PROTECTED,
            PRIVATE,
        };

        static const wchar_t* toString(type t)
        {
            switch(t)
            {
            case DEFAULT: return L"default";
            case PUBLIC: return L"public";
            case PROTECTED: return L"protected";
            case PRIVATE: return L"private";
            default: UNREACHABLE_CODE(); return L"";
            }
        }
    };

protected:
    Declaration();

public:
    explicit Declaration(Identifier* name);

    bool isGlobal() const;
    bool mayConflict() const;

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    const Identifier*    getName() const noexcept;
    const unique_name_t& getUniqueName() const noexcept;
    Architecture         getActualArch() const noexcept;

    virtual std::wstring toString() const = 0;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override = 0; using BaseNode::toSource;
    virtual Declaration* clone() const = 0;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (may_conflict )
            (name         )
            (unique_name  )
            (resolved_type)
            (arch         )
        );
    }

    /*
     * Why we need function getType()?
     * What is it's role of this function?
     * What should it return?
     *
     * yoco is not sure about it, maybe we don't this function XD
     *
     * (it maybe can speed up the resolution, resolution is a 'chain',
     *  getType() can build partial link of the chain, not always have to wait
     *  the target type be resolved)
     */
    virtual Type* getType() const = 0;
    virtual Type* getCanonicalType() const = 0;

    bool may_conflict;
    Identifier* name;
    unique_name_t unique_name;
    relinkable_ptr<Type> resolved_type;
    Architecture arch;
};

} } } // namespace 'zillians::language::tree'

#endif /* ZILLIANS_LANGUAGE_TREE_DECLARATION_H_ */
