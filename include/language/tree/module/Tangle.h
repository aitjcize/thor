/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_TANGLE_H_
#define ZILLIANS_LANGUAGE_TREE_TANGLE_H_

#include <map>
#include <set>
#include <string>
#include <utility>
#include <type_traits>

#include <boost/blank.hpp>

#include "utility/UUIDUtil.h"
#include "language/tree/ASTNode.h"
#include "language/tree/RelinkablePtr.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/module/Package.h"

namespace zillians { namespace language { namespace tree {

struct Config;

struct Tangle : public ASTNode
{
    friend class boost::serialization::access;

    using OffsetedId = std::pair<UUID, int64>;

    template<typename DataType>
    using DataToIdMap = std::map<
                            DataType,
                            std::pair<
                                std::set<OffsetedId>,
                                typename std::conditional<
                                    std::is_same<
                                        relinkable_ptr<const FunctionDecl>,
                                        DataType
                                    >::value,
                                    int, // use 'int' to remove header dependency from ASTNodeHelper.h
                                    boost::blank
                                >::type  // payload, for function declarations only (export kind)
                            >
                        >;

    AST_NODE_HIERARCHY(Tangle, ASTNode);

private:
    Tangle();

public:
    explicit Tangle(const UUID&  new_id);
    explicit Tangle(      UUID&& new_id);

    void markImported(bool is_imported);

    bool merge(Tangle& rhs);

    virtual bool isEqualImpl(const ASTNode& rhs) const;

    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    virtual Tangle* clone() const;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    const UUID& getId() const noexcept;

    std::pair<OffsetedId, bool> addType    (const     ClassDecl& decl  );
    std::pair<OffsetedId, bool> addFunction(const  FunctionDecl& decl  );
    std::pair<OffsetedId, bool> addSymbol  (const StringLiteral& symbol);

    std::pair<OffsetedId, bool> getIdOfType    (const     ClassDecl& decl  ) const;
    std::pair<OffsetedId, bool> getIdOfFunction(const  FunctionDecl& decl  ) const;
    std::pair<OffsetedId, bool> getIdOfSymbol  (const StringLiteral& symbol) const;

    const DataToIdMap<relinkable_ptr<const    ClassDecl>>& getMappingOfType    () const noexcept;
    const DataToIdMap<relinkable_ptr<const FunctionDecl>>& getMappingOfFunction() const noexcept;
    const DataToIdMap<std::wstring                      >& getMappingOfSymbol  () const noexcept;

    Package* findPackage(const std::wstring& package_path) const;
    ClassDecl* findThorLangObject() const;
    FunctionDecl* findThorLangFunction(const std::wstring& function_name) const;

    ClassDecl* findClass(const std::wstring& package_path, const std::wstring& class_name) const;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        AST_NODE_SERIALIZE(
            ar,
            (config         )
            (internal       )
            (root           )
            (id             )
            (type_id_max    )
            (func_id_max    )
            (symb_id_max    )
            (data_to_type_id)
            (data_to_func_id)
            (data_to_symb_id)
        );
    }

    Config*   config;
    Internal* internal;
    Package*  root;

    UUID                                            id;
    int64                                           type_id_max;
    int64                                           func_id_max;
    int64                                           symb_id_max;
    DataToIdMap<relinkable_ptr<const    ClassDecl>> data_to_type_id;
    DataToIdMap<relinkable_ptr<const FunctionDecl>> data_to_func_id;
    DataToIdMap<std::wstring                      > data_to_symb_id;
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_TANGLE_H_ */
