/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_INTERNAL_H_
#define ZILLIANS_LANGUAGE_TREE_INTERNAL_H_

#include <vector>

#include "language/resolver/SymbolTable.h"
#include "language/tree/ASTNode.h"
#include "language/tree/basic/Type.h"
#include "language/tree/basic/DeclType.h"
#include "language/tree/basic/FunctionType.h"
#include "language/tree/basic/TypeSpecifier.h"

namespace zillians { namespace language { namespace tree {

struct FunctionDecl;

struct Internal : public ASTNode
{
    AST_NODE_HIERARCHY(Internal, ASTNode);

    Internal();

    PrimitiveType*      getPrimitiveType(PrimitiveKind t) const;
    PrimitiveSpecifier* getPrimitiveTypeSpecifier(PrimitiveKind t) const;

    virtual bool isEqualImpl(const ASTNode& rhs) const;

    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    virtual Internal* clone() const;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        AST_NODE_SERIALIZE(
            ar,
            (VoidTypeSpecifier   )
            (BooleanTypeSpecifier)
            (Int8TypeSpecifier   )
            (Int16TypeSpecifier  )
            (Int32TypeSpecifier  )
            (Int64TypeSpecifier  )
            (Float32TypeSpecifier)
            (Float64TypeSpecifier)

            (VoidType   )
            (BooleanType)
            (Int8Type   )
            (Int16Type  )
            (Int32Type  )
            (Int64Type  )
            (Float32Type)
            (Float64Type)

            (type_set)
        );
    }

public:
    FunctionType * addFunctionType (FunctionDecl           &  func_decl);
    FunctionType * addFunctionType (const FunctionSpecifier&  func_ts);
    FunctionType * addFunctionType (std::vector<Type*>     && param_types, Type* return_type, RecordType* class_type);
    PointerType  * addPointerType  (Type                   *  pointeeType);
    ReferenceType* addReferenceType(Type                   *  pointeeType);
    MultiType    * addMultiType    (const MultiSpecifier   &  multiTs);
    MultiType    * addMultiType    (const TieExpr          &  tieExpr);
    MultiType    * addMultiType    (std::vector<Type*>     && types);

private:
    template<typename TypeType>
    TypeType* addType(TypeType* type);

public:
    std::set<Type*, Type::TypeLess> type_set;

private:
    PrimitiveSpecifier* VoidTypeSpecifier;
    PrimitiveSpecifier* BooleanTypeSpecifier;
    PrimitiveSpecifier* Int8TypeSpecifier;
    PrimitiveSpecifier* Int16TypeSpecifier;
    PrimitiveSpecifier* Int32TypeSpecifier;
    PrimitiveSpecifier* Int64TypeSpecifier;
    PrimitiveSpecifier* Float32TypeSpecifier;
    PrimitiveSpecifier* Float64TypeSpecifier;

    PrimitiveType* VoidType;
    PrimitiveType* BooleanType;
    PrimitiveType* Int8Type;
    PrimitiveType* Int16Type;
    PrimitiveType* Int32Type;
    PrimitiveType* Int64Type;
    PrimitiveType* Float32Type;
    PrimitiveType* Float64Type;
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_INTERNAL_H_ */
