/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_VISITOR_PRETTYPRINTVISITOR_H_
#define ZILLIANS_LANGUAGE_TREE_VISITOR_PRETTYPRINTVISITOR_H_

#include <boost/algorithm/string/join.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/tuple/tuple.hpp>

#include "core/Prerequisite.h"
#include "core/Visitor.h"
#include "utility/UnicodeUtil.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/parser/context/SourceInfoContext.h"

#define INDENT_CHAR  L' '
#define INDENT_WIDTH 2

#define STREAM set_indent(std::wcout, depth)

namespace zillians { namespace language { namespace tree { namespace visitor {

struct PrettyPrintVisitor : Visitor<const ASTNode, void>
{
    CREATE_INVOKER(printInvoker, print);

    PrettyPrintVisitor(bool dump_source_info = false) : dump_source_info(dump_source_info)
    {
        REGISTER_ALL_VISITABLE_ASTNODE(printInvoker);

        depth = 0;
    }

    void increaseIdent()
    {
        ++depth;
    }

    void decreaseIdent()
    {
        --depth;
    }

    //////////////////////////////////////////////////////////////////////
    /// Basic

    void print(const ASTNode& node)
    {
        UNUSED_ARGUMENT(node);
        STREAM << L"<unknown_node/>" << std::endl;
    }

    void print(const Block& node)
    {
        STREAM << L"<block>" << std::endl;
        increaseIdent();
        for(auto* object : node.objects)
        {
            visit(*object);
        }
        decreaseIdent();
        STREAM << L"</block>" << std::endl;
    }

    void print(const Annotations& node)
    {
        for(auto* annotation : node.annotation_list)
        {
            visit(*annotation);
        }
    }

    void print(const Annotation& node)
    {
        STREAM << L"<annotation name=\"" << ((node.name) ? node.name->toString() : L"<unspecified>") << L"\">" << std::endl;
        {
            printSourceInfo(node);
        }
        for(auto& attribute : node.attribute_list)
        {
            increaseIdent();
            {
                STREAM << L"<key>" << std::endl;
                {
                    increaseIdent();
                    visit(*attribute.first);
                    decreaseIdent();
                }
                STREAM << L"</key>" << std::endl;

                STREAM << L"<value>" << std::endl;
                {
                    increaseIdent();
                    visit(*attribute.second);
                    decreaseIdent();
                }
                STREAM << L"</value>" << std::endl;
            }
            decreaseIdent();
        }
        STREAM << L"</annotation>" << std::endl;
    }

    void print(const Identifier& node)
    {
        if(hasSourceInfo(node))
        {
            STREAM << L"<identifier name=\"" << node.toString() << L"\">" << std::endl;
            {
                printSourceInfo(node);
            }
            STREAM << L"</identifier>" << std::endl;
        }
        else
        {
            STREAM << L"<identifier name=\"" << node.toString() << L"\"/>" << std::endl;
        }
    }

    void print(const NumericLiteral& node)
    {
        std::wstringstream ss;
        switch(node.primitive_kind)
        {
        case PrimitiveKind::BOOL_TYPE: ss << node.value.b; break;
        case PrimitiveKind::INT8_TYPE: ss << (int32)node.value.i8; break;
        case PrimitiveKind::INT16_TYPE: ss << (int32)node.value.i16; break;
        case PrimitiveKind::INT32_TYPE: ss << node.value.i32; break;
        case PrimitiveKind::INT64_TYPE: ss << node.value.i64; break;
        case PrimitiveKind::FLOAT32_TYPE: ss << node.value.f32; break;
        case PrimitiveKind::FLOAT64_TYPE: ss << node.value.f64; break;
        default: break;
        }

        STREAM << L"<numeric_literal type=\"" << node.primitive_kind.toString() << L"\" value=\"" << ss.str() << "\">" << std::endl;
        {
            printSourceInfo(node);
        }
        STREAM << L"</numeric_literal>" << std::endl;
    }

    void print(const StringLiteral& node)
    {
        if(hasSourceInfo(node))
        {
            STREAM << L"<string_literal value=\"" << node.value << "\">" << std::endl;
            {
                printSourceInfo(node);
            }
            STREAM << L"</string_literal>" << std::endl;
        }
        else
        {
            STREAM << L"<string_literal value=\"" << node.value << "\"/>" << std::endl;
        }
    }

    void print(const TypeIdLiteral& node)
    {
        STREAM << L"<type_id_literal tag=\"" << node.tag << "\"/>" << std::endl;
    }

    void print(const SymbolIdLiteral& node)
    {
        STREAM << L"<symbol_id_literal tag=\"" << node.tag << "\"/>" << std::endl;
    }

    void print(const FunctionIdLiteral& node)
    {
        STREAM << L"<function_id_literal tag=\"" << node.tag << "\"/>" << std::endl;
    }

    //////////////////////////////////////////////////////////////////////
    /// Module

    void print(const Tangle& node)
    {
        STREAM << L"<tangle>" << std::endl;
        {
            increaseIdent();
            if(node.root) visit(*node.root);
            decreaseIdent();
        }
        STREAM << L"</tangle>" << std::endl;
    }

    void print(const Source& node)
    {
        STREAM << L"<source file=\"" << s_to_ws(node.filename) << "\">" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            increaseIdent();
            for(auto* decl : node.declares)
            {
                visit(*decl);
            }
            decreaseIdent();
        }
        STREAM << L"</source>" << std::endl;
    }

    void print(const Package& node)
    {
        STREAM << L"<package name=\"" << node.id->toString() << L"\">" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            printAnnotation(node.getAnnotations());
        }
        {
            increaseIdent();
            for(auto* child : node.children)
            {
                visit(*child);
            }
            decreaseIdent();
        }
        {
            increaseIdent();
            for(auto* source : node.sources)
            {
                visit(*source);
            }
            decreaseIdent();
        }
        STREAM << L"</package>" << std::endl;
    }

    void print(const Import& node)
    {
        if(hasSourceInfo(node))
        {
            STREAM << L"<import ns=\"" << node.ns->toString() << L"\">" << std::endl;
            {
                printSourceInfo(node);
            }
            STREAM << L"</import>" << std::endl;
        }
        else
        {
            STREAM << L"<import ns=\"" << node.ns->toString() << L"\"/>" << std::endl;
        }

    }

    void print(const PrimitiveSpecifier& node)
    {
        STREAM << L"<primitive_specifier kind=\"" << node.getKind().toString() << L"\">" << std::endl;

        printSourceInfo(node);

        STREAM << L"</primitive_specifier>" << std::endl;
    }

    void print(const NamedSpecifier& node)
    {
        STREAM << L"<named_specifier>" << std::endl;

        printSourceInfo(node);

        increaseIdent();

        visit(*node.getName());

        decreaseIdent();

        STREAM << L"</named_specifier>" << std::endl;
    }

    void print(const FunctionSpecifier& node)
    {
        STREAM << L"<function_specifier>" << std::endl;

        printSourceInfo(node);

        increaseIdent();

        for (auto*const param_type : node.getParameterTypes())
            visit(*param_type);

        if (auto*const return_type = node.getReturnType())
            visit(*return_type);

        decreaseIdent();

        STREAM << L"</function_specifier>" << std::endl;
    }

    void print(const MultiSpecifier& node)
    {
        STREAM << L"<function_specifier>" << std::endl;

        printSourceInfo(node);

        increaseIdent();

        for (TypeSpecifier* ts: node.getTypes())
            visit(*ts);

        decreaseIdent();

        STREAM << L"</function_specifier>" << std::endl;
    }

    void print(const TypeSpecifier&)
    {
        UNREACHABLE_CODE();
    }

    void print(const FunctionType& node)
    {
        STREAM << L"<function_type return_type=\"" << decodeType(node.return_type) << L"\">" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            increaseIdent();
            int index = 0;
            for(auto* type : node.parameter_types)
            {
                STREAM << L"<parameter_type index=\"" << index << "\">" << std::endl;
                {
                    increaseIdent();
                    visit(*type);
                    decreaseIdent();
                }
                STREAM << L"</parameter_type>" << std::endl;
                ++index;
            }
            decreaseIdent();
        }
        STREAM << L"</function_type>" << std::endl;
    }

    //////////////////////////////////////////////////////////////////////
    /// Declaration

    void print(const ClassDecl& node)
    {
        STREAM << L"<class_decl name=\"" << node.name->toString() << L" is_interface=" << (node.is_interface ? L"true" : L"false") << L"\">" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            printAnnotation(node.getAnnotations());
        }
        if(node.base)
        {
            increaseIdent();
            {
                STREAM << L"<base_class>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.base);
                    decreaseIdent();
                }
                STREAM << L"</base_class>" << std::endl;
            }
            decreaseIdent();
        }
        if(node.implements.size() > 0)
        {
            increaseIdent();
            {
                STREAM << L"<implements>" << std::endl;
                {
                    increaseIdent();
                    for(auto* impl : node.implements)
                    {
                        visit(*impl);
                    }
                    decreaseIdent();
                }
                STREAM << L"</implements>" << std::endl;
            }
            decreaseIdent();
        }
        {
            increaseIdent();
            for(auto* method : node.member_functions)
            {
                visit(*method);
            }
            decreaseIdent();
        }
        {
            increaseIdent();
            for(auto* attribute : node.member_variables)
            {
                visit(*attribute);
            }
            decreaseIdent();
        }
        STREAM << L"</class_decl>" << std::endl;
    }

    void print(const EnumDecl& node)
    {
        STREAM << L"<enum_decl name=\"" << node.name->toString() << L"\">" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            printAnnotation(node.getAnnotations());
        }
        {
            increaseIdent();
            for(auto* value : node.values)
            {
                visit(*value);
            }
            decreaseIdent();
        }
        STREAM << L"</enum_decl>" << std::endl;
    }

    void print(const FunctionDecl& node)
    {
        STREAM << L"<function_decl " <<
                L"name=\"" << ((node.name) ? node.name->toString() : L"<unspecified-null>") << L"\" " <<
                L"type=\"" << decodeType(node.type) << L"\" " <<
                L"is_member=\"" << (node.is_member ? L"true" : L"false") << L"\" " <<
                L"is_static=\"" << (node.is_static ? L"true" : L"false") << L"\" " <<
                L"visibility=\"" << Declaration::VisibilitySpecifier::toString(node.visibility) << L"\">" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            printAnnotation(node.getAnnotations());
        }
        {
            increaseIdent();
            {
                STREAM << L"<parameters>" << std::endl;
                {
                    increaseIdent();
                    for(auto* param : node.parameters)
                    {
                        visit(*param);
                    }
                    decreaseIdent();
                }
                STREAM << L"</parameters>" << std::endl;
            }
            decreaseIdent();

            increaseIdent();
            if(node.block)
            {
                visit(*node.block);
            }
            else
            {
                STREAM << L"<null_block/>" << std::endl;
            }
            decreaseIdent();
        }
        STREAM << L"</function_decl>" << std::endl;
    }

    void print(const VariableDecl& node)
    {
        STREAM << L"<variable_decl " <<
                L"name=\"" << node.name->toString() << L"\" " <<
                L"type=\"" << decodeType(node.type) << L"\" "
                L"is_member=\"" << (node.is_member ? L"true" : L"false") << L"\" " <<
                L"is_static=\"" << (node.is_static ? L"true" : L"false") << L"\" " <<
                L"is_const=\"" << (node.is_const ? L"true" : L"false") << L"\" " <<
                L"visibility=\"" << Declaration::VisibilitySpecifier::toString(node.visibility) << L"\">" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            printAnnotation(node.getAnnotations());
        }
        if(node.initializer)
        {
            STREAM << L"<initializer>" << std::endl;
            {
                increaseIdent();
                visit(*node.initializer);
                decreaseIdent();
            }
            STREAM << L"</initializer>" << std::endl;
        }

        STREAM << L"</variable_decl>" << std::endl;
    }

    void print(const TypedefDecl& node)
    {
        if(hasSourceInfo(node))
        {
            STREAM << L"<typedef_decl from=\"" << decodeType(node.type) << L"\" to=\"" << node.name->toString() << L"\">" << std::endl;
            {
                printSourceInfo(node);
            }
            STREAM << L"</typedef_decl>" << std::endl;
        }
        else
        {
            STREAM << L"<typedef_decl from=\"" << decodeType(node.type) << L"\" to=\"" << node.name->toString() << L"\"/>" << std::endl;
        }
    }

    void print(const TypenameDecl& node)
    {
        STREAM << L"<typename_decl " <<
                L"name=\"" << node.name->toString() << L"\" " <<
                L"type=\"" << decodeType(node.specialized_type) << L"\">" << std::endl;
        {
            printSourceInfo(node);
        }
        if(node.default_type)
        {
            STREAM << L"<default_type>" << std::endl;
            {
                increaseIdent();
                visit(*node.default_type);
                decreaseIdent();
            }
            STREAM << L"</default_type>" << std::endl;
        }

        STREAM << L"</typename_decl>" << std::endl;
    }

    //////////////////////////////////////////////////////////////////////
    /// Statement

    void print(const DeclarativeStmt& node)
    {
        STREAM << L"<declarative_stmt>" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            printAnnotation(node.getAnnotations());
        }
        {
            increaseIdent();
            visit(*node.declaration);
            decreaseIdent();
        }
        STREAM << L"</declarative_stmt>" << std::endl;
    }

    void print(const ExpressionStmt& node)
    {
        STREAM << L"<expression_stmt>" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            printAnnotation(node.getAnnotations());
        }
        {
            increaseIdent();
            visit(*node.expr);
            decreaseIdent();
        }
        STREAM << L"</expression_stmt>" << std::endl;
    }

    void print(const ForStmt& node)
    {
        STREAM << L"<for_stmt>" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            printAnnotation(node.getAnnotations());
        }
        {
            increaseIdent();
            if(node.init)
            {
                STREAM << L"<init>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.init);
                    decreaseIdent();
                }
                STREAM << L"</init>" << std::endl;
            }
            else
            {
                STREAM << L"<invalid_iterator/>" << std::endl;
            }
            if(node.cond)
            {
                STREAM << L"<cond>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.cond);
                    decreaseIdent();
                }
                STREAM << L"</cond>" << std::endl;
            }
            else
            {
                STREAM << L"<invalid_range/>" << std::endl;
            }
            if(node.step)
            {
                STREAM << L"<step>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.step);
                    decreaseIdent();
                }
                STREAM << L"</step>" << std::endl;
            }
            else
            {
                STREAM << L"<invalid_range/>" << std::endl;
            }
            if(node.block)
            {
                visit(*node.block);
            }
            else
            {
                STREAM << L"<null_block/>" << std::endl;
            }
            decreaseIdent();
        }
        STREAM << L"</for_stmt>" << std::endl;
    }

    void print(const ForeachStmt& node)
    {
        STREAM << L"<foreach_stmt>" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            printAnnotation(node.getAnnotations());
        }
        {
            increaseIdent();
            if(node.var_decl)
            {
                STREAM << L"<iterator>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.var_decl);
                    decreaseIdent();
                }
                STREAM << L"</iterator>" << std::endl;
            }
            else
            {
                STREAM << L"<invalid_iterator/>" << std::endl;
            }
            if(node.range)
            {
                STREAM << L"<range>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.range);
                    decreaseIdent();
                }
                STREAM << L"</range>" << std::endl;
            }
            else
            {
                STREAM << L"<invalid_range/>" << std::endl;
            }
            if(node.block)
            {
                visit(*node.block);
            }
            else
            {
                STREAM << L"<null_block/>" << std::endl;
            }
            decreaseIdent();
        }
        STREAM << L"</foreach_stmt>" << std::endl;
    }

    void print(const WhileStmt& node)
    {
        STREAM << L"<while_stmt type=\"" << WhileStmt::Style::toString(node.style) << "\">" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            printAnnotation(node.getAnnotations());
        }
        {
            increaseIdent();
            if(node.cond)
            {
                STREAM << L"<condition>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.cond);
                    decreaseIdent();
                }
                STREAM << L"</condition>" << std::endl;
            }
            else
            {
                STREAM << L"<invalid_condition/>" << std::endl;
            }
            if(node.block)
            {
                visit(*node.block);
            }
            else
            {
                STREAM << L"<null_block/>" << std::endl;
            }
            decreaseIdent();
        }
        STREAM << L"</while_stmt>" << std::endl;
    }

    void print(const IfElseStmt& node)
    {
        STREAM << L"<ifelse_stmt>" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            printAnnotation(node.getAnnotations());
        }
        {
            increaseIdent();
            {
                STREAM << L"<if>" << std::endl;
                {
                    increaseIdent();
                    {
                        STREAM << L"<condition>" << std::endl;
                        {
                            increaseIdent();
                            if(node.if_branch->cond) visit(*node.if_branch->cond);
                            decreaseIdent();
                        }
                        STREAM << L"</condition>" << std::endl;
                    }
                    {
                        if(node.if_branch->block)
                        {
                            visit(*node.if_branch->block);
                        }
                        else
                        {
                            STREAM << L"<null_block/>" << std::endl;
                        }
                    }
                    decreaseIdent();
                }
                STREAM << L"</if>" << std::endl;
            }
            int c = 0;
            for(auto* branch : node.elseif_branches)
            {
                STREAM << L"<elseif index=\"" << c << L"\">" << std::endl;
                {
                    increaseIdent();
                    {
                        STREAM << L"<condition>" << std::endl;
                        {
                            increaseIdent();
                            if(branch->cond) visit(*branch->cond);
                            decreaseIdent();
                        }
                        STREAM << L"</condition>" << std::endl;
                    }
                    {
                        if(branch->block)
                        {
                            visit(*branch->block);
                        }
                        else
                        {
                            STREAM << L"<null_block/>" << std::endl;
                        }
                    }
                    decreaseIdent();
                }
                STREAM << L"</elseif>" << std::endl;
                ++c;
            }
            {
                STREAM << L"<else>" << std::endl;
                {
                    increaseIdent();
                    {
                        if(node.else_block)
                        {
                            visit(*node.else_block);
                        }
                        else
                        {
                            STREAM << L"<null_block/>" << std::endl;
                        }
                    }
                    decreaseIdent();
                }
                STREAM << L"</else>" << std::endl;
            }
            decreaseIdent();
        }
        STREAM << L"</ifelse_stmt>" << std::endl;
    }

    void print(const SwitchStmt& node)
    {
        STREAM << L"<switch_stmt>" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            printAnnotation(node.getAnnotations());
        }
        {
            increaseIdent();
            {
                STREAM << L"<node>" << std::endl;
                {
                    increaseIdent();
                    if(node.node) visit(*node.node);
                    decreaseIdent();
                }
                STREAM << L"</node>" << std::endl;
            }
            size_t c = 0;
            for(auto* current_case : node.cases)
            {
                STREAM << L"<case index=\"" << c << "\">" << std::endl;
                {
                    increaseIdent();
                    {
                        STREAM << L"<condition>" << std::endl;
                        {
                            increaseIdent();
                            if(current_case->cond) visit(*current_case->cond);
                            decreaseIdent();
                        }
                        STREAM << L"</condition>" << std::endl;
                    }
                    {
                        if(current_case->block)
                        {
                            visit(*current_case->block);
                        }
                        else
                        {
                            STREAM << L"<null_block/>" << std::endl;
                        }
                    }
                    decreaseIdent();
                }
                STREAM << L"</case>" << std::endl;
                ++c;
            }
            {
                STREAM << L"<default>" << std::endl;
                {
                    increaseIdent();
                    {
                        if(node.default_block)
                        {
                            visit(*node.default_block);
                        }
                        else
                        {
                            STREAM << L"<null_block/>" << std::endl;
                        }
                    }
                    decreaseIdent();
                }
                STREAM << L"</default>" << std::endl;
            }
            decreaseIdent();
        }
        STREAM << L"</switch_stmt>" << std::endl;
    }

    void print(const BranchStmt& node)
    {
        if(node.opcode == BranchStmt::OpCode::RETURN)
        {
            STREAM << L"<branch_stmt opcode=\"" << BranchStmt::OpCode::toString(node.opcode) << L"\"/>" << std::endl;
            {
                printSourceInfo(node);
            }
            {
                printAnnotation(node.getAnnotations());
            }
            {
                increaseIdent();
                if(node.result)
                {
                    visit(*node.result);
                }
                else
                {
                    STREAM << L"<null_expression/>" << std::endl;
                }
                decreaseIdent();
            }
            STREAM << L"</branch_stmt>" << std::endl;
        }
        else
        {
            STREAM << L"<branch_stmt opcode=\"" << BranchStmt::OpCode::toString(node.opcode) << L"\">" << std::endl;
            {
                printSourceInfo(node);
            }
            {
                printAnnotation(node.getAnnotations());
            }
            STREAM << L"</branch_stmt>" << std::endl;
        }
    }

    //////////////////////////////////////////////////////////////////////
    /// Expression

    void print(const IdExpr& node)
    {
        STREAM << L"<id_expr>" << std::endl;

        increaseIdent();
        if(const auto*const id = node.getId()) visit(*id);
        decreaseIdent();

        STREAM << L"</id_expr>" << std::endl;
    }

    void print(const LambdaExpr& node)
    {
        STREAM << L"<lambda_expr>" << std::endl;

        increaseIdent();
        if(const auto*const lambda = node.getLambda()) visit(*lambda);
        decreaseIdent();

        STREAM << L"</lambda_expr>" << std::endl;
    }

    void print(const UnaryExpr& node)
    {
        STREAM << L"<unary_expr opcode=\"" << UnaryExpr::OpCode::toString(node.opcode) << L"\">" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            increaseIdent();
            visit(*node.node);
            decreaseIdent();
        }
        STREAM << L"</unary_expr>" << std::endl;
    }

    void print(const BinaryExpr& node)
    {
        STREAM << L"<binary_expr opcode=\"" << BinaryExpr::OpCode::toString(node.opcode) << L"\">" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            increaseIdent();
            {
                STREAM << L"<left>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.left);
                    decreaseIdent();
                }
                STREAM << L"</left>" << std::endl;
            }
            {
                STREAM << L"<right>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.right);
                    decreaseIdent();
                }
                STREAM << L"</right>" << std::endl;
            }
            decreaseIdent();
        }
        STREAM << L"</binary_expr>" << std::endl;
    }

    void print(const TernaryExpr& node)
    {
        STREAM << L"<ternary_expr>" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            increaseIdent();
            {
                STREAM << L"<condition>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.cond);
                    decreaseIdent();
                }
                STREAM << L"</condition>" << std::endl;
            }
            {
                STREAM << L"<true_node>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.true_node);
                    decreaseIdent();
                }
                STREAM << L"</true_node>" << std::endl;
            }
            {
                STREAM << L"<false_node>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.false_node);
                    decreaseIdent();
                }
                STREAM << L"</false_node>" << std::endl;
            }
            decreaseIdent();
        }
        STREAM << L"</ternary_expr>" << std::endl;
    }

    void print(const MemberExpr& node)
    {
        STREAM << L"<member_expr member=\"" << node.member->toString() << L"\">" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            increaseIdent();
            {
                STREAM << L"<left_hand_side>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.node);
                    decreaseIdent();
                }
                STREAM << L"</left_hand_side>" << std::endl;
            }
            decreaseIdent();
        }
        STREAM << L"</member_expr>" << std::endl;
    }

    void print(const CallExpr& node)
    {
        STREAM << L"<call_expr>" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            increaseIdent();
            {
                STREAM << L"<callee>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.node);
                    decreaseIdent();
                }
                STREAM << L"</callee>" << std::endl;
            }
            if(node.parameters.size() > 0)
            {
                STREAM << L"<parameters>" << std::endl;
                {
                    increaseIdent();
                    for(auto* param : node.parameters)
                    {
                        visit(*param);
                    }
                    decreaseIdent();
                }
                STREAM << L"</parameters>" << std::endl;
            }
            decreaseIdent();
        }
        STREAM << L"</call_expr>" << std::endl;
    }

    void print(const ArrayExpr& node)
    {
        STREAM << L"<array_expr>" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            increaseIdent();
            if(node.elements.size() > 0)
            {
                STREAM << L"<elements>" << std::endl;
                {
                    increaseIdent();
                    for(auto* impl : node.elements)
                    {
                        visit(*impl);
                    }
                    decreaseIdent();
                }
                STREAM << L"</elements>" << std::endl;
            }
            decreaseIdent();
        }
        STREAM << L"</vector_expr>" << std::endl;
    }

    void print(const CastExpr& node)
    {
        STREAM << L"<cast_expr type=\"" << decodeType(node.type) << L"\">" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            increaseIdent();
            {
                STREAM << L"<node>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.node);
                    decreaseIdent();
                }
                STREAM << L"</node>" << std::endl;
            }
            decreaseIdent();
        }
        STREAM << L"</cast_expr>" << std::endl;
    }

    void print(const IsaExpr& node)
    {
        STREAM << L"<isa_expr type=\"" << decodeType(node.type) << L"\">" << std::endl;
        {
            printSourceInfo(node);
        }
        {
            increaseIdent();
            {
                STREAM << L"<node>" << std::endl;
                {
                    increaseIdent();
                    visit(*node.node);
                    decreaseIdent();
                }
                STREAM << L"</node>" << std::endl;
            }
            decreaseIdent();
        }
        STREAM << L"</isa_expr>" << std::endl;
    }

    void print(const BlockExpr& node)
    {
        STREAM << L"<block_expr>" << std::endl;
        {
            printSourceInfo(node);
        }
        if(node.block)
        {
            increaseIdent();
            visit(*node.block);
            decreaseIdent();
        }
        STREAM << L"</block_expr>" << std::endl;
    }

private:
    bool hasSourceInfo(const ASTNode& node)
    {
        using namespace zillians::language::stage;

        if(!dump_source_info)
            return false;

        SourceInfoContext* source_info = SourceInfoContext::get((ASTNode*)&node);
        return (source_info != NULL);
    }

    void printSourceInfo(const ASTNode& node)
    {
        using namespace zillians::language::stage;

        if(!dump_source_info)
            return;

        SourceInfoContext* source_info = SourceInfoContext::get((ASTNode*)&node);
        if(source_info)
        {
            increaseIdent();
            {
                STREAM << L"<source_info line=\"" << source_info->line << "\" column=\"" << source_info->column << "\"/>" << std::endl;
            }
            decreaseIdent();
        }
    }

    void printAnnotation(Annotations* annotations)
    {
        if(annotations && annotations->annotation_list.size() > 0)
        {
            increaseIdent();
            {
                STREAM << L"<annotations>" << std::endl;
                {
                    increaseIdent();
                    for(auto* annotation : annotations->annotation_list)
                    {
                        STREAM << L"<annotation name=\"" << annotation->name->toString() << L"\">" << std::endl;
                        {
                            increaseIdent();
                            for(auto& key_value : annotation->attribute_list)
                            {
                                STREAM << L"<key>" << std::endl;
                                {
                                    increaseIdent();
                                    visit(*key_value.first);
                                    decreaseIdent();
                                }
                                STREAM << L"</key>" << std::endl;

                                STREAM << L"<value>" << std::endl;
                                {
                                    increaseIdent();
                                    visit(*key_value.second);
                                    decreaseIdent();
                                }
                                STREAM << L"</value>" << std::endl;
                            }
                            decreaseIdent();
                        }
                        STREAM << L"</annotation>" << std::endl;
                    }
                    decreaseIdent();
                }
                STREAM << L"</annotations>" << std::endl;
            }
            decreaseIdent();
        }
    }

    static std::wstring decodeType(ASTNode* type)
    {
        if (!type)
            return L"[unspecified]";

        TypeSpecifier* ts = cast<TypeSpecifier>(type);

        if (ts == nullptr)
            return L"[invalid]";

        if (const FunctionSpecifier* func_ts = cast<FunctionSpecifier>(ts))
            return decodeFunctionType(*func_ts);

        return ts->toString();
    }

    static std::wstring decodeFunctionType(const FunctionSpecifier& func_type)
    {
        std::wstringstream ss;

        ss << L"function(";
        ss << boost::algorithm::join(
            func_type.getParameterTypes() | boost::adaptors::transformed(decodeType),
            L", "
        );
        ss << ")";
        if(const auto*const return_type = func_type.getReturnType())
        {
            ss << L":";
            ss << return_type->toString();
        }

        return ss.str();
    }

    static std::basic_ostream<wchar_t>& set_indent(std::basic_ostream<wchar_t> &ss, size_t depth)
    {
        return ss << std::setfill(INDENT_CHAR) << std::setw(INDENT_WIDTH*depth) << L"";
    }

    int depth;

    bool dump_source_info;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_TREE_VISITOR_PRETTYPRINTVISITOR_H_ */
