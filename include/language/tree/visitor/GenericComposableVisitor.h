/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_VISITOR_GENERICCOMPOSABLEVISITOR_H_
#define ZILLIANS_LANGUAGE_TREE_VISITOR_GENERICCOMPOSABLEVISITOR_H_

#include "core/Prerequisite.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include <boost/fusion/container.hpp>
#include <boost/fusion/support/detail/access.hpp>
#include <boost/preprocessor/repetition/enum_params.hpp>
#include <boost/preprocessor/repetition/enum_params_with_a_default.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/include/for_each.hpp>

namespace zillians { namespace language { namespace tree { namespace visitor {

static const bool Composable = true;
static const bool Standalone = false;

template<BOOST_PP_ENUM_PARAMS_WITH_A_DEFAULT(FUSION_MAX_VECTOR_SIZE, typename T, boost::fusion::void_)>
struct GenericComposableVisitor : public GenericDoubleVisitor, boost::fusion::vector<BOOST_PP_ENUM_PARAMS(FUSION_MAX_VECTOR_SIZE, T)>
{
    typedef boost::fusion::vector<BOOST_PP_ENUM_PARAMS(FUSION_MAX_VECTOR_SIZE, T)> base_type;

    CREATE_INVOKER(applyInvoker, apply);

    // the following code uses boost PP to create multiple version of constructor
    // GenericComposableVisitor(T0)
    // GenericComposableVisitor(T0, T1)
    // GenericComposableVisitor(T0, T1, T2, ...)
    #include "language/tree/visitor/detail/GenericComposableVisitorCtor.h"

    struct invoke_visit
    {
        invoke_visit(ASTNode& node) : node(node) { }

        template<typename VisitorImpl>
        void operator()(VisitorImpl& visitor) const
        {
            visitor.visit(node);
        }

        ASTNode& node;
    };

    void apply(ASTNode& node)
    {
        boost::fusion::for_each(*this, invoke_visit(node));
        revisit(node);
    }
};

} } } }

#endif /* ZILLIANS_LANGUAGE_TREE_VISITOR_GENERICCOMPOSABLEVISITOR_H_ */
