/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_VISITOR_ASTGRAPHVIZGENERATOR_H_
#define ZILLIANS_LANGUAGE_TREE_VISITOR_ASTGRAPHVIZGENERATOR_H_

#include <iostream>
#include "core/Prerequisite.h"
#include "core/Visitor.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

using namespace zillians::language::tree;
using zillians::language::tree::visitor::GenericDoubleVisitor;

namespace zillians { namespace language { namespace stage { namespace visitor {

struct ASTGraphvizNodeGenerator : public GenericDoubleVisitor
{
    CREATE_INVOKER(mInvoker, label);

    ASTGraphvizNodeGenerator(std::wostream& os) : stream(os)
    {
        REGISTER_ALL_VISITABLE_ASTNODE(mInvoker);
    }

    ~ASTGraphvizNodeGenerator()
    { }

    void label(ASTNode& node);
    void label(Identifier& node);
    void label(UnaryExpr& node);
    void label(BinaryExpr& node);
    void label(Literal& node);
    void label(Type& node);
    void label(FunctionType& node);
    void label(TemplatedIdentifier& node);
    void label(Import& node);
    void label(Package& node);
    void label(Block& node);
    void label(BlockExpr& node);
    void label(IsaExpr& node);
    void label(CastExpr& node);
    void label(TypeSpecifier& node);
    void label(MultiSpecifier& node);
    void label(Declaration& node);
    void label(ClassDecl& node);
    void label(FunctionDecl& node);

    void addNode(ASTNode& node,
                 std::wstring label = L"",
                 const std::wstring& shape = L"",
                 const std::wstring& borderColor = L"",
                 std::wstring fillColor = L"");

    void open_sub(const std::wstring& name);
    void close_sub();
    void print_indent();

private:
    void inc_level();
    void dec_level();

    int level = 1;
    int subgraph_serial_num = 0;
    std::wostream& stream;
} ;

struct ASTGraphvizParentEdgeGenerator : public GenericDoubleVisitor
{
    CREATE_INVOKER(mInvoker, genParentEdge);

    ASTGraphvizParentEdgeGenerator(std::wostream& os) : os_(os)
    {
        REGISTER_ALL_VISITABLE_ASTNODE(mInvoker);
    }

    ~ASTGraphvizParentEdgeGenerator()
    {
    }

    void genParentEdge(zillians::language::tree::ASTNode& node);
    void addParentEdge(ASTNode* from, ASTNode* to, const std::wstring& label = L"", const std::wstring& color = L"");

private:
    std::wostream& os_;

};

struct ASTGraphvizChildEdgeGenerator : public GenericDoubleVisitor
{
    CREATE_INVOKER(mInvoker, genChildEdge);

    ASTGraphvizChildEdgeGenerator(std::wostream& os) : os_(os)
    {
        REGISTER_ALL_VISITABLE_ASTNODE(mInvoker);
    }

    ~ASTGraphvizChildEdgeGenerator()
    {
    }

    void genChildEdge(zillians::language::tree::ASTNode& node);
    void genChildEdge(zillians::language::tree::Annotation& node);
    void genChildEdge(zillians::language::tree::Annotations& node);
    void genChildEdge(zillians::language::tree::Internal& node);
    void genChildEdge(zillians::language::tree::Tangle& node);
    void genChildEdge(zillians::language::tree::Source& node);
    void genChildEdge(zillians::language::tree::Package& node);
    void genChildEdge(zillians::language::tree::Import& node);
    void genChildEdge(zillians::language::tree::Block& node);
    void genChildEdge(zillians::language::tree::Identifier& node);
    void genChildEdge(zillians::language::tree::SimpleIdentifier& node);
    void genChildEdge(zillians::language::tree::NestedIdentifier& node);
    void genChildEdge(zillians::language::tree::TemplatedIdentifier& node);
    void genChildEdge(zillians::language::tree::Literal& node);
    //void genChildEdge(zillians::language::tree::NumericLiteral& node);
    //void genChildEdge(zillians::language::tree::StringLiteral& node);
    //void genChildEdge(zillians::language::tree::ObjectLiteral& node);
    void genChildEdge(zillians::language::tree::TypeSpecifier& node);
    void genChildEdge(zillians::language::tree::FunctionSpecifier& node);
    void genChildEdge(zillians::language::tree::MultiSpecifier& node);
    void genChildEdge(zillians::language::tree::NamedSpecifier& node);
    void genChildEdge(zillians::language::tree::PrimitiveSpecifier& node);
    //void genChildEdge(zillians::language::tree::Type& node);
    void genChildEdge(zillians::language::tree::PrimitiveType& node);
    void genChildEdge(zillians::language::tree::PointerType& node);
    void genChildEdge(zillians::language::tree::ReferenceType& node);
    //void genChildEdge(zillians::language::tree::DeclType& node);
    void genChildEdge(zillians::language::tree::RecordType& node);
    void genChildEdge(zillians::language::tree::EnumType& node);
    void genChildEdge(zillians::language::tree::TypenameType& node);
    void genChildEdge(zillians::language::tree::TypedefType& node);
    void genChildEdge(zillians::language::tree::FunctionType& node);
    void genChildEdge(zillians::language::tree::Declaration& node);
    void genChildEdge(zillians::language::tree::ClassDecl& node);
    void genChildEdge(zillians::language::tree::EnumDecl& node);
    void genChildEdge(zillians::language::tree::TypedefDecl& node);
    void genChildEdge(zillians::language::tree::FunctionDecl& node);
    void genChildEdge(zillians::language::tree::VariableDecl& node);
    void genChildEdge(zillians::language::tree::TypenameDecl& node);
    void genChildEdge(zillians::language::tree::Statement& node);
    void genChildEdge(zillians::language::tree::DeclarativeStmt& node);
    void genChildEdge(zillians::language::tree::ExpressionStmt& node);
    //void genChildEdge(zillians::language::tree::IterativeStmt& node);
    void genChildEdge(zillians::language::tree::ForStmt& node);
    void genChildEdge(zillians::language::tree::ForeachStmt& node);
    void genChildEdge(zillians::language::tree::WhileStmt& node);
    void genChildEdge(zillians::language::tree::Selection& node);
    //void genChildEdge(zillians::language::tree::SelectionStmt& node);
    void genChildEdge(zillians::language::tree::IfElseStmt& node);
    void genChildEdge(zillians::language::tree::SwitchStmt& node);
    void genChildEdge(zillians::language::tree::BranchStmt& node);
    void genChildEdge(zillians::language::tree::Expression& node);
    void genChildEdge(zillians::language::tree::IdExpr& node);
    void genChildEdge(zillians::language::tree::LambdaExpr& node);
    void genChildEdge(zillians::language::tree::UnaryExpr& node);
    void genChildEdge(zillians::language::tree::BinaryExpr& node);
    void genChildEdge(zillians::language::tree::TernaryExpr& node);
    void genChildEdge(zillians::language::tree::MemberExpr& node);
    void genChildEdge(zillians::language::tree::CallExpr& node);
    void genChildEdge(zillians::language::tree::ArrayExpr& node);
    void genChildEdge(zillians::language::tree::CastExpr& node);
    void genChildEdge(zillians::language::tree::BlockExpr& node);
    void genChildEdge(zillians::language::tree::IsaExpr& node);
    void genChildEdge(zillians::language::tree::TieExpr& node);
    void genChildEdge(zillians::language::tree::IndexExpr& node);
    void genChildEdge(zillians::language::tree::UnpackExpr& node);
    void genChildEdge(zillians::language::tree::SystemCallExpr& node);
    void genChildEdge(zillians::language::tree::StringizeExpr& node);

    void addChildEdge(const ASTNode* parent, const ASTNode* child, const std::wstring& label = L"", const std::wstring& color = L"");

private:
    std::wostream& os_;

};

} } } } // namespace zillians::language::tree::visitor

#endif /* ZILLIANS_LANGUAGE_TREE_VISITOR_ASTGRAPHVIZGENERATOR_H_ */
