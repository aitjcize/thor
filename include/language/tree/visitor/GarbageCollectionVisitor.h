/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_VISITOR_GARBAGECOLLECTIONVISITOR_H_
#define ZILLIANS_LANGUAGE_TREE_VISITOR_GARBAGECOLLECTIONVISITOR_H_

#include "core/Prerequisite.h"
#include "language/tree/visitor/GenericComposableVisitor.h"

namespace zillians { namespace language { namespace tree { namespace visitor {

template<bool Composed = false>
struct GarbageCollectionVisitor : GenericDoubleVisitor
{
    CREATE_INVOKER(markInvoker, apply);

    GarbageCollectionVisitor() : nonreachable_set(ASTNodeGC::instance()->objects)
    {
        REGISTER_ALL_VISITABLE_ASTNODE(markInvoker)
    }

    ~GarbageCollectionVisitor()
    {
        ASTNodeGC::instance()->sweep(nonreachable_set);
    }

    inline std::size_t get_sweep_count()
    {
        return nonreachable_set.size();
    }

    void apply(ASTNode& node)
    {
        nonreachable_set.erase(&node);
        if(!Composed)
            revisit(node);
    }

    unordered_set<const ASTNode*> nonreachable_set;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_TREE_VISITOR_GARBAGECOLLECTIONVISITOR_H_ */
