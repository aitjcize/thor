/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_MEMBEREXPR_H_
#define ZILLIANS_LANGUAGE_TREE_MEMBEREXPR_H_

#include <ostream>

#include <boost/serialization/access.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace tree {

struct MemberExpr : public Expression
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(MemberExpr, Expression);

    explicit MemberExpr(Expression* node, Identifier* member);

    virtual bool hasValue() const override;
    virtual bool isRValue() const override;

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual MemberExpr* clone() const override;

    using BaseNode::toSource;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (node  )
            (member)
        );
    }

    Expression* node;
    Identifier* member;

protected:
    MemberExpr();
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_MEMBEREXPR_H_ */
