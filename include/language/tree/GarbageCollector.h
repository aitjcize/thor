/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_GARBAGECOLLECTOR_H_
#define ZILLIANS_LANGUAGE_TREE_GARBAGECOLLECTOR_H_

#include "core/Prerequisite.h"
#include "core/Visitor.h"
#include "core/Singleton.h"
#include "utility/Foreach.h"

namespace zillians { namespace language { namespace tree {

template<typename Base>
struct GarbageCollectorWrapper
{
    GarbageCollectorWrapper(std::unordered_set<Base*>& reachable_set) : reachable_set(reachable_set)
    { }

    inline void mark(Base* object)
    {
        reachable_set.erase(object);
    }

    std::unordered_set<Base*>& reachable_set;
};

template<typename Base>
struct GarbageCollector : Singleton<GarbageCollector<Base>, SingletonInitialization::automatic>
{
    GarbageCollector()
    { }

    ~GarbageCollector()
    {
        cleanup();
    }

    bool find(Base* object)
    {
        auto it = objects.find(object);
        return (it == objects.end());
    }

    void add(Base* object)
    {
        objects.insert(object);
    }

    void remove(Base* object)
    {
        auto it = objects.find(object);
        if(it != objects.end())
        {
            objects.erase(it);
        }
    }
//
//  // for some reason the following code does not work due to buggy template support?
//  template<typename MarkVisitor>
//  void gc(Base* root)
//  {
//      std::unordered_set<Base*> temporary(objects);
//
//      MarkVisitor visitor_impl(GarbageCollectorWrapper<Base>(temporary));
//      //visitor_impl;
//      //visitor_impl.visit(*root);
//      //visitor_impl.dummy();
//
//      printf("objects to remove = %ld\n", temporary.size());
//      //foreach(i, temporary)
//      for(auto i = temporary.begin(); i != temporary.end(); ++i)
//      {
//          remove(*i);
//          delete *i;
//      }
//  }

    void sweep(std::unordered_set<Base*>& nonreachable_set)
    {
        //printf("objects to remove = %ld\n", nonreachable_set.size());
        //foreach(i, nonreachable_set)
        for(auto i = nonreachable_set.begin(); i != nonreachable_set.end(); ++i)
        {
            remove(*i);
            delete *i;
        }
    }

    std::size_t cleanup()
    {
        std::size_t object_count_to_remove = objects.size();
        // TODO somehow this is not supported by GCC 4.4, so we have to explicit specify the iterator types
        //foreach(i, objects)
        for(auto i = objects.begin(); i != objects.end(); ++i)
        {
            delete *i;
        }
        objects.clear();
        return object_count_to_remove;
    }

    std::unordered_set<Base*> objects;
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_GARBAGECOLLECTOR_H_ */
