/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_BRANCHSTMT_H_
#define ZILLIANS_LANGUAGE_TREE_BRANCHSTMT_H_

#include <ostream>

#include <boost/serialization/access.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/tree/statement/Statement.h"

namespace zillians { namespace language { namespace tree {

struct BranchStmt : public Statement
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(BranchStmt, Statement);

    struct OpCode
    {
        enum type {
            BREAK   ,
            CONTINUE,
            RETURN  ,
        };

        static const wchar_t* toString(type t);
    };

    explicit BranchStmt(OpCode::type opcode, Expression* result = nullptr);

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual BranchStmt* clone() const override;

    using BaseNode::toSource;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (opcode)
            (result)
        );
    }

    OpCode::type opcode;
    Expression* result;

protected:
    BranchStmt();
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_BRANCHSTMT_H_ */
