/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDTYPECONTEXT_H_
#define ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDTYPECONTEXT_H_

#include "core/Prerequisite.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/stage/generator/detail/LLVMHeaders.h"

namespace zillians { namespace language { namespace stage {

struct SynthesizedTypeContext
{
	SynthesizedTypeContext() : type(NULL)
	{ }

    SynthesizedTypeContext(llvm::Type* type) : type(type)
    { }

    static SynthesizedTypeContext* get(tree::ASTNode* node)
    {
        return node->get<SynthesizedTypeContext>();
    }

    static void set(tree::ASTNode* node, SynthesizedTypeContext* ctx)
    {
        node->set<SynthesizedTypeContext>(ctx);
    }

    static void reset(tree::ASTNode* node)
    {
    	node->reset<SynthesizedTypeContext>();
    }

    llvm::Type* type;
};

#define GET_SYNTHESIZED_LLVM_TYPE(x) \
    ((SynthesizedTypeContext::get((x))) ? SynthesizedTypeContext::get((x))->type : NULL)

#define SET_SYNTHESIZED_LLVM_TYPE(x, t)  \
    { \
        if(SynthesizedTypeContext::get(x)) \
            SynthesizedTypeContext::get((x))->type = t; \
        else \
            SynthesizedTypeContext::set(x, new SynthesizedTypeContext(t)); \
    }

} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDTYPECONTEXT_H_ */
