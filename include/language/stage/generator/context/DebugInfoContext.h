/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_DEBUGINFOCONTEXT_H_
#define ZILLIANS_LANGUAGE_STAGE_DEBUGINFOCONTEXT_H_

#include <vector>

#include "core/IntTypes.h"

#include "language/tree/ASTNode.h"
#include "language/stage/generator/detail/LLVMHeaders.h"

namespace zillians { namespace language { namespace stage {

/// This is per program context
struct DebugInfoProgramContext
{
    void addProgramContext(llvm::DICompileUnit& compile_unit, llvm::DIFile& file)
    {
        compile_units.push_back(compile_unit);
        files.push_back(file);
    }

    static DebugInfoProgramContext* get(tree::ASTNode* node)
    {
        return node->get<DebugInfoProgramContext>();
    }

    static void set(tree::ASTNode* node, DebugInfoProgramContext* ctx)
    {
        node->set<DebugInfoProgramContext>(ctx);
    }

    static void reset(tree::ASTNode* node)
    {
    	node->reset<DebugInfoProgramContext>();
    }

    std::vector<llvm::DICompileUnit> compile_units;
    std::vector<llvm::DIFile> files;
};

struct DebugInfoContext
{
	DebugInfoContext()
	{ }

    DebugInfoContext(llvm::DICompileUnit compile_unit, llvm::DIFile file, llvm::DIDescriptor context) :
        context(context), compile_unit(compile_unit), file(file)
    { }

    DebugInfoContext(DebugInfoContext& dbg_context)
    {
        *this = dbg_context;
    }

    static DebugInfoContext* get(tree::ASTNode* node)
    {
        return node->get<DebugInfoContext>();
    }

    static void set(tree::ASTNode* node, DebugInfoContext* ctx)
    {
        node->set<DebugInfoContext>(ctx);
    }

    static void reset(tree::ASTNode* node)
    {
    	node->reset<DebugInfoContext>();
    }

    DebugInfoContext* clone()
    {
        return new DebugInfoContext(compile_unit, file, context);
    }

    // My own descriptor context
    llvm::DIDescriptor context;

    // which compile unit it belongs to
    llvm::DICompileUnit compile_unit;

    // which file it belongs to
    llvm::DIFile file;
};

struct DebugInfoTypeContext
{
	DebugInfoTypeContext() : bits(0), alignment(0)
	{ }

    DebugInfoTypeContext(llvm::DIType type, uint32 bits, uint32 alignment, unsigned encoding) :
    	type(type), bits(bits), alignment(alignment), encoding(encoding)
    { }

    static DebugInfoTypeContext* get(tree::ASTNode* node)
    {
        return node->get<DebugInfoTypeContext>();
    }

    static void set(tree::ASTNode* node, DebugInfoTypeContext* ctx)
    {
        node->set<DebugInfoTypeContext>(ctx);
    }

    static void reset(tree::ASTNode* node)
    {
    	node->reset<DebugInfoTypeContext>();
    }

    llvm::DIType type;
    uint32 bits;
    uint32 alignment;
    unsigned encoding;
};

} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_DEBUGINFOCONTEXT_H_ */
