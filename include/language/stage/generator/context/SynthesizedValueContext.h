/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDVALUECONTEXT_H_
#define ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDVALUECONTEXT_H_

#include "core/Prerequisite.h"
#include "core/Containers.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/stage/generator/detail/LLVMHeaders.h"

namespace zillians { namespace language { namespace stage {

struct SynthesizedValueContext
{
	SynthesizedValueContext() : v(NULL)
	{ }

    SynthesizedValueContext(llvm::Value* v) : v(v)
    { }

    static SynthesizedValueContext* get(const tree::ASTNode* node)
    {
        return node->get<SynthesizedValueContext>();
    }

    static void set(tree::ASTNode* node, SynthesizedValueContext* ctx)
    {
        node->set<SynthesizedValueContext>(ctx);
    }

    static void reset(tree::ASTNode* node)
    {
    	node->reset<SynthesizedValueContext>();
    }

    llvm::Value* v;
};

struct SynthesizedGuardValueContext
{
	SynthesizedGuardValueContext() : guard_value(NULL)
	{ }

    SynthesizedGuardValueContext(llvm::Value* v) : guard_value(v)
    { }

    static SynthesizedGuardValueContext* get(const tree::ASTNode* node)
    {
        return node->get<SynthesizedGuardValueContext>();
    }

    static void set(tree::ASTNode* node, SynthesizedGuardValueContext* ctx)
    {
        node->set<SynthesizedGuardValueContext>(ctx);
    }

    static void reset(tree::ASTNode* node)
    {
    	node->reset<SynthesizedGuardValueContext>();
    }

    llvm::Value* guard_value;
};

struct SynthesizedBlockContext
{
	SynthesizedBlockContext() : bb(NULL)
	{ }

    SynthesizedBlockContext(llvm::BasicBlock* bb) : bb(bb)
    { }

    static SynthesizedBlockContext* get(const tree::ASTNode* node)
    {
        return node->get<SynthesizedBlockContext>();
    }

    static void set(tree::ASTNode* node, SynthesizedBlockContext* ctx)
    {
        node->set<SynthesizedBlockContext>(ctx);
    }

    static void reset(tree::ASTNode* node)
    {
    	node->reset<SynthesizedBlockContext>();
    }

    llvm::BasicBlock* bb;
};

struct IntermediateValueContext
{
    IntermediateValueContext()
    { }

    static IntermediateValueContext* get(tree::ASTNode* node)
    {
        IntermediateValueContext* ctx = node->get<IntermediateValueContext>();
        if(!ctx)
        {
            ctx = new IntermediateValueContext();
            node->set<IntermediateValueContext>(ctx);
        }
        return ctx;
    }

    static void add(tree::ASTNode* node, llvm::Value* val)
    {
        get(node)->values.insert(val);
    }

    static void remove(tree::ASTNode* node, llvm::Value* val)
    {
        get(node)->values.erase(val);
    }

    static void reset(tree::ASTNode* node)
    {
    	node->reset<IntermediateValueContext>();
    }

    unordered_set<llvm::Value*> values;
};

struct SynthesizedFinalizeBlockContext
{
	SynthesizedFinalizeBlockContext() : bb(NULL)
	{ }

    SynthesizedFinalizeBlockContext(llvm::BasicBlock* bb) : bb(bb)
    { }

    static SynthesizedFinalizeBlockContext* get(const tree::ASTNode* node)
    {
        return node->get<SynthesizedFinalizeBlockContext>();
    }

    static void set(tree::ASTNode* node, SynthesizedFinalizeBlockContext* ctx)
    {
        node->set<SynthesizedFinalizeBlockContext>(ctx);
    }

    static void reset(tree::ASTNode* node)
    {
    	node->reset<SynthesizedFinalizeBlockContext>();
    }

    llvm::BasicBlock* bb;
};


#define ADD_INTERMEDIATE_LLVM_VALUE(x, val) \
    IntermediateValueContext::add(x, val)

#define REMOVE_INTERMEDIATE_LLVM_VALUE(x, val) \
    IntermediateValueContext::remove(x, val)

#define GET_INTERMEDIATE_LLVM_VALUES(x) \
    IntermediateValueContext::get(x)->values

#define GET_SYNTHESIZED_LLVM_VALUE(x) \
    ((SynthesizedValueContext::get(x)) ? SynthesizedValueContext::get(x)->v : NULL)

#define SET_SYNTHESIZED_LLVM_VALUE(x, val)  \
    { \
        if(SynthesizedValueContext::get(x)) \
            SynthesizedValueContext::get((x))->v = val; \
        else \
            SynthesizedValueContext::set(x, new SynthesizedValueContext(val)); \
    }

#define GET_SYNTHESIZED_LLVM_GUARD_VALUE(x) \
    ((SynthesizedGuardValueContext::get(x)) ? SynthesizedGuardValueContext::get(x)->guard_value : NULL)

#define SET_SYNTHESIZED_LLVM_GUARD_VALUE(x, val)  \
    { \
        if(SynthesizedGuardValueContext::get(x)) \
            SynthesizedGuardValueContext::get((x))->guard_value = val; \
        else \
            SynthesizedGuardValueContext::set(x, new SynthesizedGuardValueContext(val)); \
    }

#define GET_SYNTHESIZED_LLVM_BLOCK(x) \
    ((SynthesizedBlockContext::get(x)) ? SynthesizedBlockContext::get(x)->bb : NULL)

#define SET_SYNTHESIZED_LLVM_BLOCK(x, val)  \
    { \
        if(SynthesizedBlockContext::get(x)) \
            SynthesizedBlockContext::get((x))->bb = val; \
        else \
            SynthesizedBlockContext::set(x, new SynthesizedBlockContext(val)); \
    }

#define GET_SYNTHESIZED_LLVM_FINALIZE_BLOCK(x) \
    ((SynthesizedFinalizeBlockContext::get(x)) ? SynthesizedFinalizeBlockContext::get(x)->bb : NULL)

#define SET_SYNTHESIZED_LLVM_FINALIZE_BLOCK(x, val)  \
    { \
        if(SynthesizedFinalizeBlockContext::get(x)) \
            SynthesizedFinalizeBlockContext::get((x))->bb = val; \
        else \
            SynthesizedFinalizeBlockContext::set(x, new SynthesizedFinalizeBlockContext(val)); \
    }

} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDVALUECONTEXT_H_ */
