/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDOBJECTLAYOUTCONTEXT_H_
#define ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDOBJECTLAYOUTCONTEXT_H_

#include "core/Prerequisite.h"
#include "core/Containers.h"
#include "language/tree/ASTNodeFactory.h"

namespace zillians { namespace language { namespace stage {

struct SynthesizedObjectLayoutContext
{
    friend class boost::serialization::access;

    using OFFSET_IN_INDEX   = uint32;
    using OFFSET_IN_BYTES   = uint64;
    using SIZE_IN_BYTES     = uint32;
    using MEMBER_ATTRIBUTES = boost::tuple<OFFSET_IN_INDEX, OFFSET_IN_BYTES, SIZE_IN_BYTES>;

    SynthesizedObjectLayoutContext() : object_size(0)
    { }

    static SynthesizedObjectLayoutContext* get(const tree::ASTNode* node)
    {
        return node->get<SynthesizedObjectLayoutContext>();
    }

    static void set(tree::ASTNode* node, SynthesizedObjectLayoutContext* ctx)
    {
        node->set<SynthesizedObjectLayoutContext>(ctx);
    }

    static void reset(tree::ASTNode* node)
    {
    	node->reset<SynthesizedObjectLayoutContext>();
    }

    template<typename Archive>
    void serialize(Archive& ar, unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        ar & object_size;
        ar & member_attributes;
        ar & vptr_index;
        ar & class_offset;
    }

    // Total size of the structure in bytes
    uint64 object_size;

    // The index of the member variable in the llvm struct layout
    std::map<const tree::VariableDecl*, MEMBER_ATTRIBUTES> member_attributes;

    // The index of the virtual table pointer in the llvm struct layout
    std::vector<OFFSET_IN_INDEX> vptr_index;

    // The map between a ClassDecl pointer and its offset from base
    std::map<const tree::ClassDecl*, std::pair<OFFSET_IN_INDEX, OFFSET_IN_BYTES>> class_offset;
};


#define GET_SYNTHESIZED_OBJECTLAYOUT(x) \
    ((SynthesizedObjectLayoutContext::get(x)) ? SynthesizedObjectLayoutContext::get(x) : NULL)

#define SET_SYNTHESIZED_OBJECTLAYOUT(x, v)\
    { \
        SynthesizedObjectLayoutContext::set(x, v); \
    }

} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDOBJECTLAYOUTCONTEXT_H_ */
