/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMDEBUGINFOGENERATORSTAGEVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMDEBUGINFOGENERATORSTAGEVISITOR_H_

#include "core/Prerequisite.h"
#include <boost/filesystem.hpp>

#include "utility/Filesystem.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/generator/detail/LLVMHelper.h"
#include "language/stage/generator/context/DebugInfoContext.h"
#include "utility/UnicodeUtil.h"

using namespace zillians::language::tree;
using zillians::language::tree::visitor::GenericDoubleVisitor;

namespace zillians { namespace language { namespace stage { namespace visitor {


struct LLVMDebugInfoGeneratorStageVisitor: public GenericDoubleVisitor
{
    CREATE_INVOKER(generateInvoker, generate);

    LLVMDebugInfoGeneratorStageVisitor(llvm::LLVMContext& context, llvm::Module& current_module, llvm::DIBuilder& factory);

    virtual ~LLVMDebugInfoGeneratorStageVisitor();

    void generate(ASTNode& node);
    void generate(FunctionType& node);
    void generate(Source& node);
    void generate(CallExpr& node);
    void generate(TypeSpecifier& node);
    void generate(EnumDecl& node);
    void generate(ClassDecl& node);
    void generate(FunctionDecl& node);
    void generate(VariableDecl& node);

private:
    bool createDIType(TypeSpecifier& node, llvm::DIType& type, uint32& bits, uint32& alignment, unsigned& encoding);
    void createClassMembersDebugInfo(ClassDecl& node, std::vector<llvm::Value*>& elements, DebugInfoContext* debuginfo_context);
    llvm::DICompositeType createSubroutineType(FunctionDecl& node, llvm::DIFile file);
    llvm::DIType createMemberVariableDebugInfo(VariableDecl& node, DebugInfoContext* context);
    void generateGlobalVariableDebugInfo(VariableDecl& node);
    void generateStaticVariableDebugInfo(VariableDecl& node, bool is_class_data_member);
    void generateVariableDebugInfo(VariableDecl& node, unsigned variable_type, int argument_position = 0);
    llvm::DIVariable createVariableDebugInfoDeclare(llvm::StringRef name, unsigned variable_type, llvm::DIType type, llvm::Value* llvm_value,
            llvm::BasicBlock* block, DebugInfoContext* debug_info, SourceInfoContext* source_info, int argument_position = 0);
    bool getPrimitiveTypeInfo(PrimitiveKind kind, uint32& bits, uint32& alignment, unsigned& encoding);
    bool createStructPointerType(ClassDecl& class_decl, llvm::DIType& debug_info_type, uint32& bits, uint32& alignment, unsigned& encoding);
    bool createMultiType(MultiSpecifier& node, llvm::DIType& debug_info_type, uint32& bits, uint32& alignment, unsigned& encoding);
    bool createPrimitiveType(PrimitiveKind kind, llvm::DIType& debug_info_type, uint32& bits, uint32& alignment, unsigned& encoding);
    void insertDebugLocationForBlock(ASTNode& node);
    DebugInfoContext* getOrInheritDebugInfo(ASTNode& node);
    void insertDebugLocation(ASTNode& node);
    void insertDebugLocationForIntermediateValues(ASTNode& node);

private:
    LLVMHelper mHelper;
    llvm::IRBuilder<> mBuilder;

    llvm::LLVMContext& context;
    llvm::Module& current_module;

    llvm::DIBuilder& factory;
};

}}}}

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMDEBUGINFOGENERATORSTAGEVISITOR_H_ */
