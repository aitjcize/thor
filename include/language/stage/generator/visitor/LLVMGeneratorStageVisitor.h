/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORSTAGEVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORSTAGEVISITOR_H_

#include <llvm/Config/config.h>

#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 3
    #include <llvm/IR/InlineAsm.h>
#else
    #include <llvm/InlineAsm.h>
#endif

#include "core/Prerequisite.h"
#include "language/context/ManglingStageContext.h"
#include "language/context/ResolverContext.h"
#include "language/context/TransformerContext.h"
#include "language/GlobalConstant.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/generator/detail/LLVMHelper.h"
#include "language/stage/generator/context/SynthesizedAllocaThisPointerContext.h"
#include "language/stage/generator/context/SynthesizedFunctionContext.h"
#include "language/stage/generator/context/SynthesizedValueContext.h"
#include "language/stage/generator/context/SynthesizedVirtualFunctionContext.h"
#include "language/stage/generator/context/SynthesizedVTableContext.h"
#include <boost/algorithm/string.hpp>

using namespace zillians::language::tree;

namespace zillians { namespace language { namespace stage { namespace visitor {

/**
 * LLVMGeneratorVisitor walks the AST tree and generates LLVM instructions
 *
 * As LLVMGeneratorVisitor traverse the AST tree only once, required information must be prepared prior to the visitor,
 * including any necessary transformation.
 *
 * @see LLVMGeneratorPreambleVisitor
 */
struct LLVMGeneratorStageVisitor : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(generateInvoker, generate);

    LLVMGeneratorStageVisitor(llvm::LLVMContext& context, llvm::Module& module);

    void generate(ASTNode& node);
    void generate(NormalBlock& node);
    void generate(AtomicBlock& node);
    void generate(NumericLiteral& node);
    void generate(ObjectLiteral& node);
    void generate(StringLiteral& node);
    void generate(TypeIdLiteral& node);
    void generate(SymbolIdLiteral& node);
    void generate(FunctionIdLiteral& node);
    void generate(Tangle& node);
    void generate(Source& node);
    void generate(ClassDecl& node);
    void generate(FunctionDecl& node);
    void generate(VariableDecl& node);
    void generate(DeclarativeStmt& node);
    void generate(BranchStmt& node);
    void generate(ExpressionStmt& node);
    void generate(IfElseStmt& node);
    void generate(ForStmt& node);
    void generate(ForeachStmt& node);
    void generate(WhileStmt& node);
    void generate(SwitchStmt& node);
    void generate(IdExpr& node);
    void generate(UnaryExpr& node);
    void generate(BinaryExpr& node);
    void generate(TernaryExpr& node);
    void generate(CallExpr& node);
    void generate(CastExpr& node);
    void generate(MemberExpr& node);
    void generate(BlockExpr& node);
    void generate(UnpackExpr& node);
    void generate(SystemCallExpr& node);

private:
    llvm::Function* getRootsetAdder               ();
    llvm::Function* getAsyncInvocationAdder       ();
    llvm::Function* getAsyncInvocationReserver    ();
    llvm::Function* getAsyncInvocationCommiter    ();
    llvm::Function* getAsyncInvocationAborter     ();
    llvm::Function* getAsyncInvocationAppender    (const Type& type);
    llvm::Function* getAsyncInvocationRetPtrSetter();

    llvm::Value* generateAsyncCallParamThis(const FunctionDecl& callee, Expression& callee_this_expr);
    llvm::Value* generateAsyncCallParamNonThis(Expression& param_expr);
    llvm::Value* generateAsyncCallReturnPtrSet(llvm::Value* async_id, Expression* assignee_expr);

    template<typename RangeType>
    llvm::Value* generateAsyncCallParams(SystemCallExpr& system_call, Expression* assignee_expr, Expression& func_id_expr, const RangeType& parameters);
    llvm::Value* generateAsyncCallNoParams(SystemCallExpr& system_call, Expression* assignee_expr, Expression& func_id_expr);

private:
    struct ReadWriteValue
    {
        llvm::Value* read;
        llvm::Value* write;
    };

private:
    bool isSuperMember(ASTNode& node);
    bool isStatic(ASTNode* resolved_symbol);
    void specialFunctionHandling(FunctionDecl& node);
    void callAddToGlobalRootSet(llvm::Value* llvm_value);
    llvm::Function* generateAddToGlobalRootSet();
    llvm::Function* generateGuardAcquire();
    llvm::Function* generateGuardRelease();
    llvm::Function* generateDynCastImpl();
    llvm::Value* loadThisPointer(FunctionDecl& node);
    void callDefaultFunction(FunctionDecl& from_function, ClassDecl& to_class, std::wstring function_name);
    void callBaseDestructor(FunctionDecl& dtor_node);
    void implementDeleteDestructor(FunctionDecl& dtor_node);
    void callBaseConstructor(FunctionDecl& ctor_node);
    void initializeVTT(FunctionDecl& ctor_node);
    llvm::Value* getVirtualFunction(ClassDecl& ast_class, FunctionDecl& ast_function, llvm::Value* this_value);

    // this is for NVVM only, which has several built-in intrinsics to perform address space conversion
    llvm::Function* getAddrSpaceConversionIntrinsics(unsigned from, unsigned to, int point_to_bitsize = 8);
    llvm::BasicBlock* createBasicBlock(llvm::StringRef name = "", llvm::Function* parent = NULL, llvm::BasicBlock* before = NULL);
    void enterBasicBlock(llvm::BasicBlock* block, bool emit_branch_if_necessary = true);
    llvm::AllocaInst* createAlloca(VariableDecl* node, llvm::Type* type, const llvm::Twine& name = "");
    bool createAlloca(VariableDecl& ast_variable);
    bool startFunction(FunctionDecl& ast_function);
    bool allocateParameters(FunctionDecl& ast_function);
    bool finishFunction(FunctionDecl& ast_function);
    void generateLocalVariable(VariableDecl& var);

private:

    void shortCircuitEval(BinaryExpr& node);
    llvm::BranchInst* createCondBr(llvm::Value* value, llvm::BasicBlock* true_block, llvm::BasicBlock* false_block);

private:
    typedef bool require_read;
    typedef bool require_write;

    llvm::Value* getReadValue(ASTNode& attach, ASTNode& node);
    llvm::Value* getWriteValue(ASTNode& attach, ASTNode& node);
    llvm::Value* getAtomicReadValue     (ASTNode& node, llvm::Value* read_value);
    llvm::Value* getNonAssignedReadValue(ASTNode& attach, ASTNode& node, llvm::Value* read_value);
    llvm::Value* getAssignedReadValue   (ASTNode& attach, ASTNode& node, llvm::Value* read_value);
    llvm::Value* getNonVariableReadValue(ASTNode& attach, ASTNode& node, llvm::Value* read_value);

private:
    bool isFunctionParameter(ASTNode& node);
    int getFunctionParameterIndex(ASTNode& node);
    FunctionDecl* getContainingFunction(ASTNode& node);
    bool hasValue(ASTNode& ast_node);
    bool isResolved(ASTNode& ast_node);
    bool isFunctionVisited(FunctionDecl& ast_function);
    bool isBlockInsertionMasked();
    void setBlockInsertionMask();
    void resetBlockInsertionMask();
    llvm::BasicBlock* currentBlock();
    bool isBlockTerminated(llvm::BasicBlock* block);

private:
    bool propagate(ASTNode* to, ASTNode* from);
    void blockInitialize();
    void blockFinalize();
    llvm::Function* genLLVMExternFunc(const std::string& name, llvm::Type* return_type, const std::vector<llvm::Type*>& param_types);
    void generateAtomicDataStructures();
    bool isBeAssigned(Expression& node);

public:
    llvm::LLVMContext &mContext;
    llvm::Module& mModule;
    llvm::IRBuilder<> mBuilder;
    LLVMHelper mHelper;
    std::vector<llvm::Function*> mGlobalCtors;

    struct {
        llvm::Function* function;
        llvm::BasicBlock* entry_block;
        llvm::BasicBlock* return_block;
        llvm::BasicBlock* continue_block;
        llvm::BasicBlock* break_block;
        llvm::Instruction* alloca_insert_point;
        std::vector< llvm::Value* > return_values;
        bool mask_insertion;
        //std::stack<llvm::BasicBlock*> block_stack;
    } mFunctionContext;

    // atomic block
    struct AtomicCtxS {
        AtomicCtxS()
            : tm_read(static_cast<int>(PrimitiveKind::FLOAT64_TYPE) + 2)
            , tm_write(static_cast<int>(PrimitiveKind::FLOAT64_TYPE) + 2)
        {}

        bool isInAtomicBlock() const { return inAtomicBlock; }
        void setInAtomicBlock() { inAtomicBlock = true; }
        void clearInAtomicBlock() { inAtomicBlock = false; }

        bool inAtomicBlock;
        llvm::AllocaInst* tx;
        llvm::Type*       ty_TxThread;
        llvm::Type*       ty_JmpBuf;

        llvm::Function*   sys_init;
        llvm::Function*   thread_init;
        llvm::Function*   thread_shutdown;
        llvm::Function*   sys_shutdown;
        llvm::Function*   tm_begin;
        llvm::Function*   tm_commit;

        llvm::Function*   a_setjmp;
        llvm::Value*      a_stm_selt;

        // if value of FLOAT64_TYPE is 8, the length of array must be 10, the last is for pointer
        std::vector<llvm::Function*>   tm_read;
        std::vector<llvm::Function*>   tm_write;

        const static size_t POINTER_TYPE = PrimitiveKind::FLOAT64_TYPE + 1;

        llvm::Function* getFunctionT(ASTNode* node, const std::vector<llvm::Function*>& tm_functions) {
            Type* resolved_type = ASTNodeHelper::getCanonicalType(node);
            if(PrimitiveType* pri_type = resolved_type->getAsPrimitiveType()) {
                return tm_functions[pri_type->getKind()];
            }
            else if(ClassDecl* cd = resolved_type->getAsClassDecl()) {
                return tm_functions[POINTER_TYPE];
            }
            else {
                UNREACHABLE_CODE();
                return nullptr;
            }
        }

        llvm::Function* getReadFunction(ASTNode* node) {
            return getFunctionT(node, tm_read);
        }

        llvm::Function* getWriteFunction(ASTNode* node) {
            return getFunctionT(node, tm_write);
        }

    };

    AtomicCtxS mAtomic;
};


} } } }


#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORSTAGEVISITOR_H_ */
