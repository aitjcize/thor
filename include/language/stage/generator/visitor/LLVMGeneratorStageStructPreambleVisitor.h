/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORSTRUCTPREAMBLESTAGEVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORSTRUCTPREAMBLESTAGEVISITOR_H_

#include "core/Prerequisite.h"
#include "utility/StringUtil.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/generator/detail/LLVMHelper.h"

using namespace zillians::language::tree;
using zillians::language::tree::visitor::GenericDoubleVisitor;

namespace zillians { namespace language { namespace stage { namespace visitor {

/**
 * LLVMGeneratorStageStructPreambleVisitor is used to generate forward declaration for all ClassDecl
 *
 * @see LLVMGeneratorStagePreambleVisitor
 */
struct LLVMGeneratorStageStructPreambleVisitor : public GenericDoubleVisitor
{
    typedef std::pair<ClassDecl*, uint64> traversed_node_t;

    CREATE_INVOKER(generateInvoker, generate);

    LLVMGeneratorStageStructPreambleVisitor(llvm::LLVMContext& context, llvm::Module& module) ;

    void generate(ASTNode& node);
    void generate(ClassDecl& node);

public:
    void generateForwardDeclaration(bool forward_decl_only);

public:
    bool mGenerateForwardDeclaration;

private:
    llvm::LLVMContext &mContext;
    llvm::Module& mModule;
    llvm::IRBuilder<> mBuilder;
    LLVMHelper mHelper;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORSTRUCTPREAMBLESTAGEVISITOR_H_ */
