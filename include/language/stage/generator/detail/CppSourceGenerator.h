/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_DETAIL_CPPGENERATORHELPER_H_
#define ZILLIANS_LANGUAGE_STAGE_DETAIL_CPPGENERATORHELPER_H_

#include <functional>

#include "language/tree/ASTNodeHelper.h"

namespace zillians { namespace language { namespace stage {

using namespace zillians::language::tree;

class CppSourceGenerator
{
public:
    using BaseDerivePair = std::pair<Declaration*, Declaration*>;
    using WithInTemplateQuate = bool;
    using AddStar = bool;
    using Stream = std::wostream;

public:
    CppSourceGenerator(bool cuda_mode = false);

public:

    void setWrite(Stream* new_stream);

    std::vector<Declaration*> sortDeclByInheritence(std::set<Declaration*>& classDecls, std::set<BaseDerivePair>& baseDerivedPairs);
    void genVisibility(Declaration::VisibilitySpecifier::type v);
    void genNonNSQualifiedName(const bool addStar, Identifier* id);
    void genNamespaceQualifier(ASTNode* node);
    void genNamespaceQualifiedType(const bool addStar, TypeSpecifier* type);
    void genNamespaceQualifiedType(const bool addStar, FunctionSpecifier* type);
    void genNamespaceQualifiedType(const bool addStar, NamedSpecifier* type);
    void genNamespaceQualifiedType(const bool addStar, PrimitiveSpecifier* type);
    void genStatic(bool isStatic);
    void genVirtual(bool isVirtual);
    void genDataMember(VariableDecl* var);
    void genCppClassName(Identifier* id);
    void genCppMemberFunctionTemplateQuaifier(Identifier* id);
    void genCppSimpleName(Identifier* id);
    void genParameter(VariableDecl* var);
    void genParameters(std::vector<VariableDecl*>& parameters);
    void genMemberFunction(FunctionDecl* func);
    void genBases(NamedSpecifier* base, std::vector<NamedSpecifier*>& implements);
    void openCppNamespace(Declaration* decl);
    void closeCppNamespace(Declaration* decl);
    void genPreIncludeForwardDeclaration(std::set<Declaration*>& classDecls);
    void genForwardDeclaration(std::set<Declaration*>& classDecls);
    void genEnumDefinitions(std::set<EnumDecl*>& enumDecls);
    void genClassDecls(std::vector<Declaration*>& classDecls);
    void genPrimitiveType();
    void genClassExplicitInstantiation(std::set<Declaration*>& instantiationSet);
    void genFunctionExplicitInstantiation(std::set<Declaration*>& instantiationSet);
    void genIncludeImplementation(std::set<Declaration*>& nativeClasses);
    void genClassNameQualifiedFunctionName(FunctionDecl* funcDecl);

private:
    bool isCtorOrDtor(FunctionDecl* func);
    void genClassDecl(ClassDecl* classDecl);

private:

    bool cuda_mode;
    Stream* output;
};

} } }


#endif /* ZILLIANS_LANGUAGE_STAGE_DETAIL_CPPGENERATORHELPER_H_ */
