/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_THORBUILDGRAPH_H_
#define ZILLIANS_LANGUAGE_STAGE_THORBUILDGRAPH_H_

#include "core/Prerequisite.h"
#include "utility/GraphUtil.h"
#include "utility/Filesystem.h"

namespace zillians { namespace language { namespace stage {

class ThorBuildGraph
{
public:
    ThorBuildGraph(const boost::filesystem::path& root);
    ~ThorBuildGraph();

public:
    void setPrependPackage(const std::wstring& prepend_package);

    /**
     * Parse the given file to learn its package and dependency and add it to build graph
     * @param src
     * @return
     */
    bool addSource(const boost::filesystem::path& src);

    /**
     * Load imported AST file and find out the contained packages and add it to build graph
     * @param external_src
     * @return
     */
    bool addImport(const boost::filesystem::path& ast);

public:
    /**
     * 1. build the SCC (strong-connected-component) of the build graph
     * 2. and treat each component as tangle
     * 3. and use topological sort to find the correct ordering among tangles
     * 4. and launch builder for each tangle
     *
     * Note that if any builder function returns non-zero, we will terminate the build
     *
     * @param concurrency
     * @param builder
     * @return
     */

    bool build(std::function<int(std::vector<boost::filesystem::path>& /*source*/, std::vector<boost::filesystem::path>& /*imports*/, std::vector<boost::filesystem::path>& /*outputs*/)> builder, int concurrency = 1);

private:
    bool enumerateSourcePackage(const boost::filesystem::path& p, /*OUT*/ std::wstring& package);
    bool enumerateImportPackages(const boost::filesystem::path& p, /*OUT*/ std::vector<std::wstring>& packages);
    bool parseSourceImportedPackages(const boost::filesystem::path& p, /*OUT*/ std::vector<std::wstring>& packages);

private:
    /// we add std::string as additional vertex property to store the source file name, which can be either *.t or *.ast
    typedef indirect_graph_traits<std::string /*vertex_property*/, int /*edge_property*/> IndirectGraphTraits;

    typedef boost::property<boost::vertex_index_t, long,
            boost::property<boost::vertex_color_t, boost::default_color_type,
            boost::property<boost::vertex_name_t, std::string,
            boost::property<boost::vertex_discover_time_t, long,
            boost::property<boost::vertex_root_t, long,
            IndirectGraphTraits::vertex_property> > > > > VertexProperty;

    typedef boost::property<boost::edge_name_t, std::string,
            IndirectGraphTraits::edge_property> EdgeProperty;

    typedef boost::adjacency_list<boost::setS, boost::listS, boost::bidirectionalS, VertexProperty, EdgeProperty> Graph;
    typedef boost::graph_traits<Graph> Traits;

    typedef Traits::edge_descriptor EdgeDescriptor;
    typedef Traits::vertex_descriptor VertexDescriptor;
    typedef Traits::vertex_iterator VertexIter;
    typedef Traits::in_edge_iterator in_edge_iter;
    typedef Traits::out_edge_iterator out_edge_iter;

private:
    const boost::filesystem::path& mRootPath;
    std::wstring mPrependPackage;

private:
    int mVertexIndex;
    Graph mDependencyGraph;
    indirect_graph_mapping<IndirectGraphTraits, VertexDescriptor, EdgeDescriptor>  mDependencyGraphMapping;
    boost::property_map<Graph, boost::vertex_index_t>::type mIndex; /// the vertex index for topological sort(for listS)
//  boost::property_map<Graph, boost::vertex_color_t>::type mGroupFlag;///< the flag to mark traveled vertices in recursive work
//  boost::property_map<Graph, boost::vertex_name_t>::type mName;///<the vertex property keeping current version of the module
//  boost::property_map<Graph, boost::vertex_discover_time_t>::type mDiscoverTime;///<the vertex property keeping current version of the module

    std::multimap<std::wstring, VertexDescriptor> mPackagesToFiles;
};

} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_THORBUILDGRAPH_H_ */
