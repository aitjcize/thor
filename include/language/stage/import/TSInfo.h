/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_IMPORT_TSINFO_H_
#define ZILLIANS_LANGUAGE_STAGE_IMPORT_TSINFO_H_

#include <set>
#include <string>
#include <vector>

#include <boost/variant.hpp>

namespace zillians { namespace language { namespace stage { namespace import {

struct ts_none {};

enum class ts_primitive
{
    VOID   ,
    BOOL   ,
    INT8   ,
    INT16  ,
    INT32  ,
    INT64  ,
    FLOAT32,
    FLOAT64,
};

enum class ts_builtin
{
    C_STRING,
    C_WSTRING,
};

class ts_node {
public:
    ts_node();
    virtual ~ts_node() = 0;

public:
    static void clean();

};

// forward declarations
struct ts_class;
struct ts_decl;
struct ts_identifier;
struct ts_function;
struct ts_package;
struct ts_parameter;

typedef boost::variant<
    ts_none,
    ts_primitive,
    ts_builtin,
    ts_class*
> ts_type;

struct ts_identifier
    : public ts_node
{
    std::string name;
};

struct ts_decl
    : public ts_node
{
    ts_package*      scope = nullptr;
    ts_class*        origin_scope = nullptr;
    ts_class*        member_of = nullptr;
    ts_identifier*   id = nullptr;
    std::string      wrapped_name;

    virtual ts_package* get_package() const noexcept;
};

struct ts_parameter
    : public ts_decl
{
    enum wrap_method
    {
        BY_PASS  ,
        POINTER  ,
        VALUE    ,
        REFERENCE,
        C_STRING ,
        C_WSTRING,
    };

    ts_function*   parameter_of = nullptr;
    ts_type        type;
    unsigned       wrapping = BY_PASS;

    virtual ts_package* get_package() const noexcept override;
};

struct ts_function
    : public ts_decl
{
    enum wrap_method
    {
        BY_PASS    ,
        COPY_CREATE,
    };

    enum function_type
    {
        NORMAL_FUNCTION,
        CONSTRUCTOR    ,
        DESTRUCTOR     ,
    };

    ts_type                      result_type;
    unsigned                     result_wrapping = BY_PASS;
    std::vector<ts_parameter*>   parameters;
    function_type                type = NORMAL_FUNCTION;

    bool is_constructor() const noexcept;
    bool is_destructor() const noexcept;
};

struct ts_getset
    : public ts_decl
{
    enum getter_wrap_method
    {
        GETTER_NO_WRAP    = 0x00,
        GETTER_BY_PASS    = 0x01,
        GETTER_ADDRESS_OF = 0x02,
        GETTER_ARRAY_ELEM = 0x04,
        GETTER_ARRAY_SIZE = 0x08,
    };

    enum setter_wrap_method
    {
        SETTER_NO_WRAP     = 0x00,
        SETTER_BY_PASS     = 0x01,
        SETTER_COPY_ASSIGN = 0x02,
        SETTER_ARRAY_ELEM  = 0x04,
    };

    unsigned getter_wrapping = GETTER_NO_WRAP;
    unsigned setter_wrapping = SETTER_NO_WRAP;

    ts_type type;

    bool has_getter() const noexcept;
    bool has_getter_by_pass() const noexcept;
    bool has_getter_address_of() const noexcept;
    bool has_getter_array_element() const noexcept;
    bool has_getter_array_size() const noexcept;

    bool has_setter() const noexcept;
    bool has_setter_by_pass() const noexcept;
    bool has_setter_copy_assign() const noexcept;
    bool has_setter_array_element() const noexcept;
};

struct ts_class
    : public ts_decl
{
    std::vector<ts_function*> constructors;
    std::vector<ts_function*> destructors;

    std::vector<ts_getset*>   getsets;
};

struct ts_package
    : public ts_decl
{
    typedef boost::variant<
        ts_class*   ,
        ts_function*
    >                                declaration_variant;

    struct comparer
    {
        bool operator()(const ts_package* lhs, const ts_package* rhs) const;
    };

    std::vector<declaration_variant>  declarations;
    std::set<ts_package*, comparer>   children;
    std::set<ts_package*>             refered_packages;
    std::set<ts_builtin>              used_builtins;
    unsigned                          depth = 0;
    const std::vector<std::string>*   wrapped_includes = nullptr;

    ts_package& get_common_parent(const ts_package& another) const;
    const std::vector<std::string>& get_wrapped_includes() const;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_IMPORT_TSINFO_H_ */
