/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_TS_PACKAGE_GRAMMAR_H_
#define ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_TS_PACKAGE_GRAMMAR_H_

#include <boost/spirit/include/karma_grammar.hpp>
#include <boost/spirit/include/karma_rule.hpp>

#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/TsClassGrammar.h"
#include "language/stage/import/grammar/TsFunctionGrammar.h"
#include "language/stage/import/grammar/TsImportGrammar.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
struct ts_package_grammar : public boost::spirit::karma::grammar<iterator, ts_package*()>
{
    ts_package_grammar();

    boost::spirit::karma::rule<iterator, ts_package*()>                  start;

    boost::spirit::karma::rule<iterator, ts_package*()>                  package;

    ts_import_grammar<iterator>                                          import;

    boost::spirit::karma::rule<iterator, ts_class*()>                    cls;
    ts_class_grammar<iterator>                                           cls_impl;

    boost::spirit::karma::rule<iterator, ts_function*()>                 function;
    ts_function_grammar<iterator>                                        function_impl;
};

TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(ts_package_grammar)

} } } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_TS_PACKAGE_GRAMMAR_H_ */
