/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_FUNCTION_GRAMMAR_H_
#define ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_FUNCTION_GRAMMAR_H_

#include <vector>

#include <boost/fusion/container/vector.hpp>
#include <boost/spirit/include/karma_grammar.hpp>
#include <boost/spirit/include/karma_rule.hpp>

#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/CxxIdentifierGrammar.h"
#include "language/stage/import/grammar/CxxOptionalScopeGrammar.h"
#include "language/stage/import/grammar/CxxParameterGrammar.h"
#include "language/stage/import/grammar/CxxTypeGrammar.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/IdentifierGrammar.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
struct cxx_function_decl_grammar : public boost::spirit::karma::grammar<iterator, boost::fusion::vector<ts_function*, bool>()>
{
    cxx_function_decl_grammar();

    boost::spirit::karma::rule<iterator, boost::fusion::vector<ts_function*, bool>()>     start;

    boost::spirit::karma::rule<iterator, boost::fusion::vector<ts_function*, bool>()>     function;

    boost::spirit::karma::rule<iterator, ts_function*()>                                  function_name;
    boost::spirit::karma::rule<iterator, ts_function*()>                                  constructor_name_impl;
    boost::spirit::karma::rule<iterator, ts_function*()>                                  destructor_name_impl;
    boost::spirit::karma::rule<iterator, ts_function*()>                                  normal_function_name_impl;

    boost::spirit::karma::rule<iterator, ts_type&()>                                      optional_return_type;
    cxx_type_grammar<iterator>                                                            type;

    cxx_optional_scope_grammar<iterator>                                                  optional_scope;

    identifier_grammar<iterator>                                                          identifier;

    boost::spirit::karma::rule<iterator, std::vector<ts_parameter*>&()>                   parameters;
    cxx_parameter_grammar<iterator>                                                       parameter;
};

template<typename iterator>
struct cxx_function_def_grammar : public boost::spirit::karma::grammar<iterator, ts_function*()>
{
#define CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_WRAP_METHODS     \
    (by_pass  )                                                \
    (pointer  )                                                \
    (value    )                                                \
    (reference)                                                \
    (c_string )                                                \
    (c_wstring)

#define CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_METHODS        \
    (by_pass    )                                              \
    (copy_create)

    cxx_function_def_grammar();

    boost::spirit::karma::rule<iterator, ts_function*()>    start;

    boost::spirit::karma::rule<iterator, ts_function*()>    constructor_body;
    boost::spirit::karma::rule<iterator, ts_function*()>    constructor_pimpl_initializer;

    boost::spirit::karma::rule<iterator, ts_function*()>    destructor_body;

    boost::spirit::karma::rule<iterator, ts_function*()>    function;
    cxx_function_decl_grammar<iterator>                     function_decl;
    boost::spirit::karma::rule<iterator, ts_function*()>    function_body;

    identifier_grammar<iterator>                            identifier;
    cxx_qualified_identifier_grammar<iterator>              qualified_identifier;

    boost::spirit::karma::rule<iterator, std::vector<ts_parameter*>&()> parameter_pre_processings;
    boost::spirit::karma::rule<iterator, std::vector<ts_parameter*>&()> parameter_passings;

#define CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_SUB_RULE(r, base_rule, method)                                                             \
    boost::spirit::karma::rule<iterator, ts_parameter*()> BOOST_PP_CAT(BOOST_PP_CAT(base_rule, _), method);

#define CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_RULE(base_rule)                                                                            \
    BOOST_PP_SEQ_FOR_EACH(CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_SUB_RULE, base_rule, CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_WRAP_METHODS) \
    boost::spirit::karma::rule<iterator, ts_parameter*()> base_rule

    CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_RULE(parameter_conversion);
    CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_RULE(parameter_aliasing);
    CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_RULE(parameter_passing);

#undef CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_RULE
#undef CXX_FUNCTION_DEF_GRAMMAR_TS_PARAMETER_UNWRAP_SUB_RULE

    boost::spirit::karma::rule<iterator, ts_parameter*()> parameter_conversion_name;
    boost::spirit::karma::rule<iterator, ts_parameter*()> parameter_aliasing_name;
    boost::spirit::karma::rule<iterator, ts_parameter*()> parameter_plain_name;

#define CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_SUB_RULE(r, base_rule, method)                                                          \
    boost::spirit::karma::rule<iterator, ts_type&()> BOOST_PP_CAT(BOOST_PP_CAT(BOOST_PP_CAT(base_rule, _), method), _pre );             \
    boost::spirit::karma::rule<iterator, ts_type&()> BOOST_PP_CAT(BOOST_PP_CAT(BOOST_PP_CAT(base_rule, _), method), _post);

#define CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_RULE(base_rule)                                                                         \
    BOOST_PP_SEQ_FOR_EACH(CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_SUB_RULE, base_rule, CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_METHODS) \
    boost::spirit::karma::rule<iterator, ts_function*()> base_rule

    CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_RULE(function_return);

#undef CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_RULE
#undef CXX_FUNCTION_DEF_GRAMMAR_TS_RETURN_WRAP_SUB_RULE
};

TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_function_decl_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_function_def_grammar)

} } } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_FUNCTION_GRAMMAR_H_ */
