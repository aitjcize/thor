/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_MAKE_THORSTUBSTAGE_H_
#define ZILLIANS_LANGUAGE_STAGE_MAKE_THORSTUBSTAGE_H_

#include "language/stage/Stage.h"
#include "language/tree/ASTNode.h"
#include <vector>
#include <map>
#include <string>

namespace zillians { namespace language { namespace stage {

/**
 * The ThorStubStage is responsible for:
 *
 * 1. generate stubs based on stub templates
 */
class ThorStubStage : public Stage
{
public:
    ThorStubStage();
    virtual ~ThorStubStage();

public:
    virtual const char* name();
    virtual std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> getOptions();
    virtual bool parseOptions(po::variables_map& vm);
    virtual bool execute(bool& continue_execution);

public:
    std::vector<std::string> ast_files;

    bool use_stdout;
    std::string output_path;
    typedef std::map<std::wstring, std::wstring> var_map_t;
    var_map_t var_map;

public:
    typedef enum
    {
        UNKNOWN_STUB,
        GATEWAY_GAMECOMMAND_CLIENTCOMMANDOBJECT_H,
        GATEWAY_GAMECOMMAND_CLOUDCOMMANDOBJECT_H,
        GATEWAY_GAMECOMMAND_GAMECOMMANDTRANSLATOR_CPP,
        GATEWAY_GAMECOMMAND_GAMEMODULE_MODULE,
        CLIENT_CLIENTSTUB_H,
        CLIENT_GAMEOBJECTS_H,
        CLIENT_GAMESERVICE_CPP,
        CLIENT_GAMESERVICE_H,
        CLIENT_RPC_JS,
    } stub_type_t;

private:
    stub_type_t stub_type;
};

} } }

template<zillians::language::stage::ThorStubStage::stub_type_t ENUM>
std::string get_stub_filename(zillians::language::stage::ThorStubStage::var_map_t& var_map);

template<zillians::language::stage::ThorStubStage::stub_type_t ENUM>
void print_stub(zillians::language::tree::Tangle* node, zillians::language::stage::ThorStubStage::var_map_t& var_map);

#endif /* ZILLIANS_LANGUAGE_STAGE_MAKE_THORSTUBSTAGE_H_ */
