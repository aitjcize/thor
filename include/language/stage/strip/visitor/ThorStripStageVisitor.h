/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_VISITOR_THORSTRIPSTAGEVISITOR_H_
#define ZILLIANS_LANGUAGE_TREE_VISITOR_THORSTRIPSTAGEVISITOR_H_

#include <string>
#include "core/Prerequisite.h"
#include "core/Visitor.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

using namespace zillians::language::tree;
using zillians::language::tree::visitor::GenericDoubleVisitor;

namespace zillians { namespace language { namespace stage { namespace visitor {

struct ThorStripStageVisitor : public GenericDoubleVisitor
{
    CREATE_INVOKER(mStripeVisitorInvoker, apply);

    ThorStripStageVisitor()
    {
        REGISTER_ALL_VISITABLE_ASTNODE(mStripeVisitorInvoker)
    }

    void apply(ASTNode& node)
    {
        revisit(node);
    }

    void apply(FunctionDecl& node)
    {
        // we should strip all function blocks which is defined as "complete declaration"
        // (that has actual LLVM code generated)
        if(node.isCompleted())
        {
            if(node.block)
            {
                node.block->parent = NULL; node.block = NULL;
            }
        }

        // we should also remove all parameters which are resolved to implicit variables
        foreach(i, node.parameters)
        {
            (*i)->getCanonicalType();
        }
    }
};

} } } } // namespace zillians::language::tree::visitor

#endif /* ZILLIANS_LANGUAGE_TREE_VISITOR_THORSTRIPSTAGEVISITOR_H_ */
