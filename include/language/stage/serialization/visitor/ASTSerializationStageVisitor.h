/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_ASTSERIALIZATIONSTAGEVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_ASTSERIALIZATIONSTAGEVISITOR_H_

#include "language/tree/ASTNodeFactory.h"
#include "language/tree/CustomizedSerialization.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/serialization/detail/ASTSerializationCommon.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

/**
 * ASTSerializationStageVisitor is a helper to serialize all context object stored in ContextHub of AST
 */
template<typename Archive>
struct ASTSerializationStageVisitor : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(serializeInvoker, serialize);

    ASTSerializationStageVisitor(Archive& oa) : archive(oa)
    {
        REGISTER_ALL_VISITABLE_ASTNODE(serializeInvoker);
    }

    void serialize(tree::ASTNode& node)
    {
        FullSerializer serializer(node);
        archive << serializer;
        revisit(node);
    }

    Archive& archive;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_ASTSERIALIZATIONSTAGEVISITOR_H_ */
