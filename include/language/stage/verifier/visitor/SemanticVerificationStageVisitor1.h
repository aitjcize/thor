/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_SEMANTICVERIFICATIONSTAGEVISITOR1_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_SEMANTICVERIFICATIONSTAGEVISITOR1_H_


#include <stack>
#include <functional>
#include <unordered_map>

#include "utility/Functional.h"

#include "language/context/TransformerContext.h"
#include "language/stage/verifier/context/SemanticVerificationContext.h"
#include "language/stage/verifier/detail/ControlFlowVerifier.h"
#include "language/stage/verifier/detail/NameShadowVerifier.h"
#include "language/stage/verifier/detail/ArchitectureVerifier.h"
#include "language/stage/verifier/visitor/NameVerificationVisitor.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

// CHECKS IN SEMANTIC VERIFICATION STAGE 1

// ERRORS:
// ====================================
// WRITE_CONST
// INVALID_NONSTATIC_CALL
// INVALID_NONSTATIC_REF
// INVALID_ACCESS_PRIVATE
// INVALID_ACCESS_PROTECTED
// INVALID_COVARIANCE
// UNEXPECTED_VARIADIC_ARG

// WARNINGS:
// ====================================
// UNINIT_REF
// MISSING_CASE

namespace zillians { namespace language { namespace stage { namespace visitor {

struct SemanticVerificationStageVisitor1 : public tree::visitor::GenericDoubleVisitor
{
private:
    struct VariableState {
        enum State {
            Declared,
            Set,
            OK,
            UnInitRef,
            NeverUsed,
        };

        State state;
        tree::Block* block;
        tree::IdExpr* first_read;

        VariableState(tree::Block* block);

        void write();
        void read(tree::IdExpr* location);

        void end();
    };

    typedef std::map<tree::VariableDecl*, VariableState> VarStateMapType;

public:
    CREATE_INVOKER(verifyInvoker, verify);

    SemanticVerificationStageVisitor1();

    void verify(tree::ASTNode& node);
    void verify(tree::Package& node);
    void verify(tree::ClassDecl& node);
    void verify(tree::EnumDecl& node);
    void verify(tree::TypedefDecl& node);
    void verify(tree::TypenameDecl& node);
    void verify(tree::Source& node);
    void verify(tree::ObjectLiteral& node);
    void verify(tree::MemberExpr& node);
    void verify(tree::IdExpr& node);
    void verify(tree::CallExpr& node);
    void verify(tree::UnpackExpr& node);
    void verify(tree::UnaryExpr& node);
    void verify(tree::BinaryExpr& node);
    void verify(tree::TernaryExpr& node);
    void verify(tree::BlockExpr& node);
    void verify(tree::VariableDecl& node);
    void verify(tree::FunctionDecl& node);
    void verify(tree::Block& node);
    void verify(tree::IfElseStmt& node);
    void verify(tree::SwitchStmt& node);

private:
    void verifyModify(tree::UnaryExpr& unary);
    void verifyAssignment(tree::BinaryExpr& binary);

    /////////////////////////////////////
    // begin of virtual function handling
    void verifyNotAbstractClass(tree::BlockExpr& expr);
    void verifyVirtualFunctions(const ClassDecl& cls);
    void verifyIsReturnTypeCompatible(FunctionDecl& overrider, const FunctionDecl& overriden);
    template<typename range_type> void verifyCorrectlyOverriden(FunctionDecl& overrider, const range_type& overridens);
    template<typename range_type> void verifyNoMixingOverriden(FunctionDecl& overrider, const range_type& overridens);
    // end of virtual function handling
    /////////////////////////////////////

    /////////////////////////////////////
    // begin of variable state verification
    bool isWrite(tree::IdExpr& node);
    bool isRead(tree::IdExpr& node);
    bool isNormalLocalVariable(tree::VariableDecl& node);
    void checkVariableStatus(tree::Block& block);
    /**
     * Merge table
     *
     *          | Declared Set      OK       Uninit
     * ---------+-----------------------------------
     * Declared | Declared Declared Declared Uninit
     * Set      | Declared Set      OK       Uninit
     * OK       | Declared OK       OK       Uninit
     * Uninit   | Uninit   Uninit   Uninit   Uninit
     */
    void mergeVarState(VarStateMapType& lhs, VarStateMapType& rhs);
    // end of variable state verification
    /////////////////////////////////////

    bool verifyValidClassToExtend(tree::ClassDecl& node);
    bool verifyValidInterfacesToImplement(tree::ClassDecl& node);
    bool verifyNoCyclicInheritance(tree::ClassDecl& node);
    bool verifyNoInterfaceReImplement(tree::ClassDecl& node);
    template<class T>
    void verifyVisibilityAccessViolation(tree::ASTNode* node_ref, T* node_decl);

    template<typename ExprType>
    bool verifyStaticAccessViolationHasObject(ExprType&);
    bool verifyStaticAccessViolationHasObject(tree::MemberExpr& expr);
    template<typename ExprType>
    void verifyStaticAccessViolation(ExprType& expr);

    void verifyTernaryExprHasType(tree::TernaryExpr& node);

    void verifyCaseHasConstantExpression(tree::SwitchStmt& node);
    void verifyIntegerCaseValues(tree::SwitchStmt& node);
    void verifyCasesAreNotDuplicate(tree::SwitchStmt& node);

    void verifyNoDivideByZero(BinaryExpr& node);

    /*
     * When verifyCompare, implicit conversion had already applied,
     * if both primitive, they had cast to the same type.
     * if both class and can be convert, they had cast to the same type.
     * more, object literal (null/this/super) is take cared, too.
     * so, if left/right type are different, they can not be compare.
     */
    void verifyCompare(tree::BinaryExpr& node);

    void verifyCtorDtorMustReturnVoid(FunctionDecl& node);

public:
    void applyCleanup();

private:
    ArchitectureVerifier arch_verifier;
    NameShadowVerifier nameShadowVerifier;
    VarStateMapType var_status;
    std::vector<tree::Block*> block_stack;

    std::vector<std::function<void()>> cleanup;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_SEMANTICVERIFICATIONSTAGEVISITOR1_H_ */
