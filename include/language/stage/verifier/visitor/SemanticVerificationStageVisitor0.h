/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_SEMANTICVERIFICATIONSTAGEVISITOR0_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_SEMANTICVERIFICATIONSTAGEVISITOR0_H_

#include <set>
#include <string>
#include <unordered_set>
#include "core/Prerequisite.h"
#include "language/context/ConfigurationContext.h"
#include "language/context/ManglingStageContext.h"
#include "language/context/ParserContext.h"
#include "language/stage/verifier/context/SemanticVerificationContext.h"
#include "language/stage/verifier/detail/NameShadowVerifier.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

using namespace zillians::language::tree;
using zillians::language::tree::visitor::GenericDoubleVisitor;

// CHECKS IN SEMANTIC VERIFICATION STAGE 0

// ERRORS:
// ====================================
// INCOMPLETE_FUNC
// DUPE_NAME
// WRITE_RVALUE
// MISSING_STATIC_INIT
// MISSING_BREAK_TARGET
// MISSING_CONTINUE_TARGET
// MISSING_PARAM_INIT
// UNEXPECTED_VARIADIC_PARAM
// UNEXPECTED_VARIADIC_TEMPLATE_PARAM
// UNEXPECTED_VARIADIC_TEMPLATE_ARG
// EXCEED_PARAM_LIMIT
// EXCEED_TEMPLATE_PARAM_LIMIT

// WARNINGS:
// ====================================
// DEAD_CODE

namespace zillians { namespace language { namespace stage { namespace visitor {

struct SemanticVerificationStageVisitor0 : public GenericDoubleVisitor
{
    CREATE_INVOKER(verifyInvoker, verify);

    SemanticVerificationStageVisitor0();

    void verify(ASTNode& node);
    void verify(Source& node);
    void verify(Import& node);
    void verify(Package& node);
    void verify(FunctionSpecifier& node);
    void verify(Annotation& node);
    void verify(UnaryExpr& node);
    void verify(BranchStmt& node);
    void verify(ClassDecl& node);
    void verify(VariableDecl& node);
    void verify(FunctionDecl& node);
    void verify(SimpleIdentifier& node);
    void verify(Block& node);
    void verify(TypenameDecl& node);
    void verify(EnumDecl& node);

public:
    void applyCleanup();

private:
    void verifyImportedPackageIsValid(Import& node);
    void verifyHasValidAnnotation(Annotation& node);
    void verifyVariableHasInit(VariableDecl& node);
    void verifyTypeSpecifierNotVoid(TypeSpecifier& node);
    void verifyVariableHasValidType(VariableDecl& node);
    void verifyIsValidEntryFunction(FunctionDecl& node);
    void verifyFunctionTypeParamsValid(FunctionSpecifier& node);
    void verifyFunctionTypeReturnValid(FunctionSpecifier& node);
    void verifyValidNewExpression(UnaryExpr& node);
    void verifyValidOperatorOverloading(FunctionDecl& node);
    void verifyLambdaSuperCall(FunctionDecl& node);
    void verifyNonMemberCtorDtor(FunctionDecl& node);
    void verifyMissingReturnType(FunctionDecl& node);
    void verifyParameter(FunctionDecl& node);
    void verifyFunctionLinkage(FunctionDecl& node);
    void verifyBreakOrContinueTarget(BranchStmt& node);
    void verifyTemplateMemberFunction(FunctionDecl& node);
    void verifyFlowStatement(Statement& node);
    void verifyValidCtorOrDtor(FunctionDecl& node);
    void verifyDefaultTemplateArgumentOrder(const TemplatedIdentifier* identifier);
    void verifyCtorDtorMustNotBeStatic(FunctionDecl& node);
    void verifyCtorMustNotBeVirtual(FunctionDecl& node);
    void verifyDtorMustAcceptNoParameter(FunctionDecl& node);

private:
	void verifyIsConfictWithAlternativeKeywords(ASTNode& node, std::wstring name);
	void verifyPackageConfictWithAlternativeKeywords(Package& node);

private:
    std::vector<std::function<void()>> cleanup;
    uint64 errorCount_;
    uint64 warnCount_;

private:
    NameShadowVerifier nameShadowVerifier;
    std::unordered_set<std::wstring> alternative_keywords;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_SEMANTICVERIFICATIONSTAGEVISITOR0_H_ */
