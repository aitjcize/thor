/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_NAMESHADOWVERIFIER_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_NAMESHADOWVERIFIER_H_

#include <map>
#include <string>
#include <vector>
#include <stack>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/tag.hpp>
#include "core/Prerequisite.h"
#include "language/tree/ASTNode.h"
#include "language/tree/basic/Block.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/TypenameDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/module/Package.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

struct NameShadowVerifier
{
    // Forbid the following case
    // - variable in inner block shadow outer block one
    // - local variable shadow member (current class or from base) variable
    // - member variable shadow base one
    // - local/member variable shadow global one (in same package)

    /****************************************
     *  Target       * Group * Scope * Name *
     ****************************************
     *  Package      *   Y   *       *      *
     *  ClassDecl    *       *   Y   *      *
     *  FunctionDecl *       *   Y   *      *
     *  Block        *       *   Y   *      *
     *  EnumDecl     *       *   Y   *      *
     *  TypenameDecl *       *       *  Y   *
     *  VariableDecl *       *       *  Y   *
     ****************************************/

private:
    struct NameVarData
    {
        std::wstring name;
        tree::ASTNode* owner;
        tree::ASTNode* node;

    private:
        NameVarData();

    public:
        NameVarData(const std::wstring& name, tree::ASTNode& owner, tree::ASTNode& node);
    };

public:
    NameShadowVerifier();
    ~NameShadowVerifier();

    void enter(tree::Package     & node);
    void leave(tree::Package     & node);

    void enter(tree::ClassDecl   & node, const bool& follow_hierarchy = false);
    void leave(tree::ClassDecl   & node);

    void enter(tree::FunctionDecl& node);
    void leave(tree::FunctionDecl& node);

    void enter(tree::Block       & node);
    void leave(tree::Block       & node);

    void enter(tree::EnumDecl    & node);
    void leave(tree::EnumDecl    & node);

    void enter(tree::TypenameDecl& node);
    void leave(tree::TypenameDecl& node);

    void enter(tree::VariableDecl& node);
    void leave(tree::VariableDecl& node);

private:
    struct TagIndex;
    struct TagName;
    struct TagOwner;

    typedef boost::multi_index_container<
        NameVarData,
        boost::multi_index::indexed_by<
            boost::multi_index::sequenced<
                boost::multi_index::tag<TagIndex>
            >,
            boost::multi_index::ordered_non_unique<
                boost::multi_index::tag<TagName>,
                boost::multi_index::member<
                    NameVarData,
                    std::wstring,
                    &NameVarData::name
                >
            >,
            boost::multi_index::ordered_non_unique<
                boost::multi_index::tag<TagOwner>,
                boost::multi_index::member<
                    NameVarData,
                    tree::ASTNode*,
                    &NameVarData::owner
                >
            >
        >
    > NameStatusCollection;

    void checkShadow(tree::Declaration& node, tree::ASTNode* custom_owner = NULL);

    void followClassHierarchy(tree::ClassDecl& node);

    void addName(tree::Declaration& node, tree::ASTNode* custom_owner = NULL);

    tree::ASTNode* getOwner();

    template<typename Tag>
    typename NameStatusCollection::index<Tag>::type &getNameStatus();

    std::vector<tree::Package*> pkg_stack;
    std::vector<tree::ClassDecl*> cls_stack;
    std::stack<tree::FunctionDecl*> func_scope_stack;
    tree::EnumDecl* enum_scope;
    std::stack<tree::Block*> block_stack;

    std::map<tree::Package*, NameStatusCollection> name_statuses;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_NAMESHADOWVERIFIER_H_ */
