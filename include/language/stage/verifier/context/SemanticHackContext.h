/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_SEMANTICHACKCONTEXT_H_
#define ZILLIANS_LANGUAGE_SEMANTICHACKCONTEXT_H_

#include "core/Prerequisite.h"

namespace zillians { namespace language {

struct SemanticHackContext
{
    SemanticHackContext() 
        : skip_private_access_check(false)
        , skip_protected_access_check(false)
    { }

    static SemanticHackContext* get(tree::ASTNode* node)
    {
        return node->get<SemanticHackContext>();
    }

    static void set(tree::ASTNode* node, SemanticHackContext* ctx)
    {
        node->set<SemanticHackContext>(ctx);
    }

    template<typename Mapper>
    void update_reference(Mapper& m)
    { }

    bool skip_private_access_check;         // skip private member variable/function access check
    bool skip_protected_access_check;       // skip protected member variable/function access check
};

} }

#endif /* ZILLIANS_LANGUAGE_SEMANTICHACKCONTEXT_H_ */
