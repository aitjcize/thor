/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef _LANGUAGE_STAGE_TRANSFORMER_DETAIL_FLOWSPLITER_H_
#define _LANGUAGE_STAGE_TRANSFORMER_DETAIL_FLOWSPLITER_H_

#include <list>
#include <memory>
#include <set>
#include <string>
#include <utility>
#include <vector>
#include <boost/variant.hpp>
#include <boost/iterator/iterator_categories.hpp>
#include <boost/range/any_range.hpp>
#include <boost/ptr_container/ptr_vector.hpp>

#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/statement/Statement.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace stage { namespace visitor { namespace detail {

class BasicBlock;
using VarSet = std::set<tree::VariableDecl*>;
using VarSetPtr = const std::set<tree::VariableDecl*>*;
using VarSetPtrVec = std::vector<VarSetPtr>;

//////////////////////////////////////////////////////////////////////////////
// Transitions
//////////////////////////////////////////////////////////////////////////////

using UnknownTransition = boost::blank;

class NullTransition { } ;

class NormalTransition
{
public:
    explicit NormalTransition(BasicBlock* source, BasicBlock* target);
    BasicBlock* get_source() const noexcept { return source; }
    BasicBlock* get_target() const noexcept { return target; }

public:
    VarSetPtr get_pass_var() const noexcept;
    bool      replace_target(BasicBlock* from, BasicBlock* to);

private:
    BasicBlock* source;
    BasicBlock* target;
} ;

class ConditionTransition
{
public:
    ConditionTransition(BasicBlock* source, tree::Expression* cond, BasicBlock* true_block, BasicBlock* false_block);
    tree::Expression* get_cond()                     const noexcept { return cond; }
    BasicBlock*       get_source     ()              const noexcept { return source     ; }
    BasicBlock*       get_true_block ()              const noexcept { return true_block ; }
    BasicBlock*       get_false_block()              const noexcept { return false_block; }
    void              set_true_block (BasicBlock* b) noexcept       { true_block  = b; }
    void              set_false_block(BasicBlock* b) noexcept       { false_block = b; }

public:
    VarSetPtrVec get_pass_var() const;
    bool replace_target(BasicBlock* from, BasicBlock* to);

private:
    BasicBlock*       source;
    tree::Expression* cond;
    BasicBlock*       true_block;
    BasicBlock*       false_block;
} ;

class CaseTransition
{
public:
    using case_type = std::vector<std::pair<tree::Expression*, BasicBlock*>>;
    using size_type = case_type::size_type;

public:
    explicit CaseTransition(BasicBlock* source, tree::Expression* cond);
    size_type         size()             const noexcept { return cases_targets.size(); }
    tree::Expression* get_condition()    const noexcept { return cond; }
    BasicBlock*       get_source      () const noexcept { return source       ; }
    const case_type&  get_case_targets() const noexcept { return cases_targets; }
    case_type&        get_case_targets() noexcept       { return cases_targets; }

public:
    void         add_case_trans(tree::Expression* expr, BasicBlock* target);
    VarSetPtrVec get_pass_var() const;
    bool         replace_target(BasicBlock* from, BasicBlock* to);

private:
    BasicBlock*       source;
    case_type         cases_targets;
    tree::Expression* cond;
} ;

class FlowTransition
{
public:
    using targets_type = std::vector<BasicBlock*>;
    using size_type = targets_type::size_type;

public:
    explicit FlowTransition(BasicBlock* source);
    size_type           size()        const noexcept { return targets.size(); }
    BasicBlock*         get_source () const noexcept { return source;  }
    const targets_type& get_targets() const noexcept { return targets; }
    targets_type&       get_targets() noexcept       { return targets; }

public:
    void         add_target(BasicBlock* target);
    VarSetPtrVec get_pass_var() const;
    bool         replace_target(BasicBlock* from, BasicBlock* to);

private:
    BasicBlock*              source;
    std::vector<BasicBlock*> targets;
} ;

class MergeTransition
{
public:
    explicit MergeTransition(BasicBlock* source, BasicBlock* target);
    BasicBlock* get_source() const noexcept { return source; }
    BasicBlock* get_target() const noexcept { return target; }

public:
    VarSet get_pass_var() const;
    bool   replace_target(BasicBlock* from, BasicBlock* to);

private:
    BasicBlock*  source;
    BasicBlock*  target;
} ;

using TransitionType = boost::variant
<
    UnknownTransition,
    NullTransition,
    NormalTransition,
    ConditionTransition,
    CaseTransition,
    FlowTransition,
    MergeTransition
>;

//////////////////////////////////////////////////////////////////////////////
// BasicBlock
//////////////////////////////////////////////////////////////////////////////

class BasicBlock
{
public:
    enum class Tag {
        FUNC_INIT, FUNC_END,
        FOR_INIT, FOR_COND, FOR_STEP, FOR_BODY, FOR_END,
        IF_COND, IF_TRUE, IF_ELIF, IF_FALSE, IF_END,
        FLOW_INIT, FLOW_SUB, FLOW_END,
        WHILE_COND, WHILE_BODY, WHILE_END,
        SWITCH_COND, SWITCH_CASE, SWITCH_DEFAULT, SWITCH_END,
        DEAD_CODE,
    } ;

    using StatementRangeType = boost::any_range<
        tree::Statement*,
        boost::random_access_traversal_tag,
        tree::Statement*,
        std::ptrdiff_t
    >;

public:
    BasicBlock(Tag t, int serial_id);
    BasicBlock(std::list<tree::Statement*>::iterator begin, std::list<tree::Statement*>::iterator end);
    std::wstring get_name() const;

    // basic qeury
    Tag get_tag() const noexcept;
    int id() const noexcept;
    int num_successor() const noexcept;
    int num_predecessor() const noexcept;

    // transitions
    const TransitionType& get_transition() const noexcept;
    TransitionType& get_transition() noexcept;
    void set_transition(const TransitionType& t);
    void set_transition(TransitionType&& t);
    template<typename TransType, typename... ArgTypes>
    TransType& init_transition(ArgTypes&&... args);

    // statements
    StatementRangeType get_statements() const;
    void insert_statement(tree::Statement* stmt);
    bool empty() const noexcept;
    bool is_empty_null_transition() const noexcept;

    // data flow related functions
    VarSet& get_used_variables() noexcept;
    const VarSet& get_used_variables() const noexcept;
    void add_used_variable(tree::VariableDecl* var_decl);
    void add_used_variables(const VarSet& s);
    void sub_used_variable(tree::VariableDecl* var_decl);
    const VarSet& get_decl_variables() const noexcept;
    VarSet& get_decl_variables() noexcept;
    void construct_decl_var();
    void init_used_variables();
    bool propagate_used_variables();
    tree::VariableDecl* get_flow_assign_var() const noexcept;

    // generate string
    std::wstring get_source_string() const;
    std::wstring get_used_var_str() const;
    std::wstring get_decl_var_str() const;

    // predecessor
    void add_predecessor(BasicBlock* b);
    void erase_predecessor(BasicBlock* b);
    void replace_predecessor(BasicBlock* from, BasicBlock* to);
    void clear_predecessor();
    const std::set<BasicBlock*>& get_predecessors() const noexcept;
    std::set<BasicBlock*> get_successors() const;

private:
    int                           serial_id;
    Tag                           tag;
    TransitionType                transition;
    std::vector<tree::Statement*> statements;
    VarSet                        used_variables;
    VarSet                        decl_vars;
    std::set<BasicBlock*>         pred;
} ;

//////////////////////////////////////////////////////////////////////////////
// FlowGraph
//////////////////////////////////////////////////////////////////////////////

class FlowGraph
{
public:
    explicit FlowGraph(const std::wstring& func_name);
    BasicBlock* create_block(BasicBlock::Tag tag);
    void erase_block(BasicBlock* b);
    boost::ptr_vector<BasicBlock>::size_type size() const noexcept;
    const boost::ptr_vector<BasicBlock>& get_blocks() const noexcept;
    boost::ptr_vector<BasicBlock>& get_blocks() noexcept;
    const std::wstring& get_func_name() const noexcept;
    void gen_graphviz(const std::wstring& file_name) const;
    void init_used_variables();
    void construct_pred();
    bool propagate_used_variables();
    bool optimize();
    bool remove_empty_blocks();
    bool remove_null_transition_block();
    bool merge_blocks();
    BasicBlock* get_entry() const noexcept;
    void set_entry(BasicBlock* b);
    void set_need_this() noexcept;
    bool need_this() const noexcept;
    bool remove_unreachable_blocks();
    bool try_merge_block(BasicBlock* b);

    void err_status() const;

private:
    bool remove_null_transition_block_one_level();
    bool replace_entry(BasicBlock* from, BasicBlock* to);

private:
    BasicBlock*                   entry;
    boost::ptr_vector<BasicBlock> blocks;
    std::wstring                  func_name;
    bool                          need_this_;
    int                           serial_id_counter;

} ;

std::unique_ptr<FlowGraph> flow_split(tree::FunctionDecl& func);

} } } } }

#endif // _LANGUAGE_STAGE_TRANSFORMER_DETAIL_FLOWSPLITER_H_
