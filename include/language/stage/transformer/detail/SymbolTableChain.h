/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_SYMBOLTABLECHAIN_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_SYMBOLTABLECHAIN_H_

#include <vector>
#include "core/Prerequisite.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/resolver/SymbolTable.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

class SymbolTableChain
{
public:
    SymbolTableChain();

    bool is_valid() const;

    void chain(SymbolTable& new_table);
    void unchain(SymbolTable& old_table);

    SymbolTable::InsertResult insert_symbol(tree::Import& node);
    SymbolTable::InsertResult insert_symbol(tree::Declaration& node);

    void enter_scope(tree::ASTNode& t_id);
    void leave_scope(tree::ASTNode& t_id);
    void enter_scope(tree::ClassDecl& cls_decl, SymbolTable& new_symbol_table);
    void leave_scope(tree::ClassDecl& cls_decl, SymbolTable& old_symbol_table);

    SymbolTable::FindResult find(const std::wstring& id) const;

private:
    SymbolTable& last_chained() const;

    std::vector<SymbolTable*> chained_tables;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_SYMBOLTABLECHAIN_H_ */
