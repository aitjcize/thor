/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_OPERATOROVERLOADINGCONFIG_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_OPERATOROVERLOADINGCONFIG_H_

#include "language/tree/basic/Identifier.h"
#include "language/tree/expression/BinaryExpr.h" 
#include "language/tree/expression/UnaryExpr.h" 

#include "boost/assert.hpp"

#include <map>
#include <set>

namespace zillians { namespace language { namespace stage { namespace visitor {

namespace OperatorOverloading {

namespace tree = zillians::language::tree;

// common types will be used later
typedef decltype(tree::SimpleIdentifier::name) Name;


// default methods
template < typename Derived, typename Op > 
class Actions 
{
public:

    typedef Op Operation;
    
    typedef std::set< Name > ReservedNameSet;

protected:

    typedef Actions< Derived, Operation > Super;    

    typedef std::map<  
                     Operation,
                     Name
                    > ReservedNameMap;

public:
    static bool supports( Operation op )
    {
        return Derived::reserved_names.find(op) != Derived::reserved_names.end();
    }

    static Name getReservedNameOf( Operation op )
    {
        BOOST_ASSERT( supports(op) );

        return Derived::reserved_names[ op ];
    }

    static ReservedNameSet getReservedNames() 
    {
        ReservedNameSet names;

        for( auto & pair : Derived::reserved_names )
            names.insert( pair.second );

        return names;       
    }
};

// different types define their own support operations 
class Binary : public Actions<Binary, tree::BinaryExpr::OpCode::type> 
{
    friend Super;

public:
    static bool supports( BinaryExpr& node );

private:
    static ReservedNameMap reserved_names;
};

class Unary : public Actions<Unary, tree::UnaryExpr::OpCode::type> 
{
    friend Super; 

public:
    static bool supports( UnaryExpr& node );

private:
    static ReservedNameMap reserved_names;
};

}

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_OPERATOROVERLOADINGCONFIG_H_ */
