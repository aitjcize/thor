/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_RESOLUTIONSTAGEPREAMBLEVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_RESOLUTIONSTAGEPREAMBLEVISITOR_H_

#include "core/Prerequisite.h"
#include "language/resolver/SpecializationFwd.h"
#include "language/resolver/SymbolTable.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

struct ResolutionStagePreambleVisitor : public tree::visitor::GenericDoubleVisitor
{
    // The following types of nodes contain symbol table and should be handled now
    // - Package   : global declaration
    // - Internal  : primitive and function types
    // - EnumDecl  : enumerations

    CREATE_INVOKER(resolveInvoker, resolve);

    ResolutionStagePreambleVisitor(resolution::specialization::Grouper& specialization_grouper);

    void resolve(tree::ASTNode& node);

    void resolve(tree::Source& node);
    void resolve(tree::Tangle& node);

    void resolve(tree::ClassDecl& node);
    void resolve(tree::EnumDecl& node);
    void resolve(tree::FunctionDecl& node);
    void resolve(tree::Package& node);

    void resolve(tree::Declaration& node);

private:
    resolution::specialization::Grouper& specialization_grouper;
    SymbolTable* current_symbol_table;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_RESOLUTIONSTAGEVISITOR_H_ */
