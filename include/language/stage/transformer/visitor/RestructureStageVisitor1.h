/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_RESTRUCTURESTAGEVISITOR1_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_RESTRUCTURESTAGEVISITOR1_H_

#include <functional>
#include <vector>

#include "language/tree/ASTNodeFwd.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

/**
 * RestructureStageVisitor1 is the visitation helper for RestructureStage1
 *
 * @see RestructureStage
 */
struct RestructureStageVisitor1 : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(restructInvoker, restruct);

    RestructureStageVisitor1(bool no_system_bundle);

    void restruct(tree::ASTNode& node);

    void restruct(tree::FunctionDecl& node);
    void restruct(tree::ClassDecl& node);
    void restruct(tree::VariableDecl& node);

    void restruct(tree::ArrayExpr& node);
    void restruct(tree::BlockExpr& node);
    void restruct(tree::UnaryExpr& node);
    void restruct(tree::IsaExpr& node);

public:
    bool hasTransforms();
    void applyTransforms();

public:
    void getStatus(unsigned& split_reference_count, unsigned& assign_null_count, unsigned& system_api_transform_count, 
                   unsigned& typeid_resolved_count, unsigned& new_restruct_count,
                   unsigned& isa_restruct_count, unsigned& generated_function_count);

private:
    void restructNew(tree::BlockExpr& node);
    void tryGenerateFactoryMethod(tree::ClassDecl& class_);

private:
    bool no_system_bundle;
    std::vector<std::function<void()>> transforms;

    // to record status
    struct Status
    {
        Status();

        unsigned split_reference_count;
        unsigned assign_null_count;
        unsigned system_api_transform_count;
        unsigned typeid_resolved_count;
        unsigned new_restruct_count;
        unsigned isa_restruct_count;
        unsigned generated_function_count;
    } status;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_RESTRUCTURESTAGEVISITOR1_H_ */
