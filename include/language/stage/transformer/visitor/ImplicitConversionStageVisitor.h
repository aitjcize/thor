/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_IMPLICITCONVERSIONSTAGEVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_IMPLICITCONVERSIONSTAGEVISITOR_H_

#include "language/tree/ASTNodeFwd.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

struct ImplicitConversionStageVisitor : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(convertInvoker, convert);

    ImplicitConversionStageVisitor();

    void convert(tree::ASTNode& node);

    void convert(tree::Annotations& node);

    void convert(tree::ClassDecl& node);
    void convert(tree::FunctionDecl& node);
    void convert(tree::VariableDecl& node);

    void convert(tree::UnaryExpr& node);
    void convert(tree::BinaryExpr& node);
    void convert(tree::TernaryExpr& node);
    void convert(tree::CallExpr& node);
    void convert(tree::CastExpr& node);

    void convert(tree::BranchStmt& node);
    void convert(tree::IfElseStmt& node);
    void convert(tree::SwitchStmt& node);
    void convert(tree::ForStmt& node);
    void convert(tree::WhileStmt& node);

public:
	std::unordered_set<tree::CastExpr*> getCastExprCollection();

private:
	std::unordered_set<tree::CastExpr*> cast_expr_collection;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_IMPLICITCONVERSIONSTAGEVISITOR_H_ */
