/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_NAMEMANGLINGSTAGEVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_NAMEMANGLINGSTAGEVISITOR_H_

#include "core/Prerequisite.h"
#include "utility/StringUtil.h"
#include "language/context/ManglingStageContext.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/tree/visitor/NameManglingVisitor.h"

using namespace zillians::language::tree;
using zillians::language::tree::visitor::GenericDoubleVisitor;
using zillians::language::tree::visitor::NameManglingVisitor;

namespace zillians { namespace language { namespace stage { namespace visitor {

/**
 * NameManglingStageVisitor is the visitation helper for ManglingStage
 *
 * @see ManglingStage
 */
struct NameManglingStageVisitor : public GenericDoubleVisitor
{
    CREATE_INVOKER(mangleInvoker, apply);

    NameManglingStageVisitor();

    void apply(ASTNode&      node);
    void apply(Source&       node);
    void apply(Package&      node);
    void apply(Type&         node);
    void apply(Identifier&   node);
    void apply(Declaration&  node);
    void apply(ClassDecl&    node);
    void apply(EnumDecl&     node);
    void apply(FunctionDecl& node);
    void apply(VariableDecl& node);

private:
    NameManglingVisitor mangler;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_NAMEMANGLINGSTAGEVISITOR_H_ */
