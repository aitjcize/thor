/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_RESOLUTIONSTAGEVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_RESOLUTIONSTAGEVISITOR_H_

#include <set>
#include <tuple>
#include <type_traits>

#include <boost/optional.hpp>
#include <boost/logic/tribool_fwd.hpp>

#include "core/Prerequisite.h"

#include "language/stage/transformer/detail/ObjectReplicationHelper.h"
#include "language/stage/transformer/detail/SymbolTableChain.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/resolver/Resolver.h"
#include "language/resolver/Specialization.h"
#include "language/resolver/SymbolTable.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

/**
 * ResolutionStageVisitor is the visitation helper for ResolutionStage
 *
 * ResolutionStageVisitor will visit every node in the AST that needs to be resolved as type or symbol by using Resolver
 *
 * @see ResolutionStage, Resolver
 * @todo implement resolution cache
 */
struct ResolutionStageVisitor : public tree::visitor::GenericDoubleVisitor
{
    struct PersistantState
    {
        std::set<ClassDecl*>                           class_content_ready_set;
        detail::object_replication::verification_state verification_state;
        std::unordered_set<ASTNode*>                   failed_nodes;
        resolution::specialization::Grouper            specialization_grouper;
    };

    CREATE_INVOKER(resolveInvoker, resolve);

    explicit ResolutionStageVisitor(PersistantState& state);

    void resolve(ASTNode& node);

    void resolve(Annotations& node);
    void resolve(NumericLiteral& node);
    void resolve(ObjectLiteral& node);
    template<
        typename NodeType,
        typename std::enable_if<
            std::is_same<NodeType, TypeIdLiteral    >::value ||
            std::is_same<NodeType, SymbolIdLiteral  >::value ||
            std::is_same<NodeType, FunctionIdLiteral>::value
        >::type * = nullptr
    >
    void resolve(NodeType& node);
    void resolve(TemplatedIdentifier& node);
    void resolve(Block& node);
    void resolve(Source& node);
    void resolve(TypeSpecifier& node);
    void resolve(FunctionSpecifier& node);
    void resolve(MultiSpecifier& node);
    void resolve(NamedSpecifier& node);
    void resolve(PrimitiveSpecifier& node);
    void resolve(ClassDecl& node);
    void resolve(FunctionDecl& node);
    void resolve(EnumDecl& node);
    void resolve(VariableDecl& node);
    void resolve(BranchStmt& node);
    void resolve(DeclarativeStmt& node);
    void resolve(ExpressionStmt& node);
    void resolve(ForeachStmt& node);
    void resolve(UnaryExpr& node);
    void resolve(BinaryExpr& node);
    void resolve(TernaryExpr& node);
    void resolve(CallExpr& node);
    void resolve(ArrayExpr& node);
    void resolve(MemberExpr& node);
    void resolve(IdExpr& node);
    void resolve(LambdaExpr& node);
    void resolve(BlockExpr& node);
    void resolve(CastExpr& node);
    void resolve(IsaExpr& node);
    void resolve(TieExpr& node);
    void resolve(StringizeExpr& node);
    void resolve(SystemCallExpr& node);

    std::size_t getResolvedCount();
    std::size_t getFailedCount();
    std::size_t getUnknownCount();

    bool isResolved(ASTNode* node);
    bool isFailed(ASTNode* node);
    bool isUnknown(ASTNode* node);

    void reset();

    bool hasTransforms();
    void applyTransforms();

private:
    void markResolved(ASTNode* node);
    void markFailed(ASTNode* node);
    void markFailedSymbol(ASTNode* node, Identifier* id, bool log_error = true);
    void markFailedType(ASTNode* node, bool log_error = true);
    void markFailedExpr(ASTNode* node, ASTNode* resolved);
    void markUnknown(ASTNode* node);

    SymbolTable::FindResult findSymbol(Identifier& id, ASTNode* depend);

    boost::tribool resolveType(ASTNode& attach, Identifier& id, ASTNode* depend = NULL);
    boost::tribool resolveSymbol(ASTNode& attach, Identifier& id, ASTNode* depend = NULL);

    template<typename Resolver, typename OnResolved, typename OnFailure>
    boost::tribool resolveImpl(Resolver& resolver, OnResolved onResolved, OnFailure onFailure, ASTNode& attach, Identifier& id, ASTNode* depend);

    bool is_valid_symbol_for_call(ASTNode& node, Identifier& id);
    bool is_valid_symbol_for_top_expr(ASTNode& node);

private:
    void propogateType(ASTNode& to, ASTNode& from);
    void propogateType(ASTNode& to, TypeSpecifier& from);
    void propogateSymbol(ASTNode& to, ASTNode& from);

    template<typename NodeType>
    void resolveExprImpl(NodeType& node);

    std::tuple<boost::tribool, bool, Type*> getLogicalExprType(UnaryExpr& expr);
    std::tuple<boost::tribool, bool, Type*> getArithmeticOrBinaryExprType(UnaryExpr& expr);
    std::tuple<boost::tribool, bool, Type*> getAssignExprType(BinaryExpr& expr);
    std::tuple<boost::tribool, bool, Type*> getArithmeticOrBianryExprType(BinaryExpr& expr);
    std::tuple<boost::tribool, bool, Type*> getComparisonOrLogicalExprType(BinaryExpr& expr);

    std::tuple<boost::tribool, bool, Type*> getExprType(UnaryExpr& expr);
    std::tuple<boost::tribool, bool, Type*> getExprType(BinaryExpr& expr);

    /**
     * @brief check whether an assign operator can infer variable type
     * @return the VariableDecl of the variable if follow condition satisfied
     *
     * - lhs is primary expression
     *
     * - lhs has resolved symbol
     *
     * - lhs resolve symbol to an variable declaration
     *
     * - lhs's resolved variable declaration has NO type yet
     *
     * - rhs is not a null literal
     */
    boost::optional<VariableDecl*> canInferVariableType(BinaryExpr& node);
    void inferVariableType(BinaryExpr& node);
    void returnTypeInference(BranchStmt& node);
    void tryResolveEnumValues(EnumDecl& node);

private:
    template<typename NodeType>
    void insert_symbol(NodeType& node);

    void prepare_symbol_table(ClassDecl& cls_decl);
    void prepare_symbol_table_impl(ClassDecl& cls_decl);

    bool is_class_content_ready(ClassDecl& cls_decl) const;

    SymbolTableChain symbol_table_chain;
    std::set<tree::ClassDecl*>& class_content_ready_set;
    detail::object_replication::verification_state& verification_state;

private:
    void put_call_parameters(ASTNode& owner, ASTNode& self, const std::vector<Expression*>& parameters);
    void drop_call_parameters(ASTNode& owner, ASTNode& self);
    const std::vector<Expression*>* get_call_parameters(ASTNode& owner, ASTNode& self);

    std::map<std::pair<ASTNode*, ASTNode*>, const std::vector<Expression*>*> call_parameters_map;

private:
    // operator overload restruct
    void replaceByMethodCall(BinaryExpr& node);
    void replaceByMethodCall(UnaryExpr& node);

public:
    resolution::Instantiater instantiater;
    resolution::specialization::Grouper& specialization_grouper;

    std::size_t resolved_count;
    std::size_t unresolved_count;

private:
    std::vector<std::function<void()>> transforms;
    std::unordered_set<ASTNode*> resolved_nodes;
    std::unordered_set<ASTNode*>& failed_nodes;
    std::unordered_set<ASTNode*> unknown_nodes;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_RESOLUTIONSTAGEVISITOR_H_ */
