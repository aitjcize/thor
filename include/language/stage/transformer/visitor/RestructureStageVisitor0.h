/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_RESTRUCTURESTAGEVISITOR0_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_RESTRUCTURESTAGEVISITOR0_H_

#include "language/tree/ASTNodeFwd.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

#include "language/stage/transformer/detail/RestructureLambda0.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

/**
 * RestructureStageVisitor0 is the visitation helper for RestructureStage0
 *
 * @see RestructureStage
 */
struct RestructureStageVisitor0 : public zillians::language::tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(restructInvoker, restruct);

    RestructureStageVisitor0(bool no_system_bundle);

    void restruct(tree::ASTNode& node);

    void restruct(tree::Annotations& node);
    void restruct(tree::FunctionSpecifier& node);

    void restruct(tree::LockBlock& node);

    void restruct(tree::Source& node);

    void restruct(tree::ClassDecl& node);
    void restruct(tree::EnumDecl& node);
    void restruct(tree::FunctionDecl& node);
    void restruct(tree::TypedefDecl& node);
    void restruct(tree::VariableDecl& node);

    void restruct(tree::CallExpr& node);
    void restruct(tree::IndexExpr& node);
    void restruct(tree::LambdaExpr& node);
    void restruct(tree::StringLiteral& node);
    void restruct(tree::UnaryExpr& node);

public:
    bool hasTransforms();
    void applyTransforms();

private:
    bool no_system_bundle;
    std::vector<std::function<void()>> transforms;
    detail::RestructureLambda0 restructure_lambda_helper;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_RESTRUCTURESTAGEVISITOR0_H_ */
