/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_RESOLVER_HELPER_H_
#define ZILLIANS_LANGUAGE_RESOLVER_HELPER_H_

#include "language/tree/ASTNodeFwd.h"

namespace zillians { namespace language { namespace resolution { namespace helper {

bool is_all_resolved(const tree::TemplatedIdentifier& template_id);
bool is_all_resolved(const tree::ClassDecl& cls_decl);
bool is_all_resolved(const tree::FunctionDecl& func_decl);
bool is_all_resolved(const tree::VariableDecl& var_decl);
template<typename ContainerType>
bool is_all_resolved(const ContainerType& decls);

bool is_template(const tree::Declaration& decl);
bool is_generic_template(const tree::Declaration& decl);

} } } }

#include "language/resolver/Helper.ipp"

#endif /* ZILLIANS_LANGUAGE_RESOLVER_HELPER_H_ */
