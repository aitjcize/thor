/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_RESOLVER_IPP_
#define ZILLIANS_LANGUAGE_RESOLVER_IPP_

namespace zillians { namespace language { namespace resolution {

template<typename Collection>
inline bool isAllSameType(const Collection& nodes)
{
    bool is_same_type = true;

    if(nodes.size() > 1)
    {
        const auto first_node_type = nodes.front()->_tag();

        for(auto i = boost::next(nodes.begin()); i != nodes.end() && is_same_type; ++i)
        {
            if(first_node_type != (*i)->_tag())
            {
                is_same_type = false;
            }
        }
    }

    return is_same_type;
}

inline bool isResolved(tree::ASTNode& node)
{
    return false;
}

template<typename Resolver, typename... Resolvers>
inline bool isResolved(tree::ASTNode& node, const Resolver& resolver, const Resolvers&... resolvers)
{
    return resolver.isResolved(node) || isResolved(node, resolvers...);
}

inline boost::tribool resolve(tree::ASTNode& attach, Identifier& id, const std::vector<ASTNode*>& candidates)
{
    return false;
}

template<typename Resolver, typename... Resolvers>
inline boost::tribool resolve(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates, Resolver& resolver, Resolvers&... resolvers)
{
    boost::tribool is_resolved = resolver.resolve(attach, id, candidates);

    if(!is_resolved)
    {
        is_resolved = resolve(attach, id, candidates, resolvers...);
    }

    return is_resolved;
}

template<typename... Resolvers>
inline bool ChainedResolver<Resolvers...>::isResolved(tree::ASTNode& node) const
{
    return resolution::isResolved(node, (*static_cast<const Resolvers*>(this))...);
}

template<typename... Resolvers>
inline boost::tribool ChainedResolver<Resolvers...>::resolve(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates)
{
    if(isAllSameType(candidates))
    {
        return resolution::resolve(attach, id, candidates, (*static_cast<Resolvers*>(this))...);
    }
    else
    {
        return false;
    }
}

} } }

#endif /* ZILLIANS_LANGUAGE_RESOLVER_IPP_ */
