/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_RESOLVER_HELPER_IPP_
#define ZILLIANS_LANGUAGE_RESOLVER_HELPER_IPP_

#include "language/resolver/Helper.h"

#include <type_traits>

namespace zillians { namespace language { namespace resolution { namespace helper {

template<typename ContainerType>
inline bool is_all_resolved(const ContainerType& decls)
{
    static_assert(std::is_pointer<typename ContainerType::value_type>::value, "we expect all declarations are AST node pointers");

    typedef typename std::remove_pointer<typename ContainerType::value_type>::type DeclType;

    for(DeclType* decl: decls)
    {
        if(!is_all_resolved(*decl))
        {
            return false;
        }
    }

    return true;
}

} } } }

#endif /* ZILLIANS_LANGUAGE_RESOLVER_HELPER_IPP_ */
