/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_RESOLVER_H_
#define ZILLIANS_LANGUAGE_RESOLVER_H_

#include <map>
#include <memory>
#include <vector>
#include <boost/foreach.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/optional/optional.hpp>
#include <boost/noncopyable.hpp>
#include "language/resolver/Deductor.h"
#include "language/tree/ASTNodeFwd.h"

namespace zillians { namespace language { namespace resolution {

// Forward declarations - functions
template<typename Collection>
inline bool isAllSameType(const Collection& nodes);

template<typename Resolver, typename... Resolvers>
inline bool isResolved(tree::ASTNode& node, const Resolver& resolver, const Resolvers&... resolvers);
inline bool isResolved(tree::ASTNode& node);

template<typename Resolver, typename... Resolvers>
inline boost::tribool resolve(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates, Resolver& resolver, Resolvers&... resolvers);
inline boost::tribool resolve(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates);


// Forward declarations - classes
class Instantiater;

class PackageResolver;
class TypeResolver;
class SymbolResolver;

template<typename... Resolvers>
class ChainedResolver;


// Declarations
class Instantiater : public boost::noncopyable
{
public:
    typedef std::pair<
        tree::Declaration*         , // non-fully specialized template declaration
        std::vector<tree::Type*>  // specialization list
    > InstantiationKey;

    typedef std::map<
        InstantiationKey     , // key for instantiation: declaration + specializations
        std::vector<tree::ASTNode*>  // nodes requested instantiation
    > InstantiationMap;

    bool hasRequest() const;
    bool request(const deduction::SingleInfo& info, tree::ASTNode& attach);

    void instantiate();

private:
    tree::Declaration* instantiateOne(tree::Declaration& decl, const std::vector<tree::Type*>& specialization_list);

    InstantiationMap items;
};

class PackageResolver
{
public:
    bool isResolved(tree::ASTNode& node) const;
    boost::tribool resolve(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates);
};

class TypeResolver
{
public:
    explicit TypeResolver(Instantiater& instantiater);

    bool isResolved(tree::ASTNode& node) const;

    boost::tribool resolve(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates);

private:
    std::pair<boost::tribool, tree::Type*> resolveTemplateId(tree::ASTNode& attach, tree::TemplatedIdentifier& tid, const std::vector<tree::ASTNode*>& candidates);
    std::pair<boost::tribool, tree::Type*> resolveSimpleId(tree::ASTNode& attach, tree::SimpleIdentifier& sid, const std::vector<tree::ASTNode*>& candidates);

    boost::tribool isValidType(tree::ASTNode& resolved);
    bool isValidInstantiation(tree::TemplatedIdentifier& tid);

    Instantiater* instantiater;
};

class SymbolResolver
{
public:
    SymbolResolver(Instantiater& instantiater, const std::vector<tree::Expression*>* parameters);

    bool isResolved(tree::ASTNode& node) const;

    boost::tribool resolve(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates);

private:
    std::pair<boost::tribool, tree::ASTNode*> resolveWithDeduction(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates);
    std::pair<boost::tribool, tree::ASTNode*> resolveWithoutDeduction(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates);

    boost::tribool isValidSymbol(tree::ASTNode& resolved);
    boost::tribool isResolvingCall(const std::vector<tree::ASTNode*>& candidates);

    boost::tribool setResolved(tree::Identifier& id, tree::ASTNode& attach, tree::ASTNode& resolved_node);

    boost::tribool setResolvedImpl(tree::ASTNode& attach, tree::Declaration& decl);
    boost::tribool setResolvedImpl(tree::ASTNode& attach, tree::VariableDecl& decl);

    void reportError(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates);

    void reportCallError(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates, const std::vector<tree::Expression*>& parameters);

    Instantiater* instantiater;
    const std::vector<tree::Expression*>* parameters;
};

template<typename... Resolvers>
class ChainedResolver : public Resolvers...
{
public:
    template<typename... Factories>
    ChainedResolver(Factories&&... factories) : Resolvers(factories())... {}

    bool isResolved(tree::ASTNode& node) const;
    boost::tribool resolve(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates);
};

} } }

#include "language/resolver/Resolver.ipp"

#endif /* ZILLIANS_LANGUAGE_RESOLVER_H_ */
