/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_DEDUCTOR_H_
#define ZILLIANS_LANGUAGE_DEDUCTOR_H_

#include <string>
#include <vector>
#include <boost/logic/tribool.hpp>
#include <boost/noncopyable.hpp>
#include <boost/range/iterator_range.hpp>
#include "language/resolver/Deduction.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/tree/ASTNodeHelper.h"

namespace zillians { namespace language { namespace resolution {

//////////////////////////////////////////////////////////////////////////////
// class for deduction
//////////////////////////////////////////////////////////////////////////////
class Deductor : public boost::noncopyable
{
public:
    typedef deduction::Rank           Rank;
    typedef deduction::RankList       RankList;
    typedef deduction::SingleInfo     SingleInfo;

    typedef std::vector<SingleInfo>   InfoContainer;

    explicit Deductor(const std::vector<ASTNode*>& candidates);

    std::size_t get_in_progress_count() const;
    std::size_t get_not_match_count() const;
    std::size_t get_full_match_count() const;
    std::size_t get_best_match_count() const;

    boost::iterator_range<InfoContainer::iterator> get_in_progress_range();
    boost::iterator_range<InfoContainer::iterator> get_not_match_range();
    boost::iterator_range<InfoContainer::iterator> get_full_match_range();
    boost::iterator_range<InfoContainer::iterator> get_best_match_range();

    boost::iterator_range<InfoContainer::const_iterator> get_in_progress_range() const;
    boost::iterator_range<InfoContainer::const_iterator> get_not_match_range() const;
    boost::iterator_range<InfoContainer::const_iterator> get_full_match_range() const;
    boost::iterator_range<InfoContainer::const_iterator> get_best_match_range() const;

    boost::tribool deduct(const TemplatedIdentifier& explicit_specializations);
    boost::tribool deduct(const std::vector<Expression*>& implicit_expressions);
    boost::tribool deduct(const TemplatedIdentifier& explicit_specializations, const std::vector<Expression*>& implicit_expressions);

    std::wstring get_status() const;
    void err_status() const;

private:
    boost::tribool deduct_impl(const TemplatedIdentifier& explicit_template_id);
    boost::tribool deduct_impl(const std::vector<Expression*>& implicit_expressions);

    boost::tribool verify_best_match();

    void filter_non_template();
    void filter_non_function();
    void filter_function_specialization();
    bool filter_non_matched(const TemplatedIdentifier& use_id, const bool is_call);
    bool filter_explicit(const TemplatedIdentifier& explicit_template_id);
    bool filter_implicit(const std::vector<Expression*>& implicit_expressions, const std::vector<Type*>& implicit_types);
    bool fill_missing_arguments_by_default_types();

    template<typename Condition>
    void simple_filter_impl(Condition cond);

    void mark_in_progress_as(InfoContainer::iterator& i, SingleInfo::MatchState state);

    InfoContainer deducted_info;
    InfoContainer::iterator not_selected_begin;
    InfoContainer::iterator in_progress_begin;
    InfoContainer::iterator not_match_begin;
};

} } }

#endif /* ZILLIANS_LANGUAGE_DEDUCTOR_H_ */
