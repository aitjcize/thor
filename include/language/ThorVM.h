/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_THORVM_H_
#define ZILLIANS_LANGUAGE_THORVM_H_

#include <string>
#include <vector>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/positional_options.hpp>
#include <boost/program_options/variables_map.hpp>

#include "runtime/Reactor.h"
#include "language/ThorConfiguration.h"

namespace zillians { namespace language {

class ThorVM
{
public:
    ThorVM();
    virtual ~ThorVM();

    int main(int argc, const char** argv);

private:
    struct virtual_machine_setting;

    using variables_type = boost::program_options::variables_map;
    variables_type parse_option(int argc, const char* argv[]) const;
    bool           init_virtual_machine_setting(virtual_machine_setting& setting, const variables_type& variables) const;
    bool           init_virtual_machine_setting(virtual_machine_setting& setting, int argc, const char* argv[]   ) const;

    int execute(runtime::DomainType domain_type, runtime::VMMode vm_mode, int dev_id, const std::vector<std::string>& args);
    std::string getManglingName(std::string& name);

    void dump_help_manual() const;

    boost::program_options::options_description            options;
    boost::program_options::positional_options_description positional;

public:
    ThorBuildConfiguration config;
    std::string mangled_entry_name;
};

} } // namespace zillians::language


#endif /* ZILLIANS_LANGUAGE_THORVM_H_ */
