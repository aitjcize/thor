/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_THORCONFIGURATION_H_
#define ZILLIANS_LANGUAGE_THORCONFIGURATION_H_

#include <boost/filesystem.hpp>

#include "utility/UUIDUtil.h"

#include "language/Architecture.h"

namespace zillians { namespace language {

extern const char THOR_DEFAULT_MANIFEST_PATH    [];
extern const char THOR_DEFAULT_MANIFEST_DTD_PATH[];
extern const char THOR_DEFAULT_CACHE_PATH       [];
extern const char THOR_DEFAULT_LAST_CACHE_PATH  [];

extern const char THOR_BUNDLER [];
extern const char THOR_COMPILER[];
extern const char THOR_DRIVER  [];
extern const char THOR_LINKER  [];
extern const char THOR_MAKE    [];
extern const char THOR_DOC     [];
extern const char THOR_STUB    [];
extern const char THOR_VM      [];

extern const char THOR_DEFAULT_HOME_PATH         [];
extern const char THOR_DEFAULT_SYSTEM_BUNDLE_PATH[];

extern const char THOR_PREFIX_LIBRARY  [];

extern const char THOR_EXTENSION_SOURCE[];
extern const char THOR_EXTENSION_BUNDLE[];
extern const char THOR_EXTENSION_ASM   [];
extern const char THOR_EXTENSION_PTX   [];
extern const char THOR_EXTENSION_CU    [];
extern const char THOR_EXTENSION_CUH   [];
extern const char THOR_EXTENSION_IL    [];
extern const char THOR_EXTENSION_CL    [];
extern const char THOR_EXTENSION_SO    [];
extern const char THOR_EXTENSION_LIB   [];
extern const char THOR_EXTENSION_AST   [];
extern const char THOR_EXTENSION_BC    [];
extern const char THOR_EXTENSION_OBJ   [];
extern const char THOR_EXTENSION_CPP   [];
extern const char THOR_EXTENSION_H     [];
extern const char THOR_JIT_AST_POSTFIX [];

extern const char THOR_CPP_NATIVE_COMPILER[];
extern const char THOR_NATIVE_ARCHIVER    [];

namespace detail {

struct EnvironmentHelper
{
    enum Method
    {
        AUTOMATIC,
        BY_ENVIRONMENT_VARIABLE,
        BY_COMPILED_STATIC_PATH,
        BY_EXECUTABLE_PATH,
    };

    static bool findHomeDirectory(boost::filesystem::path& home, Method m = AUTOMATIC);
};

} // namespace 'zillians::language::detail'

class XMLValidator
{
public:
    enum class result_t
    {
        DTD_NOT_FOUND,
        DOC_NOT_FOUND,
        ILL_FORMED,
        SKIPPED,       // can't perform validating
        SUCCESS,
    };

    explicit XMLValidator(const boost::filesystem::path& xml_dtd);

    result_t validate(const boost::filesystem::path& document) const;

private:
    static const char* getToolName();
    static int execute(const std::string& command);

    const boost::filesystem::path xml_dtd;
};

struct ThorManifest
{
    enum class BundleType
    {
        JIT,
        NATIVE
    };

    ThorManifest();

    std::string name;
    std::string author;
    std::string version;
    std::string signature;      // Signature for verifying the bundle
    BundleType  bundle_type;
    bool        no_system;
    bool        using_64bit;
    UUID        project_id;

    struct DependentBundle
    {
        std::string name;
        std::string lpath;
    };

    struct DependentObject
    {
        std::string  name;
        std::string  lpath;
        Architecture arch;
    };

    struct DependentLibrary
    {
        std::string  name;
        std::string  lpath;
        Architecture arch;
    };

    struct DependentSharedLibrary
    {
        std::string  name;
        std::string  lpath;
        std::string  rpath;
        Architecture arch;
    };

    struct
    {
        std::vector<DependentBundle>        bundles;
        std::vector<DependentObject>        native_objects;
        std::vector<DependentLibrary>       native_libraries;
        std::vector<DependentSharedLibrary> native_shared_libraries;
    } deps;

    Architecture arch;

    /**
     * Read manifest from XML configuration file
     */
    bool load(const boost::filesystem::path& manifest_xml,
              const boost::filesystem::path& sdk_path,
              const XMLValidator& validator);
    /**
     * Write configuration as manifest XML file
     */
    bool save(const boost::filesystem::path& document, const XMLValidator& validator);

    bool importSystemBundle(const boost::filesystem::path& sdk_path);
};

struct ThorBuildConfiguration
{
    enum class CodeGenType
    {
        NATIVE = 0,
        JIT_TANGLE_AST = 1,
        JIT_BUNDLE_AST = 2
    };

    ThorBuildConfiguration();

    static std::string getProjectSharedObjectPath(ThorBuildConfiguration& config);
    static bool findProjectRoot(boost::filesystem::path& project_root, boost::filesystem::path current = boost::filesystem::current_path());

    bool load(const boost::filesystem::path& project_path, bool configure_build_parameters = true, bool configure_manifest = true);
    bool loadDefault(bool configure_build_parameters = true, bool configure_path = true, bool configure_manifest = true);
    bool createDirectoriesIfNecessary();
    bool configureDefaultPaths(const boost::filesystem::path& p);
    bool configureToolPath();
    bool configureProjectPaths(const boost::filesystem::path& p);
    bool loadFromCache(bool track_change = false);
    bool saveToCache(bool track_change = false);
    bool compare(const ThorBuildConfiguration& other);

    template<class Archive>
    void serialize(Archive & ar, const unsigned int)
    {
        ar & verbose;
        ar & dump_ts;
        ar & dump_llvm;
        ar & dump_graphviz;
        ar & timing;
        ar & static_test_case;
        ar & prepend_package;
        ar & max_parallel_jobs;
        ar & project_path;
        ar & manifest_path;
        ar & sdk_path;
        ar & sdk_executable_path;
        ar & sdk_bundle_path;
        ar & sdk_lib_path;
        ar & library_path;
        ar & build_path;
        ar & source_path;
        ar & extract_bundle_path;
        ar & dump_ts_dir;
        ar & dump_graphviz_dir;
        ar & binary_output_path;
        ar & stub_output_path;
    }

    bool changed;
    bool verbose;
    bool dump_ts;
    bool dump_llvm;
    bool dump_graphviz;
    bool timing;
    std::string static_test_case;
    std::string prepend_package;
    size_t max_parallel_jobs;
    CodeGenType codegen_type;
    std::string debug_tool;
    std::string debug_tool_cmd;

    boost::filesystem::path project_path;
    boost::filesystem::path manifest_path;
    boost::filesystem::path sdk_path;
    boost::filesystem::path sdk_executable_path;
    boost::filesystem::path sdk_bundle_path;
    boost::filesystem::path sdk_lib_path;
    boost::filesystem::path library_path;
    boost::filesystem::path build_path;
    boost::filesystem::path build_path_override;
    boost::filesystem::path source_path;
    boost::filesystem::path extract_bundle_path;
    boost::filesystem::path extract_bundle_path_override;
    boost::filesystem::path dump_ts_dir;
    boost::filesystem::path dump_graphviz_dir;
    boost::filesystem::path binary_output_path;
    boost::filesystem::path binary_output_path_override;
    boost::filesystem::path stub_output_path;

    ThorManifest manifest;
};

std::istream& operator >> (std::istream& in, ThorBuildConfiguration::CodeGenType& code_gen_type);
std::ostream& operator << (std::ostream& os, const ThorBuildConfiguration::CodeGenType& code_gen_type);

} } // namespace 'zillians::language'

#endif /* ZILLIANS_LANGUAGE_THORCONFIGURATION_H_ */
