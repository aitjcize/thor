/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_NETWORK_SESSION_TCPSESSION_H_
#define ZILLIANS_NETWORK_SESSION_TCPSESSION_H_

#include "core/Prerequisite.h"
#include "threading/Scheduler.h"
#include <boost/asio.hpp>
#include <atomic>

namespace zillians { namespace network { namespace session {

struct TCPSessionEngine;

struct TCPSession : public boost::asio::ip::tcp::socket
{
	typedef boost::asio::ip::tcp protocol_t;
	typedef boost::asio::ip::tcp::socket socket_t;
	friend class TCPSessionEngine;

	explicit TCPSession(Scheduler& scheduler) : socket_t(scheduler), mark_for_delete(false), mark_for_manual_delete(false)
	{
		pending_completions = 0;
		std::cout << "TCPSession ctor, this = " << this << std::endl;
	}

	~TCPSession()
	{
		std::cout << "TCPSession dtor, this = " << this << std::endl;
	}

	bool setKeepAlive(bool enable)
	{
		boost::system::error_code ec;
		boost::asio::socket_base::keep_alive option(enable);
		set_option(option, ec);
		if(ec)
		{
			return false;
		}
		return true;
	}

	bool setMinimalSend(int32 bytes)
	{
		if(native() != -1)
		{
		    if(!setsockopt(native(), SOL_SOCKET, SO_SNDLOWAT, (const char*)&bytes, sizeof(bytes)))
		    	return true;
		}

		return false;
	}

	bool setSendTimeout(uint64 timeout_in_micro_sec)
	{
		if(native() != -1)
		{
#ifdef __PLATFORM_WINDOWS__
			DWORD timeout = timeout_in_micro_sec/1000;
		    if(!setsockopt(native(), SOL_SOCKET, SO_SNDTIMEO, (const char*)&timeout, sizeof(timeout)))
		    	return true;
#else
			timeval t;
			t.tv_sec = timeout_in_micro_sec / 1000000;
			t.tv_usec = timeout_in_micro_sec % 1000000;
		    if(!setsockopt(native(), SOL_SOCKET, SO_SNDTIMEO, &t, sizeof(t)))
		    	return true;
#endif
		}

		return false;
	}

	bool setMinimalRecv(int32 bytes)
	{
		if(native() != -1)
		{
		    if(!setsockopt(native(), SOL_SOCKET, SO_RCVLOWAT, (const char*)&bytes, sizeof(bytes)))
		    	return true;
		}

		return false;
	}

	bool setRecvTimeout(uint64 timeout_in_micro_sec)
	{
		if(native() != -1)
		{
#ifdef __PLATFORM_WINDOWS__
			DWORD timeout = timeout_in_micro_sec/1000;
		    if(!setsockopt(native(), SOL_SOCKET, 	SO_RCVTIMEO, (const char*)&timeout, sizeof(timeout)))
		    	return true;
#else
			timeval t;
			t.tv_sec = timeout_in_micro_sec / 1000000;
			t.tv_usec = timeout_in_micro_sec % 1000000;
		    if(!setsockopt(native(), SOL_SOCKET, SO_RCVTIMEO, &t, sizeof(t)))
		    	return true;
#endif
		}

		return false;
	}

	bool setNoDelay(bool no_delay)
	{
		protocol_t::no_delay option(no_delay);
		boost::system::error_code error;
		set_option(option, error);
		if(error)
			return false;
		return true;
	}

	void connect(const std::string& host_name, const std::string& service_name, boost::system::error_code& error)
	{
		boost::asio::ip::tcp::resolver resolver(get_io_service());
#if (BOOST_VERSION / 100000) <= 1 && (BOOST_VERSION / 100 % 1000) <= 46 && (BOOST_VERSION / 100000) <= 1
		boost::asio::ip::tcp::resolver_query query(boost::asio::ip::tcp::v4(), host_name, service_name);
#else
		boost::asio::ip::basic_resolver_query<boost::asio::ip::tcp> query(boost::asio::ip::tcp::v4(), host_name, service_name);
#endif

#if (BOOST_VERSION / 100000) <= 1 && (BOOST_VERSION / 100 % 1000) <= 46 && (BOOST_VERSION / 100000) <= 1
		boost::asio::ip::tcp::resolver_iterator it = resolver.resolve(query, error);
#else
		boost::asio::ip::basic_resolver_iterator<boost::asio::ip::tcp> it = resolver.resolve(query, error);
#endif
		if(error) return;

#if (BOOST_VERSION / 100000) <= 1 && (BOOST_VERSION / 100 % 1000) <= 46 && (BOOST_VERSION / 100000) <= 1
		for(;it != boost::asio::ip::tcp::resolver_iterator(); ++it)
#else
		for(;it != boost::asio::ip::basic_resolver_iterator<boost::asio::ip::tcp>(); ++it)
#endif
		{
			connect(it->endpoint(), error);
			if(!error)	return;
		}

		error.assign(boost::system::posix_error::address_not_available, boost::system::get_posix_category());
	}

	void connect(const std::string& host_name, const std::string& service_name)
	{
		boost::system::error_code error;
		connect(host_name, service_name, error);
		if(error)
		{
			throw boost::system::system_error(error);
		}
	}

	void connect(const std::string& ip_address, uint16 port, /*OUT*/ boost::system::error_code& error)
	{
		std::stringstream ss; ss << port;
		connect(ip_address, ss.str(), error);
	}

	void connect(const std::string& ip_address, uint16 port)
	{
		boost::system::error_code error;
		connect(ip_address, port, error);
		if(error)
		{
			throw boost::system::system_error(error);
		}
	}

	void connect(boost::asio::ip::tcp::endpoint endpoint, /*OUT*/ boost::system::error_code& error)
	{
		if(is_open())
		{
			error.assign(boost::system::posix_error::already_connected, boost::system::get_posix_category());
			return;
		}

		// try to connect to remote through standard TCP session
		socket_t::connect(endpoint, error);
		if(error)
		{
			return;
		}
	}

	void connect(boost::asio::ip::tcp::endpoint endpoint)
	{
		boost::system::error_code error;
		connect(endpoint, error);
		if(error)
		{
			throw boost::system::system_error(error);
		}
	}

	void pushAsyncOperation()
	{
		++pending_completions;
	}

	void popAsyncOperation()
	{
		uint32 completion = --pending_completions;
		if(UNLIKELY(completion == 0 && mark_for_delete && !mark_for_manual_delete))
		{
			delete this;
		}
	}

	void markForDelete()
	{
		mark_for_delete = true;
	}

	void markForManualDelete()
	{
		mark_for_manual_delete = true;
	}

	bool mark_for_delete;
	bool mark_for_manual_delete;
	std::atomic<int> pending_completions;
};

} } }
#endif /* ZILLIANS_NETWORK_SESSION_TCPSESSION_H_ */
