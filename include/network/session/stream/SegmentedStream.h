/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_NETWORK_SESSION_SEGMENTEDSTREAM_H_
#define ZILLIANS_NETWORK_SESSION_SEGMENTEDSTREAM_H_

#include "core/Prerequisite.h"
#include "core/Buffer.h"
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/tuple/tuple.hpp>
#include <tbb/recursive_mutex.h>

namespace zillians { namespace network { namespace session {

namespace detail {

struct SegmentHeader
{
	const static std::size_t kHeaderSize = sizeof(uint32) + sizeof(uint32);
	const static std::size_t kMaxDataSize = 65536;
	const static std::size_t kDefaultBufferSize = kMaxDataSize + kHeaderSize;

	uint32 type;
	uint32 size;

	template<typename Archive>
	void serialize(Archive& ar, const unsigned int version)
	{
		UNUSED_ARGUMENT(version);

		ar & type;
		ar & size;
	}
};

}

template<typename Session>
struct SegmentedStream
{
	typedef boost::function< void (const boost::system::error_code&, const std::size_t&) > WriteCompletionHandler;

	SegmentedStream(Session& session) : session(session)
	{ }

	void writeRaw(shared_ptr<Buffer>& buffer, std::size_t size = 0)
	{
		std::size_t total_bytes_to_write = (size == 0) ? buffer->dataSize() : size;

		if(total_bytes_to_write == 0)
			return;

		for(std::size_t offset = 0; offset < total_bytes_to_write; offset += boost::asio::detail::default_max_transfer_size)
		{
			std::size_t bytes_to_write = total_bytes_to_write - offset;
			if(bytes_to_write > boost::asio::detail::default_max_transfer_size)
				bytes_to_write = boost::asio::detail::default_max_transfer_size;

			std::size_t bytes_written = boost::asio::write(session, boost::asio::buffer(buffer->rptr(), bytes_to_write));
			if(bytes_written != bytes_to_write)
			{
				throw boost::system::system_error(boost::asio::error::broken_pipe);
			}

			buffer->rskip(bytes_to_write);
		}
	}

	void write(uint32 type, std::vector< shared_ptr<Buffer> >& buffers)
	{
		std::size_t bytes_written = 0;

		// calculate the total number of bytes to send
		std::size_t bytes_to_write = 0;
		for(std::vector< shared_ptr<Buffer> >::iterator it = buffers.begin(); it != buffers.end(); ++it) bytes_to_write += (*it)->dataSize();

		// ASSERTION the given data size cannot exceed the underlying transport limit
		BOOST_ASSERT(bytes_to_write <= boost::asio::detail::default_max_transfer_size - detail::SegmentHeader::kHeaderSize);

		// if the total number of bytes to send is bigger than some threshold, use scatter send to avoid copying
		// otherwise send it in one single buffer (including the header)
		if(bytes_to_write > kScatterWriteThreshold)
		{
			BOOST_ASSERT(buffers.size() < ksMaxIOV - 1);

			// use thread-local buffer as header buffer
			Buffer* header_buffer = writeBuffer();

			// encode the header
			detail::SegmentHeader header;
			header.type = type;
			header.size = bytes_to_write;
			*header_buffer << header;

			// create asio write buffer vector to perform scatter write
			std::vector<boost::asio::mutable_buffer> write_buffers;

			// append header buffer to write buffer
			write_buffers.push_back( boost::asio::buffer(header_buffer->rptr(), detail::SegmentHeader::kHeaderSize) );

			// append all given buffers to write buffer
			for(std::vector< shared_ptr<Buffer> >::iterator it = buffers.begin(); it != buffers.end(); ++it)
				write_buffers.push_back( boost::asio::buffer((*it)->rptr(), (*it)->dataSize()) );

			// perform asio synchronous write
			{
#ifdef ZILLIANS_ASIO_NO_CONCURRENT_SYNCHRONOUS_WRITE
				tbb::spin_mutex::scoped_lock lock(mSyncSendLock);
#endif
				bytes_written = boost::asio::write(session, write_buffers);
			}

			header_buffer->clear();
		}
		else
		{
			// use thread-local buffer as write buffer
			Buffer* write_buffer = writeBuffer();

			// encode the header
			detail::SegmentHeader header;
			header.type = type;
			header.size = bytes_to_write;
			*write_buffer << header;

			// append all given buffers to write buffer
			for(std::vector< shared_ptr<Buffer> >::iterator it = buffers.begin(); it != buffers.end(); ++it)
			{
				// preserve the old read position so that it is not altered by append()
				std::size_t old_rpos = (*it)->rpos();
				write_buffer->append(**it);
				(*it)->rpos(old_rpos);
			}

			// perform asio synchronous write
			{
#ifdef ZILLIANS_ASIO_NO_CONCURRENT_SYNCHRONOUS_WRITE
				tbb::spin_mutex::scoped_lock lock(mSyncSendLock);
#endif
				bytes_written = boost::asio::write(session, boost::asio::buffer(write_buffer->rptr(), write_buffer->dataSize()));
			}

			write_buffer->clear();
		}


		if(bytes_written != bytes_to_write + detail::SegmentHeader::kHeaderSize)
		{
			throw boost::system::system_error(boost::asio::error::broken_pipe);
		}
		else
		{
			for(std::vector< shared_ptr<Buffer> >::iterator it = buffers.begin(); it != buffers.end(); ++it)
				(*it)->rskip((*it)->dataSize());
		}
	}

	void write(uint32 type, shared_ptr<Buffer>& buffer, std::size_t size = 0)
	{
		std::size_t bytes_written = 0;
		std::size_t bytes_to_write = (size == 0) ? buffer->dataSize() : size;
		BOOST_ASSERT(bytes_to_write <= buffer->dataSize());
		BOOST_ASSERT(bytes_to_write <= boost::asio::detail::default_max_transfer_size - detail::SegmentHeader::kHeaderSize);

		if(bytes_to_write > kScatterWriteThreshold)
		{
			// use thread-local buffer as header buffer
			Buffer* header_buffer = writeBuffer();

			// encode the header
			detail::SegmentHeader header;
			header.type = type;
			header.size = bytes_to_write;
			*header_buffer << header;

			// create asio write buffer vector to perform scatter write
			std::vector<boost::asio::mutable_buffer> buffers;
			buffers.push_back( boost::asio::buffer(header_buffer->rptr(), detail::SegmentHeader::kHeaderSize) );
			buffers.push_back( boost::asio::buffer(buffer->rptr(), bytes_to_write) );

			// perform asio synchronous write
			{
#ifdef ZILLIANS_ASIO_NO_CONCURRENT_SYNCHRONOUS_WRITE
				tbb::spin_mutex::scoped_lock lock(mSyncSendLock);
#endif
				bytes_written = boost::asio::write(session, buffers);
			}

			header_buffer->clear();
		}
		else
		{
			// use thread-local buffer as write buffer
			Buffer* write_buffer = writeBuffer();

			// encode the header
			detail::SegmentHeader header;
			header.type = type;
			header.size = bytes_to_write;
			*write_buffer << header;

			// append the given buffer to write buffer
			// preserve the old read position so that it is not altered by append()
			std::size_t old_rpos = buffer->rpos();
			write_buffer->append(*buffer, bytes_to_write);
			buffer->rpos(old_rpos);

			// perform asio synchronous write
			{
#ifdef ZILLIANS_ASIO_NO_CONCURRENT_SYNCHRONOUS_WRITE
				tbb::spin_mutex::scoped_lock lock(mSyncSendLock);
#endif
				bytes_written = boost::asio::write(session, boost::asio::buffer(write_buffer->rptr(), write_buffer->dataSize()));
			}

			write_buffer->clear();
		}

		if(bytes_written != bytes_to_write + detail::SegmentHeader::kHeaderSize)
		{
			throw boost::system::system_error(boost::asio::error::broken_pipe);
		}
		else
		{
			buffer->rskip(bytes_written - detail::SegmentHeader::kHeaderSize);
		}
	}

	template<typename M>
	void write(M& message)
	{
		// get the buffer from TLS
		Buffer* buffer = writeBuffer();

		try
		{
			// skip the header first
			buffer->wskip(detail::SegmentHeader::kHeaderSize);

			// serialize the data first so we know how large it is
			*buffer << message;
			size_t end_pos = buffer->wpos();

			// encode the header
			detail::SegmentHeader header;
			header.type = M::TYPE;
			header.size = end_pos - detail::SegmentHeader::kHeaderSize;

			BOOST_ASSERT(header.size <= boost::asio::detail::default_max_transfer_size - detail::SegmentHeader::kHeaderSize);

			buffer->wpos(0);
			*buffer << header;
			buffer->wpos(end_pos);

			std::size_t bytes_written;
			{
#ifdef ZILLIANS_ASIO_NO_CONCURRENT_SYNCHRONOUS_WRITE
				tbb::spin_mutex::scoped_lock lock(mSyncSendLock);
#endif
				bytes_written = boost::asio::write(session, boost::asio::buffer(buffer->rptr(), buffer->dataSize()));
			}

			if(bytes_written != buffer->dataSize())
			{
				throw boost::system::system_error(boost::asio::error::broken_pipe);
			}

			buffer->clear();
		}
		catch(boost::system::system_error& e)
		{
			throw e;
		}
		catch(std::exception& e)
		{
			throw e;
		}
	}

	/// Asynchronously write a collection of buffers to the session
	template<typename Handler>
	void writeAsync(uint32 type, std::vector< shared_ptr<Buffer> >& buffers, Handler handler)
	{
		// calculate the total number of bytes to send
		std::size_t bytes_to_write = 0;
		for(std::vector< shared_ptr<Buffer> >::iterator it = buffers.begin(); it != buffers.end(); ++it) bytes_to_write += (*it)->dataSize();

		// ASSERTION the given data size cannot exceed the underlying transport limit
		BOOST_ASSERT(bytes_to_write <= boost::asio::detail::default_max_transfer_size - detail::SegmentHeader::kHeaderSize);

		// if the total number of bytes to send is bigger than some threshold, use scatter send to avoid copying
		// otherwise send it in one single buffer (including the header)
		if(bytes_to_write > kScatterWriteThreshold)
		{
			BOOST_ASSERT(buffers.size() < ksMaxIOV - 1);

			// create a local buffer to store headers
			shared_ptr<Buffer> header_buffer(new Buffer(detail::SegmentHeader::kHeaderSize));

			// encode the header
			detail::SegmentHeader header;
			header.type = type;
			header.size = bytes_to_write;
			*header_buffer << header;

			// create asio write buffer vector to perform scatter write
			std::vector<boost::asio::mutable_buffer> write_buffers;
			write_buffers.push_back( boost::asio::buffer(header_buffer->rptr(), header_buffer->dataSize()) );

			// insert all given buffer one by one
			for(std::vector< shared_ptr<Buffer> >::iterator it = buffers.begin(); it != buffers.end(); ++it)
				write_buffers.push_back( boost::asio::buffer((*it)->rptr(), (*it)->dataSize()) );

			// lock the structures for write queues
			tbb::recursive_mutex::scoped_lock lock(mAsyncSendLock);

			// if there's no on-going write event, write directly and push the operation to sending queue
			// otherwise push the operation onto pending queue
			if(mSendingQueue.empty())
			{
				void (SegmentedStream::*f)(const boost::system::error_code&, const std::size_t, const std::size_t) = &SegmentedStream::writeAsyncBufferCompleted<Handler>;

				// increment pending completion
				session.pushAsyncOperation();

				// perform asio async write
				boost::asio::async_write(
						session,
						write_buffers,
						boost::bind(f, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, detail::SegmentHeader::kHeaderSize + header.size));

				// push the write operation along with the completion handler into sending queue
				mSendingQueue.push_back(boost::make_tuple(handler, WriteOperation(header_buffer, buffers)));
			}
			else
			{
				// push the write operation along with the completion handler into pending queue
				mPendingQueue.push_back(boost::make_tuple(handler, WriteOperation(header_buffer, buffers)));
			}
		}
		else
		{
			// create a local buffer to store headers as well as given buffers
			shared_ptr<Buffer> write_buffer(new Buffer(detail::SegmentHeader::kHeaderSize + bytes_to_write));

			// encode the header
			detail::SegmentHeader header;
			header.type = type;
			header.size = bytes_to_write;
			*write_buffer << header;

			// append all given buffers to the write buffer
			for(std::vector< shared_ptr<Buffer> >::iterator it = buffers.begin(); it != buffers.end(); ++it)
			{
				// preserve the old read position so that it is not altered by append()
				std::size_t old_rpos = (*it)->rpos();
				write_buffer->append(**it);
				(*it)->rpos(old_rpos);
			}

			// consume all given buffers (since we have encoded them into a single write buffer
			for(std::vector< shared_ptr<Buffer> >::iterator it = buffers.begin(); it != buffers.end(); ++it)
			{
				(*it)->rskip((*it)->dataSize());
			}

			// lock the structures for write queues
			tbb::recursive_mutex::scoped_lock lock(mAsyncSendLock);

			// if there's no on-going write event, write directly and push the operation to sending queue
			// otherwise push the operation onto pending queue
			if(mSendingQueue.empty())
			{
				void (SegmentedStream::*f)(const boost::system::error_code&, const std::size_t, const std::size_t) = &SegmentedStream::writeAsyncBufferCompleted<Handler>;

				// increment pending completion
				session.pushAsyncOperation();

				// perform asio async write
				boost::asio::async_write(
						session,
						boost::asio::buffer(write_buffer->rptr(), write_buffer->dataSize()),
						boost::bind(f, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, detail::SegmentHeader::kHeaderSize + header.size));

				// push the write operation along with the completion handler into sending queue
				mSendingQueue.push_back(boost::make_tuple(handler, WriteOperation(write_buffer)));
			}
			else
			{
				// push the write operation along with the completion handler into pending queue
				mPendingQueue.push_back(boost::make_tuple(handler, WriteOperation(write_buffer)));
			}
		}
	}

	/// Asynchronously write a buffer to the session
	template<typename Handler>
	void writeAsync(uint32 type, shared_ptr<Buffer>& buffer, Handler handler, std::size_t size = 0)
	{
		// calculate the total number of bytes to send
		std::size_t bytes_to_write = (size == 0) ? buffer->dataSize() : size;

		// ASSERTION the number of bytes to write cannot exceed the actual data size of the given buffer
		BOOST_ASSERT(bytes_to_write <= buffer->dataSize());

		// ASSERTION the given data size cannot exceed the underlying transport limit
		BOOST_ASSERT(bytes_to_write <= boost::asio::detail::default_max_transfer_size - detail::SegmentHeader::kHeaderSize);

		// if the total number of bytes to send is bigger than some threshold, use scatter send to avoid copying
		// otherwise send it in one single buffer (including the header)
		if(bytes_to_write > kScatterWriteThreshold)
		{
			// create a local buffer to store headers
			shared_ptr<Buffer> header_buffer(new Buffer(detail::SegmentHeader::kHeaderSize));

			// encode the header
			detail::SegmentHeader header;
			header.type = type;
			header.size = bytes_to_write;
			*header_buffer << header;

			// create asio write buffer vector to perform scatter write
			std::vector<boost::asio::mutable_buffer> buffers;
			buffers.push_back( boost::asio::buffer(header_buffer->rptr(), header_buffer->dataSize()) );
			buffers.push_back( boost::asio::buffer(buffer->rptr(), bytes_to_write) );

			// lock the structures for write queues
			tbb::recursive_mutex::scoped_lock lock(mAsyncSendLock);

			// if there's no on-going write event, write directly and push the operation to sending queue
			// otherwise push the operation onto pending queue
			if(mSendingQueue.empty())
			{
				void (SegmentedStream::*f)(const boost::system::error_code&, const std::size_t, const std::size_t) = &SegmentedStream::writeAsyncBufferCompleted<Handler>;

				// increment pending completion
				session.pushAsyncOperation();

				// perform asio async write
				boost::asio::async_write(
						session,
						buffers,
						boost::bind(f, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, detail::SegmentHeader::kHeaderSize + header.size));

				// push the write operation along with the completion handler into sending queue
				mSendingQueue.push_back(boost::make_tuple(handler, WriteOperation(header_buffer, buffer)));
			}
			else
			{
				// push the write operation along with the completion handler into pending queue
				mPendingQueue.push_back(boost::make_tuple(handler, WriteOperation(header_buffer, buffer)));
			}
		}
		else
		{
			// create a local buffer (which will be held in the write completion handler)
			shared_ptr<Buffer> write_buffer(new Buffer(detail::SegmentHeader::kHeaderSize + bytes_to_write));

			detail::SegmentHeader header;
			header.type = type;
			header.size = bytes_to_write;
			*write_buffer << header;

			// append the buffer into the write buffer
			write_buffer->append(*buffer, bytes_to_write);

			// lock the structures for write queues
			tbb::recursive_mutex::scoped_lock lock(mAsyncSendLock);

			// if there's no on-going write event, write directly and push the operation to sending queue
			// otherwise push the operation onto pending queue
			if(mSendingQueue.empty())
			{
				void (SegmentedStream::*f)(const boost::system::error_code&, const std::size_t, const std::size_t) = &SegmentedStream::writeAsyncBufferCompleted<Handler>;

				// increment pending completion
				session.pushAsyncOperation();

				// perform asio async write
				boost::asio::async_write(
						session,
						boost::asio::buffer(write_buffer->rptr(), write_buffer->dataSize()),
						boost::bind(f, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, detail::SegmentHeader::kHeaderSize + header.size));

				// push the write operation along with the completion handler into sending queue
				mSendingQueue.push_back(boost::make_tuple(handler, WriteOperation(write_buffer)));
			}
			else
			{
				// push the write operation along with the completion handler into pending queue
				mPendingQueue.push_back(boost::make_tuple(handler, WriteOperation(write_buffer)));
			}
		}
	}

	/// Asynchronously write a message structure to the session
	template<typename M, typename Handler>
	void writeAsync(M& message, Handler handler)
	{
		// create a local buffer (which will be held in the write completion handler)
		shared_ptr<Buffer> write_buffer(new Buffer(detail::SegmentHeader::kHeaderSize + Buffer::probeSize(message)));

		// ASSERTION the given data size cannot exceed the underlying transport limit
		BOOST_ASSERT(write_buffer->dataSize() <= boost::asio::detail::default_max_transfer_size);

		// skip the header first
		write_buffer->wskip(detail::SegmentHeader::kHeaderSize);

		// serialize the data first so we know how large it is
		*write_buffer << message;
		size_t end_pos = write_buffer->wpos();

		// format the header and save into buffer
		detail::SegmentHeader header;
		header.type = M::TYPE;
		header.size = end_pos - detail::SegmentHeader::kHeaderSize;
		write_buffer->wpos(0);
		*write_buffer << header;
		write_buffer->wpos(end_pos);

		// lock the structures for write queues
		tbb::recursive_mutex::scoped_lock lock(mAsyncSendLock);

		// if there's no on-going write event, write directly and push the operation to sending queue
		// otherwise push the operation onto pending queue
		if(mSendingQueue.empty())
		{
			void (SegmentedStream::*f)(const boost::system::error_code&, const std::size_t, const std::size_t) = &SegmentedStream::writeAsyncBufferCompleted<Handler>;

			// increment pending completion
			session.pushAsyncOperation();

			// perform asio async write
			boost::asio::async_write(
					session,
					boost::asio::buffer(write_buffer->rptr(), write_buffer->dataSize()),
					boost::bind(f, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, detail::SegmentHeader::kHeaderSize + header.size));

			// push the write operation along with the completion handler into sending queue
			mSendingQueue.push_back(boost::make_tuple(handler, WriteOperation(write_buffer)));
		}
		else
		{
			// push the write operation along with the completion handler into pending queue
			mPendingQueue.push_back(boost::make_tuple(handler, WriteOperation(write_buffer)));
		}
	}

	void readRaw(shared_ptr<Buffer>& buffer, std::size_t size = 0)
	{
		std::size_t total_bytes_to_read = (size == 0) ? buffer->freeSize() : size;

		if(total_bytes_to_read < size)
		{
			buffer->resize(size);
			total_bytes_to_read = size;
		}

		if(total_bytes_to_read == 0)
			return;

		for(std::size_t offset = 0; offset < total_bytes_to_read; offset += boost::asio::detail::default_max_transfer_size)
		{
			std::size_t bytes_to_read = total_bytes_to_read - offset;
			if(bytes_to_read > boost::asio::detail::default_max_transfer_size)
				bytes_to_read = boost::asio::detail::default_max_transfer_size;

			std::size_t bytes_read = boost::asio::read(session, boost::asio::buffer(buffer->wptr(), bytes_to_read));

			if(bytes_to_read != bytes_read)
			{
				throw boost::system::system_error(boost::asio::error::broken_pipe);
			}

			buffer->wskip(bytes_read);
		}
	}

	void read(uint32& type, shared_ptr<Buffer> buffer)
	{
		UNUSED_ARGUMENT(type);

		// read the header synchronously
		boost::asio::read(session, boost::asio::buffer(buffer->wptr(), detail::SegmentHeader::kHeaderSize));
		buffer->wskip(detail::SegmentHeader::kHeaderSize);

		// deserialize the header
		detail::SegmentHeader header;
		*buffer >> header;
		buffer->clear();

		// make sure the requested data size is smaller than the buffer's available size
		if(header.size > buffer->freeSize())
		{
			throw boost::system::system_error(boost::asio::error::no_buffer_space);
		}

		// read the data synchronously
		std::size_t bytes_transferred = boost::asio::read(session, boost::asio::buffer(buffer->wptr(), header.size));

		if(header.size != bytes_transferred)
		{
			throw boost::system::system_error(boost::asio::error::broken_pipe);
		}

		buffer->wskip(bytes_transferred);
	}

	template<typename M>
	void read(M& message)
	{
		// get the buffer from TLS
		Buffer* buffer = readBuffer();

		try
		{
			// read the header synchronously
			boost::asio::read(session, boost::asio::buffer(buffer->wptr(), detail::SegmentHeader::kHeaderSize));
			buffer->wskip(detail::SegmentHeader::kHeaderSize);

			// deserialize the header
			detail::SegmentHeader header;
			*buffer >> header;
			buffer->clear();

			if(header.type != M::TYPE)
			{
				throw boost::system::system_error(boost::asio::error::broken_pipe);
			}

			// read the data synchronously
			boost::asio::read(session, boost::asio::buffer(buffer->wptr(), header.size));
			buffer->wskip(header.size);

			// deserialize the message
			*buffer >> message;

			buffer->clear();
		}
		catch(boost::system::system_error& e)
		{
			buffer->clear();
			throw e;
		}
		catch(std::exception& e)
		{
			buffer->clear();
			throw e;
		}

		buffer->clear();
	}

	template<typename Handler>
	void readAsync(shared_ptr<Buffer> buffer, Handler handler, std::size_t size = 0)
	{
		void (SegmentedStream::*f)(const boost::system::error_code&, const std::size_t&, shared_ptr<Buffer>, boost::tuple<Handler>) = &SegmentedStream::readAsyncCompleted<Handler>;

		std::size_t bytes_to_read = (size == 0) ? buffer->freeSize() : size;

		// increment pending completion
		session.pushAsyncOperation();

		boost::asio::async_read(
				session,
				boost::asio::buffer(buffer->wptr(), bytes_to_read),
				boost::bind(f, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, buffer, boost::make_tuple(handler)));
	}

	 /// Asynchronously read a data structure from the socket
	template<typename M, typename Handler>
	void readAsync(M& message, Handler handler)
	{
		void (SegmentedStream::*f)(const boost::system::error_code&, const std::size_t&, M&, boost::tuple<Handler>, shared_ptr<Buffer>) = &SegmentedStream::readAsyncHeaderCompleted<M, Handler>;

		// create a local buffer (which will be held in the read completion handler)
		shared_ptr<Buffer> buffer(new Buffer(detail::SegmentHeader::kMaxDataSize));

		// increment pending completion
		session.pushAsyncOperation();

		// issue a read operation to read exactly the number of bytes in a header
		boost::asio::async_read(
				session,
				boost::asio::buffer(buffer->wptr(), detail::SegmentHeader::kHeaderSize),
				boost::bind(f, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, boost::ref(message), boost::make_tuple(handler), buffer));
	}

private:
	inline Buffer* readBuffer()
	{
		Buffer* buffer = tls_buffer_for_read.get();
		if(!buffer)
		{
			buffer = new Buffer(detail::SegmentHeader::kDefaultBufferSize);
			tls_buffer_for_read.reset(buffer);
		}
		return buffer;
	}

	inline Buffer* writeBuffer()
	{
		Buffer* buffer = tls_buffer_for_write.get();
		if(!buffer)
		{
			buffer = new Buffer(detail::SegmentHeader::kDefaultBufferSize);
			tls_buffer_for_write.reset(buffer);
		}
		return buffer;
	}

private:

	template<typename Handler>
	void writeAsyncBufferCompleted(const boost::system::error_code& ec, const std::size_t bytes_transferred, const std::size_t expected_bytes_transferred)
	{
		UNUSED_ARGUMENT(expected_bytes_transferred);

		tbb::recursive_mutex::scoped_lock lock(mAsyncSendLock);

		// if session closed, receiving zero-length write completion
		if(UNLIKELY(bytes_transferred == 0) || ec)
		{
			// invoke all callbacks in sending queue
			for(auto it = mSendingQueue.begin(); it != mSendingQueue.end(); it++)
			{
				boost::get<0>(*it)(ec, 0);
			}
			mSendingQueue.clear();

			// invoke all callbacks in pending queue
			for(auto it = mPendingQueue.begin(); it != mPendingQueue.end(); it++)
			{
				boost::get<0>(*it)(ec, 0);
			}
			mPendingQueue.clear();
		}
		else
		{
			{
				std::size_t remaining_bytes_completed = bytes_transferred;
				while(remaining_bytes_completed > 0 && !mSendingQueue.empty())
				{
					std::size_t completed_bytes = 0;
					boost::tuple< WriteCompletionHandler, WriteOperation >& op = mSendingQueue.front();

					bool all_consumed = true;
					for(auto it = boost::get<1>(op).buffers.begin(); it != boost::get<1>(op).buffers.end(); ++it)
					{
						if((*it).get())
						{
							std::size_t size = (*it)->dataSize();
							if(remaining_bytes_completed >= size)
							{
								(*it)->rskip(size);
								remaining_bytes_completed -= size;
								completed_bytes += size;
								(*it).reset();
							}
							else
							{
								(*it)->rskip(remaining_bytes_completed);
								completed_bytes += remaining_bytes_completed;
								all_consumed = false;
								break;
							}
						}
					}

					// TODO make exception safe here
					if(LIKELY(all_consumed))
					{
						boost::get<0>(op)(ec, completed_bytes);
						mSendingQueue.pop_front();
					}
					else
					{
						boost::system::error_code error(boost::asio::error::broken_pipe);
						boost::get<0>(op)(error, completed_bytes);
						break;
					}
				}
			}

			// NOTE the sending queue is supposed to be empty every time the send is completed
			if(UNLIKELY(!mSendingQueue.empty()))
			{
				BOOST_ASSERT(mSendingQueue.empty());
			}

			// for every item in the sending queue, create an entry in the buffer vector
			std::vector<boost::asio::mutable_buffer> buffers;
			std::size_t total_size = 0;
			for(auto it = mSendingQueue.begin(); it != mSendingQueue.end(); ++it)
			{
				for(auto it_buffers = boost::get<1>(*it).buffers.begin(); it_buffers != boost::get<1>(*it).buffers.end(); ++it_buffers)
				{
					if(LIKELY((*it_buffers).get()))
					{
						std::size_t size = (*it_buffers)->dataSize();
						buffers.push_back(boost::asio::buffer((*it_buffers)->rptr(), size));
						total_size += size;
					}
				}
			}

			if(total_size >= boost::asio::detail::default_max_transfer_size)
			{
				printf("damn mSendingQueue.size() = %ld\n", mSendingQueue.size());
				printf("damn total_size = %ld\n", total_size);
				BOOST_ASSERT(total_size < boost::asio::detail::default_max_transfer_size);
			}

			// move all items in pending queue to sending queue
			while(!mPendingQueue.empty() && buffers.size() < ksMaxIOV)
			{
				boost::tuple< WriteCompletionHandler, WriteOperation >& op = mPendingQueue.front();

				std::size_t size = 0;
				for(std::vector< shared_ptr<Buffer> >::iterator it_buffers = boost::get<1>(op).buffers.begin(); it_buffers != boost::get<1>(op).buffers.end(); ++it_buffers)
				{
					if(LIKELY((*it_buffers).get()))
					{
						size += (*it_buffers)->dataSize();
					}
				}

				if(total_size + size >= boost::asio::detail::default_max_transfer_size)
					break;

				for(std::vector< shared_ptr<Buffer> >::iterator it_buffers = boost::get<1>(op).buffers.begin(); it_buffers != boost::get<1>(op).buffers.end(); ++it_buffers)
				{
					if(LIKELY((*it_buffers).get()))
					{
						//size += (*it_buffers)->dataSize();
						buffers.push_back(boost::asio::buffer((*it_buffers)->rptr(), (*it_buffers)->dataSize()));
					}
				}

				total_size += size;

				mSendingQueue.push_back(op); mPendingQueue.pop_front();
			}

			if(buffers.size() > 0)
			{
				BOOST_ASSERT(total_size < boost::asio::detail::default_max_transfer_size);

				void (SegmentedStream::*f)(const boost::system::error_code&, const std::size_t, const std::size_t) = &SegmentedStream::writeAsyncBufferCompleted<Handler>;

				// increment pending completion
				session.pushAsyncOperation();

				boost::asio::async_write(
						session,
						buffers,
						boost::bind(f, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, total_size));
			}
		}

		// finish up, decrement pending completion
		session.popAsyncOperation();
	}

	template <typename Handler>
	void readAsyncCompleted(const boost::system::error_code& ec, const std::size_t& bytes_transferred, shared_ptr<Buffer> buffer, boost::tuple<Handler> handler)
	{
		buffer->wskip(bytes_transferred);
		boost::get<0>(handler)(ec, bytes_transferred);

		// finish up, decrement pending completion
		session.popAsyncOperation();
	}


	/// Handle a completed read of a message header. The handler is passed using a tuple since boost::bind seems to have trouble binding a function object created using boost::bind as a parameter
	template<typename M, typename Handler>
	void readAsyncHeaderCompleted(const boost::system::error_code& ec, const std::size_t& bytes_transferred, M& message, boost::tuple<Handler> handler, shared_ptr<Buffer> buffer)
	{
		if(!ec)
		{
			if(bytes_transferred == detail::SegmentHeader::kHeaderSize)
			{
				// the header has been read, update the writing position
				buffer->wskip(detail::SegmentHeader::kHeaderSize);

				detail::SegmentHeader header;
				try
				{
					// de-serialize the header
					*buffer >> header;
					buffer->clear();
				}
				catch (std::exception& e)
				{
					// unable to decode data
					boost::system::error_code error(boost::asio::error::broken_pipe);
					boost::get<0>(handler)(error, bytes_transferred);
				}

				//printf("received header, type = %d, size = %d\n", header.type, header.size);
				// check the data size & type
				if(UNLIKELY(header.size > detail::SegmentHeader::kMaxDataSize || header.type != M::TYPE))
				{
					boost::system::error_code error(boost::asio::error::broken_pipe);
					boost::get<0>(handler)(error, 0);
				}
				else
				{
					// everything is ok, we should continue reading the data into input buffer
					void (SegmentedStream::*f)(const boost::system::error_code&, const std::size_t&, M&, boost::tuple<Handler>, shared_ptr<Buffer>) = &SegmentedStream::readAsyncDataCompleted<M, Handler>;

					session.pushAsyncOperation();

					boost::asio::async_read(
							session,
							boost::asio::buffer(buffer->wptr(), header.size),
							boost::bind(f, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, boost::ref(message), handler, buffer));
				}
			}
			else
			{
				boost::system::error_code error(boost::asio::error::broken_pipe);
				boost::get<0>(handler)(ec, 0);
			}
		}
		else
		{
			boost::get<0>(handler)(ec, bytes_transferred);
		}

		// finish up, decrement pending completion
		session.popAsyncOperation();
	}

	/// Handle a completed read of message data.
	template<typename M, typename Handler>
	void readAsyncDataCompleted(const boost::system::error_code& ec, const std::size_t& bytes_transferred, M& message, boost::tuple<Handler> handler, shared_ptr<Buffer> buffer)
	{
		if(!ec)
		{
			buffer->wskip(bytes_transferred);

			// extract the data structure from the data just received
			try
			{
				// deserialize the message
				*buffer >> message;
			}
			catch (std::exception& e)
			{
				// unable to decode data
				boost::system::error_code error(boost::asio::error::broken_pipe);
				boost::get<0>(handler)(error, bytes_transferred);
			}

			try
			{
				// inform caller that data has been received ok
				boost::get<0>(handler)(ec, bytes_transferred);
			}
			catch(std::exception& e)
			{
				// ignore the exception thrown by the handler
			}
		}
		else
		{
			boost::get<0>(handler)(ec, bytes_transferred);
		}

		// finish up, decrement pending completion
		session.popAsyncOperation();
	}

private:
	const static std::size_t kScatterWriteThreshold = 256UL;
	const static std::size_t ksMaximumSupportedContextTypes = 8UL;
	const static std::size_t ksMaxIOV = boost::asio::detail::max_iov_len;

private:
	Session& session;
	boost::thread_specific_ptr<Buffer> tls_buffer_for_read;
	boost::thread_specific_ptr<Buffer> tls_buffer_for_write;
	std::deque<shared_ptr<Buffer>> sending_queue;
//	boost::asio::strand read_strand;
//	boost::asio::strand write_strand;

private:
	tbb::recursive_mutex mAsyncSendLock;

	struct WriteOperation
	{
		WriteOperation()
		{ }

		WriteOperation(shared_ptr<Buffer> d)
		{ buffers.push_back(d); }

		WriteOperation(shared_ptr<Buffer> h, shared_ptr<Buffer> d)
		{ buffers.push_back(h); buffers.push_back(d); }

		WriteOperation(std::vector< shared_ptr<Buffer> >& b)
		{
			for(std::vector< shared_ptr<Buffer> >::iterator it = b.begin(); it != b.end(); ++it)
			{
				buffers.push_back(*it);
			}
		}

		WriteOperation(shared_ptr<Buffer> h, std::vector< shared_ptr<Buffer> >& b)
		{
			buffers.push_back(h);
			for(std::vector< shared_ptr<Buffer> >::iterator it = b.begin(); it != b.end(); ++it)
			{
				buffers.push_back(*it);
			}
		}

		std::vector< shared_ptr<Buffer> > buffers;
	};

	std::list< boost::tuple< WriteCompletionHandler, WriteOperation > > mPendingQueue;
	std::list< boost::tuple< WriteCompletionHandler, WriteOperation > > mSendingQueue;
};

} } }

#endif /* ZILLIANS_NETWORK_SESSION_SEGMENTEDSTREAM_H_ */
