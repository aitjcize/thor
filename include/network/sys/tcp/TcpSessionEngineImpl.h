/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @date Aug 9, 2009 sdk - Initial version created.
 */


#ifndef ZILLIANS_NETWORKING_SYS_TCPSESSIONENGINEIMPL_H_
#define ZILLIANS_NETWORKING_SYS_TCPSESSIONENGINEIMPL_H_

#include "core/Prerequisite.h"
#include "utility/StringUtil.h"
#include "utility/Foreach.h"
#include "network/sys/SessionCommon.h"
#include "network/sys/Session.h"
#include "network/sys/Placeholders.h"
#include "network/sys/Dispatcher.h"
#include "network/sys/tcp/TcpSessionCommon.h"
#include "network/sys/tcp/TcpSessionImpl.h"

#define SUPPORT_MULTIPLE_REACTOR 0

namespace zillians { namespace network { namespace sys {

/**
 * This is a SessionEngineT specialization for SessionTransport::tcp
 */
template <>
class SessionEngineT<SessionTransport::tcp> : public SessionEngineBase
{
public:
	typedef boost::asio::ip::tcp Protocol;
	typedef boost::asio::io_service IoService;
	typedef Protocol::endpoint Endpoint;
	typedef Protocol::acceptor Acceptor;
	typedef Protocol::resolver Resolver;
	typedef SessionT<SessionTransport::tcp> Session;
	typedef DispatcherT<Session> Dispatcher;

	typedef boost::function< void () > CloseCallback;
	typedef boost::function< void (const boost::system::error_code&) > ErrorCallback;
	typedef TcpSessionDispatcherContext DispatcherContext;

public:
	SessionEngineT(IoService* io = NULL)
	{
		mDispatcher = new Dispatcher(256);
		mAcceptor = NULL;

		if(io)
		{
			mIsIoServiceOwner = false;
			mIoService = io;
		}
		else
		{
			mIsIoServiceOwner = true;
			mIoService = new boost::asio::io_service;
		}

		mAcceptor = new Acceptor(*mIoService);
		mResolver = new Resolver(*mIoService);

#if SUPPORT_MULTIPLE_REACTOR
		mCurrentReactorIndex = 0;
#endif
	}

	~SessionEngineT()
	{
		if(mAcceptor->is_open())
		{
			boost::system::error_code error;
			mAcceptor->cancel(error);
			mAcceptor->close(error);
		}

		mResolver->cancel();

		if(mIsIoServiceOwner)
		{
			mIoService->stop();
			SAFE_DELETE(mIoService);
		}

		SAFE_DELETE(mDispatcher);
		SAFE_DELETE(mAcceptor);
		SAFE_DELETE(mResolver);
	}

public:
#if SUPPORT_MULTIPLE_REACTOR
	void createReactor(uint32 worker_threads = 1, IoService* ios = NULL)
	{
		mReactors.push_back(shared_ptr<Reactor>(new Reactor(worker_threads, ios)));
	}
#endif

public:
	Session* createSession()
	{
#if SUPPORT_MULTIPLE_REACTOR
		if(mReactors.size() == 0)
		{
			createReactor();
		}
		uint64 current_index = mCurrentReactorIndex % mReactors.size();
		++mCurrentReactorIndex;
		printf("using reactor #%ld\n", current_index);
		Session* p = new Session(*mReactors[current_index]->ios);
#else
		Session* p = new Session(*mIoService);
#endif
		return p;
	}

public:
	void listen(const Protocol& type, uint16 port)
	{
		boost::system::error_code error;

		BOOST_ASSERT(mAcceptor != NULL);
		if(mAcceptor->is_open())
		{
			mAcceptor->cancel(error);
			mAcceptor->close(error);
		}

		mAcceptor->open(type, error);
		if(error) { throw boost::system::system_error(error); }

		// re-use the address (in some system, the listening port is not free immediately after program exit, so we have to re-use the port)
		boost::asio::socket_base::reuse_address option(true);
		mAcceptor->set_option(option);

		mAcceptor->bind(Endpoint(type, port), error);
		if(error) { throw boost::system::system_error(error); }

		mAcceptor->listen(SOMAXCONN, error);
		if(error) { throw boost::system::system_error(error); }
	}

	template <typename Handler>
	void listenAsync(const Protocol& type, uint16 port, Handler handler)
	{
		mIoService->dispatch(boost::bind(&SessionEngineT::doListenAsync<Handler>, this, type, port, boost::make_tuple(handler)));
	}

	void getListeningAddress(std::string& address, uint16& port)
	{
		if(!mAcceptor->is_open())
		{
			throw std::runtime_error("acceptor is not listening");
		}

		boost::system::error_code error;
		Protocol::endpoint ep = mAcceptor->local_endpoint(error);
		if(error) { throw boost::system::system_error(error); }

		address = ep.address().to_string();
		port = ep.port();
	}

	void accept(Session* session)
	{
		BOOST_ASSERT(mAcceptor->is_open());

		boost::system::error_code error;

		beforeAccept(session, error);
		if(error) { throw boost::system::system_error(error); }

		mAcceptor->accept(session->socket(), error);
		if(error) { throw boost::system::system_error(error); }

		afterAccept(session, error);
		if(error) { session->close(); throw boost::system::system_error(error); }
	}

	template <typename Handler>
	void acceptAsync(Session* session, Handler handler)
	{
		BOOST_ASSERT(session != NULL);
		void (SessionEngineT::*f)(Session* session, boost::tuple<Handler>) = &SessionEngineT::doAcceptAsync<Handler>;

		mIoService->dispatch(boost::bind(f, this, session, boost::make_tuple(handler)));
	}

	void connect(Session* session, const Protocol& type, const std::string& endpoint_name)
	{
		std::vector<std::string> tokens = zillians::StringUtil::tokenize(endpoint_name, ":", false);
		if(tokens.size() != 2)
			throw std::invalid_argument("given endpoint format is not correct, should be \"host_name:service_name\"");

		connect(session, type, tokens[0], tokens[1]);
	}

	void connect(Session* session, const Protocol& type, const std::string& host_name, const std::string& service_name)
	{
		boost::system::error_code error;

		Resolver::query query(type, host_name, service_name);
		Resolver::iterator it = mResolver->resolve(query, error);
		if(error) { throw boost::system::system_error(error); }

		beforeConnect(session, error);
		if(error) { throw boost::system::system_error(error); }

		session->socket().connect(*it, error);
		if(error) { throw boost::system::system_error(error); }

		afterConnect(session, error);
		if(error) { session->close(); throw boost::system::system_error(error); }
	}

	template <typename Handler>
	void connectAsync(Session* session, const Protocol& type, const std::string& endpoint_name, Handler handler)
	{
		std::vector<std::string> tokens = zillians::StringUtil::tokenize(endpoint_name, ":", false);
		if(tokens.size() != 2)
			throw std::invalid_argument("given endpoint format is not correct, should be \"host_name:service_name\"");

		connectAsync<Handler>(session, type, tokens[0], tokens[1], handler);
	}

	template <typename Handler>
	void connectAsync(Session* session, const Protocol& type, const std::string& host_name, const std::string& service_name, Handler handler)
	{
		BOOST_ASSERT(session != NULL);
		void (SessionEngineT::*f)(Session* session, const Protocol& type, const std::string& host_name, const std::string& service_name, boost::tuple<Handler>) = &SessionEngineT::doConnectAsync<Handler>;

		session->getIoService().dispatch(boost::bind(f, this, session, type, host_name, service_name, boost::make_tuple(handler)));
	}


	/**
	 *
	 * @param session
	 * @param onClose
	 * @param onError
	 * @param dispatcher
	 * @remarks This is an asynchronous call.
	 */
	void startDispatch(Session* session, CloseCallback onClose, ErrorCallback onError, Dispatcher* dispatcher = NULL)
	{
		BOOST_ASSERT(session != NULL);
		if(!dispatcher) dispatcher = mDispatcher;

		session->getIoService().dispatch(boost::bind(&SessionEngineT::doStartDispatch, this, session, onClose, onError, dispatcher));
	}


	/**
	 *
	 * @param session
	 * @param onClose
	 * @param onError
	 * @param dispatcher
	 */
	void updateDispatch(Session* session, CloseCallback onClose, ErrorCallback onError, Dispatcher* dispatcher = NULL)
	{
		BOOST_ASSERT(session != NULL);
		if(!dispatcher) dispatcher = mDispatcher;
		session->getIoService().dispatch(boost::bind(&SessionEngineT::doUpdateDispatch, this, session, onClose, onError, dispatcher));
	}


	/**
	 *
	 * @param session
	 */
	void stopDispatch(Session* session)
	{
		BOOST_ASSERT(session != NULL);
		session->getIoService().dispatch(boost::bind(&SessionEngineT::doStopDispatch, this, session));
	}

	void schedule()
	{
		// NOT YET IMPLEMENTED
		throw std::runtime_error("TcpSessionEngine::schedule() hasn't been implemented");
	}

	template <typename Handler>
	void dispatch(Handler handler)
	{
		mIoService->dispatch(handler);
	}

	template <typename Handler>
	void post(Handler handler)
	{
		mIoService->post(handler);
	}

	void run()
	{
		runImpl(mIoService);
	}

	void stop()
	{
		mIoService->stop();
#if SUPPORT_MULTIPLE_REACTOR
		foreach(i, mReactors)
		{
			(*i)->stop();
		}
#endif
	}

	Dispatcher& getDispatcher()
	{
		return *mDispatcher;
	}

private:
	template <typename Handler>
	void doListenAsync(const Protocol& type, uint16 port, boost::tuple<Handler> handler)
	{
		//printf("damn\n");
		boost::system::error_code error;

		if(mAcceptor)
		{
			mAcceptor->cancel(error);
			mAcceptor->close(error);
			SAFE_DELETE(mAcceptor);
		}

		try
		{
			mAcceptor = new Acceptor(*mIoService);
		}
		catch(boost::system::system_error& e)
		{
			error = e.code();
			boost::get<0>(handler)(error);
			return;
		}

		mAcceptor->open(type, error);
		if(error) { boost::get<0>(handler)(error); return; }

		// re-use the address (in some system, the listening port is not free immediately after program exit, so we have to re-use the port)
		boost::asio::socket_base::reuse_address option(true);
		mAcceptor->set_option(option);

		mAcceptor->bind(Endpoint(type, port), error);
		if(error) { boost::get<0>(handler)(error); return; }

		mAcceptor->listen(SOMAXCONN, error);
		if(error) { boost::get<0>(handler)(error); return; }

		boost::get<0>(handler)(error);
	}

	template <typename Handler>
	void doAcceptAsync(Session* session, boost::tuple<Handler> handler)
	{
		void (SessionEngineT::*f)(Session* session, const boost::system::error_code& ec, boost::tuple<Handler> handler) = &SessionEngineT::handleAcceptAsync<Handler>;

		boost::system::error_code error;
		beforeAccept(session, error);
		if(error)
		{
			boost::get<0>(handler)(error);
			return;
		}

		mAcceptor->async_accept(session->socket(), boost::bind(f, this, session, boost::asio::placeholders::error, handler));
	}

	template <typename Handler>
	void handleAcceptAsync(Session* session, const boost::system::error_code& ec, boost::tuple<Handler> handler)
	{
		boost::system::error_code error;
		if(!ec)
		{
			afterAccept(session, error);
			if(error) session->close();
		}
		else
			error = ec;

		boost::get<0>(handler)(ec);
	}

	template <typename Handler>
	void doConnectAsync(Session* session, const Protocol& type, const std::string& host_name, const std::string& service_name, boost::tuple<Handler> handler)
	{
		void (SessionEngineT::*f)(Session* session, const boost::system::error_code& ec, boost::tuple<Handler> handler) = &SessionEngineT::handleConnectAsync<Handler>;

		typename Protocol::resolver resolver(session->getIoService());
		typename Protocol::resolver::query query(type, host_name, service_name);
		typename Protocol::resolver::iterator it = resolver.resolve(query);

		boost::system::error_code error;
		beforeConnect(session, error);
		if(error)
		{
			boost::get<0>(handler)(error);
			return;
		}

		session->socket().async_connect(*it, boost::bind(f, this, session, boost::asio::placeholders::error, handler));
	}

	template <typename Handler>
	void handleConnectAsync(Session* session, const boost::system::error_code& ec, boost::tuple<Handler> handler)
	{
		boost::system::error_code error;
		if(!ec)
		{
			afterConnect(session, error);
			if(error) session->close();
		}
		else
			error = ec;

		boost::get<0>(handler)(error);
	}

	void doStartDispatch(Session* session, CloseCallback onClose, ErrorCallback onError, Dispatcher* dispatcher)
	{
		TcpSessionDispatcherContext* ctx = session->getContext<TcpSessionDispatcherContext>();
		if(!ctx)
		{
			ctx = new TcpSessionDispatcherContext;
			ctx->buffer.reset(new Buffer(detail::MessageHeader::kMaxDataSize));
			session->setContext<TcpSessionDispatcherContext>(ctx);
		}

		ctx->dispatchEnabled = true;
		ctx->onClose = onClose;
		ctx->onError = onError;
		ctx->dispatcher = dispatcher;

		session->readAsync(
				ctx->buffer,
				boost::bind(&SessionEngineT::handleHeaderRead, this, session, placeholders::error),
				detail::MessageHeader::kHeaderSize);
	}

	void doUpdateDispatch(Session* session, CloseCallback onClose, ErrorCallback onError, Dispatcher* dispatcher)
	{
		TcpSessionDispatcherContext* ctx = session->getContext<TcpSessionDispatcherContext>();
		if(!ctx)
		{
			ctx = new TcpSessionDispatcherContext;
			ctx->buffer.reset(new Buffer(detail::MessageHeader::kMaxDataSize));
			session->setContext<TcpSessionDispatcherContext>(ctx);
		}

		ctx->onClose = onClose;
		ctx->onError = onError;
		ctx->dispatcher = dispatcher;
	}

	void doStopDispatch(Session* session)
	{
		TcpSessionDispatcherContext* ctx = session->getContext<TcpSessionDispatcherContext>();
		BOOST_ASSERT(ctx != NULL);
		ctx->dispatchEnabled = false;
		ctx->onClose.clear();
		ctx->onError.clear();
		ctx->dispatcher = NULL;
	}

	void handleHeaderRead(Session* session, const boost::system::error_code& ec)
	{
		TcpSessionDispatcherContext* ctx = session->getContext<TcpSessionDispatcherContext>();
		BOOST_ASSERT(ctx != NULL);
		if(!ec)
		{
			try
			{
				detail::MessageHeader header;
				*ctx->buffer >> header;

				if(UNLIKELY(header.size > detail::MessageHeader::kMaxDataSize))
				{
					throw boost::system::system_error(boost::asio::error::broken_pipe);
				}
				else if(header.size == 0)
				{
					handleBodyRead(session, header.type, header.size, ec);
				}
				else
				{
					ctx->buffer->clear();

					session->readAsync(
							ctx->buffer,
							boost::bind(&SessionEngineT::handleBodyRead, this, session, header.type, header.size, placeholders::error),
							header.size);
				}
			}
			catch(boost::system::system_error& e)
			{
				if(!ctx->onError.empty())
				{
					ctx->onError(e.code());
				}

				if(!ctx->onClose.empty()) ctx->onClose();
				if(session->socket().is_open())
				{
					boost::system::error_code error;
					session->socket().close(error);
				}
				session->markForDeletion();
			}
		}
		else if(ec == boost::asio::error::eof)
		{
			//LOG4CXX_INFO(mLogger, "client closes connection");
			if(!ctx->onClose.empty()) ctx->onClose();
			if(session->socket().is_open())
			{
				boost::system::error_code error;
				session->socket().close(error);
			}
			session->markForDeletion();
		}
		else
		{
			//LOG4CXX_INFO(mLogger, "client handle message read header error, error = " << ec.message());
			if(!ctx->onError.empty())
			{
				ctx->onError(ec);
			}

			if(!ctx->onClose.empty()) ctx->onClose();
			if(session->socket().is_open())
			{
				boost::system::error_code error;
				session->socket().close(error);
			}
			session->markForDeletion();
		}
	}

	void handleBodyRead(Session* session, uint32 type, std::size_t bytes_requested, const boost::system::error_code& ec)
	{
		TcpSessionDispatcherContext* ctx = session->getContext<TcpSessionDispatcherContext>();
		BOOST_ASSERT(ctx != NULL);
		if(!ec)
		{
			try
			{
				//mDispatcher->dispatch(type, *session, ctx->buffer, bytes_requested);
				ctx->dispatcher->dispatch(type, *session, ctx->buffer, bytes_requested);
				ctx->buffer->clear();

				if(ctx->dispatchEnabled)
				{
					session->readAsync(
							ctx->buffer,
							boost::bind(&SessionEngineT::handleHeaderRead, this, session, placeholders::error),
							detail::MessageHeader::kHeaderSize);
				}
			}
			catch(boost::system::system_error& e)
			{
				if(!ctx->onError.empty())
				{
					ctx->onError(e.code());
				}

				if(!ctx->onClose.empty()) ctx->onClose();
				if(session->socket().is_open())
				{
					boost::system::error_code error;
					session->socket().close(error);
				}
				session->markForDeletion();
			}
		}
		else if(ec == boost::asio::error::eof)
		{
			//LOG4CXX_INFO(mLogger, "client closes connection");
			if(!ctx->onClose.empty()) ctx->onClose();
			if(session->socket().is_open())
			{
				boost::system::error_code error;
				session->socket().close(error);
			}
			session->markForDeletion();
		}
		else
		{
			//LOG4CXX_INFO(mLogger, "client handle message read body error, error = " << ec.message());
			if(!ctx->onError.empty())
			{
				ctx->onError(ec);
			}

			if(!ctx->onClose.empty()) ctx->onClose();
			if(session->socket().is_open())
			{
				boost::system::error_code error;
				session->socket().close(error);
			}
			session->markForDeletion();
		}
	}

protected:
	virtual void beforeConnect(SessionBase* session, boost::system::error_code& error)
	{
		UNUSED_ARGUMENT(session);
		UNUSED_ARGUMENT(error);
	}

	virtual void afterConnect(SessionBase* session, boost::system::error_code& error)
	{
		UNUSED_ARGUMENT(session);
		UNUSED_ARGUMENT(error);
	}

	virtual void beforeAccept(SessionBase* session, boost::system::error_code& error)
	{
		UNUSED_ARGUMENT(session);
		UNUSED_ARGUMENT(error);
	}

	virtual void afterAccept(SessionBase* session, boost::system::error_code& error)
	{
		UNUSED_ARGUMENT(session);
		UNUSED_ARGUMENT(error);
	}

protected:
	Acceptor* mAcceptor;
	Dispatcher* mDispatcher;
	Resolver* mResolver;

	bool mIsIoServiceOwner;
	IoService* mIoService;

#if SUPPORT_MULTIPLE_REACTOR
	struct Reactor
	{
		Reactor(uint32 _threads, IoService* _ios)
		{
			if(_ios)
			{
				ios = _ios;
				owner = false;
			}
			else
			{
				ios = new IoService;
				owner = true;
			}
			if(_threads > 0)
			{
				for(uint32 i=0;i<_threads;++i)
				{
					boost::thread* t = new boost::thread(boost::bind(SessionEngineT::runImpl, ios));
					threads.push_back(t);
				}
			}
		}

		~Reactor()
		{
			ios->stop();
			foreach(i, threads)
			{
				if((*i)->joinable())
					(*i)->join();
				SAFE_DELETE(*i);
			}
			threads.clear();

			if(owner)
			{
				SAFE_DELETE(ios);
			}
		}

		void stop()
		{
			ios->stop();
		}

		IoService* ios;
		bool owner;
		std::vector<boost::thread*> threads;
	};
	std::vector<shared_ptr<Reactor>> mReactors;
	uint64 mCurrentReactorIndex;
#endif

private:
	static void runImpl(IoService* ios)
	{
		IoService::work dummy(*ios);
		while(true)
		{
			try
			{
				ios->run();
				break;
			}
			catch(std::exception& ex)
			{
				printf("TcpSessionEngine::runImpl() catches uncaught exception: %s", ex.what());
			}
		}
	}
};

/// Pre-define TcpSessionEngine
typedef SessionEngineT< SessionTransport::tcp > TcpSessionEngine;

} } }

#endif/*ZILLIANS_NET_SYS_TCPSESSIONENGINEIMPL_H_*/
