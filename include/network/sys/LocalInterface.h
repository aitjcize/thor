/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @date Jun 6, 2010 sdk - Initial version created.
 */

#ifndef ZILLIANS_NETWORKING_SYS_LOCALINTERFACE_H_
#define ZILLIANS_NETWORKING_SYS_LOCALINTERFACE_H_

#include "core/Prerequisite.h"

namespace zillians { namespace network { namespace sys {

class LocalInterface
{
public:
	struct flags
	{
#ifdef __PLATFORM_MAC__
		enum type
		{
			up,
			broadcast,
			debug,
			loopback,
			point_to_point,
			notrailers,
			running,
			noarp,
			promisc,
			allmulti,
			oactive,
			simplex,
			link1,
			link2,
			altphys,
			multicast
		};

#else
		enum type
		{
			up,
			broadcast,
			debug,
			loopback,
			point_to_point,
			notrailers,
			running,
			noarp,
			promisc,
			allmulti,
			slave,
			master,
			multicast
		};
#endif
	};

public:
	LocalInterface();
	virtual ~LocalInterface();

public:
	bool enumerate();
	bool next();
	int count() const;

public:
	std::string name();
	std::string hw_address();
	std::string ip_address();
	std::string broadcast_address();
	std::string netmask();
	short       flags();
	int         metric();
	int         mtu();

public:
	static bool has_flag(short flag, flags::type f);
	static char* flags_to_string(short flag);

private:
	void cleanup();

private:
	struct InterfaceInfo
	{
		std::string name;
		std::string hw_address;
		std::string ip_address;
		std::string broadcast_address;
		std::string netmask;
		short flags;
		int metric;
		int mtu;
		unsigned int type;
	};

	int mCurrentIndex;
	InterfaceInfo* mCurrentInterface;
	std::vector<InterfaceInfo*> mInterfaces;
};

} } }

#endif /* ZILLIANS_NETWORKING_SYS_LOCALINTERFACE_H_ */
