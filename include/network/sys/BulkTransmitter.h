/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @date Nov 9, 2010 sdk - Initial version created.
 */

#ifndef ZILLIANS_NETWORKING_SYS_BULKTRANSMITTER_H_
#define ZILLIANS_NETWORKING_SYS_BULKTRANSMITTER_H_

#include "core/SharedPtr.h"
#include "network/sys/Placeholders.h"

namespace zillians { namespace network { namespace sys {

template<typename SessionEngine, typename Session, uint32 HeaderMessageType = 0, uint32 SegmentMessageType = 1, uint32 RawBufferType = 2>
class BulkTransmitter
{
	struct Context
	{
		Context() : type(-1), length(0), buffer(new Buffer)
		{ }

		uint32 type;
		std::size_t length;
		shared_ptr<Buffer> buffer;
	};
public:
	struct MessageHeader
	{
		enum { TYPE = HeaderMessageType };

		uint32 type;
		std::size_t length;

		template<typename Archive>
		void serialize(Archive& ar, const unsigned int version)
		{
			ar & type;
			ar & length;
		}
	};

	struct MessageSegment
	{
		enum { TYPE = SegmentMessageType };

		Buffer buffer;
		bool completed;

		template<typename Archive>
		void serialize(Archive& ar, const unsigned int version)
		{
			ar & buffer;
			ar & completed;
		}
	};

public:
	BulkTransmitter(SessionEngine& engine) : mEngine(engine)
	{
		engine.getDispatcher().template bind<MessageHeader>(
				boost::bind(
						&BulkTransmitter::handleMessageHeader,
						placeholders::dispatch::source_ref,
						placeholders::dispatch::message_ref));

		engine.getDispatcher().template bind<MessageSegment>(
				boost::bind(
						&BulkTransmitter::handleMessageSegment,
						placeholders::dispatch::source_ref,
						placeholders::dispatch::message_ref));
	}

	virtual ~BulkTransmitter()
	{

	}

public:
	void write(Session& session, uint32 type, shared_ptr<Buffer>& buffer, std::size_t size = 0)
	{
		std::size_t size_to_send = (size == 0) ? buffer->dataSize() : size;

		// send out message header
		MessageHeader hdr;
		hdr.type = type;
		hdr.length = size_to_send;
		session.write(hdr);

		// send out message segments
		std::size_t max_segment_size = detail::MessageHeader::kMaxDataSize - 16;
		std::size_t offset = 0;
		MessageSegment segment;
		while(offset < size_to_send)
		{
			if(offset + max_segment_size < size_to_send)
			{
				segment.buffer.writeArray(buffer->rptr() + offset, max_segment_size);
				segment.completed = false;
				offset += max_segment_size;
			}
			else
			{
				segment.buffer.writeArray(buffer->rptr() + offset, size_to_send - offset);
				segment.completed = true;
				offset = size_to_send;
			}
			session.write(segment);
			segment.buffer.clear();
		}
	}

	template<typename M>
	void write(Session& session, M& message)
	{
		shared_ptr<Buffer> buffer(new Buffer);
		(*buffer) << message;

		write(session, M::TYPE, buffer);
	}

	void write(Session& session, shared_ptr<Buffer> buffer)
	{
		write(session, RawBufferType, buffer);
	}

	void read(Session& session, shared_ptr<Buffer> buffer)
	{
		// Strip out header
		MessageHeader hdr;
		session.read(hdr);

		buffer->resize(hdr.length);
		while (true)
		{
			MessageSegment segment;
			session.read(segment);

			buffer->append(segment.buffer);
			if (segment.completed) break;
		}
	}

	template<typename M>
	void read(Session& session, M& message)
	{
		shared_ptr<Buffer> buffer = shared_ptr<Buffer>(new Buffer());

		read(session, buffer);
		buffer >> message;
	}

	/// Asynchronously write a buffer to the session
	template<typename Handler>
	void writeAsync(Session& session, uint32 type, shared_ptr<Buffer>& buffer, Handler handler, std::size_t size = 0)
	{
		std::size_t size_to_send = (size == 0) ? buffer->dataSize() : size;
		// send out message header
		MessageHeader hdr;
		hdr.type = type;
		hdr.length = size_to_send;
		session.writeAsync(hdr, boost::bind(BulkTransmitter::writeAsyncCompleted, placeholders::error));

		// send out message segments
		std::size_t max_segment_size = detail::MessageHeader::kMaxDataSize - 16;
		std::size_t offset = 0;
		MessageSegment segment;
		while(offset < size_to_send)
		{
			if(offset + max_segment_size < size_to_send)
			{
				segment.buffer.writeArray(buffer->rptr() + offset, max_segment_size);
				segment.completed = false;
				offset += max_segment_size;
				session.writeAsync(segment, boost::bind(BulkTransmitter::writeAsyncCompleted, placeholders::error));
			}
			else
			{
				segment.buffer.writeArray(buffer->rptr() + offset, size_to_send - offset);
				segment.completed = true;
				offset = size_to_send;
				session.writeAsync(segment, handler);
			}
			segment.buffer.clear();
		}
	}

	template<typename M, typename Handler>
	void writeAsync(Session& session, M& message, Handler handler)
	{
		shared_ptr<Buffer> buffer(new Buffer);
		(*buffer) << message;

		writeAsync(session, M::TYPE, buffer, handler);
	}

	template<typename Handler>
	void writeAsync(Session& session, shared_ptr<Buffer> buffer, Handler handler)
	{
		writeAsync(session, RawBufferType, buffer, handler);
	}

	static void writeAsyncCompleted(const boost::system::error_code& ec)
	{ }

	void close(Session& session)
	{
		session.shutdown();
		session.close();
	}

private:
	static void handleMessageHeader(Session& session, MessageHeader& message)
	{
		Context* context = session.template getContext<Context>();

		// make sure the context is available; if not, just create one
		if(!context)
		{
			context = new Context;
			session.template setContext<Context>(context);
		}

		// store the message type and header
		context->type = message.type;
		context->length = message.length;
	}

	static void handleMessageSegment(Session& session, MessageSegment& message)
	{
		Context* context = session.template getContext<Context>();

		// make sure the context is available; if not, just create one
		if(!context)
		{
			context = new Context;
			session.template setContext<Context>(context);
		}

		// append the segment to the context message buffer
		context->buffer->writeArray(message.buffer.rptr(), message.buffer.dataSize());

		// if all segments are sent, send to dispatcher
		if(message.completed)
		{
			session.getDispatcher().dispatch(context->type, session, context->buffer, context->length);

			// if the buffer is still referenced by handlers, we should give another new buffer object
			if(context->buffer.use_count() > 1)
			{
				context->buffer.reset(new Buffer);
			}
			else
			{
				context->buffer->clear();
			}
		}


	}

	inline Buffer* getTlsWriteBuffer()
	{
		Buffer* buffer = mTlsWriteBuffer.get();
		if(!buffer)
		{
			buffer = new Buffer();
			mTlsWriteBuffer.reset(buffer);
		}
		return buffer;
	}

private:
	SessionEngine& mEngine;
	boost::thread_specific_ptr<Buffer> mTlsWriteBuffer;
};

} } }

#endif /* BULKTRANSMITTER_H_ */
