/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_RUNTIME_CLIENTREACTOR_H_
#define ZILLIANS_RUNTIME_CLIENTREACTOR_H_

#include "runtime/Reactor.h"
#include "framework/ExecutionEngine.h"
#include "framework/processor/ProcessorExternal.h"
#include "utility/Filesystem.h"

namespace zillians { namespace runtime {

// TODO refactor note
// client reactor will have a single execution engine

class BasicReactor : public Reactor
{
public:
    explicit BasicReactor(DomainType domain_type, VMMode vm_mode, int dev_id, const std::vector<std::string> &args, bool verbose);
    virtual ~BasicReactor();

public:
    virtual void initialize();
    virtual void finalize();
    virtual void start();
    virtual void stop();
    virtual void kill();
    virtual int32 waitForCompletion();

public:
    void configureHardwareArchitecture(const std::string& arch);
    bool loadKernel(const boost::filesystem::path& main_ast_path,
                    const boost::filesystem::path& main_runtime_path,
                    const std::vector<boost::filesystem::path>& dep_paths);

    bool runInit();
    bool runEntry(const std::string& entry_name);
    bool runSystemInit();

private:
    bool runFunction(int64 function_id, const bool block = true);

private:
    framework::ExecutionEngine mExecutionEngine;
    boost::thread* mExecutionEngineThread;
    framework::processor::ProcessorExternal* mProcessorExternal;

private:
    struct
    {
        DomainType domain_type;
        VMMode vm_mode;
        int dev_id;
        std::vector<std::string> args;
        bool verbose;
    } config;

private:
    log4cxx::LoggerPtr mLogger;
};

} }

#endif /* ZILLIANS_RUNTIME_CLIENTREACTOR_H_ */
