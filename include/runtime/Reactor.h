/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_RUNTIME_REACTOR_H_
#define ZILLIANS_RUNTIME_REACTOR_H_

#include "core/Prerequisite.h"

namespace zillians { namespace runtime {

struct ProxyServerConfiguration
{
    int listen_port;
};

struct LoginServerConfiguration
{
    int listen_port;
};

enum class DomainType {
	SingleThreaded,
	MultiThreaded,
	Kepler,
	Tahiti,
	OpenCL,
	CUDA,
	GPU,
	K10M
};

enum class VMMode {
    GenericMode,
	ClientMode,
	ServerMode
};

class Reactor
{
public:
    Reactor() { }
    virtual ~Reactor() { }

public:
    virtual void start() = 0;
    virtual void stop() = 0;
    virtual void kill() = 0;
    virtual int32 waitForCompletion() = 0;
};

} }

#endif /* ZILLIANS_RUNTIME_REACTOR_H_ */
