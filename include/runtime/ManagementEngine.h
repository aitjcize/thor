/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

namespace zillians { namespace runtime {

// ManagementEngine provides an interface to launch or query domains and also let each domain register management callbacks
// ManagementEngine facilitates the use of ClusterEngine and RPC controller (in its implementation)
//
// ManagementEngine talks to management daemon, which is another process on different machines, also running membership provider...
// the management callbacks will check if the caller is management daemon

struct ManagementVerbs
{
	enum type {
		// common
		BasicResponse,

		// domain-related verbs
		OpenDomainRequest,
		CloseDomainRequst,
		KillDomain,
		QueryDomain,

		// interconnect-related verbs

		// cluster-related verbs
	};

	union {
		union {

		} response;

		union {

		} request;
	};

	template<typename Archive>
	void serialize(Archive& ar, const unsigned int version)
	{

	}
};

class ManagementEngine
{
public:
	void makeRequest(const ManagementVerbs& request, ManagementVerbs& response);
	void makeRequestAsync(const ManagementVerbs& request, const std::function<void(const UUID& source, const ManagementVerbs& verbs)>& response_callback);

public:
	void handleRequest(ManagementVerbs::type verbs, const std::function<void(const UUID& source, const ManagementVerbs& verbs)>& callback);
	void handleRequestAny(const std::function<void(const UUID& source, const ManagementVerbs& verbs)>& callback);
};

} }
