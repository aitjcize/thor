/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_CONCURRENTQUEUE_H_
#define ZILLIANS_CONCURRENTQUEUE_H_

#include "core/Prerequisite.h"

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>

namespace zillians {

/**
 * @brief ConcurrentQueue is a fast concurrent queue supporting multiple producers multiple consumers
 *
 * ConcurrentQueue is built entirely on boost thread library, so it's basically 100% cross-platform compatible
 */
template<typename T>
class ConcurrentQueue : public boost::noncopyable
{
    struct prevent_spurious_wakeup_predicate
    {
        prevent_spurious_wakeup_predicate(std::queue<T>& q) : _q(q) { }
        bool operator () () const { return !_q.empty(); }
        typename std::queue<T>& _q;
    };

public:
    ConcurrentQueue()
    { }

    ~ConcurrentQueue()
    { }

public:
    std::size_t unsafe_size()
    {
        return mQueue.size();
    }

    void push(T const& data)
    {
        boost::mutex::scoped_lock lock(mMutex);
        mQueue.push(data);
        lock.unlock();
        mConditionVariable.notify_one();
    }

    void clear()
    {
        boost::mutex::scoped_lock lock(mMutex);
        while(!mQueue.empty())
            mQueue.pop();
    }

    bool empty() const
    {
        boost::mutex::scoped_lock lock(mMutex);
        return mQueue.empty();
    }

    bool try_peek(T& value)
    {
        boost::mutex::scoped_lock lock(mMutex);
        if(mQueue.empty())
        {
            return false;
        }

        value = mQueue.front();
        return true;
    }

    bool try_pop(T& value)
    {
        boost::mutex::scoped_lock lock(mMutex);
        if(mQueue.empty())
        {
            return false;
        }

        value = mQueue.front();
        mQueue.pop();
        return true;
    }

    void wait_and_pop(T& value)
    {
        boost::mutex::scoped_lock lock(mMutex);
        prevent_spurious_wakeup_predicate p(mQueue);
        mConditionVariable.wait(lock, p);

        value = mQueue.front();
        mQueue.pop();
    }

    bool timed_wait_and_pop(T& value, const boost::system_time& absolute)
    {
        boost::mutex::scoped_lock lock(mMutex);
        prevent_spurious_wakeup_predicate p(mQueue);

        try {
            if(!mConditionVariable.timed_wait(lock, absolute, p))
                return false;

            value = mQueue.front();
            mQueue.pop();
        }
        catch(boost::thread_interrupted&) {
            return false;
        }

        return true;
    }

    template<typename DurationType>
    bool timed_wait_and_pop(T& value, const DurationType& relative)
    {
        boost::mutex::scoped_lock lock(mMutex);
        prevent_spurious_wakeup_predicate p(mQueue);

        try {
            if(!mConditionVariable.timed_wait(lock, relative, p))
                return false;

            value = mQueue.front();
            mQueue.pop();
        }
        catch(boost::thread_interrupted&) {
            return false;
        }

        return true;
    }

private:
    std::queue<T> mQueue;
    mutable boost::mutex mMutex;
    boost::condition_variable mConditionVariable;
};

}

#endif /* ZILLIANS_CONCURRENTQUEUE_H_ */
