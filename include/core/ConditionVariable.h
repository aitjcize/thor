/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_CONDITIONVARIABLE_H_
#define ZILLIANS_CONDITIONVARIABLE_H_

#define ZILLIANS_CONDITIONVARIABLE_CHOOSE_IMPL  0

#if (ZILLIANS_CONDITIONVARIABLE_CHOOSE_IMPL == 0)

#include <tbb/concurrent_queue.h>

namespace zillians {

/**
 * ConditionVariable is a simple condition variable implementation
 * using TBB's concurrent_bounded_queue.
 *
 * By using TBB's concurrent_bounded_queue, the ConditionVariable
 * has a more reliable signal/wait schematics than pthread's
 * condition variable. No signal will be lost, all wait() calls
 * will succeed with corresponding signal() calls (unless we reset
 * before calling wait() )
 *
 * ConditionVariable has a template parameter used to specify the
 * primitive to be used when wait or signal the condition. This
 * can be served as a simple inter-thread communication pipe.
 *
 * @note The template parameter cannot be "bool" type which will
 * lead to memory corruption due to a bug in
 * tbb::concurrent_bounded_queue implementation.
 */
template <typename T>
class ConditionVariable
{
public:
    ConditionVariable()
    { }

    void reset()
    {
        mQueue.clear();
    }

    void wait(T& result)
    {
        mQueue.pop(result);
    }

    void signal(const T& result)
    {
        mQueue.push(result);
    }

private:
    tbb::concurrent_bounded_queue<T> mQueue;

};

}

#elif (ZILLIANS_CONDITIONVARIABLE_CHOOSE_IMPL == 1)

#include "core/Prerequisite.h"

namespace zillians {

template <typename T>
class ConditionVariable : public boost::noncopyable
{
    struct prevent_spurious_wakeup_predicate
    {
        prevent_spurious_wakeup_predicate(bool& result) : _result(result) { }
        bool operator () () const { return !_result; }
        bool& _result;
    };

public:
    ConditionVariable() : mSignaled(false)
    {  }

    void reset()
    {
        boost::mutex::scoped_lock lock(mMutex);
        mSignaled = false;
        lock.unlock();
        mConditionVariable.notify_one();
    }

    void signal(const T& result)
    {
        boost::mutex::scoped_lock lock(mMutex);
        mValue = result;
        mSignaled = true;
        lock.unlock();
        mConditionVariable.notify_one();
    }

    bool try_wait(T& result)
    {
        boost::mutex::scoped_lock lock(mMutex);
        if(!mSignaled)
            return false;

        result = mValue;
        return true;
    }

    void wait(T& result)
    {
        boost::mutex::scoped_lock lock(mMutex);
        prevent_spurious_wakeup_predicate p(mSignaled);
        mConditionVariable.wait(lock, p);

        result = mValue;
    }

    bool timed_wait(T& result, const boost::system_time& absolute)
    {
        boost::mutex::scoped_lock lock(mMutex);
        prevent_spurious_wakeup_predicate p(mSignaled);
        bool f = mConditionVariable.timed_wait(lock, absolute, p);
        result = mValue;
        return f;
    }

    template<typename DurationType>
    bool timed_wait(T& result, const DurationType& relative)
    {
        boost::mutex::scoped_lock lock(mMutex);
        prevent_spurious_wakeup_predicate p(mSignaled);
        bool f = mConditionVariable.timed_wait(lock, relative, p);
        result = mValue;
        return f;
    }

private:
    bool mSignaled;
    T mValue;
    mutable boost::mutex mMutex;
    boost::condition_variable mConditionVariable;
};

}

#endif

#endif/*ZILLIANS_CONDITIONVARIABLE_H_*/
