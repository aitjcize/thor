/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_SEMAPHORE_H_
#define ZILLIANS_SEMAPHORE_H_

#include "core/Common.h"
#include <semaphore.h>
#include "utility/UUIDUtil.h"
#include "core/Platform.h"
namespace zillians { 

class Semaphore
{
public:
    Semaphore()
    {
#ifdef __PLATFORM_MAC__
	UUID uid = UUID::random();
	std::string tmp = ((std::string)(uid)).substr(20,37);
	mSemaphore = sem_open(tmp.c_str(), O_EXCL | O_CREAT, 420, 0);
	BOOST_ASSERT(mSemaphore != SEM_FAILED);
#else
	int result = sem_init(&mSemaphore, 0 /* in-process semaphore */, 0 /* initial value */);
	BOOST_ASSERT(result != -1);
#endif
    }

    ~Semaphore()
    {
#ifdef __PLATFORM_MAC__
	sem_close(mSemaphore);
#else
        int result = sem_destroy(&mSemaphore);
        BOOST_ASSERT(result != -1);
#endif
    }

public:
    inline void wait()
    {
#ifdef __PLATFORM_MAC__
        int result = sem_wait(mSemaphore);
#else
        int result = sem_wait(&mSemaphore);
#endif
        BOOST_ASSERT(result != -1);
    }

    inline void post()
    {
#ifdef __PLATFORM_MAC__
        int result = sem_post(mSemaphore) ;
#else
        int result = sem_post(&mSemaphore);
#endif
        BOOST_ASSERT(result != -1);
    }

private:
#ifdef __PLATFORM_MAC__
    sem_t* mSemaphore;
#else
    sem_t mSemaphore;
    // forbid object copy constructor and copy operator
#endif

private:
    Semaphore(const Semaphore&);
    void operator = (const Semaphore&);
};

}

#endif /* ZILLIANS_SEMAPHORE_H_ */
