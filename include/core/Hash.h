/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_HASHMAP_H_
#define ZILLIANS_HASHMAP_H_

#if defined(__PLATFORM_LINUX__) || defined(__PLATFORM_MAC__)
#include <ext/hash_map>
#include <ext/hash_set>
#elif defined(__PLATFORM_WINDOWS__)
#include <hash_map>
#include <hash_set>
#else
#include <tr1/unordered_map>
#include <tr1/unordered_set>
#endif

namespace zillians {

#if defined(__PLATFORM_LINUX__) || defined(__PLATFORM_MAC__)
using __gnu_cxx::hash_map;
using __gnu_cxx::hash_set;
#elif defined(__PLATFORM_WINDOWS__)
using::stdext::hash_map;
using ::stdext::hash_set;
#else
// the following code choose the best hash map/set implementation on different platform
// since the template alias is not present before GCC 4.7, this is a really dirty workaround to define types
#define hash_map std::tr1::unordered_map
#define hash_set std::tr1::unordered_set
#endif

template <typename T>
struct PointerHashCompare
{
    static size_t hash( const T& x )
    {
        return reinterpret_cast<size_t>(x);
    }
    static bool equal( const T& x, const T& y )
    {
        return x==y;
    }
};

template <typename T>
struct SharedPointerHashCompare
{
    static size_t hash( const T& x )
    {
        return reinterpret_cast<size_t>(x.get());
    }
    static bool equal( const T& x, const T& y )
    {
        return x==y;
    }
};

}

#endif/*ZILLIANS_HASHMAP_H_*/
