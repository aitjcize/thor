/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_ATOMIC_ATOMICSTACK_H_
#define ZILLIANS_ATOMIC_ATOMICSTACK_H_

#include "core/Atomic.h"

namespace zillians { namespace atomic {

#if 0
struct stack_node
{
    stack_node() : _nexts(0)
    { }

    stack_node* volatile _nexts;
};

/**
 * Simple Atomic Stack
 */
template<class T>
class stack
{
private:
    struct ptr_t
    {
        ptr_t() : _ptr(0), _pops(0)
        { }

        ptr_t(stack_node* const ptr, const uint32 pops) : _ptr(ptr), _pops(pops)
        { }

        union
        {
            struct
            {
                stack_node* volatile _ptr;
                volatile uint32 _pops;
            };
            volatile int64 _data;
        };
    };

    ptr_t _head;

public:
    stack() : _head()
    { }

    void push(T * item)
    {
        stack_node* node = item;

        while(true)
        {
            node->_nexts = _head._ptr;
            if(b_cas_ptr(reinterpret_cast<void* volatile*> (&_head._ptr), node, node->_nexts))
                break;
        }
    }

    T* pop()
    {
        while(true)
        {
            const ptr_t head = _head;

            if(head._ptr == 0)
                return 0;

            const ptr_t next(head._ptr->_nexts, head._pops + 1);

            if(b_cas(&_head._data, next._data, head._data))
            {
                return static_cast<T*> (head._ptr);
            }
        }
    }
};

#endif

} }

#endif /* ZILLIANS_ATOMIC_ATOMICSTACK_H_ */
