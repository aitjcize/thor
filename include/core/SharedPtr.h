/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_SHAREDPTR_H_
#define ZILLIANS_SHAREDPTR_H_

#include "core/Common.h"
//#include <hash_set>

//#if defined _WIN32
//    #include <boost/shared_ptr.hpp>
//    #include <boost/weak_ptr.hpp>
//    #include <boost/enable_shared_from_this.hpp>
//    #include <boost/make_shared.hpp>
//    using boost::static_pointer_cast;
//    using boost::const_pointer_cast;
//    using boost::dynamic_pointer_cast;
//    using boost::shared_ptr;
//    using boost::weak_ptr;
//    using boost::enable_shared_from_this;
//    using boost::make_shared;
//#else
//    #ifndef __GXX_EXPERIMENTAL_CXX0X__
//        #include <boost/shared_ptr.hpp>
//        #include <boost/weak_ptr.hpp>
//        #include <boost/enable_shared_from_this.hpp>
//        #include <boost/make_shared.hpp>
//        using boost::static_pointer_cast;
//        using boost::const_pointer_cast;
//        using boost::dynamic_pointer_cast;
//        using boost::shared_ptr;
//        using boost::weak_ptr;
//        using boost::enable_shared_from_this;
//        using boost::make_shared;
//    #else
        #include <memory>
        using std::static_pointer_cast;
        using std::const_pointer_cast;
        using std::dynamic_pointer_cast;
        using std::shared_ptr;
        using std::weak_ptr;
        using std::enable_shared_from_this;
        using std::make_shared;
//    #endif
//#endif

namespace zillians {

struct null_deleter
{
    void operator()(void const *) const
    { }
};

template<typename T>
struct reference_holder
{
    reference_holder(const shared_ptr<T>& obj) : ref(obj) { }
    void operator()(void const*) const
    { }
    const shared_ptr<T> ref;
};

template<typename A, typename B>
shared_ptr<B> reinterpret_pointer_cast(const shared_ptr<A>& obj)
{
    return shared_ptr<B>((B*)obj.get(), reference_holder<A>(obj));
}

}

/**
 * @brief Allow direct comparison of shared_ptr and its wrapping type (for "equal to" operator)
 */
template<class T>
inline bool operator == (const shared_ptr<T> &a, const T* b)
{
    return (a.get() == b);
}

/**
 * @brief Allow direct comparison of shared_ptr and its wrapping type (for "not equal to" operator)
 */
template<class T>
inline bool operator != (const shared_ptr<T> &a, const T* b)
{
    return (a.get() != b);
}


//#if defined __PLATFORM_LINUX__ || defined __PLATFORM_MAC__
//namespace __gnu_cxx
//{
//    template<typename T>
//    struct hash<shared_ptr<T> >
//    {
//        size_t operator()(const shared_ptr<T>& __x) const
//        {
//            return reinterpret_cast<size_t>(__x.get());
//        }
//    };
//}
//#else
//namespace stdext
//{
//    template<typename T>
//    struct hash_compare<shared_ptr<T> >
//    {
//        size_t operator()(const shared_ptr<T>& __x) const
//        {
//            return reinterpret_cast<size_t>(__x.get());
//        }

//        bool operator()(const shared_ptr<T>& v1, const shared_ptr<T>& v2) const
//        {
//            return (v1 == v2) ? true : false;
//        }
//    };
//}
//#endif

#endif/*ZILLIANS_SHAREDPTR_H_*/
