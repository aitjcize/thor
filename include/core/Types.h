/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_TYPES_H_
#define ZILLIANS_TYPES_H_

#include "core/Platform.h"
#include "core/IntTypes.h"

#include <cstddef>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <wchar.h>

#define UNUSED_ARGUMENT(x) \
    (void)x

#define UNREACHABLE_CODE() \
    BOOST_ASSERT(false && "reaching unreachable code")

#define UNIMPLEMENTED_CODE() \
    BOOST_ASSERT(false && "not yet implemented")

#define NOT_NULL(expr) \
    BOOST_ASSERT((expr) != nullptr && "null pointer exception")

#endif/*ZILLIANS_TYPES_H_*/
