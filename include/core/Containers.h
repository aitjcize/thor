/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_CONTAINERS_H_
#define ZILLIANS_CONTAINERS_H_

#include "core/Common.h"

#if defined _WIN32
    #include <unordered_set>
    #include <unordered_map>
    using std::unordered_set;
    using std::unordered_map;
#else
    #ifndef __GXX_EXPERIMENTAL_CXX0X__
        #include <boost/unordered_set.hpp>
        #include <boost/unordered_map.hpp>
        using boost::unordered_set;
        using boost::unordered_map;
    #else
        #include <unordered_set>
        #include <unordered_map>
        using std::unordered_set;
        using std::unordered_map;
    #endif
#endif

#endif /* ZILLIANS_CONTAINERS_H_ */
