/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_INVERTEDARRAY_H_
#define ZILLIANS_INVERTEDARRAY_H_

#include <vector>
#include <algorithm>

namespace zillians {

class InvertedArrayItem
{
public:
    InvertedArrayItem() : mInvertedArrayIndex(-1) { }
    ~InvertedArrayItem() { }

public:
    inline void setIndex(int index)
    {
        mInvertedArrayIndex = index;
    }

    inline int getIndex()
    {
        return mInvertedArrayIndex;
    }

private:
    int mInvertedArrayIndex;

    // forbid object copy constructor and copy operator
private:
    InvertedArrayItem(const InvertedArrayItem&);
    void operator = (const InvertedArrayItem&);
};

template<typename T>
class InvertedArray
{
public:
    InvertedArray()
    { }

    ~InvertedArray()
    { }

    inline bool empty()
    {
        return mArrayItems.empty();
    }

    inline std::size_t size()
    {
        return mArrayItems.size();
    }

    inline T*& operator [] (std::size_t index)
    {
        return mArrayItems[index];
    }

    inline void pushBack(T* item)
    {
        if(item)
            item->setIndex(mArrayItems.size());

        mArrayItems.push_back(item);
    }

    inline void erase(T* item)
    {
        erase(T->getIndex());
    }

    inline void erase(std::size_t index)
    {
        if(mArrayItems.back())
            mArrayItems.back()->setIndex(index);

        mArrayItems[index] = mArrayItems.back();
        mArrayItems.pop_back();
    }

    inline void swap(std::size_t index1, std::size_t index2)
    {
        if(mArrayItems[index1])
            mArrayItems[index1]->setIndex(index2);
        if(mArrayItems[index2])
            mArrayItems[index2]->setIndex(index1);
        std::swap(mArrayItems[index1], mArrayItems[index2]);
    }

    inline void clear()
    {
        mArrayItems.clear();
    }

    inline std::size_t index(T* item)
    {
        return item->getIndex();
    }

private:
    std::vector<T*> mArrayItems;

    // forbid object copy constructor and copy operator
private:
    InvertedArray(const InvertedArray&);
    void operator = (const InvertedArray&);

};

} } }

#endif /* ZILLIANS_INVERTEDARRAY_H_ */
