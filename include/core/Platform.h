/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_PLATFORM_H_
#define ZILLIANS_PLATFORM_H_

//////////////////////////////////////////////////////////////////////////
#define _REENTRANT 1

#if defined _WIN32_
#define __PLATFORM_WINDOWS__
#include <winsock2.h>// This is to prevent inclusion of winsock.h in some third party libraries, which would cause duplicated definitions
#elif defined __APPLE__
#define __PLATFORM_MAC__
#else
#define __PLATFORM_LINUX__
#endif






#ifdef __PLATFORM_WINDOWS__
#define NOMINMAX
#endif

#endif /*ZILLIANS_PLATFORM_H_*/
