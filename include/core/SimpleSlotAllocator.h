/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_SIMPLESLOTALLOCATOR_H_
#define ZILLIANS_SIMPLESLOTALLOCATOR_H_

#include "core/Prerequisite.h"
#include <tbb/concurrent_queue.h>

namespace zillians {

template<typename Key, typename Value, std::size_t TotalSize, bool Concurrent = false>
struct SimpleSlotAllocator;

template<typename Key, typename Value, std::size_t TotalSize>
struct SimpleSlotAllocator<Key, Value, TotalSize, true>
{
	SimpleSlotAllocator()
	{
		for(std::size_t i=0;i<TotalSize;++i)
			allocator.push((Key)i);
	}

	Key allocate()
	{
		Key index = -1;
		allocator.pop(index);
		return index;
	}

	void deallocate(int32 index)
	{
		allocator.push(index);
	}

	inline Value& operator[](Key index)
	{
		return slots[index];
	}

	Value slots[TotalSize];
	tbb::concurrent_bounded_queue<Key> allocator;
};

template<typename Key, typename Value, std::size_t TotalSize>
struct SimpleSlotAllocator<Key, Value, TotalSize, false>
{
	SimpleSlotAllocator()
	{
		for(std::size_t i=0;i<TotalSize;++i)
			allocator.push(i);
	}

	Key allocate()
	{
		if(allocator.empty())
			return -1;

		Key index = allocator.front();
		allocator.pop();

		return index;
	}

	void deallocate(Key index)
	{
		allocator.push(index);
	}

	inline Value& operator[](Key index)
	{
		return slots[index];
	}

	Value slots[TotalSize];
	std::queue<Key> allocator;
};

}
#endif /* ZILLIANS_SIMPLESLOTALLOCATOR_H_ */
