/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_THREADING_DISPATCHER_H_
#define ZILLIANS_THREADING_DISPATCHER_H_

#include "core/Prerequisite.h"
#include "core/Semaphore.h"
#include "core/AtomicQueue.h"
#include "threading/DispatcherThreadContext.h"
#include "threading/DispatcherNetwork.h"

#include <boost/lockfree/spsc_queue.hpp>

#define ZILLIANS_DISPATCHER_MAX_THREADS     63
#define ZILLIANS_DISPATCHER_PIPE_BOUND_SIZE 256

namespace zillians { namespace threading {

class DispatcherOptions
{
public:
    DispatcherOptions()
      : mMaxDispatcherThreads(ZILLIANS_DISPATCHER_MAX_THREADS)
      , mPipeBoundSize(ZILLIANS_DISPATCHER_PIPE_BOUND_SIZE) {}

    DispatcherOptions& setMaxDispatcherThreads(uint32 n)
    {
        mMaxDispatcherThreads = n;
        return *this;
    }
    DispatcherOptions& setPipeBoundSize(uint32 n)
    {
        mPipeBoundSize = n;
        return *this;
    }
    uint32 getMaxDispatcherThreads() const
    {
        return mMaxDispatcherThreads;
    }
    uint32 getPipeBoundSize() const
    {
        return mPipeBoundSize;
    }

private:
    uint32 mMaxDispatcherThreads;
    uint32 mPipeBoundSize;
};

template<typename Message>
class Dispatcher : public DispatcherNetwork<Message>
{
public:
    Dispatcher(const DispatcherOptions& options)
      : mMaxThreadContextCount(options.getMaxDispatcherThreads())
    {
        BOOST_ASSERT(mMaxThreadContextCount <= ZILLIANS_DISPATCHER_MAX_THREADS);

        mPipes = new boost::lockfree::spsc_queue<Message>*[mMaxThreadContextCount * mMaxThreadContextCount];
        mSignalers = new DispatcherThreadSignaler*[mMaxThreadContextCount];
        mAttachedFlags = new bool[mMaxThreadContextCount];

        for(uint32 i = 0; i < mMaxThreadContextCount; ++i)
        {
            mSignalers[i] = NULL;
            mAttachedFlags[i] = false;
        }

        for(uint32 i = 0; i < mMaxThreadContextCount * mMaxThreadContextCount; ++i)
        {
            mPipes[i] = new boost::lockfree::spsc_queue<Message>{ options.getPipeBoundSize() };
        }
    }

    ~Dispatcher()
    {
        for(uint32 i=0;i<mMaxThreadContextCount;++i)
        {
            BOOST_ASSERT(!mAttachedFlags[i]);
        }

        for(uint32 i = 0; i < mMaxThreadContextCount * mMaxThreadContextCount; ++i)
        {
            SAFE_DELETE(mPipes[i]);
        }

        SAFE_DELETE_ARRAY(mPipes);
        SAFE_DELETE_ARRAY(mSignalers);
        SAFE_DELETE_ARRAY(mAttachedFlags);
    }

public:
    shared_ptr<DispatcherThreadContext<Message> > createThreadContext(int contextId = -1)
    {
        if(contextId == -1)
        {
            for(uint32 i = 0; i < mMaxThreadContextCount; ++i)
            {
                if(!mAttachedFlags[i])
                {
                    contextId = i;
                    break;
                }
            }
        }

        if(contextId < 0)
        {
            BOOST_ASSERT("out of thread context" && 0);
        }

        BOOST_ASSERT(mAttachedFlags[contextId] == false && "context already assigned");

        mAttachedFlags[contextId] = true;
        shared_ptr<DispatcherThreadContext<Message> > context = shared_ptr<DispatcherThreadContext<Message> >(new DispatcherThreadContext<Message>(this, contextId, mMaxThreadContextCount));

        // store the signaler object into the local signaler array
        mSignalers[contextId] = &context->getSignaler();

        return context;
    }   

    virtual void distroyThreadContext(uint32 contextId)
    {
        BOOST_ASSERT(mAttachedFlags[contextId] == true);
        mAttachedFlags[contextId] = false;
        mSignalers[contextId] = NULL;
    }

public:
    virtual void write(uint32 source, uint32 destination, const Message& message, bool incomplete)
    {
        auto pipe = mPipes[source * mMaxThreadContextCount + destination];
        // blocking push
        for(uint32_t backoff = 1; !pipe->push(message); ++backoff)
        {
            usleep(backoff);
        }

        if(!incomplete)
        {
            mSignalers[destination]->signal(source);
        }
    }

    virtual bool read(uint32 source, uint32 destination, Message* message)
    {
        return mPipes[source * mMaxThreadContextCount + destination]->pop(*message);
    }

private:
    boost::lockfree::spsc_queue<Message>** mPipes;
    DispatcherThreadSignaler** mSignalers;
    bool* mAttachedFlags;
    uint32 mMaxThreadContextCount;
};

} }

#endif /* ZILLIANS_THREADING_DISPATCHER_H_ */
