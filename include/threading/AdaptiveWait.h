/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_THREADING_ADAPTIVEWAIT_H_
#define ZILLIANS_THREADING_ADAPTIVEWAIT_H_

#include "core/Prerequisite.h"

namespace zillians { namespace threading {

template<uint32 NumberOfSignalsBeforeEnteringWaitState, uint32 NumberOfSignalsBeforeLeavingWaitState,
        uint32 MinWait, uint32 MaxWait,
        uint32 SlowDownStep, uint32 SpeedUpStep>
struct AdaptiveWait
{
    AdaptiveWait() : mWaiting(false), mSignalsBeforeEnter(0), mSignalsBeforeLeave(0), mTimeToWait(MinWait)
    { }

    void slowdown()
    {
        if(!mWaiting)
        {
            if(++mSignalsBeforeEnter >= NumberOfSignalsBeforeEnteringWaitState)
            {
                mWaiting = true; mTimeToWait = MinWait;
                mSignalsBeforeEnter = 0;
            }
        }
        else
        {
            mTimeToWait += SlowDownStep;
            if(mTimeToWait >= MaxWait) mTimeToWait = MaxWait;
        }
    }

    void speedup()
    {
        if(!mWaiting)
            return;

        if(mTimeToWait > MinWait + SpeedUpStep)
        {
            mTimeToWait -= SpeedUpStep;
        }
        else
        {
            mTimeToWait = MinWait;
            if(++mSignalsBeforeLeave >= NumberOfSignalsBeforeLeavingWaitState)
            {
                mWaiting = false;
                mSignalsBeforeLeave = 0;
            }
        }
    }

    bool is_waiting()
    {
        return mWaiting;
    }

    uint32 time_to_wait()
    {
        return mWaiting ? mTimeToWait : 0;
    }

    void wait()
    {
        if(mWaiting) ::usleep(mTimeToWait);
    }

public:
    bool mWaiting;
    uint32 mSignalsBeforeEnter;
    uint32 mSignalsBeforeLeave;
    uint32 mTimeToWait;
};

} }

#endif /* ZILLIANS_THREADING_ADAPTIVEWAIT_H_ */
