/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_THREADING_DISPATCHERTHREADCONTEXT_H_
#define ZILLIANS_THREADING_DISPATCHERTHREADCONTEXT_H_

#include "core/Prerequisite.h"
#include "core/SharedPtr.h"
#include "core/ContextHub.h"
#include "threading/Dispatcher.h"
#include "threading/DispatcherNetwork.h"
#include "threading/DispatcherDestination.h"
#include "threading/DispatcherThreadSignaler.h"

namespace zillians { namespace threading {

template<typename Message>
class DispatcherThreadContext : public ContextHub<ContextOwnership::transfer>
{
public:
    DispatcherThreadContext(DispatcherNetwork<Message>* dispatcher, uint32 id, uint32 max_thread_id) : mId(id), mMaxThreadId(max_thread_id), mDispatcher(dispatcher)
    { }

    virtual ~DispatcherThreadContext()
    {
        mDispatcher->distroyThreadContext(mId);
    }

public:
    uint32 getIdentity() const
    { return mId; }

    DispatcherNetwork<Message>* getDispatcherNetwork() const
    { return mDispatcher; }

    DispatcherThreadSignaler& getSignaler()
    { return mSignaler; }

public:
    shared_ptr<DispatcherDestination<Message> > createDestination(uint32 dest)
    {
        return shared_ptr<DispatcherDestination<Message> >(new DispatcherDestination<Message>(mDispatcher, mId, dest));
    }

public:
    bool read(/*OUT*/ uint32& source, /*OUT*/ Message& message, bool blocking = false)
    {
        uint32 n = 1;
        return read(&source, &message, n, blocking);
    }

    /**
     * Read the first message available from any pipes
     * @param source
     * @param message
     */
    bool read(/*OUT*/ uint32* source, /*OUT*/ Message* message, /*INOUT*/ uint32& count, bool blocking = false)
    {
        uint64 signals = 0;
        uint32 n = 0;

        if(blocking)
            signals = mSignaler.poll(mId);
        else
            signals = mSignaler.check();

        if(signals)
        {
            for(uint32 i = 0; i < mMaxThreadId && n < count; ++i)
            {
                if(signals & (uint64 (1) << i))
                {
                    for(; n < count; ++n)
                    {
                        if(!mDispatcher->read(i, mId, &message[n]))
                        {
                            signals = signals & ~(uint64(1) << i);
                            break;
                        }

                        if(source)
                            source[n] = i;
                    }
                }
            }
            count = n;
            mSignaler.bitOr(signals);
        }

        return n > 0;
    }

private:
    uint32 mId;
    uint32 mMaxThreadId;
    DispatcherNetwork<Message>* mDispatcher;
    DispatcherThreadSignaler mSignaler;
};

} }

#endif /* ZILLIANS_THREADING_DISPATCHERTHREADCONTEXT_H_ */
