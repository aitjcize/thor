/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_THREADING_DISPATCHERDESTINATION_H_
#define ZILLIANS_THREADING_DISPATCHERDESTINATION_H_

#include "core/Prerequisite.h"
#include "core/SharedPtr.h"
#include "core/ContextHub.h"
#include "threading/DispatcherNetwork.h"

namespace zillians { namespace threading {

template<typename Message>
class DispatcherDestination : public ContextHub<ContextOwnership::transfer>
{
public:
    DispatcherDestination(DispatcherNetwork<Message>* dispatcher, uint32 sourceId, uint32 destId)
    {
        mDispatcher = dispatcher;
        mDestinationId = destId;
        mSouceId = sourceId;
    }

    ~DispatcherDestination()
    { }

public:
    DispatcherNetwork<Message>* getDispatcherNetwork() const
    { return mDispatcher; }

public:
    void write(const Message& message)
    {
        write(&message, 1);
    }

    void write(const Message* message, uint32 count)
    {
        for(uint32 i = 0; i < count - 1; ++i)
            mDispatcher->write(mSouceId, mDestinationId, message[i], true);
        mDispatcher->write(mSouceId, mDestinationId, message[count-1], false);
    }

    bool read(Message* message, bool blocking = false)
    {
        return read(message, 1, blocking);
    }

    bool read(Message* messages, uint32 count, bool blocking = false)
    {
        uint32 n = 0;

        if(UNLIKELY(blocking))
        {
            while(!mDispatcher->read(mSouceId, mDestinationId, messages)) { }
            ++n;
        }

        for(;n < count; ++n)
        {
            if(!mDispatcher->read(mSouceId, mDestinationId, messages))
                break;
        }

        return n > 0U;
    }

private:
    DispatcherNetwork<Message>* mDispatcher;
    uint32 mDestinationId;
    uint32 mSouceId;
};

} }

#endif /* ZILLIANS_THREADING_DISPATCHERDESTINATION_H_ */
