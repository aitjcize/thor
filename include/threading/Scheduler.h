/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_THREADING_SCHEDULER_H_
#define ZILLIANS_THREADING_SCHEDULER_H_

#include "core/Prerequisite.h"
#include <boost/asio.hpp>

namespace zillians {

struct Scheduler final : public boost::asio::io_service
{
	Scheduler(uint32 concurrency = 1) : boost::asio::io_service(concurrency)
	{
		if(concurrency > 0)
		{
			for(uint32 i=0;i<concurrency;++i)
			{
				boost::thread* t = new boost::thread([=] {
						boost::asio::io_service::work dummy(*this);
						while(true)
						{
							try
							{
								this->run();
								break;
							}
							catch(std::exception& ex)
							{
								std::cerr << "Scheduler catches uncaught exception: " << ex.what() << std::endl;
							}
						}
				});
				threads.push_back(t);
			}
		}
	}

	~Scheduler()
	{
		stop();
		wait();

		for(auto i = threads.begin(); i != threads.end(); ++i)
			SAFE_DELETE(*i);
		threads.clear();
	}

	void wait()
	{
		for(auto i = threads.begin(); i != threads.end(); ++i)
		{
			if((*i)->joinable())
				(*i)->join();
		}
	}

	std::vector<boost::thread*> threads;
};

struct Synchronized : public boost::asio::strand
{
	Synchronized(Scheduler* scheduler) : boost::asio::strand(*scheduler)
	{ }
};

}

#endif
