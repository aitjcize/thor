#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# - Try to find the TBB libraries in system path
# Once done this will define
#
# TBB_FOUND - system has TBB
# TBB_INCLUDE_DIR - the TBB include directory
# TBB_LIBRARIES - TBB library

MESSAGE(STATUS "Looking for TBB...")

FIND_PATH(TBB_INCLUDE_DIR cudpp.h PATHS 
	/usr/include/ 
	/usr/local/include/
	${CUDA_INCLUDE_DIRS}
	)

FIND_LIBRARY(TBB_LIBRARIES_TBB NAMES tbb)
FIND_LIBRARY(TBB_LIBRARIES_TBBMALLOC NAMES tbbmalloc)
#FIND_LIBRARY(TBB_LIBRARIES_TBBMALLOC_PROXY NAMES tbbmalloc_proxy)

FIND_LIBRARY(TBB_DEBUG_LIBRARIES_TBB NAMES tbb_debug)
FIND_LIBRARY(TBB_DEBUG_LIBRARIES_TBBMALLOC NAMES tbbmalloc_debug)
#FIND_LIBRARY(TBB_DEBUG_LIBRARIES_TBBMALLOC_PROXY NAMES tbbmalloc_proxy_debug)

FIND_PATH(TBB_INCLUDE_DIR tbb/task_scheduler_init.h)

IF(TBB_INCLUDE_DIR AND TBB_LIBRARIES_TBB AND TBB_LIBRARIES_TBBMALLOC) #AND TBB_LIBRARIES_TBBMALLOC_PROXY)
	SET(TBB_FOUND 1)
	SET(TBB_LIBRARIES ${TBB_LIBRARIES_TBB} ${TBB_LIBRARIES_TBBMALLOC})# ${TBB_LIBRARIES_TBBMALLOC_PROXY})

    IF(TBB_DEBUG_LIBRARIES_TBB AND TBB_DEBUG_LIBRARIES_TBBMALLOC)# AND TBB_DEBUG_LIBRARIES_TBBMALLOC_PROXY)
        SET(TBB_DEBUG_FOUND 1)
        SET(TBB_DEBUG_LIBRARIES ${TBB_DEBUG_LIBRARIES_TBB} ${TBB_DEBUG_LIBRARIES_TBBMALLOC})# ${TBB_DEBUG_LIBRARIES_TBBMALLOC_PROXY})
    ENDIF()
    
	IF(NOT TBB_FIND_QUIETLY)
		MESSAGE(STATUS "Found TBB: release libraries = ${TBB_LIBRARIES}, debug libraries = ${TBB_DEBUG_LIBRARIES}")
	ENDIF()
ELSE()
	SET(TBB_FOUND 0 CACHE BOOL "TBB not found")
	IF(TBB_FIND_REQUIRED)
	    MESSAGE(FATAL_ERROR "Could NOT find TBB, error")
	ELSE()
	    MESSAGE(STATUS "Could NOT find TBB, disabled")
	ENDIF()
ENDIF()

MARK_AS_ADVANCED(TBB_INCLUDE_DIR TBB_LIBRARIES TBB_DEBUG_LIBRARIES_TBB)

