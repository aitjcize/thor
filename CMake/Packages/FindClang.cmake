#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# - Find Clang
# Find the Clang includes and library
#
#  Clang_FOUND        - True if Clang found.
#  Clang_INCLUDE_DIRS - where to find Clang headers
#  Clang_LIB_DIRS     - where to find Clang libraries
#  Clang_LIBS         - Clang libraries

SET(Clang_FOUND FALSE)

IF(NOT LLVM_FOUND)
    MESSAGE(FATAL_ERROR "LLVM is not found but Clang requires it!")
ELSE()
    SET(clang_libs
        clangAnalysis
        clangARCMigrate
        clangAST
        clangBasic
        clangCodeGen
        clangDriver
        clangEdit
        clangFrontend
        clangFrontendTool
        clangLex
        clangParse
        clangRewrite
        clangSema
        clangSerialization
        clangStaticAnalyzerCheckers
        clangStaticAnalyzerCore
        clangStaticAnalyzerFrontend
        clangTooling
    )

    FOREACH(clang_lib ${clang_libs})
        SET(clang_lib_var Clang_${clang_lib}_LIB)

        FIND_LIBRARY(${clang_lib_var} ${clang_lib} ${LLVM_LIB_DIR})
        IF(${clang_lib_var})
            SET(Clang_LIBS ${Clang_LIBS} ${${clang_lib_var}})
        ENDIF()

        MARK_AS_ADVANCED(${clang_lib_var})
    ENDFOREACH()

    SET(Clang_INCLUDE_DIRS ${LLVM_INCLUDE_DIR} CACHE PATH "Clang include direcotry ")
    SET(Clang_LIB_DIRS ${LLVM_LIB_DIR} CACHE PATH "Clang library directory")
    SET(Clang_CXX_FLAGS ${LLVM_CPPFLAGS} CACHE STRING "Clang CXX Flags")

    #MESSAGE(STATUS "Clang libs: " ${Clang_LIBS})

    IF(Clang_LIBS)
        SET(Clang_FOUND TRUE)
    ELSE()
        MESSAGE("Could not find Clang libraries")
    ENDIF()
ENDIF()
