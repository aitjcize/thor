#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# - Try to find the Corosync libraries
# Once done this will define
#
# COROSYNC_FOUND - system has Corosync
# COROSYNC_INCLUDE_DIR - the Corosync include directory
# COROSYNC_LIBRARIES - Corosync library

MESSAGE(STATUS "Looking for COROSYNC...")

FIND_PATH(COROSYNC_INCLUDE_DIR cpg.h PATHS 
	/usr/include/ 
	/usr/local/include/
	PATH_SUFFIXES "corosync"
	)
     
FIND_LIBRARY(COROSYNC_LIBRARY_CPG NAMES cpg PATHS
    /usr/lib/
    /usr/local/lib/ 
    )
    
FIND_LIBRARY(COROSYNC_LIBRARY_EVS NAMES evs PATHS
    /usr/lib/
    /usr/local/lib/ 
    )
    
FIND_LIBRARY(COROSYNC_LIBRARY_COROIPCC NAMES coroipcc PATHS
    /usr/lib/
    /usr/local/lib/ 
    )
    
FIND_LIBRARY(COROSYNC_LIBRARY_QUORUM NAMES quorum PATHS
    /usr/lib/
    /usr/local/lib/ 
    )
    
FIND_LIBRARY(COROSYNC_LIBRARY_CONFDB NAMES confdb PATHS
    /usr/lib/
    /usr/local/lib/ 
    )

FIND_LIBRARY(COROSYNC_LIBRARY_CFG NAMES cfg PATHS
    /usr/lib/
    /usr/local/lib/ 
    )
    
IF(COROSYNC_INCLUDE_DIR AND COROSYNC_LIBRARY_CPG AND COROSYNC_LIBRARY_EVS AND COROSYNC_LIBRARY_COROIPCC AND COROSYNC_LIBRARY_QUORUM AND COROSYNC_LIBRARY_CONFDB AND COROSYNC_LIBRARY_CFG)
	SET(COROSYNC_FOUND 1)
	SET(COROSYNC_LIBRARIES "")
	LIST(APPEND COROSYNC_LIBRARIES ${COROSYNC_LIBRARY_CPG} ${COROSYNC_LIBRARY_EVS} ${COROSYNC_LIBRARY_COROIPCC} ${COROSYNC_LIBRARY_QUORUM} ${COROSYNC_LIBRARY_CONFDB} ${COROSYNC_LIBRARY_CFG})
	SET(COROSYNC_LIBRARIES ${COROSYNC_LIBRARIES} CACHE STRING "Corosync libraries" FORCE)
	IF(NOT COROSYNC_FIND_QUIETLY)
		MESSAGE(STATUS "Found Corosync: libraries = ${COROSYNC_LIBRARIES}")
	ENDIF(NOT COROSYNC_FIND_QUIETLY)
ELSE()
	SET(COROSYNC_FOUND 0 CACHE BOOL "Corosync not found")
	IF(COROSYNC_FIND_REQUIRED)
	    MESSAGE(FATAL_ERROR "Could NOT find COROSYNC, error")
	ELSE()
	    MESSAGE(STATUS "Could NOT find COROSYNC, disabled")
	ENDIF()
ENDIF()

MARK_AS_ADVANCED(COROSYNC_INCLUDE_DIR COROSYNC_LIBRARIES)

