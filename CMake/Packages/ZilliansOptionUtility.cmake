#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#
#
# Split list of arguments into seperate list stored in a hashmap
#
macro(split_options options default_catagory catagories option_sets)
    #message("split_options: ${options} ${catagories} ${option_sets}")
    set(__default_option_list "")
    hashmap(CLEAR ${option_sets})
    foreach(__catalog IN LISTS ${catagories})
        #message("split_options: catalog found \"${__catalog}\"")
        hashmap(PUT ${option_sets} ${__catalog} "")
    endforeach()
    
    set(__current_catalog ${default_catagory})
    foreach(__option IN LISTS ${options})
        list(FIND ${catagories} ${__option} __catalog_index)
        if(NOT (__catalog_index EQUAL -1) )
            list(GET ${catagories} ${__catalog_index} __current_catalog)
        else()        
            if(__current_catalog STREQUAL ${default_catagory})
                list(APPEND __default_option_list ${__option})
            else()
                #message("updating option list for ${__current_catalog}")
                set(__current_option_list "")
                hashmap(GET ${option_sets} ${__current_catalog} __current_option_list)
                list(APPEND __current_option_list ${__option})
                hashmap(PUT ${option_sets} ${__current_catalog} __current_option_list)
            endif()
        endif()
    endforeach()
    
    #message("__default_option_list = ${__default_option_list}")
    if(NOT (__default_option_list STREQUAL "") )
        hashmap(PUT ${option_sets} ${default_catagory} __default_option_list)
    endif()
endmacro()