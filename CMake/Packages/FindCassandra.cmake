#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# - Try to find the Cassandra libraries
# Once done this will define
#
# CASSANDRA_FOUND - system has Cassandra
# CASSANDRA_INCLUDE_DIR - the Cassandra include directory
# CASSANDRA_LIBRARIES - Cassandra library

IF(NOT WIN32)
    FIND_PATH(CASSANDRA_INCLUDE_DIR Cassandra.h PATHS
        ${CMAKE_SOURCE_DIR}/dep/linux/cassandra/include
        )
ENDIF()

IF(CASSANDRA_INCLUDE_DIR)
    SET(CASSANDRA_FOUND 1)
    IF(NOT CASSANDRA_FOUND_QUIETLY)
        MESSAGE(STATUS "Found CASSANDRA: include = ${CASSANDRA_INCLUDE_DIR}, libraries = ${CASSANDRA_LIBRARIES}")
    ENDIF()
ELSE()
    SET(CASSANDRA_FOUND 0 CACHE BOOL "CASSANDRA not found")
    MESSAGE(STATUS "CASSANDRA not found, disabled")
ENDIF()

MARK_AS_ADVANCED(CASSANDRA_INCLUDE_DIR)
