#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# Locate ActiveMQ-CPP include paths and libraries

# This module defines
# AMQ_INCLUDES, where to find *.h, etc.
# AMQ_LIBS, the libraries to link against to use ActiveMQ-CPP.
# AMQ_FLAGS, the flags to use to compile
# AMQ_FOUND, set to 'yes' if found

MESSAGE(STATUS "Looking for AMQ...")

find_program(AMQ_CONFIG_EXECUTABLE
    activemqcpp-config
    /usr/local/bin
    /usr/bin
    )

mark_as_advanced(AMQ_CONFIG_EXECUTABLE)

macro(_amq_invoke _varname _regexp)
    execute_process(
        COMMAND ${AMQ_CONFIG_EXECUTABLE} ${ARGN}
        OUTPUT_VARIABLE _amq_output
        RESULT_VARIABLE _amq_failed
    )

    if(_amq_failed)
        message(FATAL_ERROR "activemqcpp-config ${ARGN} failed")
    else(_amq_failed)
        string(REGEX REPLACE "[\r\n]"  "" _amq_output "${_amq_output}")
        string(REGEX REPLACE " +$"     "" _amq_output "${_amq_output}")

        if(NOT ${_regexp} STREQUAL "")
            string(REGEX REPLACE "${_regexp}" " " _amq_output "${_amq_output}")
        endif(NOT ${_regexp} STREQUAL "")

        separate_arguments(_amq_output)
        set(${_varname} "${_amq_output}")
    endif(_amq_failed)
endmacro(_amq_invoke)

_amq_invoke(AMQ_INCLUDES "(^| )-I" --includes)
_amq_invoke(AMQ_FLAGS    ""        --cflags)
_amq_invoke(AMQ_LIBS     ""        --libs)

if(AMQ_INCLUDES AND AMQ_LIBS)
    set(AMQ_FOUND "YES")
    message (STATUS "Found AMQ: libraries = ${AMQ_LIBS}")
endif(AMQ_INCLUDES AND AMQ_LIBS)
